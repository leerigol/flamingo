#include "spu.h"
Spu_reg SpuMemProxy::getVERSION()
{
	return VERSION;
}


Spu_reg SpuMemProxy::getTEST()
{
	return TEST;
}


void SpuMemProxy::assignCTRL_spu_clr( Spu_reg value )
{
	mem_assign_field(CTRL,spu_clr,value);
}
void SpuMemProxy::assignCTRL_adc_sync( Spu_reg value )
{
	mem_assign_field(CTRL,adc_sync,value);
}
void SpuMemProxy::assignCTRL_adc_align_check( Spu_reg value )
{
	mem_assign_field(CTRL,adc_align_check,value);
}
void SpuMemProxy::assignCTRL_non_inter( Spu_reg value )
{
	mem_assign_field(CTRL,non_inter,value);
}
void SpuMemProxy::assignCTRL_peak( Spu_reg value )
{
	mem_assign_field(CTRL,peak,value);
}
void SpuMemProxy::assignCTRL_hi_res( Spu_reg value )
{
	mem_assign_field(CTRL,hi_res,value);
}
void SpuMemProxy::assignCTRL_anti_alias( Spu_reg value )
{
	mem_assign_field(CTRL,anti_alias,value);
}
void SpuMemProxy::assignCTRL_trace_avg( Spu_reg value )
{
	mem_assign_field(CTRL,trace_avg,value);
}
void SpuMemProxy::assignCTRL_trace_avg_mem( Spu_reg value )
{
	mem_assign_field(CTRL,trace_avg_mem,value);
}
void SpuMemProxy::assignCTRL_adc_data_inv( Spu_reg value )
{
	mem_assign_field(CTRL,adc_data_inv,value);
}
void SpuMemProxy::assignCTRL_la_probe( Spu_reg value )
{
	mem_assign_field(CTRL,la_probe,value);
}
void SpuMemProxy::assignCTRL_coarse_trig_range( Spu_reg value )
{
	mem_assign_field(CTRL,coarse_trig_range,value);
}

void SpuMemProxy::assignCTRL( Spu_reg value )
{
	mem_assign_field(CTRL,payload,value);
}


Spu_reg SpuMemProxy::getCTRL_spu_clr()
{
	return CTRL.spu_clr;
}
Spu_reg SpuMemProxy::getCTRL_adc_sync()
{
	return CTRL.adc_sync;
}
Spu_reg SpuMemProxy::getCTRL_adc_align_check()
{
	return CTRL.adc_align_check;
}
Spu_reg SpuMemProxy::getCTRL_non_inter()
{
	return CTRL.non_inter;
}
Spu_reg SpuMemProxy::getCTRL_peak()
{
	return CTRL.peak;
}
Spu_reg SpuMemProxy::getCTRL_hi_res()
{
	return CTRL.hi_res;
}
Spu_reg SpuMemProxy::getCTRL_anti_alias()
{
	return CTRL.anti_alias;
}
Spu_reg SpuMemProxy::getCTRL_trace_avg()
{
	return CTRL.trace_avg;
}
Spu_reg SpuMemProxy::getCTRL_trace_avg_mem()
{
	return CTRL.trace_avg_mem;
}
Spu_reg SpuMemProxy::getCTRL_adc_data_inv()
{
	return CTRL.adc_data_inv;
}
Spu_reg SpuMemProxy::getCTRL_la_probe()
{
	return CTRL.la_probe;
}
Spu_reg SpuMemProxy::getCTRL_coarse_trig_range()
{
	return CTRL.coarse_trig_range;
}

Spu_reg SpuMemProxy::getCTRL()
{
	return CTRL.payload;
}


void SpuMemProxy::assignDEBUG_gray_bypass( Spu_reg value )
{
	mem_assign_field(DEBUG,gray_bypass,value);
}
void SpuMemProxy::assignDEBUG_ms_order( Spu_reg value )
{
	mem_assign_field(DEBUG,ms_order,value);
}
void SpuMemProxy::assignDEBUG_s_view( Spu_reg value )
{
	mem_assign_field(DEBUG,s_view,value);
}
void SpuMemProxy::assignDEBUG_bram_ddr_n( Spu_reg value )
{
	mem_assign_field(DEBUG,bram_ddr_n,value);
}
void SpuMemProxy::assignDEBUG_ddr_init( Spu_reg value )
{
	mem_assign_field(DEBUG,ddr_init,value);
}
void SpuMemProxy::assignDEBUG_ddr_err( Spu_reg value )
{
	mem_assign_field(DEBUG,ddr_err,value);
}
void SpuMemProxy::assignDEBUG_dbg_trig_src( Spu_reg value )
{
	mem_assign_field(DEBUG,dbg_trig_src,value);
}
void SpuMemProxy::assignDEBUG_ddr_fsm_state( Spu_reg value )
{
	mem_assign_field(DEBUG,ddr_fsm_state,value);
}
void SpuMemProxy::assignDEBUG_debug_mem( Spu_reg value )
{
	mem_assign_field(DEBUG,debug_mem,value);
}

void SpuMemProxy::assignDEBUG( Spu_reg value )
{
	mem_assign_field(DEBUG,payload,value);
}


Spu_reg SpuMemProxy::getDEBUG_gray_bypass()
{
	return DEBUG.gray_bypass;
}
Spu_reg SpuMemProxy::getDEBUG_ms_order()
{
	return DEBUG.ms_order;
}
Spu_reg SpuMemProxy::getDEBUG_s_view()
{
	return DEBUG.s_view;
}
Spu_reg SpuMemProxy::getDEBUG_bram_ddr_n()
{
	return DEBUG.bram_ddr_n;
}
Spu_reg SpuMemProxy::getDEBUG_ddr_init()
{
	return DEBUG.ddr_init;
}
Spu_reg SpuMemProxy::getDEBUG_ddr_err()
{
	return DEBUG.ddr_err;
}
Spu_reg SpuMemProxy::getDEBUG_dbg_trig_src()
{
	return DEBUG.dbg_trig_src;
}
Spu_reg SpuMemProxy::getDEBUG_ddr_fsm_state()
{
	return DEBUG.ddr_fsm_state;
}
Spu_reg SpuMemProxy::getDEBUG_debug_mem()
{
	return DEBUG.debug_mem;
}

Spu_reg SpuMemProxy::getDEBUG()
{
	return DEBUG.payload;
}


void SpuMemProxy::assignADC_DATA_IDELAY_idelay_var( Spu_reg value )
{
	mem_assign_field(ADC_DATA_IDELAY,idelay_var,value);
}
void SpuMemProxy::assignADC_DATA_IDELAY_chn_bit( Spu_reg value )
{
	mem_assign_field(ADC_DATA_IDELAY,chn_bit,value);
}
void SpuMemProxy::assignADC_DATA_IDELAY_h( Spu_reg value )
{
	mem_assign_field(ADC_DATA_IDELAY,h,value);
}
void SpuMemProxy::assignADC_DATA_IDELAY_l( Spu_reg value )
{
	mem_assign_field(ADC_DATA_IDELAY,l,value);
}
void SpuMemProxy::assignADC_DATA_IDELAY_core_ABCD( Spu_reg value )
{
	mem_assign_field(ADC_DATA_IDELAY,core_ABCD,value);
}

void SpuMemProxy::assignADC_DATA_IDELAY( Spu_reg value )
{
	mem_assign_field(ADC_DATA_IDELAY,payload,value);
}


Spu_reg SpuMemProxy::getADC_DATA_IDELAY_idelay_var()
{
	return ADC_DATA_IDELAY.idelay_var;
}
Spu_reg SpuMemProxy::getADC_DATA_IDELAY_chn_bit()
{
	return ADC_DATA_IDELAY.chn_bit;
}
Spu_reg SpuMemProxy::getADC_DATA_IDELAY_h()
{
	return ADC_DATA_IDELAY.h;
}
Spu_reg SpuMemProxy::getADC_DATA_IDELAY_l()
{
	return ADC_DATA_IDELAY.l;
}
Spu_reg SpuMemProxy::getADC_DATA_IDELAY_core_ABCD()
{
	return ADC_DATA_IDELAY.core_ABCD;
}

Spu_reg SpuMemProxy::getADC_DATA_IDELAY()
{
	return ADC_DATA_IDELAY.payload;
}


Spu_reg SpuMemProxy::getADC_DATA_CHECK_a_l()
{
	return ADC_DATA_CHECK.a_l;
}
Spu_reg SpuMemProxy::getADC_DATA_CHECK_a_h()
{
	return ADC_DATA_CHECK.a_h;
}
Spu_reg SpuMemProxy::getADC_DATA_CHECK_b_l()
{
	return ADC_DATA_CHECK.b_l;
}
Spu_reg SpuMemProxy::getADC_DATA_CHECK_b_h()
{
	return ADC_DATA_CHECK.b_h;
}
Spu_reg SpuMemProxy::getADC_DATA_CHECK_c_l()
{
	return ADC_DATA_CHECK.c_l;
}
Spu_reg SpuMemProxy::getADC_DATA_CHECK_c_h()
{
	return ADC_DATA_CHECK.c_h;
}
Spu_reg SpuMemProxy::getADC_DATA_CHECK_d_l()
{
	return ADC_DATA_CHECK.d_l;
}
Spu_reg SpuMemProxy::getADC_DATA_CHECK_d_h()
{
	return ADC_DATA_CHECK.d_h;
}

Spu_reg SpuMemProxy::getADC_DATA_CHECK()
{
	return ADC_DATA_CHECK.payload;
}


Spu_reg SpuMemProxy::getADC_CHN_ALIGN_CHK_adc_d()
{
	return ADC_CHN_ALIGN_CHK.adc_d;
}
Spu_reg SpuMemProxy::getADC_CHN_ALIGN_CHK_adc_c()
{
	return ADC_CHN_ALIGN_CHK.adc_c;
}
Spu_reg SpuMemProxy::getADC_CHN_ALIGN_CHK_adc_b()
{
	return ADC_CHN_ALIGN_CHK.adc_b;
}
Spu_reg SpuMemProxy::getADC_CHN_ALIGN_CHK_adc_a()
{
	return ADC_CHN_ALIGN_CHK.adc_a;
}

Spu_reg SpuMemProxy::getADC_CHN_ALIGN_CHK()
{
	return ADC_CHN_ALIGN_CHK.payload;
}


void SpuMemProxy::assignADC_CHN_ALIGN_adc_d( Spu_reg value )
{
	mem_assign_field(ADC_CHN_ALIGN,adc_d,value);
}
void SpuMemProxy::assignADC_CHN_ALIGN_adc_c( Spu_reg value )
{
	mem_assign_field(ADC_CHN_ALIGN,adc_c,value);
}
void SpuMemProxy::assignADC_CHN_ALIGN_adc_b( Spu_reg value )
{
	mem_assign_field(ADC_CHN_ALIGN,adc_b,value);
}
void SpuMemProxy::assignADC_CHN_ALIGN_adc_a( Spu_reg value )
{
	mem_assign_field(ADC_CHN_ALIGN,adc_a,value);
}

void SpuMemProxy::assignADC_CHN_ALIGN( Spu_reg value )
{
	mem_assign_field(ADC_CHN_ALIGN,payload,value);
}


Spu_reg SpuMemProxy::getADC_CHN_ALIGN_adc_d()
{
	return ADC_CHN_ALIGN.adc_d;
}
Spu_reg SpuMemProxy::getADC_CHN_ALIGN_adc_c()
{
	return ADC_CHN_ALIGN.adc_c;
}
Spu_reg SpuMemProxy::getADC_CHN_ALIGN_adc_b()
{
	return ADC_CHN_ALIGN.adc_b;
}
Spu_reg SpuMemProxy::getADC_CHN_ALIGN_adc_a()
{
	return ADC_CHN_ALIGN.adc_a;
}

Spu_reg SpuMemProxy::getADC_CHN_ALIGN()
{
	return ADC_CHN_ALIGN.payload;
}


void SpuMemProxy::assignPRE_PROC_proc_en( Spu_reg value )
{
	mem_assign_field(PRE_PROC,proc_en,value);
}
void SpuMemProxy::assignPRE_PROC_cfg_filter_en( Spu_reg value )
{
	mem_assign_field(PRE_PROC,cfg_filter_en,value);
}

void SpuMemProxy::assignPRE_PROC( Spu_reg value )
{
	mem_assign_field(PRE_PROC,payload,value);
}


void SpuMemProxy::assignFILTER_coeff( Spu_reg value )
{
	mem_assign_field(FILTER,coeff,value);
}
void SpuMemProxy::assignFILTER_coeff_id( Spu_reg value )
{
	mem_assign_field(FILTER,coeff_id,value);
}
void SpuMemProxy::assignFILTER_coeff_sel( Spu_reg value )
{
	mem_assign_field(FILTER,coeff_sel,value);
}
void SpuMemProxy::assignFILTER_coeff_ch( Spu_reg value )
{
	mem_assign_field(FILTER,coeff_ch,value);
}

void SpuMemProxy::assignFILTER( Spu_reg value )
{
	mem_assign_field(FILTER,payload,value);
}


void SpuMemProxy::assignDOWN_SAMP_FAST_down_samp_fast( Spu_reg value )
{
	mem_assign_field(DOWN_SAMP_FAST,down_samp_fast,value);
}
void SpuMemProxy::assignDOWN_SAMP_FAST_en( Spu_reg value )
{
	mem_assign_field(DOWN_SAMP_FAST,en,value);
}

void SpuMemProxy::assignDOWN_SAMP_FAST( Spu_reg value )
{
	mem_assign_field(DOWN_SAMP_FAST,payload,value);
}


Spu_reg SpuMemProxy::getDOWN_SAMP_FAST_down_samp_fast()
{
	return DOWN_SAMP_FAST.down_samp_fast;
}
Spu_reg SpuMemProxy::getDOWN_SAMP_FAST_en()
{
	return DOWN_SAMP_FAST.en;
}

Spu_reg SpuMemProxy::getDOWN_SAMP_FAST()
{
	return DOWN_SAMP_FAST.payload;
}


void SpuMemProxy::assignDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value )
{
	mem_assign_field(DOWN_SAMP_SLOW_H,down_samp_slow_h,value);
}

void SpuMemProxy::assignDOWN_SAMP_SLOW_H( Spu_reg value )
{
	mem_assign_field(DOWN_SAMP_SLOW_H,payload,value);
}


Spu_reg SpuMemProxy::getDOWN_SAMP_SLOW_H_down_samp_slow_h()
{
	return DOWN_SAMP_SLOW_H.down_samp_slow_h;
}

Spu_reg SpuMemProxy::getDOWN_SAMP_SLOW_H()
{
	return DOWN_SAMP_SLOW_H.payload;
}


void SpuMemProxy::assignDOWN_SAMP_SLOW_L( Spu_reg value )
{
	mem_assign(DOWN_SAMP_SLOW_L,value);
}


Spu_reg SpuMemProxy::getDOWN_SAMP_SLOW_L()
{
	return DOWN_SAMP_SLOW_L;
}


void SpuMemProxy::assignLA_DELAY( Spu_reg value )
{
	mem_assign(LA_DELAY,value);
}


Spu_reg SpuMemProxy::getLA_DELAY()
{
	return LA_DELAY;
}


void SpuMemProxy::assignCHN_DELAY_AB_B( Spu_reg value )
{
	mem_assign_field(CHN_DELAY_AB,B,value);
}
void SpuMemProxy::assignCHN_DELAY_AB_A( Spu_reg value )
{
	mem_assign_field(CHN_DELAY_AB,A,value);
}

void SpuMemProxy::assignCHN_DELAY_AB( Spu_reg value )
{
	mem_assign_field(CHN_DELAY_AB,payload,value);
}


Spu_reg SpuMemProxy::getCHN_DELAY_AB_B()
{
	return CHN_DELAY_AB.B;
}
Spu_reg SpuMemProxy::getCHN_DELAY_AB_A()
{
	return CHN_DELAY_AB.A;
}

Spu_reg SpuMemProxy::getCHN_DELAY_AB()
{
	return CHN_DELAY_AB.payload;
}


void SpuMemProxy::assignCHN_DELAY_CD_D( Spu_reg value )
{
	mem_assign_field(CHN_DELAY_CD,D,value);
}
void SpuMemProxy::assignCHN_DELAY_CD_C( Spu_reg value )
{
	mem_assign_field(CHN_DELAY_CD,C,value);
}

void SpuMemProxy::assignCHN_DELAY_CD( Spu_reg value )
{
	mem_assign_field(CHN_DELAY_CD,payload,value);
}


Spu_reg SpuMemProxy::getCHN_DELAY_CD_D()
{
	return CHN_DELAY_CD.D;
}
Spu_reg SpuMemProxy::getCHN_DELAY_CD_C()
{
	return CHN_DELAY_CD.C;
}

Spu_reg SpuMemProxy::getCHN_DELAY_CD()
{
	return CHN_DELAY_CD.payload;
}


void SpuMemProxy::assignRANDOM_RANGE( Spu_reg value )
{
	mem_assign(RANDOM_RANGE,value);
}


Spu_reg SpuMemProxy::getRANDOM_RANGE()
{
	return RANDOM_RANGE;
}


void SpuMemProxy::assignEXT_TRIG_IDEALY_idelay_var( Spu_reg value )
{
	mem_assign_field(EXT_TRIG_IDEALY,idelay_var,value);
}
void SpuMemProxy::assignEXT_TRIG_IDEALY_ld( Spu_reg value )
{
	mem_assign_field(EXT_TRIG_IDEALY,ld,value);
}

void SpuMemProxy::assignEXT_TRIG_IDEALY( Spu_reg value )
{
	mem_assign_field(EXT_TRIG_IDEALY,payload,value);
}


Spu_reg SpuMemProxy::getEXT_TRIG_IDEALY_idelay_var()
{
	return EXT_TRIG_IDEALY.idelay_var;
}
Spu_reg SpuMemProxy::getEXT_TRIG_IDEALY_ld()
{
	return EXT_TRIG_IDEALY.ld;
}

Spu_reg SpuMemProxy::getEXT_TRIG_IDEALY()
{
	return EXT_TRIG_IDEALY.payload;
}


void SpuMemProxy::assignSEGMENT_SIZE( Spu_reg value )
{
	mem_assign(SEGMENT_SIZE,value);
}


Spu_reg SpuMemProxy::getSEGMENT_SIZE()
{
	return SEGMENT_SIZE;
}


void SpuMemProxy::assignRECORD_LEN( Spu_reg value )
{
	mem_assign(RECORD_LEN,value);
}


Spu_reg SpuMemProxy::getRECORD_LEN()
{
	return RECORD_LEN;
}


void SpuMemProxy::assignWAVE_LEN_MAIN( Spu_reg value )
{
	mem_assign(WAVE_LEN_MAIN,value);
}


Spu_reg SpuMemProxy::getWAVE_LEN_MAIN()
{
	return WAVE_LEN_MAIN;
}


void SpuMemProxy::assignWAVE_LEN_ZOOM( Spu_reg value )
{
	mem_assign(WAVE_LEN_ZOOM,value);
}


Spu_reg SpuMemProxy::getWAVE_LEN_ZOOM()
{
	return WAVE_LEN_ZOOM;
}


void SpuMemProxy::assignWAVE_OFFSET_MAIN( Spu_reg value )
{
	mem_assign(WAVE_OFFSET_MAIN,value);
}


Spu_reg SpuMemProxy::getWAVE_OFFSET_MAIN()
{
	return WAVE_OFFSET_MAIN;
}


void SpuMemProxy::assignWAVE_OFFSET_ZOOM( Spu_reg value )
{
	mem_assign(WAVE_OFFSET_ZOOM,value);
}


Spu_reg SpuMemProxy::getWAVE_OFFSET_ZOOM()
{
	return WAVE_OFFSET_ZOOM;
}


void SpuMemProxy::assignMEAS_OFFSET_CH( Spu_reg value )
{
	mem_assign(MEAS_OFFSET_CH,value);
}


Spu_reg SpuMemProxy::getMEAS_OFFSET_CH()
{
	return MEAS_OFFSET_CH;
}


void SpuMemProxy::assignMEAS_LEN_CH( Spu_reg value )
{
	mem_assign(MEAS_LEN_CH,value);
}


Spu_reg SpuMemProxy::getMEAS_LEN_CH()
{
	return MEAS_LEN_CH;
}


void SpuMemProxy::assignMEAS_OFFSET_LA( Spu_reg value )
{
	mem_assign(MEAS_OFFSET_LA,value);
}


Spu_reg SpuMemProxy::getMEAS_OFFSET_LA()
{
	return MEAS_OFFSET_LA;
}


void SpuMemProxy::assignMEAS_LEN_LA( Spu_reg value )
{
	mem_assign(MEAS_LEN_LA,value);
}


Spu_reg SpuMemProxy::getMEAS_LEN_LA()
{
	return MEAS_LEN_LA;
}


void SpuMemProxy::assignCOARSE_DELAY_MAIN_chn_delay( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_MAIN,chn_delay,value);
}
void SpuMemProxy::assignCOARSE_DELAY_MAIN_chn_ce( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_MAIN,chn_ce,value);
}
void SpuMemProxy::assignCOARSE_DELAY_MAIN_offset_en( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_MAIN,offset_en,value);
}
void SpuMemProxy::assignCOARSE_DELAY_MAIN_delay_en( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_MAIN,delay_en,value);
}

void SpuMemProxy::assignCOARSE_DELAY_MAIN( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getCOARSE_DELAY_MAIN_chn_delay()
{
	return COARSE_DELAY_MAIN.chn_delay;
}
Spu_reg SpuMemProxy::getCOARSE_DELAY_MAIN_chn_ce()
{
	return COARSE_DELAY_MAIN.chn_ce;
}
Spu_reg SpuMemProxy::getCOARSE_DELAY_MAIN_offset_en()
{
	return COARSE_DELAY_MAIN.offset_en;
}
Spu_reg SpuMemProxy::getCOARSE_DELAY_MAIN_delay_en()
{
	return COARSE_DELAY_MAIN.delay_en;
}

Spu_reg SpuMemProxy::getCOARSE_DELAY_MAIN()
{
	return COARSE_DELAY_MAIN.payload;
}


void SpuMemProxy::assignCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_ZOOM,chn_delay,value);
}
void SpuMemProxy::assignCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_ZOOM,chn_ce,value);
}
void SpuMemProxy::assignCOARSE_DELAY_ZOOM_offset_en( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_ZOOM,offset_en,value);
}
void SpuMemProxy::assignCOARSE_DELAY_ZOOM_delay_en( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_ZOOM,delay_en,value);
}

void SpuMemProxy::assignCOARSE_DELAY_ZOOM( Spu_reg value )
{
	mem_assign_field(COARSE_DELAY_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getCOARSE_DELAY_ZOOM_chn_delay()
{
	return COARSE_DELAY_ZOOM.chn_delay;
}
Spu_reg SpuMemProxy::getCOARSE_DELAY_ZOOM_chn_ce()
{
	return COARSE_DELAY_ZOOM.chn_ce;
}
Spu_reg SpuMemProxy::getCOARSE_DELAY_ZOOM_offset_en()
{
	return COARSE_DELAY_ZOOM.offset_en;
}
Spu_reg SpuMemProxy::getCOARSE_DELAY_ZOOM_delay_en()
{
	return COARSE_DELAY_ZOOM.delay_en;
}

Spu_reg SpuMemProxy::getCOARSE_DELAY_ZOOM()
{
	return COARSE_DELAY_ZOOM.payload;
}


void SpuMemProxy::assignFINE_DELAY_MAIN_chn_delay( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_MAIN,chn_delay,value);
}
void SpuMemProxy::assignFINE_DELAY_MAIN_chn_ce( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_MAIN,chn_ce,value);
}
void SpuMemProxy::assignFINE_DELAY_MAIN_offset_en( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_MAIN,offset_en,value);
}
void SpuMemProxy::assignFINE_DELAY_MAIN_delay_en( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_MAIN,delay_en,value);
}

void SpuMemProxy::assignFINE_DELAY_MAIN( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getFINE_DELAY_MAIN_chn_delay()
{
	return FINE_DELAY_MAIN.chn_delay;
}
Spu_reg SpuMemProxy::getFINE_DELAY_MAIN_chn_ce()
{
	return FINE_DELAY_MAIN.chn_ce;
}
Spu_reg SpuMemProxy::getFINE_DELAY_MAIN_offset_en()
{
	return FINE_DELAY_MAIN.offset_en;
}
Spu_reg SpuMemProxy::getFINE_DELAY_MAIN_delay_en()
{
	return FINE_DELAY_MAIN.delay_en;
}

Spu_reg SpuMemProxy::getFINE_DELAY_MAIN()
{
	return FINE_DELAY_MAIN.payload;
}


void SpuMemProxy::assignFINE_DELAY_ZOOM_chn_delay( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_ZOOM,chn_delay,value);
}
void SpuMemProxy::assignFINE_DELAY_ZOOM_chn_ce( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_ZOOM,chn_ce,value);
}
void SpuMemProxy::assignFINE_DELAY_ZOOM_offset_en( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_ZOOM,offset_en,value);
}
void SpuMemProxy::assignFINE_DELAY_ZOOM_delay_en( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_ZOOM,delay_en,value);
}

void SpuMemProxy::assignFINE_DELAY_ZOOM( Spu_reg value )
{
	mem_assign_field(FINE_DELAY_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getFINE_DELAY_ZOOM_chn_delay()
{
	return FINE_DELAY_ZOOM.chn_delay;
}
Spu_reg SpuMemProxy::getFINE_DELAY_ZOOM_chn_ce()
{
	return FINE_DELAY_ZOOM.chn_ce;
}
Spu_reg SpuMemProxy::getFINE_DELAY_ZOOM_offset_en()
{
	return FINE_DELAY_ZOOM.offset_en;
}
Spu_reg SpuMemProxy::getFINE_DELAY_ZOOM_delay_en()
{
	return FINE_DELAY_ZOOM.delay_en;
}

Spu_reg SpuMemProxy::getFINE_DELAY_ZOOM()
{
	return FINE_DELAY_ZOOM.payload;
}


void SpuMemProxy::assignINTERP_MULT_MAIN_coef_len( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN,coef_len,value);
}
void SpuMemProxy::assignINTERP_MULT_MAIN_comm_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN,comm_mult,value);
}
void SpuMemProxy::assignINTERP_MULT_MAIN_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN,mult,value);
}
void SpuMemProxy::assignINTERP_MULT_MAIN_en( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN,en,value);
}

void SpuMemProxy::assignINTERP_MULT_MAIN( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_coef_len()
{
	return INTERP_MULT_MAIN.coef_len;
}
Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_comm_mult()
{
	return INTERP_MULT_MAIN.comm_mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_mult()
{
	return INTERP_MULT_MAIN.mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_en()
{
	return INTERP_MULT_MAIN.en;
}

Spu_reg SpuMemProxy::getINTERP_MULT_MAIN()
{
	return INTERP_MULT_MAIN.payload;
}


void SpuMemProxy::assignINTERP_MULT_ZOOM_coef_len( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_ZOOM,coef_len,value);
}
void SpuMemProxy::assignINTERP_MULT_ZOOM_comm_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_ZOOM,comm_mult,value);
}
void SpuMemProxy::assignINTERP_MULT_ZOOM_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_ZOOM,mult,value);
}
void SpuMemProxy::assignINTERP_MULT_ZOOM_en( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_ZOOM,en,value);
}

void SpuMemProxy::assignINTERP_MULT_ZOOM( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getINTERP_MULT_ZOOM_coef_len()
{
	return INTERP_MULT_ZOOM.coef_len;
}
Spu_reg SpuMemProxy::getINTERP_MULT_ZOOM_comm_mult()
{
	return INTERP_MULT_ZOOM.comm_mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_ZOOM_mult()
{
	return INTERP_MULT_ZOOM.mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_ZOOM_en()
{
	return INTERP_MULT_ZOOM.en;
}

Spu_reg SpuMemProxy::getINTERP_MULT_ZOOM()
{
	return INTERP_MULT_ZOOM.payload;
}


void SpuMemProxy::assignINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN_FINE,coef_len,value);
}
void SpuMemProxy::assignINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN_FINE,comm_mult,value);
}
void SpuMemProxy::assignINTERP_MULT_MAIN_FINE_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN_FINE,mult,value);
}
void SpuMemProxy::assignINTERP_MULT_MAIN_FINE_en( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN_FINE,en,value);
}

void SpuMemProxy::assignINTERP_MULT_MAIN_FINE( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_MAIN_FINE,payload,value);
}


Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_FINE_coef_len()
{
	return INTERP_MULT_MAIN_FINE.coef_len;
}
Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_FINE_comm_mult()
{
	return INTERP_MULT_MAIN_FINE.comm_mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_FINE_mult()
{
	return INTERP_MULT_MAIN_FINE.mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_FINE_en()
{
	return INTERP_MULT_MAIN_FINE.en;
}

Spu_reg SpuMemProxy::getINTERP_MULT_MAIN_FINE()
{
	return INTERP_MULT_MAIN_FINE.payload;
}


void SpuMemProxy::assignINTERP_MULT_EYE_coef_len( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_EYE,coef_len,value);
}
void SpuMemProxy::assignINTERP_MULT_EYE_comm_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_EYE,comm_mult,value);
}
void SpuMemProxy::assignINTERP_MULT_EYE_mult( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_EYE,mult,value);
}
void SpuMemProxy::assignINTERP_MULT_EYE_en( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_EYE,en,value);
}

void SpuMemProxy::assignINTERP_MULT_EYE( Spu_reg value )
{
	mem_assign_field(INTERP_MULT_EYE,payload,value);
}


Spu_reg SpuMemProxy::getINTERP_MULT_EYE_coef_len()
{
	return INTERP_MULT_EYE.coef_len;
}
Spu_reg SpuMemProxy::getINTERP_MULT_EYE_comm_mult()
{
	return INTERP_MULT_EYE.comm_mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_EYE_mult()
{
	return INTERP_MULT_EYE.mult;
}
Spu_reg SpuMemProxy::getINTERP_MULT_EYE_en()
{
	return INTERP_MULT_EYE.en;
}

Spu_reg SpuMemProxy::getINTERP_MULT_EYE()
{
	return INTERP_MULT_EYE.payload;
}


void SpuMemProxy::assignINTERP_COEF_TAP_tap( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_TAP,tap,value);
}
void SpuMemProxy::assignINTERP_COEF_TAP_ce( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_TAP,ce,value);
}

void SpuMemProxy::assignINTERP_COEF_TAP( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_TAP,payload,value);
}


void SpuMemProxy::assignINTERP_COEF_WR_wdata( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_WR,wdata,value);
}
void SpuMemProxy::assignINTERP_COEF_WR_waddr( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_WR,waddr,value);
}
void SpuMemProxy::assignINTERP_COEF_WR_coef_group( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_WR,coef_group,value);
}
void SpuMemProxy::assignINTERP_COEF_WR_wr_en( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_WR,wr_en,value);
}

void SpuMemProxy::assignINTERP_COEF_WR( Spu_reg value )
{
	mem_assign_field(INTERP_COEF_WR,payload,value);
}


void SpuMemProxy::assignCOMPRESS_MAIN_rate( Spu_reg value )
{
	mem_assign_field(COMPRESS_MAIN,rate,value);
}
void SpuMemProxy::assignCOMPRESS_MAIN_peak( Spu_reg value )
{
	mem_assign_field(COMPRESS_MAIN,peak,value);
}
void SpuMemProxy::assignCOMPRESS_MAIN_en( Spu_reg value )
{
	mem_assign_field(COMPRESS_MAIN,en,value);
}

void SpuMemProxy::assignCOMPRESS_MAIN( Spu_reg value )
{
	mem_assign_field(COMPRESS_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getCOMPRESS_MAIN_rate()
{
	return COMPRESS_MAIN.rate;
}
Spu_reg SpuMemProxy::getCOMPRESS_MAIN_peak()
{
	return COMPRESS_MAIN.peak;
}
Spu_reg SpuMemProxy::getCOMPRESS_MAIN_en()
{
	return COMPRESS_MAIN.en;
}

Spu_reg SpuMemProxy::getCOMPRESS_MAIN()
{
	return COMPRESS_MAIN.payload;
}


void SpuMemProxy::assignCOMPRESS_ZOOM_rate( Spu_reg value )
{
	mem_assign_field(COMPRESS_ZOOM,rate,value);
}
void SpuMemProxy::assignCOMPRESS_ZOOM_peak( Spu_reg value )
{
	mem_assign_field(COMPRESS_ZOOM,peak,value);
}
void SpuMemProxy::assignCOMPRESS_ZOOM_en( Spu_reg value )
{
	mem_assign_field(COMPRESS_ZOOM,en,value);
}

void SpuMemProxy::assignCOMPRESS_ZOOM( Spu_reg value )
{
	mem_assign_field(COMPRESS_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getCOMPRESS_ZOOM_rate()
{
	return COMPRESS_ZOOM.rate;
}
Spu_reg SpuMemProxy::getCOMPRESS_ZOOM_peak()
{
	return COMPRESS_ZOOM.peak;
}
Spu_reg SpuMemProxy::getCOMPRESS_ZOOM_en()
{
	return COMPRESS_ZOOM.en;
}

Spu_reg SpuMemProxy::getCOMPRESS_ZOOM()
{
	return COMPRESS_ZOOM.payload;
}


void SpuMemProxy::assignLA_FINE_DELAY_MAIN_delay( Spu_reg value )
{
	mem_assign_field(LA_FINE_DELAY_MAIN,delay,value);
}
void SpuMemProxy::assignLA_FINE_DELAY_MAIN_en( Spu_reg value )
{
	mem_assign_field(LA_FINE_DELAY_MAIN,en,value);
}

void SpuMemProxy::assignLA_FINE_DELAY_MAIN( Spu_reg value )
{
	mem_assign_field(LA_FINE_DELAY_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getLA_FINE_DELAY_MAIN_delay()
{
	return LA_FINE_DELAY_MAIN.delay;
}
Spu_reg SpuMemProxy::getLA_FINE_DELAY_MAIN_en()
{
	return LA_FINE_DELAY_MAIN.en;
}

Spu_reg SpuMemProxy::getLA_FINE_DELAY_MAIN()
{
	return LA_FINE_DELAY_MAIN.payload;
}


void SpuMemProxy::assignLA_FINE_DELAY_ZOOM_delay( Spu_reg value )
{
	mem_assign_field(LA_FINE_DELAY_ZOOM,delay,value);
}
void SpuMemProxy::assignLA_FINE_DELAY_ZOOM_en( Spu_reg value )
{
	mem_assign_field(LA_FINE_DELAY_ZOOM,en,value);
}

void SpuMemProxy::assignLA_FINE_DELAY_ZOOM( Spu_reg value )
{
	mem_assign_field(LA_FINE_DELAY_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getLA_FINE_DELAY_ZOOM_delay()
{
	return LA_FINE_DELAY_ZOOM.delay;
}
Spu_reg SpuMemProxy::getLA_FINE_DELAY_ZOOM_en()
{
	return LA_FINE_DELAY_ZOOM.en;
}

Spu_reg SpuMemProxy::getLA_FINE_DELAY_ZOOM()
{
	return LA_FINE_DELAY_ZOOM.payload;
}


void SpuMemProxy::assignLA_COMP_MAIN_rate( Spu_reg value )
{
	mem_assign_field(LA_COMP_MAIN,rate,value);
}
void SpuMemProxy::assignLA_COMP_MAIN_en( Spu_reg value )
{
	mem_assign_field(LA_COMP_MAIN,en,value);
}

void SpuMemProxy::assignLA_COMP_MAIN( Spu_reg value )
{
	mem_assign_field(LA_COMP_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getLA_COMP_MAIN_rate()
{
	return LA_COMP_MAIN.rate;
}
Spu_reg SpuMemProxy::getLA_COMP_MAIN_en()
{
	return LA_COMP_MAIN.en;
}

Spu_reg SpuMemProxy::getLA_COMP_MAIN()
{
	return LA_COMP_MAIN.payload;
}


void SpuMemProxy::assignLA_COMP_ZOOM_rate( Spu_reg value )
{
	mem_assign_field(LA_COMP_ZOOM,rate,value);
}
void SpuMemProxy::assignLA_COMP_ZOOM_en( Spu_reg value )
{
	mem_assign_field(LA_COMP_ZOOM,en,value);
}

void SpuMemProxy::assignLA_COMP_ZOOM( Spu_reg value )
{
	mem_assign_field(LA_COMP_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getLA_COMP_ZOOM_rate()
{
	return LA_COMP_ZOOM.rate;
}
Spu_reg SpuMemProxy::getLA_COMP_ZOOM_en()
{
	return LA_COMP_ZOOM.en;
}

Spu_reg SpuMemProxy::getLA_COMP_ZOOM()
{
	return LA_COMP_ZOOM.payload;
}


void SpuMemProxy::assignTX_LEN_CH( Spu_reg value )
{
	mem_assign(TX_LEN_CH,value);
}


Spu_reg SpuMemProxy::getTX_LEN_CH()
{
	return TX_LEN_CH;
}


void SpuMemProxy::assignTX_LEN_LA( Spu_reg value )
{
	mem_assign(TX_LEN_LA,value);
}


Spu_reg SpuMemProxy::getTX_LEN_LA()
{
	return TX_LEN_LA;
}


void SpuMemProxy::assignTX_LEN_ZOOM_CH( Spu_reg value )
{
	mem_assign(TX_LEN_ZOOM_CH,value);
}


Spu_reg SpuMemProxy::getTX_LEN_ZOOM_CH()
{
	return TX_LEN_ZOOM_CH;
}


void SpuMemProxy::assignTX_LEN_ZOOM_LA( Spu_reg value )
{
	mem_assign(TX_LEN_ZOOM_LA,value);
}


Spu_reg SpuMemProxy::getTX_LEN_ZOOM_LA()
{
	return TX_LEN_ZOOM_LA;
}


void SpuMemProxy::assignTX_LEN_TRACE_CH( Spu_reg value )
{
	mem_assign(TX_LEN_TRACE_CH,value);
}


Spu_reg SpuMemProxy::getTX_LEN_TRACE_CH()
{
	return TX_LEN_TRACE_CH;
}


void SpuMemProxy::assignTX_LEN_TRACE_LA( Spu_reg value )
{
	mem_assign(TX_LEN_TRACE_LA,value);
}


Spu_reg SpuMemProxy::getTX_LEN_TRACE_LA()
{
	return TX_LEN_TRACE_LA;
}


void SpuMemProxy::assignTX_LEN_COL_CH( Spu_reg value )
{
	mem_assign(TX_LEN_COL_CH,value);
}


Spu_reg SpuMemProxy::getTX_LEN_COL_CH()
{
	return TX_LEN_COL_CH;
}


void SpuMemProxy::assignTX_LEN_COL_LA( Spu_reg value )
{
	mem_assign(TX_LEN_COL_LA,value);
}


Spu_reg SpuMemProxy::getTX_LEN_COL_LA()
{
	return TX_LEN_COL_LA;
}


void SpuMemProxy::assignDEBUG_ACQ_samp_extract_vld( Spu_reg value )
{
	mem_assign_field(DEBUG_ACQ,samp_extract_vld,value);
}
void SpuMemProxy::assignDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value )
{
	mem_assign_field(DEBUG_ACQ,acq_dly_cfg_en,value);
}
void SpuMemProxy::assignDEBUG_ACQ_acq_dly_cfg( Spu_reg value )
{
	mem_assign_field(DEBUG_ACQ,acq_dly_cfg,value);
}
void SpuMemProxy::assignDEBUG_ACQ_trig_src_type( Spu_reg value )
{
	mem_assign_field(DEBUG_ACQ,trig_src_type,value);
}

void SpuMemProxy::assignDEBUG_ACQ( Spu_reg value )
{
	mem_assign_field(DEBUG_ACQ,payload,value);
}


Spu_reg SpuMemProxy::getDEBUG_ACQ_samp_extract_vld()
{
	return DEBUG_ACQ.samp_extract_vld;
}
Spu_reg SpuMemProxy::getDEBUG_ACQ_acq_dly_cfg_en()
{
	return DEBUG_ACQ.acq_dly_cfg_en;
}
Spu_reg SpuMemProxy::getDEBUG_ACQ_acq_dly_cfg()
{
	return DEBUG_ACQ.acq_dly_cfg;
}
Spu_reg SpuMemProxy::getDEBUG_ACQ_trig_src_type()
{
	return DEBUG_ACQ.trig_src_type;
}

Spu_reg SpuMemProxy::getDEBUG_ACQ()
{
	return DEBUG_ACQ.payload;
}


void SpuMemProxy::assignDEBUG_TX_spu_tx_cnt( Spu_reg value )
{
	mem_assign_field(DEBUG_TX,spu_tx_cnt,value);
}
void SpuMemProxy::assignDEBUG_TX_spu_frm_fsm( Spu_reg value )
{
	mem_assign_field(DEBUG_TX,spu_frm_fsm,value);
}
void SpuMemProxy::assignDEBUG_TX_tx_cnt_clr( Spu_reg value )
{
	mem_assign_field(DEBUG_TX,tx_cnt_clr,value);
}
void SpuMemProxy::assignDEBUG_TX_v_tx_bypass( Spu_reg value )
{
	mem_assign_field(DEBUG_TX,v_tx_bypass,value);
}
void SpuMemProxy::assignDEBUG_TX_frm_head_jmp( Spu_reg value )
{
	mem_assign_field(DEBUG_TX,frm_head_jmp,value);
}

void SpuMemProxy::assignDEBUG_TX( Spu_reg value )
{
	mem_assign_field(DEBUG_TX,payload,value);
}


Spu_reg SpuMemProxy::getDEBUG_TX_spu_tx_cnt()
{
	return DEBUG_TX.spu_tx_cnt;
}
Spu_reg SpuMemProxy::getDEBUG_TX_spu_frm_fsm()
{
	return DEBUG_TX.spu_frm_fsm;
}
Spu_reg SpuMemProxy::getDEBUG_TX_tx_cnt_clr()
{
	return DEBUG_TX.tx_cnt_clr;
}
Spu_reg SpuMemProxy::getDEBUG_TX_v_tx_bypass()
{
	return DEBUG_TX.v_tx_bypass;
}
Spu_reg SpuMemProxy::getDEBUG_TX_frm_head_jmp()
{
	return DEBUG_TX.frm_head_jmp;
}

Spu_reg SpuMemProxy::getDEBUG_TX()
{
	return DEBUG_TX.payload;
}


void SpuMemProxy::assignDEBUG_MEM_trig_tpu_coarse( Spu_reg value )
{
	mem_assign_field(DEBUG_MEM,trig_tpu_coarse,value);
}
void SpuMemProxy::assignDEBUG_MEM_mem_bram_dly( Spu_reg value )
{
	mem_assign_field(DEBUG_MEM,mem_bram_dly,value);
}
void SpuMemProxy::assignDEBUG_MEM_trig_offset_dot_pre( Spu_reg value )
{
	mem_assign_field(DEBUG_MEM,trig_offset_dot_pre,value);
}
void SpuMemProxy::assignDEBUG_MEM_trig_dly_dot_out( Spu_reg value )
{
	mem_assign_field(DEBUG_MEM,trig_dly_dot_out,value);
}
void SpuMemProxy::assignDEBUG_MEM_trig_coarse_fine( Spu_reg value )
{
	mem_assign_field(DEBUG_MEM,trig_coarse_fine,value);
}
void SpuMemProxy::assignDEBUG_MEM_mem_last_dot( Spu_reg value )
{
	mem_assign_field(DEBUG_MEM,mem_last_dot,value);
}

void SpuMemProxy::assignDEBUG_MEM( Spu_reg value )
{
	mem_assign_field(DEBUG_MEM,payload,value);
}


Spu_reg SpuMemProxy::getDEBUG_MEM_trig_tpu_coarse()
{
	return DEBUG_MEM.trig_tpu_coarse;
}
Spu_reg SpuMemProxy::getDEBUG_MEM_mem_bram_dly()
{
	return DEBUG_MEM.mem_bram_dly;
}
Spu_reg SpuMemProxy::getDEBUG_MEM_trig_offset_dot_pre()
{
	return DEBUG_MEM.trig_offset_dot_pre;
}
Spu_reg SpuMemProxy::getDEBUG_MEM_trig_dly_dot_out()
{
	return DEBUG_MEM.trig_dly_dot_out;
}
Spu_reg SpuMemProxy::getDEBUG_MEM_trig_coarse_fine()
{
	return DEBUG_MEM.trig_coarse_fine;
}
Spu_reg SpuMemProxy::getDEBUG_MEM_mem_last_dot()
{
	return DEBUG_MEM.mem_last_dot;
}

Spu_reg SpuMemProxy::getDEBUG_MEM()
{
	return DEBUG_MEM.payload;
}


Spu_reg SpuMemProxy::getDEBUG_SCAN_pos_sa_view_frm_cnt()
{
	return DEBUG_SCAN.pos_sa_view_frm_cnt;
}

Spu_reg SpuMemProxy::getDEBUG_SCAN()
{
	return DEBUG_SCAN.payload;
}


Spu_reg SpuMemProxy::getRECORD_SUM_wav_view_len()
{
	return RECORD_SUM.wav_view_len;
}
Spu_reg SpuMemProxy::getRECORD_SUM_wav_view_full()
{
	return RECORD_SUM.wav_view_full;
}

Spu_reg SpuMemProxy::getRECORD_SUM()
{
	return RECORD_SUM.payload;
}


void SpuMemProxy::assignSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value )
{
	mem_assign_field(SCAN_ROLL_FRM,scan_roll_frm_num,value);
}

void SpuMemProxy::assignSCAN_ROLL_FRM( Spu_reg value )
{
	mem_assign_field(SCAN_ROLL_FRM,payload,value);
}


Spu_reg SpuMemProxy::getSCAN_ROLL_FRM_scan_roll_frm_num()
{
	return SCAN_ROLL_FRM.scan_roll_frm_num;
}

Spu_reg SpuMemProxy::getSCAN_ROLL_FRM()
{
	return SCAN_ROLL_FRM.payload;
}


void SpuMemProxy::assignADC_AVG_rate( Spu_reg value )
{
	mem_assign_field(ADC_AVG,rate,value);
}
void SpuMemProxy::assignADC_AVG_done( Spu_reg value )
{
	mem_assign_field(ADC_AVG,done,value);
}
void SpuMemProxy::assignADC_AVG_en( Spu_reg value )
{
	mem_assign_field(ADC_AVG,en,value);
}

void SpuMemProxy::assignADC_AVG( Spu_reg value )
{
	mem_assign_field(ADC_AVG,payload,value);
}


Spu_reg SpuMemProxy::getADC_AVG_rate()
{
	return ADC_AVG.rate;
}
Spu_reg SpuMemProxy::getADC_AVG_done()
{
	return ADC_AVG.done;
}
Spu_reg SpuMemProxy::getADC_AVG_en()
{
	return ADC_AVG.en;
}

Spu_reg SpuMemProxy::getADC_AVG()
{
	return ADC_AVG.payload;
}


Spu_reg SpuMemProxy::getADC_AVG_RES_AB_B()
{
	return ADC_AVG_RES_AB.B;
}
Spu_reg SpuMemProxy::getADC_AVG_RES_AB_A()
{
	return ADC_AVG_RES_AB.A;
}

Spu_reg SpuMemProxy::getADC_AVG_RES_AB()
{
	return ADC_AVG_RES_AB.payload;
}


Spu_reg SpuMemProxy::getADC_AVG_RES_CD_D()
{
	return ADC_AVG_RES_CD.D;
}
Spu_reg SpuMemProxy::getADC_AVG_RES_CD_C()
{
	return ADC_AVG_RES_CD.C;
}

Spu_reg SpuMemProxy::getADC_AVG_RES_CD()
{
	return ADC_AVG_RES_CD.payload;
}


Spu_reg SpuMemProxy::getADC_AVG_MAX_max_D()
{
	return ADC_AVG_MAX.max_D;
}
Spu_reg SpuMemProxy::getADC_AVG_MAX_max_C()
{
	return ADC_AVG_MAX.max_C;
}
Spu_reg SpuMemProxy::getADC_AVG_MAX_max_B()
{
	return ADC_AVG_MAX.max_B;
}
Spu_reg SpuMemProxy::getADC_AVG_MAX_max_A()
{
	return ADC_AVG_MAX.max_A;
}

Spu_reg SpuMemProxy::getADC_AVG_MAX()
{
	return ADC_AVG_MAX.payload;
}


Spu_reg SpuMemProxy::getADC_AVG_MIN_min_D()
{
	return ADC_AVG_MIN.min_D;
}
Spu_reg SpuMemProxy::getADC_AVG_MIN_min_C()
{
	return ADC_AVG_MIN.min_C;
}
Spu_reg SpuMemProxy::getADC_AVG_MIN_min_B()
{
	return ADC_AVG_MIN.min_B;
}
Spu_reg SpuMemProxy::getADC_AVG_MIN_min_A()
{
	return ADC_AVG_MIN.min_A;
}

Spu_reg SpuMemProxy::getADC_AVG_MIN()
{
	return ADC_AVG_MIN.payload;
}


Spu_reg SpuMemProxy::getLA_SA_RES_la()
{
	return LA_SA_RES.la;
}

Spu_reg SpuMemProxy::getLA_SA_RES()
{
	return LA_SA_RES.payload;
}


void SpuMemProxy::assignZONE_TRIG_AUX_pulse_width( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_AUX,pulse_width,value);
}
void SpuMemProxy::assignZONE_TRIG_AUX_inv( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_AUX,inv,value);
}
void SpuMemProxy::assignZONE_TRIG_AUX_pass_fail( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_AUX,pass_fail,value);
}

void SpuMemProxy::assignZONE_TRIG_AUX( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_AUX,payload,value);
}


Spu_reg SpuMemProxy::getZONE_TRIG_AUX_pulse_width()
{
	return ZONE_TRIG_AUX.pulse_width;
}
Spu_reg SpuMemProxy::getZONE_TRIG_AUX_inv()
{
	return ZONE_TRIG_AUX.inv;
}
Spu_reg SpuMemProxy::getZONE_TRIG_AUX_pass_fail()
{
	return ZONE_TRIG_AUX.pass_fail;
}

Spu_reg SpuMemProxy::getZONE_TRIG_AUX()
{
	return ZONE_TRIG_AUX.payload;
}


void SpuMemProxy::assignZONE_TRIG_TAB1_threshold_l( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB1,threshold_l,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB1_threshold_h( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB1,threshold_h,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB1_threshold_en( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB1,threshold_en,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB1_column_addr( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB1,column_addr,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB1_tab_index( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB1,tab_index,value);
}

void SpuMemProxy::assignZONE_TRIG_TAB1( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB1,payload,value);
}


void SpuMemProxy::assignZONE_TRIG_TAB2_threshold_l( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB2,threshold_l,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB2_threshold_h( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB2,threshold_h,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB2_threshold_en( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB2,threshold_en,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB2_column_addr( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB2,column_addr,value);
}
void SpuMemProxy::assignZONE_TRIG_TAB2_tab_index( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB2,tab_index,value);
}

void SpuMemProxy::assignZONE_TRIG_TAB2( Spu_reg value )
{
	mem_assign_field(ZONE_TRIG_TAB2,payload,value);
}


void SpuMemProxy::assignMASK_TAB_threshold_l( Spu_reg value )
{
	mem_assign_field(MASK_TAB,threshold_l,value);
}
void SpuMemProxy::assignMASK_TAB_threshold_l_less_en( Spu_reg value )
{
	mem_assign_field(MASK_TAB,threshold_l_less_en,value);
}
void SpuMemProxy::assignMASK_TAB_threshold_h( Spu_reg value )
{
	mem_assign_field(MASK_TAB,threshold_h,value);
}
void SpuMemProxy::assignMASK_TAB_threshold_h_greater_en( Spu_reg value )
{
	mem_assign_field(MASK_TAB,threshold_h_greater_en,value);
}
void SpuMemProxy::assignMASK_TAB_column_addr( Spu_reg value )
{
	mem_assign_field(MASK_TAB,column_addr,value);
}
void SpuMemProxy::assignMASK_TAB_tab_index( Spu_reg value )
{
	mem_assign_field(MASK_TAB,tab_index,value);
}

void SpuMemProxy::assignMASK_TAB( Spu_reg value )
{
	mem_assign_field(MASK_TAB,payload,value);
}


Spu_reg SpuMemProxy::getMASK_TAB_threshold_l()
{
	return MASK_TAB.threshold_l;
}
Spu_reg SpuMemProxy::getMASK_TAB_threshold_l_less_en()
{
	return MASK_TAB.threshold_l_less_en;
}
Spu_reg SpuMemProxy::getMASK_TAB_threshold_h()
{
	return MASK_TAB.threshold_h;
}
Spu_reg SpuMemProxy::getMASK_TAB_threshold_h_greater_en()
{
	return MASK_TAB.threshold_h_greater_en;
}
Spu_reg SpuMemProxy::getMASK_TAB_column_addr()
{
	return MASK_TAB.column_addr;
}
Spu_reg SpuMemProxy::getMASK_TAB_tab_index()
{
	return MASK_TAB.tab_index;
}

Spu_reg SpuMemProxy::getMASK_TAB()
{
	return MASK_TAB.payload;
}


void SpuMemProxy::assignMASK_TAB_EX_threshold_l( Spu_reg value )
{
	mem_assign_field(MASK_TAB_EX,threshold_l,value);
}
void SpuMemProxy::assignMASK_TAB_EX_threshold_l_greater_en( Spu_reg value )
{
	mem_assign_field(MASK_TAB_EX,threshold_l_greater_en,value);
}
void SpuMemProxy::assignMASK_TAB_EX_threshold_h( Spu_reg value )
{
	mem_assign_field(MASK_TAB_EX,threshold_h,value);
}
void SpuMemProxy::assignMASK_TAB_EX_threshold_h_less_en( Spu_reg value )
{
	mem_assign_field(MASK_TAB_EX,threshold_h_less_en,value);
}
void SpuMemProxy::assignMASK_TAB_EX_column_addr( Spu_reg value )
{
	mem_assign_field(MASK_TAB_EX,column_addr,value);
}
void SpuMemProxy::assignMASK_TAB_EX_tab_index( Spu_reg value )
{
	mem_assign_field(MASK_TAB_EX,tab_index,value);
}

void SpuMemProxy::assignMASK_TAB_EX( Spu_reg value )
{
	mem_assign_field(MASK_TAB_EX,payload,value);
}


Spu_reg SpuMemProxy::getMASK_TAB_EX_threshold_l()
{
	return MASK_TAB_EX.threshold_l;
}
Spu_reg SpuMemProxy::getMASK_TAB_EX_threshold_l_greater_en()
{
	return MASK_TAB_EX.threshold_l_greater_en;
}
Spu_reg SpuMemProxy::getMASK_TAB_EX_threshold_h()
{
	return MASK_TAB_EX.threshold_h;
}
Spu_reg SpuMemProxy::getMASK_TAB_EX_threshold_h_less_en()
{
	return MASK_TAB_EX.threshold_h_less_en;
}
Spu_reg SpuMemProxy::getMASK_TAB_EX_column_addr()
{
	return MASK_TAB_EX.column_addr;
}
Spu_reg SpuMemProxy::getMASK_TAB_EX_tab_index()
{
	return MASK_TAB_EX.tab_index;
}

Spu_reg SpuMemProxy::getMASK_TAB_EX()
{
	return MASK_TAB_EX.payload;
}


void SpuMemProxy::assignMASK_COMPRESS_rate( Spu_reg value )
{
	mem_assign_field(MASK_COMPRESS,rate,value);
}
void SpuMemProxy::assignMASK_COMPRESS_peak( Spu_reg value )
{
	mem_assign_field(MASK_COMPRESS,peak,value);
}
void SpuMemProxy::assignMASK_COMPRESS_en( Spu_reg value )
{
	mem_assign_field(MASK_COMPRESS,en,value);
}

void SpuMemProxy::assignMASK_COMPRESS( Spu_reg value )
{
	mem_assign_field(MASK_COMPRESS,payload,value);
}


Spu_reg SpuMemProxy::getMASK_COMPRESS_rate()
{
	return MASK_COMPRESS.rate;
}
Spu_reg SpuMemProxy::getMASK_COMPRESS_peak()
{
	return MASK_COMPRESS.peak;
}
Spu_reg SpuMemProxy::getMASK_COMPRESS_en()
{
	return MASK_COMPRESS.en;
}

Spu_reg SpuMemProxy::getMASK_COMPRESS()
{
	return MASK_COMPRESS.payload;
}


void SpuMemProxy::assignMASK_CTRL_cross_index( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,cross_index,value);
}
void SpuMemProxy::assignMASK_CTRL_mask_index_en( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,mask_index_en,value);
}
void SpuMemProxy::assignMASK_CTRL_zone1_trig_tab_en( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,zone1_trig_tab_en,value);
}
void SpuMemProxy::assignMASK_CTRL_zone2_trig_tab_en( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,zone2_trig_tab_en,value);
}
void SpuMemProxy::assignMASK_CTRL_zone1_trig_cross( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,zone1_trig_cross,value);
}
void SpuMemProxy::assignMASK_CTRL_zone2_trig_cross( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,zone2_trig_cross,value);
}
void SpuMemProxy::assignMASK_CTRL_mask_result_rd( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,mask_result_rd,value);
}
void SpuMemProxy::assignMASK_CTRL_sum_clr( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,sum_clr,value);
}
void SpuMemProxy::assignMASK_CTRL_cross_clr( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,cross_clr,value);
}
void SpuMemProxy::assignMASK_CTRL_mask_we( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,mask_we,value);
}

void SpuMemProxy::assignMASK_CTRL( Spu_reg value )
{
	mem_assign_field(MASK_CTRL,payload,value);
}


Spu_reg SpuMemProxy::getMASK_CTRL_cross_index()
{
	return MASK_CTRL.cross_index;
}
Spu_reg SpuMemProxy::getMASK_CTRL_mask_index_en()
{
	return MASK_CTRL.mask_index_en;
}
Spu_reg SpuMemProxy::getMASK_CTRL_zone1_trig_tab_en()
{
	return MASK_CTRL.zone1_trig_tab_en;
}
Spu_reg SpuMemProxy::getMASK_CTRL_zone2_trig_tab_en()
{
	return MASK_CTRL.zone2_trig_tab_en;
}
Spu_reg SpuMemProxy::getMASK_CTRL_zone1_trig_cross()
{
	return MASK_CTRL.zone1_trig_cross;
}
Spu_reg SpuMemProxy::getMASK_CTRL_zone2_trig_cross()
{
	return MASK_CTRL.zone2_trig_cross;
}
Spu_reg SpuMemProxy::getMASK_CTRL_mask_result_rd()
{
	return MASK_CTRL.mask_result_rd;
}
Spu_reg SpuMemProxy::getMASK_CTRL_sum_clr()
{
	return MASK_CTRL.sum_clr;
}
Spu_reg SpuMemProxy::getMASK_CTRL_cross_clr()
{
	return MASK_CTRL.cross_clr;
}
Spu_reg SpuMemProxy::getMASK_CTRL_mask_we()
{
	return MASK_CTRL.mask_we;
}

Spu_reg SpuMemProxy::getMASK_CTRL()
{
	return MASK_CTRL.payload;
}


Spu_reg SpuMemProxy::getMASK_FRM_CNT_L()
{
	return MASK_FRM_CNT_L;
}


Spu_reg SpuMemProxy::getMASK_FRM_CNT_H()
{
	return MASK_FRM_CNT_H;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_L_CHA()
{
	return MASK_CROSS_CNT_L_CHA;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_H_CHA()
{
	return MASK_CROSS_CNT_H_CHA;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_L_CHB()
{
	return MASK_CROSS_CNT_L_CHB;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_H_CHB()
{
	return MASK_CROSS_CNT_H_CHB;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_L_CHC()
{
	return MASK_CROSS_CNT_L_CHC;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_H_CHC()
{
	return MASK_CROSS_CNT_H_CHC;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_L_CHD()
{
	return MASK_CROSS_CNT_L_CHD;
}


Spu_reg SpuMemProxy::getMASK_CROSS_CNT_H_CHD()
{
	return MASK_CROSS_CNT_H_CHD;
}


void SpuMemProxy::assignMASK_OUTPUT_pulse_width( Spu_reg value )
{
	mem_assign_field(MASK_OUTPUT,pulse_width,value);
}
void SpuMemProxy::assignMASK_OUTPUT_inv( Spu_reg value )
{
	mem_assign_field(MASK_OUTPUT,inv,value);
}
void SpuMemProxy::assignMASK_OUTPUT_pass_fail( Spu_reg value )
{
	mem_assign_field(MASK_OUTPUT,pass_fail,value);
}

void SpuMemProxy::assignMASK_OUTPUT( Spu_reg value )
{
	mem_assign_field(MASK_OUTPUT,payload,value);
}


Spu_reg SpuMemProxy::getMASK_OUTPUT_pulse_width()
{
	return MASK_OUTPUT.pulse_width;
}
Spu_reg SpuMemProxy::getMASK_OUTPUT_inv()
{
	return MASK_OUTPUT.inv;
}
Spu_reg SpuMemProxy::getMASK_OUTPUT_pass_fail()
{
	return MASK_OUTPUT.pass_fail;
}

Spu_reg SpuMemProxy::getMASK_OUTPUT()
{
	return MASK_OUTPUT.payload;
}


void SpuMemProxy::assignSEARCH_OFFSET_CH( Spu_reg value )
{
	mem_assign(SEARCH_OFFSET_CH,value);
}


Spu_reg SpuMemProxy::getSEARCH_OFFSET_CH()
{
	return SEARCH_OFFSET_CH;
}


void SpuMemProxy::assignSEARCH_LEN_CH( Spu_reg value )
{
	mem_assign(SEARCH_LEN_CH,value);
}


Spu_reg SpuMemProxy::getSEARCH_LEN_CH()
{
	return SEARCH_LEN_CH;
}


void SpuMemProxy::assignSEARCH_OFFSET_LA( Spu_reg value )
{
	mem_assign(SEARCH_OFFSET_LA,value);
}


Spu_reg SpuMemProxy::getSEARCH_OFFSET_LA()
{
	return SEARCH_OFFSET_LA;
}


void SpuMemProxy::assignSEARCH_LEN_LA( Spu_reg value )
{
	mem_assign(SEARCH_LEN_LA,value);
}


Spu_reg SpuMemProxy::getSEARCH_LEN_LA()
{
	return SEARCH_LEN_LA;
}


void SpuMemProxy::assignTIME_TAG_L( Spu_reg value )
{
	mem_assign(TIME_TAG_L,value);
}


Spu_reg SpuMemProxy::getTIME_TAG_L()
{
	return TIME_TAG_L;
}


void SpuMemProxy::assignTIME_TAG_H( Spu_reg value )
{
	mem_assign(TIME_TAG_H,value);
}


Spu_reg SpuMemProxy::getTIME_TAG_H()
{
	return TIME_TAG_H;
}


void SpuMemProxy::assignFINE_TRIG_POSITION( Spu_reg value )
{
	mem_assign(FINE_TRIG_POSITION,value);
}


Spu_reg SpuMemProxy::getFINE_TRIG_POSITION()
{
	return FINE_TRIG_POSITION;
}


void SpuMemProxy::assignTIME_TAG_CTRL_rd_en( Spu_reg value )
{
	mem_assign_field(TIME_TAG_CTRL,rd_en,value);
}
void SpuMemProxy::assignTIME_TAG_CTRL_rec_first_done( Spu_reg value )
{
	mem_assign_field(TIME_TAG_CTRL,rec_first_done,value);
}

void SpuMemProxy::assignTIME_TAG_CTRL( Spu_reg value )
{
	mem_assign_field(TIME_TAG_CTRL,payload,value);
}


Spu_reg SpuMemProxy::getTIME_TAG_CTRL_rd_en()
{
	return TIME_TAG_CTRL.rd_en;
}
Spu_reg SpuMemProxy::getTIME_TAG_CTRL_rec_first_done()
{
	return TIME_TAG_CTRL.rec_first_done;
}

Spu_reg SpuMemProxy::getTIME_TAG_CTRL()
{
	return TIME_TAG_CTRL.payload;
}


void SpuMemProxy::assignIMPORT_INFO_import_done( Spu_reg value )
{
	mem_assign_field(IMPORT_INFO,import_done,value);
}

void SpuMemProxy::assignIMPORT_INFO( Spu_reg value )
{
	mem_assign_field(IMPORT_INFO,payload,value);
}


Spu_reg SpuMemProxy::getIMPORT_INFO_import_done()
{
	return IMPORT_INFO.import_done;
}

Spu_reg SpuMemProxy::getIMPORT_INFO()
{
	return IMPORT_INFO.payload;
}


Spu_reg SpuMemProxy::getTIME_TAG_L_REC_FIRST()
{
	return TIME_TAG_L_REC_FIRST;
}


Spu_reg SpuMemProxy::getTIME_TAG_H_REC_FIRST()
{
	return TIME_TAG_H_REC_FIRST;
}


void SpuMemProxy::assignTRACE_AVG_RATE_X( Spu_reg value )
{
	mem_assign(TRACE_AVG_RATE_X,value);
}


Spu_reg SpuMemProxy::getTRACE_AVG_RATE_X()
{
	return TRACE_AVG_RATE_X;
}


void SpuMemProxy::assignTRACE_AVG_RATE_Y( Spu_reg value )
{
	mem_assign(TRACE_AVG_RATE_Y,value);
}


Spu_reg SpuMemProxy::getTRACE_AVG_RATE_Y()
{
	return TRACE_AVG_RATE_Y;
}


void SpuMemProxy::assignLINEAR_INTX_CTRL_mian_bypass( Spu_reg value )
{
	mem_assign_field(LINEAR_INTX_CTRL,mian_bypass,value);
}
void SpuMemProxy::assignLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value )
{
	mem_assign_field(LINEAR_INTX_CTRL,zoom_bypass,value);
}

void SpuMemProxy::assignLINEAR_INTX_CTRL( Spu_reg value )
{
	mem_assign_field(LINEAR_INTX_CTRL,payload,value);
}


Spu_reg SpuMemProxy::getLINEAR_INTX_CTRL_mian_bypass()
{
	return LINEAR_INTX_CTRL.mian_bypass;
}
Spu_reg SpuMemProxy::getLINEAR_INTX_CTRL_zoom_bypass()
{
	return LINEAR_INTX_CTRL.zoom_bypass;
}

Spu_reg SpuMemProxy::getLINEAR_INTX_CTRL()
{
	return LINEAR_INTX_CTRL.payload;
}


void SpuMemProxy::assignLINEAR_INTX_INIT( Spu_reg value )
{
	mem_assign(LINEAR_INTX_INIT,value);
}


Spu_reg SpuMemProxy::getLINEAR_INTX_INIT()
{
	return LINEAR_INTX_INIT;
}


void SpuMemProxy::assignLINEAR_INTX_STEP( Spu_reg value )
{
	mem_assign(LINEAR_INTX_STEP,value);
}


Spu_reg SpuMemProxy::getLINEAR_INTX_STEP()
{
	return LINEAR_INTX_STEP;
}


void SpuMemProxy::assignLINEAR_INTX_INIT_ZOOM( Spu_reg value )
{
	mem_assign(LINEAR_INTX_INIT_ZOOM,value);
}


Spu_reg SpuMemProxy::getLINEAR_INTX_INIT_ZOOM()
{
	return LINEAR_INTX_INIT_ZOOM;
}


void SpuMemProxy::assignLINEAR_INTX_STEP_ZOOM( Spu_reg value )
{
	mem_assign(LINEAR_INTX_STEP_ZOOM,value);
}


Spu_reg SpuMemProxy::getLINEAR_INTX_STEP_ZOOM()
{
	return LINEAR_INTX_STEP_ZOOM;
}


void SpuMemProxy::assignWAVE_OFFSET_EYE( Spu_reg value )
{
	mem_assign(WAVE_OFFSET_EYE,value);
}


Spu_reg SpuMemProxy::getWAVE_OFFSET_EYE()
{
	return WAVE_OFFSET_EYE;
}


void SpuMemProxy::assignWAVE_LEN_EYE( Spu_reg value )
{
	mem_assign(WAVE_LEN_EYE,value);
}


Spu_reg SpuMemProxy::getWAVE_LEN_EYE()
{
	return WAVE_LEN_EYE;
}


void SpuMemProxy::assignCOMPRESS_EYE_rate( Spu_reg value )
{
	mem_assign_field(COMPRESS_EYE,rate,value);
}
void SpuMemProxy::assignCOMPRESS_EYE_peak( Spu_reg value )
{
	mem_assign_field(COMPRESS_EYE,peak,value);
}
void SpuMemProxy::assignCOMPRESS_EYE_en( Spu_reg value )
{
	mem_assign_field(COMPRESS_EYE,en,value);
}

void SpuMemProxy::assignCOMPRESS_EYE( Spu_reg value )
{
	mem_assign_field(COMPRESS_EYE,payload,value);
}


Spu_reg SpuMemProxy::getCOMPRESS_EYE_rate()
{
	return COMPRESS_EYE.rate;
}
Spu_reg SpuMemProxy::getCOMPRESS_EYE_peak()
{
	return COMPRESS_EYE.peak;
}
Spu_reg SpuMemProxy::getCOMPRESS_EYE_en()
{
	return COMPRESS_EYE.en;
}

Spu_reg SpuMemProxy::getCOMPRESS_EYE()
{
	return COMPRESS_EYE.payload;
}


void SpuMemProxy::assignTX_LEN_EYE( Spu_reg value )
{
	mem_assign(TX_LEN_EYE,value);
}


Spu_reg SpuMemProxy::getTX_LEN_EYE()
{
	return TX_LEN_EYE;
}


void SpuMemProxy::assignLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value )
{
	mem_assign_field(LA_LINEAR_INTX_CTRL,mian_bypass,value);
}
void SpuMemProxy::assignLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value )
{
	mem_assign_field(LA_LINEAR_INTX_CTRL,zoom_bypass,value);
}

void SpuMemProxy::assignLA_LINEAR_INTX_CTRL( Spu_reg value )
{
	mem_assign_field(LA_LINEAR_INTX_CTRL,payload,value);
}


Spu_reg SpuMemProxy::getLA_LINEAR_INTX_CTRL_mian_bypass()
{
	return LA_LINEAR_INTX_CTRL.mian_bypass;
}
Spu_reg SpuMemProxy::getLA_LINEAR_INTX_CTRL_zoom_bypass()
{
	return LA_LINEAR_INTX_CTRL.zoom_bypass;
}

Spu_reg SpuMemProxy::getLA_LINEAR_INTX_CTRL()
{
	return LA_LINEAR_INTX_CTRL.payload;
}


void SpuMemProxy::assignLA_LINEAR_INTX_INIT( Spu_reg value )
{
	mem_assign(LA_LINEAR_INTX_INIT,value);
}


Spu_reg SpuMemProxy::getLA_LINEAR_INTX_INIT()
{
	return LA_LINEAR_INTX_INIT;
}


void SpuMemProxy::assignLA_LINEAR_INTX_STEP( Spu_reg value )
{
	mem_assign(LA_LINEAR_INTX_STEP,value);
}


Spu_reg SpuMemProxy::getLA_LINEAR_INTX_STEP()
{
	return LA_LINEAR_INTX_STEP;
}


void SpuMemProxy::assignLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value )
{
	mem_assign(LA_LINEAR_INTX_INIT_ZOOM,value);
}


Spu_reg SpuMemProxy::getLA_LINEAR_INTX_INIT_ZOOM()
{
	return LA_LINEAR_INTX_INIT_ZOOM;
}


void SpuMemProxy::assignLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value )
{
	mem_assign(LA_LINEAR_INTX_STEP_ZOOM,value);
}


Spu_reg SpuMemProxy::getLA_LINEAR_INTX_STEP_ZOOM()
{
	return LA_LINEAR_INTX_STEP_ZOOM;
}


void SpuMemProxy::assignTRACE_COMPRESS_MAIN_rate( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_MAIN,rate,value);
}
void SpuMemProxy::assignTRACE_COMPRESS_MAIN_peak( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_MAIN,peak,value);
}
void SpuMemProxy::assignTRACE_COMPRESS_MAIN_en( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_MAIN,en,value);
}

void SpuMemProxy::assignTRACE_COMPRESS_MAIN( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getTRACE_COMPRESS_MAIN_rate()
{
	return TRACE_COMPRESS_MAIN.rate;
}
Spu_reg SpuMemProxy::getTRACE_COMPRESS_MAIN_peak()
{
	return TRACE_COMPRESS_MAIN.peak;
}
Spu_reg SpuMemProxy::getTRACE_COMPRESS_MAIN_en()
{
	return TRACE_COMPRESS_MAIN.en;
}

Spu_reg SpuMemProxy::getTRACE_COMPRESS_MAIN()
{
	return TRACE_COMPRESS_MAIN.payload;
}


void SpuMemProxy::assignTRACE_COMPRESS_ZOOM_rate( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_ZOOM,rate,value);
}
void SpuMemProxy::assignTRACE_COMPRESS_ZOOM_peak( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_ZOOM,peak,value);
}
void SpuMemProxy::assignTRACE_COMPRESS_ZOOM_en( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_ZOOM,en,value);
}

void SpuMemProxy::assignTRACE_COMPRESS_ZOOM( Spu_reg value )
{
	mem_assign_field(TRACE_COMPRESS_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getTRACE_COMPRESS_ZOOM_rate()
{
	return TRACE_COMPRESS_ZOOM.rate;
}
Spu_reg SpuMemProxy::getTRACE_COMPRESS_ZOOM_peak()
{
	return TRACE_COMPRESS_ZOOM.peak;
}
Spu_reg SpuMemProxy::getTRACE_COMPRESS_ZOOM_en()
{
	return TRACE_COMPRESS_ZOOM.en;
}

Spu_reg SpuMemProxy::getTRACE_COMPRESS_ZOOM()
{
	return TRACE_COMPRESS_ZOOM.payload;
}


void SpuMemProxy::assignZYNQ_COMPRESS_MAIN_rate( Spu_reg value )
{
	mem_assign_field(ZYNQ_COMPRESS_MAIN,rate,value);
}

void SpuMemProxy::assignZYNQ_COMPRESS_MAIN( Spu_reg value )
{
	mem_assign_field(ZYNQ_COMPRESS_MAIN,payload,value);
}


Spu_reg SpuMemProxy::getZYNQ_COMPRESS_MAIN_rate()
{
	return ZYNQ_COMPRESS_MAIN.rate;
}

Spu_reg SpuMemProxy::getZYNQ_COMPRESS_MAIN()
{
	return ZYNQ_COMPRESS_MAIN.payload;
}


void SpuMemProxy::assignZYNQ_COMPRESS_ZOOM_rate( Spu_reg value )
{
	mem_assign_field(ZYNQ_COMPRESS_ZOOM,rate,value);
}

void SpuMemProxy::assignZYNQ_COMPRESS_ZOOM( Spu_reg value )
{
	mem_assign_field(ZYNQ_COMPRESS_ZOOM,payload,value);
}


Spu_reg SpuMemProxy::getZYNQ_COMPRESS_ZOOM_rate()
{
	return ZYNQ_COMPRESS_ZOOM.rate;
}

Spu_reg SpuMemProxy::getZYNQ_COMPRESS_ZOOM()
{
	return ZYNQ_COMPRESS_ZOOM.payload;
}


Spu_reg SpuMemProxy::getDEBUG_TX_LA_spu_tx_cnt()
{
	return DEBUG_TX_LA.spu_tx_cnt;
}

Spu_reg SpuMemProxy::getDEBUG_TX_LA()
{
	return DEBUG_TX_LA.payload;
}


Spu_reg SpuMemProxy::getDEBUG_TX_FRM_spu_tx_frm_cnt()
{
	return DEBUG_TX_FRM.spu_tx_frm_cnt;
}

Spu_reg SpuMemProxy::getDEBUG_TX_FRM()
{
	return DEBUG_TX_FRM.payload;
}


Spu_reg SpuMemProxy::getIMPORT_CNT_CH_cnt()
{
	return IMPORT_CNT_CH.cnt;
}
Spu_reg SpuMemProxy::getIMPORT_CNT_CH_cnt_clr()
{
	return IMPORT_CNT_CH.cnt_clr;
}

Spu_reg SpuMemProxy::getIMPORT_CNT_CH()
{
	return IMPORT_CNT_CH.payload;
}


Spu_reg SpuMemProxy::getIMPORT_CNT_LA_cnt()
{
	return IMPORT_CNT_LA.cnt;
}
Spu_reg SpuMemProxy::getIMPORT_CNT_LA_cnt_clr()
{
	return IMPORT_CNT_LA.cnt_clr;
}

Spu_reg SpuMemProxy::getIMPORT_CNT_LA()
{
	return IMPORT_CNT_LA.payload;
}


Spu_reg SpuMemProxy::getDEBUG_LINEAR_INTX1()
{
	return DEBUG_LINEAR_INTX1;
}


Spu_reg SpuMemProxy::getDEBUG_LINEAR_INTX2()
{
	return DEBUG_LINEAR_INTX2;
}


Spu_reg SpuMemProxy::getDEBUG_DDR()
{
	return DEBUG_DDR;
}


Spu_reg SpuMemProxy::getDEBUG_TX_LEN_BURST_tx_length_burst()
{
	return DEBUG_TX_LEN_BURST.tx_length_burst;
}

Spu_reg SpuMemProxy::getDEBUG_TX_LEN_BURST()
{
	return DEBUG_TX_LEN_BURST.payload;
}



void SpuMem::_loadOtp()
{
	VERSION=0x0;
	TEST=0x0;
	CTRL.payload=0x0;
		CTRL.spu_clr=0x0;
		CTRL.adc_sync=0x0;
		CTRL.adc_align_check=0x2;
		CTRL.non_inter=0x0;

		CTRL.peak=0x0;
		CTRL.hi_res=0x0;
		CTRL.anti_alias=0x0;
		CTRL.trace_avg=0x0;

		CTRL.trace_avg_mem=0x1;
		CTRL.adc_data_inv=0xf;
		CTRL.la_probe=0x0;
		CTRL.coarse_trig_range=0x0;

	DEBUG.payload=0x0;
		DEBUG.gray_bypass=0x0;
		DEBUG.ms_order=0x0;
		DEBUG.s_view=0x0;
		DEBUG.bram_ddr_n=0x0;

		DEBUG.ddr_init=0x0;
		DEBUG.ddr_err=0x0;
		DEBUG.dbg_trig_src=0x0;
		DEBUG.ddr_fsm_state=0x0;

		DEBUG.debug_mem=0x0;


	ADC_DATA_IDELAY.payload=0x0;
		ADC_DATA_IDELAY.idelay_var=0x7;
		ADC_DATA_IDELAY.chn_bit=0xff;
		ADC_DATA_IDELAY.h=0x1;
		ADC_DATA_IDELAY.l=0x1;

		ADC_DATA_IDELAY.core_ABCD=0xf;

	ADC_DATA_CHECK.payload=0x0;
		ADC_DATA_CHECK.a_l=0x0;
		ADC_DATA_CHECK.a_h=0x0;
		ADC_DATA_CHECK.b_l=0x0;
		ADC_DATA_CHECK.b_h=0x0;

		ADC_DATA_CHECK.c_l=0x0;
		ADC_DATA_CHECK.c_h=0x0;
		ADC_DATA_CHECK.d_l=0x0;
		ADC_DATA_CHECK.d_h=0x0;

	ADC_CHN_ALIGN_CHK.payload=0x0;
		ADC_CHN_ALIGN_CHK.adc_d=0x0;
		ADC_CHN_ALIGN_CHK.adc_c=0x0;
		ADC_CHN_ALIGN_CHK.adc_b=0x0;
		ADC_CHN_ALIGN_CHK.adc_a=0x0;

	ADC_CHN_ALIGN.payload=0x0;
		ADC_CHN_ALIGN.adc_d=0x0;
		ADC_CHN_ALIGN.adc_c=0x0;
		ADC_CHN_ALIGN.adc_b=0x0;
		ADC_CHN_ALIGN.adc_a=0x0;


	PRE_PROC.payload=0x0;
		PRE_PROC.proc_en=0x0;
		PRE_PROC.cfg_filter_en=0x0;

	FILTER.payload=0x0;
		FILTER.coeff=0x0;
		FILTER.coeff_id=0x0;
		FILTER.coeff_sel=0x0;
		FILTER.coeff_ch=0x0;

	DOWN_SAMP_FAST.payload=0x0;
		DOWN_SAMP_FAST.down_samp_fast=0x1;
		DOWN_SAMP_FAST.en=0x1;

	DOWN_SAMP_SLOW_H.payload=0x0;
		DOWN_SAMP_SLOW_H.down_samp_slow_h=0x0;


	DOWN_SAMP_SLOW_L=0x2;
	LA_DELAY=0x0;
	CHN_DELAY_AB.payload=0x0;
		CHN_DELAY_AB.B=0x0;
		CHN_DELAY_AB.A=0x0;

	CHN_DELAY_CD.payload=0x0;
		CHN_DELAY_CD.D=0x0;
		CHN_DELAY_CD.C=0x0;


	RANDOM_RANGE=0xf;
	EXT_TRIG_IDEALY.payload=0x0;
		EXT_TRIG_IDEALY.idelay_var=0x0;
		EXT_TRIG_IDEALY.ld=0x0;

	SEGMENT_SIZE=0x20000000;
	RECORD_LEN=0x3e8;

	WAVE_LEN_MAIN=0x3e8;
	WAVE_LEN_ZOOM=0x3e8;
	WAVE_OFFSET_MAIN=0x0;
	WAVE_OFFSET_ZOOM=0x0;

	MEAS_OFFSET_CH=0x0;
	MEAS_LEN_CH=0x3e8;
	MEAS_OFFSET_LA=0x0;
	MEAS_LEN_LA=0x3e8;

	COARSE_DELAY_MAIN.payload=0x0;
		COARSE_DELAY_MAIN.chn_delay=0x0;
		COARSE_DELAY_MAIN.chn_ce=0xf;
		COARSE_DELAY_MAIN.offset_en=0x1;
		COARSE_DELAY_MAIN.delay_en=0x1;

	COARSE_DELAY_ZOOM.payload=0x0;
		COARSE_DELAY_ZOOM.chn_delay=0x0;
		COARSE_DELAY_ZOOM.chn_ce=0xf;
		COARSE_DELAY_ZOOM.offset_en=0x1;
		COARSE_DELAY_ZOOM.delay_en=0x1;

	FINE_DELAY_MAIN.payload=0x0;
		FINE_DELAY_MAIN.chn_delay=0x0;
		FINE_DELAY_MAIN.chn_ce=0xf;
		FINE_DELAY_MAIN.offset_en=0x1;
		FINE_DELAY_MAIN.delay_en=0x1;

	FINE_DELAY_ZOOM.payload=0x0;
		FINE_DELAY_ZOOM.chn_delay=0x0;
		FINE_DELAY_ZOOM.chn_ce=0xf;
		FINE_DELAY_ZOOM.offset_en=0x1;
		FINE_DELAY_ZOOM.delay_en=0x1;


	INTERP_MULT_MAIN.payload=0x0;
		INTERP_MULT_MAIN.coef_len=0x4;
		INTERP_MULT_MAIN.comm_mult=0x4;
		INTERP_MULT_MAIN.mult=0x4;
		INTERP_MULT_MAIN.en=0x0;

	INTERP_MULT_ZOOM.payload=0x0;
		INTERP_MULT_ZOOM.coef_len=0x4;
		INTERP_MULT_ZOOM.comm_mult=0x4;
		INTERP_MULT_ZOOM.mult=0x4;
		INTERP_MULT_ZOOM.en=0x0;

	INTERP_MULT_MAIN_FINE.payload=0x0;
		INTERP_MULT_MAIN_FINE.coef_len=0x3f;
		INTERP_MULT_MAIN_FINE.comm_mult=0xfc;
		INTERP_MULT_MAIN_FINE.mult=0xfc;
		INTERP_MULT_MAIN_FINE.en=0x1;

	INTERP_MULT_EYE.payload=0x0;
		INTERP_MULT_EYE.coef_len=0x1;
		INTERP_MULT_EYE.comm_mult=0x4;
		INTERP_MULT_EYE.mult=0x4;
		INTERP_MULT_EYE.en=0x0;


	INTERP_COEF_TAP.payload=0x0;
		INTERP_COEF_TAP.tap=0x1;
		INTERP_COEF_TAP.ce=0xf;

	INTERP_COEF_WR.payload=0x0;
		INTERP_COEF_WR.wdata=0x0;
		INTERP_COEF_WR.waddr=0x0;
		INTERP_COEF_WR.coef_group=0x0;
		INTERP_COEF_WR.wr_en=0x0;

	COMPRESS_MAIN.payload=0x0;
		COMPRESS_MAIN.rate=0x4;
		COMPRESS_MAIN.peak=0x1;
		COMPRESS_MAIN.en=0x0;

	COMPRESS_ZOOM.payload=0x0;
		COMPRESS_ZOOM.rate=0x4;
		COMPRESS_ZOOM.peak=0x1;
		COMPRESS_ZOOM.en=0x0;


	LA_FINE_DELAY_MAIN.payload=0x0;
		LA_FINE_DELAY_MAIN.delay=0x4;
		LA_FINE_DELAY_MAIN.en=0x1;

	LA_FINE_DELAY_ZOOM.payload=0x0;
		LA_FINE_DELAY_ZOOM.delay=0x4;
		LA_FINE_DELAY_ZOOM.en=0x1;

	LA_COMP_MAIN.payload=0x0;
		LA_COMP_MAIN.rate=0x1;
		LA_COMP_MAIN.en=0x0;

	LA_COMP_ZOOM.payload=0x0;
		LA_COMP_ZOOM.rate=0x1;
		LA_COMP_ZOOM.en=0x0;


	TX_LEN_CH=0x3e8;
	TX_LEN_LA=0x3e8;
	TX_LEN_ZOOM_CH=0x3e8;
	TX_LEN_ZOOM_LA=0x3e8;

	TX_LEN_TRACE_CH=0xf4240;
	TX_LEN_TRACE_LA=0xf4240;
	TX_LEN_COL_CH=0x3e8;
	TX_LEN_COL_LA=0x3e8;

	DEBUG_ACQ.payload=0x0;
		DEBUG_ACQ.samp_extract_vld=0x0;
		DEBUG_ACQ.acq_dly_cfg_en=0x0;
		DEBUG_ACQ.acq_dly_cfg=0x4;
		DEBUG_ACQ.trig_src_type=0x0;

	DEBUG_TX.payload=0x0;
		DEBUG_TX.spu_tx_cnt=0x0;
		DEBUG_TX.spu_frm_fsm=0x0;
		DEBUG_TX.tx_cnt_clr=0x1;
		DEBUG_TX.v_tx_bypass=0x1;

		DEBUG_TX.frm_head_jmp=0x0;

	DEBUG_MEM.payload=0x0;
		DEBUG_MEM.trig_tpu_coarse=0x0;
		DEBUG_MEM.mem_bram_dly=0x0;
		DEBUG_MEM.trig_offset_dot_pre=0x0;
		DEBUG_MEM.trig_dly_dot_out=0x0;

		DEBUG_MEM.trig_coarse_fine=0x0;
		DEBUG_MEM.mem_last_dot=0x0;

	DEBUG_SCAN.payload=0x0;
		DEBUG_SCAN.pos_sa_view_frm_cnt=0x0;


	RECORD_SUM.payload=0x0;
		RECORD_SUM.wav_view_len=0x0;
		RECORD_SUM.wav_view_full=0x0;

	SCAN_ROLL_FRM.payload=0x0;
		SCAN_ROLL_FRM.scan_roll_frm_num=0x0;

	ADC_AVG.payload=0x0;
		ADC_AVG.rate=0x10;
		ADC_AVG.done=0x0;
		ADC_AVG.en=0x1;

	ADC_AVG_RES_AB.payload=0x0;
		ADC_AVG_RES_AB.B=0x0;
		ADC_AVG_RES_AB.A=0x0;


	ADC_AVG_RES_CD.payload=0x0;
		ADC_AVG_RES_CD.D=0x0;
		ADC_AVG_RES_CD.C=0x0;

	ADC_AVG_MAX.payload=0x0;
		ADC_AVG_MAX.max_D=0x0;
		ADC_AVG_MAX.max_C=0x0;
		ADC_AVG_MAX.max_B=0x0;
		ADC_AVG_MAX.max_A=0x0;

	ADC_AVG_MIN.payload=0x0;
		ADC_AVG_MIN.min_D=0x0;
		ADC_AVG_MIN.min_C=0x0;
		ADC_AVG_MIN.min_B=0x0;
		ADC_AVG_MIN.min_A=0x0;

	LA_SA_RES.payload=0x0;
		LA_SA_RES.la=0x0;


	ZONE_TRIG_AUX.payload=0x0;
		ZONE_TRIG_AUX.pulse_width=0x1;
		ZONE_TRIG_AUX.inv=0x0;
		ZONE_TRIG_AUX.pass_fail=0x0;

	ZONE_TRIG_TAB1.payload=0x0;
		ZONE_TRIG_TAB1.threshold_l=0x78;
		ZONE_TRIG_TAB1.threshold_h=0x96;
		ZONE_TRIG_TAB1.threshold_en=0x1;
		ZONE_TRIG_TAB1.column_addr=0x1;

		ZONE_TRIG_TAB1.tab_index=0x1;

	ZONE_TRIG_TAB2.payload=0x0;
		ZONE_TRIG_TAB2.threshold_l=0x50;
		ZONE_TRIG_TAB2.threshold_h=0x64;
		ZONE_TRIG_TAB2.threshold_en=0x1;
		ZONE_TRIG_TAB2.column_addr=0x1;

		ZONE_TRIG_TAB2.tab_index=0x1;

	MASK_TAB.payload=0x0;
		MASK_TAB.threshold_l=0x50;
		MASK_TAB.threshold_l_less_en=0x1;
		MASK_TAB.threshold_h=0x64;
		MASK_TAB.threshold_h_greater_en=0x1;

		MASK_TAB.column_addr=0x1;
		MASK_TAB.tab_index=0x1;


	MASK_TAB_EX.payload=0x0;
		MASK_TAB_EX.threshold_l=0x50;
		MASK_TAB_EX.threshold_l_greater_en=0x1;
		MASK_TAB_EX.threshold_h=0x64;
		MASK_TAB_EX.threshold_h_less_en=0x1;

		MASK_TAB_EX.column_addr=0x1;
		MASK_TAB_EX.tab_index=0x1;

	MASK_COMPRESS.payload=0x0;
		MASK_COMPRESS.rate=0x4;
		MASK_COMPRESS.peak=0x1;
		MASK_COMPRESS.en=0x0;

	MASK_CTRL.payload=0x0;
		MASK_CTRL.cross_index=0x0;
		MASK_CTRL.mask_index_en=0xf;
		MASK_CTRL.zone1_trig_tab_en=0x0;
		MASK_CTRL.zone2_trig_tab_en=0x0;

		MASK_CTRL.zone1_trig_cross=0x0;
		MASK_CTRL.zone2_trig_cross=0x0;
		MASK_CTRL.mask_result_rd=0x0;
		MASK_CTRL.sum_clr=0x0;

		MASK_CTRL.cross_clr=0x0;
		MASK_CTRL.mask_we=0x1;

	MASK_FRM_CNT_L=0x0;

	MASK_FRM_CNT_H=0x0;
	MASK_CROSS_CNT_L_CHA=0x0;
	MASK_CROSS_CNT_H_CHA=0x0;
	MASK_CROSS_CNT_L_CHB=0x0;

	MASK_CROSS_CNT_H_CHB=0x0;
	MASK_CROSS_CNT_L_CHC=0x0;
	MASK_CROSS_CNT_H_CHC=0x0;
	MASK_CROSS_CNT_L_CHD=0x0;

	MASK_CROSS_CNT_H_CHD=0x0;
	MASK_OUTPUT.payload=0x0;
		MASK_OUTPUT.pulse_width=0x1;
		MASK_OUTPUT.inv=0x0;
		MASK_OUTPUT.pass_fail=0x0;

	SEARCH_OFFSET_CH=0x0;
	SEARCH_LEN_CH=0x3e8;

	SEARCH_OFFSET_LA=0x0;
	SEARCH_LEN_LA=0x7d;
	TIME_TAG_L=0x0;
	TIME_TAG_H=0x0;

	FINE_TRIG_POSITION=0x0;
	TIME_TAG_CTRL.payload=0x0;
		TIME_TAG_CTRL.rd_en=0x1;
		TIME_TAG_CTRL.rec_first_done=0x0;

	IMPORT_INFO.payload=0x0;
		IMPORT_INFO.import_done=0x0;

	TIME_TAG_L_REC_FIRST=0x0;

	TIME_TAG_H_REC_FIRST=0x0;
	TRACE_AVG_RATE_X=0x8000;
	TRACE_AVG_RATE_Y=0x8000;
	LINEAR_INTX_CTRL.payload=0x0;
		LINEAR_INTX_CTRL.mian_bypass=0x1;
		LINEAR_INTX_CTRL.zoom_bypass=0x1;


	LINEAR_INTX_INIT=0x0;
	LINEAR_INTX_STEP=0xffffffff;
	LINEAR_INTX_INIT_ZOOM=0x0;
	LINEAR_INTX_STEP_ZOOM=0xffffffff;

	WAVE_OFFSET_EYE=0x0;
	WAVE_LEN_EYE=0xf4240;
	COMPRESS_EYE.payload=0x0;
		COMPRESS_EYE.rate=0x4;
		COMPRESS_EYE.peak=0x1;
		COMPRESS_EYE.en=0x0;

	TX_LEN_EYE=0x3e8;

	LA_LINEAR_INTX_CTRL.payload=0x0;
		LA_LINEAR_INTX_CTRL.mian_bypass=0x1;
		LA_LINEAR_INTX_CTRL.zoom_bypass=0x1;

	LA_LINEAR_INTX_INIT=0x0;
	LA_LINEAR_INTX_STEP=0xffffffff;
	LA_LINEAR_INTX_INIT_ZOOM=0x0;

	LA_LINEAR_INTX_STEP_ZOOM=0xffffffff;
	TRACE_COMPRESS_MAIN.payload=0x0;
		TRACE_COMPRESS_MAIN.rate=0x4;
		TRACE_COMPRESS_MAIN.peak=0x0;
		TRACE_COMPRESS_MAIN.en=0x0;

	TRACE_COMPRESS_ZOOM.payload=0x0;
		TRACE_COMPRESS_ZOOM.rate=0x4;
		TRACE_COMPRESS_ZOOM.peak=0x0;
		TRACE_COMPRESS_ZOOM.en=0x0;

	ZYNQ_COMPRESS_MAIN.payload=0x0;
		ZYNQ_COMPRESS_MAIN.rate=0x1;


	ZYNQ_COMPRESS_ZOOM.payload=0x0;
		ZYNQ_COMPRESS_ZOOM.rate=0x1;

	DEBUG_TX_LA.payload=0x0;
		DEBUG_TX_LA.spu_tx_cnt=0x0;

	DEBUG_TX_FRM.payload=0x0;
		DEBUG_TX_FRM.spu_tx_frm_cnt=0x0;

	IMPORT_CNT_CH.payload=0x0;
		IMPORT_CNT_CH.cnt=0x0;
		IMPORT_CNT_CH.cnt_clr=0x0;


	IMPORT_CNT_LA.payload=0x0;
		IMPORT_CNT_LA.cnt=0x0;
		IMPORT_CNT_LA.cnt_clr=0x0;

	DEBUG_LINEAR_INTX1=0x0;
	DEBUG_LINEAR_INTX2=0x0;
	DEBUG_DDR=0x0;

	DEBUG_TX_LEN_BURST.payload=0x0;
		DEBUG_TX_LEN_BURST.tx_length_burst=0x0;

}

void SpuMem::_outOtp()
{
	outCTRL();
	outDEBUG();

	outADC_DATA_IDELAY();
	outADC_CHN_ALIGN();

	outPRE_PROC();
	outFILTER();
	outDOWN_SAMP_FAST();
	outDOWN_SAMP_SLOW_H();

	outDOWN_SAMP_SLOW_L();
	outLA_DELAY();
	outCHN_DELAY_AB();
	outCHN_DELAY_CD();

	outRANDOM_RANGE();
	outEXT_TRIG_IDEALY();
	outSEGMENT_SIZE();
	outRECORD_LEN();

	outWAVE_LEN_MAIN();
	outWAVE_LEN_ZOOM();
	outWAVE_OFFSET_MAIN();
	outWAVE_OFFSET_ZOOM();

	outMEAS_OFFSET_CH();
	outMEAS_LEN_CH();
	outMEAS_OFFSET_LA();
	outMEAS_LEN_LA();

	outCOARSE_DELAY_MAIN();
	outCOARSE_DELAY_ZOOM();
	outFINE_DELAY_MAIN();
	outFINE_DELAY_ZOOM();

	outINTERP_MULT_MAIN();
	outINTERP_MULT_ZOOM();
	outINTERP_MULT_MAIN_FINE();
	outINTERP_MULT_EYE();

	outINTERP_COEF_TAP();
	outINTERP_COEF_WR();
	outCOMPRESS_MAIN();
	outCOMPRESS_ZOOM();

	outLA_FINE_DELAY_MAIN();
	outLA_FINE_DELAY_ZOOM();
	outLA_COMP_MAIN();
	outLA_COMP_ZOOM();

	outTX_LEN_CH();
	outTX_LEN_LA();
	outTX_LEN_ZOOM_CH();
	outTX_LEN_ZOOM_LA();

	outTX_LEN_TRACE_CH();
	outTX_LEN_TRACE_LA();
	outTX_LEN_COL_CH();
	outTX_LEN_COL_LA();

	outDEBUG_ACQ();
	outDEBUG_TX();
	outDEBUG_MEM();

	outSCAN_ROLL_FRM();
	outADC_AVG();


	outZONE_TRIG_AUX();
	outZONE_TRIG_TAB1();
	outZONE_TRIG_TAB2();
	outMASK_TAB();

	outMASK_TAB_EX();
	outMASK_COMPRESS();
	outMASK_CTRL();



	outMASK_OUTPUT();
	outSEARCH_OFFSET_CH();
	outSEARCH_LEN_CH();

	outSEARCH_OFFSET_LA();
	outSEARCH_LEN_LA();
	outTIME_TAG_L();
	outTIME_TAG_H();

	outFINE_TRIG_POSITION();
	outTIME_TAG_CTRL();
	outIMPORT_INFO();

	outTRACE_AVG_RATE_X();
	outTRACE_AVG_RATE_Y();
	outLINEAR_INTX_CTRL();

	outLINEAR_INTX_INIT();
	outLINEAR_INTX_STEP();
	outLINEAR_INTX_INIT_ZOOM();
	outLINEAR_INTX_STEP_ZOOM();

	outWAVE_OFFSET_EYE();
	outWAVE_LEN_EYE();
	outCOMPRESS_EYE();
	outTX_LEN_EYE();

	outLA_LINEAR_INTX_CTRL();
	outLA_LINEAR_INTX_INIT();
	outLA_LINEAR_INTX_STEP();
	outLA_LINEAR_INTX_INIT_ZOOM();

	outLA_LINEAR_INTX_STEP_ZOOM();
	outTRACE_COMPRESS_MAIN();
	outTRACE_COMPRESS_ZOOM();
	outZYNQ_COMPRESS_MAIN();

	outZYNQ_COMPRESS_ZOOM();


}
void SpuMem::push( SpuMemProxy *proxy )
{
	if ( CTRL.payload != proxy->CTRL.payload )
	{reg_assign_field(CTRL,payload,proxy->CTRL.payload);}

	if ( DEBUG.payload != proxy->DEBUG.payload )
	{reg_assign_field(DEBUG,payload,proxy->DEBUG.payload);}

	if ( ADC_DATA_IDELAY.payload != proxy->ADC_DATA_IDELAY.payload )
	{reg_assign_field(ADC_DATA_IDELAY,payload,proxy->ADC_DATA_IDELAY.payload);}

	if ( ADC_CHN_ALIGN.payload != proxy->ADC_CHN_ALIGN.payload )
	{reg_assign_field(ADC_CHN_ALIGN,payload,proxy->ADC_CHN_ALIGN.payload);}

	if ( PRE_PROC.payload != proxy->PRE_PROC.payload )
	{reg_assign_field(PRE_PROC,payload,proxy->PRE_PROC.payload);}

	if ( FILTER.payload != proxy->FILTER.payload )
	{reg_assign_field(FILTER,payload,proxy->FILTER.payload);}

	if ( DOWN_SAMP_FAST.payload != proxy->DOWN_SAMP_FAST.payload )
	{reg_assign_field(DOWN_SAMP_FAST,payload,proxy->DOWN_SAMP_FAST.payload);}

	if ( DOWN_SAMP_SLOW_H.payload != proxy->DOWN_SAMP_SLOW_H.payload )
	{reg_assign_field(DOWN_SAMP_SLOW_H,payload,proxy->DOWN_SAMP_SLOW_H.payload);}

	if ( DOWN_SAMP_SLOW_L != proxy->DOWN_SAMP_SLOW_L )
	{reg_assign(DOWN_SAMP_SLOW_L,proxy->DOWN_SAMP_SLOW_L);}

	if ( LA_DELAY != proxy->LA_DELAY )
	{reg_assign(LA_DELAY,proxy->LA_DELAY);}

	if ( CHN_DELAY_AB.payload != proxy->CHN_DELAY_AB.payload )
	{reg_assign_field(CHN_DELAY_AB,payload,proxy->CHN_DELAY_AB.payload);}

	if ( CHN_DELAY_CD.payload != proxy->CHN_DELAY_CD.payload )
	{reg_assign_field(CHN_DELAY_CD,payload,proxy->CHN_DELAY_CD.payload);}

	if ( RANDOM_RANGE != proxy->RANDOM_RANGE )
	{reg_assign(RANDOM_RANGE,proxy->RANDOM_RANGE);}

	if ( EXT_TRIG_IDEALY.payload != proxy->EXT_TRIG_IDEALY.payload )
	{reg_assign_field(EXT_TRIG_IDEALY,payload,proxy->EXT_TRIG_IDEALY.payload);}

	if ( SEGMENT_SIZE != proxy->SEGMENT_SIZE )
	{reg_assign(SEGMENT_SIZE,proxy->SEGMENT_SIZE);}

	if ( RECORD_LEN != proxy->RECORD_LEN )
	{reg_assign(RECORD_LEN,proxy->RECORD_LEN);}

	if ( WAVE_LEN_MAIN != proxy->WAVE_LEN_MAIN )
	{reg_assign(WAVE_LEN_MAIN,proxy->WAVE_LEN_MAIN);}

	if ( WAVE_LEN_ZOOM != proxy->WAVE_LEN_ZOOM )
	{reg_assign(WAVE_LEN_ZOOM,proxy->WAVE_LEN_ZOOM);}

	if ( WAVE_OFFSET_MAIN != proxy->WAVE_OFFSET_MAIN )
	{reg_assign(WAVE_OFFSET_MAIN,proxy->WAVE_OFFSET_MAIN);}

	if ( WAVE_OFFSET_ZOOM != proxy->WAVE_OFFSET_ZOOM )
	{reg_assign(WAVE_OFFSET_ZOOM,proxy->WAVE_OFFSET_ZOOM);}

	if ( MEAS_OFFSET_CH != proxy->MEAS_OFFSET_CH )
	{reg_assign(MEAS_OFFSET_CH,proxy->MEAS_OFFSET_CH);}

	if ( MEAS_LEN_CH != proxy->MEAS_LEN_CH )
	{reg_assign(MEAS_LEN_CH,proxy->MEAS_LEN_CH);}

	if ( MEAS_OFFSET_LA != proxy->MEAS_OFFSET_LA )
	{reg_assign(MEAS_OFFSET_LA,proxy->MEAS_OFFSET_LA);}

	if ( MEAS_LEN_LA != proxy->MEAS_LEN_LA )
	{reg_assign(MEAS_LEN_LA,proxy->MEAS_LEN_LA);}

	if ( COARSE_DELAY_MAIN.payload != proxy->COARSE_DELAY_MAIN.payload )
	{reg_assign_field(COARSE_DELAY_MAIN,payload,proxy->COARSE_DELAY_MAIN.payload);}

	if ( COARSE_DELAY_ZOOM.payload != proxy->COARSE_DELAY_ZOOM.payload )
	{reg_assign_field(COARSE_DELAY_ZOOM,payload,proxy->COARSE_DELAY_ZOOM.payload);}

	if ( FINE_DELAY_MAIN.payload != proxy->FINE_DELAY_MAIN.payload )
	{reg_assign_field(FINE_DELAY_MAIN,payload,proxy->FINE_DELAY_MAIN.payload);}

	if ( FINE_DELAY_ZOOM.payload != proxy->FINE_DELAY_ZOOM.payload )
	{reg_assign_field(FINE_DELAY_ZOOM,payload,proxy->FINE_DELAY_ZOOM.payload);}

	if ( INTERP_MULT_MAIN.payload != proxy->INTERP_MULT_MAIN.payload )
	{reg_assign_field(INTERP_MULT_MAIN,payload,proxy->INTERP_MULT_MAIN.payload);}

	if ( INTERP_MULT_ZOOM.payload != proxy->INTERP_MULT_ZOOM.payload )
	{reg_assign_field(INTERP_MULT_ZOOM,payload,proxy->INTERP_MULT_ZOOM.payload);}

	if ( INTERP_MULT_MAIN_FINE.payload != proxy->INTERP_MULT_MAIN_FINE.payload )
	{reg_assign_field(INTERP_MULT_MAIN_FINE,payload,proxy->INTERP_MULT_MAIN_FINE.payload);}

	if ( INTERP_MULT_EYE.payload != proxy->INTERP_MULT_EYE.payload )
	{reg_assign_field(INTERP_MULT_EYE,payload,proxy->INTERP_MULT_EYE.payload);}

	if ( INTERP_COEF_TAP.payload != proxy->INTERP_COEF_TAP.payload )
	{reg_assign_field(INTERP_COEF_TAP,payload,proxy->INTERP_COEF_TAP.payload);}

	if ( INTERP_COEF_WR.payload != proxy->INTERP_COEF_WR.payload )
	{reg_assign_field(INTERP_COEF_WR,payload,proxy->INTERP_COEF_WR.payload);}

	if ( COMPRESS_MAIN.payload != proxy->COMPRESS_MAIN.payload )
	{reg_assign_field(COMPRESS_MAIN,payload,proxy->COMPRESS_MAIN.payload);}

	if ( COMPRESS_ZOOM.payload != proxy->COMPRESS_ZOOM.payload )
	{reg_assign_field(COMPRESS_ZOOM,payload,proxy->COMPRESS_ZOOM.payload);}

	if ( LA_FINE_DELAY_MAIN.payload != proxy->LA_FINE_DELAY_MAIN.payload )
	{reg_assign_field(LA_FINE_DELAY_MAIN,payload,proxy->LA_FINE_DELAY_MAIN.payload);}

	if ( LA_FINE_DELAY_ZOOM.payload != proxy->LA_FINE_DELAY_ZOOM.payload )
	{reg_assign_field(LA_FINE_DELAY_ZOOM,payload,proxy->LA_FINE_DELAY_ZOOM.payload);}

	if ( LA_COMP_MAIN.payload != proxy->LA_COMP_MAIN.payload )
	{reg_assign_field(LA_COMP_MAIN,payload,proxy->LA_COMP_MAIN.payload);}

	if ( LA_COMP_ZOOM.payload != proxy->LA_COMP_ZOOM.payload )
	{reg_assign_field(LA_COMP_ZOOM,payload,proxy->LA_COMP_ZOOM.payload);}

	if ( TX_LEN_CH != proxy->TX_LEN_CH )
	{reg_assign(TX_LEN_CH,proxy->TX_LEN_CH);}

	if ( TX_LEN_LA != proxy->TX_LEN_LA )
	{reg_assign(TX_LEN_LA,proxy->TX_LEN_LA);}

	if ( TX_LEN_ZOOM_CH != proxy->TX_LEN_ZOOM_CH )
	{reg_assign(TX_LEN_ZOOM_CH,proxy->TX_LEN_ZOOM_CH);}

	if ( TX_LEN_ZOOM_LA != proxy->TX_LEN_ZOOM_LA )
	{reg_assign(TX_LEN_ZOOM_LA,proxy->TX_LEN_ZOOM_LA);}

	if ( TX_LEN_TRACE_CH != proxy->TX_LEN_TRACE_CH )
	{reg_assign(TX_LEN_TRACE_CH,proxy->TX_LEN_TRACE_CH);}

	if ( TX_LEN_TRACE_LA != proxy->TX_LEN_TRACE_LA )
	{reg_assign(TX_LEN_TRACE_LA,proxy->TX_LEN_TRACE_LA);}

	if ( TX_LEN_COL_CH != proxy->TX_LEN_COL_CH )
	{reg_assign(TX_LEN_COL_CH,proxy->TX_LEN_COL_CH);}

	if ( TX_LEN_COL_LA != proxy->TX_LEN_COL_LA )
	{reg_assign(TX_LEN_COL_LA,proxy->TX_LEN_COL_LA);}

	if ( DEBUG_ACQ.payload != proxy->DEBUG_ACQ.payload )
	{reg_assign_field(DEBUG_ACQ,payload,proxy->DEBUG_ACQ.payload);}

	if ( DEBUG_TX.payload != proxy->DEBUG_TX.payload )
	{reg_assign_field(DEBUG_TX,payload,proxy->DEBUG_TX.payload);}

	if ( DEBUG_MEM.payload != proxy->DEBUG_MEM.payload )
	{reg_assign_field(DEBUG_MEM,payload,proxy->DEBUG_MEM.payload);}

	if ( SCAN_ROLL_FRM.payload != proxy->SCAN_ROLL_FRM.payload )
	{reg_assign_field(SCAN_ROLL_FRM,payload,proxy->SCAN_ROLL_FRM.payload);}

	if ( ADC_AVG.payload != proxy->ADC_AVG.payload )
	{reg_assign_field(ADC_AVG,payload,proxy->ADC_AVG.payload);}

	if ( ZONE_TRIG_AUX.payload != proxy->ZONE_TRIG_AUX.payload )
	{reg_assign_field(ZONE_TRIG_AUX,payload,proxy->ZONE_TRIG_AUX.payload);}

	if ( ZONE_TRIG_TAB1.payload != proxy->ZONE_TRIG_TAB1.payload )
	{reg_assign_field(ZONE_TRIG_TAB1,payload,proxy->ZONE_TRIG_TAB1.payload);}

	if ( ZONE_TRIG_TAB2.payload != proxy->ZONE_TRIG_TAB2.payload )
	{reg_assign_field(ZONE_TRIG_TAB2,payload,proxy->ZONE_TRIG_TAB2.payload);}

	if ( MASK_TAB.payload != proxy->MASK_TAB.payload )
	{reg_assign_field(MASK_TAB,payload,proxy->MASK_TAB.payload);}

	if ( MASK_TAB_EX.payload != proxy->MASK_TAB_EX.payload )
	{reg_assign_field(MASK_TAB_EX,payload,proxy->MASK_TAB_EX.payload);}

	if ( MASK_COMPRESS.payload != proxy->MASK_COMPRESS.payload )
	{reg_assign_field(MASK_COMPRESS,payload,proxy->MASK_COMPRESS.payload);}

	if ( MASK_CTRL.payload != proxy->MASK_CTRL.payload )
	{reg_assign_field(MASK_CTRL,payload,proxy->MASK_CTRL.payload);}

	if ( MASK_OUTPUT.payload != proxy->MASK_OUTPUT.payload )
	{reg_assign_field(MASK_OUTPUT,payload,proxy->MASK_OUTPUT.payload);}

	if ( SEARCH_OFFSET_CH != proxy->SEARCH_OFFSET_CH )
	{reg_assign(SEARCH_OFFSET_CH,proxy->SEARCH_OFFSET_CH);}

	if ( SEARCH_LEN_CH != proxy->SEARCH_LEN_CH )
	{reg_assign(SEARCH_LEN_CH,proxy->SEARCH_LEN_CH);}

	if ( SEARCH_OFFSET_LA != proxy->SEARCH_OFFSET_LA )
	{reg_assign(SEARCH_OFFSET_LA,proxy->SEARCH_OFFSET_LA);}

	if ( SEARCH_LEN_LA != proxy->SEARCH_LEN_LA )
	{reg_assign(SEARCH_LEN_LA,proxy->SEARCH_LEN_LA);}

	if ( TIME_TAG_L != proxy->TIME_TAG_L )
	{reg_assign(TIME_TAG_L,proxy->TIME_TAG_L);}

	if ( TIME_TAG_H != proxy->TIME_TAG_H )
	{reg_assign(TIME_TAG_H,proxy->TIME_TAG_H);}

	if ( FINE_TRIG_POSITION != proxy->FINE_TRIG_POSITION )
	{reg_assign(FINE_TRIG_POSITION,proxy->FINE_TRIG_POSITION);}

	if ( TIME_TAG_CTRL.payload != proxy->TIME_TAG_CTRL.payload )
	{reg_assign_field(TIME_TAG_CTRL,payload,proxy->TIME_TAG_CTRL.payload);}

	if ( IMPORT_INFO.payload != proxy->IMPORT_INFO.payload )
	{reg_assign_field(IMPORT_INFO,payload,proxy->IMPORT_INFO.payload);}

	if ( TRACE_AVG_RATE_X != proxy->TRACE_AVG_RATE_X )
	{reg_assign(TRACE_AVG_RATE_X,proxy->TRACE_AVG_RATE_X);}

	if ( TRACE_AVG_RATE_Y != proxy->TRACE_AVG_RATE_Y )
	{reg_assign(TRACE_AVG_RATE_Y,proxy->TRACE_AVG_RATE_Y);}

	if ( LINEAR_INTX_CTRL.payload != proxy->LINEAR_INTX_CTRL.payload )
	{reg_assign_field(LINEAR_INTX_CTRL,payload,proxy->LINEAR_INTX_CTRL.payload);}

	if ( LINEAR_INTX_INIT != proxy->LINEAR_INTX_INIT )
	{reg_assign(LINEAR_INTX_INIT,proxy->LINEAR_INTX_INIT);}

	if ( LINEAR_INTX_STEP != proxy->LINEAR_INTX_STEP )
	{reg_assign(LINEAR_INTX_STEP,proxy->LINEAR_INTX_STEP);}

	if ( LINEAR_INTX_INIT_ZOOM != proxy->LINEAR_INTX_INIT_ZOOM )
	{reg_assign(LINEAR_INTX_INIT_ZOOM,proxy->LINEAR_INTX_INIT_ZOOM);}

	if ( LINEAR_INTX_STEP_ZOOM != proxy->LINEAR_INTX_STEP_ZOOM )
	{reg_assign(LINEAR_INTX_STEP_ZOOM,proxy->LINEAR_INTX_STEP_ZOOM);}

	if ( WAVE_OFFSET_EYE != proxy->WAVE_OFFSET_EYE )
	{reg_assign(WAVE_OFFSET_EYE,proxy->WAVE_OFFSET_EYE);}

	if ( WAVE_LEN_EYE != proxy->WAVE_LEN_EYE )
	{reg_assign(WAVE_LEN_EYE,proxy->WAVE_LEN_EYE);}

	if ( COMPRESS_EYE.payload != proxy->COMPRESS_EYE.payload )
	{reg_assign_field(COMPRESS_EYE,payload,proxy->COMPRESS_EYE.payload);}

	if ( TX_LEN_EYE != proxy->TX_LEN_EYE )
	{reg_assign(TX_LEN_EYE,proxy->TX_LEN_EYE);}

	if ( LA_LINEAR_INTX_CTRL.payload != proxy->LA_LINEAR_INTX_CTRL.payload )
	{reg_assign_field(LA_LINEAR_INTX_CTRL,payload,proxy->LA_LINEAR_INTX_CTRL.payload);}

	if ( LA_LINEAR_INTX_INIT != proxy->LA_LINEAR_INTX_INIT )
	{reg_assign(LA_LINEAR_INTX_INIT,proxy->LA_LINEAR_INTX_INIT);}

	if ( LA_LINEAR_INTX_STEP != proxy->LA_LINEAR_INTX_STEP )
	{reg_assign(LA_LINEAR_INTX_STEP,proxy->LA_LINEAR_INTX_STEP);}

	if ( LA_LINEAR_INTX_INIT_ZOOM != proxy->LA_LINEAR_INTX_INIT_ZOOM )
	{reg_assign(LA_LINEAR_INTX_INIT_ZOOM,proxy->LA_LINEAR_INTX_INIT_ZOOM);}

	if ( LA_LINEAR_INTX_STEP_ZOOM != proxy->LA_LINEAR_INTX_STEP_ZOOM )
	{reg_assign(LA_LINEAR_INTX_STEP_ZOOM,proxy->LA_LINEAR_INTX_STEP_ZOOM);}

	if ( TRACE_COMPRESS_MAIN.payload != proxy->TRACE_COMPRESS_MAIN.payload )
	{reg_assign_field(TRACE_COMPRESS_MAIN,payload,proxy->TRACE_COMPRESS_MAIN.payload);}

	if ( TRACE_COMPRESS_ZOOM.payload != proxy->TRACE_COMPRESS_ZOOM.payload )
	{reg_assign_field(TRACE_COMPRESS_ZOOM,payload,proxy->TRACE_COMPRESS_ZOOM.payload);}

	if ( ZYNQ_COMPRESS_MAIN.payload != proxy->ZYNQ_COMPRESS_MAIN.payload )
	{reg_assign_field(ZYNQ_COMPRESS_MAIN,payload,proxy->ZYNQ_COMPRESS_MAIN.payload);}

	if ( ZYNQ_COMPRESS_ZOOM.payload != proxy->ZYNQ_COMPRESS_ZOOM.payload )
	{reg_assign_field(ZYNQ_COMPRESS_ZOOM,payload,proxy->ZYNQ_COMPRESS_ZOOM.payload);}

}
void SpuMem::pull( SpuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(VERSION);
	reg_in(TEST);
	reg_in(CTRL);
	reg_in(DEBUG);
	reg_in(ADC_DATA_IDELAY);
	reg_in(ADC_DATA_CHECK);
	reg_in(ADC_CHN_ALIGN_CHK);
	reg_in(ADC_CHN_ALIGN);
	reg_in(DOWN_SAMP_FAST);
	reg_in(DOWN_SAMP_SLOW_H);
	reg_in(DOWN_SAMP_SLOW_L);
	reg_in(LA_DELAY);
	reg_in(CHN_DELAY_AB);
	reg_in(CHN_DELAY_CD);
	reg_in(RANDOM_RANGE);
	reg_in(EXT_TRIG_IDEALY);
	reg_in(SEGMENT_SIZE);
	reg_in(RECORD_LEN);
	reg_in(WAVE_LEN_MAIN);
	reg_in(WAVE_LEN_ZOOM);
	reg_in(WAVE_OFFSET_MAIN);
	reg_in(WAVE_OFFSET_ZOOM);
	reg_in(MEAS_OFFSET_CH);
	reg_in(MEAS_LEN_CH);
	reg_in(MEAS_OFFSET_LA);
	reg_in(MEAS_LEN_LA);
	reg_in(COARSE_DELAY_MAIN);
	reg_in(COARSE_DELAY_ZOOM);
	reg_in(FINE_DELAY_MAIN);
	reg_in(FINE_DELAY_ZOOM);
	reg_in(INTERP_MULT_MAIN);
	reg_in(INTERP_MULT_ZOOM);
	reg_in(INTERP_MULT_MAIN_FINE);
	reg_in(INTERP_MULT_EYE);
	reg_in(COMPRESS_MAIN);
	reg_in(COMPRESS_ZOOM);
	reg_in(LA_FINE_DELAY_MAIN);
	reg_in(LA_FINE_DELAY_ZOOM);
	reg_in(LA_COMP_MAIN);
	reg_in(LA_COMP_ZOOM);
	reg_in(TX_LEN_CH);
	reg_in(TX_LEN_LA);
	reg_in(TX_LEN_ZOOM_CH);
	reg_in(TX_LEN_ZOOM_LA);
	reg_in(TX_LEN_TRACE_CH);
	reg_in(TX_LEN_TRACE_LA);
	reg_in(TX_LEN_COL_CH);
	reg_in(TX_LEN_COL_LA);
	reg_in(DEBUG_ACQ);
	reg_in(DEBUG_TX);
	reg_in(DEBUG_MEM);
	reg_in(DEBUG_SCAN);
	reg_in(RECORD_SUM);
	reg_in(SCAN_ROLL_FRM);
	reg_in(ADC_AVG);
	reg_in(ADC_AVG_RES_AB);
	reg_in(ADC_AVG_RES_CD);
	reg_in(ADC_AVG_MAX);
	reg_in(ADC_AVG_MIN);
	reg_in(LA_SA_RES);
	reg_in(ZONE_TRIG_AUX);
	reg_in(MASK_TAB);
	reg_in(MASK_TAB_EX);
	reg_in(MASK_COMPRESS);
	reg_in(MASK_CTRL);
	reg_in(MASK_FRM_CNT_L);
	reg_in(MASK_FRM_CNT_H);
	reg_in(MASK_CROSS_CNT_L_CHA);
	reg_in(MASK_CROSS_CNT_H_CHA);
	reg_in(MASK_CROSS_CNT_L_CHB);
	reg_in(MASK_CROSS_CNT_H_CHB);
	reg_in(MASK_CROSS_CNT_L_CHC);
	reg_in(MASK_CROSS_CNT_H_CHC);
	reg_in(MASK_CROSS_CNT_L_CHD);
	reg_in(MASK_CROSS_CNT_H_CHD);
	reg_in(MASK_OUTPUT);
	reg_in(SEARCH_OFFSET_CH);
	reg_in(SEARCH_LEN_CH);
	reg_in(SEARCH_OFFSET_LA);
	reg_in(SEARCH_LEN_LA);
	reg_in(TIME_TAG_L);
	reg_in(TIME_TAG_H);
	reg_in(FINE_TRIG_POSITION);
	reg_in(TIME_TAG_CTRL);
	reg_in(IMPORT_INFO);
	reg_in(TIME_TAG_L_REC_FIRST);
	reg_in(TIME_TAG_H_REC_FIRST);
	reg_in(TRACE_AVG_RATE_X);
	reg_in(TRACE_AVG_RATE_Y);
	reg_in(LINEAR_INTX_CTRL);
	reg_in(LINEAR_INTX_INIT);
	reg_in(LINEAR_INTX_STEP);
	reg_in(LINEAR_INTX_INIT_ZOOM);
	reg_in(LINEAR_INTX_STEP_ZOOM);
	reg_in(WAVE_OFFSET_EYE);
	reg_in(WAVE_LEN_EYE);
	reg_in(COMPRESS_EYE);
	reg_in(TX_LEN_EYE);
	reg_in(LA_LINEAR_INTX_CTRL);
	reg_in(LA_LINEAR_INTX_INIT);
	reg_in(LA_LINEAR_INTX_STEP);
	reg_in(LA_LINEAR_INTX_INIT_ZOOM);
	reg_in(LA_LINEAR_INTX_STEP_ZOOM);
	reg_in(TRACE_COMPRESS_MAIN);
	reg_in(TRACE_COMPRESS_ZOOM);
	reg_in(ZYNQ_COMPRESS_MAIN);
	reg_in(ZYNQ_COMPRESS_ZOOM);
	reg_in(DEBUG_TX_LA);
	reg_in(DEBUG_TX_FRM);
	reg_in(IMPORT_CNT_CH);
	reg_in(IMPORT_CNT_LA);
	reg_in(DEBUG_LINEAR_INTX1);
	reg_in(DEBUG_LINEAR_INTX2);
	reg_in(DEBUG_DDR);
	reg_in(DEBUG_TX_LEN_BURST);

	*proxy = SpuMemProxy(*this);
}
Spu_reg SpuMem::inVERSION()
{
	reg_in(VERSION);
	return VERSION;
}


Spu_reg SpuMem::inTEST()
{
	reg_in(TEST);
	return TEST;
}


void SpuMem::setCTRL_spu_clr( Spu_reg value )
{
	reg_assign_field(CTRL,spu_clr,value);
}
void SpuMem::setCTRL_adc_sync( Spu_reg value )
{
	reg_assign_field(CTRL,adc_sync,value);
}
void SpuMem::setCTRL_adc_align_check( Spu_reg value )
{
	reg_assign_field(CTRL,adc_align_check,value);
}
void SpuMem::setCTRL_non_inter( Spu_reg value )
{
	reg_assign_field(CTRL,non_inter,value);
}
void SpuMem::setCTRL_peak( Spu_reg value )
{
	reg_assign_field(CTRL,peak,value);
}
void SpuMem::setCTRL_hi_res( Spu_reg value )
{
	reg_assign_field(CTRL,hi_res,value);
}
void SpuMem::setCTRL_anti_alias( Spu_reg value )
{
	reg_assign_field(CTRL,anti_alias,value);
}
void SpuMem::setCTRL_trace_avg( Spu_reg value )
{
	reg_assign_field(CTRL,trace_avg,value);
}
void SpuMem::setCTRL_trace_avg_mem( Spu_reg value )
{
	reg_assign_field(CTRL,trace_avg_mem,value);
}
void SpuMem::setCTRL_adc_data_inv( Spu_reg value )
{
	reg_assign_field(CTRL,adc_data_inv,value);
}
void SpuMem::setCTRL_la_probe( Spu_reg value )
{
	reg_assign_field(CTRL,la_probe,value);
}
void SpuMem::setCTRL_coarse_trig_range( Spu_reg value )
{
	reg_assign_field(CTRL,coarse_trig_range,value);
}

void SpuMem::setCTRL( Spu_reg value )
{
	reg_assign_field(CTRL,payload,value);
}

void SpuMem::outCTRL_spu_clr( Spu_reg value )
{
	out_assign_field(CTRL,spu_clr,value);
}
void SpuMem::pulseCTRL_spu_clr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,spu_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,spu_clr,idleV);
}
void SpuMem::outCTRL_adc_sync( Spu_reg value )
{
	out_assign_field(CTRL,adc_sync,value);
}
void SpuMem::pulseCTRL_adc_sync( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,adc_sync,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,adc_sync,idleV);
}
void SpuMem::outCTRL_adc_align_check( Spu_reg value )
{
	out_assign_field(CTRL,adc_align_check,value);
}
void SpuMem::pulseCTRL_adc_align_check( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,adc_align_check,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,adc_align_check,idleV);
}
void SpuMem::outCTRL_non_inter( Spu_reg value )
{
	out_assign_field(CTRL,non_inter,value);
}
void SpuMem::pulseCTRL_non_inter( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,non_inter,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,non_inter,idleV);
}
void SpuMem::outCTRL_peak( Spu_reg value )
{
	out_assign_field(CTRL,peak,value);
}
void SpuMem::pulseCTRL_peak( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,peak,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,peak,idleV);
}
void SpuMem::outCTRL_hi_res( Spu_reg value )
{
	out_assign_field(CTRL,hi_res,value);
}
void SpuMem::pulseCTRL_hi_res( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,hi_res,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,hi_res,idleV);
}
void SpuMem::outCTRL_anti_alias( Spu_reg value )
{
	out_assign_field(CTRL,anti_alias,value);
}
void SpuMem::pulseCTRL_anti_alias( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,anti_alias,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,anti_alias,idleV);
}
void SpuMem::outCTRL_trace_avg( Spu_reg value )
{
	out_assign_field(CTRL,trace_avg,value);
}
void SpuMem::pulseCTRL_trace_avg( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,trace_avg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,trace_avg,idleV);
}
void SpuMem::outCTRL_trace_avg_mem( Spu_reg value )
{
	out_assign_field(CTRL,trace_avg_mem,value);
}
void SpuMem::pulseCTRL_trace_avg_mem( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,trace_avg_mem,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,trace_avg_mem,idleV);
}
void SpuMem::outCTRL_adc_data_inv( Spu_reg value )
{
	out_assign_field(CTRL,adc_data_inv,value);
}
void SpuMem::pulseCTRL_adc_data_inv( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,adc_data_inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,adc_data_inv,idleV);
}
void SpuMem::outCTRL_la_probe( Spu_reg value )
{
	out_assign_field(CTRL,la_probe,value);
}
void SpuMem::pulseCTRL_la_probe( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,la_probe,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,la_probe,idleV);
}
void SpuMem::outCTRL_coarse_trig_range( Spu_reg value )
{
	out_assign_field(CTRL,coarse_trig_range,value);
}
void SpuMem::pulseCTRL_coarse_trig_range( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CTRL,coarse_trig_range,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,coarse_trig_range,idleV);
}

void SpuMem::outCTRL( Spu_reg value )
{
	out_assign_field(CTRL,payload,value);
}

void SpuMem::outCTRL()
{
	out_member(CTRL);
}


Spu_reg SpuMem::inCTRL()
{
	reg_in(CTRL);
	return CTRL.payload;
}


void SpuMem::setDEBUG_gray_bypass( Spu_reg value )
{
	reg_assign_field(DEBUG,gray_bypass,value);
}
void SpuMem::setDEBUG_ms_order( Spu_reg value )
{
	reg_assign_field(DEBUG,ms_order,value);
}
void SpuMem::setDEBUG_s_view( Spu_reg value )
{
	reg_assign_field(DEBUG,s_view,value);
}
void SpuMem::setDEBUG_bram_ddr_n( Spu_reg value )
{
	reg_assign_field(DEBUG,bram_ddr_n,value);
}
void SpuMem::setDEBUG_ddr_init( Spu_reg value )
{
	reg_assign_field(DEBUG,ddr_init,value);
}
void SpuMem::setDEBUG_ddr_err( Spu_reg value )
{
	reg_assign_field(DEBUG,ddr_err,value);
}
void SpuMem::setDEBUG_dbg_trig_src( Spu_reg value )
{
	reg_assign_field(DEBUG,dbg_trig_src,value);
}
void SpuMem::setDEBUG_ddr_fsm_state( Spu_reg value )
{
	reg_assign_field(DEBUG,ddr_fsm_state,value);
}
void SpuMem::setDEBUG_debug_mem( Spu_reg value )
{
	reg_assign_field(DEBUG,debug_mem,value);
}

void SpuMem::setDEBUG( Spu_reg value )
{
	reg_assign_field(DEBUG,payload,value);
}

void SpuMem::outDEBUG_gray_bypass( Spu_reg value )
{
	out_assign_field(DEBUG,gray_bypass,value);
}
void SpuMem::pulseDEBUG_gray_bypass( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,gray_bypass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,gray_bypass,idleV);
}
void SpuMem::outDEBUG_ms_order( Spu_reg value )
{
	out_assign_field(DEBUG,ms_order,value);
}
void SpuMem::pulseDEBUG_ms_order( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,ms_order,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,ms_order,idleV);
}
void SpuMem::outDEBUG_s_view( Spu_reg value )
{
	out_assign_field(DEBUG,s_view,value);
}
void SpuMem::pulseDEBUG_s_view( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,s_view,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,s_view,idleV);
}
void SpuMem::outDEBUG_bram_ddr_n( Spu_reg value )
{
	out_assign_field(DEBUG,bram_ddr_n,value);
}
void SpuMem::pulseDEBUG_bram_ddr_n( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,bram_ddr_n,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,bram_ddr_n,idleV);
}
void SpuMem::outDEBUG_ddr_init( Spu_reg value )
{
	out_assign_field(DEBUG,ddr_init,value);
}
void SpuMem::pulseDEBUG_ddr_init( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,ddr_init,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,ddr_init,idleV);
}
void SpuMem::outDEBUG_ddr_err( Spu_reg value )
{
	out_assign_field(DEBUG,ddr_err,value);
}
void SpuMem::pulseDEBUG_ddr_err( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,ddr_err,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,ddr_err,idleV);
}
void SpuMem::outDEBUG_dbg_trig_src( Spu_reg value )
{
	out_assign_field(DEBUG,dbg_trig_src,value);
}
void SpuMem::pulseDEBUG_dbg_trig_src( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,dbg_trig_src,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,dbg_trig_src,idleV);
}
void SpuMem::outDEBUG_ddr_fsm_state( Spu_reg value )
{
	out_assign_field(DEBUG,ddr_fsm_state,value);
}
void SpuMem::pulseDEBUG_ddr_fsm_state( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,ddr_fsm_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,ddr_fsm_state,idleV);
}
void SpuMem::outDEBUG_debug_mem( Spu_reg value )
{
	out_assign_field(DEBUG,debug_mem,value);
}
void SpuMem::pulseDEBUG_debug_mem( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG,debug_mem,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG,debug_mem,idleV);
}

void SpuMem::outDEBUG( Spu_reg value )
{
	out_assign_field(DEBUG,payload,value);
}

void SpuMem::outDEBUG()
{
	out_member(DEBUG);
}


Spu_reg SpuMem::inDEBUG()
{
	reg_in(DEBUG);
	return DEBUG.payload;
}


void SpuMem::setADC_DATA_IDELAY_idelay_var( Spu_reg value )
{
	reg_assign_field(ADC_DATA_IDELAY,idelay_var,value);
}
void SpuMem::setADC_DATA_IDELAY_chn_bit( Spu_reg value )
{
	reg_assign_field(ADC_DATA_IDELAY,chn_bit,value);
}
void SpuMem::setADC_DATA_IDELAY_h( Spu_reg value )
{
	reg_assign_field(ADC_DATA_IDELAY,h,value);
}
void SpuMem::setADC_DATA_IDELAY_l( Spu_reg value )
{
	reg_assign_field(ADC_DATA_IDELAY,l,value);
}
void SpuMem::setADC_DATA_IDELAY_core_ABCD( Spu_reg value )
{
	reg_assign_field(ADC_DATA_IDELAY,core_ABCD,value);
}

void SpuMem::setADC_DATA_IDELAY( Spu_reg value )
{
	reg_assign_field(ADC_DATA_IDELAY,payload,value);
}

void SpuMem::outADC_DATA_IDELAY_idelay_var( Spu_reg value )
{
	out_assign_field(ADC_DATA_IDELAY,idelay_var,value);
}
void SpuMem::pulseADC_DATA_IDELAY_idelay_var( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_DATA_IDELAY,idelay_var,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_DATA_IDELAY,idelay_var,idleV);
}
void SpuMem::outADC_DATA_IDELAY_chn_bit( Spu_reg value )
{
	out_assign_field(ADC_DATA_IDELAY,chn_bit,value);
}
void SpuMem::pulseADC_DATA_IDELAY_chn_bit( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_DATA_IDELAY,chn_bit,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_DATA_IDELAY,chn_bit,idleV);
}
void SpuMem::outADC_DATA_IDELAY_h( Spu_reg value )
{
	out_assign_field(ADC_DATA_IDELAY,h,value);
}
void SpuMem::pulseADC_DATA_IDELAY_h( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_DATA_IDELAY,h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_DATA_IDELAY,h,idleV);
}
void SpuMem::outADC_DATA_IDELAY_l( Spu_reg value )
{
	out_assign_field(ADC_DATA_IDELAY,l,value);
}
void SpuMem::pulseADC_DATA_IDELAY_l( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_DATA_IDELAY,l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_DATA_IDELAY,l,idleV);
}
void SpuMem::outADC_DATA_IDELAY_core_ABCD( Spu_reg value )
{
	out_assign_field(ADC_DATA_IDELAY,core_ABCD,value);
}
void SpuMem::pulseADC_DATA_IDELAY_core_ABCD( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_DATA_IDELAY,core_ABCD,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_DATA_IDELAY,core_ABCD,idleV);
}

void SpuMem::outADC_DATA_IDELAY( Spu_reg value )
{
	out_assign_field(ADC_DATA_IDELAY,payload,value);
}

void SpuMem::outADC_DATA_IDELAY()
{
	out_member(ADC_DATA_IDELAY);
}


Spu_reg SpuMem::inADC_DATA_IDELAY()
{
	reg_in(ADC_DATA_IDELAY);
	return ADC_DATA_IDELAY.payload;
}


Spu_reg SpuMem::inADC_DATA_CHECK()
{
	reg_in(ADC_DATA_CHECK);
	return ADC_DATA_CHECK.payload;
}


Spu_reg SpuMem::inADC_CHN_ALIGN_CHK()
{
	reg_in(ADC_CHN_ALIGN_CHK);
	return ADC_CHN_ALIGN_CHK.payload;
}


void SpuMem::setADC_CHN_ALIGN_adc_d( Spu_reg value )
{
	reg_assign_field(ADC_CHN_ALIGN,adc_d,value);
}
void SpuMem::setADC_CHN_ALIGN_adc_c( Spu_reg value )
{
	reg_assign_field(ADC_CHN_ALIGN,adc_c,value);
}
void SpuMem::setADC_CHN_ALIGN_adc_b( Spu_reg value )
{
	reg_assign_field(ADC_CHN_ALIGN,adc_b,value);
}
void SpuMem::setADC_CHN_ALIGN_adc_a( Spu_reg value )
{
	reg_assign_field(ADC_CHN_ALIGN,adc_a,value);
}

void SpuMem::setADC_CHN_ALIGN( Spu_reg value )
{
	reg_assign_field(ADC_CHN_ALIGN,payload,value);
}

void SpuMem::outADC_CHN_ALIGN_adc_d( Spu_reg value )
{
	out_assign_field(ADC_CHN_ALIGN,adc_d,value);
}
void SpuMem::pulseADC_CHN_ALIGN_adc_d( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_CHN_ALIGN,adc_d,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_CHN_ALIGN,adc_d,idleV);
}
void SpuMem::outADC_CHN_ALIGN_adc_c( Spu_reg value )
{
	out_assign_field(ADC_CHN_ALIGN,adc_c,value);
}
void SpuMem::pulseADC_CHN_ALIGN_adc_c( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_CHN_ALIGN,adc_c,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_CHN_ALIGN,adc_c,idleV);
}
void SpuMem::outADC_CHN_ALIGN_adc_b( Spu_reg value )
{
	out_assign_field(ADC_CHN_ALIGN,adc_b,value);
}
void SpuMem::pulseADC_CHN_ALIGN_adc_b( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_CHN_ALIGN,adc_b,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_CHN_ALIGN,adc_b,idleV);
}
void SpuMem::outADC_CHN_ALIGN_adc_a( Spu_reg value )
{
	out_assign_field(ADC_CHN_ALIGN,adc_a,value);
}
void SpuMem::pulseADC_CHN_ALIGN_adc_a( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_CHN_ALIGN,adc_a,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_CHN_ALIGN,adc_a,idleV);
}

void SpuMem::outADC_CHN_ALIGN( Spu_reg value )
{
	out_assign_field(ADC_CHN_ALIGN,payload,value);
}

void SpuMem::outADC_CHN_ALIGN()
{
	out_member(ADC_CHN_ALIGN);
}


Spu_reg SpuMem::inADC_CHN_ALIGN()
{
	reg_in(ADC_CHN_ALIGN);
	return ADC_CHN_ALIGN.payload;
}


void SpuMem::setPRE_PROC_proc_en( Spu_reg value )
{
	reg_assign_field(PRE_PROC,proc_en,value);
}
void SpuMem::setPRE_PROC_cfg_filter_en( Spu_reg value )
{
	reg_assign_field(PRE_PROC,cfg_filter_en,value);
}

void SpuMem::setPRE_PROC( Spu_reg value )
{
	reg_assign_field(PRE_PROC,payload,value);
}

void SpuMem::outPRE_PROC_proc_en( Spu_reg value )
{
	out_assign_field(PRE_PROC,proc_en,value);
}
void SpuMem::pulsePRE_PROC_proc_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(PRE_PROC,proc_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PRE_PROC,proc_en,idleV);
}
void SpuMem::outPRE_PROC_cfg_filter_en( Spu_reg value )
{
	out_assign_field(PRE_PROC,cfg_filter_en,value);
}
void SpuMem::pulsePRE_PROC_cfg_filter_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(PRE_PROC,cfg_filter_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(PRE_PROC,cfg_filter_en,idleV);
}

void SpuMem::outPRE_PROC( Spu_reg value )
{
	out_assign_field(PRE_PROC,payload,value);
}

void SpuMem::outPRE_PROC()
{
	out_member(PRE_PROC);
}


void SpuMem::setFILTER_coeff( Spu_reg value )
{
	reg_assign_field(FILTER,coeff,value);
}
void SpuMem::setFILTER_coeff_id( Spu_reg value )
{
	reg_assign_field(FILTER,coeff_id,value);
}
void SpuMem::setFILTER_coeff_sel( Spu_reg value )
{
	reg_assign_field(FILTER,coeff_sel,value);
}
void SpuMem::setFILTER_coeff_ch( Spu_reg value )
{
	reg_assign_field(FILTER,coeff_ch,value);
}

void SpuMem::setFILTER( Spu_reg value )
{
	reg_assign_field(FILTER,payload,value);
}

void SpuMem::outFILTER_coeff( Spu_reg value )
{
	out_assign_field(FILTER,coeff,value);
}
void SpuMem::pulseFILTER_coeff( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FILTER,coeff,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FILTER,coeff,idleV);
}
void SpuMem::outFILTER_coeff_id( Spu_reg value )
{
	out_assign_field(FILTER,coeff_id,value);
}
void SpuMem::pulseFILTER_coeff_id( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FILTER,coeff_id,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FILTER,coeff_id,idleV);
}
void SpuMem::outFILTER_coeff_sel( Spu_reg value )
{
	out_assign_field(FILTER,coeff_sel,value);
}
void SpuMem::pulseFILTER_coeff_sel( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FILTER,coeff_sel,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FILTER,coeff_sel,idleV);
}
void SpuMem::outFILTER_coeff_ch( Spu_reg value )
{
	out_assign_field(FILTER,coeff_ch,value);
}
void SpuMem::pulseFILTER_coeff_ch( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FILTER,coeff_ch,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FILTER,coeff_ch,idleV);
}

void SpuMem::outFILTER( Spu_reg value )
{
	out_assign_field(FILTER,payload,value);
}

void SpuMem::outFILTER()
{
	out_member(FILTER);
}


void SpuMem::setDOWN_SAMP_FAST_down_samp_fast( Spu_reg value )
{
	reg_assign_field(DOWN_SAMP_FAST,down_samp_fast,value);
}
void SpuMem::setDOWN_SAMP_FAST_en( Spu_reg value )
{
	reg_assign_field(DOWN_SAMP_FAST,en,value);
}

void SpuMem::setDOWN_SAMP_FAST( Spu_reg value )
{
	reg_assign_field(DOWN_SAMP_FAST,payload,value);
}

void SpuMem::outDOWN_SAMP_FAST_down_samp_fast( Spu_reg value )
{
	out_assign_field(DOWN_SAMP_FAST,down_samp_fast,value);
}
void SpuMem::pulseDOWN_SAMP_FAST_down_samp_fast( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DOWN_SAMP_FAST,down_samp_fast,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DOWN_SAMP_FAST,down_samp_fast,idleV);
}
void SpuMem::outDOWN_SAMP_FAST_en( Spu_reg value )
{
	out_assign_field(DOWN_SAMP_FAST,en,value);
}
void SpuMem::pulseDOWN_SAMP_FAST_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DOWN_SAMP_FAST,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DOWN_SAMP_FAST,en,idleV);
}

void SpuMem::outDOWN_SAMP_FAST( Spu_reg value )
{
	out_assign_field(DOWN_SAMP_FAST,payload,value);
}

void SpuMem::outDOWN_SAMP_FAST()
{
	out_member(DOWN_SAMP_FAST);
}


Spu_reg SpuMem::inDOWN_SAMP_FAST()
{
	reg_in(DOWN_SAMP_FAST);
	return DOWN_SAMP_FAST.payload;
}


void SpuMem::setDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value )
{
	reg_assign_field(DOWN_SAMP_SLOW_H,down_samp_slow_h,value);
}

void SpuMem::setDOWN_SAMP_SLOW_H( Spu_reg value )
{
	reg_assign_field(DOWN_SAMP_SLOW_H,payload,value);
}

void SpuMem::outDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value )
{
	out_assign_field(DOWN_SAMP_SLOW_H,down_samp_slow_h,value);
}
void SpuMem::pulseDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DOWN_SAMP_SLOW_H,down_samp_slow_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DOWN_SAMP_SLOW_H,down_samp_slow_h,idleV);
}

void SpuMem::outDOWN_SAMP_SLOW_H( Spu_reg value )
{
	out_assign_field(DOWN_SAMP_SLOW_H,payload,value);
}

void SpuMem::outDOWN_SAMP_SLOW_H()
{
	out_member(DOWN_SAMP_SLOW_H);
}


Spu_reg SpuMem::inDOWN_SAMP_SLOW_H()
{
	reg_in(DOWN_SAMP_SLOW_H);
	return DOWN_SAMP_SLOW_H.payload;
}


void SpuMem::setDOWN_SAMP_SLOW_L( Spu_reg value )
{
	reg_assign(DOWN_SAMP_SLOW_L,value);
}

void SpuMem::outDOWN_SAMP_SLOW_L( Spu_reg value )
{
	out_assign(DOWN_SAMP_SLOW_L,value);
}

void SpuMem::outDOWN_SAMP_SLOW_L()
{
	out_member(DOWN_SAMP_SLOW_L);
}


Spu_reg SpuMem::inDOWN_SAMP_SLOW_L()
{
	reg_in(DOWN_SAMP_SLOW_L);
	return DOWN_SAMP_SLOW_L;
}


void SpuMem::setLA_DELAY( Spu_reg value )
{
	reg_assign(LA_DELAY,value);
}

void SpuMem::outLA_DELAY( Spu_reg value )
{
	out_assign(LA_DELAY,value);
}

void SpuMem::outLA_DELAY()
{
	out_member(LA_DELAY);
}


Spu_reg SpuMem::inLA_DELAY()
{
	reg_in(LA_DELAY);
	return LA_DELAY;
}


void SpuMem::setCHN_DELAY_AB_B( Spu_reg value )
{
	reg_assign_field(CHN_DELAY_AB,B,value);
}
void SpuMem::setCHN_DELAY_AB_A( Spu_reg value )
{
	reg_assign_field(CHN_DELAY_AB,A,value);
}

void SpuMem::setCHN_DELAY_AB( Spu_reg value )
{
	reg_assign_field(CHN_DELAY_AB,payload,value);
}

void SpuMem::outCHN_DELAY_AB_B( Spu_reg value )
{
	out_assign_field(CHN_DELAY_AB,B,value);
}
void SpuMem::pulseCHN_DELAY_AB_B( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CHN_DELAY_AB,B,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CHN_DELAY_AB,B,idleV);
}
void SpuMem::outCHN_DELAY_AB_A( Spu_reg value )
{
	out_assign_field(CHN_DELAY_AB,A,value);
}
void SpuMem::pulseCHN_DELAY_AB_A( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CHN_DELAY_AB,A,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CHN_DELAY_AB,A,idleV);
}

void SpuMem::outCHN_DELAY_AB( Spu_reg value )
{
	out_assign_field(CHN_DELAY_AB,payload,value);
}

void SpuMem::outCHN_DELAY_AB()
{
	out_member(CHN_DELAY_AB);
}


Spu_reg SpuMem::inCHN_DELAY_AB()
{
	reg_in(CHN_DELAY_AB);
	return CHN_DELAY_AB.payload;
}


void SpuMem::setCHN_DELAY_CD_D( Spu_reg value )
{
	reg_assign_field(CHN_DELAY_CD,D,value);
}
void SpuMem::setCHN_DELAY_CD_C( Spu_reg value )
{
	reg_assign_field(CHN_DELAY_CD,C,value);
}

void SpuMem::setCHN_DELAY_CD( Spu_reg value )
{
	reg_assign_field(CHN_DELAY_CD,payload,value);
}

void SpuMem::outCHN_DELAY_CD_D( Spu_reg value )
{
	out_assign_field(CHN_DELAY_CD,D,value);
}
void SpuMem::pulseCHN_DELAY_CD_D( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CHN_DELAY_CD,D,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CHN_DELAY_CD,D,idleV);
}
void SpuMem::outCHN_DELAY_CD_C( Spu_reg value )
{
	out_assign_field(CHN_DELAY_CD,C,value);
}
void SpuMem::pulseCHN_DELAY_CD_C( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(CHN_DELAY_CD,C,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CHN_DELAY_CD,C,idleV);
}

void SpuMem::outCHN_DELAY_CD( Spu_reg value )
{
	out_assign_field(CHN_DELAY_CD,payload,value);
}

void SpuMem::outCHN_DELAY_CD()
{
	out_member(CHN_DELAY_CD);
}


Spu_reg SpuMem::inCHN_DELAY_CD()
{
	reg_in(CHN_DELAY_CD);
	return CHN_DELAY_CD.payload;
}


void SpuMem::setRANDOM_RANGE( Spu_reg value )
{
	reg_assign(RANDOM_RANGE,value);
}

void SpuMem::outRANDOM_RANGE( Spu_reg value )
{
	out_assign(RANDOM_RANGE,value);
}

void SpuMem::outRANDOM_RANGE()
{
	out_member(RANDOM_RANGE);
}


Spu_reg SpuMem::inRANDOM_RANGE()
{
	reg_in(RANDOM_RANGE);
	return RANDOM_RANGE;
}


void SpuMem::setEXT_TRIG_IDEALY_idelay_var( Spu_reg value )
{
	reg_assign_field(EXT_TRIG_IDEALY,idelay_var,value);
}
void SpuMem::setEXT_TRIG_IDEALY_ld( Spu_reg value )
{
	reg_assign_field(EXT_TRIG_IDEALY,ld,value);
}

void SpuMem::setEXT_TRIG_IDEALY( Spu_reg value )
{
	reg_assign_field(EXT_TRIG_IDEALY,payload,value);
}

void SpuMem::outEXT_TRIG_IDEALY_idelay_var( Spu_reg value )
{
	out_assign_field(EXT_TRIG_IDEALY,idelay_var,value);
}
void SpuMem::pulseEXT_TRIG_IDEALY_idelay_var( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(EXT_TRIG_IDEALY,idelay_var,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(EXT_TRIG_IDEALY,idelay_var,idleV);
}
void SpuMem::outEXT_TRIG_IDEALY_ld( Spu_reg value )
{
	out_assign_field(EXT_TRIG_IDEALY,ld,value);
}
void SpuMem::pulseEXT_TRIG_IDEALY_ld( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(EXT_TRIG_IDEALY,ld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(EXT_TRIG_IDEALY,ld,idleV);
}

void SpuMem::outEXT_TRIG_IDEALY( Spu_reg value )
{
	out_assign_field(EXT_TRIG_IDEALY,payload,value);
}

void SpuMem::outEXT_TRIG_IDEALY()
{
	out_member(EXT_TRIG_IDEALY);
}


Spu_reg SpuMem::inEXT_TRIG_IDEALY()
{
	reg_in(EXT_TRIG_IDEALY);
	return EXT_TRIG_IDEALY.payload;
}


void SpuMem::setSEGMENT_SIZE( Spu_reg value )
{
	reg_assign(SEGMENT_SIZE,value);
}

void SpuMem::outSEGMENT_SIZE( Spu_reg value )
{
	out_assign(SEGMENT_SIZE,value);
}

void SpuMem::outSEGMENT_SIZE()
{
	out_member(SEGMENT_SIZE);
}


Spu_reg SpuMem::inSEGMENT_SIZE()
{
	reg_in(SEGMENT_SIZE);
	return SEGMENT_SIZE;
}


void SpuMem::setRECORD_LEN( Spu_reg value )
{
	reg_assign(RECORD_LEN,value);
}

void SpuMem::outRECORD_LEN( Spu_reg value )
{
	out_assign(RECORD_LEN,value);
}

void SpuMem::outRECORD_LEN()
{
	out_member(RECORD_LEN);
}


Spu_reg SpuMem::inRECORD_LEN()
{
	reg_in(RECORD_LEN);
	return RECORD_LEN;
}


void SpuMem::setWAVE_LEN_MAIN( Spu_reg value )
{
	reg_assign(WAVE_LEN_MAIN,value);
}

void SpuMem::outWAVE_LEN_MAIN( Spu_reg value )
{
	out_assign(WAVE_LEN_MAIN,value);
}

void SpuMem::outWAVE_LEN_MAIN()
{
	out_member(WAVE_LEN_MAIN);
}


Spu_reg SpuMem::inWAVE_LEN_MAIN()
{
	reg_in(WAVE_LEN_MAIN);
	return WAVE_LEN_MAIN;
}


void SpuMem::setWAVE_LEN_ZOOM( Spu_reg value )
{
	reg_assign(WAVE_LEN_ZOOM,value);
}

void SpuMem::outWAVE_LEN_ZOOM( Spu_reg value )
{
	out_assign(WAVE_LEN_ZOOM,value);
}

void SpuMem::outWAVE_LEN_ZOOM()
{
	out_member(WAVE_LEN_ZOOM);
}


Spu_reg SpuMem::inWAVE_LEN_ZOOM()
{
	reg_in(WAVE_LEN_ZOOM);
	return WAVE_LEN_ZOOM;
}


void SpuMem::setWAVE_OFFSET_MAIN( Spu_reg value )
{
	reg_assign(WAVE_OFFSET_MAIN,value);
}

void SpuMem::outWAVE_OFFSET_MAIN( Spu_reg value )
{
	out_assign(WAVE_OFFSET_MAIN,value);
}

void SpuMem::outWAVE_OFFSET_MAIN()
{
	out_member(WAVE_OFFSET_MAIN);
}


Spu_reg SpuMem::inWAVE_OFFSET_MAIN()
{
	reg_in(WAVE_OFFSET_MAIN);
	return WAVE_OFFSET_MAIN;
}


void SpuMem::setWAVE_OFFSET_ZOOM( Spu_reg value )
{
	reg_assign(WAVE_OFFSET_ZOOM,value);
}

void SpuMem::outWAVE_OFFSET_ZOOM( Spu_reg value )
{
	out_assign(WAVE_OFFSET_ZOOM,value);
}

void SpuMem::outWAVE_OFFSET_ZOOM()
{
	out_member(WAVE_OFFSET_ZOOM);
}


Spu_reg SpuMem::inWAVE_OFFSET_ZOOM()
{
	reg_in(WAVE_OFFSET_ZOOM);
	return WAVE_OFFSET_ZOOM;
}


void SpuMem::setMEAS_OFFSET_CH( Spu_reg value )
{
	reg_assign(MEAS_OFFSET_CH,value);
}

void SpuMem::outMEAS_OFFSET_CH( Spu_reg value )
{
	out_assign(MEAS_OFFSET_CH,value);
}

void SpuMem::outMEAS_OFFSET_CH()
{
	out_member(MEAS_OFFSET_CH);
}


Spu_reg SpuMem::inMEAS_OFFSET_CH()
{
	reg_in(MEAS_OFFSET_CH);
	return MEAS_OFFSET_CH;
}


void SpuMem::setMEAS_LEN_CH( Spu_reg value )
{
	reg_assign(MEAS_LEN_CH,value);
}

void SpuMem::outMEAS_LEN_CH( Spu_reg value )
{
	out_assign(MEAS_LEN_CH,value);
}

void SpuMem::outMEAS_LEN_CH()
{
	out_member(MEAS_LEN_CH);
}


Spu_reg SpuMem::inMEAS_LEN_CH()
{
	reg_in(MEAS_LEN_CH);
	return MEAS_LEN_CH;
}


void SpuMem::setMEAS_OFFSET_LA( Spu_reg value )
{
	reg_assign(MEAS_OFFSET_LA,value);
}

void SpuMem::outMEAS_OFFSET_LA( Spu_reg value )
{
	out_assign(MEAS_OFFSET_LA,value);
}

void SpuMem::outMEAS_OFFSET_LA()
{
	out_member(MEAS_OFFSET_LA);
}


Spu_reg SpuMem::inMEAS_OFFSET_LA()
{
	reg_in(MEAS_OFFSET_LA);
	return MEAS_OFFSET_LA;
}


void SpuMem::setMEAS_LEN_LA( Spu_reg value )
{
	reg_assign(MEAS_LEN_LA,value);
}

void SpuMem::outMEAS_LEN_LA( Spu_reg value )
{
	out_assign(MEAS_LEN_LA,value);
}

void SpuMem::outMEAS_LEN_LA()
{
	out_member(MEAS_LEN_LA);
}


Spu_reg SpuMem::inMEAS_LEN_LA()
{
	reg_in(MEAS_LEN_LA);
	return MEAS_LEN_LA;
}


void SpuMem::setCOARSE_DELAY_MAIN_chn_delay( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_MAIN,chn_delay,value);
}
void SpuMem::setCOARSE_DELAY_MAIN_chn_ce( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_MAIN,chn_ce,value);
}
void SpuMem::setCOARSE_DELAY_MAIN_offset_en( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_MAIN,offset_en,value);
}
void SpuMem::setCOARSE_DELAY_MAIN_delay_en( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_MAIN,delay_en,value);
}

void SpuMem::setCOARSE_DELAY_MAIN( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_MAIN,payload,value);
}

void SpuMem::outCOARSE_DELAY_MAIN_chn_delay( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_MAIN,chn_delay,value);
}
void SpuMem::pulseCOARSE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_MAIN,chn_delay,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_MAIN,chn_delay,idleV);
}
void SpuMem::outCOARSE_DELAY_MAIN_chn_ce( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_MAIN,chn_ce,value);
}
void SpuMem::pulseCOARSE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_MAIN,chn_ce,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_MAIN,chn_ce,idleV);
}
void SpuMem::outCOARSE_DELAY_MAIN_offset_en( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_MAIN,offset_en,value);
}
void SpuMem::pulseCOARSE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_MAIN,offset_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_MAIN,offset_en,idleV);
}
void SpuMem::outCOARSE_DELAY_MAIN_delay_en( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_MAIN,delay_en,value);
}
void SpuMem::pulseCOARSE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_MAIN,delay_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_MAIN,delay_en,idleV);
}

void SpuMem::outCOARSE_DELAY_MAIN( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_MAIN,payload,value);
}

void SpuMem::outCOARSE_DELAY_MAIN()
{
	out_member(COARSE_DELAY_MAIN);
}


Spu_reg SpuMem::inCOARSE_DELAY_MAIN()
{
	reg_in(COARSE_DELAY_MAIN);
	return COARSE_DELAY_MAIN.payload;
}


void SpuMem::setCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_ZOOM,chn_delay,value);
}
void SpuMem::setCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_ZOOM,chn_ce,value);
}
void SpuMem::setCOARSE_DELAY_ZOOM_offset_en( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_ZOOM,offset_en,value);
}
void SpuMem::setCOARSE_DELAY_ZOOM_delay_en( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_ZOOM,delay_en,value);
}

void SpuMem::setCOARSE_DELAY_ZOOM( Spu_reg value )
{
	reg_assign_field(COARSE_DELAY_ZOOM,payload,value);
}

void SpuMem::outCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_ZOOM,chn_delay,value);
}
void SpuMem::pulseCOARSE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_ZOOM,chn_delay,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_ZOOM,chn_delay,idleV);
}
void SpuMem::outCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_ZOOM,chn_ce,value);
}
void SpuMem::pulseCOARSE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_ZOOM,chn_ce,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_ZOOM,chn_ce,idleV);
}
void SpuMem::outCOARSE_DELAY_ZOOM_offset_en( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_ZOOM,offset_en,value);
}
void SpuMem::pulseCOARSE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_ZOOM,offset_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_ZOOM,offset_en,idleV);
}
void SpuMem::outCOARSE_DELAY_ZOOM_delay_en( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_ZOOM,delay_en,value);
}
void SpuMem::pulseCOARSE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COARSE_DELAY_ZOOM,delay_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COARSE_DELAY_ZOOM,delay_en,idleV);
}

void SpuMem::outCOARSE_DELAY_ZOOM( Spu_reg value )
{
	out_assign_field(COARSE_DELAY_ZOOM,payload,value);
}

void SpuMem::outCOARSE_DELAY_ZOOM()
{
	out_member(COARSE_DELAY_ZOOM);
}


Spu_reg SpuMem::inCOARSE_DELAY_ZOOM()
{
	reg_in(COARSE_DELAY_ZOOM);
	return COARSE_DELAY_ZOOM.payload;
}


void SpuMem::setFINE_DELAY_MAIN_chn_delay( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_MAIN,chn_delay,value);
}
void SpuMem::setFINE_DELAY_MAIN_chn_ce( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_MAIN,chn_ce,value);
}
void SpuMem::setFINE_DELAY_MAIN_offset_en( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_MAIN,offset_en,value);
}
void SpuMem::setFINE_DELAY_MAIN_delay_en( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_MAIN,delay_en,value);
}

void SpuMem::setFINE_DELAY_MAIN( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_MAIN,payload,value);
}

void SpuMem::outFINE_DELAY_MAIN_chn_delay( Spu_reg value )
{
	out_assign_field(FINE_DELAY_MAIN,chn_delay,value);
}
void SpuMem::pulseFINE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_MAIN,chn_delay,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_MAIN,chn_delay,idleV);
}
void SpuMem::outFINE_DELAY_MAIN_chn_ce( Spu_reg value )
{
	out_assign_field(FINE_DELAY_MAIN,chn_ce,value);
}
void SpuMem::pulseFINE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_MAIN,chn_ce,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_MAIN,chn_ce,idleV);
}
void SpuMem::outFINE_DELAY_MAIN_offset_en( Spu_reg value )
{
	out_assign_field(FINE_DELAY_MAIN,offset_en,value);
}
void SpuMem::pulseFINE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_MAIN,offset_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_MAIN,offset_en,idleV);
}
void SpuMem::outFINE_DELAY_MAIN_delay_en( Spu_reg value )
{
	out_assign_field(FINE_DELAY_MAIN,delay_en,value);
}
void SpuMem::pulseFINE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_MAIN,delay_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_MAIN,delay_en,idleV);
}

void SpuMem::outFINE_DELAY_MAIN( Spu_reg value )
{
	out_assign_field(FINE_DELAY_MAIN,payload,value);
}

void SpuMem::outFINE_DELAY_MAIN()
{
	out_member(FINE_DELAY_MAIN);
}


Spu_reg SpuMem::inFINE_DELAY_MAIN()
{
	reg_in(FINE_DELAY_MAIN);
	return FINE_DELAY_MAIN.payload;
}


void SpuMem::setFINE_DELAY_ZOOM_chn_delay( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_ZOOM,chn_delay,value);
}
void SpuMem::setFINE_DELAY_ZOOM_chn_ce( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_ZOOM,chn_ce,value);
}
void SpuMem::setFINE_DELAY_ZOOM_offset_en( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_ZOOM,offset_en,value);
}
void SpuMem::setFINE_DELAY_ZOOM_delay_en( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_ZOOM,delay_en,value);
}

void SpuMem::setFINE_DELAY_ZOOM( Spu_reg value )
{
	reg_assign_field(FINE_DELAY_ZOOM,payload,value);
}

void SpuMem::outFINE_DELAY_ZOOM_chn_delay( Spu_reg value )
{
	out_assign_field(FINE_DELAY_ZOOM,chn_delay,value);
}
void SpuMem::pulseFINE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_ZOOM,chn_delay,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_ZOOM,chn_delay,idleV);
}
void SpuMem::outFINE_DELAY_ZOOM_chn_ce( Spu_reg value )
{
	out_assign_field(FINE_DELAY_ZOOM,chn_ce,value);
}
void SpuMem::pulseFINE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_ZOOM,chn_ce,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_ZOOM,chn_ce,idleV);
}
void SpuMem::outFINE_DELAY_ZOOM_offset_en( Spu_reg value )
{
	out_assign_field(FINE_DELAY_ZOOM,offset_en,value);
}
void SpuMem::pulseFINE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_ZOOM,offset_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_ZOOM,offset_en,idleV);
}
void SpuMem::outFINE_DELAY_ZOOM_delay_en( Spu_reg value )
{
	out_assign_field(FINE_DELAY_ZOOM,delay_en,value);
}
void SpuMem::pulseFINE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(FINE_DELAY_ZOOM,delay_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FINE_DELAY_ZOOM,delay_en,idleV);
}

void SpuMem::outFINE_DELAY_ZOOM( Spu_reg value )
{
	out_assign_field(FINE_DELAY_ZOOM,payload,value);
}

void SpuMem::outFINE_DELAY_ZOOM()
{
	out_member(FINE_DELAY_ZOOM);
}


Spu_reg SpuMem::inFINE_DELAY_ZOOM()
{
	reg_in(FINE_DELAY_ZOOM);
	return FINE_DELAY_ZOOM.payload;
}


void SpuMem::setINTERP_MULT_MAIN_coef_len( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN,coef_len,value);
}
void SpuMem::setINTERP_MULT_MAIN_comm_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN,comm_mult,value);
}
void SpuMem::setINTERP_MULT_MAIN_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN,mult,value);
}
void SpuMem::setINTERP_MULT_MAIN_en( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN,en,value);
}

void SpuMem::setINTERP_MULT_MAIN( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN,payload,value);
}

void SpuMem::outINTERP_MULT_MAIN_coef_len( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN,coef_len,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN,coef_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN,coef_len,idleV);
}
void SpuMem::outINTERP_MULT_MAIN_comm_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN,comm_mult,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN,comm_mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN,comm_mult,idleV);
}
void SpuMem::outINTERP_MULT_MAIN_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN,mult,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN,mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN,mult,idleV);
}
void SpuMem::outINTERP_MULT_MAIN_en( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN,en,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN,en,idleV);
}

void SpuMem::outINTERP_MULT_MAIN( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN,payload,value);
}

void SpuMem::outINTERP_MULT_MAIN()
{
	out_member(INTERP_MULT_MAIN);
}


Spu_reg SpuMem::inINTERP_MULT_MAIN()
{
	reg_in(INTERP_MULT_MAIN);
	return INTERP_MULT_MAIN.payload;
}


void SpuMem::setINTERP_MULT_ZOOM_coef_len( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_ZOOM,coef_len,value);
}
void SpuMem::setINTERP_MULT_ZOOM_comm_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_ZOOM,comm_mult,value);
}
void SpuMem::setINTERP_MULT_ZOOM_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_ZOOM,mult,value);
}
void SpuMem::setINTERP_MULT_ZOOM_en( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_ZOOM,en,value);
}

void SpuMem::setINTERP_MULT_ZOOM( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_ZOOM,payload,value);
}

void SpuMem::outINTERP_MULT_ZOOM_coef_len( Spu_reg value )
{
	out_assign_field(INTERP_MULT_ZOOM,coef_len,value);
}
void SpuMem::pulseINTERP_MULT_ZOOM_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_ZOOM,coef_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_ZOOM,coef_len,idleV);
}
void SpuMem::outINTERP_MULT_ZOOM_comm_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_ZOOM,comm_mult,value);
}
void SpuMem::pulseINTERP_MULT_ZOOM_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_ZOOM,comm_mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_ZOOM,comm_mult,idleV);
}
void SpuMem::outINTERP_MULT_ZOOM_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_ZOOM,mult,value);
}
void SpuMem::pulseINTERP_MULT_ZOOM_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_ZOOM,mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_ZOOM,mult,idleV);
}
void SpuMem::outINTERP_MULT_ZOOM_en( Spu_reg value )
{
	out_assign_field(INTERP_MULT_ZOOM,en,value);
}
void SpuMem::pulseINTERP_MULT_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_ZOOM,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_ZOOM,en,idleV);
}

void SpuMem::outINTERP_MULT_ZOOM( Spu_reg value )
{
	out_assign_field(INTERP_MULT_ZOOM,payload,value);
}

void SpuMem::outINTERP_MULT_ZOOM()
{
	out_member(INTERP_MULT_ZOOM);
}


Spu_reg SpuMem::inINTERP_MULT_ZOOM()
{
	reg_in(INTERP_MULT_ZOOM);
	return INTERP_MULT_ZOOM.payload;
}


void SpuMem::setINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN_FINE,coef_len,value);
}
void SpuMem::setINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN_FINE,comm_mult,value);
}
void SpuMem::setINTERP_MULT_MAIN_FINE_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN_FINE,mult,value);
}
void SpuMem::setINTERP_MULT_MAIN_FINE_en( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN_FINE,en,value);
}

void SpuMem::setINTERP_MULT_MAIN_FINE( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_MAIN_FINE,payload,value);
}

void SpuMem::outINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,coef_len,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_FINE_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,coef_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN_FINE,coef_len,idleV);
}
void SpuMem::outINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,comm_mult,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,comm_mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN_FINE,comm_mult,idleV);
}
void SpuMem::outINTERP_MULT_MAIN_FINE_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,mult,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_FINE_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN_FINE,mult,idleV);
}
void SpuMem::outINTERP_MULT_MAIN_FINE_en( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,en,value);
}
void SpuMem::pulseINTERP_MULT_MAIN_FINE_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_MAIN_FINE,en,idleV);
}

void SpuMem::outINTERP_MULT_MAIN_FINE( Spu_reg value )
{
	out_assign_field(INTERP_MULT_MAIN_FINE,payload,value);
}

void SpuMem::outINTERP_MULT_MAIN_FINE()
{
	out_member(INTERP_MULT_MAIN_FINE);
}


Spu_reg SpuMem::inINTERP_MULT_MAIN_FINE()
{
	reg_in(INTERP_MULT_MAIN_FINE);
	return INTERP_MULT_MAIN_FINE.payload;
}


void SpuMem::setINTERP_MULT_EYE_coef_len( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_EYE,coef_len,value);
}
void SpuMem::setINTERP_MULT_EYE_comm_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_EYE,comm_mult,value);
}
void SpuMem::setINTERP_MULT_EYE_mult( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_EYE,mult,value);
}
void SpuMem::setINTERP_MULT_EYE_en( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_EYE,en,value);
}

void SpuMem::setINTERP_MULT_EYE( Spu_reg value )
{
	reg_assign_field(INTERP_MULT_EYE,payload,value);
}

void SpuMem::outINTERP_MULT_EYE_coef_len( Spu_reg value )
{
	out_assign_field(INTERP_MULT_EYE,coef_len,value);
}
void SpuMem::pulseINTERP_MULT_EYE_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_EYE,coef_len,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_EYE,coef_len,idleV);
}
void SpuMem::outINTERP_MULT_EYE_comm_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_EYE,comm_mult,value);
}
void SpuMem::pulseINTERP_MULT_EYE_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_EYE,comm_mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_EYE,comm_mult,idleV);
}
void SpuMem::outINTERP_MULT_EYE_mult( Spu_reg value )
{
	out_assign_field(INTERP_MULT_EYE,mult,value);
}
void SpuMem::pulseINTERP_MULT_EYE_mult( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_EYE,mult,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_EYE,mult,idleV);
}
void SpuMem::outINTERP_MULT_EYE_en( Spu_reg value )
{
	out_assign_field(INTERP_MULT_EYE,en,value);
}
void SpuMem::pulseINTERP_MULT_EYE_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_MULT_EYE,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_MULT_EYE,en,idleV);
}

void SpuMem::outINTERP_MULT_EYE( Spu_reg value )
{
	out_assign_field(INTERP_MULT_EYE,payload,value);
}

void SpuMem::outINTERP_MULT_EYE()
{
	out_member(INTERP_MULT_EYE);
}


Spu_reg SpuMem::inINTERP_MULT_EYE()
{
	reg_in(INTERP_MULT_EYE);
	return INTERP_MULT_EYE.payload;
}


void SpuMem::setINTERP_COEF_TAP_tap( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_TAP,tap,value);
}
void SpuMem::setINTERP_COEF_TAP_ce( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_TAP,ce,value);
}

void SpuMem::setINTERP_COEF_TAP( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_TAP,payload,value);
}

void SpuMem::outINTERP_COEF_TAP_tap( Spu_reg value )
{
	out_assign_field(INTERP_COEF_TAP,tap,value);
}
void SpuMem::pulseINTERP_COEF_TAP_tap( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_COEF_TAP,tap,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_COEF_TAP,tap,idleV);
}
void SpuMem::outINTERP_COEF_TAP_ce( Spu_reg value )
{
	out_assign_field(INTERP_COEF_TAP,ce,value);
}
void SpuMem::pulseINTERP_COEF_TAP_ce( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_COEF_TAP,ce,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_COEF_TAP,ce,idleV);
}

void SpuMem::outINTERP_COEF_TAP( Spu_reg value )
{
	out_assign_field(INTERP_COEF_TAP,payload,value);
}

void SpuMem::outINTERP_COEF_TAP()
{
	out_member(INTERP_COEF_TAP);
}


void SpuMem::setINTERP_COEF_WR_wdata( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_WR,wdata,value);
}
void SpuMem::setINTERP_COEF_WR_waddr( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_WR,waddr,value);
}
void SpuMem::setINTERP_COEF_WR_coef_group( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_WR,coef_group,value);
}
void SpuMem::setINTERP_COEF_WR_wr_en( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_WR,wr_en,value);
}

void SpuMem::setINTERP_COEF_WR( Spu_reg value )
{
	reg_assign_field(INTERP_COEF_WR,payload,value);
}

void SpuMem::outINTERP_COEF_WR_wdata( Spu_reg value )
{
	out_assign_field(INTERP_COEF_WR,wdata,value);
}
void SpuMem::pulseINTERP_COEF_WR_wdata( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_COEF_WR,wdata,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_COEF_WR,wdata,idleV);
}
void SpuMem::outINTERP_COEF_WR_waddr( Spu_reg value )
{
	out_assign_field(INTERP_COEF_WR,waddr,value);
}
void SpuMem::pulseINTERP_COEF_WR_waddr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_COEF_WR,waddr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_COEF_WR,waddr,idleV);
}
void SpuMem::outINTERP_COEF_WR_coef_group( Spu_reg value )
{
	out_assign_field(INTERP_COEF_WR,coef_group,value);
}
void SpuMem::pulseINTERP_COEF_WR_coef_group( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_COEF_WR,coef_group,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_COEF_WR,coef_group,idleV);
}
void SpuMem::outINTERP_COEF_WR_wr_en( Spu_reg value )
{
	out_assign_field(INTERP_COEF_WR,wr_en,value);
}
void SpuMem::pulseINTERP_COEF_WR_wr_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(INTERP_COEF_WR,wr_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INTERP_COEF_WR,wr_en,idleV);
}

void SpuMem::outINTERP_COEF_WR( Spu_reg value )
{
	out_assign_field(INTERP_COEF_WR,payload,value);
}

void SpuMem::outINTERP_COEF_WR()
{
	out_member(INTERP_COEF_WR);
}


void SpuMem::setCOMPRESS_MAIN_rate( Spu_reg value )
{
	reg_assign_field(COMPRESS_MAIN,rate,value);
}
void SpuMem::setCOMPRESS_MAIN_peak( Spu_reg value )
{
	reg_assign_field(COMPRESS_MAIN,peak,value);
}
void SpuMem::setCOMPRESS_MAIN_en( Spu_reg value )
{
	reg_assign_field(COMPRESS_MAIN,en,value);
}

void SpuMem::setCOMPRESS_MAIN( Spu_reg value )
{
	reg_assign_field(COMPRESS_MAIN,payload,value);
}

void SpuMem::outCOMPRESS_MAIN_rate( Spu_reg value )
{
	out_assign_field(COMPRESS_MAIN,rate,value);
}
void SpuMem::pulseCOMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_MAIN,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_MAIN,rate,idleV);
}
void SpuMem::outCOMPRESS_MAIN_peak( Spu_reg value )
{
	out_assign_field(COMPRESS_MAIN,peak,value);
}
void SpuMem::pulseCOMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_MAIN,peak,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_MAIN,peak,idleV);
}
void SpuMem::outCOMPRESS_MAIN_en( Spu_reg value )
{
	out_assign_field(COMPRESS_MAIN,en,value);
}
void SpuMem::pulseCOMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_MAIN,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_MAIN,en,idleV);
}

void SpuMem::outCOMPRESS_MAIN( Spu_reg value )
{
	out_assign_field(COMPRESS_MAIN,payload,value);
}

void SpuMem::outCOMPRESS_MAIN()
{
	out_member(COMPRESS_MAIN);
}


Spu_reg SpuMem::inCOMPRESS_MAIN()
{
	reg_in(COMPRESS_MAIN);
	return COMPRESS_MAIN.payload;
}


void SpuMem::setCOMPRESS_ZOOM_rate( Spu_reg value )
{
	reg_assign_field(COMPRESS_ZOOM,rate,value);
}
void SpuMem::setCOMPRESS_ZOOM_peak( Spu_reg value )
{
	reg_assign_field(COMPRESS_ZOOM,peak,value);
}
void SpuMem::setCOMPRESS_ZOOM_en( Spu_reg value )
{
	reg_assign_field(COMPRESS_ZOOM,en,value);
}

void SpuMem::setCOMPRESS_ZOOM( Spu_reg value )
{
	reg_assign_field(COMPRESS_ZOOM,payload,value);
}

void SpuMem::outCOMPRESS_ZOOM_rate( Spu_reg value )
{
	out_assign_field(COMPRESS_ZOOM,rate,value);
}
void SpuMem::pulseCOMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_ZOOM,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_ZOOM,rate,idleV);
}
void SpuMem::outCOMPRESS_ZOOM_peak( Spu_reg value )
{
	out_assign_field(COMPRESS_ZOOM,peak,value);
}
void SpuMem::pulseCOMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_ZOOM,peak,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_ZOOM,peak,idleV);
}
void SpuMem::outCOMPRESS_ZOOM_en( Spu_reg value )
{
	out_assign_field(COMPRESS_ZOOM,en,value);
}
void SpuMem::pulseCOMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_ZOOM,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_ZOOM,en,idleV);
}

void SpuMem::outCOMPRESS_ZOOM( Spu_reg value )
{
	out_assign_field(COMPRESS_ZOOM,payload,value);
}

void SpuMem::outCOMPRESS_ZOOM()
{
	out_member(COMPRESS_ZOOM);
}


Spu_reg SpuMem::inCOMPRESS_ZOOM()
{
	reg_in(COMPRESS_ZOOM);
	return COMPRESS_ZOOM.payload;
}


void SpuMem::setLA_FINE_DELAY_MAIN_delay( Spu_reg value )
{
	reg_assign_field(LA_FINE_DELAY_MAIN,delay,value);
}
void SpuMem::setLA_FINE_DELAY_MAIN_en( Spu_reg value )
{
	reg_assign_field(LA_FINE_DELAY_MAIN,en,value);
}

void SpuMem::setLA_FINE_DELAY_MAIN( Spu_reg value )
{
	reg_assign_field(LA_FINE_DELAY_MAIN,payload,value);
}

void SpuMem::outLA_FINE_DELAY_MAIN_delay( Spu_reg value )
{
	out_assign_field(LA_FINE_DELAY_MAIN,delay,value);
}
void SpuMem::pulseLA_FINE_DELAY_MAIN_delay( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_FINE_DELAY_MAIN,delay,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_FINE_DELAY_MAIN,delay,idleV);
}
void SpuMem::outLA_FINE_DELAY_MAIN_en( Spu_reg value )
{
	out_assign_field(LA_FINE_DELAY_MAIN,en,value);
}
void SpuMem::pulseLA_FINE_DELAY_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_FINE_DELAY_MAIN,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_FINE_DELAY_MAIN,en,idleV);
}

void SpuMem::outLA_FINE_DELAY_MAIN( Spu_reg value )
{
	out_assign_field(LA_FINE_DELAY_MAIN,payload,value);
}

void SpuMem::outLA_FINE_DELAY_MAIN()
{
	out_member(LA_FINE_DELAY_MAIN);
}


Spu_reg SpuMem::inLA_FINE_DELAY_MAIN()
{
	reg_in(LA_FINE_DELAY_MAIN);
	return LA_FINE_DELAY_MAIN.payload;
}


void SpuMem::setLA_FINE_DELAY_ZOOM_delay( Spu_reg value )
{
	reg_assign_field(LA_FINE_DELAY_ZOOM,delay,value);
}
void SpuMem::setLA_FINE_DELAY_ZOOM_en( Spu_reg value )
{
	reg_assign_field(LA_FINE_DELAY_ZOOM,en,value);
}

void SpuMem::setLA_FINE_DELAY_ZOOM( Spu_reg value )
{
	reg_assign_field(LA_FINE_DELAY_ZOOM,payload,value);
}

void SpuMem::outLA_FINE_DELAY_ZOOM_delay( Spu_reg value )
{
	out_assign_field(LA_FINE_DELAY_ZOOM,delay,value);
}
void SpuMem::pulseLA_FINE_DELAY_ZOOM_delay( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_FINE_DELAY_ZOOM,delay,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_FINE_DELAY_ZOOM,delay,idleV);
}
void SpuMem::outLA_FINE_DELAY_ZOOM_en( Spu_reg value )
{
	out_assign_field(LA_FINE_DELAY_ZOOM,en,value);
}
void SpuMem::pulseLA_FINE_DELAY_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_FINE_DELAY_ZOOM,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_FINE_DELAY_ZOOM,en,idleV);
}

void SpuMem::outLA_FINE_DELAY_ZOOM( Spu_reg value )
{
	out_assign_field(LA_FINE_DELAY_ZOOM,payload,value);
}

void SpuMem::outLA_FINE_DELAY_ZOOM()
{
	out_member(LA_FINE_DELAY_ZOOM);
}


Spu_reg SpuMem::inLA_FINE_DELAY_ZOOM()
{
	reg_in(LA_FINE_DELAY_ZOOM);
	return LA_FINE_DELAY_ZOOM.payload;
}


void SpuMem::setLA_COMP_MAIN_rate( Spu_reg value )
{
	reg_assign_field(LA_COMP_MAIN,rate,value);
}
void SpuMem::setLA_COMP_MAIN_en( Spu_reg value )
{
	reg_assign_field(LA_COMP_MAIN,en,value);
}

void SpuMem::setLA_COMP_MAIN( Spu_reg value )
{
	reg_assign_field(LA_COMP_MAIN,payload,value);
}

void SpuMem::outLA_COMP_MAIN_rate( Spu_reg value )
{
	out_assign_field(LA_COMP_MAIN,rate,value);
}
void SpuMem::pulseLA_COMP_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_COMP_MAIN,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_COMP_MAIN,rate,idleV);
}
void SpuMem::outLA_COMP_MAIN_en( Spu_reg value )
{
	out_assign_field(LA_COMP_MAIN,en,value);
}
void SpuMem::pulseLA_COMP_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_COMP_MAIN,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_COMP_MAIN,en,idleV);
}

void SpuMem::outLA_COMP_MAIN( Spu_reg value )
{
	out_assign_field(LA_COMP_MAIN,payload,value);
}

void SpuMem::outLA_COMP_MAIN()
{
	out_member(LA_COMP_MAIN);
}


Spu_reg SpuMem::inLA_COMP_MAIN()
{
	reg_in(LA_COMP_MAIN);
	return LA_COMP_MAIN.payload;
}


void SpuMem::setLA_COMP_ZOOM_rate( Spu_reg value )
{
	reg_assign_field(LA_COMP_ZOOM,rate,value);
}
void SpuMem::setLA_COMP_ZOOM_en( Spu_reg value )
{
	reg_assign_field(LA_COMP_ZOOM,en,value);
}

void SpuMem::setLA_COMP_ZOOM( Spu_reg value )
{
	reg_assign_field(LA_COMP_ZOOM,payload,value);
}

void SpuMem::outLA_COMP_ZOOM_rate( Spu_reg value )
{
	out_assign_field(LA_COMP_ZOOM,rate,value);
}
void SpuMem::pulseLA_COMP_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_COMP_ZOOM,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_COMP_ZOOM,rate,idleV);
}
void SpuMem::outLA_COMP_ZOOM_en( Spu_reg value )
{
	out_assign_field(LA_COMP_ZOOM,en,value);
}
void SpuMem::pulseLA_COMP_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_COMP_ZOOM,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_COMP_ZOOM,en,idleV);
}

void SpuMem::outLA_COMP_ZOOM( Spu_reg value )
{
	out_assign_field(LA_COMP_ZOOM,payload,value);
}

void SpuMem::outLA_COMP_ZOOM()
{
	out_member(LA_COMP_ZOOM);
}


Spu_reg SpuMem::inLA_COMP_ZOOM()
{
	reg_in(LA_COMP_ZOOM);
	return LA_COMP_ZOOM.payload;
}


void SpuMem::setTX_LEN_CH( Spu_reg value )
{
	reg_assign(TX_LEN_CH,value);
}

void SpuMem::outTX_LEN_CH( Spu_reg value )
{
	out_assign(TX_LEN_CH,value);
}

void SpuMem::outTX_LEN_CH()
{
	out_member(TX_LEN_CH);
}


Spu_reg SpuMem::inTX_LEN_CH()
{
	reg_in(TX_LEN_CH);
	return TX_LEN_CH;
}


void SpuMem::setTX_LEN_LA( Spu_reg value )
{
	reg_assign(TX_LEN_LA,value);
}

void SpuMem::outTX_LEN_LA( Spu_reg value )
{
	out_assign(TX_LEN_LA,value);
}

void SpuMem::outTX_LEN_LA()
{
	out_member(TX_LEN_LA);
}


Spu_reg SpuMem::inTX_LEN_LA()
{
	reg_in(TX_LEN_LA);
	return TX_LEN_LA;
}


void SpuMem::setTX_LEN_ZOOM_CH( Spu_reg value )
{
	reg_assign(TX_LEN_ZOOM_CH,value);
}

void SpuMem::outTX_LEN_ZOOM_CH( Spu_reg value )
{
	out_assign(TX_LEN_ZOOM_CH,value);
}

void SpuMem::outTX_LEN_ZOOM_CH()
{
	out_member(TX_LEN_ZOOM_CH);
}


Spu_reg SpuMem::inTX_LEN_ZOOM_CH()
{
	reg_in(TX_LEN_ZOOM_CH);
	return TX_LEN_ZOOM_CH;
}


void SpuMem::setTX_LEN_ZOOM_LA( Spu_reg value )
{
	reg_assign(TX_LEN_ZOOM_LA,value);
}

void SpuMem::outTX_LEN_ZOOM_LA( Spu_reg value )
{
	out_assign(TX_LEN_ZOOM_LA,value);
}

void SpuMem::outTX_LEN_ZOOM_LA()
{
	out_member(TX_LEN_ZOOM_LA);
}


Spu_reg SpuMem::inTX_LEN_ZOOM_LA()
{
	reg_in(TX_LEN_ZOOM_LA);
	return TX_LEN_ZOOM_LA;
}


void SpuMem::setTX_LEN_TRACE_CH( Spu_reg value )
{
	reg_assign(TX_LEN_TRACE_CH,value);
}

void SpuMem::outTX_LEN_TRACE_CH( Spu_reg value )
{
	out_assign(TX_LEN_TRACE_CH,value);
}

void SpuMem::outTX_LEN_TRACE_CH()
{
	out_member(TX_LEN_TRACE_CH);
}


Spu_reg SpuMem::inTX_LEN_TRACE_CH()
{
	reg_in(TX_LEN_TRACE_CH);
	return TX_LEN_TRACE_CH;
}


void SpuMem::setTX_LEN_TRACE_LA( Spu_reg value )
{
	reg_assign(TX_LEN_TRACE_LA,value);
}

void SpuMem::outTX_LEN_TRACE_LA( Spu_reg value )
{
	out_assign(TX_LEN_TRACE_LA,value);
}

void SpuMem::outTX_LEN_TRACE_LA()
{
	out_member(TX_LEN_TRACE_LA);
}


Spu_reg SpuMem::inTX_LEN_TRACE_LA()
{
	reg_in(TX_LEN_TRACE_LA);
	return TX_LEN_TRACE_LA;
}


void SpuMem::setTX_LEN_COL_CH( Spu_reg value )
{
	reg_assign(TX_LEN_COL_CH,value);
}

void SpuMem::outTX_LEN_COL_CH( Spu_reg value )
{
	out_assign(TX_LEN_COL_CH,value);
}

void SpuMem::outTX_LEN_COL_CH()
{
	out_member(TX_LEN_COL_CH);
}


Spu_reg SpuMem::inTX_LEN_COL_CH()
{
	reg_in(TX_LEN_COL_CH);
	return TX_LEN_COL_CH;
}


void SpuMem::setTX_LEN_COL_LA( Spu_reg value )
{
	reg_assign(TX_LEN_COL_LA,value);
}

void SpuMem::outTX_LEN_COL_LA( Spu_reg value )
{
	out_assign(TX_LEN_COL_LA,value);
}

void SpuMem::outTX_LEN_COL_LA()
{
	out_member(TX_LEN_COL_LA);
}


Spu_reg SpuMem::inTX_LEN_COL_LA()
{
	reg_in(TX_LEN_COL_LA);
	return TX_LEN_COL_LA;
}


void SpuMem::setDEBUG_ACQ_samp_extract_vld( Spu_reg value )
{
	reg_assign_field(DEBUG_ACQ,samp_extract_vld,value);
}
void SpuMem::setDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value )
{
	reg_assign_field(DEBUG_ACQ,acq_dly_cfg_en,value);
}
void SpuMem::setDEBUG_ACQ_acq_dly_cfg( Spu_reg value )
{
	reg_assign_field(DEBUG_ACQ,acq_dly_cfg,value);
}
void SpuMem::setDEBUG_ACQ_trig_src_type( Spu_reg value )
{
	reg_assign_field(DEBUG_ACQ,trig_src_type,value);
}

void SpuMem::setDEBUG_ACQ( Spu_reg value )
{
	reg_assign_field(DEBUG_ACQ,payload,value);
}

void SpuMem::outDEBUG_ACQ_samp_extract_vld( Spu_reg value )
{
	out_assign_field(DEBUG_ACQ,samp_extract_vld,value);
}
void SpuMem::pulseDEBUG_ACQ_samp_extract_vld( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_ACQ,samp_extract_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_ACQ,samp_extract_vld,idleV);
}
void SpuMem::outDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value )
{
	out_assign_field(DEBUG_ACQ,acq_dly_cfg_en,value);
}
void SpuMem::pulseDEBUG_ACQ_acq_dly_cfg_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_ACQ,acq_dly_cfg_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_ACQ,acq_dly_cfg_en,idleV);
}
void SpuMem::outDEBUG_ACQ_acq_dly_cfg( Spu_reg value )
{
	out_assign_field(DEBUG_ACQ,acq_dly_cfg,value);
}
void SpuMem::pulseDEBUG_ACQ_acq_dly_cfg( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_ACQ,acq_dly_cfg,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_ACQ,acq_dly_cfg,idleV);
}
void SpuMem::outDEBUG_ACQ_trig_src_type( Spu_reg value )
{
	out_assign_field(DEBUG_ACQ,trig_src_type,value);
}
void SpuMem::pulseDEBUG_ACQ_trig_src_type( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_ACQ,trig_src_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_ACQ,trig_src_type,idleV);
}

void SpuMem::outDEBUG_ACQ( Spu_reg value )
{
	out_assign_field(DEBUG_ACQ,payload,value);
}

void SpuMem::outDEBUG_ACQ()
{
	out_member(DEBUG_ACQ);
}


Spu_reg SpuMem::inDEBUG_ACQ()
{
	reg_in(DEBUG_ACQ);
	return DEBUG_ACQ.payload;
}


void SpuMem::setDEBUG_TX_spu_tx_cnt( Spu_reg value )
{
	reg_assign_field(DEBUG_TX,spu_tx_cnt,value);
}
void SpuMem::setDEBUG_TX_spu_frm_fsm( Spu_reg value )
{
	reg_assign_field(DEBUG_TX,spu_frm_fsm,value);
}
void SpuMem::setDEBUG_TX_tx_cnt_clr( Spu_reg value )
{
	reg_assign_field(DEBUG_TX,tx_cnt_clr,value);
}
void SpuMem::setDEBUG_TX_v_tx_bypass( Spu_reg value )
{
	reg_assign_field(DEBUG_TX,v_tx_bypass,value);
}
void SpuMem::setDEBUG_TX_frm_head_jmp( Spu_reg value )
{
	reg_assign_field(DEBUG_TX,frm_head_jmp,value);
}

void SpuMem::setDEBUG_TX( Spu_reg value )
{
	reg_assign_field(DEBUG_TX,payload,value);
}

void SpuMem::outDEBUG_TX_spu_tx_cnt( Spu_reg value )
{
	out_assign_field(DEBUG_TX,spu_tx_cnt,value);
}
void SpuMem::pulseDEBUG_TX_spu_tx_cnt( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_TX,spu_tx_cnt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_TX,spu_tx_cnt,idleV);
}
void SpuMem::outDEBUG_TX_spu_frm_fsm( Spu_reg value )
{
	out_assign_field(DEBUG_TX,spu_frm_fsm,value);
}
void SpuMem::pulseDEBUG_TX_spu_frm_fsm( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_TX,spu_frm_fsm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_TX,spu_frm_fsm,idleV);
}
void SpuMem::outDEBUG_TX_tx_cnt_clr( Spu_reg value )
{
	out_assign_field(DEBUG_TX,tx_cnt_clr,value);
}
void SpuMem::pulseDEBUG_TX_tx_cnt_clr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_TX,tx_cnt_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_TX,tx_cnt_clr,idleV);
}
void SpuMem::outDEBUG_TX_v_tx_bypass( Spu_reg value )
{
	out_assign_field(DEBUG_TX,v_tx_bypass,value);
}
void SpuMem::pulseDEBUG_TX_v_tx_bypass( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_TX,v_tx_bypass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_TX,v_tx_bypass,idleV);
}
void SpuMem::outDEBUG_TX_frm_head_jmp( Spu_reg value )
{
	out_assign_field(DEBUG_TX,frm_head_jmp,value);
}
void SpuMem::pulseDEBUG_TX_frm_head_jmp( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_TX,frm_head_jmp,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_TX,frm_head_jmp,idleV);
}

void SpuMem::outDEBUG_TX( Spu_reg value )
{
	out_assign_field(DEBUG_TX,payload,value);
}

void SpuMem::outDEBUG_TX()
{
	out_member(DEBUG_TX);
}


Spu_reg SpuMem::inDEBUG_TX()
{
	reg_in(DEBUG_TX);
	return DEBUG_TX.payload;
}


void SpuMem::setDEBUG_MEM_trig_tpu_coarse( Spu_reg value )
{
	reg_assign_field(DEBUG_MEM,trig_tpu_coarse,value);
}
void SpuMem::setDEBUG_MEM_mem_bram_dly( Spu_reg value )
{
	reg_assign_field(DEBUG_MEM,mem_bram_dly,value);
}
void SpuMem::setDEBUG_MEM_trig_offset_dot_pre( Spu_reg value )
{
	reg_assign_field(DEBUG_MEM,trig_offset_dot_pre,value);
}
void SpuMem::setDEBUG_MEM_trig_dly_dot_out( Spu_reg value )
{
	reg_assign_field(DEBUG_MEM,trig_dly_dot_out,value);
}
void SpuMem::setDEBUG_MEM_trig_coarse_fine( Spu_reg value )
{
	reg_assign_field(DEBUG_MEM,trig_coarse_fine,value);
}
void SpuMem::setDEBUG_MEM_mem_last_dot( Spu_reg value )
{
	reg_assign_field(DEBUG_MEM,mem_last_dot,value);
}

void SpuMem::setDEBUG_MEM( Spu_reg value )
{
	reg_assign_field(DEBUG_MEM,payload,value);
}

void SpuMem::outDEBUG_MEM_trig_tpu_coarse( Spu_reg value )
{
	out_assign_field(DEBUG_MEM,trig_tpu_coarse,value);
}
void SpuMem::pulseDEBUG_MEM_trig_tpu_coarse( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_MEM,trig_tpu_coarse,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_MEM,trig_tpu_coarse,idleV);
}
void SpuMem::outDEBUG_MEM_mem_bram_dly( Spu_reg value )
{
	out_assign_field(DEBUG_MEM,mem_bram_dly,value);
}
void SpuMem::pulseDEBUG_MEM_mem_bram_dly( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_MEM,mem_bram_dly,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_MEM,mem_bram_dly,idleV);
}
void SpuMem::outDEBUG_MEM_trig_offset_dot_pre( Spu_reg value )
{
	out_assign_field(DEBUG_MEM,trig_offset_dot_pre,value);
}
void SpuMem::pulseDEBUG_MEM_trig_offset_dot_pre( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_MEM,trig_offset_dot_pre,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_MEM,trig_offset_dot_pre,idleV);
}
void SpuMem::outDEBUG_MEM_trig_dly_dot_out( Spu_reg value )
{
	out_assign_field(DEBUG_MEM,trig_dly_dot_out,value);
}
void SpuMem::pulseDEBUG_MEM_trig_dly_dot_out( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_MEM,trig_dly_dot_out,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_MEM,trig_dly_dot_out,idleV);
}
void SpuMem::outDEBUG_MEM_trig_coarse_fine( Spu_reg value )
{
	out_assign_field(DEBUG_MEM,trig_coarse_fine,value);
}
void SpuMem::pulseDEBUG_MEM_trig_coarse_fine( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_MEM,trig_coarse_fine,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_MEM,trig_coarse_fine,idleV);
}
void SpuMem::outDEBUG_MEM_mem_last_dot( Spu_reg value )
{
	out_assign_field(DEBUG_MEM,mem_last_dot,value);
}
void SpuMem::pulseDEBUG_MEM_mem_last_dot( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_MEM,mem_last_dot,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_MEM,mem_last_dot,idleV);
}

void SpuMem::outDEBUG_MEM( Spu_reg value )
{
	out_assign_field(DEBUG_MEM,payload,value);
}

void SpuMem::outDEBUG_MEM()
{
	out_member(DEBUG_MEM);
}


Spu_reg SpuMem::inDEBUG_MEM()
{
	reg_in(DEBUG_MEM);
	return DEBUG_MEM.payload;
}


Spu_reg SpuMem::inDEBUG_SCAN()
{
	reg_in(DEBUG_SCAN);
	return DEBUG_SCAN.payload;
}


Spu_reg SpuMem::inRECORD_SUM()
{
	reg_in(RECORD_SUM);
	return RECORD_SUM.payload;
}


void SpuMem::setSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value )
{
	reg_assign_field(SCAN_ROLL_FRM,scan_roll_frm_num,value);
}

void SpuMem::setSCAN_ROLL_FRM( Spu_reg value )
{
	reg_assign_field(SCAN_ROLL_FRM,payload,value);
}

void SpuMem::outSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value )
{
	out_assign_field(SCAN_ROLL_FRM,scan_roll_frm_num,value);
}
void SpuMem::pulseSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(SCAN_ROLL_FRM,scan_roll_frm_num,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCAN_ROLL_FRM,scan_roll_frm_num,idleV);
}

void SpuMem::outSCAN_ROLL_FRM( Spu_reg value )
{
	out_assign_field(SCAN_ROLL_FRM,payload,value);
}

void SpuMem::outSCAN_ROLL_FRM()
{
	out_member(SCAN_ROLL_FRM);
}


Spu_reg SpuMem::inSCAN_ROLL_FRM()
{
	reg_in(SCAN_ROLL_FRM);
	return SCAN_ROLL_FRM.payload;
}


void SpuMem::setADC_AVG_rate( Spu_reg value )
{
	reg_assign_field(ADC_AVG,rate,value);
}
void SpuMem::setADC_AVG_done( Spu_reg value )
{
	reg_assign_field(ADC_AVG,done,value);
}
void SpuMem::setADC_AVG_en( Spu_reg value )
{
	reg_assign_field(ADC_AVG,en,value);
}

void SpuMem::setADC_AVG( Spu_reg value )
{
	reg_assign_field(ADC_AVG,payload,value);
}

void SpuMem::outADC_AVG_rate( Spu_reg value )
{
	out_assign_field(ADC_AVG,rate,value);
}
void SpuMem::pulseADC_AVG_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_AVG,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_AVG,rate,idleV);
}
void SpuMem::outADC_AVG_done( Spu_reg value )
{
	out_assign_field(ADC_AVG,done,value);
}
void SpuMem::pulseADC_AVG_done( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_AVG,done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_AVG,done,idleV);
}
void SpuMem::outADC_AVG_en( Spu_reg value )
{
	out_assign_field(ADC_AVG,en,value);
}
void SpuMem::pulseADC_AVG_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ADC_AVG,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ADC_AVG,en,idleV);
}

void SpuMem::outADC_AVG( Spu_reg value )
{
	out_assign_field(ADC_AVG,payload,value);
}

void SpuMem::outADC_AVG()
{
	out_member(ADC_AVG);
}


Spu_reg SpuMem::inADC_AVG()
{
	reg_in(ADC_AVG);
	return ADC_AVG.payload;
}


Spu_reg SpuMem::inADC_AVG_RES_AB()
{
	reg_in(ADC_AVG_RES_AB);
	return ADC_AVG_RES_AB.payload;
}


Spu_reg SpuMem::inADC_AVG_RES_CD()
{
	reg_in(ADC_AVG_RES_CD);
	return ADC_AVG_RES_CD.payload;
}


Spu_reg SpuMem::inADC_AVG_MAX()
{
	reg_in(ADC_AVG_MAX);
	return ADC_AVG_MAX.payload;
}


Spu_reg SpuMem::inADC_AVG_MIN()
{
	reg_in(ADC_AVG_MIN);
	return ADC_AVG_MIN.payload;
}


Spu_reg SpuMem::inLA_SA_RES()
{
	reg_in(LA_SA_RES);
	return LA_SA_RES.payload;
}


void SpuMem::setZONE_TRIG_AUX_pulse_width( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_AUX,pulse_width,value);
}
void SpuMem::setZONE_TRIG_AUX_inv( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_AUX,inv,value);
}
void SpuMem::setZONE_TRIG_AUX_pass_fail( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_AUX,pass_fail,value);
}

void SpuMem::setZONE_TRIG_AUX( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_AUX,payload,value);
}

void SpuMem::outZONE_TRIG_AUX_pulse_width( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_AUX,pulse_width,value);
}
void SpuMem::pulseZONE_TRIG_AUX_pulse_width( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_AUX,pulse_width,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_AUX,pulse_width,idleV);
}
void SpuMem::outZONE_TRIG_AUX_inv( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_AUX,inv,value);
}
void SpuMem::pulseZONE_TRIG_AUX_inv( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_AUX,inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_AUX,inv,idleV);
}
void SpuMem::outZONE_TRIG_AUX_pass_fail( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_AUX,pass_fail,value);
}
void SpuMem::pulseZONE_TRIG_AUX_pass_fail( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_AUX,pass_fail,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_AUX,pass_fail,idleV);
}

void SpuMem::outZONE_TRIG_AUX( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_AUX,payload,value);
}

void SpuMem::outZONE_TRIG_AUX()
{
	out_member(ZONE_TRIG_AUX);
}


Spu_reg SpuMem::inZONE_TRIG_AUX()
{
	reg_in(ZONE_TRIG_AUX);
	return ZONE_TRIG_AUX.payload;
}


void SpuMem::setZONE_TRIG_TAB1_threshold_l( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB1,threshold_l,value);
}
void SpuMem::setZONE_TRIG_TAB1_threshold_h( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB1,threshold_h,value);
}
void SpuMem::setZONE_TRIG_TAB1_threshold_en( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB1,threshold_en,value);
}
void SpuMem::setZONE_TRIG_TAB1_column_addr( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB1,column_addr,value);
}
void SpuMem::setZONE_TRIG_TAB1_tab_index( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB1,tab_index,value);
}

void SpuMem::setZONE_TRIG_TAB1( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB1,payload,value);
}

void SpuMem::outZONE_TRIG_TAB1_threshold_l( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB1,threshold_l,value);
}
void SpuMem::pulseZONE_TRIG_TAB1_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB1,threshold_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB1,threshold_l,idleV);
}
void SpuMem::outZONE_TRIG_TAB1_threshold_h( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB1,threshold_h,value);
}
void SpuMem::pulseZONE_TRIG_TAB1_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB1,threshold_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB1,threshold_h,idleV);
}
void SpuMem::outZONE_TRIG_TAB1_threshold_en( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB1,threshold_en,value);
}
void SpuMem::pulseZONE_TRIG_TAB1_threshold_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB1,threshold_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB1,threshold_en,idleV);
}
void SpuMem::outZONE_TRIG_TAB1_column_addr( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB1,column_addr,value);
}
void SpuMem::pulseZONE_TRIG_TAB1_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB1,column_addr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB1,column_addr,idleV);
}
void SpuMem::outZONE_TRIG_TAB1_tab_index( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB1,tab_index,value);
}
void SpuMem::pulseZONE_TRIG_TAB1_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB1,tab_index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB1,tab_index,idleV);
}

void SpuMem::outZONE_TRIG_TAB1( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB1,payload,value);
}

void SpuMem::outZONE_TRIG_TAB1()
{
	out_member(ZONE_TRIG_TAB1);
}


void SpuMem::setZONE_TRIG_TAB2_threshold_l( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB2,threshold_l,value);
}
void SpuMem::setZONE_TRIG_TAB2_threshold_h( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB2,threshold_h,value);
}
void SpuMem::setZONE_TRIG_TAB2_threshold_en( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB2,threshold_en,value);
}
void SpuMem::setZONE_TRIG_TAB2_column_addr( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB2,column_addr,value);
}
void SpuMem::setZONE_TRIG_TAB2_tab_index( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB2,tab_index,value);
}

void SpuMem::setZONE_TRIG_TAB2( Spu_reg value )
{
	reg_assign_field(ZONE_TRIG_TAB2,payload,value);
}

void SpuMem::outZONE_TRIG_TAB2_threshold_l( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB2,threshold_l,value);
}
void SpuMem::pulseZONE_TRIG_TAB2_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB2,threshold_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB2,threshold_l,idleV);
}
void SpuMem::outZONE_TRIG_TAB2_threshold_h( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB2,threshold_h,value);
}
void SpuMem::pulseZONE_TRIG_TAB2_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB2,threshold_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB2,threshold_h,idleV);
}
void SpuMem::outZONE_TRIG_TAB2_threshold_en( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB2,threshold_en,value);
}
void SpuMem::pulseZONE_TRIG_TAB2_threshold_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB2,threshold_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB2,threshold_en,idleV);
}
void SpuMem::outZONE_TRIG_TAB2_column_addr( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB2,column_addr,value);
}
void SpuMem::pulseZONE_TRIG_TAB2_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB2,column_addr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB2,column_addr,idleV);
}
void SpuMem::outZONE_TRIG_TAB2_tab_index( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB2,tab_index,value);
}
void SpuMem::pulseZONE_TRIG_TAB2_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZONE_TRIG_TAB2,tab_index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZONE_TRIG_TAB2,tab_index,idleV);
}

void SpuMem::outZONE_TRIG_TAB2( Spu_reg value )
{
	out_assign_field(ZONE_TRIG_TAB2,payload,value);
}

void SpuMem::outZONE_TRIG_TAB2()
{
	out_member(ZONE_TRIG_TAB2);
}


void SpuMem::setMASK_TAB_threshold_l( Spu_reg value )
{
	reg_assign_field(MASK_TAB,threshold_l,value);
}
void SpuMem::setMASK_TAB_threshold_l_less_en( Spu_reg value )
{
	reg_assign_field(MASK_TAB,threshold_l_less_en,value);
}
void SpuMem::setMASK_TAB_threshold_h( Spu_reg value )
{
	reg_assign_field(MASK_TAB,threshold_h,value);
}
void SpuMem::setMASK_TAB_threshold_h_greater_en( Spu_reg value )
{
	reg_assign_field(MASK_TAB,threshold_h_greater_en,value);
}
void SpuMem::setMASK_TAB_column_addr( Spu_reg value )
{
	reg_assign_field(MASK_TAB,column_addr,value);
}
void SpuMem::setMASK_TAB_tab_index( Spu_reg value )
{
	reg_assign_field(MASK_TAB,tab_index,value);
}

void SpuMem::setMASK_TAB( Spu_reg value )
{
	reg_assign_field(MASK_TAB,payload,value);
}

void SpuMem::outMASK_TAB_threshold_l( Spu_reg value )
{
	out_assign_field(MASK_TAB,threshold_l,value);
}
void SpuMem::pulseMASK_TAB_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB,threshold_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB,threshold_l,idleV);
}
void SpuMem::outMASK_TAB_threshold_l_less_en( Spu_reg value )
{
	out_assign_field(MASK_TAB,threshold_l_less_en,value);
}
void SpuMem::pulseMASK_TAB_threshold_l_less_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB,threshold_l_less_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB,threshold_l_less_en,idleV);
}
void SpuMem::outMASK_TAB_threshold_h( Spu_reg value )
{
	out_assign_field(MASK_TAB,threshold_h,value);
}
void SpuMem::pulseMASK_TAB_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB,threshold_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB,threshold_h,idleV);
}
void SpuMem::outMASK_TAB_threshold_h_greater_en( Spu_reg value )
{
	out_assign_field(MASK_TAB,threshold_h_greater_en,value);
}
void SpuMem::pulseMASK_TAB_threshold_h_greater_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB,threshold_h_greater_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB,threshold_h_greater_en,idleV);
}
void SpuMem::outMASK_TAB_column_addr( Spu_reg value )
{
	out_assign_field(MASK_TAB,column_addr,value);
}
void SpuMem::pulseMASK_TAB_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB,column_addr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB,column_addr,idleV);
}
void SpuMem::outMASK_TAB_tab_index( Spu_reg value )
{
	out_assign_field(MASK_TAB,tab_index,value);
}
void SpuMem::pulseMASK_TAB_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB,tab_index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB,tab_index,idleV);
}

void SpuMem::outMASK_TAB( Spu_reg value )
{
	out_assign_field(MASK_TAB,payload,value);
}

void SpuMem::outMASK_TAB()
{
	out_member(MASK_TAB);
}


Spu_reg SpuMem::inMASK_TAB()
{
	reg_in(MASK_TAB);
	return MASK_TAB.payload;
}


void SpuMem::setMASK_TAB_EX_threshold_l( Spu_reg value )
{
	reg_assign_field(MASK_TAB_EX,threshold_l,value);
}
void SpuMem::setMASK_TAB_EX_threshold_l_greater_en( Spu_reg value )
{
	reg_assign_field(MASK_TAB_EX,threshold_l_greater_en,value);
}
void SpuMem::setMASK_TAB_EX_threshold_h( Spu_reg value )
{
	reg_assign_field(MASK_TAB_EX,threshold_h,value);
}
void SpuMem::setMASK_TAB_EX_threshold_h_less_en( Spu_reg value )
{
	reg_assign_field(MASK_TAB_EX,threshold_h_less_en,value);
}
void SpuMem::setMASK_TAB_EX_column_addr( Spu_reg value )
{
	reg_assign_field(MASK_TAB_EX,column_addr,value);
}
void SpuMem::setMASK_TAB_EX_tab_index( Spu_reg value )
{
	reg_assign_field(MASK_TAB_EX,tab_index,value);
}

void SpuMem::setMASK_TAB_EX( Spu_reg value )
{
	reg_assign_field(MASK_TAB_EX,payload,value);
}

void SpuMem::outMASK_TAB_EX_threshold_l( Spu_reg value )
{
	out_assign_field(MASK_TAB_EX,threshold_l,value);
}
void SpuMem::pulseMASK_TAB_EX_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB_EX,threshold_l,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB_EX,threshold_l,idleV);
}
void SpuMem::outMASK_TAB_EX_threshold_l_greater_en( Spu_reg value )
{
	out_assign_field(MASK_TAB_EX,threshold_l_greater_en,value);
}
void SpuMem::pulseMASK_TAB_EX_threshold_l_greater_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB_EX,threshold_l_greater_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB_EX,threshold_l_greater_en,idleV);
}
void SpuMem::outMASK_TAB_EX_threshold_h( Spu_reg value )
{
	out_assign_field(MASK_TAB_EX,threshold_h,value);
}
void SpuMem::pulseMASK_TAB_EX_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB_EX,threshold_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB_EX,threshold_h,idleV);
}
void SpuMem::outMASK_TAB_EX_threshold_h_less_en( Spu_reg value )
{
	out_assign_field(MASK_TAB_EX,threshold_h_less_en,value);
}
void SpuMem::pulseMASK_TAB_EX_threshold_h_less_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB_EX,threshold_h_less_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB_EX,threshold_h_less_en,idleV);
}
void SpuMem::outMASK_TAB_EX_column_addr( Spu_reg value )
{
	out_assign_field(MASK_TAB_EX,column_addr,value);
}
void SpuMem::pulseMASK_TAB_EX_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB_EX,column_addr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB_EX,column_addr,idleV);
}
void SpuMem::outMASK_TAB_EX_tab_index( Spu_reg value )
{
	out_assign_field(MASK_TAB_EX,tab_index,value);
}
void SpuMem::pulseMASK_TAB_EX_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_TAB_EX,tab_index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TAB_EX,tab_index,idleV);
}

void SpuMem::outMASK_TAB_EX( Spu_reg value )
{
	out_assign_field(MASK_TAB_EX,payload,value);
}

void SpuMem::outMASK_TAB_EX()
{
	out_member(MASK_TAB_EX);
}


Spu_reg SpuMem::inMASK_TAB_EX()
{
	reg_in(MASK_TAB_EX);
	return MASK_TAB_EX.payload;
}


void SpuMem::setMASK_COMPRESS_rate( Spu_reg value )
{
	reg_assign_field(MASK_COMPRESS,rate,value);
}
void SpuMem::setMASK_COMPRESS_peak( Spu_reg value )
{
	reg_assign_field(MASK_COMPRESS,peak,value);
}
void SpuMem::setMASK_COMPRESS_en( Spu_reg value )
{
	reg_assign_field(MASK_COMPRESS,en,value);
}

void SpuMem::setMASK_COMPRESS( Spu_reg value )
{
	reg_assign_field(MASK_COMPRESS,payload,value);
}

void SpuMem::outMASK_COMPRESS_rate( Spu_reg value )
{
	out_assign_field(MASK_COMPRESS,rate,value);
}
void SpuMem::pulseMASK_COMPRESS_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_COMPRESS,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_COMPRESS,rate,idleV);
}
void SpuMem::outMASK_COMPRESS_peak( Spu_reg value )
{
	out_assign_field(MASK_COMPRESS,peak,value);
}
void SpuMem::pulseMASK_COMPRESS_peak( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_COMPRESS,peak,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_COMPRESS,peak,idleV);
}
void SpuMem::outMASK_COMPRESS_en( Spu_reg value )
{
	out_assign_field(MASK_COMPRESS,en,value);
}
void SpuMem::pulseMASK_COMPRESS_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_COMPRESS,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_COMPRESS,en,idleV);
}

void SpuMem::outMASK_COMPRESS( Spu_reg value )
{
	out_assign_field(MASK_COMPRESS,payload,value);
}

void SpuMem::outMASK_COMPRESS()
{
	out_member(MASK_COMPRESS);
}


Spu_reg SpuMem::inMASK_COMPRESS()
{
	reg_in(MASK_COMPRESS);
	return MASK_COMPRESS.payload;
}


void SpuMem::setMASK_CTRL_cross_index( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,cross_index,value);
}
void SpuMem::setMASK_CTRL_mask_index_en( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,mask_index_en,value);
}
void SpuMem::setMASK_CTRL_zone1_trig_tab_en( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,zone1_trig_tab_en,value);
}
void SpuMem::setMASK_CTRL_zone2_trig_tab_en( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,zone2_trig_tab_en,value);
}
void SpuMem::setMASK_CTRL_zone1_trig_cross( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,zone1_trig_cross,value);
}
void SpuMem::setMASK_CTRL_zone2_trig_cross( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,zone2_trig_cross,value);
}
void SpuMem::setMASK_CTRL_mask_result_rd( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,mask_result_rd,value);
}
void SpuMem::setMASK_CTRL_sum_clr( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,sum_clr,value);
}
void SpuMem::setMASK_CTRL_cross_clr( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,cross_clr,value);
}
void SpuMem::setMASK_CTRL_mask_we( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,mask_we,value);
}

void SpuMem::setMASK_CTRL( Spu_reg value )
{
	reg_assign_field(MASK_CTRL,payload,value);
}

void SpuMem::outMASK_CTRL_cross_index( Spu_reg value )
{
	out_assign_field(MASK_CTRL,cross_index,value);
}
void SpuMem::pulseMASK_CTRL_cross_index( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,cross_index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,cross_index,idleV);
}
void SpuMem::outMASK_CTRL_mask_index_en( Spu_reg value )
{
	out_assign_field(MASK_CTRL,mask_index_en,value);
}
void SpuMem::pulseMASK_CTRL_mask_index_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,mask_index_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,mask_index_en,idleV);
}
void SpuMem::outMASK_CTRL_zone1_trig_tab_en( Spu_reg value )
{
	out_assign_field(MASK_CTRL,zone1_trig_tab_en,value);
}
void SpuMem::pulseMASK_CTRL_zone1_trig_tab_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,zone1_trig_tab_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,zone1_trig_tab_en,idleV);
}
void SpuMem::outMASK_CTRL_zone2_trig_tab_en( Spu_reg value )
{
	out_assign_field(MASK_CTRL,zone2_trig_tab_en,value);
}
void SpuMem::pulseMASK_CTRL_zone2_trig_tab_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,zone2_trig_tab_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,zone2_trig_tab_en,idleV);
}
void SpuMem::outMASK_CTRL_zone1_trig_cross( Spu_reg value )
{
	out_assign_field(MASK_CTRL,zone1_trig_cross,value);
}
void SpuMem::pulseMASK_CTRL_zone1_trig_cross( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,zone1_trig_cross,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,zone1_trig_cross,idleV);
}
void SpuMem::outMASK_CTRL_zone2_trig_cross( Spu_reg value )
{
	out_assign_field(MASK_CTRL,zone2_trig_cross,value);
}
void SpuMem::pulseMASK_CTRL_zone2_trig_cross( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,zone2_trig_cross,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,zone2_trig_cross,idleV);
}
void SpuMem::outMASK_CTRL_mask_result_rd( Spu_reg value )
{
	out_assign_field(MASK_CTRL,mask_result_rd,value);
}
void SpuMem::pulseMASK_CTRL_mask_result_rd( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,mask_result_rd,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,mask_result_rd,idleV);
}
void SpuMem::outMASK_CTRL_sum_clr( Spu_reg value )
{
	out_assign_field(MASK_CTRL,sum_clr,value);
}
void SpuMem::pulseMASK_CTRL_sum_clr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,sum_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,sum_clr,idleV);
}
void SpuMem::outMASK_CTRL_cross_clr( Spu_reg value )
{
	out_assign_field(MASK_CTRL,cross_clr,value);
}
void SpuMem::pulseMASK_CTRL_cross_clr( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,cross_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,cross_clr,idleV);
}
void SpuMem::outMASK_CTRL_mask_we( Spu_reg value )
{
	out_assign_field(MASK_CTRL,mask_we,value);
}
void SpuMem::pulseMASK_CTRL_mask_we( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_CTRL,mask_we,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_CTRL,mask_we,idleV);
}

void SpuMem::outMASK_CTRL( Spu_reg value )
{
	out_assign_field(MASK_CTRL,payload,value);
}

void SpuMem::outMASK_CTRL()
{
	out_member(MASK_CTRL);
}


Spu_reg SpuMem::inMASK_CTRL()
{
	reg_in(MASK_CTRL);
	return MASK_CTRL.payload;
}


Spu_reg SpuMem::inMASK_FRM_CNT_L()
{
	reg_in(MASK_FRM_CNT_L);
	return MASK_FRM_CNT_L;
}


Spu_reg SpuMem::inMASK_FRM_CNT_H()
{
	reg_in(MASK_FRM_CNT_H);
	return MASK_FRM_CNT_H;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_L_CHA()
{
	reg_in(MASK_CROSS_CNT_L_CHA);
	return MASK_CROSS_CNT_L_CHA;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_H_CHA()
{
	reg_in(MASK_CROSS_CNT_H_CHA);
	return MASK_CROSS_CNT_H_CHA;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_L_CHB()
{
	reg_in(MASK_CROSS_CNT_L_CHB);
	return MASK_CROSS_CNT_L_CHB;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_H_CHB()
{
	reg_in(MASK_CROSS_CNT_H_CHB);
	return MASK_CROSS_CNT_H_CHB;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_L_CHC()
{
	reg_in(MASK_CROSS_CNT_L_CHC);
	return MASK_CROSS_CNT_L_CHC;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_H_CHC()
{
	reg_in(MASK_CROSS_CNT_H_CHC);
	return MASK_CROSS_CNT_H_CHC;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_L_CHD()
{
	reg_in(MASK_CROSS_CNT_L_CHD);
	return MASK_CROSS_CNT_L_CHD;
}


Spu_reg SpuMem::inMASK_CROSS_CNT_H_CHD()
{
	reg_in(MASK_CROSS_CNT_H_CHD);
	return MASK_CROSS_CNT_H_CHD;
}


void SpuMem::setMASK_OUTPUT_pulse_width( Spu_reg value )
{
	reg_assign_field(MASK_OUTPUT,pulse_width,value);
}
void SpuMem::setMASK_OUTPUT_inv( Spu_reg value )
{
	reg_assign_field(MASK_OUTPUT,inv,value);
}
void SpuMem::setMASK_OUTPUT_pass_fail( Spu_reg value )
{
	reg_assign_field(MASK_OUTPUT,pass_fail,value);
}

void SpuMem::setMASK_OUTPUT( Spu_reg value )
{
	reg_assign_field(MASK_OUTPUT,payload,value);
}

void SpuMem::outMASK_OUTPUT_pulse_width( Spu_reg value )
{
	out_assign_field(MASK_OUTPUT,pulse_width,value);
}
void SpuMem::pulseMASK_OUTPUT_pulse_width( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_OUTPUT,pulse_width,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_OUTPUT,pulse_width,idleV);
}
void SpuMem::outMASK_OUTPUT_inv( Spu_reg value )
{
	out_assign_field(MASK_OUTPUT,inv,value);
}
void SpuMem::pulseMASK_OUTPUT_inv( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_OUTPUT,inv,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_OUTPUT,inv,idleV);
}
void SpuMem::outMASK_OUTPUT_pass_fail( Spu_reg value )
{
	out_assign_field(MASK_OUTPUT,pass_fail,value);
}
void SpuMem::pulseMASK_OUTPUT_pass_fail( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(MASK_OUTPUT,pass_fail,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_OUTPUT,pass_fail,idleV);
}

void SpuMem::outMASK_OUTPUT( Spu_reg value )
{
	out_assign_field(MASK_OUTPUT,payload,value);
}

void SpuMem::outMASK_OUTPUT()
{
	out_member(MASK_OUTPUT);
}


Spu_reg SpuMem::inMASK_OUTPUT()
{
	reg_in(MASK_OUTPUT);
	return MASK_OUTPUT.payload;
}


void SpuMem::setSEARCH_OFFSET_CH( Spu_reg value )
{
	reg_assign(SEARCH_OFFSET_CH,value);
}

void SpuMem::outSEARCH_OFFSET_CH( Spu_reg value )
{
	out_assign(SEARCH_OFFSET_CH,value);
}

void SpuMem::outSEARCH_OFFSET_CH()
{
	out_member(SEARCH_OFFSET_CH);
}


Spu_reg SpuMem::inSEARCH_OFFSET_CH()
{
	reg_in(SEARCH_OFFSET_CH);
	return SEARCH_OFFSET_CH;
}


void SpuMem::setSEARCH_LEN_CH( Spu_reg value )
{
	reg_assign(SEARCH_LEN_CH,value);
}

void SpuMem::outSEARCH_LEN_CH( Spu_reg value )
{
	out_assign(SEARCH_LEN_CH,value);
}

void SpuMem::outSEARCH_LEN_CH()
{
	out_member(SEARCH_LEN_CH);
}


Spu_reg SpuMem::inSEARCH_LEN_CH()
{
	reg_in(SEARCH_LEN_CH);
	return SEARCH_LEN_CH;
}


void SpuMem::setSEARCH_OFFSET_LA( Spu_reg value )
{
	reg_assign(SEARCH_OFFSET_LA,value);
}

void SpuMem::outSEARCH_OFFSET_LA( Spu_reg value )
{
	out_assign(SEARCH_OFFSET_LA,value);
}

void SpuMem::outSEARCH_OFFSET_LA()
{
	out_member(SEARCH_OFFSET_LA);
}


Spu_reg SpuMem::inSEARCH_OFFSET_LA()
{
	reg_in(SEARCH_OFFSET_LA);
	return SEARCH_OFFSET_LA;
}


void SpuMem::setSEARCH_LEN_LA( Spu_reg value )
{
	reg_assign(SEARCH_LEN_LA,value);
}

void SpuMem::outSEARCH_LEN_LA( Spu_reg value )
{
	out_assign(SEARCH_LEN_LA,value);
}

void SpuMem::outSEARCH_LEN_LA()
{
	out_member(SEARCH_LEN_LA);
}


Spu_reg SpuMem::inSEARCH_LEN_LA()
{
	reg_in(SEARCH_LEN_LA);
	return SEARCH_LEN_LA;
}


void SpuMem::setTIME_TAG_L( Spu_reg value )
{
	reg_assign(TIME_TAG_L,value);
}

void SpuMem::outTIME_TAG_L( Spu_reg value )
{
	out_assign(TIME_TAG_L,value);
}

void SpuMem::outTIME_TAG_L()
{
	out_member(TIME_TAG_L);
}


Spu_reg SpuMem::inTIME_TAG_L()
{
	reg_in(TIME_TAG_L);
	return TIME_TAG_L;
}


void SpuMem::setTIME_TAG_H( Spu_reg value )
{
	reg_assign(TIME_TAG_H,value);
}

void SpuMem::outTIME_TAG_H( Spu_reg value )
{
	out_assign(TIME_TAG_H,value);
}

void SpuMem::outTIME_TAG_H()
{
	out_member(TIME_TAG_H);
}


Spu_reg SpuMem::inTIME_TAG_H()
{
	reg_in(TIME_TAG_H);
	return TIME_TAG_H;
}


void SpuMem::setFINE_TRIG_POSITION( Spu_reg value )
{
	reg_assign(FINE_TRIG_POSITION,value);
}

void SpuMem::outFINE_TRIG_POSITION( Spu_reg value )
{
	out_assign(FINE_TRIG_POSITION,value);
}

void SpuMem::outFINE_TRIG_POSITION()
{
	out_member(FINE_TRIG_POSITION);
}


Spu_reg SpuMem::inFINE_TRIG_POSITION()
{
	reg_in(FINE_TRIG_POSITION);
	return FINE_TRIG_POSITION;
}


void SpuMem::setTIME_TAG_CTRL_rd_en( Spu_reg value )
{
	reg_assign_field(TIME_TAG_CTRL,rd_en,value);
}
void SpuMem::setTIME_TAG_CTRL_rec_first_done( Spu_reg value )
{
	reg_assign_field(TIME_TAG_CTRL,rec_first_done,value);
}

void SpuMem::setTIME_TAG_CTRL( Spu_reg value )
{
	reg_assign_field(TIME_TAG_CTRL,payload,value);
}

void SpuMem::outTIME_TAG_CTRL_rd_en( Spu_reg value )
{
	out_assign_field(TIME_TAG_CTRL,rd_en,value);
}
void SpuMem::pulseTIME_TAG_CTRL_rd_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TIME_TAG_CTRL,rd_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TIME_TAG_CTRL,rd_en,idleV);
}
void SpuMem::outTIME_TAG_CTRL_rec_first_done( Spu_reg value )
{
	out_assign_field(TIME_TAG_CTRL,rec_first_done,value);
}
void SpuMem::pulseTIME_TAG_CTRL_rec_first_done( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TIME_TAG_CTRL,rec_first_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TIME_TAG_CTRL,rec_first_done,idleV);
}

void SpuMem::outTIME_TAG_CTRL( Spu_reg value )
{
	out_assign_field(TIME_TAG_CTRL,payload,value);
}

void SpuMem::outTIME_TAG_CTRL()
{
	out_member(TIME_TAG_CTRL);
}


Spu_reg SpuMem::inTIME_TAG_CTRL()
{
	reg_in(TIME_TAG_CTRL);
	return TIME_TAG_CTRL.payload;
}


void SpuMem::setIMPORT_INFO_import_done( Spu_reg value )
{
	reg_assign_field(IMPORT_INFO,import_done,value);
}

void SpuMem::setIMPORT_INFO( Spu_reg value )
{
	reg_assign_field(IMPORT_INFO,payload,value);
}

void SpuMem::outIMPORT_INFO_import_done( Spu_reg value )
{
	out_assign_field(IMPORT_INFO,import_done,value);
}
void SpuMem::pulseIMPORT_INFO_import_done( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(IMPORT_INFO,import_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(IMPORT_INFO,import_done,idleV);
}

void SpuMem::outIMPORT_INFO( Spu_reg value )
{
	out_assign_field(IMPORT_INFO,payload,value);
}

void SpuMem::outIMPORT_INFO()
{
	out_member(IMPORT_INFO);
}


Spu_reg SpuMem::inIMPORT_INFO()
{
	reg_in(IMPORT_INFO);
	return IMPORT_INFO.payload;
}


Spu_reg SpuMem::inTIME_TAG_L_REC_FIRST()
{
	reg_in(TIME_TAG_L_REC_FIRST);
	return TIME_TAG_L_REC_FIRST;
}


Spu_reg SpuMem::inTIME_TAG_H_REC_FIRST()
{
	reg_in(TIME_TAG_H_REC_FIRST);
	return TIME_TAG_H_REC_FIRST;
}


void SpuMem::setTRACE_AVG_RATE_X( Spu_reg value )
{
	reg_assign(TRACE_AVG_RATE_X,value);
}

void SpuMem::outTRACE_AVG_RATE_X( Spu_reg value )
{
	out_assign(TRACE_AVG_RATE_X,value);
}

void SpuMem::outTRACE_AVG_RATE_X()
{
	out_member(TRACE_AVG_RATE_X);
}


Spu_reg SpuMem::inTRACE_AVG_RATE_X()
{
	reg_in(TRACE_AVG_RATE_X);
	return TRACE_AVG_RATE_X;
}


void SpuMem::setTRACE_AVG_RATE_Y( Spu_reg value )
{
	reg_assign(TRACE_AVG_RATE_Y,value);
}

void SpuMem::outTRACE_AVG_RATE_Y( Spu_reg value )
{
	out_assign(TRACE_AVG_RATE_Y,value);
}

void SpuMem::outTRACE_AVG_RATE_Y()
{
	out_member(TRACE_AVG_RATE_Y);
}


Spu_reg SpuMem::inTRACE_AVG_RATE_Y()
{
	reg_in(TRACE_AVG_RATE_Y);
	return TRACE_AVG_RATE_Y;
}


void SpuMem::setLINEAR_INTX_CTRL_mian_bypass( Spu_reg value )
{
	reg_assign_field(LINEAR_INTX_CTRL,mian_bypass,value);
}
void SpuMem::setLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value )
{
	reg_assign_field(LINEAR_INTX_CTRL,zoom_bypass,value);
}

void SpuMem::setLINEAR_INTX_CTRL( Spu_reg value )
{
	reg_assign_field(LINEAR_INTX_CTRL,payload,value);
}

void SpuMem::outLINEAR_INTX_CTRL_mian_bypass( Spu_reg value )
{
	out_assign_field(LINEAR_INTX_CTRL,mian_bypass,value);
}
void SpuMem::pulseLINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LINEAR_INTX_CTRL,mian_bypass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LINEAR_INTX_CTRL,mian_bypass,idleV);
}
void SpuMem::outLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value )
{
	out_assign_field(LINEAR_INTX_CTRL,zoom_bypass,value);
}
void SpuMem::pulseLINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LINEAR_INTX_CTRL,zoom_bypass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LINEAR_INTX_CTRL,zoom_bypass,idleV);
}

void SpuMem::outLINEAR_INTX_CTRL( Spu_reg value )
{
	out_assign_field(LINEAR_INTX_CTRL,payload,value);
}

void SpuMem::outLINEAR_INTX_CTRL()
{
	out_member(LINEAR_INTX_CTRL);
}


Spu_reg SpuMem::inLINEAR_INTX_CTRL()
{
	reg_in(LINEAR_INTX_CTRL);
	return LINEAR_INTX_CTRL.payload;
}


void SpuMem::setLINEAR_INTX_INIT( Spu_reg value )
{
	reg_assign(LINEAR_INTX_INIT,value);
}

void SpuMem::outLINEAR_INTX_INIT( Spu_reg value )
{
	out_assign(LINEAR_INTX_INIT,value);
}

void SpuMem::outLINEAR_INTX_INIT()
{
	out_member(LINEAR_INTX_INIT);
}


Spu_reg SpuMem::inLINEAR_INTX_INIT()
{
	reg_in(LINEAR_INTX_INIT);
	return LINEAR_INTX_INIT;
}


void SpuMem::setLINEAR_INTX_STEP( Spu_reg value )
{
	reg_assign(LINEAR_INTX_STEP,value);
}

void SpuMem::outLINEAR_INTX_STEP( Spu_reg value )
{
	out_assign(LINEAR_INTX_STEP,value);
}

void SpuMem::outLINEAR_INTX_STEP()
{
	out_member(LINEAR_INTX_STEP);
}


Spu_reg SpuMem::inLINEAR_INTX_STEP()
{
	reg_in(LINEAR_INTX_STEP);
	return LINEAR_INTX_STEP;
}


void SpuMem::setLINEAR_INTX_INIT_ZOOM( Spu_reg value )
{
	reg_assign(LINEAR_INTX_INIT_ZOOM,value);
}

void SpuMem::outLINEAR_INTX_INIT_ZOOM( Spu_reg value )
{
	out_assign(LINEAR_INTX_INIT_ZOOM,value);
}

void SpuMem::outLINEAR_INTX_INIT_ZOOM()
{
	out_member(LINEAR_INTX_INIT_ZOOM);
}


Spu_reg SpuMem::inLINEAR_INTX_INIT_ZOOM()
{
	reg_in(LINEAR_INTX_INIT_ZOOM);
	return LINEAR_INTX_INIT_ZOOM;
}


void SpuMem::setLINEAR_INTX_STEP_ZOOM( Spu_reg value )
{
	reg_assign(LINEAR_INTX_STEP_ZOOM,value);
}

void SpuMem::outLINEAR_INTX_STEP_ZOOM( Spu_reg value )
{
	out_assign(LINEAR_INTX_STEP_ZOOM,value);
}

void SpuMem::outLINEAR_INTX_STEP_ZOOM()
{
	out_member(LINEAR_INTX_STEP_ZOOM);
}


Spu_reg SpuMem::inLINEAR_INTX_STEP_ZOOM()
{
	reg_in(LINEAR_INTX_STEP_ZOOM);
	return LINEAR_INTX_STEP_ZOOM;
}


void SpuMem::setWAVE_OFFSET_EYE( Spu_reg value )
{
	reg_assign(WAVE_OFFSET_EYE,value);
}

void SpuMem::outWAVE_OFFSET_EYE( Spu_reg value )
{
	out_assign(WAVE_OFFSET_EYE,value);
}

void SpuMem::outWAVE_OFFSET_EYE()
{
	out_member(WAVE_OFFSET_EYE);
}


Spu_reg SpuMem::inWAVE_OFFSET_EYE()
{
	reg_in(WAVE_OFFSET_EYE);
	return WAVE_OFFSET_EYE;
}


void SpuMem::setWAVE_LEN_EYE( Spu_reg value )
{
	reg_assign(WAVE_LEN_EYE,value);
}

void SpuMem::outWAVE_LEN_EYE( Spu_reg value )
{
	out_assign(WAVE_LEN_EYE,value);
}

void SpuMem::outWAVE_LEN_EYE()
{
	out_member(WAVE_LEN_EYE);
}


Spu_reg SpuMem::inWAVE_LEN_EYE()
{
	reg_in(WAVE_LEN_EYE);
	return WAVE_LEN_EYE;
}


void SpuMem::setCOMPRESS_EYE_rate( Spu_reg value )
{
	reg_assign_field(COMPRESS_EYE,rate,value);
}
void SpuMem::setCOMPRESS_EYE_peak( Spu_reg value )
{
	reg_assign_field(COMPRESS_EYE,peak,value);
}
void SpuMem::setCOMPRESS_EYE_en( Spu_reg value )
{
	reg_assign_field(COMPRESS_EYE,en,value);
}

void SpuMem::setCOMPRESS_EYE( Spu_reg value )
{
	reg_assign_field(COMPRESS_EYE,payload,value);
}

void SpuMem::outCOMPRESS_EYE_rate( Spu_reg value )
{
	out_assign_field(COMPRESS_EYE,rate,value);
}
void SpuMem::pulseCOMPRESS_EYE_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_EYE,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_EYE,rate,idleV);
}
void SpuMem::outCOMPRESS_EYE_peak( Spu_reg value )
{
	out_assign_field(COMPRESS_EYE,peak,value);
}
void SpuMem::pulseCOMPRESS_EYE_peak( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_EYE,peak,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_EYE,peak,idleV);
}
void SpuMem::outCOMPRESS_EYE_en( Spu_reg value )
{
	out_assign_field(COMPRESS_EYE,en,value);
}
void SpuMem::pulseCOMPRESS_EYE_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(COMPRESS_EYE,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(COMPRESS_EYE,en,idleV);
}

void SpuMem::outCOMPRESS_EYE( Spu_reg value )
{
	out_assign_field(COMPRESS_EYE,payload,value);
}

void SpuMem::outCOMPRESS_EYE()
{
	out_member(COMPRESS_EYE);
}


Spu_reg SpuMem::inCOMPRESS_EYE()
{
	reg_in(COMPRESS_EYE);
	return COMPRESS_EYE.payload;
}


void SpuMem::setTX_LEN_EYE( Spu_reg value )
{
	reg_assign(TX_LEN_EYE,value);
}

void SpuMem::outTX_LEN_EYE( Spu_reg value )
{
	out_assign(TX_LEN_EYE,value);
}

void SpuMem::outTX_LEN_EYE()
{
	out_member(TX_LEN_EYE);
}


Spu_reg SpuMem::inTX_LEN_EYE()
{
	reg_in(TX_LEN_EYE);
	return TX_LEN_EYE;
}


void SpuMem::setLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value )
{
	reg_assign_field(LA_LINEAR_INTX_CTRL,mian_bypass,value);
}
void SpuMem::setLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value )
{
	reg_assign_field(LA_LINEAR_INTX_CTRL,zoom_bypass,value);
}

void SpuMem::setLA_LINEAR_INTX_CTRL( Spu_reg value )
{
	reg_assign_field(LA_LINEAR_INTX_CTRL,payload,value);
}

void SpuMem::outLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value )
{
	out_assign_field(LA_LINEAR_INTX_CTRL,mian_bypass,value);
}
void SpuMem::pulseLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_LINEAR_INTX_CTRL,mian_bypass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_LINEAR_INTX_CTRL,mian_bypass,idleV);
}
void SpuMem::outLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value )
{
	out_assign_field(LA_LINEAR_INTX_CTRL,zoom_bypass,value);
}
void SpuMem::pulseLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(LA_LINEAR_INTX_CTRL,zoom_bypass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(LA_LINEAR_INTX_CTRL,zoom_bypass,idleV);
}

void SpuMem::outLA_LINEAR_INTX_CTRL( Spu_reg value )
{
	out_assign_field(LA_LINEAR_INTX_CTRL,payload,value);
}

void SpuMem::outLA_LINEAR_INTX_CTRL()
{
	out_member(LA_LINEAR_INTX_CTRL);
}


Spu_reg SpuMem::inLA_LINEAR_INTX_CTRL()
{
	reg_in(LA_LINEAR_INTX_CTRL);
	return LA_LINEAR_INTX_CTRL.payload;
}


void SpuMem::setLA_LINEAR_INTX_INIT( Spu_reg value )
{
	reg_assign(LA_LINEAR_INTX_INIT,value);
}

void SpuMem::outLA_LINEAR_INTX_INIT( Spu_reg value )
{
	out_assign(LA_LINEAR_INTX_INIT,value);
}

void SpuMem::outLA_LINEAR_INTX_INIT()
{
	out_member(LA_LINEAR_INTX_INIT);
}


Spu_reg SpuMem::inLA_LINEAR_INTX_INIT()
{
	reg_in(LA_LINEAR_INTX_INIT);
	return LA_LINEAR_INTX_INIT;
}


void SpuMem::setLA_LINEAR_INTX_STEP( Spu_reg value )
{
	reg_assign(LA_LINEAR_INTX_STEP,value);
}

void SpuMem::outLA_LINEAR_INTX_STEP( Spu_reg value )
{
	out_assign(LA_LINEAR_INTX_STEP,value);
}

void SpuMem::outLA_LINEAR_INTX_STEP()
{
	out_member(LA_LINEAR_INTX_STEP);
}


Spu_reg SpuMem::inLA_LINEAR_INTX_STEP()
{
	reg_in(LA_LINEAR_INTX_STEP);
	return LA_LINEAR_INTX_STEP;
}


void SpuMem::setLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value )
{
	reg_assign(LA_LINEAR_INTX_INIT_ZOOM,value);
}

void SpuMem::outLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value )
{
	out_assign(LA_LINEAR_INTX_INIT_ZOOM,value);
}

void SpuMem::outLA_LINEAR_INTX_INIT_ZOOM()
{
	out_member(LA_LINEAR_INTX_INIT_ZOOM);
}


Spu_reg SpuMem::inLA_LINEAR_INTX_INIT_ZOOM()
{
	reg_in(LA_LINEAR_INTX_INIT_ZOOM);
	return LA_LINEAR_INTX_INIT_ZOOM;
}


void SpuMem::setLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value )
{
	reg_assign(LA_LINEAR_INTX_STEP_ZOOM,value);
}

void SpuMem::outLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value )
{
	out_assign(LA_LINEAR_INTX_STEP_ZOOM,value);
}

void SpuMem::outLA_LINEAR_INTX_STEP_ZOOM()
{
	out_member(LA_LINEAR_INTX_STEP_ZOOM);
}


Spu_reg SpuMem::inLA_LINEAR_INTX_STEP_ZOOM()
{
	reg_in(LA_LINEAR_INTX_STEP_ZOOM);
	return LA_LINEAR_INTX_STEP_ZOOM;
}


void SpuMem::setTRACE_COMPRESS_MAIN_rate( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_MAIN,rate,value);
}
void SpuMem::setTRACE_COMPRESS_MAIN_peak( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_MAIN,peak,value);
}
void SpuMem::setTRACE_COMPRESS_MAIN_en( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_MAIN,en,value);
}

void SpuMem::setTRACE_COMPRESS_MAIN( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_MAIN,payload,value);
}

void SpuMem::outTRACE_COMPRESS_MAIN_rate( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_MAIN,rate,value);
}
void SpuMem::pulseTRACE_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TRACE_COMPRESS_MAIN,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE_COMPRESS_MAIN,rate,idleV);
}
void SpuMem::outTRACE_COMPRESS_MAIN_peak( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_MAIN,peak,value);
}
void SpuMem::pulseTRACE_COMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TRACE_COMPRESS_MAIN,peak,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE_COMPRESS_MAIN,peak,idleV);
}
void SpuMem::outTRACE_COMPRESS_MAIN_en( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_MAIN,en,value);
}
void SpuMem::pulseTRACE_COMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TRACE_COMPRESS_MAIN,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE_COMPRESS_MAIN,en,idleV);
}

void SpuMem::outTRACE_COMPRESS_MAIN( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_MAIN,payload,value);
}

void SpuMem::outTRACE_COMPRESS_MAIN()
{
	out_member(TRACE_COMPRESS_MAIN);
}


Spu_reg SpuMem::inTRACE_COMPRESS_MAIN()
{
	reg_in(TRACE_COMPRESS_MAIN);
	return TRACE_COMPRESS_MAIN.payload;
}


void SpuMem::setTRACE_COMPRESS_ZOOM_rate( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_ZOOM,rate,value);
}
void SpuMem::setTRACE_COMPRESS_ZOOM_peak( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_ZOOM,peak,value);
}
void SpuMem::setTRACE_COMPRESS_ZOOM_en( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_ZOOM,en,value);
}

void SpuMem::setTRACE_COMPRESS_ZOOM( Spu_reg value )
{
	reg_assign_field(TRACE_COMPRESS_ZOOM,payload,value);
}

void SpuMem::outTRACE_COMPRESS_ZOOM_rate( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_ZOOM,rate,value);
}
void SpuMem::pulseTRACE_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TRACE_COMPRESS_ZOOM,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE_COMPRESS_ZOOM,rate,idleV);
}
void SpuMem::outTRACE_COMPRESS_ZOOM_peak( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_ZOOM,peak,value);
}
void SpuMem::pulseTRACE_COMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TRACE_COMPRESS_ZOOM,peak,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE_COMPRESS_ZOOM,peak,idleV);
}
void SpuMem::outTRACE_COMPRESS_ZOOM_en( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_ZOOM,en,value);
}
void SpuMem::pulseTRACE_COMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(TRACE_COMPRESS_ZOOM,en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE_COMPRESS_ZOOM,en,idleV);
}

void SpuMem::outTRACE_COMPRESS_ZOOM( Spu_reg value )
{
	out_assign_field(TRACE_COMPRESS_ZOOM,payload,value);
}

void SpuMem::outTRACE_COMPRESS_ZOOM()
{
	out_member(TRACE_COMPRESS_ZOOM);
}


Spu_reg SpuMem::inTRACE_COMPRESS_ZOOM()
{
	reg_in(TRACE_COMPRESS_ZOOM);
	return TRACE_COMPRESS_ZOOM.payload;
}


void SpuMem::setZYNQ_COMPRESS_MAIN_rate( Spu_reg value )
{
	reg_assign_field(ZYNQ_COMPRESS_MAIN,rate,value);
}

void SpuMem::setZYNQ_COMPRESS_MAIN( Spu_reg value )
{
	reg_assign_field(ZYNQ_COMPRESS_MAIN,payload,value);
}

void SpuMem::outZYNQ_COMPRESS_MAIN_rate( Spu_reg value )
{
	out_assign_field(ZYNQ_COMPRESS_MAIN,rate,value);
}
void SpuMem::pulseZYNQ_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZYNQ_COMPRESS_MAIN,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZYNQ_COMPRESS_MAIN,rate,idleV);
}

void SpuMem::outZYNQ_COMPRESS_MAIN( Spu_reg value )
{
	out_assign_field(ZYNQ_COMPRESS_MAIN,payload,value);
}

void SpuMem::outZYNQ_COMPRESS_MAIN()
{
	out_member(ZYNQ_COMPRESS_MAIN);
}


Spu_reg SpuMem::inZYNQ_COMPRESS_MAIN()
{
	reg_in(ZYNQ_COMPRESS_MAIN);
	return ZYNQ_COMPRESS_MAIN.payload;
}


void SpuMem::setZYNQ_COMPRESS_ZOOM_rate( Spu_reg value )
{
	reg_assign_field(ZYNQ_COMPRESS_ZOOM,rate,value);
}

void SpuMem::setZYNQ_COMPRESS_ZOOM( Spu_reg value )
{
	reg_assign_field(ZYNQ_COMPRESS_ZOOM,payload,value);
}

void SpuMem::outZYNQ_COMPRESS_ZOOM_rate( Spu_reg value )
{
	out_assign_field(ZYNQ_COMPRESS_ZOOM,rate,value);
}
void SpuMem::pulseZYNQ_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus )
{
	out_assign_field(ZYNQ_COMPRESS_ZOOM,rate,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(ZYNQ_COMPRESS_ZOOM,rate,idleV);
}

void SpuMem::outZYNQ_COMPRESS_ZOOM( Spu_reg value )
{
	out_assign_field(ZYNQ_COMPRESS_ZOOM,payload,value);
}

void SpuMem::outZYNQ_COMPRESS_ZOOM()
{
	out_member(ZYNQ_COMPRESS_ZOOM);
}


Spu_reg SpuMem::inZYNQ_COMPRESS_ZOOM()
{
	reg_in(ZYNQ_COMPRESS_ZOOM);
	return ZYNQ_COMPRESS_ZOOM.payload;
}


Spu_reg SpuMem::inDEBUG_TX_LA()
{
	reg_in(DEBUG_TX_LA);
	return DEBUG_TX_LA.payload;
}


Spu_reg SpuMem::inDEBUG_TX_FRM()
{
	reg_in(DEBUG_TX_FRM);
	return DEBUG_TX_FRM.payload;
}


Spu_reg SpuMem::inIMPORT_CNT_CH()
{
	reg_in(IMPORT_CNT_CH);
	return IMPORT_CNT_CH.payload;
}


Spu_reg SpuMem::inIMPORT_CNT_LA()
{
	reg_in(IMPORT_CNT_LA);
	return IMPORT_CNT_LA.payload;
}


Spu_reg SpuMem::inDEBUG_LINEAR_INTX1()
{
	reg_in(DEBUG_LINEAR_INTX1);
	return DEBUG_LINEAR_INTX1;
}


Spu_reg SpuMem::inDEBUG_LINEAR_INTX2()
{
	reg_in(DEBUG_LINEAR_INTX2);
	return DEBUG_LINEAR_INTX2;
}


Spu_reg SpuMem::inDEBUG_DDR()
{
	reg_in(DEBUG_DDR);
	return DEBUG_DDR;
}


Spu_reg SpuMem::inDEBUG_TX_LEN_BURST()
{
	reg_in(DEBUG_TX_LEN_BURST);
	return DEBUG_TX_LEN_BURST.payload;
}


cache_addr SpuMem::addr_VERSION(){ return 0x1400+mAddrBase; }
cache_addr SpuMem::addr_TEST(){ return 0x1404+mAddrBase; }
cache_addr SpuMem::addr_CTRL(){ return 0x1408+mAddrBase; }
cache_addr SpuMem::addr_DEBUG(){ return 0x140C+mAddrBase; }

cache_addr SpuMem::addr_ADC_DATA_IDELAY(){ return 0x1410+mAddrBase; }
cache_addr SpuMem::addr_ADC_DATA_CHECK(){ return 0x1414+mAddrBase; }
cache_addr SpuMem::addr_ADC_CHN_ALIGN_CHK(){ return 0x1418+mAddrBase; }
cache_addr SpuMem::addr_ADC_CHN_ALIGN(){ return 0x141C+mAddrBase; }

cache_addr SpuMem::addr_PRE_PROC(){ return 0x1420+mAddrBase; }
cache_addr SpuMem::addr_FILTER(){ return 0x1424+mAddrBase; }
cache_addr SpuMem::addr_DOWN_SAMP_FAST(){ return 0x1428+mAddrBase; }
cache_addr SpuMem::addr_DOWN_SAMP_SLOW_H(){ return 0x142C+mAddrBase; }

cache_addr SpuMem::addr_DOWN_SAMP_SLOW_L(){ return 0x1430+mAddrBase; }
cache_addr SpuMem::addr_LA_DELAY(){ return 0x1434+mAddrBase; }
cache_addr SpuMem::addr_CHN_DELAY_AB(){ return 0x1438+mAddrBase; }
cache_addr SpuMem::addr_CHN_DELAY_CD(){ return 0x143C+mAddrBase; }

cache_addr SpuMem::addr_RANDOM_RANGE(){ return 0x1440+mAddrBase; }
cache_addr SpuMem::addr_EXT_TRIG_IDEALY(){ return 0x1444+mAddrBase; }
cache_addr SpuMem::addr_SEGMENT_SIZE(){ return 0x144C+mAddrBase; }
cache_addr SpuMem::addr_RECORD_LEN(){ return 0x1450+mAddrBase; }

cache_addr SpuMem::addr_WAVE_LEN_MAIN(){ return 0x1454+mAddrBase; }
cache_addr SpuMem::addr_WAVE_LEN_ZOOM(){ return 0x1458+mAddrBase; }
cache_addr SpuMem::addr_WAVE_OFFSET_MAIN(){ return 0x145C+mAddrBase; }
cache_addr SpuMem::addr_WAVE_OFFSET_ZOOM(){ return 0x1460+mAddrBase; }

cache_addr SpuMem::addr_MEAS_OFFSET_CH(){ return 0x1470+mAddrBase; }
cache_addr SpuMem::addr_MEAS_LEN_CH(){ return 0x1474+mAddrBase; }
cache_addr SpuMem::addr_MEAS_OFFSET_LA(){ return 0x1478+mAddrBase; }
cache_addr SpuMem::addr_MEAS_LEN_LA(){ return 0x147C+mAddrBase; }

cache_addr SpuMem::addr_COARSE_DELAY_MAIN(){ return 0x1480+mAddrBase; }
cache_addr SpuMem::addr_COARSE_DELAY_ZOOM(){ return 0x1484+mAddrBase; }
cache_addr SpuMem::addr_FINE_DELAY_MAIN(){ return 0x1488+mAddrBase; }
cache_addr SpuMem::addr_FINE_DELAY_ZOOM(){ return 0x148C+mAddrBase; }

cache_addr SpuMem::addr_INTERP_MULT_MAIN(){ return 0x1490+mAddrBase; }
cache_addr SpuMem::addr_INTERP_MULT_ZOOM(){ return 0x1494+mAddrBase; }
cache_addr SpuMem::addr_INTERP_MULT_MAIN_FINE(){ return 0x1498+mAddrBase; }
cache_addr SpuMem::addr_INTERP_MULT_EYE(){ return 0x149C+mAddrBase; }

cache_addr SpuMem::addr_INTERP_COEF_TAP(){ return 0x14A0+mAddrBase; }
cache_addr SpuMem::addr_INTERP_COEF_WR(){ return 0x14A4+mAddrBase; }
cache_addr SpuMem::addr_COMPRESS_MAIN(){ return 0x14A8+mAddrBase; }
cache_addr SpuMem::addr_COMPRESS_ZOOM(){ return 0x14AC+mAddrBase; }

cache_addr SpuMem::addr_LA_FINE_DELAY_MAIN(){ return 0x14B0+mAddrBase; }
cache_addr SpuMem::addr_LA_FINE_DELAY_ZOOM(){ return 0x14B4+mAddrBase; }
cache_addr SpuMem::addr_LA_COMP_MAIN(){ return 0x14B8+mAddrBase; }
cache_addr SpuMem::addr_LA_COMP_ZOOM(){ return 0x14BC+mAddrBase; }

cache_addr SpuMem::addr_TX_LEN_CH(){ return 0x14C0+mAddrBase; }
cache_addr SpuMem::addr_TX_LEN_LA(){ return 0x14C4+mAddrBase; }
cache_addr SpuMem::addr_TX_LEN_ZOOM_CH(){ return 0x14C8+mAddrBase; }
cache_addr SpuMem::addr_TX_LEN_ZOOM_LA(){ return 0x14CC+mAddrBase; }

cache_addr SpuMem::addr_TX_LEN_TRACE_CH(){ return 0x14D0+mAddrBase; }
cache_addr SpuMem::addr_TX_LEN_TRACE_LA(){ return 0x14D4+mAddrBase; }
cache_addr SpuMem::addr_TX_LEN_COL_CH(){ return 0x14D8+mAddrBase; }
cache_addr SpuMem::addr_TX_LEN_COL_LA(){ return 0x14DC+mAddrBase; }

cache_addr SpuMem::addr_DEBUG_ACQ(){ return 0x14E0+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_TX(){ return 0x14E4+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_MEM(){ return 0x14EC+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_SCAN(){ return 0x14F0+mAddrBase; }

cache_addr SpuMem::addr_RECORD_SUM(){ return 0x14F4+mAddrBase; }
cache_addr SpuMem::addr_SCAN_ROLL_FRM(){ return 0x14F8+mAddrBase; }
cache_addr SpuMem::addr_ADC_AVG(){ return 0x1500+mAddrBase; }
cache_addr SpuMem::addr_ADC_AVG_RES_AB(){ return 0x1504+mAddrBase; }

cache_addr SpuMem::addr_ADC_AVG_RES_CD(){ return 0x1508+mAddrBase; }
cache_addr SpuMem::addr_ADC_AVG_MAX(){ return 0x150C+mAddrBase; }
cache_addr SpuMem::addr_ADC_AVG_MIN(){ return 0x1510+mAddrBase; }
cache_addr SpuMem::addr_LA_SA_RES(){ return 0x1514+mAddrBase; }

cache_addr SpuMem::addr_ZONE_TRIG_AUX(){ return 0x1524+mAddrBase; }
cache_addr SpuMem::addr_ZONE_TRIG_TAB1(){ return 0x1528+mAddrBase; }
cache_addr SpuMem::addr_ZONE_TRIG_TAB2(){ return 0x152C+mAddrBase; }
cache_addr SpuMem::addr_MASK_TAB(){ return 0x1530+mAddrBase; }

cache_addr SpuMem::addr_MASK_TAB_EX(){ return 0x1534+mAddrBase; }
cache_addr SpuMem::addr_MASK_COMPRESS(){ return 0x1538+mAddrBase; }
cache_addr SpuMem::addr_MASK_CTRL(){ return 0x153C+mAddrBase; }
cache_addr SpuMem::addr_MASK_FRM_CNT_L(){ return 0x1540+mAddrBase; }

cache_addr SpuMem::addr_MASK_FRM_CNT_H(){ return 0x1544+mAddrBase; }
cache_addr SpuMem::addr_MASK_CROSS_CNT_L_CHA(){ return 0x1548+mAddrBase; }
cache_addr SpuMem::addr_MASK_CROSS_CNT_H_CHA(){ return 0x154C+mAddrBase; }
cache_addr SpuMem::addr_MASK_CROSS_CNT_L_CHB(){ return 0x1550+mAddrBase; }

cache_addr SpuMem::addr_MASK_CROSS_CNT_H_CHB(){ return 0x1554+mAddrBase; }
cache_addr SpuMem::addr_MASK_CROSS_CNT_L_CHC(){ return 0x1558+mAddrBase; }
cache_addr SpuMem::addr_MASK_CROSS_CNT_H_CHC(){ return 0x155C+mAddrBase; }
cache_addr SpuMem::addr_MASK_CROSS_CNT_L_CHD(){ return 0x1560+mAddrBase; }

cache_addr SpuMem::addr_MASK_CROSS_CNT_H_CHD(){ return 0x1564+mAddrBase; }
cache_addr SpuMem::addr_MASK_OUTPUT(){ return 0x1568+mAddrBase; }
cache_addr SpuMem::addr_SEARCH_OFFSET_CH(){ return 0x1570+mAddrBase; }
cache_addr SpuMem::addr_SEARCH_LEN_CH(){ return 0x1574+mAddrBase; }

cache_addr SpuMem::addr_SEARCH_OFFSET_LA(){ return 0x1578+mAddrBase; }
cache_addr SpuMem::addr_SEARCH_LEN_LA(){ return 0x157C+mAddrBase; }
cache_addr SpuMem::addr_TIME_TAG_L(){ return 0x1580+mAddrBase; }
cache_addr SpuMem::addr_TIME_TAG_H(){ return 0x1584+mAddrBase; }

cache_addr SpuMem::addr_FINE_TRIG_POSITION(){ return 0x1588+mAddrBase; }
cache_addr SpuMem::addr_TIME_TAG_CTRL(){ return 0x158C+mAddrBase; }
cache_addr SpuMem::addr_IMPORT_INFO(){ return 0x1590+mAddrBase; }
cache_addr SpuMem::addr_TIME_TAG_L_REC_FIRST(){ return 0x1598+mAddrBase; }

cache_addr SpuMem::addr_TIME_TAG_H_REC_FIRST(){ return 0x159C+mAddrBase; }
cache_addr SpuMem::addr_TRACE_AVG_RATE_X(){ return 0x15A0+mAddrBase; }
cache_addr SpuMem::addr_TRACE_AVG_RATE_Y(){ return 0x15A4+mAddrBase; }
cache_addr SpuMem::addr_LINEAR_INTX_CTRL(){ return 0x15AC+mAddrBase; }

cache_addr SpuMem::addr_LINEAR_INTX_INIT(){ return 0x15B0+mAddrBase; }
cache_addr SpuMem::addr_LINEAR_INTX_STEP(){ return 0x15B4+mAddrBase; }
cache_addr SpuMem::addr_LINEAR_INTX_INIT_ZOOM(){ return 0x15B8+mAddrBase; }
cache_addr SpuMem::addr_LINEAR_INTX_STEP_ZOOM(){ return 0x15BC+mAddrBase; }

cache_addr SpuMem::addr_WAVE_OFFSET_EYE(){ return 0x15C0+mAddrBase; }
cache_addr SpuMem::addr_WAVE_LEN_EYE(){ return 0x15C4+mAddrBase; }
cache_addr SpuMem::addr_COMPRESS_EYE(){ return 0x15C8+mAddrBase; }
cache_addr SpuMem::addr_TX_LEN_EYE(){ return 0x15CC+mAddrBase; }

cache_addr SpuMem::addr_LA_LINEAR_INTX_CTRL(){ return 0x15D0+mAddrBase; }
cache_addr SpuMem::addr_LA_LINEAR_INTX_INIT(){ return 0x15D4+mAddrBase; }
cache_addr SpuMem::addr_LA_LINEAR_INTX_STEP(){ return 0x15D8+mAddrBase; }
cache_addr SpuMem::addr_LA_LINEAR_INTX_INIT_ZOOM(){ return 0x15DC+mAddrBase; }

cache_addr SpuMem::addr_LA_LINEAR_INTX_STEP_ZOOM(){ return 0x15E0+mAddrBase; }
cache_addr SpuMem::addr_TRACE_COMPRESS_MAIN(){ return 0x15F8+mAddrBase; }
cache_addr SpuMem::addr_TRACE_COMPRESS_ZOOM(){ return 0x15FC+mAddrBase; }
cache_addr SpuMem::addr_ZYNQ_COMPRESS_MAIN(){ return 0x1600+mAddrBase; }

cache_addr SpuMem::addr_ZYNQ_COMPRESS_ZOOM(){ return 0x1604+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_TX_LA(){ return 0x1610+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_TX_FRM(){ return 0x1614+mAddrBase; }
cache_addr SpuMem::addr_IMPORT_CNT_CH(){ return 0x1618+mAddrBase; }

cache_addr SpuMem::addr_IMPORT_CNT_LA(){ return 0x161C+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_LINEAR_INTX1(){ return 0x1620+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_LINEAR_INTX2(){ return 0x1624+mAddrBase; }
cache_addr SpuMem::addr_DEBUG_DDR(){ return 0x1628+mAddrBase; }

cache_addr SpuMem::addr_DEBUG_TX_LEN_BURST(){ return 0x1630+mAddrBase; }


void SpuGp::flushWCache(int id)
{
	if(id==-1){ foreach( IPhySpu *pItem, mSubItems){ Q_ASSERT(NULL!=pItem); pItem->flushWCache(); } }
	else{ Q_ASSERT( id>=0 && mSubItems[id] != NULL); mSubItems[id]->flushWCache(); }
}

Spu_reg SpuGp::getVERSION( int id )
{
	return mSubItems[id]->getVERSION();
}

Spu_reg SpuGp::inVERSION( int id )
{
	return mSubItems[id]->inVERSION();
}


Spu_reg SpuGp::getTEST( int id )
{
	return mSubItems[id]->getTEST();
}

Spu_reg SpuGp::inTEST( int id )
{
	return mSubItems[id]->inTEST();
}


void SpuGp::setCTRL_spu_clr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_spu_clr(value);} }
	else{ mSubItems[id]->setCTRL_spu_clr( value); }
}
void SpuGp::setCTRL_adc_sync(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_adc_sync(value);} }
	else{ mSubItems[id]->setCTRL_adc_sync( value); }
}
void SpuGp::setCTRL_adc_align_check(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_adc_align_check(value);} }
	else{ mSubItems[id]->setCTRL_adc_align_check( value); }
}
void SpuGp::setCTRL_non_inter(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_non_inter(value);} }
	else{ mSubItems[id]->setCTRL_non_inter( value); }
}
void SpuGp::setCTRL_peak(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_peak(value);} }
	else{ mSubItems[id]->setCTRL_peak( value); }
}
void SpuGp::setCTRL_hi_res(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_hi_res(value);} }
	else{ mSubItems[id]->setCTRL_hi_res( value); }
}
void SpuGp::setCTRL_anti_alias(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_anti_alias(value);} }
	else{ mSubItems[id]->setCTRL_anti_alias( value); }
}
void SpuGp::setCTRL_trace_avg(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_trace_avg(value);} }
	else{ mSubItems[id]->setCTRL_trace_avg( value); }
}
void SpuGp::setCTRL_trace_avg_mem(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_trace_avg_mem(value);} }
	else{ mSubItems[id]->setCTRL_trace_avg_mem( value); }
}
void SpuGp::setCTRL_adc_data_inv(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_adc_data_inv(value);} }
	else{ mSubItems[id]->setCTRL_adc_data_inv( value); }
}
void SpuGp::setCTRL_la_probe(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_la_probe(value);} }
	else{ mSubItems[id]->setCTRL_la_probe( value); }
}
void SpuGp::setCTRL_coarse_trig_range(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL_coarse_trig_range(value);} }
	else{ mSubItems[id]->setCTRL_coarse_trig_range( value); }
}

void SpuGp::setCTRL( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCTRL(value);} }
	else{ mSubItems[id]->setCTRL( value); }
}

void SpuGp::outCTRL_spu_clr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_spu_clr(value);}}
	else{ mSubItems[id]->outCTRL_spu_clr( value); }
}
void SpuGp::pulseCTRL_spu_clr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_spu_clr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_spu_clr(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_spu_clr( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_adc_sync( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_adc_sync(value);}}
	else{ mSubItems[id]->outCTRL_adc_sync( value); }
}
void SpuGp::pulseCTRL_adc_sync( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_adc_sync(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_adc_sync(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_adc_sync( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_adc_align_check( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_adc_align_check(value);}}
	else{ mSubItems[id]->outCTRL_adc_align_check( value); }
}
void SpuGp::pulseCTRL_adc_align_check( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_adc_align_check(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_adc_align_check(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_adc_align_check( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_non_inter( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_non_inter(value);}}
	else{ mSubItems[id]->outCTRL_non_inter( value); }
}
void SpuGp::pulseCTRL_non_inter( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_non_inter(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_non_inter(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_non_inter( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_peak( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_peak(value);}}
	else{ mSubItems[id]->outCTRL_peak( value); }
}
void SpuGp::pulseCTRL_peak( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_peak(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_peak(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_peak( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_hi_res( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_hi_res(value);}}
	else{ mSubItems[id]->outCTRL_hi_res( value); }
}
void SpuGp::pulseCTRL_hi_res( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_hi_res(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_hi_res(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_hi_res( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_anti_alias( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_anti_alias(value);}}
	else{ mSubItems[id]->outCTRL_anti_alias( value); }
}
void SpuGp::pulseCTRL_anti_alias( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_anti_alias(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_anti_alias(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_anti_alias( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_trace_avg( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_trace_avg(value);}}
	else{ mSubItems[id]->outCTRL_trace_avg( value); }
}
void SpuGp::pulseCTRL_trace_avg( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_trace_avg(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_trace_avg(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_trace_avg( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_trace_avg_mem( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_trace_avg_mem(value);}}
	else{ mSubItems[id]->outCTRL_trace_avg_mem( value); }
}
void SpuGp::pulseCTRL_trace_avg_mem( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_trace_avg_mem(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_trace_avg_mem(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_trace_avg_mem( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_adc_data_inv( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_adc_data_inv(value);}}
	else{ mSubItems[id]->outCTRL_adc_data_inv( value); }
}
void SpuGp::pulseCTRL_adc_data_inv( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_adc_data_inv(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_adc_data_inv(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_adc_data_inv( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_la_probe( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_la_probe(value);}}
	else{ mSubItems[id]->outCTRL_la_probe( value); }
}
void SpuGp::pulseCTRL_la_probe( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_la_probe(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_la_probe(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_la_probe( activeV, idleV, timeus ); }
}
void SpuGp::outCTRL_coarse_trig_range( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_coarse_trig_range(value);}}
	else{ mSubItems[id]->outCTRL_coarse_trig_range( value); }
}
void SpuGp::pulseCTRL_coarse_trig_range( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL_coarse_trig_range(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCTRL_coarse_trig_range(idleV);}
	}
	else
	{ mSubItems[id]->pulseCTRL_coarse_trig_range( activeV, idleV, timeus ); }
}

void SpuGp::outCTRL(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL(value);} }
	else{ mSubItems[id]->outCTRL( value); }
}

void SpuGp::outCTRL( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCTRL();} }
	else{ mSubItems[id]->outCTRL(); }
}


Spu_reg SpuGp::getCTRL_spu_clr( int id )
{
	return mSubItems[id]->getCTRL_spu_clr();
}
Spu_reg SpuGp::getCTRL_adc_sync( int id )
{
	return mSubItems[id]->getCTRL_adc_sync();
}
Spu_reg SpuGp::getCTRL_adc_align_check( int id )
{
	return mSubItems[id]->getCTRL_adc_align_check();
}
Spu_reg SpuGp::getCTRL_non_inter( int id )
{
	return mSubItems[id]->getCTRL_non_inter();
}
Spu_reg SpuGp::getCTRL_peak( int id )
{
	return mSubItems[id]->getCTRL_peak();
}
Spu_reg SpuGp::getCTRL_hi_res( int id )
{
	return mSubItems[id]->getCTRL_hi_res();
}
Spu_reg SpuGp::getCTRL_anti_alias( int id )
{
	return mSubItems[id]->getCTRL_anti_alias();
}
Spu_reg SpuGp::getCTRL_trace_avg( int id )
{
	return mSubItems[id]->getCTRL_trace_avg();
}
Spu_reg SpuGp::getCTRL_trace_avg_mem( int id )
{
	return mSubItems[id]->getCTRL_trace_avg_mem();
}
Spu_reg SpuGp::getCTRL_adc_data_inv( int id )
{
	return mSubItems[id]->getCTRL_adc_data_inv();
}
Spu_reg SpuGp::getCTRL_la_probe( int id )
{
	return mSubItems[id]->getCTRL_la_probe();
}
Spu_reg SpuGp::getCTRL_coarse_trig_range( int id )
{
	return mSubItems[id]->getCTRL_coarse_trig_range();
}

Spu_reg SpuGp::getCTRL( int id )
{
	return mSubItems[id]->getCTRL();
}

Spu_reg SpuGp::inCTRL( int id )
{
	return mSubItems[id]->inCTRL();
}


void SpuGp::setDEBUG_gray_bypass(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_gray_bypass(value);} }
	else{ mSubItems[id]->setDEBUG_gray_bypass( value); }
}
void SpuGp::setDEBUG_ms_order(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ms_order(value);} }
	else{ mSubItems[id]->setDEBUG_ms_order( value); }
}
void SpuGp::setDEBUG_s_view(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_s_view(value);} }
	else{ mSubItems[id]->setDEBUG_s_view( value); }
}
void SpuGp::setDEBUG_bram_ddr_n(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_bram_ddr_n(value);} }
	else{ mSubItems[id]->setDEBUG_bram_ddr_n( value); }
}
void SpuGp::setDEBUG_ddr_init(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ddr_init(value);} }
	else{ mSubItems[id]->setDEBUG_ddr_init( value); }
}
void SpuGp::setDEBUG_ddr_err(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ddr_err(value);} }
	else{ mSubItems[id]->setDEBUG_ddr_err( value); }
}
void SpuGp::setDEBUG_dbg_trig_src(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_dbg_trig_src(value);} }
	else{ mSubItems[id]->setDEBUG_dbg_trig_src( value); }
}
void SpuGp::setDEBUG_ddr_fsm_state(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ddr_fsm_state(value);} }
	else{ mSubItems[id]->setDEBUG_ddr_fsm_state( value); }
}
void SpuGp::setDEBUG_debug_mem(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_debug_mem(value);} }
	else{ mSubItems[id]->setDEBUG_debug_mem( value); }
}

void SpuGp::setDEBUG( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG(value);} }
	else{ mSubItems[id]->setDEBUG( value); }
}

void SpuGp::outDEBUG_gray_bypass( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_gray_bypass(value);}}
	else{ mSubItems[id]->outDEBUG_gray_bypass( value); }
}
void SpuGp::pulseDEBUG_gray_bypass( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_gray_bypass(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_gray_bypass(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_gray_bypass( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_ms_order( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ms_order(value);}}
	else{ mSubItems[id]->outDEBUG_ms_order( value); }
}
void SpuGp::pulseDEBUG_ms_order( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ms_order(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ms_order(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ms_order( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_s_view( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_s_view(value);}}
	else{ mSubItems[id]->outDEBUG_s_view( value); }
}
void SpuGp::pulseDEBUG_s_view( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_s_view(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_s_view(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_s_view( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_bram_ddr_n( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_bram_ddr_n(value);}}
	else{ mSubItems[id]->outDEBUG_bram_ddr_n( value); }
}
void SpuGp::pulseDEBUG_bram_ddr_n( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_bram_ddr_n(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_bram_ddr_n(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_bram_ddr_n( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_ddr_init( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ddr_init(value);}}
	else{ mSubItems[id]->outDEBUG_ddr_init( value); }
}
void SpuGp::pulseDEBUG_ddr_init( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ddr_init(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ddr_init(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ddr_init( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_ddr_err( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ddr_err(value);}}
	else{ mSubItems[id]->outDEBUG_ddr_err( value); }
}
void SpuGp::pulseDEBUG_ddr_err( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ddr_err(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ddr_err(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ddr_err( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_dbg_trig_src( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_dbg_trig_src(value);}}
	else{ mSubItems[id]->outDEBUG_dbg_trig_src( value); }
}
void SpuGp::pulseDEBUG_dbg_trig_src( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_dbg_trig_src(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_dbg_trig_src(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_dbg_trig_src( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_ddr_fsm_state( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ddr_fsm_state(value);}}
	else{ mSubItems[id]->outDEBUG_ddr_fsm_state( value); }
}
void SpuGp::pulseDEBUG_ddr_fsm_state( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ddr_fsm_state(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ddr_fsm_state(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ddr_fsm_state( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_debug_mem( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_debug_mem(value);}}
	else{ mSubItems[id]->outDEBUG_debug_mem( value); }
}
void SpuGp::pulseDEBUG_debug_mem( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_debug_mem(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_debug_mem(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_debug_mem( activeV, idleV, timeus ); }
}

void SpuGp::outDEBUG(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG(value);} }
	else{ mSubItems[id]->outDEBUG( value); }
}

void SpuGp::outDEBUG( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG();} }
	else{ mSubItems[id]->outDEBUG(); }
}


Spu_reg SpuGp::getDEBUG_gray_bypass( int id )
{
	return mSubItems[id]->getDEBUG_gray_bypass();
}
Spu_reg SpuGp::getDEBUG_ms_order( int id )
{
	return mSubItems[id]->getDEBUG_ms_order();
}
Spu_reg SpuGp::getDEBUG_s_view( int id )
{
	return mSubItems[id]->getDEBUG_s_view();
}
Spu_reg SpuGp::getDEBUG_bram_ddr_n( int id )
{
	return mSubItems[id]->getDEBUG_bram_ddr_n();
}
Spu_reg SpuGp::getDEBUG_ddr_init( int id )
{
	return mSubItems[id]->getDEBUG_ddr_init();
}
Spu_reg SpuGp::getDEBUG_ddr_err( int id )
{
	return mSubItems[id]->getDEBUG_ddr_err();
}
Spu_reg SpuGp::getDEBUG_dbg_trig_src( int id )
{
	return mSubItems[id]->getDEBUG_dbg_trig_src();
}
Spu_reg SpuGp::getDEBUG_ddr_fsm_state( int id )
{
	return mSubItems[id]->getDEBUG_ddr_fsm_state();
}
Spu_reg SpuGp::getDEBUG_debug_mem( int id )
{
	return mSubItems[id]->getDEBUG_debug_mem();
}

Spu_reg SpuGp::getDEBUG( int id )
{
	return mSubItems[id]->getDEBUG();
}

Spu_reg SpuGp::inDEBUG( int id )
{
	return mSubItems[id]->inDEBUG();
}


void SpuGp::setADC_DATA_IDELAY_idelay_var(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_DATA_IDELAY_idelay_var(value);} }
	else{ mSubItems[id]->setADC_DATA_IDELAY_idelay_var( value); }
}
void SpuGp::setADC_DATA_IDELAY_chn_bit(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_DATA_IDELAY_chn_bit(value);} }
	else{ mSubItems[id]->setADC_DATA_IDELAY_chn_bit( value); }
}
void SpuGp::setADC_DATA_IDELAY_h(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_DATA_IDELAY_h(value);} }
	else{ mSubItems[id]->setADC_DATA_IDELAY_h( value); }
}
void SpuGp::setADC_DATA_IDELAY_l(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_DATA_IDELAY_l(value);} }
	else{ mSubItems[id]->setADC_DATA_IDELAY_l( value); }
}
void SpuGp::setADC_DATA_IDELAY_core_ABCD(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_DATA_IDELAY_core_ABCD(value);} }
	else{ mSubItems[id]->setADC_DATA_IDELAY_core_ABCD( value); }
}

void SpuGp::setADC_DATA_IDELAY( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_DATA_IDELAY(value);} }
	else{ mSubItems[id]->setADC_DATA_IDELAY( value); }
}

void SpuGp::outADC_DATA_IDELAY_idelay_var( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_idelay_var(value);}}
	else{ mSubItems[id]->outADC_DATA_IDELAY_idelay_var( value); }
}
void SpuGp::pulseADC_DATA_IDELAY_idelay_var( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_idelay_var(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_DATA_IDELAY_idelay_var(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_DATA_IDELAY_idelay_var( activeV, idleV, timeus ); }
}
void SpuGp::outADC_DATA_IDELAY_chn_bit( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_chn_bit(value);}}
	else{ mSubItems[id]->outADC_DATA_IDELAY_chn_bit( value); }
}
void SpuGp::pulseADC_DATA_IDELAY_chn_bit( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_chn_bit(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_DATA_IDELAY_chn_bit(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_DATA_IDELAY_chn_bit( activeV, idleV, timeus ); }
}
void SpuGp::outADC_DATA_IDELAY_h( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_h(value);}}
	else{ mSubItems[id]->outADC_DATA_IDELAY_h( value); }
}
void SpuGp::pulseADC_DATA_IDELAY_h( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_h(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_DATA_IDELAY_h(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_DATA_IDELAY_h( activeV, idleV, timeus ); }
}
void SpuGp::outADC_DATA_IDELAY_l( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_l(value);}}
	else{ mSubItems[id]->outADC_DATA_IDELAY_l( value); }
}
void SpuGp::pulseADC_DATA_IDELAY_l( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_l(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_DATA_IDELAY_l(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_DATA_IDELAY_l( activeV, idleV, timeus ); }
}
void SpuGp::outADC_DATA_IDELAY_core_ABCD( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_core_ABCD(value);}}
	else{ mSubItems[id]->outADC_DATA_IDELAY_core_ABCD( value); }
}
void SpuGp::pulseADC_DATA_IDELAY_core_ABCD( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY_core_ABCD(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_DATA_IDELAY_core_ABCD(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_DATA_IDELAY_core_ABCD( activeV, idleV, timeus ); }
}

void SpuGp::outADC_DATA_IDELAY(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY(value);} }
	else{ mSubItems[id]->outADC_DATA_IDELAY( value); }
}

void SpuGp::outADC_DATA_IDELAY( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_DATA_IDELAY();} }
	else{ mSubItems[id]->outADC_DATA_IDELAY(); }
}


Spu_reg SpuGp::getADC_DATA_IDELAY_idelay_var( int id )
{
	return mSubItems[id]->getADC_DATA_IDELAY_idelay_var();
}
Spu_reg SpuGp::getADC_DATA_IDELAY_chn_bit( int id )
{
	return mSubItems[id]->getADC_DATA_IDELAY_chn_bit();
}
Spu_reg SpuGp::getADC_DATA_IDELAY_h( int id )
{
	return mSubItems[id]->getADC_DATA_IDELAY_h();
}
Spu_reg SpuGp::getADC_DATA_IDELAY_l( int id )
{
	return mSubItems[id]->getADC_DATA_IDELAY_l();
}
Spu_reg SpuGp::getADC_DATA_IDELAY_core_ABCD( int id )
{
	return mSubItems[id]->getADC_DATA_IDELAY_core_ABCD();
}

Spu_reg SpuGp::getADC_DATA_IDELAY( int id )
{
	return mSubItems[id]->getADC_DATA_IDELAY();
}

Spu_reg SpuGp::inADC_DATA_IDELAY( int id )
{
	return mSubItems[id]->inADC_DATA_IDELAY();
}


Spu_reg SpuGp::getADC_DATA_CHECK_a_l( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_a_l();
}
Spu_reg SpuGp::getADC_DATA_CHECK_a_h( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_a_h();
}
Spu_reg SpuGp::getADC_DATA_CHECK_b_l( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_b_l();
}
Spu_reg SpuGp::getADC_DATA_CHECK_b_h( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_b_h();
}
Spu_reg SpuGp::getADC_DATA_CHECK_c_l( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_c_l();
}
Spu_reg SpuGp::getADC_DATA_CHECK_c_h( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_c_h();
}
Spu_reg SpuGp::getADC_DATA_CHECK_d_l( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_d_l();
}
Spu_reg SpuGp::getADC_DATA_CHECK_d_h( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK_d_h();
}

Spu_reg SpuGp::getADC_DATA_CHECK( int id )
{
	return mSubItems[id]->getADC_DATA_CHECK();
}

Spu_reg SpuGp::inADC_DATA_CHECK( int id )
{
	return mSubItems[id]->inADC_DATA_CHECK();
}


Spu_reg SpuGp::getADC_CHN_ALIGN_CHK_adc_d( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_CHK_adc_d();
}
Spu_reg SpuGp::getADC_CHN_ALIGN_CHK_adc_c( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_CHK_adc_c();
}
Spu_reg SpuGp::getADC_CHN_ALIGN_CHK_adc_b( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_CHK_adc_b();
}
Spu_reg SpuGp::getADC_CHN_ALIGN_CHK_adc_a( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_CHK_adc_a();
}

Spu_reg SpuGp::getADC_CHN_ALIGN_CHK( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_CHK();
}

Spu_reg SpuGp::inADC_CHN_ALIGN_CHK( int id )
{
	return mSubItems[id]->inADC_CHN_ALIGN_CHK();
}


void SpuGp::setADC_CHN_ALIGN_adc_d(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_CHN_ALIGN_adc_d(value);} }
	else{ mSubItems[id]->setADC_CHN_ALIGN_adc_d( value); }
}
void SpuGp::setADC_CHN_ALIGN_adc_c(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_CHN_ALIGN_adc_c(value);} }
	else{ mSubItems[id]->setADC_CHN_ALIGN_adc_c( value); }
}
void SpuGp::setADC_CHN_ALIGN_adc_b(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_CHN_ALIGN_adc_b(value);} }
	else{ mSubItems[id]->setADC_CHN_ALIGN_adc_b( value); }
}
void SpuGp::setADC_CHN_ALIGN_adc_a(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_CHN_ALIGN_adc_a(value);} }
	else{ mSubItems[id]->setADC_CHN_ALIGN_adc_a( value); }
}

void SpuGp::setADC_CHN_ALIGN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_CHN_ALIGN(value);} }
	else{ mSubItems[id]->setADC_CHN_ALIGN( value); }
}

void SpuGp::outADC_CHN_ALIGN_adc_d( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_d(value);}}
	else{ mSubItems[id]->outADC_CHN_ALIGN_adc_d( value); }
}
void SpuGp::pulseADC_CHN_ALIGN_adc_d( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_d(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_CHN_ALIGN_adc_d(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_CHN_ALIGN_adc_d( activeV, idleV, timeus ); }
}
void SpuGp::outADC_CHN_ALIGN_adc_c( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_c(value);}}
	else{ mSubItems[id]->outADC_CHN_ALIGN_adc_c( value); }
}
void SpuGp::pulseADC_CHN_ALIGN_adc_c( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_c(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_CHN_ALIGN_adc_c(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_CHN_ALIGN_adc_c( activeV, idleV, timeus ); }
}
void SpuGp::outADC_CHN_ALIGN_adc_b( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_b(value);}}
	else{ mSubItems[id]->outADC_CHN_ALIGN_adc_b( value); }
}
void SpuGp::pulseADC_CHN_ALIGN_adc_b( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_b(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_CHN_ALIGN_adc_b(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_CHN_ALIGN_adc_b( activeV, idleV, timeus ); }
}
void SpuGp::outADC_CHN_ALIGN_adc_a( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_a(value);}}
	else{ mSubItems[id]->outADC_CHN_ALIGN_adc_a( value); }
}
void SpuGp::pulseADC_CHN_ALIGN_adc_a( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN_adc_a(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_CHN_ALIGN_adc_a(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_CHN_ALIGN_adc_a( activeV, idleV, timeus ); }
}

void SpuGp::outADC_CHN_ALIGN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN(value);} }
	else{ mSubItems[id]->outADC_CHN_ALIGN( value); }
}

void SpuGp::outADC_CHN_ALIGN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_CHN_ALIGN();} }
	else{ mSubItems[id]->outADC_CHN_ALIGN(); }
}


Spu_reg SpuGp::getADC_CHN_ALIGN_adc_d( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_adc_d();
}
Spu_reg SpuGp::getADC_CHN_ALIGN_adc_c( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_adc_c();
}
Spu_reg SpuGp::getADC_CHN_ALIGN_adc_b( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_adc_b();
}
Spu_reg SpuGp::getADC_CHN_ALIGN_adc_a( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN_adc_a();
}

Spu_reg SpuGp::getADC_CHN_ALIGN( int id )
{
	return mSubItems[id]->getADC_CHN_ALIGN();
}

Spu_reg SpuGp::inADC_CHN_ALIGN( int id )
{
	return mSubItems[id]->inADC_CHN_ALIGN();
}


void SpuGp::setPRE_PROC_proc_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setPRE_PROC_proc_en(value);} }
	else{ mSubItems[id]->setPRE_PROC_proc_en( value); }
}
void SpuGp::setPRE_PROC_cfg_filter_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setPRE_PROC_cfg_filter_en(value);} }
	else{ mSubItems[id]->setPRE_PROC_cfg_filter_en( value); }
}

void SpuGp::setPRE_PROC( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setPRE_PROC(value);} }
	else{ mSubItems[id]->setPRE_PROC( value); }
}

void SpuGp::outPRE_PROC_proc_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outPRE_PROC_proc_en(value);}}
	else{ mSubItems[id]->outPRE_PROC_proc_en( value); }
}
void SpuGp::pulsePRE_PROC_proc_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outPRE_PROC_proc_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignPRE_PROC_proc_en(idleV);}
	}
	else
	{ mSubItems[id]->pulsePRE_PROC_proc_en( activeV, idleV, timeus ); }
}
void SpuGp::outPRE_PROC_cfg_filter_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outPRE_PROC_cfg_filter_en(value);}}
	else{ mSubItems[id]->outPRE_PROC_cfg_filter_en( value); }
}
void SpuGp::pulsePRE_PROC_cfg_filter_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outPRE_PROC_cfg_filter_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignPRE_PROC_cfg_filter_en(idleV);}
	}
	else
	{ mSubItems[id]->pulsePRE_PROC_cfg_filter_en( activeV, idleV, timeus ); }
}

void SpuGp::outPRE_PROC(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outPRE_PROC(value);} }
	else{ mSubItems[id]->outPRE_PROC( value); }
}

void SpuGp::outPRE_PROC( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outPRE_PROC();} }
	else{ mSubItems[id]->outPRE_PROC(); }
}


void SpuGp::setFILTER_coeff(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFILTER_coeff(value);} }
	else{ mSubItems[id]->setFILTER_coeff( value); }
}
void SpuGp::setFILTER_coeff_id(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFILTER_coeff_id(value);} }
	else{ mSubItems[id]->setFILTER_coeff_id( value); }
}
void SpuGp::setFILTER_coeff_sel(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFILTER_coeff_sel(value);} }
	else{ mSubItems[id]->setFILTER_coeff_sel( value); }
}
void SpuGp::setFILTER_coeff_ch(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFILTER_coeff_ch(value);} }
	else{ mSubItems[id]->setFILTER_coeff_ch( value); }
}

void SpuGp::setFILTER( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFILTER(value);} }
	else{ mSubItems[id]->setFILTER( value); }
}

void SpuGp::outFILTER_coeff( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff(value);}}
	else{ mSubItems[id]->outFILTER_coeff( value); }
}
void SpuGp::pulseFILTER_coeff( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFILTER_coeff(idleV);}
	}
	else
	{ mSubItems[id]->pulseFILTER_coeff( activeV, idleV, timeus ); }
}
void SpuGp::outFILTER_coeff_id( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff_id(value);}}
	else{ mSubItems[id]->outFILTER_coeff_id( value); }
}
void SpuGp::pulseFILTER_coeff_id( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff_id(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFILTER_coeff_id(idleV);}
	}
	else
	{ mSubItems[id]->pulseFILTER_coeff_id( activeV, idleV, timeus ); }
}
void SpuGp::outFILTER_coeff_sel( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff_sel(value);}}
	else{ mSubItems[id]->outFILTER_coeff_sel( value); }
}
void SpuGp::pulseFILTER_coeff_sel( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff_sel(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFILTER_coeff_sel(idleV);}
	}
	else
	{ mSubItems[id]->pulseFILTER_coeff_sel( activeV, idleV, timeus ); }
}
void SpuGp::outFILTER_coeff_ch( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff_ch(value);}}
	else{ mSubItems[id]->outFILTER_coeff_ch( value); }
}
void SpuGp::pulseFILTER_coeff_ch( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER_coeff_ch(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFILTER_coeff_ch(idleV);}
	}
	else
	{ mSubItems[id]->pulseFILTER_coeff_ch( activeV, idleV, timeus ); }
}

void SpuGp::outFILTER(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER(value);} }
	else{ mSubItems[id]->outFILTER( value); }
}

void SpuGp::outFILTER( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFILTER();} }
	else{ mSubItems[id]->outFILTER(); }
}


void SpuGp::setDOWN_SAMP_FAST_down_samp_fast(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDOWN_SAMP_FAST_down_samp_fast(value);} }
	else{ mSubItems[id]->setDOWN_SAMP_FAST_down_samp_fast( value); }
}
void SpuGp::setDOWN_SAMP_FAST_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDOWN_SAMP_FAST_en(value);} }
	else{ mSubItems[id]->setDOWN_SAMP_FAST_en( value); }
}

void SpuGp::setDOWN_SAMP_FAST( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDOWN_SAMP_FAST(value);} }
	else{ mSubItems[id]->setDOWN_SAMP_FAST( value); }
}

void SpuGp::outDOWN_SAMP_FAST_down_samp_fast( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_FAST_down_samp_fast(value);}}
	else{ mSubItems[id]->outDOWN_SAMP_FAST_down_samp_fast( value); }
}
void SpuGp::pulseDOWN_SAMP_FAST_down_samp_fast( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_FAST_down_samp_fast(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDOWN_SAMP_FAST_down_samp_fast(idleV);}
	}
	else
	{ mSubItems[id]->pulseDOWN_SAMP_FAST_down_samp_fast( activeV, idleV, timeus ); }
}
void SpuGp::outDOWN_SAMP_FAST_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_FAST_en(value);}}
	else{ mSubItems[id]->outDOWN_SAMP_FAST_en( value); }
}
void SpuGp::pulseDOWN_SAMP_FAST_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_FAST_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDOWN_SAMP_FAST_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDOWN_SAMP_FAST_en( activeV, idleV, timeus ); }
}

void SpuGp::outDOWN_SAMP_FAST(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_FAST(value);} }
	else{ mSubItems[id]->outDOWN_SAMP_FAST( value); }
}

void SpuGp::outDOWN_SAMP_FAST( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_FAST();} }
	else{ mSubItems[id]->outDOWN_SAMP_FAST(); }
}


Spu_reg SpuGp::getDOWN_SAMP_FAST_down_samp_fast( int id )
{
	return mSubItems[id]->getDOWN_SAMP_FAST_down_samp_fast();
}
Spu_reg SpuGp::getDOWN_SAMP_FAST_en( int id )
{
	return mSubItems[id]->getDOWN_SAMP_FAST_en();
}

Spu_reg SpuGp::getDOWN_SAMP_FAST( int id )
{
	return mSubItems[id]->getDOWN_SAMP_FAST();
}

Spu_reg SpuGp::inDOWN_SAMP_FAST( int id )
{
	return mSubItems[id]->inDOWN_SAMP_FAST();
}


void SpuGp::setDOWN_SAMP_SLOW_H_down_samp_slow_h(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDOWN_SAMP_SLOW_H_down_samp_slow_h(value);} }
	else{ mSubItems[id]->setDOWN_SAMP_SLOW_H_down_samp_slow_h( value); }
}

void SpuGp::setDOWN_SAMP_SLOW_H( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDOWN_SAMP_SLOW_H(value);} }
	else{ mSubItems[id]->setDOWN_SAMP_SLOW_H( value); }
}

void SpuGp::outDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_SLOW_H_down_samp_slow_h(value);}}
	else{ mSubItems[id]->outDOWN_SAMP_SLOW_H_down_samp_slow_h( value); }
}
void SpuGp::pulseDOWN_SAMP_SLOW_H_down_samp_slow_h( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_SLOW_H_down_samp_slow_h(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDOWN_SAMP_SLOW_H_down_samp_slow_h(idleV);}
	}
	else
	{ mSubItems[id]->pulseDOWN_SAMP_SLOW_H_down_samp_slow_h( activeV, idleV, timeus ); }
}

void SpuGp::outDOWN_SAMP_SLOW_H(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_SLOW_H(value);} }
	else{ mSubItems[id]->outDOWN_SAMP_SLOW_H( value); }
}

void SpuGp::outDOWN_SAMP_SLOW_H( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_SLOW_H();} }
	else{ mSubItems[id]->outDOWN_SAMP_SLOW_H(); }
}


Spu_reg SpuGp::getDOWN_SAMP_SLOW_H_down_samp_slow_h( int id )
{
	return mSubItems[id]->getDOWN_SAMP_SLOW_H_down_samp_slow_h();
}

Spu_reg SpuGp::getDOWN_SAMP_SLOW_H( int id )
{
	return mSubItems[id]->getDOWN_SAMP_SLOW_H();
}

Spu_reg SpuGp::inDOWN_SAMP_SLOW_H( int id )
{
	return mSubItems[id]->inDOWN_SAMP_SLOW_H();
}


void SpuGp::setDOWN_SAMP_SLOW_L( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDOWN_SAMP_SLOW_L(value);} }
	else{ mSubItems[id]->setDOWN_SAMP_SLOW_L( value); }
}

void SpuGp::outDOWN_SAMP_SLOW_L( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_SLOW_L(value);} }
	else{ mSubItems[id]->outDOWN_SAMP_SLOW_L( value); }
}

void SpuGp::outDOWN_SAMP_SLOW_L( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDOWN_SAMP_SLOW_L();} }
	else{ mSubItems[id]->outDOWN_SAMP_SLOW_L(); }
}


Spu_reg SpuGp::getDOWN_SAMP_SLOW_L( int id )
{
	return mSubItems[id]->getDOWN_SAMP_SLOW_L();
}

Spu_reg SpuGp::inDOWN_SAMP_SLOW_L( int id )
{
	return mSubItems[id]->inDOWN_SAMP_SLOW_L();
}


void SpuGp::setLA_DELAY( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_DELAY(value);} }
	else{ mSubItems[id]->setLA_DELAY( value); }
}

void SpuGp::outLA_DELAY( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_DELAY(value);} }
	else{ mSubItems[id]->outLA_DELAY( value); }
}

void SpuGp::outLA_DELAY( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_DELAY();} }
	else{ mSubItems[id]->outLA_DELAY(); }
}


Spu_reg SpuGp::getLA_DELAY( int id )
{
	return mSubItems[id]->getLA_DELAY();
}

Spu_reg SpuGp::inLA_DELAY( int id )
{
	return mSubItems[id]->inLA_DELAY();
}


void SpuGp::setCHN_DELAY_AB_B(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCHN_DELAY_AB_B(value);} }
	else{ mSubItems[id]->setCHN_DELAY_AB_B( value); }
}
void SpuGp::setCHN_DELAY_AB_A(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCHN_DELAY_AB_A(value);} }
	else{ mSubItems[id]->setCHN_DELAY_AB_A( value); }
}

void SpuGp::setCHN_DELAY_AB( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCHN_DELAY_AB(value);} }
	else{ mSubItems[id]->setCHN_DELAY_AB( value); }
}

void SpuGp::outCHN_DELAY_AB_B( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_AB_B(value);}}
	else{ mSubItems[id]->outCHN_DELAY_AB_B( value); }
}
void SpuGp::pulseCHN_DELAY_AB_B( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_AB_B(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCHN_DELAY_AB_B(idleV);}
	}
	else
	{ mSubItems[id]->pulseCHN_DELAY_AB_B( activeV, idleV, timeus ); }
}
void SpuGp::outCHN_DELAY_AB_A( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_AB_A(value);}}
	else{ mSubItems[id]->outCHN_DELAY_AB_A( value); }
}
void SpuGp::pulseCHN_DELAY_AB_A( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_AB_A(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCHN_DELAY_AB_A(idleV);}
	}
	else
	{ mSubItems[id]->pulseCHN_DELAY_AB_A( activeV, idleV, timeus ); }
}

void SpuGp::outCHN_DELAY_AB(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_AB(value);} }
	else{ mSubItems[id]->outCHN_DELAY_AB( value); }
}

void SpuGp::outCHN_DELAY_AB( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_AB();} }
	else{ mSubItems[id]->outCHN_DELAY_AB(); }
}


Spu_reg SpuGp::getCHN_DELAY_AB_B( int id )
{
	return mSubItems[id]->getCHN_DELAY_AB_B();
}
Spu_reg SpuGp::getCHN_DELAY_AB_A( int id )
{
	return mSubItems[id]->getCHN_DELAY_AB_A();
}

Spu_reg SpuGp::getCHN_DELAY_AB( int id )
{
	return mSubItems[id]->getCHN_DELAY_AB();
}

Spu_reg SpuGp::inCHN_DELAY_AB( int id )
{
	return mSubItems[id]->inCHN_DELAY_AB();
}


void SpuGp::setCHN_DELAY_CD_D(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCHN_DELAY_CD_D(value);} }
	else{ mSubItems[id]->setCHN_DELAY_CD_D( value); }
}
void SpuGp::setCHN_DELAY_CD_C(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCHN_DELAY_CD_C(value);} }
	else{ mSubItems[id]->setCHN_DELAY_CD_C( value); }
}

void SpuGp::setCHN_DELAY_CD( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCHN_DELAY_CD(value);} }
	else{ mSubItems[id]->setCHN_DELAY_CD( value); }
}

void SpuGp::outCHN_DELAY_CD_D( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_CD_D(value);}}
	else{ mSubItems[id]->outCHN_DELAY_CD_D( value); }
}
void SpuGp::pulseCHN_DELAY_CD_D( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_CD_D(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCHN_DELAY_CD_D(idleV);}
	}
	else
	{ mSubItems[id]->pulseCHN_DELAY_CD_D( activeV, idleV, timeus ); }
}
void SpuGp::outCHN_DELAY_CD_C( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_CD_C(value);}}
	else{ mSubItems[id]->outCHN_DELAY_CD_C( value); }
}
void SpuGp::pulseCHN_DELAY_CD_C( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_CD_C(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCHN_DELAY_CD_C(idleV);}
	}
	else
	{ mSubItems[id]->pulseCHN_DELAY_CD_C( activeV, idleV, timeus ); }
}

void SpuGp::outCHN_DELAY_CD(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_CD(value);} }
	else{ mSubItems[id]->outCHN_DELAY_CD( value); }
}

void SpuGp::outCHN_DELAY_CD( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCHN_DELAY_CD();} }
	else{ mSubItems[id]->outCHN_DELAY_CD(); }
}


Spu_reg SpuGp::getCHN_DELAY_CD_D( int id )
{
	return mSubItems[id]->getCHN_DELAY_CD_D();
}
Spu_reg SpuGp::getCHN_DELAY_CD_C( int id )
{
	return mSubItems[id]->getCHN_DELAY_CD_C();
}

Spu_reg SpuGp::getCHN_DELAY_CD( int id )
{
	return mSubItems[id]->getCHN_DELAY_CD();
}

Spu_reg SpuGp::inCHN_DELAY_CD( int id )
{
	return mSubItems[id]->inCHN_DELAY_CD();
}


void SpuGp::setRANDOM_RANGE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setRANDOM_RANGE(value);} }
	else{ mSubItems[id]->setRANDOM_RANGE( value); }
}

void SpuGp::outRANDOM_RANGE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outRANDOM_RANGE(value);} }
	else{ mSubItems[id]->outRANDOM_RANGE( value); }
}

void SpuGp::outRANDOM_RANGE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outRANDOM_RANGE();} }
	else{ mSubItems[id]->outRANDOM_RANGE(); }
}


Spu_reg SpuGp::getRANDOM_RANGE( int id )
{
	return mSubItems[id]->getRANDOM_RANGE();
}

Spu_reg SpuGp::inRANDOM_RANGE( int id )
{
	return mSubItems[id]->inRANDOM_RANGE();
}


void SpuGp::setEXT_TRIG_IDEALY_idelay_var(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setEXT_TRIG_IDEALY_idelay_var(value);} }
	else{ mSubItems[id]->setEXT_TRIG_IDEALY_idelay_var( value); }
}
void SpuGp::setEXT_TRIG_IDEALY_ld(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setEXT_TRIG_IDEALY_ld(value);} }
	else{ mSubItems[id]->setEXT_TRIG_IDEALY_ld( value); }
}

void SpuGp::setEXT_TRIG_IDEALY( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setEXT_TRIG_IDEALY(value);} }
	else{ mSubItems[id]->setEXT_TRIG_IDEALY( value); }
}

void SpuGp::outEXT_TRIG_IDEALY_idelay_var( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outEXT_TRIG_IDEALY_idelay_var(value);}}
	else{ mSubItems[id]->outEXT_TRIG_IDEALY_idelay_var( value); }
}
void SpuGp::pulseEXT_TRIG_IDEALY_idelay_var( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outEXT_TRIG_IDEALY_idelay_var(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignEXT_TRIG_IDEALY_idelay_var(idleV);}
	}
	else
	{ mSubItems[id]->pulseEXT_TRIG_IDEALY_idelay_var( activeV, idleV, timeus ); }
}
void SpuGp::outEXT_TRIG_IDEALY_ld( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outEXT_TRIG_IDEALY_ld(value);}}
	else{ mSubItems[id]->outEXT_TRIG_IDEALY_ld( value); }
}
void SpuGp::pulseEXT_TRIG_IDEALY_ld( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outEXT_TRIG_IDEALY_ld(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignEXT_TRIG_IDEALY_ld(idleV);}
	}
	else
	{ mSubItems[id]->pulseEXT_TRIG_IDEALY_ld( activeV, idleV, timeus ); }
}

void SpuGp::outEXT_TRIG_IDEALY(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outEXT_TRIG_IDEALY(value);} }
	else{ mSubItems[id]->outEXT_TRIG_IDEALY( value); }
}

void SpuGp::outEXT_TRIG_IDEALY( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outEXT_TRIG_IDEALY();} }
	else{ mSubItems[id]->outEXT_TRIG_IDEALY(); }
}


Spu_reg SpuGp::getEXT_TRIG_IDEALY_idelay_var( int id )
{
	return mSubItems[id]->getEXT_TRIG_IDEALY_idelay_var();
}
Spu_reg SpuGp::getEXT_TRIG_IDEALY_ld( int id )
{
	return mSubItems[id]->getEXT_TRIG_IDEALY_ld();
}

Spu_reg SpuGp::getEXT_TRIG_IDEALY( int id )
{
	return mSubItems[id]->getEXT_TRIG_IDEALY();
}

Spu_reg SpuGp::inEXT_TRIG_IDEALY( int id )
{
	return mSubItems[id]->inEXT_TRIG_IDEALY();
}


void SpuGp::setSEGMENT_SIZE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setSEGMENT_SIZE(value);} }
	else{ mSubItems[id]->setSEGMENT_SIZE( value); }
}

void SpuGp::outSEGMENT_SIZE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEGMENT_SIZE(value);} }
	else{ mSubItems[id]->outSEGMENT_SIZE( value); }
}

void SpuGp::outSEGMENT_SIZE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEGMENT_SIZE();} }
	else{ mSubItems[id]->outSEGMENT_SIZE(); }
}


Spu_reg SpuGp::getSEGMENT_SIZE( int id )
{
	return mSubItems[id]->getSEGMENT_SIZE();
}

Spu_reg SpuGp::inSEGMENT_SIZE( int id )
{
	return mSubItems[id]->inSEGMENT_SIZE();
}


void SpuGp::setRECORD_LEN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setRECORD_LEN(value);} }
	else{ mSubItems[id]->setRECORD_LEN( value); }
}

void SpuGp::outRECORD_LEN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outRECORD_LEN(value);} }
	else{ mSubItems[id]->outRECORD_LEN( value); }
}

void SpuGp::outRECORD_LEN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outRECORD_LEN();} }
	else{ mSubItems[id]->outRECORD_LEN(); }
}


Spu_reg SpuGp::getRECORD_LEN( int id )
{
	return mSubItems[id]->getRECORD_LEN();
}

Spu_reg SpuGp::inRECORD_LEN( int id )
{
	return mSubItems[id]->inRECORD_LEN();
}


void SpuGp::setWAVE_LEN_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setWAVE_LEN_MAIN(value);} }
	else{ mSubItems[id]->setWAVE_LEN_MAIN( value); }
}

void SpuGp::outWAVE_LEN_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_LEN_MAIN(value);} }
	else{ mSubItems[id]->outWAVE_LEN_MAIN( value); }
}

void SpuGp::outWAVE_LEN_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_LEN_MAIN();} }
	else{ mSubItems[id]->outWAVE_LEN_MAIN(); }
}


Spu_reg SpuGp::getWAVE_LEN_MAIN( int id )
{
	return mSubItems[id]->getWAVE_LEN_MAIN();
}

Spu_reg SpuGp::inWAVE_LEN_MAIN( int id )
{
	return mSubItems[id]->inWAVE_LEN_MAIN();
}


void SpuGp::setWAVE_LEN_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setWAVE_LEN_ZOOM(value);} }
	else{ mSubItems[id]->setWAVE_LEN_ZOOM( value); }
}

void SpuGp::outWAVE_LEN_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_LEN_ZOOM(value);} }
	else{ mSubItems[id]->outWAVE_LEN_ZOOM( value); }
}

void SpuGp::outWAVE_LEN_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_LEN_ZOOM();} }
	else{ mSubItems[id]->outWAVE_LEN_ZOOM(); }
}


Spu_reg SpuGp::getWAVE_LEN_ZOOM( int id )
{
	return mSubItems[id]->getWAVE_LEN_ZOOM();
}

Spu_reg SpuGp::inWAVE_LEN_ZOOM( int id )
{
	return mSubItems[id]->inWAVE_LEN_ZOOM();
}


void SpuGp::setWAVE_OFFSET_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setWAVE_OFFSET_MAIN(value);} }
	else{ mSubItems[id]->setWAVE_OFFSET_MAIN( value); }
}

void SpuGp::outWAVE_OFFSET_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_OFFSET_MAIN(value);} }
	else{ mSubItems[id]->outWAVE_OFFSET_MAIN( value); }
}

void SpuGp::outWAVE_OFFSET_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_OFFSET_MAIN();} }
	else{ mSubItems[id]->outWAVE_OFFSET_MAIN(); }
}


Spu_reg SpuGp::getWAVE_OFFSET_MAIN( int id )
{
	return mSubItems[id]->getWAVE_OFFSET_MAIN();
}

Spu_reg SpuGp::inWAVE_OFFSET_MAIN( int id )
{
	return mSubItems[id]->inWAVE_OFFSET_MAIN();
}


void SpuGp::setWAVE_OFFSET_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setWAVE_OFFSET_ZOOM(value);} }
	else{ mSubItems[id]->setWAVE_OFFSET_ZOOM( value); }
}

void SpuGp::outWAVE_OFFSET_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_OFFSET_ZOOM(value);} }
	else{ mSubItems[id]->outWAVE_OFFSET_ZOOM( value); }
}

void SpuGp::outWAVE_OFFSET_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_OFFSET_ZOOM();} }
	else{ mSubItems[id]->outWAVE_OFFSET_ZOOM(); }
}


Spu_reg SpuGp::getWAVE_OFFSET_ZOOM( int id )
{
	return mSubItems[id]->getWAVE_OFFSET_ZOOM();
}

Spu_reg SpuGp::inWAVE_OFFSET_ZOOM( int id )
{
	return mSubItems[id]->inWAVE_OFFSET_ZOOM();
}


void SpuGp::setMEAS_OFFSET_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMEAS_OFFSET_CH(value);} }
	else{ mSubItems[id]->setMEAS_OFFSET_CH( value); }
}

void SpuGp::outMEAS_OFFSET_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_OFFSET_CH(value);} }
	else{ mSubItems[id]->outMEAS_OFFSET_CH( value); }
}

void SpuGp::outMEAS_OFFSET_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_OFFSET_CH();} }
	else{ mSubItems[id]->outMEAS_OFFSET_CH(); }
}


Spu_reg SpuGp::getMEAS_OFFSET_CH( int id )
{
	return mSubItems[id]->getMEAS_OFFSET_CH();
}

Spu_reg SpuGp::inMEAS_OFFSET_CH( int id )
{
	return mSubItems[id]->inMEAS_OFFSET_CH();
}


void SpuGp::setMEAS_LEN_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMEAS_LEN_CH(value);} }
	else{ mSubItems[id]->setMEAS_LEN_CH( value); }
}

void SpuGp::outMEAS_LEN_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_LEN_CH(value);} }
	else{ mSubItems[id]->outMEAS_LEN_CH( value); }
}

void SpuGp::outMEAS_LEN_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_LEN_CH();} }
	else{ mSubItems[id]->outMEAS_LEN_CH(); }
}


Spu_reg SpuGp::getMEAS_LEN_CH( int id )
{
	return mSubItems[id]->getMEAS_LEN_CH();
}

Spu_reg SpuGp::inMEAS_LEN_CH( int id )
{
	return mSubItems[id]->inMEAS_LEN_CH();
}


void SpuGp::setMEAS_OFFSET_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMEAS_OFFSET_LA(value);} }
	else{ mSubItems[id]->setMEAS_OFFSET_LA( value); }
}

void SpuGp::outMEAS_OFFSET_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_OFFSET_LA(value);} }
	else{ mSubItems[id]->outMEAS_OFFSET_LA( value); }
}

void SpuGp::outMEAS_OFFSET_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_OFFSET_LA();} }
	else{ mSubItems[id]->outMEAS_OFFSET_LA(); }
}


Spu_reg SpuGp::getMEAS_OFFSET_LA( int id )
{
	return mSubItems[id]->getMEAS_OFFSET_LA();
}

Spu_reg SpuGp::inMEAS_OFFSET_LA( int id )
{
	return mSubItems[id]->inMEAS_OFFSET_LA();
}


void SpuGp::setMEAS_LEN_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMEAS_LEN_LA(value);} }
	else{ mSubItems[id]->setMEAS_LEN_LA( value); }
}

void SpuGp::outMEAS_LEN_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_LEN_LA(value);} }
	else{ mSubItems[id]->outMEAS_LEN_LA( value); }
}

void SpuGp::outMEAS_LEN_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMEAS_LEN_LA();} }
	else{ mSubItems[id]->outMEAS_LEN_LA(); }
}


Spu_reg SpuGp::getMEAS_LEN_LA( int id )
{
	return mSubItems[id]->getMEAS_LEN_LA();
}

Spu_reg SpuGp::inMEAS_LEN_LA( int id )
{
	return mSubItems[id]->inMEAS_LEN_LA();
}


void SpuGp::setCOARSE_DELAY_MAIN_chn_delay(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_MAIN_chn_delay(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_MAIN_chn_delay( value); }
}
void SpuGp::setCOARSE_DELAY_MAIN_chn_ce(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_MAIN_chn_ce(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_MAIN_chn_ce( value); }
}
void SpuGp::setCOARSE_DELAY_MAIN_offset_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_MAIN_offset_en(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_MAIN_offset_en( value); }
}
void SpuGp::setCOARSE_DELAY_MAIN_delay_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_MAIN_delay_en(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_MAIN_delay_en( value); }
}

void SpuGp::setCOARSE_DELAY_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_MAIN(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_MAIN( value); }
}

void SpuGp::outCOARSE_DELAY_MAIN_chn_delay( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_chn_delay(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_MAIN_chn_delay( value); }
}
void SpuGp::pulseCOARSE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_chn_delay(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_MAIN_chn_delay(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_MAIN_chn_delay( activeV, idleV, timeus ); }
}
void SpuGp::outCOARSE_DELAY_MAIN_chn_ce( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_chn_ce(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_MAIN_chn_ce( value); }
}
void SpuGp::pulseCOARSE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_chn_ce(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_MAIN_chn_ce(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_MAIN_chn_ce( activeV, idleV, timeus ); }
}
void SpuGp::outCOARSE_DELAY_MAIN_offset_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_offset_en(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_MAIN_offset_en( value); }
}
void SpuGp::pulseCOARSE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_offset_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_MAIN_offset_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_MAIN_offset_en( activeV, idleV, timeus ); }
}
void SpuGp::outCOARSE_DELAY_MAIN_delay_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_delay_en(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_MAIN_delay_en( value); }
}
void SpuGp::pulseCOARSE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN_delay_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_MAIN_delay_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_MAIN_delay_en( activeV, idleV, timeus ); }
}

void SpuGp::outCOARSE_DELAY_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN(value);} }
	else{ mSubItems[id]->outCOARSE_DELAY_MAIN( value); }
}

void SpuGp::outCOARSE_DELAY_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_MAIN();} }
	else{ mSubItems[id]->outCOARSE_DELAY_MAIN(); }
}


Spu_reg SpuGp::getCOARSE_DELAY_MAIN_chn_delay( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_MAIN_chn_delay();
}
Spu_reg SpuGp::getCOARSE_DELAY_MAIN_chn_ce( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_MAIN_chn_ce();
}
Spu_reg SpuGp::getCOARSE_DELAY_MAIN_offset_en( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_MAIN_offset_en();
}
Spu_reg SpuGp::getCOARSE_DELAY_MAIN_delay_en( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_MAIN_delay_en();
}

Spu_reg SpuGp::getCOARSE_DELAY_MAIN( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_MAIN();
}

Spu_reg SpuGp::inCOARSE_DELAY_MAIN( int id )
{
	return mSubItems[id]->inCOARSE_DELAY_MAIN();
}


void SpuGp::setCOARSE_DELAY_ZOOM_chn_delay(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_ZOOM_chn_delay(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_ZOOM_chn_delay( value); }
}
void SpuGp::setCOARSE_DELAY_ZOOM_chn_ce(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_ZOOM_chn_ce(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_ZOOM_chn_ce( value); }
}
void SpuGp::setCOARSE_DELAY_ZOOM_offset_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_ZOOM_offset_en(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_ZOOM_offset_en( value); }
}
void SpuGp::setCOARSE_DELAY_ZOOM_delay_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_ZOOM_delay_en(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_ZOOM_delay_en( value); }
}

void SpuGp::setCOARSE_DELAY_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOARSE_DELAY_ZOOM(value);} }
	else{ mSubItems[id]->setCOARSE_DELAY_ZOOM( value); }
}

void SpuGp::outCOARSE_DELAY_ZOOM_chn_delay( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_chn_delay(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_ZOOM_chn_delay( value); }
}
void SpuGp::pulseCOARSE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_chn_delay(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_ZOOM_chn_delay(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_ZOOM_chn_delay( activeV, idleV, timeus ); }
}
void SpuGp::outCOARSE_DELAY_ZOOM_chn_ce( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_chn_ce(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_ZOOM_chn_ce( value); }
}
void SpuGp::pulseCOARSE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_chn_ce(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_ZOOM_chn_ce(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_ZOOM_chn_ce( activeV, idleV, timeus ); }
}
void SpuGp::outCOARSE_DELAY_ZOOM_offset_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_offset_en(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_ZOOM_offset_en( value); }
}
void SpuGp::pulseCOARSE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_offset_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_ZOOM_offset_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_ZOOM_offset_en( activeV, idleV, timeus ); }
}
void SpuGp::outCOARSE_DELAY_ZOOM_delay_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_delay_en(value);}}
	else{ mSubItems[id]->outCOARSE_DELAY_ZOOM_delay_en( value); }
}
void SpuGp::pulseCOARSE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM_delay_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOARSE_DELAY_ZOOM_delay_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOARSE_DELAY_ZOOM_delay_en( activeV, idleV, timeus ); }
}

void SpuGp::outCOARSE_DELAY_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM(value);} }
	else{ mSubItems[id]->outCOARSE_DELAY_ZOOM( value); }
}

void SpuGp::outCOARSE_DELAY_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOARSE_DELAY_ZOOM();} }
	else{ mSubItems[id]->outCOARSE_DELAY_ZOOM(); }
}


Spu_reg SpuGp::getCOARSE_DELAY_ZOOM_chn_delay( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_ZOOM_chn_delay();
}
Spu_reg SpuGp::getCOARSE_DELAY_ZOOM_chn_ce( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_ZOOM_chn_ce();
}
Spu_reg SpuGp::getCOARSE_DELAY_ZOOM_offset_en( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_ZOOM_offset_en();
}
Spu_reg SpuGp::getCOARSE_DELAY_ZOOM_delay_en( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_ZOOM_delay_en();
}

Spu_reg SpuGp::getCOARSE_DELAY_ZOOM( int id )
{
	return mSubItems[id]->getCOARSE_DELAY_ZOOM();
}

Spu_reg SpuGp::inCOARSE_DELAY_ZOOM( int id )
{
	return mSubItems[id]->inCOARSE_DELAY_ZOOM();
}


void SpuGp::setFINE_DELAY_MAIN_chn_delay(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_MAIN_chn_delay(value);} }
	else{ mSubItems[id]->setFINE_DELAY_MAIN_chn_delay( value); }
}
void SpuGp::setFINE_DELAY_MAIN_chn_ce(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_MAIN_chn_ce(value);} }
	else{ mSubItems[id]->setFINE_DELAY_MAIN_chn_ce( value); }
}
void SpuGp::setFINE_DELAY_MAIN_offset_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_MAIN_offset_en(value);} }
	else{ mSubItems[id]->setFINE_DELAY_MAIN_offset_en( value); }
}
void SpuGp::setFINE_DELAY_MAIN_delay_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_MAIN_delay_en(value);} }
	else{ mSubItems[id]->setFINE_DELAY_MAIN_delay_en( value); }
}

void SpuGp::setFINE_DELAY_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_MAIN(value);} }
	else{ mSubItems[id]->setFINE_DELAY_MAIN( value); }
}

void SpuGp::outFINE_DELAY_MAIN_chn_delay( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_chn_delay(value);}}
	else{ mSubItems[id]->outFINE_DELAY_MAIN_chn_delay( value); }
}
void SpuGp::pulseFINE_DELAY_MAIN_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_chn_delay(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_MAIN_chn_delay(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_MAIN_chn_delay( activeV, idleV, timeus ); }
}
void SpuGp::outFINE_DELAY_MAIN_chn_ce( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_chn_ce(value);}}
	else{ mSubItems[id]->outFINE_DELAY_MAIN_chn_ce( value); }
}
void SpuGp::pulseFINE_DELAY_MAIN_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_chn_ce(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_MAIN_chn_ce(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_MAIN_chn_ce( activeV, idleV, timeus ); }
}
void SpuGp::outFINE_DELAY_MAIN_offset_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_offset_en(value);}}
	else{ mSubItems[id]->outFINE_DELAY_MAIN_offset_en( value); }
}
void SpuGp::pulseFINE_DELAY_MAIN_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_offset_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_MAIN_offset_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_MAIN_offset_en( activeV, idleV, timeus ); }
}
void SpuGp::outFINE_DELAY_MAIN_delay_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_delay_en(value);}}
	else{ mSubItems[id]->outFINE_DELAY_MAIN_delay_en( value); }
}
void SpuGp::pulseFINE_DELAY_MAIN_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN_delay_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_MAIN_delay_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_MAIN_delay_en( activeV, idleV, timeus ); }
}

void SpuGp::outFINE_DELAY_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN(value);} }
	else{ mSubItems[id]->outFINE_DELAY_MAIN( value); }
}

void SpuGp::outFINE_DELAY_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_MAIN();} }
	else{ mSubItems[id]->outFINE_DELAY_MAIN(); }
}


Spu_reg SpuGp::getFINE_DELAY_MAIN_chn_delay( int id )
{
	return mSubItems[id]->getFINE_DELAY_MAIN_chn_delay();
}
Spu_reg SpuGp::getFINE_DELAY_MAIN_chn_ce( int id )
{
	return mSubItems[id]->getFINE_DELAY_MAIN_chn_ce();
}
Spu_reg SpuGp::getFINE_DELAY_MAIN_offset_en( int id )
{
	return mSubItems[id]->getFINE_DELAY_MAIN_offset_en();
}
Spu_reg SpuGp::getFINE_DELAY_MAIN_delay_en( int id )
{
	return mSubItems[id]->getFINE_DELAY_MAIN_delay_en();
}

Spu_reg SpuGp::getFINE_DELAY_MAIN( int id )
{
	return mSubItems[id]->getFINE_DELAY_MAIN();
}

Spu_reg SpuGp::inFINE_DELAY_MAIN( int id )
{
	return mSubItems[id]->inFINE_DELAY_MAIN();
}


void SpuGp::setFINE_DELAY_ZOOM_chn_delay(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_ZOOM_chn_delay(value);} }
	else{ mSubItems[id]->setFINE_DELAY_ZOOM_chn_delay( value); }
}
void SpuGp::setFINE_DELAY_ZOOM_chn_ce(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_ZOOM_chn_ce(value);} }
	else{ mSubItems[id]->setFINE_DELAY_ZOOM_chn_ce( value); }
}
void SpuGp::setFINE_DELAY_ZOOM_offset_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_ZOOM_offset_en(value);} }
	else{ mSubItems[id]->setFINE_DELAY_ZOOM_offset_en( value); }
}
void SpuGp::setFINE_DELAY_ZOOM_delay_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_ZOOM_delay_en(value);} }
	else{ mSubItems[id]->setFINE_DELAY_ZOOM_delay_en( value); }
}

void SpuGp::setFINE_DELAY_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_DELAY_ZOOM(value);} }
	else{ mSubItems[id]->setFINE_DELAY_ZOOM( value); }
}

void SpuGp::outFINE_DELAY_ZOOM_chn_delay( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_chn_delay(value);}}
	else{ mSubItems[id]->outFINE_DELAY_ZOOM_chn_delay( value); }
}
void SpuGp::pulseFINE_DELAY_ZOOM_chn_delay( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_chn_delay(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_ZOOM_chn_delay(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_ZOOM_chn_delay( activeV, idleV, timeus ); }
}
void SpuGp::outFINE_DELAY_ZOOM_chn_ce( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_chn_ce(value);}}
	else{ mSubItems[id]->outFINE_DELAY_ZOOM_chn_ce( value); }
}
void SpuGp::pulseFINE_DELAY_ZOOM_chn_ce( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_chn_ce(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_ZOOM_chn_ce(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_ZOOM_chn_ce( activeV, idleV, timeus ); }
}
void SpuGp::outFINE_DELAY_ZOOM_offset_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_offset_en(value);}}
	else{ mSubItems[id]->outFINE_DELAY_ZOOM_offset_en( value); }
}
void SpuGp::pulseFINE_DELAY_ZOOM_offset_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_offset_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_ZOOM_offset_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_ZOOM_offset_en( activeV, idleV, timeus ); }
}
void SpuGp::outFINE_DELAY_ZOOM_delay_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_delay_en(value);}}
	else{ mSubItems[id]->outFINE_DELAY_ZOOM_delay_en( value); }
}
void SpuGp::pulseFINE_DELAY_ZOOM_delay_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM_delay_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignFINE_DELAY_ZOOM_delay_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseFINE_DELAY_ZOOM_delay_en( activeV, idleV, timeus ); }
}

void SpuGp::outFINE_DELAY_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM(value);} }
	else{ mSubItems[id]->outFINE_DELAY_ZOOM( value); }
}

void SpuGp::outFINE_DELAY_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_DELAY_ZOOM();} }
	else{ mSubItems[id]->outFINE_DELAY_ZOOM(); }
}


Spu_reg SpuGp::getFINE_DELAY_ZOOM_chn_delay( int id )
{
	return mSubItems[id]->getFINE_DELAY_ZOOM_chn_delay();
}
Spu_reg SpuGp::getFINE_DELAY_ZOOM_chn_ce( int id )
{
	return mSubItems[id]->getFINE_DELAY_ZOOM_chn_ce();
}
Spu_reg SpuGp::getFINE_DELAY_ZOOM_offset_en( int id )
{
	return mSubItems[id]->getFINE_DELAY_ZOOM_offset_en();
}
Spu_reg SpuGp::getFINE_DELAY_ZOOM_delay_en( int id )
{
	return mSubItems[id]->getFINE_DELAY_ZOOM_delay_en();
}

Spu_reg SpuGp::getFINE_DELAY_ZOOM( int id )
{
	return mSubItems[id]->getFINE_DELAY_ZOOM();
}

Spu_reg SpuGp::inFINE_DELAY_ZOOM( int id )
{
	return mSubItems[id]->inFINE_DELAY_ZOOM();
}


void SpuGp::setINTERP_MULT_MAIN_coef_len(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_coef_len(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_coef_len( value); }
}
void SpuGp::setINTERP_MULT_MAIN_comm_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_comm_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_comm_mult( value); }
}
void SpuGp::setINTERP_MULT_MAIN_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_mult( value); }
}
void SpuGp::setINTERP_MULT_MAIN_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_en(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_en( value); }
}

void SpuGp::setINTERP_MULT_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN( value); }
}

void SpuGp::outINTERP_MULT_MAIN_coef_len( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_coef_len(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_coef_len( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_coef_len(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_coef_len(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_coef_len( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_MAIN_comm_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_comm_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_comm_mult( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_comm_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_comm_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_comm_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_MAIN_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_mult( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_MAIN_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_en(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_en( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_en( activeV, idleV, timeus ); }
}

void SpuGp::outINTERP_MULT_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN(value);} }
	else{ mSubItems[id]->outINTERP_MULT_MAIN( value); }
}

void SpuGp::outINTERP_MULT_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN();} }
	else{ mSubItems[id]->outINTERP_MULT_MAIN(); }
}


Spu_reg SpuGp::getINTERP_MULT_MAIN_coef_len( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_coef_len();
}
Spu_reg SpuGp::getINTERP_MULT_MAIN_comm_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_comm_mult();
}
Spu_reg SpuGp::getINTERP_MULT_MAIN_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_mult();
}
Spu_reg SpuGp::getINTERP_MULT_MAIN_en( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_en();
}

Spu_reg SpuGp::getINTERP_MULT_MAIN( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN();
}

Spu_reg SpuGp::inINTERP_MULT_MAIN( int id )
{
	return mSubItems[id]->inINTERP_MULT_MAIN();
}


void SpuGp::setINTERP_MULT_ZOOM_coef_len(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_ZOOM_coef_len(value);} }
	else{ mSubItems[id]->setINTERP_MULT_ZOOM_coef_len( value); }
}
void SpuGp::setINTERP_MULT_ZOOM_comm_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_ZOOM_comm_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_ZOOM_comm_mult( value); }
}
void SpuGp::setINTERP_MULT_ZOOM_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_ZOOM_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_ZOOM_mult( value); }
}
void SpuGp::setINTERP_MULT_ZOOM_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_ZOOM_en(value);} }
	else{ mSubItems[id]->setINTERP_MULT_ZOOM_en( value); }
}

void SpuGp::setINTERP_MULT_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_ZOOM(value);} }
	else{ mSubItems[id]->setINTERP_MULT_ZOOM( value); }
}

void SpuGp::outINTERP_MULT_ZOOM_coef_len( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_coef_len(value);}}
	else{ mSubItems[id]->outINTERP_MULT_ZOOM_coef_len( value); }
}
void SpuGp::pulseINTERP_MULT_ZOOM_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_coef_len(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_ZOOM_coef_len(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_ZOOM_coef_len( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_ZOOM_comm_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_comm_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_ZOOM_comm_mult( value); }
}
void SpuGp::pulseINTERP_MULT_ZOOM_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_comm_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_ZOOM_comm_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_ZOOM_comm_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_ZOOM_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_ZOOM_mult( value); }
}
void SpuGp::pulseINTERP_MULT_ZOOM_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_ZOOM_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_ZOOM_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_ZOOM_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_en(value);}}
	else{ mSubItems[id]->outINTERP_MULT_ZOOM_en( value); }
}
void SpuGp::pulseINTERP_MULT_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_ZOOM_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_ZOOM_en( activeV, idleV, timeus ); }
}

void SpuGp::outINTERP_MULT_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM(value);} }
	else{ mSubItems[id]->outINTERP_MULT_ZOOM( value); }
}

void SpuGp::outINTERP_MULT_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_ZOOM();} }
	else{ mSubItems[id]->outINTERP_MULT_ZOOM(); }
}


Spu_reg SpuGp::getINTERP_MULT_ZOOM_coef_len( int id )
{
	return mSubItems[id]->getINTERP_MULT_ZOOM_coef_len();
}
Spu_reg SpuGp::getINTERP_MULT_ZOOM_comm_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_ZOOM_comm_mult();
}
Spu_reg SpuGp::getINTERP_MULT_ZOOM_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_ZOOM_mult();
}
Spu_reg SpuGp::getINTERP_MULT_ZOOM_en( int id )
{
	return mSubItems[id]->getINTERP_MULT_ZOOM_en();
}

Spu_reg SpuGp::getINTERP_MULT_ZOOM( int id )
{
	return mSubItems[id]->getINTERP_MULT_ZOOM();
}

Spu_reg SpuGp::inINTERP_MULT_ZOOM( int id )
{
	return mSubItems[id]->inINTERP_MULT_ZOOM();
}


void SpuGp::setINTERP_MULT_MAIN_FINE_coef_len(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_FINE_coef_len(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_FINE_coef_len( value); }
}
void SpuGp::setINTERP_MULT_MAIN_FINE_comm_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_FINE_comm_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_FINE_comm_mult( value); }
}
void SpuGp::setINTERP_MULT_MAIN_FINE_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_FINE_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_FINE_mult( value); }
}
void SpuGp::setINTERP_MULT_MAIN_FINE_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_FINE_en(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_FINE_en( value); }
}

void SpuGp::setINTERP_MULT_MAIN_FINE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_MAIN_FINE(value);} }
	else{ mSubItems[id]->setINTERP_MULT_MAIN_FINE( value); }
}

void SpuGp::outINTERP_MULT_MAIN_FINE_coef_len( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_coef_len(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_FINE_coef_len( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_FINE_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_coef_len(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_FINE_coef_len(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_FINE_coef_len( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_comm_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_FINE_comm_mult( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_FINE_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_comm_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_FINE_comm_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_FINE_comm_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_MAIN_FINE_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_FINE_mult( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_FINE_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_FINE_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_FINE_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_MAIN_FINE_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_en(value);}}
	else{ mSubItems[id]->outINTERP_MULT_MAIN_FINE_en( value); }
}
void SpuGp::pulseINTERP_MULT_MAIN_FINE_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_MAIN_FINE_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_MAIN_FINE_en( activeV, idleV, timeus ); }
}

void SpuGp::outINTERP_MULT_MAIN_FINE(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE(value);} }
	else{ mSubItems[id]->outINTERP_MULT_MAIN_FINE( value); }
}

void SpuGp::outINTERP_MULT_MAIN_FINE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_MAIN_FINE();} }
	else{ mSubItems[id]->outINTERP_MULT_MAIN_FINE(); }
}


Spu_reg SpuGp::getINTERP_MULT_MAIN_FINE_coef_len( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_FINE_coef_len();
}
Spu_reg SpuGp::getINTERP_MULT_MAIN_FINE_comm_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_FINE_comm_mult();
}
Spu_reg SpuGp::getINTERP_MULT_MAIN_FINE_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_FINE_mult();
}
Spu_reg SpuGp::getINTERP_MULT_MAIN_FINE_en( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_FINE_en();
}

Spu_reg SpuGp::getINTERP_MULT_MAIN_FINE( int id )
{
	return mSubItems[id]->getINTERP_MULT_MAIN_FINE();
}

Spu_reg SpuGp::inINTERP_MULT_MAIN_FINE( int id )
{
	return mSubItems[id]->inINTERP_MULT_MAIN_FINE();
}


void SpuGp::setINTERP_MULT_EYE_coef_len(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_EYE_coef_len(value);} }
	else{ mSubItems[id]->setINTERP_MULT_EYE_coef_len( value); }
}
void SpuGp::setINTERP_MULT_EYE_comm_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_EYE_comm_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_EYE_comm_mult( value); }
}
void SpuGp::setINTERP_MULT_EYE_mult(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_EYE_mult(value);} }
	else{ mSubItems[id]->setINTERP_MULT_EYE_mult( value); }
}
void SpuGp::setINTERP_MULT_EYE_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_EYE_en(value);} }
	else{ mSubItems[id]->setINTERP_MULT_EYE_en( value); }
}

void SpuGp::setINTERP_MULT_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_MULT_EYE(value);} }
	else{ mSubItems[id]->setINTERP_MULT_EYE( value); }
}

void SpuGp::outINTERP_MULT_EYE_coef_len( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_coef_len(value);}}
	else{ mSubItems[id]->outINTERP_MULT_EYE_coef_len( value); }
}
void SpuGp::pulseINTERP_MULT_EYE_coef_len( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_coef_len(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_EYE_coef_len(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_EYE_coef_len( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_EYE_comm_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_comm_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_EYE_comm_mult( value); }
}
void SpuGp::pulseINTERP_MULT_EYE_comm_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_comm_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_EYE_comm_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_EYE_comm_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_EYE_mult( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_mult(value);}}
	else{ mSubItems[id]->outINTERP_MULT_EYE_mult( value); }
}
void SpuGp::pulseINTERP_MULT_EYE_mult( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_mult(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_EYE_mult(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_EYE_mult( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_MULT_EYE_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_en(value);}}
	else{ mSubItems[id]->outINTERP_MULT_EYE_en( value); }
}
void SpuGp::pulseINTERP_MULT_EYE_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_MULT_EYE_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_MULT_EYE_en( activeV, idleV, timeus ); }
}

void SpuGp::outINTERP_MULT_EYE(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE(value);} }
	else{ mSubItems[id]->outINTERP_MULT_EYE( value); }
}

void SpuGp::outINTERP_MULT_EYE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_MULT_EYE();} }
	else{ mSubItems[id]->outINTERP_MULT_EYE(); }
}


Spu_reg SpuGp::getINTERP_MULT_EYE_coef_len( int id )
{
	return mSubItems[id]->getINTERP_MULT_EYE_coef_len();
}
Spu_reg SpuGp::getINTERP_MULT_EYE_comm_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_EYE_comm_mult();
}
Spu_reg SpuGp::getINTERP_MULT_EYE_mult( int id )
{
	return mSubItems[id]->getINTERP_MULT_EYE_mult();
}
Spu_reg SpuGp::getINTERP_MULT_EYE_en( int id )
{
	return mSubItems[id]->getINTERP_MULT_EYE_en();
}

Spu_reg SpuGp::getINTERP_MULT_EYE( int id )
{
	return mSubItems[id]->getINTERP_MULT_EYE();
}

Spu_reg SpuGp::inINTERP_MULT_EYE( int id )
{
	return mSubItems[id]->inINTERP_MULT_EYE();
}


void SpuGp::setINTERP_COEF_TAP_tap(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_TAP_tap(value);} }
	else{ mSubItems[id]->setINTERP_COEF_TAP_tap( value); }
}
void SpuGp::setINTERP_COEF_TAP_ce(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_TAP_ce(value);} }
	else{ mSubItems[id]->setINTERP_COEF_TAP_ce( value); }
}

void SpuGp::setINTERP_COEF_TAP( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_TAP(value);} }
	else{ mSubItems[id]->setINTERP_COEF_TAP( value); }
}

void SpuGp::outINTERP_COEF_TAP_tap( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_TAP_tap(value);}}
	else{ mSubItems[id]->outINTERP_COEF_TAP_tap( value); }
}
void SpuGp::pulseINTERP_COEF_TAP_tap( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_TAP_tap(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_COEF_TAP_tap(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_COEF_TAP_tap( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_COEF_TAP_ce( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_TAP_ce(value);}}
	else{ mSubItems[id]->outINTERP_COEF_TAP_ce( value); }
}
void SpuGp::pulseINTERP_COEF_TAP_ce( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_TAP_ce(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_COEF_TAP_ce(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_COEF_TAP_ce( activeV, idleV, timeus ); }
}

void SpuGp::outINTERP_COEF_TAP(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_TAP(value);} }
	else{ mSubItems[id]->outINTERP_COEF_TAP( value); }
}

void SpuGp::outINTERP_COEF_TAP( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_TAP();} }
	else{ mSubItems[id]->outINTERP_COEF_TAP(); }
}


void SpuGp::setINTERP_COEF_WR_wdata(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_WR_wdata(value);} }
	else{ mSubItems[id]->setINTERP_COEF_WR_wdata( value); }
}
void SpuGp::setINTERP_COEF_WR_waddr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_WR_waddr(value);} }
	else{ mSubItems[id]->setINTERP_COEF_WR_waddr( value); }
}
void SpuGp::setINTERP_COEF_WR_coef_group(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_WR_coef_group(value);} }
	else{ mSubItems[id]->setINTERP_COEF_WR_coef_group( value); }
}
void SpuGp::setINTERP_COEF_WR_wr_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_WR_wr_en(value);} }
	else{ mSubItems[id]->setINTERP_COEF_WR_wr_en( value); }
}

void SpuGp::setINTERP_COEF_WR( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setINTERP_COEF_WR(value);} }
	else{ mSubItems[id]->setINTERP_COEF_WR( value); }
}

void SpuGp::outINTERP_COEF_WR_wdata( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_wdata(value);}}
	else{ mSubItems[id]->outINTERP_COEF_WR_wdata( value); }
}
void SpuGp::pulseINTERP_COEF_WR_wdata( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_wdata(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_COEF_WR_wdata(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_COEF_WR_wdata( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_COEF_WR_waddr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_waddr(value);}}
	else{ mSubItems[id]->outINTERP_COEF_WR_waddr( value); }
}
void SpuGp::pulseINTERP_COEF_WR_waddr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_waddr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_COEF_WR_waddr(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_COEF_WR_waddr( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_COEF_WR_coef_group( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_coef_group(value);}}
	else{ mSubItems[id]->outINTERP_COEF_WR_coef_group( value); }
}
void SpuGp::pulseINTERP_COEF_WR_coef_group( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_coef_group(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_COEF_WR_coef_group(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_COEF_WR_coef_group( activeV, idleV, timeus ); }
}
void SpuGp::outINTERP_COEF_WR_wr_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_wr_en(value);}}
	else{ mSubItems[id]->outINTERP_COEF_WR_wr_en( value); }
}
void SpuGp::pulseINTERP_COEF_WR_wr_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR_wr_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignINTERP_COEF_WR_wr_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseINTERP_COEF_WR_wr_en( activeV, idleV, timeus ); }
}

void SpuGp::outINTERP_COEF_WR(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR(value);} }
	else{ mSubItems[id]->outINTERP_COEF_WR( value); }
}

void SpuGp::outINTERP_COEF_WR( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outINTERP_COEF_WR();} }
	else{ mSubItems[id]->outINTERP_COEF_WR(); }
}


void SpuGp::setCOMPRESS_MAIN_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_MAIN_rate(value);} }
	else{ mSubItems[id]->setCOMPRESS_MAIN_rate( value); }
}
void SpuGp::setCOMPRESS_MAIN_peak(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_MAIN_peak(value);} }
	else{ mSubItems[id]->setCOMPRESS_MAIN_peak( value); }
}
void SpuGp::setCOMPRESS_MAIN_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_MAIN_en(value);} }
	else{ mSubItems[id]->setCOMPRESS_MAIN_en( value); }
}

void SpuGp::setCOMPRESS_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_MAIN(value);} }
	else{ mSubItems[id]->setCOMPRESS_MAIN( value); }
}

void SpuGp::outCOMPRESS_MAIN_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN_rate(value);}}
	else{ mSubItems[id]->outCOMPRESS_MAIN_rate( value); }
}
void SpuGp::pulseCOMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_MAIN_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_MAIN_rate( activeV, idleV, timeus ); }
}
void SpuGp::outCOMPRESS_MAIN_peak( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN_peak(value);}}
	else{ mSubItems[id]->outCOMPRESS_MAIN_peak( value); }
}
void SpuGp::pulseCOMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN_peak(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_MAIN_peak(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_MAIN_peak( activeV, idleV, timeus ); }
}
void SpuGp::outCOMPRESS_MAIN_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN_en(value);}}
	else{ mSubItems[id]->outCOMPRESS_MAIN_en( value); }
}
void SpuGp::pulseCOMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_MAIN_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_MAIN_en( activeV, idleV, timeus ); }
}

void SpuGp::outCOMPRESS_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN(value);} }
	else{ mSubItems[id]->outCOMPRESS_MAIN( value); }
}

void SpuGp::outCOMPRESS_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_MAIN();} }
	else{ mSubItems[id]->outCOMPRESS_MAIN(); }
}


Spu_reg SpuGp::getCOMPRESS_MAIN_rate( int id )
{
	return mSubItems[id]->getCOMPRESS_MAIN_rate();
}
Spu_reg SpuGp::getCOMPRESS_MAIN_peak( int id )
{
	return mSubItems[id]->getCOMPRESS_MAIN_peak();
}
Spu_reg SpuGp::getCOMPRESS_MAIN_en( int id )
{
	return mSubItems[id]->getCOMPRESS_MAIN_en();
}

Spu_reg SpuGp::getCOMPRESS_MAIN( int id )
{
	return mSubItems[id]->getCOMPRESS_MAIN();
}

Spu_reg SpuGp::inCOMPRESS_MAIN( int id )
{
	return mSubItems[id]->inCOMPRESS_MAIN();
}


void SpuGp::setCOMPRESS_ZOOM_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_ZOOM_rate(value);} }
	else{ mSubItems[id]->setCOMPRESS_ZOOM_rate( value); }
}
void SpuGp::setCOMPRESS_ZOOM_peak(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_ZOOM_peak(value);} }
	else{ mSubItems[id]->setCOMPRESS_ZOOM_peak( value); }
}
void SpuGp::setCOMPRESS_ZOOM_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_ZOOM_en(value);} }
	else{ mSubItems[id]->setCOMPRESS_ZOOM_en( value); }
}

void SpuGp::setCOMPRESS_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_ZOOM(value);} }
	else{ mSubItems[id]->setCOMPRESS_ZOOM( value); }
}

void SpuGp::outCOMPRESS_ZOOM_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM_rate(value);}}
	else{ mSubItems[id]->outCOMPRESS_ZOOM_rate( value); }
}
void SpuGp::pulseCOMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_ZOOM_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_ZOOM_rate( activeV, idleV, timeus ); }
}
void SpuGp::outCOMPRESS_ZOOM_peak( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM_peak(value);}}
	else{ mSubItems[id]->outCOMPRESS_ZOOM_peak( value); }
}
void SpuGp::pulseCOMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM_peak(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_ZOOM_peak(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_ZOOM_peak( activeV, idleV, timeus ); }
}
void SpuGp::outCOMPRESS_ZOOM_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM_en(value);}}
	else{ mSubItems[id]->outCOMPRESS_ZOOM_en( value); }
}
void SpuGp::pulseCOMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_ZOOM_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_ZOOM_en( activeV, idleV, timeus ); }
}

void SpuGp::outCOMPRESS_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM(value);} }
	else{ mSubItems[id]->outCOMPRESS_ZOOM( value); }
}

void SpuGp::outCOMPRESS_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_ZOOM();} }
	else{ mSubItems[id]->outCOMPRESS_ZOOM(); }
}


Spu_reg SpuGp::getCOMPRESS_ZOOM_rate( int id )
{
	return mSubItems[id]->getCOMPRESS_ZOOM_rate();
}
Spu_reg SpuGp::getCOMPRESS_ZOOM_peak( int id )
{
	return mSubItems[id]->getCOMPRESS_ZOOM_peak();
}
Spu_reg SpuGp::getCOMPRESS_ZOOM_en( int id )
{
	return mSubItems[id]->getCOMPRESS_ZOOM_en();
}

Spu_reg SpuGp::getCOMPRESS_ZOOM( int id )
{
	return mSubItems[id]->getCOMPRESS_ZOOM();
}

Spu_reg SpuGp::inCOMPRESS_ZOOM( int id )
{
	return mSubItems[id]->inCOMPRESS_ZOOM();
}


void SpuGp::setLA_FINE_DELAY_MAIN_delay(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_FINE_DELAY_MAIN_delay(value);} }
	else{ mSubItems[id]->setLA_FINE_DELAY_MAIN_delay( value); }
}
void SpuGp::setLA_FINE_DELAY_MAIN_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_FINE_DELAY_MAIN_en(value);} }
	else{ mSubItems[id]->setLA_FINE_DELAY_MAIN_en( value); }
}

void SpuGp::setLA_FINE_DELAY_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_FINE_DELAY_MAIN(value);} }
	else{ mSubItems[id]->setLA_FINE_DELAY_MAIN( value); }
}

void SpuGp::outLA_FINE_DELAY_MAIN_delay( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_MAIN_delay(value);}}
	else{ mSubItems[id]->outLA_FINE_DELAY_MAIN_delay( value); }
}
void SpuGp::pulseLA_FINE_DELAY_MAIN_delay( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_MAIN_delay(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_FINE_DELAY_MAIN_delay(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_FINE_DELAY_MAIN_delay( activeV, idleV, timeus ); }
}
void SpuGp::outLA_FINE_DELAY_MAIN_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_MAIN_en(value);}}
	else{ mSubItems[id]->outLA_FINE_DELAY_MAIN_en( value); }
}
void SpuGp::pulseLA_FINE_DELAY_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_MAIN_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_FINE_DELAY_MAIN_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_FINE_DELAY_MAIN_en( activeV, idleV, timeus ); }
}

void SpuGp::outLA_FINE_DELAY_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_MAIN(value);} }
	else{ mSubItems[id]->outLA_FINE_DELAY_MAIN( value); }
}

void SpuGp::outLA_FINE_DELAY_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_MAIN();} }
	else{ mSubItems[id]->outLA_FINE_DELAY_MAIN(); }
}


Spu_reg SpuGp::getLA_FINE_DELAY_MAIN_delay( int id )
{
	return mSubItems[id]->getLA_FINE_DELAY_MAIN_delay();
}
Spu_reg SpuGp::getLA_FINE_DELAY_MAIN_en( int id )
{
	return mSubItems[id]->getLA_FINE_DELAY_MAIN_en();
}

Spu_reg SpuGp::getLA_FINE_DELAY_MAIN( int id )
{
	return mSubItems[id]->getLA_FINE_DELAY_MAIN();
}

Spu_reg SpuGp::inLA_FINE_DELAY_MAIN( int id )
{
	return mSubItems[id]->inLA_FINE_DELAY_MAIN();
}


void SpuGp::setLA_FINE_DELAY_ZOOM_delay(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_FINE_DELAY_ZOOM_delay(value);} }
	else{ mSubItems[id]->setLA_FINE_DELAY_ZOOM_delay( value); }
}
void SpuGp::setLA_FINE_DELAY_ZOOM_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_FINE_DELAY_ZOOM_en(value);} }
	else{ mSubItems[id]->setLA_FINE_DELAY_ZOOM_en( value); }
}

void SpuGp::setLA_FINE_DELAY_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_FINE_DELAY_ZOOM(value);} }
	else{ mSubItems[id]->setLA_FINE_DELAY_ZOOM( value); }
}

void SpuGp::outLA_FINE_DELAY_ZOOM_delay( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_ZOOM_delay(value);}}
	else{ mSubItems[id]->outLA_FINE_DELAY_ZOOM_delay( value); }
}
void SpuGp::pulseLA_FINE_DELAY_ZOOM_delay( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_ZOOM_delay(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_FINE_DELAY_ZOOM_delay(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_FINE_DELAY_ZOOM_delay( activeV, idleV, timeus ); }
}
void SpuGp::outLA_FINE_DELAY_ZOOM_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_ZOOM_en(value);}}
	else{ mSubItems[id]->outLA_FINE_DELAY_ZOOM_en( value); }
}
void SpuGp::pulseLA_FINE_DELAY_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_ZOOM_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_FINE_DELAY_ZOOM_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_FINE_DELAY_ZOOM_en( activeV, idleV, timeus ); }
}

void SpuGp::outLA_FINE_DELAY_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_ZOOM(value);} }
	else{ mSubItems[id]->outLA_FINE_DELAY_ZOOM( value); }
}

void SpuGp::outLA_FINE_DELAY_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_FINE_DELAY_ZOOM();} }
	else{ mSubItems[id]->outLA_FINE_DELAY_ZOOM(); }
}


Spu_reg SpuGp::getLA_FINE_DELAY_ZOOM_delay( int id )
{
	return mSubItems[id]->getLA_FINE_DELAY_ZOOM_delay();
}
Spu_reg SpuGp::getLA_FINE_DELAY_ZOOM_en( int id )
{
	return mSubItems[id]->getLA_FINE_DELAY_ZOOM_en();
}

Spu_reg SpuGp::getLA_FINE_DELAY_ZOOM( int id )
{
	return mSubItems[id]->getLA_FINE_DELAY_ZOOM();
}

Spu_reg SpuGp::inLA_FINE_DELAY_ZOOM( int id )
{
	return mSubItems[id]->inLA_FINE_DELAY_ZOOM();
}


void SpuGp::setLA_COMP_MAIN_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_COMP_MAIN_rate(value);} }
	else{ mSubItems[id]->setLA_COMP_MAIN_rate( value); }
}
void SpuGp::setLA_COMP_MAIN_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_COMP_MAIN_en(value);} }
	else{ mSubItems[id]->setLA_COMP_MAIN_en( value); }
}

void SpuGp::setLA_COMP_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_COMP_MAIN(value);} }
	else{ mSubItems[id]->setLA_COMP_MAIN( value); }
}

void SpuGp::outLA_COMP_MAIN_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_MAIN_rate(value);}}
	else{ mSubItems[id]->outLA_COMP_MAIN_rate( value); }
}
void SpuGp::pulseLA_COMP_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_MAIN_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_COMP_MAIN_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_COMP_MAIN_rate( activeV, idleV, timeus ); }
}
void SpuGp::outLA_COMP_MAIN_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_MAIN_en(value);}}
	else{ mSubItems[id]->outLA_COMP_MAIN_en( value); }
}
void SpuGp::pulseLA_COMP_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_MAIN_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_COMP_MAIN_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_COMP_MAIN_en( activeV, idleV, timeus ); }
}

void SpuGp::outLA_COMP_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_MAIN(value);} }
	else{ mSubItems[id]->outLA_COMP_MAIN( value); }
}

void SpuGp::outLA_COMP_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_MAIN();} }
	else{ mSubItems[id]->outLA_COMP_MAIN(); }
}


Spu_reg SpuGp::getLA_COMP_MAIN_rate( int id )
{
	return mSubItems[id]->getLA_COMP_MAIN_rate();
}
Spu_reg SpuGp::getLA_COMP_MAIN_en( int id )
{
	return mSubItems[id]->getLA_COMP_MAIN_en();
}

Spu_reg SpuGp::getLA_COMP_MAIN( int id )
{
	return mSubItems[id]->getLA_COMP_MAIN();
}

Spu_reg SpuGp::inLA_COMP_MAIN( int id )
{
	return mSubItems[id]->inLA_COMP_MAIN();
}


void SpuGp::setLA_COMP_ZOOM_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_COMP_ZOOM_rate(value);} }
	else{ mSubItems[id]->setLA_COMP_ZOOM_rate( value); }
}
void SpuGp::setLA_COMP_ZOOM_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_COMP_ZOOM_en(value);} }
	else{ mSubItems[id]->setLA_COMP_ZOOM_en( value); }
}

void SpuGp::setLA_COMP_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_COMP_ZOOM(value);} }
	else{ mSubItems[id]->setLA_COMP_ZOOM( value); }
}

void SpuGp::outLA_COMP_ZOOM_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_ZOOM_rate(value);}}
	else{ mSubItems[id]->outLA_COMP_ZOOM_rate( value); }
}
void SpuGp::pulseLA_COMP_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_ZOOM_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_COMP_ZOOM_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_COMP_ZOOM_rate( activeV, idleV, timeus ); }
}
void SpuGp::outLA_COMP_ZOOM_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_ZOOM_en(value);}}
	else{ mSubItems[id]->outLA_COMP_ZOOM_en( value); }
}
void SpuGp::pulseLA_COMP_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_ZOOM_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_COMP_ZOOM_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_COMP_ZOOM_en( activeV, idleV, timeus ); }
}

void SpuGp::outLA_COMP_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_ZOOM(value);} }
	else{ mSubItems[id]->outLA_COMP_ZOOM( value); }
}

void SpuGp::outLA_COMP_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_COMP_ZOOM();} }
	else{ mSubItems[id]->outLA_COMP_ZOOM(); }
}


Spu_reg SpuGp::getLA_COMP_ZOOM_rate( int id )
{
	return mSubItems[id]->getLA_COMP_ZOOM_rate();
}
Spu_reg SpuGp::getLA_COMP_ZOOM_en( int id )
{
	return mSubItems[id]->getLA_COMP_ZOOM_en();
}

Spu_reg SpuGp::getLA_COMP_ZOOM( int id )
{
	return mSubItems[id]->getLA_COMP_ZOOM();
}

Spu_reg SpuGp::inLA_COMP_ZOOM( int id )
{
	return mSubItems[id]->inLA_COMP_ZOOM();
}


void SpuGp::setTX_LEN_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_CH(value);} }
	else{ mSubItems[id]->setTX_LEN_CH( value); }
}

void SpuGp::outTX_LEN_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_CH(value);} }
	else{ mSubItems[id]->outTX_LEN_CH( value); }
}

void SpuGp::outTX_LEN_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_CH();} }
	else{ mSubItems[id]->outTX_LEN_CH(); }
}


Spu_reg SpuGp::getTX_LEN_CH( int id )
{
	return mSubItems[id]->getTX_LEN_CH();
}

Spu_reg SpuGp::inTX_LEN_CH( int id )
{
	return mSubItems[id]->inTX_LEN_CH();
}


void SpuGp::setTX_LEN_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_LA(value);} }
	else{ mSubItems[id]->setTX_LEN_LA( value); }
}

void SpuGp::outTX_LEN_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_LA(value);} }
	else{ mSubItems[id]->outTX_LEN_LA( value); }
}

void SpuGp::outTX_LEN_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_LA();} }
	else{ mSubItems[id]->outTX_LEN_LA(); }
}


Spu_reg SpuGp::getTX_LEN_LA( int id )
{
	return mSubItems[id]->getTX_LEN_LA();
}

Spu_reg SpuGp::inTX_LEN_LA( int id )
{
	return mSubItems[id]->inTX_LEN_LA();
}


void SpuGp::setTX_LEN_ZOOM_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_ZOOM_CH(value);} }
	else{ mSubItems[id]->setTX_LEN_ZOOM_CH( value); }
}

void SpuGp::outTX_LEN_ZOOM_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_ZOOM_CH(value);} }
	else{ mSubItems[id]->outTX_LEN_ZOOM_CH( value); }
}

void SpuGp::outTX_LEN_ZOOM_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_ZOOM_CH();} }
	else{ mSubItems[id]->outTX_LEN_ZOOM_CH(); }
}


Spu_reg SpuGp::getTX_LEN_ZOOM_CH( int id )
{
	return mSubItems[id]->getTX_LEN_ZOOM_CH();
}

Spu_reg SpuGp::inTX_LEN_ZOOM_CH( int id )
{
	return mSubItems[id]->inTX_LEN_ZOOM_CH();
}


void SpuGp::setTX_LEN_ZOOM_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_ZOOM_LA(value);} }
	else{ mSubItems[id]->setTX_LEN_ZOOM_LA( value); }
}

void SpuGp::outTX_LEN_ZOOM_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_ZOOM_LA(value);} }
	else{ mSubItems[id]->outTX_LEN_ZOOM_LA( value); }
}

void SpuGp::outTX_LEN_ZOOM_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_ZOOM_LA();} }
	else{ mSubItems[id]->outTX_LEN_ZOOM_LA(); }
}


Spu_reg SpuGp::getTX_LEN_ZOOM_LA( int id )
{
	return mSubItems[id]->getTX_LEN_ZOOM_LA();
}

Spu_reg SpuGp::inTX_LEN_ZOOM_LA( int id )
{
	return mSubItems[id]->inTX_LEN_ZOOM_LA();
}


void SpuGp::setTX_LEN_TRACE_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_TRACE_CH(value);} }
	else{ mSubItems[id]->setTX_LEN_TRACE_CH( value); }
}

void SpuGp::outTX_LEN_TRACE_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_TRACE_CH(value);} }
	else{ mSubItems[id]->outTX_LEN_TRACE_CH( value); }
}

void SpuGp::outTX_LEN_TRACE_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_TRACE_CH();} }
	else{ mSubItems[id]->outTX_LEN_TRACE_CH(); }
}


Spu_reg SpuGp::getTX_LEN_TRACE_CH( int id )
{
	return mSubItems[id]->getTX_LEN_TRACE_CH();
}

Spu_reg SpuGp::inTX_LEN_TRACE_CH( int id )
{
	return mSubItems[id]->inTX_LEN_TRACE_CH();
}


void SpuGp::setTX_LEN_TRACE_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_TRACE_LA(value);} }
	else{ mSubItems[id]->setTX_LEN_TRACE_LA( value); }
}

void SpuGp::outTX_LEN_TRACE_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_TRACE_LA(value);} }
	else{ mSubItems[id]->outTX_LEN_TRACE_LA( value); }
}

void SpuGp::outTX_LEN_TRACE_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_TRACE_LA();} }
	else{ mSubItems[id]->outTX_LEN_TRACE_LA(); }
}


Spu_reg SpuGp::getTX_LEN_TRACE_LA( int id )
{
	return mSubItems[id]->getTX_LEN_TRACE_LA();
}

Spu_reg SpuGp::inTX_LEN_TRACE_LA( int id )
{
	return mSubItems[id]->inTX_LEN_TRACE_LA();
}


void SpuGp::setTX_LEN_COL_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_COL_CH(value);} }
	else{ mSubItems[id]->setTX_LEN_COL_CH( value); }
}

void SpuGp::outTX_LEN_COL_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_COL_CH(value);} }
	else{ mSubItems[id]->outTX_LEN_COL_CH( value); }
}

void SpuGp::outTX_LEN_COL_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_COL_CH();} }
	else{ mSubItems[id]->outTX_LEN_COL_CH(); }
}


Spu_reg SpuGp::getTX_LEN_COL_CH( int id )
{
	return mSubItems[id]->getTX_LEN_COL_CH();
}

Spu_reg SpuGp::inTX_LEN_COL_CH( int id )
{
	return mSubItems[id]->inTX_LEN_COL_CH();
}


void SpuGp::setTX_LEN_COL_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_COL_LA(value);} }
	else{ mSubItems[id]->setTX_LEN_COL_LA( value); }
}

void SpuGp::outTX_LEN_COL_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_COL_LA(value);} }
	else{ mSubItems[id]->outTX_LEN_COL_LA( value); }
}

void SpuGp::outTX_LEN_COL_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_COL_LA();} }
	else{ mSubItems[id]->outTX_LEN_COL_LA(); }
}


Spu_reg SpuGp::getTX_LEN_COL_LA( int id )
{
	return mSubItems[id]->getTX_LEN_COL_LA();
}

Spu_reg SpuGp::inTX_LEN_COL_LA( int id )
{
	return mSubItems[id]->inTX_LEN_COL_LA();
}


void SpuGp::setDEBUG_ACQ_samp_extract_vld(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ACQ_samp_extract_vld(value);} }
	else{ mSubItems[id]->setDEBUG_ACQ_samp_extract_vld( value); }
}
void SpuGp::setDEBUG_ACQ_acq_dly_cfg_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ACQ_acq_dly_cfg_en(value);} }
	else{ mSubItems[id]->setDEBUG_ACQ_acq_dly_cfg_en( value); }
}
void SpuGp::setDEBUG_ACQ_acq_dly_cfg(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ACQ_acq_dly_cfg(value);} }
	else{ mSubItems[id]->setDEBUG_ACQ_acq_dly_cfg( value); }
}
void SpuGp::setDEBUG_ACQ_trig_src_type(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ACQ_trig_src_type(value);} }
	else{ mSubItems[id]->setDEBUG_ACQ_trig_src_type( value); }
}

void SpuGp::setDEBUG_ACQ( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_ACQ(value);} }
	else{ mSubItems[id]->setDEBUG_ACQ( value); }
}

void SpuGp::outDEBUG_ACQ_samp_extract_vld( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_samp_extract_vld(value);}}
	else{ mSubItems[id]->outDEBUG_ACQ_samp_extract_vld( value); }
}
void SpuGp::pulseDEBUG_ACQ_samp_extract_vld( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_samp_extract_vld(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ACQ_samp_extract_vld(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ACQ_samp_extract_vld( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_ACQ_acq_dly_cfg_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_acq_dly_cfg_en(value);}}
	else{ mSubItems[id]->outDEBUG_ACQ_acq_dly_cfg_en( value); }
}
void SpuGp::pulseDEBUG_ACQ_acq_dly_cfg_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_acq_dly_cfg_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ACQ_acq_dly_cfg_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ACQ_acq_dly_cfg_en( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_ACQ_acq_dly_cfg( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_acq_dly_cfg(value);}}
	else{ mSubItems[id]->outDEBUG_ACQ_acq_dly_cfg( value); }
}
void SpuGp::pulseDEBUG_ACQ_acq_dly_cfg( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_acq_dly_cfg(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ACQ_acq_dly_cfg(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ACQ_acq_dly_cfg( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_ACQ_trig_src_type( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_trig_src_type(value);}}
	else{ mSubItems[id]->outDEBUG_ACQ_trig_src_type( value); }
}
void SpuGp::pulseDEBUG_ACQ_trig_src_type( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ_trig_src_type(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_ACQ_trig_src_type(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_ACQ_trig_src_type( activeV, idleV, timeus ); }
}

void SpuGp::outDEBUG_ACQ(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ(value);} }
	else{ mSubItems[id]->outDEBUG_ACQ( value); }
}

void SpuGp::outDEBUG_ACQ( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_ACQ();} }
	else{ mSubItems[id]->outDEBUG_ACQ(); }
}


Spu_reg SpuGp::getDEBUG_ACQ_samp_extract_vld( int id )
{
	return mSubItems[id]->getDEBUG_ACQ_samp_extract_vld();
}
Spu_reg SpuGp::getDEBUG_ACQ_acq_dly_cfg_en( int id )
{
	return mSubItems[id]->getDEBUG_ACQ_acq_dly_cfg_en();
}
Spu_reg SpuGp::getDEBUG_ACQ_acq_dly_cfg( int id )
{
	return mSubItems[id]->getDEBUG_ACQ_acq_dly_cfg();
}
Spu_reg SpuGp::getDEBUG_ACQ_trig_src_type( int id )
{
	return mSubItems[id]->getDEBUG_ACQ_trig_src_type();
}

Spu_reg SpuGp::getDEBUG_ACQ( int id )
{
	return mSubItems[id]->getDEBUG_ACQ();
}

Spu_reg SpuGp::inDEBUG_ACQ( int id )
{
	return mSubItems[id]->inDEBUG_ACQ();
}


void SpuGp::setDEBUG_TX_spu_tx_cnt(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_TX_spu_tx_cnt(value);} }
	else{ mSubItems[id]->setDEBUG_TX_spu_tx_cnt( value); }
}
void SpuGp::setDEBUG_TX_spu_frm_fsm(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_TX_spu_frm_fsm(value);} }
	else{ mSubItems[id]->setDEBUG_TX_spu_frm_fsm( value); }
}
void SpuGp::setDEBUG_TX_tx_cnt_clr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_TX_tx_cnt_clr(value);} }
	else{ mSubItems[id]->setDEBUG_TX_tx_cnt_clr( value); }
}
void SpuGp::setDEBUG_TX_v_tx_bypass(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_TX_v_tx_bypass(value);} }
	else{ mSubItems[id]->setDEBUG_TX_v_tx_bypass( value); }
}
void SpuGp::setDEBUG_TX_frm_head_jmp(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_TX_frm_head_jmp(value);} }
	else{ mSubItems[id]->setDEBUG_TX_frm_head_jmp( value); }
}

void SpuGp::setDEBUG_TX( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_TX(value);} }
	else{ mSubItems[id]->setDEBUG_TX( value); }
}

void SpuGp::outDEBUG_TX_spu_tx_cnt( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_spu_tx_cnt(value);}}
	else{ mSubItems[id]->outDEBUG_TX_spu_tx_cnt( value); }
}
void SpuGp::pulseDEBUG_TX_spu_tx_cnt( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_spu_tx_cnt(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_TX_spu_tx_cnt(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_TX_spu_tx_cnt( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_TX_spu_frm_fsm( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_spu_frm_fsm(value);}}
	else{ mSubItems[id]->outDEBUG_TX_spu_frm_fsm( value); }
}
void SpuGp::pulseDEBUG_TX_spu_frm_fsm( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_spu_frm_fsm(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_TX_spu_frm_fsm(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_TX_spu_frm_fsm( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_TX_tx_cnt_clr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_tx_cnt_clr(value);}}
	else{ mSubItems[id]->outDEBUG_TX_tx_cnt_clr( value); }
}
void SpuGp::pulseDEBUG_TX_tx_cnt_clr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_tx_cnt_clr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_TX_tx_cnt_clr(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_TX_tx_cnt_clr( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_TX_v_tx_bypass( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_v_tx_bypass(value);}}
	else{ mSubItems[id]->outDEBUG_TX_v_tx_bypass( value); }
}
void SpuGp::pulseDEBUG_TX_v_tx_bypass( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_v_tx_bypass(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_TX_v_tx_bypass(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_TX_v_tx_bypass( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_TX_frm_head_jmp( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_frm_head_jmp(value);}}
	else{ mSubItems[id]->outDEBUG_TX_frm_head_jmp( value); }
}
void SpuGp::pulseDEBUG_TX_frm_head_jmp( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX_frm_head_jmp(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_TX_frm_head_jmp(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_TX_frm_head_jmp( activeV, idleV, timeus ); }
}

void SpuGp::outDEBUG_TX(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX(value);} }
	else{ mSubItems[id]->outDEBUG_TX( value); }
}

void SpuGp::outDEBUG_TX( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_TX();} }
	else{ mSubItems[id]->outDEBUG_TX(); }
}


Spu_reg SpuGp::getDEBUG_TX_spu_tx_cnt( int id )
{
	return mSubItems[id]->getDEBUG_TX_spu_tx_cnt();
}
Spu_reg SpuGp::getDEBUG_TX_spu_frm_fsm( int id )
{
	return mSubItems[id]->getDEBUG_TX_spu_frm_fsm();
}
Spu_reg SpuGp::getDEBUG_TX_tx_cnt_clr( int id )
{
	return mSubItems[id]->getDEBUG_TX_tx_cnt_clr();
}
Spu_reg SpuGp::getDEBUG_TX_v_tx_bypass( int id )
{
	return mSubItems[id]->getDEBUG_TX_v_tx_bypass();
}
Spu_reg SpuGp::getDEBUG_TX_frm_head_jmp( int id )
{
	return mSubItems[id]->getDEBUG_TX_frm_head_jmp();
}

Spu_reg SpuGp::getDEBUG_TX( int id )
{
	return mSubItems[id]->getDEBUG_TX();
}

Spu_reg SpuGp::inDEBUG_TX( int id )
{
	return mSubItems[id]->inDEBUG_TX();
}


void SpuGp::setDEBUG_MEM_trig_tpu_coarse(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_MEM_trig_tpu_coarse(value);} }
	else{ mSubItems[id]->setDEBUG_MEM_trig_tpu_coarse( value); }
}
void SpuGp::setDEBUG_MEM_mem_bram_dly(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_MEM_mem_bram_dly(value);} }
	else{ mSubItems[id]->setDEBUG_MEM_mem_bram_dly( value); }
}
void SpuGp::setDEBUG_MEM_trig_offset_dot_pre(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_MEM_trig_offset_dot_pre(value);} }
	else{ mSubItems[id]->setDEBUG_MEM_trig_offset_dot_pre( value); }
}
void SpuGp::setDEBUG_MEM_trig_dly_dot_out(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_MEM_trig_dly_dot_out(value);} }
	else{ mSubItems[id]->setDEBUG_MEM_trig_dly_dot_out( value); }
}
void SpuGp::setDEBUG_MEM_trig_coarse_fine(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_MEM_trig_coarse_fine(value);} }
	else{ mSubItems[id]->setDEBUG_MEM_trig_coarse_fine( value); }
}
void SpuGp::setDEBUG_MEM_mem_last_dot(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_MEM_mem_last_dot(value);} }
	else{ mSubItems[id]->setDEBUG_MEM_mem_last_dot( value); }
}

void SpuGp::setDEBUG_MEM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setDEBUG_MEM(value);} }
	else{ mSubItems[id]->setDEBUG_MEM( value); }
}

void SpuGp::outDEBUG_MEM_trig_tpu_coarse( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_tpu_coarse(value);}}
	else{ mSubItems[id]->outDEBUG_MEM_trig_tpu_coarse( value); }
}
void SpuGp::pulseDEBUG_MEM_trig_tpu_coarse( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_tpu_coarse(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_MEM_trig_tpu_coarse(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_MEM_trig_tpu_coarse( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_MEM_mem_bram_dly( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_mem_bram_dly(value);}}
	else{ mSubItems[id]->outDEBUG_MEM_mem_bram_dly( value); }
}
void SpuGp::pulseDEBUG_MEM_mem_bram_dly( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_mem_bram_dly(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_MEM_mem_bram_dly(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_MEM_mem_bram_dly( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_MEM_trig_offset_dot_pre( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_offset_dot_pre(value);}}
	else{ mSubItems[id]->outDEBUG_MEM_trig_offset_dot_pre( value); }
}
void SpuGp::pulseDEBUG_MEM_trig_offset_dot_pre( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_offset_dot_pre(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_MEM_trig_offset_dot_pre(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_MEM_trig_offset_dot_pre( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_MEM_trig_dly_dot_out( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_dly_dot_out(value);}}
	else{ mSubItems[id]->outDEBUG_MEM_trig_dly_dot_out( value); }
}
void SpuGp::pulseDEBUG_MEM_trig_dly_dot_out( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_dly_dot_out(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_MEM_trig_dly_dot_out(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_MEM_trig_dly_dot_out( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_MEM_trig_coarse_fine( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_coarse_fine(value);}}
	else{ mSubItems[id]->outDEBUG_MEM_trig_coarse_fine( value); }
}
void SpuGp::pulseDEBUG_MEM_trig_coarse_fine( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_trig_coarse_fine(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_MEM_trig_coarse_fine(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_MEM_trig_coarse_fine( activeV, idleV, timeus ); }
}
void SpuGp::outDEBUG_MEM_mem_last_dot( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_mem_last_dot(value);}}
	else{ mSubItems[id]->outDEBUG_MEM_mem_last_dot( value); }
}
void SpuGp::pulseDEBUG_MEM_mem_last_dot( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM_mem_last_dot(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignDEBUG_MEM_mem_last_dot(idleV);}
	}
	else
	{ mSubItems[id]->pulseDEBUG_MEM_mem_last_dot( activeV, idleV, timeus ); }
}

void SpuGp::outDEBUG_MEM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM(value);} }
	else{ mSubItems[id]->outDEBUG_MEM( value); }
}

void SpuGp::outDEBUG_MEM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outDEBUG_MEM();} }
	else{ mSubItems[id]->outDEBUG_MEM(); }
}


Spu_reg SpuGp::getDEBUG_MEM_trig_tpu_coarse( int id )
{
	return mSubItems[id]->getDEBUG_MEM_trig_tpu_coarse();
}
Spu_reg SpuGp::getDEBUG_MEM_mem_bram_dly( int id )
{
	return mSubItems[id]->getDEBUG_MEM_mem_bram_dly();
}
Spu_reg SpuGp::getDEBUG_MEM_trig_offset_dot_pre( int id )
{
	return mSubItems[id]->getDEBUG_MEM_trig_offset_dot_pre();
}
Spu_reg SpuGp::getDEBUG_MEM_trig_dly_dot_out( int id )
{
	return mSubItems[id]->getDEBUG_MEM_trig_dly_dot_out();
}
Spu_reg SpuGp::getDEBUG_MEM_trig_coarse_fine( int id )
{
	return mSubItems[id]->getDEBUG_MEM_trig_coarse_fine();
}
Spu_reg SpuGp::getDEBUG_MEM_mem_last_dot( int id )
{
	return mSubItems[id]->getDEBUG_MEM_mem_last_dot();
}

Spu_reg SpuGp::getDEBUG_MEM( int id )
{
	return mSubItems[id]->getDEBUG_MEM();
}

Spu_reg SpuGp::inDEBUG_MEM( int id )
{
	return mSubItems[id]->inDEBUG_MEM();
}


Spu_reg SpuGp::getDEBUG_SCAN_pos_sa_view_frm_cnt( int id )
{
	return mSubItems[id]->getDEBUG_SCAN_pos_sa_view_frm_cnt();
}

Spu_reg SpuGp::getDEBUG_SCAN( int id )
{
	return mSubItems[id]->getDEBUG_SCAN();
}

Spu_reg SpuGp::inDEBUG_SCAN( int id )
{
	return mSubItems[id]->inDEBUG_SCAN();
}


Spu_reg SpuGp::getRECORD_SUM_wav_view_len( int id )
{
	return mSubItems[id]->getRECORD_SUM_wav_view_len();
}
Spu_reg SpuGp::getRECORD_SUM_wav_view_full( int id )
{
	return mSubItems[id]->getRECORD_SUM_wav_view_full();
}

Spu_reg SpuGp::getRECORD_SUM( int id )
{
	return mSubItems[id]->getRECORD_SUM();
}

Spu_reg SpuGp::inRECORD_SUM( int id )
{
	return mSubItems[id]->inRECORD_SUM();
}


void SpuGp::setSCAN_ROLL_FRM_scan_roll_frm_num(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setSCAN_ROLL_FRM_scan_roll_frm_num(value);} }
	else{ mSubItems[id]->setSCAN_ROLL_FRM_scan_roll_frm_num( value); }
}

void SpuGp::setSCAN_ROLL_FRM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setSCAN_ROLL_FRM(value);} }
	else{ mSubItems[id]->setSCAN_ROLL_FRM( value); }
}

void SpuGp::outSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSCAN_ROLL_FRM_scan_roll_frm_num(value);}}
	else{ mSubItems[id]->outSCAN_ROLL_FRM_scan_roll_frm_num( value); }
}
void SpuGp::pulseSCAN_ROLL_FRM_scan_roll_frm_num( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outSCAN_ROLL_FRM_scan_roll_frm_num(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignSCAN_ROLL_FRM_scan_roll_frm_num(idleV);}
	}
	else
	{ mSubItems[id]->pulseSCAN_ROLL_FRM_scan_roll_frm_num( activeV, idleV, timeus ); }
}

void SpuGp::outSCAN_ROLL_FRM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSCAN_ROLL_FRM(value);} }
	else{ mSubItems[id]->outSCAN_ROLL_FRM( value); }
}

void SpuGp::outSCAN_ROLL_FRM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSCAN_ROLL_FRM();} }
	else{ mSubItems[id]->outSCAN_ROLL_FRM(); }
}


Spu_reg SpuGp::getSCAN_ROLL_FRM_scan_roll_frm_num( int id )
{
	return mSubItems[id]->getSCAN_ROLL_FRM_scan_roll_frm_num();
}

Spu_reg SpuGp::getSCAN_ROLL_FRM( int id )
{
	return mSubItems[id]->getSCAN_ROLL_FRM();
}

Spu_reg SpuGp::inSCAN_ROLL_FRM( int id )
{
	return mSubItems[id]->inSCAN_ROLL_FRM();
}


void SpuGp::setADC_AVG_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_AVG_rate(value);} }
	else{ mSubItems[id]->setADC_AVG_rate( value); }
}
void SpuGp::setADC_AVG_done(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_AVG_done(value);} }
	else{ mSubItems[id]->setADC_AVG_done( value); }
}
void SpuGp::setADC_AVG_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_AVG_en(value);} }
	else{ mSubItems[id]->setADC_AVG_en( value); }
}

void SpuGp::setADC_AVG( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setADC_AVG(value);} }
	else{ mSubItems[id]->setADC_AVG( value); }
}

void SpuGp::outADC_AVG_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG_rate(value);}}
	else{ mSubItems[id]->outADC_AVG_rate( value); }
}
void SpuGp::pulseADC_AVG_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_AVG_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_AVG_rate( activeV, idleV, timeus ); }
}
void SpuGp::outADC_AVG_done( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG_done(value);}}
	else{ mSubItems[id]->outADC_AVG_done( value); }
}
void SpuGp::pulseADC_AVG_done( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_AVG_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_AVG_done( activeV, idleV, timeus ); }
}
void SpuGp::outADC_AVG_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG_en(value);}}
	else{ mSubItems[id]->outADC_AVG_en( value); }
}
void SpuGp::pulseADC_AVG_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignADC_AVG_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseADC_AVG_en( activeV, idleV, timeus ); }
}

void SpuGp::outADC_AVG(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG(value);} }
	else{ mSubItems[id]->outADC_AVG( value); }
}

void SpuGp::outADC_AVG( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outADC_AVG();} }
	else{ mSubItems[id]->outADC_AVG(); }
}


Spu_reg SpuGp::getADC_AVG_rate( int id )
{
	return mSubItems[id]->getADC_AVG_rate();
}
Spu_reg SpuGp::getADC_AVG_done( int id )
{
	return mSubItems[id]->getADC_AVG_done();
}
Spu_reg SpuGp::getADC_AVG_en( int id )
{
	return mSubItems[id]->getADC_AVG_en();
}

Spu_reg SpuGp::getADC_AVG( int id )
{
	return mSubItems[id]->getADC_AVG();
}

Spu_reg SpuGp::inADC_AVG( int id )
{
	return mSubItems[id]->inADC_AVG();
}


Spu_reg SpuGp::getADC_AVG_RES_AB_B( int id )
{
	return mSubItems[id]->getADC_AVG_RES_AB_B();
}
Spu_reg SpuGp::getADC_AVG_RES_AB_A( int id )
{
	return mSubItems[id]->getADC_AVG_RES_AB_A();
}

Spu_reg SpuGp::getADC_AVG_RES_AB( int id )
{
	return mSubItems[id]->getADC_AVG_RES_AB();
}

Spu_reg SpuGp::inADC_AVG_RES_AB( int id )
{
	return mSubItems[id]->inADC_AVG_RES_AB();
}


Spu_reg SpuGp::getADC_AVG_RES_CD_D( int id )
{
	return mSubItems[id]->getADC_AVG_RES_CD_D();
}
Spu_reg SpuGp::getADC_AVG_RES_CD_C( int id )
{
	return mSubItems[id]->getADC_AVG_RES_CD_C();
}

Spu_reg SpuGp::getADC_AVG_RES_CD( int id )
{
	return mSubItems[id]->getADC_AVG_RES_CD();
}

Spu_reg SpuGp::inADC_AVG_RES_CD( int id )
{
	return mSubItems[id]->inADC_AVG_RES_CD();
}


Spu_reg SpuGp::getADC_AVG_MAX_max_D( int id )
{
	return mSubItems[id]->getADC_AVG_MAX_max_D();
}
Spu_reg SpuGp::getADC_AVG_MAX_max_C( int id )
{
	return mSubItems[id]->getADC_AVG_MAX_max_C();
}
Spu_reg SpuGp::getADC_AVG_MAX_max_B( int id )
{
	return mSubItems[id]->getADC_AVG_MAX_max_B();
}
Spu_reg SpuGp::getADC_AVG_MAX_max_A( int id )
{
	return mSubItems[id]->getADC_AVG_MAX_max_A();
}

Spu_reg SpuGp::getADC_AVG_MAX( int id )
{
	return mSubItems[id]->getADC_AVG_MAX();
}

Spu_reg SpuGp::inADC_AVG_MAX( int id )
{
	return mSubItems[id]->inADC_AVG_MAX();
}


Spu_reg SpuGp::getADC_AVG_MIN_min_D( int id )
{
	return mSubItems[id]->getADC_AVG_MIN_min_D();
}
Spu_reg SpuGp::getADC_AVG_MIN_min_C( int id )
{
	return mSubItems[id]->getADC_AVG_MIN_min_C();
}
Spu_reg SpuGp::getADC_AVG_MIN_min_B( int id )
{
	return mSubItems[id]->getADC_AVG_MIN_min_B();
}
Spu_reg SpuGp::getADC_AVG_MIN_min_A( int id )
{
	return mSubItems[id]->getADC_AVG_MIN_min_A();
}

Spu_reg SpuGp::getADC_AVG_MIN( int id )
{
	return mSubItems[id]->getADC_AVG_MIN();
}

Spu_reg SpuGp::inADC_AVG_MIN( int id )
{
	return mSubItems[id]->inADC_AVG_MIN();
}


Spu_reg SpuGp::getLA_SA_RES_la( int id )
{
	return mSubItems[id]->getLA_SA_RES_la();
}

Spu_reg SpuGp::getLA_SA_RES( int id )
{
	return mSubItems[id]->getLA_SA_RES();
}

Spu_reg SpuGp::inLA_SA_RES( int id )
{
	return mSubItems[id]->inLA_SA_RES();
}


void SpuGp::setZONE_TRIG_AUX_pulse_width(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_AUX_pulse_width(value);} }
	else{ mSubItems[id]->setZONE_TRIG_AUX_pulse_width( value); }
}
void SpuGp::setZONE_TRIG_AUX_inv(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_AUX_inv(value);} }
	else{ mSubItems[id]->setZONE_TRIG_AUX_inv( value); }
}
void SpuGp::setZONE_TRIG_AUX_pass_fail(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_AUX_pass_fail(value);} }
	else{ mSubItems[id]->setZONE_TRIG_AUX_pass_fail( value); }
}

void SpuGp::setZONE_TRIG_AUX( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_AUX(value);} }
	else{ mSubItems[id]->setZONE_TRIG_AUX( value); }
}

void SpuGp::outZONE_TRIG_AUX_pulse_width( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX_pulse_width(value);}}
	else{ mSubItems[id]->outZONE_TRIG_AUX_pulse_width( value); }
}
void SpuGp::pulseZONE_TRIG_AUX_pulse_width( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX_pulse_width(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_AUX_pulse_width(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_AUX_pulse_width( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_AUX_inv( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX_inv(value);}}
	else{ mSubItems[id]->outZONE_TRIG_AUX_inv( value); }
}
void SpuGp::pulseZONE_TRIG_AUX_inv( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX_inv(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_AUX_inv(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_AUX_inv( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_AUX_pass_fail( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX_pass_fail(value);}}
	else{ mSubItems[id]->outZONE_TRIG_AUX_pass_fail( value); }
}
void SpuGp::pulseZONE_TRIG_AUX_pass_fail( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX_pass_fail(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_AUX_pass_fail(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_AUX_pass_fail( activeV, idleV, timeus ); }
}

void SpuGp::outZONE_TRIG_AUX(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX(value);} }
	else{ mSubItems[id]->outZONE_TRIG_AUX( value); }
}

void SpuGp::outZONE_TRIG_AUX( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_AUX();} }
	else{ mSubItems[id]->outZONE_TRIG_AUX(); }
}


Spu_reg SpuGp::getZONE_TRIG_AUX_pulse_width( int id )
{
	return mSubItems[id]->getZONE_TRIG_AUX_pulse_width();
}
Spu_reg SpuGp::getZONE_TRIG_AUX_inv( int id )
{
	return mSubItems[id]->getZONE_TRIG_AUX_inv();
}
Spu_reg SpuGp::getZONE_TRIG_AUX_pass_fail( int id )
{
	return mSubItems[id]->getZONE_TRIG_AUX_pass_fail();
}

Spu_reg SpuGp::getZONE_TRIG_AUX( int id )
{
	return mSubItems[id]->getZONE_TRIG_AUX();
}

Spu_reg SpuGp::inZONE_TRIG_AUX( int id )
{
	return mSubItems[id]->inZONE_TRIG_AUX();
}


void SpuGp::setZONE_TRIG_TAB1_threshold_l(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB1_threshold_l(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB1_threshold_l( value); }
}
void SpuGp::setZONE_TRIG_TAB1_threshold_h(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB1_threshold_h(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB1_threshold_h( value); }
}
void SpuGp::setZONE_TRIG_TAB1_threshold_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB1_threshold_en(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB1_threshold_en( value); }
}
void SpuGp::setZONE_TRIG_TAB1_column_addr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB1_column_addr(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB1_column_addr( value); }
}
void SpuGp::setZONE_TRIG_TAB1_tab_index(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB1_tab_index(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB1_tab_index( value); }
}

void SpuGp::setZONE_TRIG_TAB1( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB1(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB1( value); }
}

void SpuGp::outZONE_TRIG_TAB1_threshold_l( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_threshold_l(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB1_threshold_l( value); }
}
void SpuGp::pulseZONE_TRIG_TAB1_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_threshold_l(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB1_threshold_l(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB1_threshold_l( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB1_threshold_h( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_threshold_h(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB1_threshold_h( value); }
}
void SpuGp::pulseZONE_TRIG_TAB1_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_threshold_h(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB1_threshold_h(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB1_threshold_h( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB1_threshold_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_threshold_en(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB1_threshold_en( value); }
}
void SpuGp::pulseZONE_TRIG_TAB1_threshold_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_threshold_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB1_threshold_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB1_threshold_en( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB1_column_addr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_column_addr(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB1_column_addr( value); }
}
void SpuGp::pulseZONE_TRIG_TAB1_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_column_addr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB1_column_addr(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB1_column_addr( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB1_tab_index( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_tab_index(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB1_tab_index( value); }
}
void SpuGp::pulseZONE_TRIG_TAB1_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1_tab_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB1_tab_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB1_tab_index( activeV, idleV, timeus ); }
}

void SpuGp::outZONE_TRIG_TAB1(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1(value);} }
	else{ mSubItems[id]->outZONE_TRIG_TAB1( value); }
}

void SpuGp::outZONE_TRIG_TAB1( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB1();} }
	else{ mSubItems[id]->outZONE_TRIG_TAB1(); }
}


void SpuGp::setZONE_TRIG_TAB2_threshold_l(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB2_threshold_l(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB2_threshold_l( value); }
}
void SpuGp::setZONE_TRIG_TAB2_threshold_h(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB2_threshold_h(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB2_threshold_h( value); }
}
void SpuGp::setZONE_TRIG_TAB2_threshold_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB2_threshold_en(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB2_threshold_en( value); }
}
void SpuGp::setZONE_TRIG_TAB2_column_addr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB2_column_addr(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB2_column_addr( value); }
}
void SpuGp::setZONE_TRIG_TAB2_tab_index(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB2_tab_index(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB2_tab_index( value); }
}

void SpuGp::setZONE_TRIG_TAB2( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZONE_TRIG_TAB2(value);} }
	else{ mSubItems[id]->setZONE_TRIG_TAB2( value); }
}

void SpuGp::outZONE_TRIG_TAB2_threshold_l( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_threshold_l(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB2_threshold_l( value); }
}
void SpuGp::pulseZONE_TRIG_TAB2_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_threshold_l(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB2_threshold_l(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB2_threshold_l( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB2_threshold_h( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_threshold_h(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB2_threshold_h( value); }
}
void SpuGp::pulseZONE_TRIG_TAB2_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_threshold_h(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB2_threshold_h(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB2_threshold_h( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB2_threshold_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_threshold_en(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB2_threshold_en( value); }
}
void SpuGp::pulseZONE_TRIG_TAB2_threshold_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_threshold_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB2_threshold_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB2_threshold_en( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB2_column_addr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_column_addr(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB2_column_addr( value); }
}
void SpuGp::pulseZONE_TRIG_TAB2_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_column_addr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB2_column_addr(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB2_column_addr( activeV, idleV, timeus ); }
}
void SpuGp::outZONE_TRIG_TAB2_tab_index( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_tab_index(value);}}
	else{ mSubItems[id]->outZONE_TRIG_TAB2_tab_index( value); }
}
void SpuGp::pulseZONE_TRIG_TAB2_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2_tab_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZONE_TRIG_TAB2_tab_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseZONE_TRIG_TAB2_tab_index( activeV, idleV, timeus ); }
}

void SpuGp::outZONE_TRIG_TAB2(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2(value);} }
	else{ mSubItems[id]->outZONE_TRIG_TAB2( value); }
}

void SpuGp::outZONE_TRIG_TAB2( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZONE_TRIG_TAB2();} }
	else{ mSubItems[id]->outZONE_TRIG_TAB2(); }
}


void SpuGp::setMASK_TAB_threshold_l(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_threshold_l(value);} }
	else{ mSubItems[id]->setMASK_TAB_threshold_l( value); }
}
void SpuGp::setMASK_TAB_threshold_l_less_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_threshold_l_less_en(value);} }
	else{ mSubItems[id]->setMASK_TAB_threshold_l_less_en( value); }
}
void SpuGp::setMASK_TAB_threshold_h(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_threshold_h(value);} }
	else{ mSubItems[id]->setMASK_TAB_threshold_h( value); }
}
void SpuGp::setMASK_TAB_threshold_h_greater_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_threshold_h_greater_en(value);} }
	else{ mSubItems[id]->setMASK_TAB_threshold_h_greater_en( value); }
}
void SpuGp::setMASK_TAB_column_addr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_column_addr(value);} }
	else{ mSubItems[id]->setMASK_TAB_column_addr( value); }
}
void SpuGp::setMASK_TAB_tab_index(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_tab_index(value);} }
	else{ mSubItems[id]->setMASK_TAB_tab_index( value); }
}

void SpuGp::setMASK_TAB( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB(value);} }
	else{ mSubItems[id]->setMASK_TAB( value); }
}

void SpuGp::outMASK_TAB_threshold_l( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_l(value);}}
	else{ mSubItems[id]->outMASK_TAB_threshold_l( value); }
}
void SpuGp::pulseMASK_TAB_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_l(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_threshold_l(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_threshold_l( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_threshold_l_less_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_l_less_en(value);}}
	else{ mSubItems[id]->outMASK_TAB_threshold_l_less_en( value); }
}
void SpuGp::pulseMASK_TAB_threshold_l_less_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_l_less_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_threshold_l_less_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_threshold_l_less_en( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_threshold_h( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_h(value);}}
	else{ mSubItems[id]->outMASK_TAB_threshold_h( value); }
}
void SpuGp::pulseMASK_TAB_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_h(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_threshold_h(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_threshold_h( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_threshold_h_greater_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_h_greater_en(value);}}
	else{ mSubItems[id]->outMASK_TAB_threshold_h_greater_en( value); }
}
void SpuGp::pulseMASK_TAB_threshold_h_greater_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_threshold_h_greater_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_threshold_h_greater_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_threshold_h_greater_en( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_column_addr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_column_addr(value);}}
	else{ mSubItems[id]->outMASK_TAB_column_addr( value); }
}
void SpuGp::pulseMASK_TAB_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_column_addr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_column_addr(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_column_addr( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_tab_index( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_tab_index(value);}}
	else{ mSubItems[id]->outMASK_TAB_tab_index( value); }
}
void SpuGp::pulseMASK_TAB_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_tab_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_tab_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_tab_index( activeV, idleV, timeus ); }
}

void SpuGp::outMASK_TAB(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB(value);} }
	else{ mSubItems[id]->outMASK_TAB( value); }
}

void SpuGp::outMASK_TAB( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB();} }
	else{ mSubItems[id]->outMASK_TAB(); }
}


Spu_reg SpuGp::getMASK_TAB_threshold_l( int id )
{
	return mSubItems[id]->getMASK_TAB_threshold_l();
}
Spu_reg SpuGp::getMASK_TAB_threshold_l_less_en( int id )
{
	return mSubItems[id]->getMASK_TAB_threshold_l_less_en();
}
Spu_reg SpuGp::getMASK_TAB_threshold_h( int id )
{
	return mSubItems[id]->getMASK_TAB_threshold_h();
}
Spu_reg SpuGp::getMASK_TAB_threshold_h_greater_en( int id )
{
	return mSubItems[id]->getMASK_TAB_threshold_h_greater_en();
}
Spu_reg SpuGp::getMASK_TAB_column_addr( int id )
{
	return mSubItems[id]->getMASK_TAB_column_addr();
}
Spu_reg SpuGp::getMASK_TAB_tab_index( int id )
{
	return mSubItems[id]->getMASK_TAB_tab_index();
}

Spu_reg SpuGp::getMASK_TAB( int id )
{
	return mSubItems[id]->getMASK_TAB();
}

Spu_reg SpuGp::inMASK_TAB( int id )
{
	return mSubItems[id]->inMASK_TAB();
}


void SpuGp::setMASK_TAB_EX_threshold_l(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_EX_threshold_l(value);} }
	else{ mSubItems[id]->setMASK_TAB_EX_threshold_l( value); }
}
void SpuGp::setMASK_TAB_EX_threshold_l_greater_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_EX_threshold_l_greater_en(value);} }
	else{ mSubItems[id]->setMASK_TAB_EX_threshold_l_greater_en( value); }
}
void SpuGp::setMASK_TAB_EX_threshold_h(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_EX_threshold_h(value);} }
	else{ mSubItems[id]->setMASK_TAB_EX_threshold_h( value); }
}
void SpuGp::setMASK_TAB_EX_threshold_h_less_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_EX_threshold_h_less_en(value);} }
	else{ mSubItems[id]->setMASK_TAB_EX_threshold_h_less_en( value); }
}
void SpuGp::setMASK_TAB_EX_column_addr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_EX_column_addr(value);} }
	else{ mSubItems[id]->setMASK_TAB_EX_column_addr( value); }
}
void SpuGp::setMASK_TAB_EX_tab_index(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_EX_tab_index(value);} }
	else{ mSubItems[id]->setMASK_TAB_EX_tab_index( value); }
}

void SpuGp::setMASK_TAB_EX( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_TAB_EX(value);} }
	else{ mSubItems[id]->setMASK_TAB_EX( value); }
}

void SpuGp::outMASK_TAB_EX_threshold_l( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_l(value);}}
	else{ mSubItems[id]->outMASK_TAB_EX_threshold_l( value); }
}
void SpuGp::pulseMASK_TAB_EX_threshold_l( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_l(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_EX_threshold_l(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_EX_threshold_l( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_EX_threshold_l_greater_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_l_greater_en(value);}}
	else{ mSubItems[id]->outMASK_TAB_EX_threshold_l_greater_en( value); }
}
void SpuGp::pulseMASK_TAB_EX_threshold_l_greater_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_l_greater_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_EX_threshold_l_greater_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_EX_threshold_l_greater_en( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_EX_threshold_h( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_h(value);}}
	else{ mSubItems[id]->outMASK_TAB_EX_threshold_h( value); }
}
void SpuGp::pulseMASK_TAB_EX_threshold_h( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_h(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_EX_threshold_h(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_EX_threshold_h( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_EX_threshold_h_less_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_h_less_en(value);}}
	else{ mSubItems[id]->outMASK_TAB_EX_threshold_h_less_en( value); }
}
void SpuGp::pulseMASK_TAB_EX_threshold_h_less_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_threshold_h_less_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_EX_threshold_h_less_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_EX_threshold_h_less_en( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_EX_column_addr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_column_addr(value);}}
	else{ mSubItems[id]->outMASK_TAB_EX_column_addr( value); }
}
void SpuGp::pulseMASK_TAB_EX_column_addr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_column_addr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_EX_column_addr(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_EX_column_addr( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_TAB_EX_tab_index( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_tab_index(value);}}
	else{ mSubItems[id]->outMASK_TAB_EX_tab_index( value); }
}
void SpuGp::pulseMASK_TAB_EX_tab_index( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX_tab_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_TAB_EX_tab_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_TAB_EX_tab_index( activeV, idleV, timeus ); }
}

void SpuGp::outMASK_TAB_EX(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX(value);} }
	else{ mSubItems[id]->outMASK_TAB_EX( value); }
}

void SpuGp::outMASK_TAB_EX( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_TAB_EX();} }
	else{ mSubItems[id]->outMASK_TAB_EX(); }
}


Spu_reg SpuGp::getMASK_TAB_EX_threshold_l( int id )
{
	return mSubItems[id]->getMASK_TAB_EX_threshold_l();
}
Spu_reg SpuGp::getMASK_TAB_EX_threshold_l_greater_en( int id )
{
	return mSubItems[id]->getMASK_TAB_EX_threshold_l_greater_en();
}
Spu_reg SpuGp::getMASK_TAB_EX_threshold_h( int id )
{
	return mSubItems[id]->getMASK_TAB_EX_threshold_h();
}
Spu_reg SpuGp::getMASK_TAB_EX_threshold_h_less_en( int id )
{
	return mSubItems[id]->getMASK_TAB_EX_threshold_h_less_en();
}
Spu_reg SpuGp::getMASK_TAB_EX_column_addr( int id )
{
	return mSubItems[id]->getMASK_TAB_EX_column_addr();
}
Spu_reg SpuGp::getMASK_TAB_EX_tab_index( int id )
{
	return mSubItems[id]->getMASK_TAB_EX_tab_index();
}

Spu_reg SpuGp::getMASK_TAB_EX( int id )
{
	return mSubItems[id]->getMASK_TAB_EX();
}

Spu_reg SpuGp::inMASK_TAB_EX( int id )
{
	return mSubItems[id]->inMASK_TAB_EX();
}


void SpuGp::setMASK_COMPRESS_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_COMPRESS_rate(value);} }
	else{ mSubItems[id]->setMASK_COMPRESS_rate( value); }
}
void SpuGp::setMASK_COMPRESS_peak(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_COMPRESS_peak(value);} }
	else{ mSubItems[id]->setMASK_COMPRESS_peak( value); }
}
void SpuGp::setMASK_COMPRESS_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_COMPRESS_en(value);} }
	else{ mSubItems[id]->setMASK_COMPRESS_en( value); }
}

void SpuGp::setMASK_COMPRESS( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_COMPRESS(value);} }
	else{ mSubItems[id]->setMASK_COMPRESS( value); }
}

void SpuGp::outMASK_COMPRESS_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS_rate(value);}}
	else{ mSubItems[id]->outMASK_COMPRESS_rate( value); }
}
void SpuGp::pulseMASK_COMPRESS_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_COMPRESS_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_COMPRESS_rate( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_COMPRESS_peak( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS_peak(value);}}
	else{ mSubItems[id]->outMASK_COMPRESS_peak( value); }
}
void SpuGp::pulseMASK_COMPRESS_peak( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS_peak(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_COMPRESS_peak(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_COMPRESS_peak( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_COMPRESS_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS_en(value);}}
	else{ mSubItems[id]->outMASK_COMPRESS_en( value); }
}
void SpuGp::pulseMASK_COMPRESS_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_COMPRESS_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_COMPRESS_en( activeV, idleV, timeus ); }
}

void SpuGp::outMASK_COMPRESS(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS(value);} }
	else{ mSubItems[id]->outMASK_COMPRESS( value); }
}

void SpuGp::outMASK_COMPRESS( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_COMPRESS();} }
	else{ mSubItems[id]->outMASK_COMPRESS(); }
}


Spu_reg SpuGp::getMASK_COMPRESS_rate( int id )
{
	return mSubItems[id]->getMASK_COMPRESS_rate();
}
Spu_reg SpuGp::getMASK_COMPRESS_peak( int id )
{
	return mSubItems[id]->getMASK_COMPRESS_peak();
}
Spu_reg SpuGp::getMASK_COMPRESS_en( int id )
{
	return mSubItems[id]->getMASK_COMPRESS_en();
}

Spu_reg SpuGp::getMASK_COMPRESS( int id )
{
	return mSubItems[id]->getMASK_COMPRESS();
}

Spu_reg SpuGp::inMASK_COMPRESS( int id )
{
	return mSubItems[id]->inMASK_COMPRESS();
}


void SpuGp::setMASK_CTRL_cross_index(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_cross_index(value);} }
	else{ mSubItems[id]->setMASK_CTRL_cross_index( value); }
}
void SpuGp::setMASK_CTRL_mask_index_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_mask_index_en(value);} }
	else{ mSubItems[id]->setMASK_CTRL_mask_index_en( value); }
}
void SpuGp::setMASK_CTRL_zone1_trig_tab_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_zone1_trig_tab_en(value);} }
	else{ mSubItems[id]->setMASK_CTRL_zone1_trig_tab_en( value); }
}
void SpuGp::setMASK_CTRL_zone2_trig_tab_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_zone2_trig_tab_en(value);} }
	else{ mSubItems[id]->setMASK_CTRL_zone2_trig_tab_en( value); }
}
void SpuGp::setMASK_CTRL_zone1_trig_cross(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_zone1_trig_cross(value);} }
	else{ mSubItems[id]->setMASK_CTRL_zone1_trig_cross( value); }
}
void SpuGp::setMASK_CTRL_zone2_trig_cross(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_zone2_trig_cross(value);} }
	else{ mSubItems[id]->setMASK_CTRL_zone2_trig_cross( value); }
}
void SpuGp::setMASK_CTRL_mask_result_rd(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_mask_result_rd(value);} }
	else{ mSubItems[id]->setMASK_CTRL_mask_result_rd( value); }
}
void SpuGp::setMASK_CTRL_sum_clr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_sum_clr(value);} }
	else{ mSubItems[id]->setMASK_CTRL_sum_clr( value); }
}
void SpuGp::setMASK_CTRL_cross_clr(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_cross_clr(value);} }
	else{ mSubItems[id]->setMASK_CTRL_cross_clr( value); }
}
void SpuGp::setMASK_CTRL_mask_we(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL_mask_we(value);} }
	else{ mSubItems[id]->setMASK_CTRL_mask_we( value); }
}

void SpuGp::setMASK_CTRL( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_CTRL(value);} }
	else{ mSubItems[id]->setMASK_CTRL( value); }
}

void SpuGp::outMASK_CTRL_cross_index( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_cross_index(value);}}
	else{ mSubItems[id]->outMASK_CTRL_cross_index( value); }
}
void SpuGp::pulseMASK_CTRL_cross_index( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_cross_index(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_cross_index(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_cross_index( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_mask_index_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_mask_index_en(value);}}
	else{ mSubItems[id]->outMASK_CTRL_mask_index_en( value); }
}
void SpuGp::pulseMASK_CTRL_mask_index_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_mask_index_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_mask_index_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_mask_index_en( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_zone1_trig_tab_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone1_trig_tab_en(value);}}
	else{ mSubItems[id]->outMASK_CTRL_zone1_trig_tab_en( value); }
}
void SpuGp::pulseMASK_CTRL_zone1_trig_tab_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone1_trig_tab_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_zone1_trig_tab_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_zone1_trig_tab_en( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_zone2_trig_tab_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone2_trig_tab_en(value);}}
	else{ mSubItems[id]->outMASK_CTRL_zone2_trig_tab_en( value); }
}
void SpuGp::pulseMASK_CTRL_zone2_trig_tab_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone2_trig_tab_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_zone2_trig_tab_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_zone2_trig_tab_en( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_zone1_trig_cross( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone1_trig_cross(value);}}
	else{ mSubItems[id]->outMASK_CTRL_zone1_trig_cross( value); }
}
void SpuGp::pulseMASK_CTRL_zone1_trig_cross( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone1_trig_cross(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_zone1_trig_cross(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_zone1_trig_cross( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_zone2_trig_cross( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone2_trig_cross(value);}}
	else{ mSubItems[id]->outMASK_CTRL_zone2_trig_cross( value); }
}
void SpuGp::pulseMASK_CTRL_zone2_trig_cross( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_zone2_trig_cross(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_zone2_trig_cross(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_zone2_trig_cross( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_mask_result_rd( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_mask_result_rd(value);}}
	else{ mSubItems[id]->outMASK_CTRL_mask_result_rd( value); }
}
void SpuGp::pulseMASK_CTRL_mask_result_rd( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_mask_result_rd(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_mask_result_rd(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_mask_result_rd( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_sum_clr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_sum_clr(value);}}
	else{ mSubItems[id]->outMASK_CTRL_sum_clr( value); }
}
void SpuGp::pulseMASK_CTRL_sum_clr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_sum_clr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_sum_clr(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_sum_clr( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_cross_clr( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_cross_clr(value);}}
	else{ mSubItems[id]->outMASK_CTRL_cross_clr( value); }
}
void SpuGp::pulseMASK_CTRL_cross_clr( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_cross_clr(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_cross_clr(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_cross_clr( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_CTRL_mask_we( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_mask_we(value);}}
	else{ mSubItems[id]->outMASK_CTRL_mask_we( value); }
}
void SpuGp::pulseMASK_CTRL_mask_we( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL_mask_we(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_CTRL_mask_we(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_CTRL_mask_we( activeV, idleV, timeus ); }
}

void SpuGp::outMASK_CTRL(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL(value);} }
	else{ mSubItems[id]->outMASK_CTRL( value); }
}

void SpuGp::outMASK_CTRL( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_CTRL();} }
	else{ mSubItems[id]->outMASK_CTRL(); }
}


Spu_reg SpuGp::getMASK_CTRL_cross_index( int id )
{
	return mSubItems[id]->getMASK_CTRL_cross_index();
}
Spu_reg SpuGp::getMASK_CTRL_mask_index_en( int id )
{
	return mSubItems[id]->getMASK_CTRL_mask_index_en();
}
Spu_reg SpuGp::getMASK_CTRL_zone1_trig_tab_en( int id )
{
	return mSubItems[id]->getMASK_CTRL_zone1_trig_tab_en();
}
Spu_reg SpuGp::getMASK_CTRL_zone2_trig_tab_en( int id )
{
	return mSubItems[id]->getMASK_CTRL_zone2_trig_tab_en();
}
Spu_reg SpuGp::getMASK_CTRL_zone1_trig_cross( int id )
{
	return mSubItems[id]->getMASK_CTRL_zone1_trig_cross();
}
Spu_reg SpuGp::getMASK_CTRL_zone2_trig_cross( int id )
{
	return mSubItems[id]->getMASK_CTRL_zone2_trig_cross();
}
Spu_reg SpuGp::getMASK_CTRL_mask_result_rd( int id )
{
	return mSubItems[id]->getMASK_CTRL_mask_result_rd();
}
Spu_reg SpuGp::getMASK_CTRL_sum_clr( int id )
{
	return mSubItems[id]->getMASK_CTRL_sum_clr();
}
Spu_reg SpuGp::getMASK_CTRL_cross_clr( int id )
{
	return mSubItems[id]->getMASK_CTRL_cross_clr();
}
Spu_reg SpuGp::getMASK_CTRL_mask_we( int id )
{
	return mSubItems[id]->getMASK_CTRL_mask_we();
}

Spu_reg SpuGp::getMASK_CTRL( int id )
{
	return mSubItems[id]->getMASK_CTRL();
}

Spu_reg SpuGp::inMASK_CTRL( int id )
{
	return mSubItems[id]->inMASK_CTRL();
}


Spu_reg SpuGp::getMASK_FRM_CNT_L( int id )
{
	return mSubItems[id]->getMASK_FRM_CNT_L();
}

Spu_reg SpuGp::inMASK_FRM_CNT_L( int id )
{
	return mSubItems[id]->inMASK_FRM_CNT_L();
}


Spu_reg SpuGp::getMASK_FRM_CNT_H( int id )
{
	return mSubItems[id]->getMASK_FRM_CNT_H();
}

Spu_reg SpuGp::inMASK_FRM_CNT_H( int id )
{
	return mSubItems[id]->inMASK_FRM_CNT_H();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_L_CHA( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_L_CHA();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_L_CHA( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_L_CHA();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_H_CHA( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_H_CHA();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_H_CHA( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_H_CHA();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_L_CHB( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_L_CHB();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_L_CHB( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_L_CHB();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_H_CHB( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_H_CHB();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_H_CHB( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_H_CHB();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_L_CHC( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_L_CHC();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_L_CHC( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_L_CHC();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_H_CHC( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_H_CHC();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_H_CHC( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_H_CHC();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_L_CHD( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_L_CHD();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_L_CHD( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_L_CHD();
}


Spu_reg SpuGp::getMASK_CROSS_CNT_H_CHD( int id )
{
	return mSubItems[id]->getMASK_CROSS_CNT_H_CHD();
}

Spu_reg SpuGp::inMASK_CROSS_CNT_H_CHD( int id )
{
	return mSubItems[id]->inMASK_CROSS_CNT_H_CHD();
}


void SpuGp::setMASK_OUTPUT_pulse_width(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_OUTPUT_pulse_width(value);} }
	else{ mSubItems[id]->setMASK_OUTPUT_pulse_width( value); }
}
void SpuGp::setMASK_OUTPUT_inv(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_OUTPUT_inv(value);} }
	else{ mSubItems[id]->setMASK_OUTPUT_inv( value); }
}
void SpuGp::setMASK_OUTPUT_pass_fail(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_OUTPUT_pass_fail(value);} }
	else{ mSubItems[id]->setMASK_OUTPUT_pass_fail( value); }
}

void SpuGp::setMASK_OUTPUT( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setMASK_OUTPUT(value);} }
	else{ mSubItems[id]->setMASK_OUTPUT( value); }
}

void SpuGp::outMASK_OUTPUT_pulse_width( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT_pulse_width(value);}}
	else{ mSubItems[id]->outMASK_OUTPUT_pulse_width( value); }
}
void SpuGp::pulseMASK_OUTPUT_pulse_width( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT_pulse_width(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_OUTPUT_pulse_width(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_OUTPUT_pulse_width( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_OUTPUT_inv( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT_inv(value);}}
	else{ mSubItems[id]->outMASK_OUTPUT_inv( value); }
}
void SpuGp::pulseMASK_OUTPUT_inv( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT_inv(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_OUTPUT_inv(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_OUTPUT_inv( activeV, idleV, timeus ); }
}
void SpuGp::outMASK_OUTPUT_pass_fail( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT_pass_fail(value);}}
	else{ mSubItems[id]->outMASK_OUTPUT_pass_fail( value); }
}
void SpuGp::pulseMASK_OUTPUT_pass_fail( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT_pass_fail(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignMASK_OUTPUT_pass_fail(idleV);}
	}
	else
	{ mSubItems[id]->pulseMASK_OUTPUT_pass_fail( activeV, idleV, timeus ); }
}

void SpuGp::outMASK_OUTPUT(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT(value);} }
	else{ mSubItems[id]->outMASK_OUTPUT( value); }
}

void SpuGp::outMASK_OUTPUT( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outMASK_OUTPUT();} }
	else{ mSubItems[id]->outMASK_OUTPUT(); }
}


Spu_reg SpuGp::getMASK_OUTPUT_pulse_width( int id )
{
	return mSubItems[id]->getMASK_OUTPUT_pulse_width();
}
Spu_reg SpuGp::getMASK_OUTPUT_inv( int id )
{
	return mSubItems[id]->getMASK_OUTPUT_inv();
}
Spu_reg SpuGp::getMASK_OUTPUT_pass_fail( int id )
{
	return mSubItems[id]->getMASK_OUTPUT_pass_fail();
}

Spu_reg SpuGp::getMASK_OUTPUT( int id )
{
	return mSubItems[id]->getMASK_OUTPUT();
}

Spu_reg SpuGp::inMASK_OUTPUT( int id )
{
	return mSubItems[id]->inMASK_OUTPUT();
}


void SpuGp::setSEARCH_OFFSET_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setSEARCH_OFFSET_CH(value);} }
	else{ mSubItems[id]->setSEARCH_OFFSET_CH( value); }
}

void SpuGp::outSEARCH_OFFSET_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_OFFSET_CH(value);} }
	else{ mSubItems[id]->outSEARCH_OFFSET_CH( value); }
}

void SpuGp::outSEARCH_OFFSET_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_OFFSET_CH();} }
	else{ mSubItems[id]->outSEARCH_OFFSET_CH(); }
}


Spu_reg SpuGp::getSEARCH_OFFSET_CH( int id )
{
	return mSubItems[id]->getSEARCH_OFFSET_CH();
}

Spu_reg SpuGp::inSEARCH_OFFSET_CH( int id )
{
	return mSubItems[id]->inSEARCH_OFFSET_CH();
}


void SpuGp::setSEARCH_LEN_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setSEARCH_LEN_CH(value);} }
	else{ mSubItems[id]->setSEARCH_LEN_CH( value); }
}

void SpuGp::outSEARCH_LEN_CH( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_LEN_CH(value);} }
	else{ mSubItems[id]->outSEARCH_LEN_CH( value); }
}

void SpuGp::outSEARCH_LEN_CH( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_LEN_CH();} }
	else{ mSubItems[id]->outSEARCH_LEN_CH(); }
}


Spu_reg SpuGp::getSEARCH_LEN_CH( int id )
{
	return mSubItems[id]->getSEARCH_LEN_CH();
}

Spu_reg SpuGp::inSEARCH_LEN_CH( int id )
{
	return mSubItems[id]->inSEARCH_LEN_CH();
}


void SpuGp::setSEARCH_OFFSET_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setSEARCH_OFFSET_LA(value);} }
	else{ mSubItems[id]->setSEARCH_OFFSET_LA( value); }
}

void SpuGp::outSEARCH_OFFSET_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_OFFSET_LA(value);} }
	else{ mSubItems[id]->outSEARCH_OFFSET_LA( value); }
}

void SpuGp::outSEARCH_OFFSET_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_OFFSET_LA();} }
	else{ mSubItems[id]->outSEARCH_OFFSET_LA(); }
}


Spu_reg SpuGp::getSEARCH_OFFSET_LA( int id )
{
	return mSubItems[id]->getSEARCH_OFFSET_LA();
}

Spu_reg SpuGp::inSEARCH_OFFSET_LA( int id )
{
	return mSubItems[id]->inSEARCH_OFFSET_LA();
}


void SpuGp::setSEARCH_LEN_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setSEARCH_LEN_LA(value);} }
	else{ mSubItems[id]->setSEARCH_LEN_LA( value); }
}

void SpuGp::outSEARCH_LEN_LA( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_LEN_LA(value);} }
	else{ mSubItems[id]->outSEARCH_LEN_LA( value); }
}

void SpuGp::outSEARCH_LEN_LA( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outSEARCH_LEN_LA();} }
	else{ mSubItems[id]->outSEARCH_LEN_LA(); }
}


Spu_reg SpuGp::getSEARCH_LEN_LA( int id )
{
	return mSubItems[id]->getSEARCH_LEN_LA();
}

Spu_reg SpuGp::inSEARCH_LEN_LA( int id )
{
	return mSubItems[id]->inSEARCH_LEN_LA();
}


void SpuGp::setTIME_TAG_L( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTIME_TAG_L(value);} }
	else{ mSubItems[id]->setTIME_TAG_L( value); }
}

void SpuGp::outTIME_TAG_L( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_L(value);} }
	else{ mSubItems[id]->outTIME_TAG_L( value); }
}

void SpuGp::outTIME_TAG_L( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_L();} }
	else{ mSubItems[id]->outTIME_TAG_L(); }
}


Spu_reg SpuGp::getTIME_TAG_L( int id )
{
	return mSubItems[id]->getTIME_TAG_L();
}

Spu_reg SpuGp::inTIME_TAG_L( int id )
{
	return mSubItems[id]->inTIME_TAG_L();
}


void SpuGp::setTIME_TAG_H( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTIME_TAG_H(value);} }
	else{ mSubItems[id]->setTIME_TAG_H( value); }
}

void SpuGp::outTIME_TAG_H( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_H(value);} }
	else{ mSubItems[id]->outTIME_TAG_H( value); }
}

void SpuGp::outTIME_TAG_H( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_H();} }
	else{ mSubItems[id]->outTIME_TAG_H(); }
}


Spu_reg SpuGp::getTIME_TAG_H( int id )
{
	return mSubItems[id]->getTIME_TAG_H();
}

Spu_reg SpuGp::inTIME_TAG_H( int id )
{
	return mSubItems[id]->inTIME_TAG_H();
}


void SpuGp::setFINE_TRIG_POSITION( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setFINE_TRIG_POSITION(value);} }
	else{ mSubItems[id]->setFINE_TRIG_POSITION( value); }
}

void SpuGp::outFINE_TRIG_POSITION( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_TRIG_POSITION(value);} }
	else{ mSubItems[id]->outFINE_TRIG_POSITION( value); }
}

void SpuGp::outFINE_TRIG_POSITION( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outFINE_TRIG_POSITION();} }
	else{ mSubItems[id]->outFINE_TRIG_POSITION(); }
}


Spu_reg SpuGp::getFINE_TRIG_POSITION( int id )
{
	return mSubItems[id]->getFINE_TRIG_POSITION();
}

Spu_reg SpuGp::inFINE_TRIG_POSITION( int id )
{
	return mSubItems[id]->inFINE_TRIG_POSITION();
}


void SpuGp::setTIME_TAG_CTRL_rd_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTIME_TAG_CTRL_rd_en(value);} }
	else{ mSubItems[id]->setTIME_TAG_CTRL_rd_en( value); }
}
void SpuGp::setTIME_TAG_CTRL_rec_first_done(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTIME_TAG_CTRL_rec_first_done(value);} }
	else{ mSubItems[id]->setTIME_TAG_CTRL_rec_first_done( value); }
}

void SpuGp::setTIME_TAG_CTRL( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTIME_TAG_CTRL(value);} }
	else{ mSubItems[id]->setTIME_TAG_CTRL( value); }
}

void SpuGp::outTIME_TAG_CTRL_rd_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_CTRL_rd_en(value);}}
	else{ mSubItems[id]->outTIME_TAG_CTRL_rd_en( value); }
}
void SpuGp::pulseTIME_TAG_CTRL_rd_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_CTRL_rd_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTIME_TAG_CTRL_rd_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseTIME_TAG_CTRL_rd_en( activeV, idleV, timeus ); }
}
void SpuGp::outTIME_TAG_CTRL_rec_first_done( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_CTRL_rec_first_done(value);}}
	else{ mSubItems[id]->outTIME_TAG_CTRL_rec_first_done( value); }
}
void SpuGp::pulseTIME_TAG_CTRL_rec_first_done( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_CTRL_rec_first_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTIME_TAG_CTRL_rec_first_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseTIME_TAG_CTRL_rec_first_done( activeV, idleV, timeus ); }
}

void SpuGp::outTIME_TAG_CTRL(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_CTRL(value);} }
	else{ mSubItems[id]->outTIME_TAG_CTRL( value); }
}

void SpuGp::outTIME_TAG_CTRL( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTIME_TAG_CTRL();} }
	else{ mSubItems[id]->outTIME_TAG_CTRL(); }
}


Spu_reg SpuGp::getTIME_TAG_CTRL_rd_en( int id )
{
	return mSubItems[id]->getTIME_TAG_CTRL_rd_en();
}
Spu_reg SpuGp::getTIME_TAG_CTRL_rec_first_done( int id )
{
	return mSubItems[id]->getTIME_TAG_CTRL_rec_first_done();
}

Spu_reg SpuGp::getTIME_TAG_CTRL( int id )
{
	return mSubItems[id]->getTIME_TAG_CTRL();
}

Spu_reg SpuGp::inTIME_TAG_CTRL( int id )
{
	return mSubItems[id]->inTIME_TAG_CTRL();
}


void SpuGp::setIMPORT_INFO_import_done(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setIMPORT_INFO_import_done(value);} }
	else{ mSubItems[id]->setIMPORT_INFO_import_done( value); }
}

void SpuGp::setIMPORT_INFO( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setIMPORT_INFO(value);} }
	else{ mSubItems[id]->setIMPORT_INFO( value); }
}

void SpuGp::outIMPORT_INFO_import_done( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outIMPORT_INFO_import_done(value);}}
	else{ mSubItems[id]->outIMPORT_INFO_import_done( value); }
}
void SpuGp::pulseIMPORT_INFO_import_done( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outIMPORT_INFO_import_done(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignIMPORT_INFO_import_done(idleV);}
	}
	else
	{ mSubItems[id]->pulseIMPORT_INFO_import_done( activeV, idleV, timeus ); }
}

void SpuGp::outIMPORT_INFO(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outIMPORT_INFO(value);} }
	else{ mSubItems[id]->outIMPORT_INFO( value); }
}

void SpuGp::outIMPORT_INFO( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outIMPORT_INFO();} }
	else{ mSubItems[id]->outIMPORT_INFO(); }
}


Spu_reg SpuGp::getIMPORT_INFO_import_done( int id )
{
	return mSubItems[id]->getIMPORT_INFO_import_done();
}

Spu_reg SpuGp::getIMPORT_INFO( int id )
{
	return mSubItems[id]->getIMPORT_INFO();
}

Spu_reg SpuGp::inIMPORT_INFO( int id )
{
	return mSubItems[id]->inIMPORT_INFO();
}


Spu_reg SpuGp::getTIME_TAG_L_REC_FIRST( int id )
{
	return mSubItems[id]->getTIME_TAG_L_REC_FIRST();
}

Spu_reg SpuGp::inTIME_TAG_L_REC_FIRST( int id )
{
	return mSubItems[id]->inTIME_TAG_L_REC_FIRST();
}


Spu_reg SpuGp::getTIME_TAG_H_REC_FIRST( int id )
{
	return mSubItems[id]->getTIME_TAG_H_REC_FIRST();
}

Spu_reg SpuGp::inTIME_TAG_H_REC_FIRST( int id )
{
	return mSubItems[id]->inTIME_TAG_H_REC_FIRST();
}


void SpuGp::setTRACE_AVG_RATE_X( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_AVG_RATE_X(value);} }
	else{ mSubItems[id]->setTRACE_AVG_RATE_X( value); }
}

void SpuGp::outTRACE_AVG_RATE_X( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_AVG_RATE_X(value);} }
	else{ mSubItems[id]->outTRACE_AVG_RATE_X( value); }
}

void SpuGp::outTRACE_AVG_RATE_X( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_AVG_RATE_X();} }
	else{ mSubItems[id]->outTRACE_AVG_RATE_X(); }
}


Spu_reg SpuGp::getTRACE_AVG_RATE_X( int id )
{
	return mSubItems[id]->getTRACE_AVG_RATE_X();
}

Spu_reg SpuGp::inTRACE_AVG_RATE_X( int id )
{
	return mSubItems[id]->inTRACE_AVG_RATE_X();
}


void SpuGp::setTRACE_AVG_RATE_Y( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_AVG_RATE_Y(value);} }
	else{ mSubItems[id]->setTRACE_AVG_RATE_Y( value); }
}

void SpuGp::outTRACE_AVG_RATE_Y( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_AVG_RATE_Y(value);} }
	else{ mSubItems[id]->outTRACE_AVG_RATE_Y( value); }
}

void SpuGp::outTRACE_AVG_RATE_Y( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_AVG_RATE_Y();} }
	else{ mSubItems[id]->outTRACE_AVG_RATE_Y(); }
}


Spu_reg SpuGp::getTRACE_AVG_RATE_Y( int id )
{
	return mSubItems[id]->getTRACE_AVG_RATE_Y();
}

Spu_reg SpuGp::inTRACE_AVG_RATE_Y( int id )
{
	return mSubItems[id]->inTRACE_AVG_RATE_Y();
}


void SpuGp::setLINEAR_INTX_CTRL_mian_bypass(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLINEAR_INTX_CTRL_mian_bypass(value);} }
	else{ mSubItems[id]->setLINEAR_INTX_CTRL_mian_bypass( value); }
}
void SpuGp::setLINEAR_INTX_CTRL_zoom_bypass(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLINEAR_INTX_CTRL_zoom_bypass(value);} }
	else{ mSubItems[id]->setLINEAR_INTX_CTRL_zoom_bypass( value); }
}

void SpuGp::setLINEAR_INTX_CTRL( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLINEAR_INTX_CTRL(value);} }
	else{ mSubItems[id]->setLINEAR_INTX_CTRL( value); }
}

void SpuGp::outLINEAR_INTX_CTRL_mian_bypass( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_CTRL_mian_bypass(value);}}
	else{ mSubItems[id]->outLINEAR_INTX_CTRL_mian_bypass( value); }
}
void SpuGp::pulseLINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_CTRL_mian_bypass(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLINEAR_INTX_CTRL_mian_bypass(idleV);}
	}
	else
	{ mSubItems[id]->pulseLINEAR_INTX_CTRL_mian_bypass( activeV, idleV, timeus ); }
}
void SpuGp::outLINEAR_INTX_CTRL_zoom_bypass( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_CTRL_zoom_bypass(value);}}
	else{ mSubItems[id]->outLINEAR_INTX_CTRL_zoom_bypass( value); }
}
void SpuGp::pulseLINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_CTRL_zoom_bypass(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLINEAR_INTX_CTRL_zoom_bypass(idleV);}
	}
	else
	{ mSubItems[id]->pulseLINEAR_INTX_CTRL_zoom_bypass( activeV, idleV, timeus ); }
}

void SpuGp::outLINEAR_INTX_CTRL(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_CTRL(value);} }
	else{ mSubItems[id]->outLINEAR_INTX_CTRL( value); }
}

void SpuGp::outLINEAR_INTX_CTRL( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_CTRL();} }
	else{ mSubItems[id]->outLINEAR_INTX_CTRL(); }
}


Spu_reg SpuGp::getLINEAR_INTX_CTRL_mian_bypass( int id )
{
	return mSubItems[id]->getLINEAR_INTX_CTRL_mian_bypass();
}
Spu_reg SpuGp::getLINEAR_INTX_CTRL_zoom_bypass( int id )
{
	return mSubItems[id]->getLINEAR_INTX_CTRL_zoom_bypass();
}

Spu_reg SpuGp::getLINEAR_INTX_CTRL( int id )
{
	return mSubItems[id]->getLINEAR_INTX_CTRL();
}

Spu_reg SpuGp::inLINEAR_INTX_CTRL( int id )
{
	return mSubItems[id]->inLINEAR_INTX_CTRL();
}


void SpuGp::setLINEAR_INTX_INIT( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLINEAR_INTX_INIT(value);} }
	else{ mSubItems[id]->setLINEAR_INTX_INIT( value); }
}

void SpuGp::outLINEAR_INTX_INIT( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_INIT(value);} }
	else{ mSubItems[id]->outLINEAR_INTX_INIT( value); }
}

void SpuGp::outLINEAR_INTX_INIT( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_INIT();} }
	else{ mSubItems[id]->outLINEAR_INTX_INIT(); }
}


Spu_reg SpuGp::getLINEAR_INTX_INIT( int id )
{
	return mSubItems[id]->getLINEAR_INTX_INIT();
}

Spu_reg SpuGp::inLINEAR_INTX_INIT( int id )
{
	return mSubItems[id]->inLINEAR_INTX_INIT();
}


void SpuGp::setLINEAR_INTX_STEP( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLINEAR_INTX_STEP(value);} }
	else{ mSubItems[id]->setLINEAR_INTX_STEP( value); }
}

void SpuGp::outLINEAR_INTX_STEP( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_STEP(value);} }
	else{ mSubItems[id]->outLINEAR_INTX_STEP( value); }
}

void SpuGp::outLINEAR_INTX_STEP( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_STEP();} }
	else{ mSubItems[id]->outLINEAR_INTX_STEP(); }
}


Spu_reg SpuGp::getLINEAR_INTX_STEP( int id )
{
	return mSubItems[id]->getLINEAR_INTX_STEP();
}

Spu_reg SpuGp::inLINEAR_INTX_STEP( int id )
{
	return mSubItems[id]->inLINEAR_INTX_STEP();
}


void SpuGp::setLINEAR_INTX_INIT_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLINEAR_INTX_INIT_ZOOM(value);} }
	else{ mSubItems[id]->setLINEAR_INTX_INIT_ZOOM( value); }
}

void SpuGp::outLINEAR_INTX_INIT_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_INIT_ZOOM(value);} }
	else{ mSubItems[id]->outLINEAR_INTX_INIT_ZOOM( value); }
}

void SpuGp::outLINEAR_INTX_INIT_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_INIT_ZOOM();} }
	else{ mSubItems[id]->outLINEAR_INTX_INIT_ZOOM(); }
}


Spu_reg SpuGp::getLINEAR_INTX_INIT_ZOOM( int id )
{
	return mSubItems[id]->getLINEAR_INTX_INIT_ZOOM();
}

Spu_reg SpuGp::inLINEAR_INTX_INIT_ZOOM( int id )
{
	return mSubItems[id]->inLINEAR_INTX_INIT_ZOOM();
}


void SpuGp::setLINEAR_INTX_STEP_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLINEAR_INTX_STEP_ZOOM(value);} }
	else{ mSubItems[id]->setLINEAR_INTX_STEP_ZOOM( value); }
}

void SpuGp::outLINEAR_INTX_STEP_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_STEP_ZOOM(value);} }
	else{ mSubItems[id]->outLINEAR_INTX_STEP_ZOOM( value); }
}

void SpuGp::outLINEAR_INTX_STEP_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLINEAR_INTX_STEP_ZOOM();} }
	else{ mSubItems[id]->outLINEAR_INTX_STEP_ZOOM(); }
}


Spu_reg SpuGp::getLINEAR_INTX_STEP_ZOOM( int id )
{
	return mSubItems[id]->getLINEAR_INTX_STEP_ZOOM();
}

Spu_reg SpuGp::inLINEAR_INTX_STEP_ZOOM( int id )
{
	return mSubItems[id]->inLINEAR_INTX_STEP_ZOOM();
}


void SpuGp::setWAVE_OFFSET_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setWAVE_OFFSET_EYE(value);} }
	else{ mSubItems[id]->setWAVE_OFFSET_EYE( value); }
}

void SpuGp::outWAVE_OFFSET_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_OFFSET_EYE(value);} }
	else{ mSubItems[id]->outWAVE_OFFSET_EYE( value); }
}

void SpuGp::outWAVE_OFFSET_EYE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_OFFSET_EYE();} }
	else{ mSubItems[id]->outWAVE_OFFSET_EYE(); }
}


Spu_reg SpuGp::getWAVE_OFFSET_EYE( int id )
{
	return mSubItems[id]->getWAVE_OFFSET_EYE();
}

Spu_reg SpuGp::inWAVE_OFFSET_EYE( int id )
{
	return mSubItems[id]->inWAVE_OFFSET_EYE();
}


void SpuGp::setWAVE_LEN_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setWAVE_LEN_EYE(value);} }
	else{ mSubItems[id]->setWAVE_LEN_EYE( value); }
}

void SpuGp::outWAVE_LEN_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_LEN_EYE(value);} }
	else{ mSubItems[id]->outWAVE_LEN_EYE( value); }
}

void SpuGp::outWAVE_LEN_EYE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outWAVE_LEN_EYE();} }
	else{ mSubItems[id]->outWAVE_LEN_EYE(); }
}


Spu_reg SpuGp::getWAVE_LEN_EYE( int id )
{
	return mSubItems[id]->getWAVE_LEN_EYE();
}

Spu_reg SpuGp::inWAVE_LEN_EYE( int id )
{
	return mSubItems[id]->inWAVE_LEN_EYE();
}


void SpuGp::setCOMPRESS_EYE_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_EYE_rate(value);} }
	else{ mSubItems[id]->setCOMPRESS_EYE_rate( value); }
}
void SpuGp::setCOMPRESS_EYE_peak(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_EYE_peak(value);} }
	else{ mSubItems[id]->setCOMPRESS_EYE_peak( value); }
}
void SpuGp::setCOMPRESS_EYE_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_EYE_en(value);} }
	else{ mSubItems[id]->setCOMPRESS_EYE_en( value); }
}

void SpuGp::setCOMPRESS_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setCOMPRESS_EYE(value);} }
	else{ mSubItems[id]->setCOMPRESS_EYE( value); }
}

void SpuGp::outCOMPRESS_EYE_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE_rate(value);}}
	else{ mSubItems[id]->outCOMPRESS_EYE_rate( value); }
}
void SpuGp::pulseCOMPRESS_EYE_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_EYE_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_EYE_rate( activeV, idleV, timeus ); }
}
void SpuGp::outCOMPRESS_EYE_peak( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE_peak(value);}}
	else{ mSubItems[id]->outCOMPRESS_EYE_peak( value); }
}
void SpuGp::pulseCOMPRESS_EYE_peak( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE_peak(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_EYE_peak(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_EYE_peak( activeV, idleV, timeus ); }
}
void SpuGp::outCOMPRESS_EYE_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE_en(value);}}
	else{ mSubItems[id]->outCOMPRESS_EYE_en( value); }
}
void SpuGp::pulseCOMPRESS_EYE_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignCOMPRESS_EYE_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseCOMPRESS_EYE_en( activeV, idleV, timeus ); }
}

void SpuGp::outCOMPRESS_EYE(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE(value);} }
	else{ mSubItems[id]->outCOMPRESS_EYE( value); }
}

void SpuGp::outCOMPRESS_EYE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outCOMPRESS_EYE();} }
	else{ mSubItems[id]->outCOMPRESS_EYE(); }
}


Spu_reg SpuGp::getCOMPRESS_EYE_rate( int id )
{
	return mSubItems[id]->getCOMPRESS_EYE_rate();
}
Spu_reg SpuGp::getCOMPRESS_EYE_peak( int id )
{
	return mSubItems[id]->getCOMPRESS_EYE_peak();
}
Spu_reg SpuGp::getCOMPRESS_EYE_en( int id )
{
	return mSubItems[id]->getCOMPRESS_EYE_en();
}

Spu_reg SpuGp::getCOMPRESS_EYE( int id )
{
	return mSubItems[id]->getCOMPRESS_EYE();
}

Spu_reg SpuGp::inCOMPRESS_EYE( int id )
{
	return mSubItems[id]->inCOMPRESS_EYE();
}


void SpuGp::setTX_LEN_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTX_LEN_EYE(value);} }
	else{ mSubItems[id]->setTX_LEN_EYE( value); }
}

void SpuGp::outTX_LEN_EYE( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_EYE(value);} }
	else{ mSubItems[id]->outTX_LEN_EYE( value); }
}

void SpuGp::outTX_LEN_EYE( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTX_LEN_EYE();} }
	else{ mSubItems[id]->outTX_LEN_EYE(); }
}


Spu_reg SpuGp::getTX_LEN_EYE( int id )
{
	return mSubItems[id]->getTX_LEN_EYE();
}

Spu_reg SpuGp::inTX_LEN_EYE( int id )
{
	return mSubItems[id]->inTX_LEN_EYE();
}


void SpuGp::setLA_LINEAR_INTX_CTRL_mian_bypass(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_LINEAR_INTX_CTRL_mian_bypass(value);} }
	else{ mSubItems[id]->setLA_LINEAR_INTX_CTRL_mian_bypass( value); }
}
void SpuGp::setLA_LINEAR_INTX_CTRL_zoom_bypass(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_LINEAR_INTX_CTRL_zoom_bypass(value);} }
	else{ mSubItems[id]->setLA_LINEAR_INTX_CTRL_zoom_bypass( value); }
}

void SpuGp::setLA_LINEAR_INTX_CTRL( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_LINEAR_INTX_CTRL(value);} }
	else{ mSubItems[id]->setLA_LINEAR_INTX_CTRL( value); }
}

void SpuGp::outLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_CTRL_mian_bypass(value);}}
	else{ mSubItems[id]->outLA_LINEAR_INTX_CTRL_mian_bypass( value); }
}
void SpuGp::pulseLA_LINEAR_INTX_CTRL_mian_bypass( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_CTRL_mian_bypass(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_LINEAR_INTX_CTRL_mian_bypass(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_LINEAR_INTX_CTRL_mian_bypass( activeV, idleV, timeus ); }
}
void SpuGp::outLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_CTRL_zoom_bypass(value);}}
	else{ mSubItems[id]->outLA_LINEAR_INTX_CTRL_zoom_bypass( value); }
}
void SpuGp::pulseLA_LINEAR_INTX_CTRL_zoom_bypass( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_CTRL_zoom_bypass(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignLA_LINEAR_INTX_CTRL_zoom_bypass(idleV);}
	}
	else
	{ mSubItems[id]->pulseLA_LINEAR_INTX_CTRL_zoom_bypass( activeV, idleV, timeus ); }
}

void SpuGp::outLA_LINEAR_INTX_CTRL(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_CTRL(value);} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_CTRL( value); }
}

void SpuGp::outLA_LINEAR_INTX_CTRL( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_CTRL();} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_CTRL(); }
}


Spu_reg SpuGp::getLA_LINEAR_INTX_CTRL_mian_bypass( int id )
{
	return mSubItems[id]->getLA_LINEAR_INTX_CTRL_mian_bypass();
}
Spu_reg SpuGp::getLA_LINEAR_INTX_CTRL_zoom_bypass( int id )
{
	return mSubItems[id]->getLA_LINEAR_INTX_CTRL_zoom_bypass();
}

Spu_reg SpuGp::getLA_LINEAR_INTX_CTRL( int id )
{
	return mSubItems[id]->getLA_LINEAR_INTX_CTRL();
}

Spu_reg SpuGp::inLA_LINEAR_INTX_CTRL( int id )
{
	return mSubItems[id]->inLA_LINEAR_INTX_CTRL();
}


void SpuGp::setLA_LINEAR_INTX_INIT( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_LINEAR_INTX_INIT(value);} }
	else{ mSubItems[id]->setLA_LINEAR_INTX_INIT( value); }
}

void SpuGp::outLA_LINEAR_INTX_INIT( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_INIT(value);} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_INIT( value); }
}

void SpuGp::outLA_LINEAR_INTX_INIT( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_INIT();} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_INIT(); }
}


Spu_reg SpuGp::getLA_LINEAR_INTX_INIT( int id )
{
	return mSubItems[id]->getLA_LINEAR_INTX_INIT();
}

Spu_reg SpuGp::inLA_LINEAR_INTX_INIT( int id )
{
	return mSubItems[id]->inLA_LINEAR_INTX_INIT();
}


void SpuGp::setLA_LINEAR_INTX_STEP( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_LINEAR_INTX_STEP(value);} }
	else{ mSubItems[id]->setLA_LINEAR_INTX_STEP( value); }
}

void SpuGp::outLA_LINEAR_INTX_STEP( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_STEP(value);} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_STEP( value); }
}

void SpuGp::outLA_LINEAR_INTX_STEP( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_STEP();} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_STEP(); }
}


Spu_reg SpuGp::getLA_LINEAR_INTX_STEP( int id )
{
	return mSubItems[id]->getLA_LINEAR_INTX_STEP();
}

Spu_reg SpuGp::inLA_LINEAR_INTX_STEP( int id )
{
	return mSubItems[id]->inLA_LINEAR_INTX_STEP();
}


void SpuGp::setLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_LINEAR_INTX_INIT_ZOOM(value);} }
	else{ mSubItems[id]->setLA_LINEAR_INTX_INIT_ZOOM( value); }
}

void SpuGp::outLA_LINEAR_INTX_INIT_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_INIT_ZOOM(value);} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_INIT_ZOOM( value); }
}

void SpuGp::outLA_LINEAR_INTX_INIT_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_INIT_ZOOM();} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_INIT_ZOOM(); }
}


Spu_reg SpuGp::getLA_LINEAR_INTX_INIT_ZOOM( int id )
{
	return mSubItems[id]->getLA_LINEAR_INTX_INIT_ZOOM();
}

Spu_reg SpuGp::inLA_LINEAR_INTX_INIT_ZOOM( int id )
{
	return mSubItems[id]->inLA_LINEAR_INTX_INIT_ZOOM();
}


void SpuGp::setLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setLA_LINEAR_INTX_STEP_ZOOM(value);} }
	else{ mSubItems[id]->setLA_LINEAR_INTX_STEP_ZOOM( value); }
}

void SpuGp::outLA_LINEAR_INTX_STEP_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_STEP_ZOOM(value);} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_STEP_ZOOM( value); }
}

void SpuGp::outLA_LINEAR_INTX_STEP_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outLA_LINEAR_INTX_STEP_ZOOM();} }
	else{ mSubItems[id]->outLA_LINEAR_INTX_STEP_ZOOM(); }
}


Spu_reg SpuGp::getLA_LINEAR_INTX_STEP_ZOOM( int id )
{
	return mSubItems[id]->getLA_LINEAR_INTX_STEP_ZOOM();
}

Spu_reg SpuGp::inLA_LINEAR_INTX_STEP_ZOOM( int id )
{
	return mSubItems[id]->inLA_LINEAR_INTX_STEP_ZOOM();
}


void SpuGp::setTRACE_COMPRESS_MAIN_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_MAIN_rate(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_MAIN_rate( value); }
}
void SpuGp::setTRACE_COMPRESS_MAIN_peak(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_MAIN_peak(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_MAIN_peak( value); }
}
void SpuGp::setTRACE_COMPRESS_MAIN_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_MAIN_en(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_MAIN_en( value); }
}

void SpuGp::setTRACE_COMPRESS_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_MAIN(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_MAIN( value); }
}

void SpuGp::outTRACE_COMPRESS_MAIN_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN_rate(value);}}
	else{ mSubItems[id]->outTRACE_COMPRESS_MAIN_rate( value); }
}
void SpuGp::pulseTRACE_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTRACE_COMPRESS_MAIN_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_COMPRESS_MAIN_rate( activeV, idleV, timeus ); }
}
void SpuGp::outTRACE_COMPRESS_MAIN_peak( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN_peak(value);}}
	else{ mSubItems[id]->outTRACE_COMPRESS_MAIN_peak( value); }
}
void SpuGp::pulseTRACE_COMPRESS_MAIN_peak( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN_peak(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTRACE_COMPRESS_MAIN_peak(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_COMPRESS_MAIN_peak( activeV, idleV, timeus ); }
}
void SpuGp::outTRACE_COMPRESS_MAIN_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN_en(value);}}
	else{ mSubItems[id]->outTRACE_COMPRESS_MAIN_en( value); }
}
void SpuGp::pulseTRACE_COMPRESS_MAIN_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTRACE_COMPRESS_MAIN_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_COMPRESS_MAIN_en( activeV, idleV, timeus ); }
}

void SpuGp::outTRACE_COMPRESS_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN(value);} }
	else{ mSubItems[id]->outTRACE_COMPRESS_MAIN( value); }
}

void SpuGp::outTRACE_COMPRESS_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_MAIN();} }
	else{ mSubItems[id]->outTRACE_COMPRESS_MAIN(); }
}


Spu_reg SpuGp::getTRACE_COMPRESS_MAIN_rate( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_MAIN_rate();
}
Spu_reg SpuGp::getTRACE_COMPRESS_MAIN_peak( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_MAIN_peak();
}
Spu_reg SpuGp::getTRACE_COMPRESS_MAIN_en( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_MAIN_en();
}

Spu_reg SpuGp::getTRACE_COMPRESS_MAIN( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_MAIN();
}

Spu_reg SpuGp::inTRACE_COMPRESS_MAIN( int id )
{
	return mSubItems[id]->inTRACE_COMPRESS_MAIN();
}


void SpuGp::setTRACE_COMPRESS_ZOOM_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_ZOOM_rate(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_ZOOM_rate( value); }
}
void SpuGp::setTRACE_COMPRESS_ZOOM_peak(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_ZOOM_peak(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_ZOOM_peak( value); }
}
void SpuGp::setTRACE_COMPRESS_ZOOM_en(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_ZOOM_en(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_ZOOM_en( value); }
}

void SpuGp::setTRACE_COMPRESS_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setTRACE_COMPRESS_ZOOM(value);} }
	else{ mSubItems[id]->setTRACE_COMPRESS_ZOOM( value); }
}

void SpuGp::outTRACE_COMPRESS_ZOOM_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM_rate(value);}}
	else{ mSubItems[id]->outTRACE_COMPRESS_ZOOM_rate( value); }
}
void SpuGp::pulseTRACE_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTRACE_COMPRESS_ZOOM_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_COMPRESS_ZOOM_rate( activeV, idleV, timeus ); }
}
void SpuGp::outTRACE_COMPRESS_ZOOM_peak( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM_peak(value);}}
	else{ mSubItems[id]->outTRACE_COMPRESS_ZOOM_peak( value); }
}
void SpuGp::pulseTRACE_COMPRESS_ZOOM_peak( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM_peak(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTRACE_COMPRESS_ZOOM_peak(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_COMPRESS_ZOOM_peak( activeV, idleV, timeus ); }
}
void SpuGp::outTRACE_COMPRESS_ZOOM_en( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM_en(value);}}
	else{ mSubItems[id]->outTRACE_COMPRESS_ZOOM_en( value); }
}
void SpuGp::pulseTRACE_COMPRESS_ZOOM_en( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM_en(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignTRACE_COMPRESS_ZOOM_en(idleV);}
	}
	else
	{ mSubItems[id]->pulseTRACE_COMPRESS_ZOOM_en( activeV, idleV, timeus ); }
}

void SpuGp::outTRACE_COMPRESS_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM(value);} }
	else{ mSubItems[id]->outTRACE_COMPRESS_ZOOM( value); }
}

void SpuGp::outTRACE_COMPRESS_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outTRACE_COMPRESS_ZOOM();} }
	else{ mSubItems[id]->outTRACE_COMPRESS_ZOOM(); }
}


Spu_reg SpuGp::getTRACE_COMPRESS_ZOOM_rate( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_ZOOM_rate();
}
Spu_reg SpuGp::getTRACE_COMPRESS_ZOOM_peak( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_ZOOM_peak();
}
Spu_reg SpuGp::getTRACE_COMPRESS_ZOOM_en( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_ZOOM_en();
}

Spu_reg SpuGp::getTRACE_COMPRESS_ZOOM( int id )
{
	return mSubItems[id]->getTRACE_COMPRESS_ZOOM();
}

Spu_reg SpuGp::inTRACE_COMPRESS_ZOOM( int id )
{
	return mSubItems[id]->inTRACE_COMPRESS_ZOOM();
}


void SpuGp::setZYNQ_COMPRESS_MAIN_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZYNQ_COMPRESS_MAIN_rate(value);} }
	else{ mSubItems[id]->setZYNQ_COMPRESS_MAIN_rate( value); }
}

void SpuGp::setZYNQ_COMPRESS_MAIN( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZYNQ_COMPRESS_MAIN(value);} }
	else{ mSubItems[id]->setZYNQ_COMPRESS_MAIN( value); }
}

void SpuGp::outZYNQ_COMPRESS_MAIN_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_MAIN_rate(value);}}
	else{ mSubItems[id]->outZYNQ_COMPRESS_MAIN_rate( value); }
}
void SpuGp::pulseZYNQ_COMPRESS_MAIN_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_MAIN_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZYNQ_COMPRESS_MAIN_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseZYNQ_COMPRESS_MAIN_rate( activeV, idleV, timeus ); }
}

void SpuGp::outZYNQ_COMPRESS_MAIN(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_MAIN(value);} }
	else{ mSubItems[id]->outZYNQ_COMPRESS_MAIN( value); }
}

void SpuGp::outZYNQ_COMPRESS_MAIN( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_MAIN();} }
	else{ mSubItems[id]->outZYNQ_COMPRESS_MAIN(); }
}


Spu_reg SpuGp::getZYNQ_COMPRESS_MAIN_rate( int id )
{
	return mSubItems[id]->getZYNQ_COMPRESS_MAIN_rate();
}

Spu_reg SpuGp::getZYNQ_COMPRESS_MAIN( int id )
{
	return mSubItems[id]->getZYNQ_COMPRESS_MAIN();
}

Spu_reg SpuGp::inZYNQ_COMPRESS_MAIN( int id )
{
	return mSubItems[id]->inZYNQ_COMPRESS_MAIN();
}


void SpuGp::setZYNQ_COMPRESS_ZOOM_rate(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZYNQ_COMPRESS_ZOOM_rate(value);} }
	else{ mSubItems[id]->setZYNQ_COMPRESS_ZOOM_rate( value); }
}

void SpuGp::setZYNQ_COMPRESS_ZOOM( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->setZYNQ_COMPRESS_ZOOM(value);} }
	else{ mSubItems[id]->setZYNQ_COMPRESS_ZOOM( value); }
}

void SpuGp::outZYNQ_COMPRESS_ZOOM_rate( Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_ZOOM_rate(value);}}
	else{ mSubItems[id]->outZYNQ_COMPRESS_ZOOM_rate( value); }
}
void SpuGp::pulseZYNQ_COMPRESS_ZOOM_rate( Spu_reg activeV, Spu_reg idleV, int timeus, int id )
{
	if ( id == -1 )
	{
		foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_ZOOM_rate(activeV);}
		IPhy::usleep(timeus);
		foreach( IPhySpu *pItem, mSubItems){ pItem->assignZYNQ_COMPRESS_ZOOM_rate(idleV);}
	}
	else
	{ mSubItems[id]->pulseZYNQ_COMPRESS_ZOOM_rate( activeV, idleV, timeus ); }
}

void SpuGp::outZYNQ_COMPRESS_ZOOM(Spu_reg value, int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_ZOOM(value);} }
	else{ mSubItems[id]->outZYNQ_COMPRESS_ZOOM( value); }
}

void SpuGp::outZYNQ_COMPRESS_ZOOM( int id )
{
	if ( id == -1 ){ foreach( IPhySpu *pItem, mSubItems){ pItem->outZYNQ_COMPRESS_ZOOM();} }
	else{ mSubItems[id]->outZYNQ_COMPRESS_ZOOM(); }
}


Spu_reg SpuGp::getZYNQ_COMPRESS_ZOOM_rate( int id )
{
	return mSubItems[id]->getZYNQ_COMPRESS_ZOOM_rate();
}

Spu_reg SpuGp::getZYNQ_COMPRESS_ZOOM( int id )
{
	return mSubItems[id]->getZYNQ_COMPRESS_ZOOM();
}

Spu_reg SpuGp::inZYNQ_COMPRESS_ZOOM( int id )
{
	return mSubItems[id]->inZYNQ_COMPRESS_ZOOM();
}


Spu_reg SpuGp::getDEBUG_TX_LA_spu_tx_cnt( int id )
{
	return mSubItems[id]->getDEBUG_TX_LA_spu_tx_cnt();
}

Spu_reg SpuGp::getDEBUG_TX_LA( int id )
{
	return mSubItems[id]->getDEBUG_TX_LA();
}

Spu_reg SpuGp::inDEBUG_TX_LA( int id )
{
	return mSubItems[id]->inDEBUG_TX_LA();
}


Spu_reg SpuGp::getDEBUG_TX_FRM_spu_tx_frm_cnt( int id )
{
	return mSubItems[id]->getDEBUG_TX_FRM_spu_tx_frm_cnt();
}

Spu_reg SpuGp::getDEBUG_TX_FRM( int id )
{
	return mSubItems[id]->getDEBUG_TX_FRM();
}

Spu_reg SpuGp::inDEBUG_TX_FRM( int id )
{
	return mSubItems[id]->inDEBUG_TX_FRM();
}


Spu_reg SpuGp::getIMPORT_CNT_CH_cnt( int id )
{
	return mSubItems[id]->getIMPORT_CNT_CH_cnt();
}
Spu_reg SpuGp::getIMPORT_CNT_CH_cnt_clr( int id )
{
	return mSubItems[id]->getIMPORT_CNT_CH_cnt_clr();
}

Spu_reg SpuGp::getIMPORT_CNT_CH( int id )
{
	return mSubItems[id]->getIMPORT_CNT_CH();
}

Spu_reg SpuGp::inIMPORT_CNT_CH( int id )
{
	return mSubItems[id]->inIMPORT_CNT_CH();
}


Spu_reg SpuGp::getIMPORT_CNT_LA_cnt( int id )
{
	return mSubItems[id]->getIMPORT_CNT_LA_cnt();
}
Spu_reg SpuGp::getIMPORT_CNT_LA_cnt_clr( int id )
{
	return mSubItems[id]->getIMPORT_CNT_LA_cnt_clr();
}

Spu_reg SpuGp::getIMPORT_CNT_LA( int id )
{
	return mSubItems[id]->getIMPORT_CNT_LA();
}

Spu_reg SpuGp::inIMPORT_CNT_LA( int id )
{
	return mSubItems[id]->inIMPORT_CNT_LA();
}


Spu_reg SpuGp::getDEBUG_LINEAR_INTX1( int id )
{
	return mSubItems[id]->getDEBUG_LINEAR_INTX1();
}

Spu_reg SpuGp::inDEBUG_LINEAR_INTX1( int id )
{
	return mSubItems[id]->inDEBUG_LINEAR_INTX1();
}


Spu_reg SpuGp::getDEBUG_LINEAR_INTX2( int id )
{
	return mSubItems[id]->getDEBUG_LINEAR_INTX2();
}

Spu_reg SpuGp::inDEBUG_LINEAR_INTX2( int id )
{
	return mSubItems[id]->inDEBUG_LINEAR_INTX2();
}


Spu_reg SpuGp::getDEBUG_DDR( int id )
{
	return mSubItems[id]->getDEBUG_DDR();
}

Spu_reg SpuGp::inDEBUG_DDR( int id )
{
	return mSubItems[id]->inDEBUG_DDR();
}


Spu_reg SpuGp::getDEBUG_TX_LEN_BURST_tx_length_burst( int id )
{
	return mSubItems[id]->getDEBUG_TX_LEN_BURST_tx_length_burst();
}

Spu_reg SpuGp::getDEBUG_TX_LEN_BURST( int id )
{
	return mSubItems[id]->getDEBUG_TX_LEN_BURST();
}

Spu_reg SpuGp::inDEBUG_TX_LEN_BURST( int id )
{
	return mSubItems[id]->inDEBUG_TX_LEN_BURST();
}


