#include "ccu.h"
Ccu_reg CcuMemProxy::getVERSION()
{
	return VERSION;
}


void CcuMemProxy::assignSCU_STATE_scu_fsm( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,scu_fsm,value);
}
void CcuMemProxy::assignSCU_STATE_scu_sa_fsm( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,scu_sa_fsm,value);
}
void CcuMemProxy::assignSCU_STATE_sys_stop( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_stop,value);
}
void CcuMemProxy::assignSCU_STATE_scu_stop( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,scu_stop,value);
}
void CcuMemProxy::assignSCU_STATE_scu_loop( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,scu_loop,value);
}
void CcuMemProxy::assignSCU_STATE_sys_force_stop( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_force_stop,value);
}
void CcuMemProxy::assignSCU_STATE_process_sa( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,process_sa,value);
}
void CcuMemProxy::assignSCU_STATE_process_sa_done( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,process_sa_done,value);
}
void CcuMemProxy::assignSCU_STATE_sys_sa_en( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_sa_en,value);
}
void CcuMemProxy::assignSCU_STATE_sys_pre_sa_done( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_pre_sa_done,value);
}
void CcuMemProxy::assignSCU_STATE_sys_pos_sa_done( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_pos_sa_done,value);
}
void CcuMemProxy::assignSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,value);
}
void CcuMemProxy::assignSCU_STATE_sys_trig_d( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_trig_d,value);
}
void CcuMemProxy::assignSCU_STATE_tpu_scu_trig( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,tpu_scu_trig,value);
}
void CcuMemProxy::assignSCU_STATE_fine_trig_done_buf( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,fine_trig_done_buf,value);
}
void CcuMemProxy::assignSCU_STATE_avg_proc_done( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,avg_proc_done,value);
}
void CcuMemProxy::assignSCU_STATE_sys_avg_proc( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_avg_proc,value);
}
void CcuMemProxy::assignSCU_STATE_sys_avg_exp( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_avg_exp,value);
}
void CcuMemProxy::assignSCU_STATE_sys_wave_rd_en( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_wave_rd_en,value);
}
void CcuMemProxy::assignSCU_STATE_sys_wave_proc_en( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,sys_wave_proc_en,value);
}
void CcuMemProxy::assignSCU_STATE_wave_mem_rd_done_hold( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,wave_mem_rd_done_hold,value);
}
void CcuMemProxy::assignSCU_STATE_spu_tx_done_hold( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,spu_tx_done_hold,value);
}
void CcuMemProxy::assignSCU_STATE_gt_wave_tx_done( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,gt_wave_tx_done,value);
}
void CcuMemProxy::assignSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,value);
}
void CcuMemProxy::assignSCU_STATE_task_wave_tx_done_LA( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,task_wave_tx_done_LA,value);
}
void CcuMemProxy::assignSCU_STATE_task_wave_tx_done_CH( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,task_wave_tx_done_CH,value);
}
void CcuMemProxy::assignSCU_STATE_spu_proc_done( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,spu_proc_done,value);
}
void CcuMemProxy::assignSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,value);
}

void CcuMemProxy::assignSCU_STATE( Ccu_reg value )
{
	mem_assign_field(SCU_STATE,payload,value);
}


Ccu_reg CcuMemProxy::getSCU_STATE_scu_fsm()
{
	return SCU_STATE.scu_fsm;
}
Ccu_reg CcuMemProxy::getSCU_STATE_scu_sa_fsm()
{
	return SCU_STATE.scu_sa_fsm;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_stop()
{
	return SCU_STATE.sys_stop;
}
Ccu_reg CcuMemProxy::getSCU_STATE_scu_stop()
{
	return SCU_STATE.scu_stop;
}
Ccu_reg CcuMemProxy::getSCU_STATE_scu_loop()
{
	return SCU_STATE.scu_loop;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_force_stop()
{
	return SCU_STATE.sys_force_stop;
}
Ccu_reg CcuMemProxy::getSCU_STATE_process_sa()
{
	return SCU_STATE.process_sa;
}
Ccu_reg CcuMemProxy::getSCU_STATE_process_sa_done()
{
	return SCU_STATE.process_sa_done;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_sa_en()
{
	return SCU_STATE.sys_sa_en;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_pre_sa_done()
{
	return SCU_STATE.sys_pre_sa_done;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_pos_sa_done()
{
	return SCU_STATE.sys_pos_sa_done;
}
Ccu_reg CcuMemProxy::getSCU_STATE_spu_wave_mem_wr_done_hold()
{
	return SCU_STATE.spu_wave_mem_wr_done_hold;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_trig_d()
{
	return SCU_STATE.sys_trig_d;
}
Ccu_reg CcuMemProxy::getSCU_STATE_tpu_scu_trig()
{
	return SCU_STATE.tpu_scu_trig;
}
Ccu_reg CcuMemProxy::getSCU_STATE_fine_trig_done_buf()
{
	return SCU_STATE.fine_trig_done_buf;
}
Ccu_reg CcuMemProxy::getSCU_STATE_avg_proc_done()
{
	return SCU_STATE.avg_proc_done;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_avg_proc()
{
	return SCU_STATE.sys_avg_proc;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_avg_exp()
{
	return SCU_STATE.sys_avg_exp;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_wave_rd_en()
{
	return SCU_STATE.sys_wave_rd_en;
}
Ccu_reg CcuMemProxy::getSCU_STATE_sys_wave_proc_en()
{
	return SCU_STATE.sys_wave_proc_en;
}
Ccu_reg CcuMemProxy::getSCU_STATE_wave_mem_rd_done_hold()
{
	return SCU_STATE.wave_mem_rd_done_hold;
}
Ccu_reg CcuMemProxy::getSCU_STATE_spu_tx_done_hold()
{
	return SCU_STATE.spu_tx_done_hold;
}
Ccu_reg CcuMemProxy::getSCU_STATE_gt_wave_tx_done()
{
	return SCU_STATE.gt_wave_tx_done;
}
Ccu_reg CcuMemProxy::getSCU_STATE_spu_wav_rd_meas_once_tx_done()
{
	return SCU_STATE.spu_wav_rd_meas_once_tx_done;
}
Ccu_reg CcuMemProxy::getSCU_STATE_task_wave_tx_done_LA()
{
	return SCU_STATE.task_wave_tx_done_LA;
}
Ccu_reg CcuMemProxy::getSCU_STATE_task_wave_tx_done_CH()
{
	return SCU_STATE.task_wave_tx_done_CH;
}
Ccu_reg CcuMemProxy::getSCU_STATE_spu_proc_done()
{
	return SCU_STATE.spu_proc_done;
}
Ccu_reg CcuMemProxy::getSCU_STATE_scan_roll_sa_done_mem_tx_null()
{
	return SCU_STATE.scan_roll_sa_done_mem_tx_null;
}

Ccu_reg CcuMemProxy::getSCU_STATE()
{
	return SCU_STATE.payload;
}


void CcuMemProxy::assignCTRL_sys_stop_state( Ccu_reg value )
{
	mem_assign_field(CTRL,sys_stop_state,value);
}
void CcuMemProxy::assignCTRL_cfg_fsm_run_stop_n( Ccu_reg value )
{
	mem_assign_field(CTRL,cfg_fsm_run_stop_n,value);
}
void CcuMemProxy::assignCTRL_run_single( Ccu_reg value )
{
	mem_assign_field(CTRL,run_single,value);
}
void CcuMemProxy::assignCTRL_mode_play_last( Ccu_reg value )
{
	mem_assign_field(CTRL,mode_play_last,value);
}
void CcuMemProxy::assignCTRL_scu_fsm_run( Ccu_reg value )
{
	mem_assign_field(CTRL,scu_fsm_run,value);
}
void CcuMemProxy::assignCTRL_force_trig( Ccu_reg value )
{
	mem_assign_field(CTRL,force_trig,value);
}
void CcuMemProxy::assignCTRL_wpu_plot_display_finish( Ccu_reg value )
{
	mem_assign_field(CTRL,wpu_plot_display_finish,value);
}
void CcuMemProxy::assignCTRL_soft_reset_gt( Ccu_reg value )
{
	mem_assign_field(CTRL,soft_reset_gt,value);
}
void CcuMemProxy::assignCTRL_ms_sync( Ccu_reg value )
{
	mem_assign_field(CTRL,ms_sync,value);
}
void CcuMemProxy::assignCTRL_fsm_reset( Ccu_reg value )
{
	mem_assign_field(CTRL,fsm_reset,value);
}

void CcuMemProxy::assignCTRL( Ccu_reg value )
{
	mem_assign_field(CTRL,payload,value);
}


Ccu_reg CcuMemProxy::getCTRL_sys_stop_state()
{
	return CTRL.sys_stop_state;
}
Ccu_reg CcuMemProxy::getCTRL_cfg_fsm_run_stop_n()
{
	return CTRL.cfg_fsm_run_stop_n;
}
Ccu_reg CcuMemProxy::getCTRL_run_single()
{
	return CTRL.run_single;
}
Ccu_reg CcuMemProxy::getCTRL_mode_play_last()
{
	return CTRL.mode_play_last;
}
Ccu_reg CcuMemProxy::getCTRL_scu_fsm_run()
{
	return CTRL.scu_fsm_run;
}
Ccu_reg CcuMemProxy::getCTRL_force_trig()
{
	return CTRL.force_trig;
}
Ccu_reg CcuMemProxy::getCTRL_wpu_plot_display_finish()
{
	return CTRL.wpu_plot_display_finish;
}
Ccu_reg CcuMemProxy::getCTRL_soft_reset_gt()
{
	return CTRL.soft_reset_gt;
}
Ccu_reg CcuMemProxy::getCTRL_ms_sync()
{
	return CTRL.ms_sync;
}
Ccu_reg CcuMemProxy::getCTRL_fsm_reset()
{
	return CTRL.fsm_reset;
}

Ccu_reg CcuMemProxy::getCTRL()
{
	return CTRL.payload;
}


void CcuMemProxy::assignMODE_cfg_mode_scan( Ccu_reg value )
{
	mem_assign_field(MODE,cfg_mode_scan,value);
}
void CcuMemProxy::assignMODE_sys_mode_scan_trace( Ccu_reg value )
{
	mem_assign_field(MODE,sys_mode_scan_trace,value);
}
void CcuMemProxy::assignMODE_sys_mode_scan_zoom( Ccu_reg value )
{
	mem_assign_field(MODE,sys_mode_scan_zoom,value);
}
void CcuMemProxy::assignMODE_mode_scan_en( Ccu_reg value )
{
	mem_assign_field(MODE,mode_scan_en,value);
}
void CcuMemProxy::assignMODE_search_en_zoom( Ccu_reg value )
{
	mem_assign_field(MODE,search_en_zoom,value);
}
void CcuMemProxy::assignMODE_search_en_ch( Ccu_reg value )
{
	mem_assign_field(MODE,search_en_ch,value);
}
void CcuMemProxy::assignMODE_search_en_la( Ccu_reg value )
{
	mem_assign_field(MODE,search_en_la,value);
}
void CcuMemProxy::assignMODE_mode_import_type( Ccu_reg value )
{
	mem_assign_field(MODE,mode_import_type,value);
}
void CcuMemProxy::assignMODE_mode_export( Ccu_reg value )
{
	mem_assign_field(MODE,mode_export,value);
}
void CcuMemProxy::assignMODE_mode_import_play( Ccu_reg value )
{
	mem_assign_field(MODE,mode_import_play,value);
}
void CcuMemProxy::assignMODE_mode_import_rec( Ccu_reg value )
{
	mem_assign_field(MODE,mode_import_rec,value);
}
void CcuMemProxy::assignMODE_measure_en_zoom( Ccu_reg value )
{
	mem_assign_field(MODE,measure_en_zoom,value);
}
void CcuMemProxy::assignMODE_measure_en_ch( Ccu_reg value )
{
	mem_assign_field(MODE,measure_en_ch,value);
}
void CcuMemProxy::assignMODE_measure_en_la( Ccu_reg value )
{
	mem_assign_field(MODE,measure_en_la,value);
}
void CcuMemProxy::assignMODE_wave_ch_en( Ccu_reg value )
{
	mem_assign_field(MODE,wave_ch_en,value);
}
void CcuMemProxy::assignMODE_wave_la_en( Ccu_reg value )
{
	mem_assign_field(MODE,wave_la_en,value);
}
void CcuMemProxy::assignMODE_zoom_en( Ccu_reg value )
{
	mem_assign_field(MODE,zoom_en,value);
}
void CcuMemProxy::assignMODE_mask_err_stop_en( Ccu_reg value )
{
	mem_assign_field(MODE,mask_err_stop_en,value);
}
void CcuMemProxy::assignMODE_mask_save_fail( Ccu_reg value )
{
	mem_assign_field(MODE,mask_save_fail,value);
}
void CcuMemProxy::assignMODE_mask_save_pass( Ccu_reg value )
{
	mem_assign_field(MODE,mask_save_pass,value);
}
void CcuMemProxy::assignMODE_mask_pf_en( Ccu_reg value )
{
	mem_assign_field(MODE,mask_pf_en,value);
}
void CcuMemProxy::assignMODE_zone_trig_en( Ccu_reg value )
{
	mem_assign_field(MODE,zone_trig_en,value);
}
void CcuMemProxy::assignMODE_mode_roll_en( Ccu_reg value )
{
	mem_assign_field(MODE,mode_roll_en,value);
}
void CcuMemProxy::assignMODE_mode_fast_ref( Ccu_reg value )
{
	mem_assign_field(MODE,mode_fast_ref,value);
}
void CcuMemProxy::assignMODE_auto_trig( Ccu_reg value )
{
	mem_assign_field(MODE,auto_trig,value);
}
void CcuMemProxy::assignMODE_interleave_20G( Ccu_reg value )
{
	mem_assign_field(MODE,interleave_20G,value);
}
void CcuMemProxy::assignMODE_en_20G( Ccu_reg value )
{
	mem_assign_field(MODE,en_20G,value);
}

void CcuMemProxy::assignMODE( Ccu_reg value )
{
	mem_assign_field(MODE,payload,value);
}


Ccu_reg CcuMemProxy::getMODE_cfg_mode_scan()
{
	return MODE.cfg_mode_scan;
}
Ccu_reg CcuMemProxy::getMODE_sys_mode_scan_trace()
{
	return MODE.sys_mode_scan_trace;
}
Ccu_reg CcuMemProxy::getMODE_sys_mode_scan_zoom()
{
	return MODE.sys_mode_scan_zoom;
}
Ccu_reg CcuMemProxy::getMODE_mode_scan_en()
{
	return MODE.mode_scan_en;
}
Ccu_reg CcuMemProxy::getMODE_search_en_zoom()
{
	return MODE.search_en_zoom;
}
Ccu_reg CcuMemProxy::getMODE_search_en_ch()
{
	return MODE.search_en_ch;
}
Ccu_reg CcuMemProxy::getMODE_search_en_la()
{
	return MODE.search_en_la;
}
Ccu_reg CcuMemProxy::getMODE_mode_import_type()
{
	return MODE.mode_import_type;
}
Ccu_reg CcuMemProxy::getMODE_mode_export()
{
	return MODE.mode_export;
}
Ccu_reg CcuMemProxy::getMODE_mode_import_play()
{
	return MODE.mode_import_play;
}
Ccu_reg CcuMemProxy::getMODE_mode_import_rec()
{
	return MODE.mode_import_rec;
}
Ccu_reg CcuMemProxy::getMODE_measure_en_zoom()
{
	return MODE.measure_en_zoom;
}
Ccu_reg CcuMemProxy::getMODE_measure_en_ch()
{
	return MODE.measure_en_ch;
}
Ccu_reg CcuMemProxy::getMODE_measure_en_la()
{
	return MODE.measure_en_la;
}
Ccu_reg CcuMemProxy::getMODE_wave_ch_en()
{
	return MODE.wave_ch_en;
}
Ccu_reg CcuMemProxy::getMODE_wave_la_en()
{
	return MODE.wave_la_en;
}
Ccu_reg CcuMemProxy::getMODE_zoom_en()
{
	return MODE.zoom_en;
}
Ccu_reg CcuMemProxy::getMODE_mask_err_stop_en()
{
	return MODE.mask_err_stop_en;
}
Ccu_reg CcuMemProxy::getMODE_mask_save_fail()
{
	return MODE.mask_save_fail;
}
Ccu_reg CcuMemProxy::getMODE_mask_save_pass()
{
	return MODE.mask_save_pass;
}
Ccu_reg CcuMemProxy::getMODE_mask_pf_en()
{
	return MODE.mask_pf_en;
}
Ccu_reg CcuMemProxy::getMODE_zone_trig_en()
{
	return MODE.zone_trig_en;
}
Ccu_reg CcuMemProxy::getMODE_mode_roll_en()
{
	return MODE.mode_roll_en;
}
Ccu_reg CcuMemProxy::getMODE_mode_fast_ref()
{
	return MODE.mode_fast_ref;
}
Ccu_reg CcuMemProxy::getMODE_auto_trig()
{
	return MODE.auto_trig;
}
Ccu_reg CcuMemProxy::getMODE_interleave_20G()
{
	return MODE.interleave_20G;
}
Ccu_reg CcuMemProxy::getMODE_en_20G()
{
	return MODE.en_20G;
}

Ccu_reg CcuMemProxy::getMODE()
{
	return MODE.payload;
}


void CcuMemProxy::assignMODE_10G_chn_10G( Ccu_reg value )
{
	mem_assign_field(MODE_10G,chn_10G,value);
}
void CcuMemProxy::assignMODE_10G_cfg_chn( Ccu_reg value )
{
	mem_assign_field(MODE_10G,cfg_chn,value);
}

void CcuMemProxy::assignMODE_10G( Ccu_reg value )
{
	mem_assign_field(MODE_10G,payload,value);
}


Ccu_reg CcuMemProxy::getMODE_10G_chn_10G()
{
	return MODE_10G.chn_10G;
}
Ccu_reg CcuMemProxy::getMODE_10G_cfg_chn()
{
	return MODE_10G.cfg_chn;
}

Ccu_reg CcuMemProxy::getMODE_10G()
{
	return MODE_10G.payload;
}


void CcuMemProxy::assignPRE_SA( Ccu_reg value )
{
	mem_assign(PRE_SA,value);
}


Ccu_reg CcuMemProxy::getPRE_SA()
{
	return PRE_SA;
}


void CcuMemProxy::assignPOS_SA( Ccu_reg value )
{
	mem_assign(POS_SA,value);
}


Ccu_reg CcuMemProxy::getPOS_SA()
{
	return POS_SA;
}


void CcuMemProxy::assignSCAN_TRIG_POSITION_position( Ccu_reg value )
{
	mem_assign_field(SCAN_TRIG_POSITION,position,value);
}
void CcuMemProxy::assignSCAN_TRIG_POSITION_trig_left_side( Ccu_reg value )
{
	mem_assign_field(SCAN_TRIG_POSITION,trig_left_side,value);
}

void CcuMemProxy::assignSCAN_TRIG_POSITION( Ccu_reg value )
{
	mem_assign_field(SCAN_TRIG_POSITION,payload,value);
}


Ccu_reg CcuMemProxy::getSCAN_TRIG_POSITION_position()
{
	return SCAN_TRIG_POSITION.position;
}
Ccu_reg CcuMemProxy::getSCAN_TRIG_POSITION_trig_left_side()
{
	return SCAN_TRIG_POSITION.trig_left_side;
}

Ccu_reg CcuMemProxy::getSCAN_TRIG_POSITION()
{
	return SCAN_TRIG_POSITION.payload;
}


void CcuMemProxy::assignAUTO_TRIG_TIMEOUT( Ccu_reg value )
{
	mem_assign(AUTO_TRIG_TIMEOUT,value);
}


Ccu_reg CcuMemProxy::getAUTO_TRIG_TIMEOUT()
{
	return AUTO_TRIG_TIMEOUT;
}


void CcuMemProxy::assignAUTO_TRIG_HOLD_OFF( Ccu_reg value )
{
	mem_assign(AUTO_TRIG_HOLD_OFF,value);
}


Ccu_reg CcuMemProxy::getAUTO_TRIG_HOLD_OFF()
{
	return AUTO_TRIG_HOLD_OFF;
}


void CcuMemProxy::assignFSM_DELAY( Ccu_reg value )
{
	mem_assign(FSM_DELAY,value);
}


Ccu_reg CcuMemProxy::getFSM_DELAY()
{
	return FSM_DELAY;
}


void CcuMemProxy::assignPLOT_DELAY( Ccu_reg value )
{
	mem_assign(PLOT_DELAY,value);
}


Ccu_reg CcuMemProxy::getPLOT_DELAY()
{
	return PLOT_DELAY;
}


void CcuMemProxy::assignFRAME_PLOT_NUM( Ccu_reg value )
{
	mem_assign(FRAME_PLOT_NUM,value);
}


Ccu_reg CcuMemProxy::getFRAME_PLOT_NUM()
{
	return FRAME_PLOT_NUM;
}


void CcuMemProxy::assignREC_PLAY_index_full( Ccu_reg value )
{
	mem_assign_field(REC_PLAY,index_full,value);
}
void CcuMemProxy::assignREC_PLAY_loop_playback( Ccu_reg value )
{
	mem_assign_field(REC_PLAY,loop_playback,value);
}
void CcuMemProxy::assignREC_PLAY_index_load( Ccu_reg value )
{
	mem_assign_field(REC_PLAY,index_load,value);
}
void CcuMemProxy::assignREC_PLAY_index_inc( Ccu_reg value )
{
	mem_assign_field(REC_PLAY,index_inc,value);
}
void CcuMemProxy::assignREC_PLAY_mode_play( Ccu_reg value )
{
	mem_assign_field(REC_PLAY,mode_play,value);
}
void CcuMemProxy::assignREC_PLAY_mode_rec( Ccu_reg value )
{
	mem_assign_field(REC_PLAY,mode_rec,value);
}

void CcuMemProxy::assignREC_PLAY( Ccu_reg value )
{
	mem_assign_field(REC_PLAY,payload,value);
}


Ccu_reg CcuMemProxy::getREC_PLAY_index_full()
{
	return REC_PLAY.index_full;
}
Ccu_reg CcuMemProxy::getREC_PLAY_loop_playback()
{
	return REC_PLAY.loop_playback;
}
Ccu_reg CcuMemProxy::getREC_PLAY_index_load()
{
	return REC_PLAY.index_load;
}
Ccu_reg CcuMemProxy::getREC_PLAY_index_inc()
{
	return REC_PLAY.index_inc;
}
Ccu_reg CcuMemProxy::getREC_PLAY_mode_play()
{
	return REC_PLAY.mode_play;
}
Ccu_reg CcuMemProxy::getREC_PLAY_mode_rec()
{
	return REC_PLAY.mode_rec;
}

Ccu_reg CcuMemProxy::getREC_PLAY()
{
	return REC_PLAY.payload;
}


void CcuMemProxy::assignWAVE_INFO_wave_plot_frame_cnt( Ccu_reg value )
{
	mem_assign_field(WAVE_INFO,wave_plot_frame_cnt,value);
}
void CcuMemProxy::assignWAVE_INFO_wave_play_vld( Ccu_reg value )
{
	mem_assign_field(WAVE_INFO,wave_play_vld,value);
}

void CcuMemProxy::assignWAVE_INFO( Ccu_reg value )
{
	mem_assign_field(WAVE_INFO,payload,value);
}


Ccu_reg CcuMemProxy::getWAVE_INFO_wave_plot_frame_cnt()
{
	return WAVE_INFO.wave_plot_frame_cnt;
}
Ccu_reg CcuMemProxy::getWAVE_INFO_wave_play_vld()
{
	return WAVE_INFO.wave_play_vld;
}

Ccu_reg CcuMemProxy::getWAVE_INFO()
{
	return WAVE_INFO.payload;
}


void CcuMemProxy::assignINDEX_CUR_index( Ccu_reg value )
{
	mem_assign_field(INDEX_CUR,index,value);
}

void CcuMemProxy::assignINDEX_CUR( Ccu_reg value )
{
	mem_assign_field(INDEX_CUR,payload,value);
}


Ccu_reg CcuMemProxy::getINDEX_CUR_index()
{
	return INDEX_CUR.index;
}

Ccu_reg CcuMemProxy::getINDEX_CUR()
{
	return INDEX_CUR.payload;
}


void CcuMemProxy::assignINDEX_BASE_index( Ccu_reg value )
{
	mem_assign_field(INDEX_BASE,index,value);
}

void CcuMemProxy::assignINDEX_BASE( Ccu_reg value )
{
	mem_assign_field(INDEX_BASE,payload,value);
}


Ccu_reg CcuMemProxy::getINDEX_BASE_index()
{
	return INDEX_BASE.index;
}

Ccu_reg CcuMemProxy::getINDEX_BASE()
{
	return INDEX_BASE.payload;
}


void CcuMemProxy::assignINDEX_UPPER_index( Ccu_reg value )
{
	mem_assign_field(INDEX_UPPER,index,value);
}

void CcuMemProxy::assignINDEX_UPPER( Ccu_reg value )
{
	mem_assign_field(INDEX_UPPER,payload,value);
}


Ccu_reg CcuMemProxy::getINDEX_UPPER_index()
{
	return INDEX_UPPER.index;
}

Ccu_reg CcuMemProxy::getINDEX_UPPER()
{
	return INDEX_UPPER.payload;
}


void CcuMemProxy::assignTRACE_trace_once_req( Ccu_reg value )
{
	mem_assign_field(TRACE,trace_once_req,value);
}
void CcuMemProxy::assignTRACE_measure_once_req( Ccu_reg value )
{
	mem_assign_field(TRACE,measure_once_req,value);
}
void CcuMemProxy::assignTRACE_search_once_req( Ccu_reg value )
{
	mem_assign_field(TRACE,search_once_req,value);
}
void CcuMemProxy::assignTRACE_eye_once_req( Ccu_reg value )
{
	mem_assign_field(TRACE,eye_once_req,value);
}
void CcuMemProxy::assignTRACE_wave_bypass_wpu( Ccu_reg value )
{
	mem_assign_field(TRACE,wave_bypass_wpu,value);
}
void CcuMemProxy::assignTRACE_wave_bypass_trace( Ccu_reg value )
{
	mem_assign_field(TRACE,wave_bypass_trace,value);
}
void CcuMemProxy::assignTRACE_avg_clr( Ccu_reg value )
{
	mem_assign_field(TRACE,avg_clr,value);
}

void CcuMemProxy::assignTRACE( Ccu_reg value )
{
	mem_assign_field(TRACE,payload,value);
}


Ccu_reg CcuMemProxy::getTRACE_trace_once_req()
{
	return TRACE.trace_once_req;
}
Ccu_reg CcuMemProxy::getTRACE_measure_once_req()
{
	return TRACE.measure_once_req;
}
Ccu_reg CcuMemProxy::getTRACE_search_once_req()
{
	return TRACE.search_once_req;
}
Ccu_reg CcuMemProxy::getTRACE_eye_once_req()
{
	return TRACE.eye_once_req;
}
Ccu_reg CcuMemProxy::getTRACE_wave_bypass_wpu()
{
	return TRACE.wave_bypass_wpu;
}
Ccu_reg CcuMemProxy::getTRACE_wave_bypass_trace()
{
	return TRACE.wave_bypass_trace;
}
Ccu_reg CcuMemProxy::getTRACE_avg_clr()
{
	return TRACE.avg_clr;
}

Ccu_reg CcuMemProxy::getTRACE()
{
	return TRACE.payload;
}


void CcuMemProxy::assignTRIG_DLY( Ccu_reg value )
{
	mem_assign(TRIG_DLY,value);
}


Ccu_reg CcuMemProxy::getTRIG_DLY()
{
	return TRIG_DLY;
}


void CcuMemProxy::assignPOS_SA_LAST_VIEW( Ccu_reg value )
{
	mem_assign(POS_SA_LAST_VIEW,value);
}


Ccu_reg CcuMemProxy::getPOS_SA_LAST_VIEW()
{
	return POS_SA_LAST_VIEW;
}


void CcuMemProxy::assignMASK_FRM_NUM_L( Ccu_reg value )
{
	mem_assign(MASK_FRM_NUM_L,value);
}


Ccu_reg CcuMemProxy::getMASK_FRM_NUM_L()
{
	return MASK_FRM_NUM_L;
}


void CcuMemProxy::assignMASK_FRM_NUM_H_frm_num_h( Ccu_reg value )
{
	mem_assign_field(MASK_FRM_NUM_H,frm_num_h,value);
}
void CcuMemProxy::assignMASK_FRM_NUM_H_frm_num_en( Ccu_reg value )
{
	mem_assign_field(MASK_FRM_NUM_H,frm_num_en,value);
}

void CcuMemProxy::assignMASK_FRM_NUM_H( Ccu_reg value )
{
	mem_assign_field(MASK_FRM_NUM_H,payload,value);
}


Ccu_reg CcuMemProxy::getMASK_FRM_NUM_H_frm_num_h()
{
	return MASK_FRM_NUM_H.frm_num_h;
}
Ccu_reg CcuMemProxy::getMASK_FRM_NUM_H_frm_num_en()
{
	return MASK_FRM_NUM_H.frm_num_en;
}

Ccu_reg CcuMemProxy::getMASK_FRM_NUM_H()
{
	return MASK_FRM_NUM_H.payload;
}


void CcuMemProxy::assignMASK_TIMEOUT_timeout( Ccu_reg value )
{
	mem_assign_field(MASK_TIMEOUT,timeout,value);
}
void CcuMemProxy::assignMASK_TIMEOUT_timeout_en( Ccu_reg value )
{
	mem_assign_field(MASK_TIMEOUT,timeout_en,value);
}

void CcuMemProxy::assignMASK_TIMEOUT( Ccu_reg value )
{
	mem_assign_field(MASK_TIMEOUT,payload,value);
}


Ccu_reg CcuMemProxy::getMASK_TIMEOUT_timeout()
{
	return MASK_TIMEOUT.timeout;
}
Ccu_reg CcuMemProxy::getMASK_TIMEOUT_timeout_en()
{
	return MASK_TIMEOUT.timeout_en;
}

Ccu_reg CcuMemProxy::getMASK_TIMEOUT()
{
	return MASK_TIMEOUT.payload;
}


void CcuMemProxy::assignSTATE_DISPLAY_scu_fsm( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,scu_fsm,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,sys_pre_sa_done,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_sys_stop_state( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,sys_stop_state,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,disp_sys_trig_d,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_rec_play_stop_state( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,rec_play_stop_state,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_mask_stop_state( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,mask_stop_state,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_stop_stata_clr( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,stop_stata_clr,value);
}
void CcuMemProxy::assignSTATE_DISPLAY_disp_trig_clr( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,disp_trig_clr,value);
}

void CcuMemProxy::assignSTATE_DISPLAY( Ccu_reg value )
{
	mem_assign_field(STATE_DISPLAY,payload,value);
}


Ccu_reg CcuMemProxy::getSTATE_DISPLAY_scu_fsm()
{
	return STATE_DISPLAY.scu_fsm;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_sys_pre_sa_done()
{
	return STATE_DISPLAY.sys_pre_sa_done;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_sys_stop_state()
{
	return STATE_DISPLAY.sys_stop_state;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_disp_tpu_scu_trig()
{
	return STATE_DISPLAY.disp_tpu_scu_trig;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_disp_sys_trig_d()
{
	return STATE_DISPLAY.disp_sys_trig_d;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_rec_play_stop_state()
{
	return STATE_DISPLAY.rec_play_stop_state;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_mask_stop_state()
{
	return STATE_DISPLAY.mask_stop_state;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_stop_stata_clr()
{
	return STATE_DISPLAY.stop_stata_clr;
}
Ccu_reg CcuMemProxy::getSTATE_DISPLAY_disp_trig_clr()
{
	return STATE_DISPLAY.disp_trig_clr;
}

Ccu_reg CcuMemProxy::getSTATE_DISPLAY()
{
	return STATE_DISPLAY.payload;
}


Ccu_reg CcuMemProxy::getMASK_TIMER()
{
	return MASK_TIMER;
}


void CcuMemProxy::assignCONFIG_ID( Ccu_reg value )
{
	mem_assign(CONFIG_ID,value);
}


Ccu_reg CcuMemProxy::getCONFIG_ID()
{
	return CONFIG_ID;
}


void CcuMemProxy::assignDEUBG_wave_sa_ctrl( Ccu_reg value )
{
	mem_assign_field(DEUBG,wave_sa_ctrl,value);
}
void CcuMemProxy::assignDEUBG_sys_auto_trig_act( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_auto_trig_act,value);
}
void CcuMemProxy::assignDEUBG_sys_auto_trig_en( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_auto_trig_en,value);
}
void CcuMemProxy::assignDEUBG_sys_fine_trig_req( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_fine_trig_req,value);
}
void CcuMemProxy::assignDEUBG_sys_meas_only( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_meas_only,value);
}
void CcuMemProxy::assignDEUBG_sys_meas_en( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_meas_en,value);
}
void CcuMemProxy::assignDEUBG_sys_meas_type( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_meas_type,value);
}
void CcuMemProxy::assignDEUBG_spu_tx_roll_null( Ccu_reg value )
{
	mem_assign_field(DEUBG,spu_tx_roll_null,value);
}
void CcuMemProxy::assignDEUBG_measure_once_done_hold( Ccu_reg value )
{
	mem_assign_field(DEUBG,measure_once_done_hold,value);
}
void CcuMemProxy::assignDEUBG_measure_finish_done_hold( Ccu_reg value )
{
	mem_assign_field(DEUBG,measure_finish_done_hold,value);
}
void CcuMemProxy::assignDEUBG_sys_mask_en( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_mask_en,value);
}
void CcuMemProxy::assignDEUBG_mask_pf_result_vld_hold( Ccu_reg value )
{
	mem_assign_field(DEUBG,mask_pf_result_vld_hold,value);
}
void CcuMemProxy::assignDEUBG_mask_pf_result_cross_hold( Ccu_reg value )
{
	mem_assign_field(DEUBG,mask_pf_result_cross_hold,value);
}
void CcuMemProxy::assignDEUBG_mask_pf_timeout( Ccu_reg value )
{
	mem_assign_field(DEUBG,mask_pf_timeout,value);
}
void CcuMemProxy::assignDEUBG_mask_frm_overflow( Ccu_reg value )
{
	mem_assign_field(DEUBG,mask_frm_overflow,value);
}
void CcuMemProxy::assignDEUBG_measure_once_buf( Ccu_reg value )
{
	mem_assign_field(DEUBG,measure_once_buf,value);
}
void CcuMemProxy::assignDEUBG_zone_result_vld_hold( Ccu_reg value )
{
	mem_assign_field(DEUBG,zone_result_vld_hold,value);
}
void CcuMemProxy::assignDEUBG_zone_result_trig_hold( Ccu_reg value )
{
	mem_assign_field(DEUBG,zone_result_trig_hold,value);
}
void CcuMemProxy::assignDEUBG_wave_proc_la( Ccu_reg value )
{
	mem_assign_field(DEUBG,wave_proc_la,value);
}
void CcuMemProxy::assignDEUBG_sys_zoom( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_zoom,value);
}
void CcuMemProxy::assignDEUBG_sys_fast_ref( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_fast_ref,value);
}
void CcuMemProxy::assignDEUBG_spu_tx_done( Ccu_reg value )
{
	mem_assign_field(DEUBG,spu_tx_done,value);
}
void CcuMemProxy::assignDEUBG_sys_avg_en( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_avg_en,value);
}
void CcuMemProxy::assignDEUBG_sys_trace( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_trace,value);
}
void CcuMemProxy::assignDEUBG_sys_pos_sa_last_frm( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_pos_sa_last_frm,value);
}
void CcuMemProxy::assignDEUBG_sys_pos_sa_view( Ccu_reg value )
{
	mem_assign_field(DEUBG,sys_pos_sa_view,value);
}
void CcuMemProxy::assignDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg value )
{
	mem_assign_field(DEUBG,spu_wav_sa_rd_tx_done,value);
}
void CcuMemProxy::assignDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg value )
{
	mem_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,value);
}
void CcuMemProxy::assignDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg value )
{
	mem_assign_field(DEUBG,scan_roll_pos_last_frm_buf,value);
}

void CcuMemProxy::assignDEUBG( Ccu_reg value )
{
	mem_assign_field(DEUBG,payload,value);
}


Ccu_reg CcuMemProxy::getDEUBG_wave_sa_ctrl()
{
	return DEUBG.wave_sa_ctrl;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_auto_trig_act()
{
	return DEUBG.sys_auto_trig_act;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_auto_trig_en()
{
	return DEUBG.sys_auto_trig_en;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_fine_trig_req()
{
	return DEUBG.sys_fine_trig_req;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_meas_only()
{
	return DEUBG.sys_meas_only;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_meas_en()
{
	return DEUBG.sys_meas_en;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_meas_type()
{
	return DEUBG.sys_meas_type;
}
Ccu_reg CcuMemProxy::getDEUBG_spu_tx_roll_null()
{
	return DEUBG.spu_tx_roll_null;
}
Ccu_reg CcuMemProxy::getDEUBG_measure_once_done_hold()
{
	return DEUBG.measure_once_done_hold;
}
Ccu_reg CcuMemProxy::getDEUBG_measure_finish_done_hold()
{
	return DEUBG.measure_finish_done_hold;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_mask_en()
{
	return DEUBG.sys_mask_en;
}
Ccu_reg CcuMemProxy::getDEUBG_mask_pf_result_vld_hold()
{
	return DEUBG.mask_pf_result_vld_hold;
}
Ccu_reg CcuMemProxy::getDEUBG_mask_pf_result_cross_hold()
{
	return DEUBG.mask_pf_result_cross_hold;
}
Ccu_reg CcuMemProxy::getDEUBG_mask_pf_timeout()
{
	return DEUBG.mask_pf_timeout;
}
Ccu_reg CcuMemProxy::getDEUBG_mask_frm_overflow()
{
	return DEUBG.mask_frm_overflow;
}
Ccu_reg CcuMemProxy::getDEUBG_measure_once_buf()
{
	return DEUBG.measure_once_buf;
}
Ccu_reg CcuMemProxy::getDEUBG_zone_result_vld_hold()
{
	return DEUBG.zone_result_vld_hold;
}
Ccu_reg CcuMemProxy::getDEUBG_zone_result_trig_hold()
{
	return DEUBG.zone_result_trig_hold;
}
Ccu_reg CcuMemProxy::getDEUBG_wave_proc_la()
{
	return DEUBG.wave_proc_la;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_zoom()
{
	return DEUBG.sys_zoom;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_fast_ref()
{
	return DEUBG.sys_fast_ref;
}
Ccu_reg CcuMemProxy::getDEUBG_spu_tx_done()
{
	return DEUBG.spu_tx_done;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_avg_en()
{
	return DEUBG.sys_avg_en;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_trace()
{
	return DEUBG.sys_trace;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_pos_sa_last_frm()
{
	return DEUBG.sys_pos_sa_last_frm;
}
Ccu_reg CcuMemProxy::getDEUBG_sys_pos_sa_view()
{
	return DEUBG.sys_pos_sa_view;
}
Ccu_reg CcuMemProxy::getDEUBG_spu_wav_sa_rd_tx_done()
{
	return DEUBG.spu_wav_sa_rd_tx_done;
}
Ccu_reg CcuMemProxy::getDEUBG_scan_roll_pos_trig_frm_buf()
{
	return DEUBG.scan_roll_pos_trig_frm_buf;
}
Ccu_reg CcuMemProxy::getDEUBG_scan_roll_pos_last_frm_buf()
{
	return DEUBG.scan_roll_pos_last_frm_buf;
}

Ccu_reg CcuMemProxy::getDEUBG()
{
	return DEUBG.payload;
}


Ccu_reg CcuMemProxy::getDEBUG_GT_receiver_gtu_state()
{
	return DEBUG_GT.receiver_gtu_state;
}

Ccu_reg CcuMemProxy::getDEBUG_GT()
{
	return DEBUG_GT.payload;
}


void CcuMemProxy::assignDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg value )
{
	mem_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,value);
}

void CcuMemProxy::assignDEBUG_PLOT_DONE( Ccu_reg value )
{
	mem_assign_field(DEBUG_PLOT_DONE,payload,value);
}


Ccu_reg CcuMemProxy::getDEBUG_PLOT_DONE_wpu_plot_done_dly()
{
	return DEBUG_PLOT_DONE.wpu_plot_done_dly;
}

Ccu_reg CcuMemProxy::getDEBUG_PLOT_DONE()
{
	return DEBUG_PLOT_DONE.payload;
}


void CcuMemProxy::assignFPGA_SYNC_sync_cnt( Ccu_reg value )
{
	mem_assign_field(FPGA_SYNC,sync_cnt,value);
}
void CcuMemProxy::assignFPGA_SYNC_debug_scu_reset( Ccu_reg value )
{
	mem_assign_field(FPGA_SYNC,debug_scu_reset,value);
}
void CcuMemProxy::assignFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg value )
{
	mem_assign_field(FPGA_SYNC,debug_sys_rst_cnt,value);
}

void CcuMemProxy::assignFPGA_SYNC( Ccu_reg value )
{
	mem_assign_field(FPGA_SYNC,payload,value);
}


Ccu_reg CcuMemProxy::getFPGA_SYNC_sync_cnt()
{
	return FPGA_SYNC.sync_cnt;
}
Ccu_reg CcuMemProxy::getFPGA_SYNC_debug_scu_reset()
{
	return FPGA_SYNC.debug_scu_reset;
}
Ccu_reg CcuMemProxy::getFPGA_SYNC_debug_sys_rst_cnt()
{
	return FPGA_SYNC.debug_sys_rst_cnt;
}

Ccu_reg CcuMemProxy::getFPGA_SYNC()
{
	return FPGA_SYNC.payload;
}


Ccu_reg CcuMemProxy::getWPU_ID_sys_wpu_plot_id()
{
	return WPU_ID.sys_wpu_plot_id;
}
Ccu_reg CcuMemProxy::getWPU_ID_wpu_plot_done_id()
{
	return WPU_ID.wpu_plot_done_id;
}
Ccu_reg CcuMemProxy::getWPU_ID_wpu_display_done_id()
{
	return WPU_ID.wpu_display_done_id;
}

Ccu_reg CcuMemProxy::getWPU_ID()
{
	return WPU_ID.payload;
}


void CcuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,value);
}
void CcuMemProxy::assignDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,value);
}

void CcuMemProxy::assignDEBUG_WPU_PLOT( Ccu_reg value )
{
	mem_assign_field(DEBUG_WPU_PLOT,payload,value);
}


Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_display_finish()
{
	return DEBUG_WPU_PLOT.wpu_plot_display_finish;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_wpu_fast_done_buf()
{
	return DEBUG_WPU_PLOT.wpu_fast_done_buf;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_wpu_display_done_buf()
{
	return DEBUG_WPU_PLOT.wpu_display_done_buf;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_display_id_equ()
{
	return DEBUG_WPU_PLOT.wpu_plot_display_id_equ;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_plot_last()
{
	return DEBUG_WPU_PLOT.scu_wpu_plot_last;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_scu_wave_plot_req()
{
	return DEBUG_WPU_PLOT.scu_wave_plot_req;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_start()
{
	return DEBUG_WPU_PLOT.scu_wpu_start;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_sys_wpu_plot()
{
	return DEBUG_WPU_PLOT.sys_wpu_plot;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_done_buf()
{
	return DEBUG_WPU_PLOT.wpu_plot_done_buf;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_plot_en()
{
	return DEBUG_WPU_PLOT.scu_wpu_plot_en;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf()
{
	return DEBUG_WPU_PLOT.wpu_plot_scan_finish_buf;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf()
{
	return DEBUG_WPU_PLOT.wpu_plot_scan_zoom_finish_buf;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_stop_plot_en()
{
	return DEBUG_WPU_PLOT.scu_wpu_stop_plot_en;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld()
{
	return DEBUG_WPU_PLOT.scu_wpu_stop_plot_vld;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_cfg_wpu_flush()
{
	return DEBUG_WPU_PLOT.cfg_wpu_flush;
}
Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT_cfg_stop_plot()
{
	return DEBUG_WPU_PLOT.cfg_stop_plot;
}

Ccu_reg CcuMemProxy::getDEBUG_WPU_PLOT()
{
	return DEBUG_WPU_PLOT.payload;
}


void CcuMemProxy::assignDEBUG_GT_CRC_fail_sum( Ccu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,fail_sum,value);
}
void CcuMemProxy::assignDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,value);
}
void CcuMemProxy::assignDEBUG_GT_CRC_crc_valid_i( Ccu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,crc_valid_i,value);
}

void CcuMemProxy::assignDEBUG_GT_CRC( Ccu_reg value )
{
	mem_assign_field(DEBUG_GT_CRC,payload,value);
}


Ccu_reg CcuMemProxy::getDEBUG_GT_CRC_fail_sum()
{
	return DEBUG_GT_CRC.fail_sum;
}
Ccu_reg CcuMemProxy::getDEBUG_GT_CRC_crc_pass_fail_n_i()
{
	return DEBUG_GT_CRC.crc_pass_fail_n_i;
}
Ccu_reg CcuMemProxy::getDEBUG_GT_CRC_crc_valid_i()
{
	return DEBUG_GT_CRC.crc_valid_i;
}

Ccu_reg CcuMemProxy::getDEBUG_GT_CRC()
{
	return DEBUG_GT_CRC.payload;
}



void CcuMem::_loadOtp()
{
	VERSION=0x0;
	SCU_STATE.payload=0x0;
		SCU_STATE.scu_fsm=0x0;
		SCU_STATE.scu_sa_fsm=0x0;
		SCU_STATE.sys_stop=0x0;
		SCU_STATE.scu_stop=0x0;

		SCU_STATE.scu_loop=0x0;
		SCU_STATE.sys_force_stop=0x0;
		SCU_STATE.process_sa=0x0;
		SCU_STATE.process_sa_done=0x0;

		SCU_STATE.sys_sa_en=0x0;
		SCU_STATE.sys_pre_sa_done=0x0;
		SCU_STATE.sys_pos_sa_done=0x0;
		SCU_STATE.spu_wave_mem_wr_done_hold=0x0;

		SCU_STATE.sys_trig_d=0x0;
		SCU_STATE.tpu_scu_trig=0x0;
		SCU_STATE.fine_trig_done_buf=0x0;
		SCU_STATE.avg_proc_done=0x0;

		SCU_STATE.sys_avg_proc=0x0;
		SCU_STATE.sys_avg_exp=0x0;
		SCU_STATE.sys_wave_rd_en=0x0;
		SCU_STATE.sys_wave_proc_en=0x0;

		SCU_STATE.wave_mem_rd_done_hold=0x0;
		SCU_STATE.spu_tx_done_hold=0x0;
		SCU_STATE.gt_wave_tx_done=0x0;
		SCU_STATE.spu_wav_rd_meas_once_tx_done=0x0;

		SCU_STATE.task_wave_tx_done_LA=0x0;
		SCU_STATE.task_wave_tx_done_CH=0x0;
		SCU_STATE.spu_proc_done=0x0;
		SCU_STATE.scan_roll_sa_done_mem_tx_null=0x0;

	CTRL.payload=0x0;
		CTRL.sys_stop_state=0x0;
		CTRL.cfg_fsm_run_stop_n=0x0;
		CTRL.run_single=0x0;
		CTRL.mode_play_last=0x0;

		CTRL.scu_fsm_run=0x0;
		CTRL.force_trig=0x0;
		CTRL.wpu_plot_display_finish=0x0;
		CTRL.soft_reset_gt=0x0;

		CTRL.ms_sync=0x0;
		CTRL.fsm_reset=0x0;

	MODE.payload=0x0;
		MODE.cfg_mode_scan=0x0;
		MODE.sys_mode_scan_trace=0x0;
		MODE.sys_mode_scan_zoom=0x0;
		MODE.mode_scan_en=0x0;

		MODE.search_en_zoom=0x0;
		MODE.search_en_ch=0x1;
		MODE.search_en_la=0x1;
		MODE.mode_import_type=0x0;

		MODE.mode_export=0x0;
		MODE.mode_import_play=0x0;
		MODE.mode_import_rec=0x0;
		MODE.measure_en_zoom=0x0;

		MODE.measure_en_ch=0x0;
		MODE.measure_en_la=0x0;
		MODE.wave_ch_en=0x1;
		MODE.wave_la_en=0x0;

		MODE.zoom_en=0x0;
		MODE.mask_err_stop_en=0x0;
		MODE.mask_save_fail=0x1;
		MODE.mask_save_pass=0x1;

		MODE.mask_pf_en=0x0;
		MODE.zone_trig_en=0x0;
		MODE.mode_roll_en=0x0;
		MODE.mode_fast_ref=0x0;

		MODE.auto_trig=0x1;
		MODE.interleave_20G=0x4;
		MODE.en_20G=0x0;


	MODE_10G.payload=0x0;
		MODE_10G.chn_10G=0xf;
		MODE_10G.cfg_chn=0x0;

	PRE_SA=0x1f4;
	POS_SA=0x1f4;
	SCAN_TRIG_POSITION.payload=0x0;
		SCAN_TRIG_POSITION.position=0x0;
		SCAN_TRIG_POSITION.trig_left_side=0x0;


	AUTO_TRIG_TIMEOUT=0x3e8;
	AUTO_TRIG_HOLD_OFF=0x3e8;
	FSM_DELAY=0x0;
	PLOT_DELAY=0xbebc20;

	FRAME_PLOT_NUM=0x3e8;
	REC_PLAY.payload=0x0;
		REC_PLAY.index_full=0x0;
		REC_PLAY.loop_playback=0x0;
		REC_PLAY.index_load=0x0;
		REC_PLAY.index_inc=0x1;

		REC_PLAY.mode_play=0x0;
		REC_PLAY.mode_rec=0x0;

	WAVE_INFO.payload=0x0;
		WAVE_INFO.wave_plot_frame_cnt=0x0;
		WAVE_INFO.wave_play_vld=0x0;

	INDEX_CUR.payload=0x0;
		INDEX_CUR.index=0x0;


	INDEX_BASE.payload=0x0;
		INDEX_BASE.index=0x0;

	INDEX_UPPER.payload=0x64;
		INDEX_UPPER.index=0x3e7;

	TRACE.payload=0x0;
		TRACE.trace_once_req=0x1;
		TRACE.measure_once_req=0x0;
		TRACE.search_once_req=0x0;
		TRACE.eye_once_req=0x0;

		TRACE.wave_bypass_wpu=0x0;
		TRACE.wave_bypass_trace=0x0;
		TRACE.avg_clr=0x0;

	TRIG_DLY=0x28;

	POS_SA_LAST_VIEW=0x79d38;
	MASK_FRM_NUM_L=0x3e8;
	MASK_FRM_NUM_H.payload=0x0;
		MASK_FRM_NUM_H.frm_num_h=0x0;
		MASK_FRM_NUM_H.frm_num_en=0x1;

	MASK_TIMEOUT.payload=0x0;
		MASK_TIMEOUT.timeout=0x3e8;
		MASK_TIMEOUT.timeout_en=0x0;


	STATE_DISPLAY.payload=0x0;
		STATE_DISPLAY.scu_fsm=0x0;
		STATE_DISPLAY.sys_pre_sa_done=0x0;
		STATE_DISPLAY.sys_stop_state=0x0;
		STATE_DISPLAY.disp_tpu_scu_trig=0x0;

		STATE_DISPLAY.disp_sys_trig_d=0x0;
		STATE_DISPLAY.rec_play_stop_state=0x0;
		STATE_DISPLAY.mask_stop_state=0x0;
		STATE_DISPLAY.stop_stata_clr=0x0;

		STATE_DISPLAY.disp_trig_clr=0x0;

	MASK_TIMER=0x0;
	CONFIG_ID=0x0;
	DEUBG.payload=0x0;
		DEUBG.wave_sa_ctrl=0x0;
		DEUBG.sys_auto_trig_act=0x0;
		DEUBG.sys_auto_trig_en=0x0;
		DEUBG.sys_fine_trig_req=0x0;

		DEUBG.sys_meas_only=0x0;
		DEUBG.sys_meas_en=0x0;
		DEUBG.sys_meas_type=0x0;
		DEUBG.spu_tx_roll_null=0x0;

		DEUBG.measure_once_done_hold=0x0;
		DEUBG.measure_finish_done_hold=0x0;
		DEUBG.sys_mask_en=0x0;
		DEUBG.mask_pf_result_vld_hold=0x0;

		DEUBG.mask_pf_result_cross_hold=0x0;
		DEUBG.mask_pf_timeout=0x0;
		DEUBG.mask_frm_overflow=0x0;
		DEUBG.measure_once_buf=0x0;

		DEUBG.zone_result_vld_hold=0x0;
		DEUBG.zone_result_trig_hold=0x0;
		DEUBG.wave_proc_la=0x0;
		DEUBG.sys_zoom=0x0;

		DEUBG.sys_fast_ref=0x0;
		DEUBG.spu_tx_done=0x0;
		DEUBG.sys_avg_en=0x0;
		DEUBG.sys_trace=0x0;

		DEUBG.sys_pos_sa_last_frm=0x0;
		DEUBG.sys_pos_sa_view=0x0;
		DEUBG.spu_wav_sa_rd_tx_done=0x0;
		DEUBG.scan_roll_pos_trig_frm_buf=0x0;

		DEUBG.scan_roll_pos_last_frm_buf=0x0;


	DEBUG_GT.payload=0x0;
		DEBUG_GT.receiver_gtu_state=0x0;

	DEBUG_PLOT_DONE.payload=0x0;
		DEBUG_PLOT_DONE.wpu_plot_done_dly=0x0;

	FPGA_SYNC.payload=0x0;
		FPGA_SYNC.sync_cnt=0x0;
		FPGA_SYNC.debug_scu_reset=0x0;
		FPGA_SYNC.debug_sys_rst_cnt=0x0;

	WPU_ID.payload=0x0;
		WPU_ID.sys_wpu_plot_id=0x0;
		WPU_ID.wpu_plot_done_id=0x0;
		WPU_ID.wpu_display_done_id=0x0;


	DEBUG_WPU_PLOT.payload=0x0;
		DEBUG_WPU_PLOT.wpu_plot_display_finish=0x0;
		DEBUG_WPU_PLOT.wpu_fast_done_buf=0x0;
		DEBUG_WPU_PLOT.wpu_display_done_buf=0x0;
		DEBUG_WPU_PLOT.wpu_plot_display_id_equ=0x0;

		DEBUG_WPU_PLOT.scu_wpu_plot_last=0x0;
		DEBUG_WPU_PLOT.scu_wave_plot_req=0x0;
		DEBUG_WPU_PLOT.scu_wpu_start=0x0;
		DEBUG_WPU_PLOT.sys_wpu_plot=0x0;

		DEBUG_WPU_PLOT.wpu_plot_done_buf=0x0;
		DEBUG_WPU_PLOT.scu_wpu_plot_en=0x0;
		DEBUG_WPU_PLOT.wpu_plot_scan_finish_buf=0x0;
		DEBUG_WPU_PLOT.wpu_plot_scan_zoom_finish_buf=0x0;

		DEBUG_WPU_PLOT.scu_wpu_stop_plot_en=0x0;
		DEBUG_WPU_PLOT.scu_wpu_stop_plot_vld=0x0;
		DEBUG_WPU_PLOT.cfg_wpu_flush=0x0;
		DEBUG_WPU_PLOT.cfg_stop_plot=0x0;

	DEBUG_GT_CRC.payload=0x0;
		DEBUG_GT_CRC.fail_sum=0x0;
		DEBUG_GT_CRC.crc_pass_fail_n_i=0x0;
		DEBUG_GT_CRC.crc_valid_i=0x0;

}

void CcuMem::_outOtp()
{
	outSCU_STATE();
	outCTRL();
	outMODE();

	outMODE_10G();
	outPRE_SA();
	outPOS_SA();
	outSCAN_TRIG_POSITION();

	outAUTO_TRIG_TIMEOUT();
	outAUTO_TRIG_HOLD_OFF();
	outFSM_DELAY();
	outPLOT_DELAY();

	outFRAME_PLOT_NUM();
	outREC_PLAY();
	outWAVE_INFO();
	outINDEX_CUR();

	outINDEX_BASE();
	outINDEX_UPPER();
	outTRACE();
	outTRIG_DLY();

	outPOS_SA_LAST_VIEW();
	outMASK_FRM_NUM_L();
	outMASK_FRM_NUM_H();
	outMASK_TIMEOUT();

	outSTATE_DISPLAY();
	outCONFIG_ID();
	outDEUBG();

	outDEBUG_PLOT_DONE();
	outFPGA_SYNC();

	outDEBUG_WPU_PLOT();
	outDEBUG_GT_CRC();
}
void CcuMem::push( CcuMemProxy *proxy )
{
	if ( SCU_STATE.payload != proxy->SCU_STATE.payload )
	{reg_assign_field(SCU_STATE,payload,proxy->SCU_STATE.payload);}

	if ( CTRL.payload != proxy->CTRL.payload )
	{reg_assign_field(CTRL,payload,proxy->CTRL.payload);}

	if ( MODE.payload != proxy->MODE.payload )
	{reg_assign_field(MODE,payload,proxy->MODE.payload);}

	if ( MODE_10G.payload != proxy->MODE_10G.payload )
	{reg_assign_field(MODE_10G,payload,proxy->MODE_10G.payload);}

	if ( PRE_SA != proxy->PRE_SA )
	{reg_assign(PRE_SA,proxy->PRE_SA);}

	if ( POS_SA != proxy->POS_SA )
	{reg_assign(POS_SA,proxy->POS_SA);}

	if ( SCAN_TRIG_POSITION.payload != proxy->SCAN_TRIG_POSITION.payload )
	{reg_assign_field(SCAN_TRIG_POSITION,payload,proxy->SCAN_TRIG_POSITION.payload);}

	if ( AUTO_TRIG_TIMEOUT != proxy->AUTO_TRIG_TIMEOUT )
	{reg_assign(AUTO_TRIG_TIMEOUT,proxy->AUTO_TRIG_TIMEOUT);}

	if ( AUTO_TRIG_HOLD_OFF != proxy->AUTO_TRIG_HOLD_OFF )
	{reg_assign(AUTO_TRIG_HOLD_OFF,proxy->AUTO_TRIG_HOLD_OFF);}

	if ( FSM_DELAY != proxy->FSM_DELAY )
	{reg_assign(FSM_DELAY,proxy->FSM_DELAY);}

	if ( PLOT_DELAY != proxy->PLOT_DELAY )
	{reg_assign(PLOT_DELAY,proxy->PLOT_DELAY);}

	if ( FRAME_PLOT_NUM != proxy->FRAME_PLOT_NUM )
	{reg_assign(FRAME_PLOT_NUM,proxy->FRAME_PLOT_NUM);}

	if ( REC_PLAY.payload != proxy->REC_PLAY.payload )
	{reg_assign_field(REC_PLAY,payload,proxy->REC_PLAY.payload);}

	if ( WAVE_INFO.payload != proxy->WAVE_INFO.payload )
	{reg_assign_field(WAVE_INFO,payload,proxy->WAVE_INFO.payload);}

	if ( INDEX_CUR.payload != proxy->INDEX_CUR.payload )
	{reg_assign_field(INDEX_CUR,payload,proxy->INDEX_CUR.payload);}

	if ( INDEX_BASE.payload != proxy->INDEX_BASE.payload )
	{reg_assign_field(INDEX_BASE,payload,proxy->INDEX_BASE.payload);}

	if ( INDEX_UPPER.payload != proxy->INDEX_UPPER.payload )
	{reg_assign_field(INDEX_UPPER,payload,proxy->INDEX_UPPER.payload);}

	if ( TRACE.payload != proxy->TRACE.payload )
	{reg_assign_field(TRACE,payload,proxy->TRACE.payload);}

	if ( TRIG_DLY != proxy->TRIG_DLY )
	{reg_assign(TRIG_DLY,proxy->TRIG_DLY);}

	if ( POS_SA_LAST_VIEW != proxy->POS_SA_LAST_VIEW )
	{reg_assign(POS_SA_LAST_VIEW,proxy->POS_SA_LAST_VIEW);}

	if ( MASK_FRM_NUM_L != proxy->MASK_FRM_NUM_L )
	{reg_assign(MASK_FRM_NUM_L,proxy->MASK_FRM_NUM_L);}

	if ( MASK_FRM_NUM_H.payload != proxy->MASK_FRM_NUM_H.payload )
	{reg_assign_field(MASK_FRM_NUM_H,payload,proxy->MASK_FRM_NUM_H.payload);}

	if ( MASK_TIMEOUT.payload != proxy->MASK_TIMEOUT.payload )
	{reg_assign_field(MASK_TIMEOUT,payload,proxy->MASK_TIMEOUT.payload);}

	if ( STATE_DISPLAY.payload != proxy->STATE_DISPLAY.payload )
	{reg_assign_field(STATE_DISPLAY,payload,proxy->STATE_DISPLAY.payload);}

	if ( CONFIG_ID != proxy->CONFIG_ID )
	{reg_assign(CONFIG_ID,proxy->CONFIG_ID);}

	if ( DEUBG.payload != proxy->DEUBG.payload )
	{reg_assign_field(DEUBG,payload,proxy->DEUBG.payload);}

	if ( DEBUG_PLOT_DONE.payload != proxy->DEBUG_PLOT_DONE.payload )
	{reg_assign_field(DEBUG_PLOT_DONE,payload,proxy->DEBUG_PLOT_DONE.payload);}

	if ( FPGA_SYNC.payload != proxy->FPGA_SYNC.payload )
	{reg_assign_field(FPGA_SYNC,payload,proxy->FPGA_SYNC.payload);}

	if ( DEBUG_WPU_PLOT.payload != proxy->DEBUG_WPU_PLOT.payload )
	{reg_assign_field(DEBUG_WPU_PLOT,payload,proxy->DEBUG_WPU_PLOT.payload);}

	if ( DEBUG_GT_CRC.payload != proxy->DEBUG_GT_CRC.payload )
	{reg_assign_field(DEBUG_GT_CRC,payload,proxy->DEBUG_GT_CRC.payload);}

}
void CcuMem::pull( CcuMemProxy *proxy )
{
	Q_ASSERT( proxy!=NULL);

	reg_in(VERSION);
	reg_in(SCU_STATE);
	reg_in(CTRL);
	reg_in(MODE);
	reg_in(MODE_10G);
	reg_in(PRE_SA);
	reg_in(POS_SA);
	reg_in(SCAN_TRIG_POSITION);
	reg_in(AUTO_TRIG_TIMEOUT);
	reg_in(AUTO_TRIG_HOLD_OFF);
	reg_in(FSM_DELAY);
	reg_in(PLOT_DELAY);
	reg_in(FRAME_PLOT_NUM);
	reg_in(REC_PLAY);
	reg_in(WAVE_INFO);
	reg_in(INDEX_CUR);
	reg_in(INDEX_BASE);
	reg_in(INDEX_UPPER);
	reg_in(TRACE);
	reg_in(TRIG_DLY);
	reg_in(POS_SA_LAST_VIEW);
	reg_in(MASK_FRM_NUM_L);
	reg_in(MASK_FRM_NUM_H);
	reg_in(MASK_TIMEOUT);
	reg_in(STATE_DISPLAY);
	reg_in(MASK_TIMER);
	reg_in(CONFIG_ID);
	reg_in(DEUBG);
	reg_in(DEBUG_GT);
	reg_in(DEBUG_PLOT_DONE);
	reg_in(FPGA_SYNC);
	reg_in(WPU_ID);
	reg_in(DEBUG_WPU_PLOT);
	reg_in(DEBUG_GT_CRC);

	*proxy = CcuMemProxy(*this);
}
Ccu_reg CcuMem::inVERSION()
{
	reg_in(VERSION);
	return VERSION;
}


void CcuMem::setSCU_STATE_scu_fsm( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,scu_fsm,value);
}
void CcuMem::setSCU_STATE_scu_sa_fsm( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,scu_sa_fsm,value);
}
void CcuMem::setSCU_STATE_sys_stop( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_stop,value);
}
void CcuMem::setSCU_STATE_scu_stop( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,scu_stop,value);
}
void CcuMem::setSCU_STATE_scu_loop( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,scu_loop,value);
}
void CcuMem::setSCU_STATE_sys_force_stop( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_force_stop,value);
}
void CcuMem::setSCU_STATE_process_sa( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,process_sa,value);
}
void CcuMem::setSCU_STATE_process_sa_done( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,process_sa_done,value);
}
void CcuMem::setSCU_STATE_sys_sa_en( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_sa_en,value);
}
void CcuMem::setSCU_STATE_sys_pre_sa_done( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_pre_sa_done,value);
}
void CcuMem::setSCU_STATE_sys_pos_sa_done( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_pos_sa_done,value);
}
void CcuMem::setSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,value);
}
void CcuMem::setSCU_STATE_sys_trig_d( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_trig_d,value);
}
void CcuMem::setSCU_STATE_tpu_scu_trig( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,tpu_scu_trig,value);
}
void CcuMem::setSCU_STATE_fine_trig_done_buf( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,fine_trig_done_buf,value);
}
void CcuMem::setSCU_STATE_avg_proc_done( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,avg_proc_done,value);
}
void CcuMem::setSCU_STATE_sys_avg_proc( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_avg_proc,value);
}
void CcuMem::setSCU_STATE_sys_avg_exp( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_avg_exp,value);
}
void CcuMem::setSCU_STATE_sys_wave_rd_en( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_wave_rd_en,value);
}
void CcuMem::setSCU_STATE_sys_wave_proc_en( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,sys_wave_proc_en,value);
}
void CcuMem::setSCU_STATE_wave_mem_rd_done_hold( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,wave_mem_rd_done_hold,value);
}
void CcuMem::setSCU_STATE_spu_tx_done_hold( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,spu_tx_done_hold,value);
}
void CcuMem::setSCU_STATE_gt_wave_tx_done( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,gt_wave_tx_done,value);
}
void CcuMem::setSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,value);
}
void CcuMem::setSCU_STATE_task_wave_tx_done_LA( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,task_wave_tx_done_LA,value);
}
void CcuMem::setSCU_STATE_task_wave_tx_done_CH( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,task_wave_tx_done_CH,value);
}
void CcuMem::setSCU_STATE_spu_proc_done( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,spu_proc_done,value);
}
void CcuMem::setSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,value);
}

void CcuMem::setSCU_STATE( Ccu_reg value )
{
	reg_assign_field(SCU_STATE,payload,value);
}

void CcuMem::outSCU_STATE_scu_fsm( Ccu_reg value )
{
	out_assign_field(SCU_STATE,scu_fsm,value);
}
void CcuMem::pulseSCU_STATE_scu_fsm( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_fsm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_fsm,idleV);
}
void CcuMem::outSCU_STATE_scu_sa_fsm( Ccu_reg value )
{
	out_assign_field(SCU_STATE,scu_sa_fsm,value);
}
void CcuMem::pulseSCU_STATE_scu_sa_fsm( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_sa_fsm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_sa_fsm,idleV);
}
void CcuMem::outSCU_STATE_sys_stop( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_stop,value);
}
void CcuMem::pulseSCU_STATE_sys_stop( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_stop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_stop,idleV);
}
void CcuMem::outSCU_STATE_scu_stop( Ccu_reg value )
{
	out_assign_field(SCU_STATE,scu_stop,value);
}
void CcuMem::pulseSCU_STATE_scu_stop( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_stop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_stop,idleV);
}
void CcuMem::outSCU_STATE_scu_loop( Ccu_reg value )
{
	out_assign_field(SCU_STATE,scu_loop,value);
}
void CcuMem::pulseSCU_STATE_scu_loop( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scu_loop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scu_loop,idleV);
}
void CcuMem::outSCU_STATE_sys_force_stop( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_force_stop,value);
}
void CcuMem::pulseSCU_STATE_sys_force_stop( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_force_stop,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_force_stop,idleV);
}
void CcuMem::outSCU_STATE_process_sa( Ccu_reg value )
{
	out_assign_field(SCU_STATE,process_sa,value);
}
void CcuMem::pulseSCU_STATE_process_sa( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,process_sa,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,process_sa,idleV);
}
void CcuMem::outSCU_STATE_process_sa_done( Ccu_reg value )
{
	out_assign_field(SCU_STATE,process_sa_done,value);
}
void CcuMem::pulseSCU_STATE_process_sa_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,process_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,process_sa_done,idleV);
}
void CcuMem::outSCU_STATE_sys_sa_en( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_sa_en,value);
}
void CcuMem::pulseSCU_STATE_sys_sa_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_sa_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_sa_en,idleV);
}
void CcuMem::outSCU_STATE_sys_pre_sa_done( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_pre_sa_done,value);
}
void CcuMem::pulseSCU_STATE_sys_pre_sa_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_pre_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_pre_sa_done,idleV);
}
void CcuMem::outSCU_STATE_sys_pos_sa_done( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_pos_sa_done,value);
}
void CcuMem::pulseSCU_STATE_sys_pos_sa_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_pos_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_pos_sa_done,idleV);
}
void CcuMem::outSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg value )
{
	out_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,value);
}
void CcuMem::pulseSCU_STATE_spu_wave_mem_wr_done_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_wave_mem_wr_done_hold,idleV);
}
void CcuMem::outSCU_STATE_sys_trig_d( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_trig_d,value);
}
void CcuMem::pulseSCU_STATE_sys_trig_d( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_trig_d,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_trig_d,idleV);
}
void CcuMem::outSCU_STATE_tpu_scu_trig( Ccu_reg value )
{
	out_assign_field(SCU_STATE,tpu_scu_trig,value);
}
void CcuMem::pulseSCU_STATE_tpu_scu_trig( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,tpu_scu_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,tpu_scu_trig,idleV);
}
void CcuMem::outSCU_STATE_fine_trig_done_buf( Ccu_reg value )
{
	out_assign_field(SCU_STATE,fine_trig_done_buf,value);
}
void CcuMem::pulseSCU_STATE_fine_trig_done_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,fine_trig_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,fine_trig_done_buf,idleV);
}
void CcuMem::outSCU_STATE_avg_proc_done( Ccu_reg value )
{
	out_assign_field(SCU_STATE,avg_proc_done,value);
}
void CcuMem::pulseSCU_STATE_avg_proc_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,avg_proc_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,avg_proc_done,idleV);
}
void CcuMem::outSCU_STATE_sys_avg_proc( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_avg_proc,value);
}
void CcuMem::pulseSCU_STATE_sys_avg_proc( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_avg_proc,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_avg_proc,idleV);
}
void CcuMem::outSCU_STATE_sys_avg_exp( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_avg_exp,value);
}
void CcuMem::pulseSCU_STATE_sys_avg_exp( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_avg_exp,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_avg_exp,idleV);
}
void CcuMem::outSCU_STATE_sys_wave_rd_en( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_wave_rd_en,value);
}
void CcuMem::pulseSCU_STATE_sys_wave_rd_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_wave_rd_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_wave_rd_en,idleV);
}
void CcuMem::outSCU_STATE_sys_wave_proc_en( Ccu_reg value )
{
	out_assign_field(SCU_STATE,sys_wave_proc_en,value);
}
void CcuMem::pulseSCU_STATE_sys_wave_proc_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,sys_wave_proc_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,sys_wave_proc_en,idleV);
}
void CcuMem::outSCU_STATE_wave_mem_rd_done_hold( Ccu_reg value )
{
	out_assign_field(SCU_STATE,wave_mem_rd_done_hold,value);
}
void CcuMem::pulseSCU_STATE_wave_mem_rd_done_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,wave_mem_rd_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,wave_mem_rd_done_hold,idleV);
}
void CcuMem::outSCU_STATE_spu_tx_done_hold( Ccu_reg value )
{
	out_assign_field(SCU_STATE,spu_tx_done_hold,value);
}
void CcuMem::pulseSCU_STATE_spu_tx_done_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_tx_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_tx_done_hold,idleV);
}
void CcuMem::outSCU_STATE_gt_wave_tx_done( Ccu_reg value )
{
	out_assign_field(SCU_STATE,gt_wave_tx_done,value);
}
void CcuMem::pulseSCU_STATE_gt_wave_tx_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,gt_wave_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,gt_wave_tx_done,idleV);
}
void CcuMem::outSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg value )
{
	out_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,value);
}
void CcuMem::pulseSCU_STATE_spu_wav_rd_meas_once_tx_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_wav_rd_meas_once_tx_done,idleV);
}
void CcuMem::outSCU_STATE_task_wave_tx_done_LA( Ccu_reg value )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_LA,value);
}
void CcuMem::pulseSCU_STATE_task_wave_tx_done_LA( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_LA,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,task_wave_tx_done_LA,idleV);
}
void CcuMem::outSCU_STATE_task_wave_tx_done_CH( Ccu_reg value )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_CH,value);
}
void CcuMem::pulseSCU_STATE_task_wave_tx_done_CH( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,task_wave_tx_done_CH,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,task_wave_tx_done_CH,idleV);
}
void CcuMem::outSCU_STATE_spu_proc_done( Ccu_reg value )
{
	out_assign_field(SCU_STATE,spu_proc_done,value);
}
void CcuMem::pulseSCU_STATE_spu_proc_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,spu_proc_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,spu_proc_done,idleV);
}
void CcuMem::outSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg value )
{
	out_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,value);
}
void CcuMem::pulseSCU_STATE_scan_roll_sa_done_mem_tx_null( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCU_STATE,scan_roll_sa_done_mem_tx_null,idleV);
}

void CcuMem::outSCU_STATE( Ccu_reg value )
{
	out_assign_field(SCU_STATE,payload,value);
}

void CcuMem::outSCU_STATE()
{
	out_member(SCU_STATE);
}


Ccu_reg CcuMem::inSCU_STATE()
{
	reg_in(SCU_STATE);
	return SCU_STATE.payload;
}


void CcuMem::setCTRL_sys_stop_state( Ccu_reg value )
{
	reg_assign_field(CTRL,sys_stop_state,value);
}
void CcuMem::setCTRL_cfg_fsm_run_stop_n( Ccu_reg value )
{
	reg_assign_field(CTRL,cfg_fsm_run_stop_n,value);
}
void CcuMem::setCTRL_run_single( Ccu_reg value )
{
	reg_assign_field(CTRL,run_single,value);
}
void CcuMem::setCTRL_mode_play_last( Ccu_reg value )
{
	reg_assign_field(CTRL,mode_play_last,value);
}
void CcuMem::setCTRL_scu_fsm_run( Ccu_reg value )
{
	reg_assign_field(CTRL,scu_fsm_run,value);
}
void CcuMem::setCTRL_force_trig( Ccu_reg value )
{
	reg_assign_field(CTRL,force_trig,value);
}
void CcuMem::setCTRL_wpu_plot_display_finish( Ccu_reg value )
{
	reg_assign_field(CTRL,wpu_plot_display_finish,value);
}
void CcuMem::setCTRL_soft_reset_gt( Ccu_reg value )
{
	reg_assign_field(CTRL,soft_reset_gt,value);
}
void CcuMem::setCTRL_ms_sync( Ccu_reg value )
{
	reg_assign_field(CTRL,ms_sync,value);
}
void CcuMem::setCTRL_fsm_reset( Ccu_reg value )
{
	reg_assign_field(CTRL,fsm_reset,value);
}

void CcuMem::setCTRL( Ccu_reg value )
{
	reg_assign_field(CTRL,payload,value);
}

void CcuMem::outCTRL_sys_stop_state( Ccu_reg value )
{
	out_assign_field(CTRL,sys_stop_state,value);
}
void CcuMem::pulseCTRL_sys_stop_state( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,sys_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,sys_stop_state,idleV);
}
void CcuMem::outCTRL_cfg_fsm_run_stop_n( Ccu_reg value )
{
	out_assign_field(CTRL,cfg_fsm_run_stop_n,value);
}
void CcuMem::pulseCTRL_cfg_fsm_run_stop_n( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,cfg_fsm_run_stop_n,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,cfg_fsm_run_stop_n,idleV);
}
void CcuMem::outCTRL_run_single( Ccu_reg value )
{
	out_assign_field(CTRL,run_single,value);
}
void CcuMem::pulseCTRL_run_single( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,run_single,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,run_single,idleV);
}
void CcuMem::outCTRL_mode_play_last( Ccu_reg value )
{
	out_assign_field(CTRL,mode_play_last,value);
}
void CcuMem::pulseCTRL_mode_play_last( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,mode_play_last,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,mode_play_last,idleV);
}
void CcuMem::outCTRL_scu_fsm_run( Ccu_reg value )
{
	out_assign_field(CTRL,scu_fsm_run,value);
}
void CcuMem::pulseCTRL_scu_fsm_run( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,scu_fsm_run,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,scu_fsm_run,idleV);
}
void CcuMem::outCTRL_force_trig( Ccu_reg value )
{
	out_assign_field(CTRL,force_trig,value);
}
void CcuMem::pulseCTRL_force_trig( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,force_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,force_trig,idleV);
}
void CcuMem::outCTRL_wpu_plot_display_finish( Ccu_reg value )
{
	out_assign_field(CTRL,wpu_plot_display_finish,value);
}
void CcuMem::pulseCTRL_wpu_plot_display_finish( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,wpu_plot_display_finish,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,wpu_plot_display_finish,idleV);
}
void CcuMem::outCTRL_soft_reset_gt( Ccu_reg value )
{
	out_assign_field(CTRL,soft_reset_gt,value);
}
void CcuMem::pulseCTRL_soft_reset_gt( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,soft_reset_gt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,soft_reset_gt,idleV);
}
void CcuMem::outCTRL_ms_sync( Ccu_reg value )
{
	out_assign_field(CTRL,ms_sync,value);
}
void CcuMem::pulseCTRL_ms_sync( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,ms_sync,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,ms_sync,idleV);
}
void CcuMem::outCTRL_fsm_reset( Ccu_reg value )
{
	out_assign_field(CTRL,fsm_reset,value);
}
void CcuMem::pulseCTRL_fsm_reset( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(CTRL,fsm_reset,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(CTRL,fsm_reset,idleV);
}

void CcuMem::outCTRL( Ccu_reg value )
{
	out_assign_field(CTRL,payload,value);
}

void CcuMem::outCTRL()
{
	out_member(CTRL);
}


Ccu_reg CcuMem::inCTRL()
{
	reg_in(CTRL);
	return CTRL.payload;
}


void CcuMem::setMODE_cfg_mode_scan( Ccu_reg value )
{
	reg_assign_field(MODE,cfg_mode_scan,value);
}
void CcuMem::setMODE_sys_mode_scan_trace( Ccu_reg value )
{
	reg_assign_field(MODE,sys_mode_scan_trace,value);
}
void CcuMem::setMODE_sys_mode_scan_zoom( Ccu_reg value )
{
	reg_assign_field(MODE,sys_mode_scan_zoom,value);
}
void CcuMem::setMODE_mode_scan_en( Ccu_reg value )
{
	reg_assign_field(MODE,mode_scan_en,value);
}
void CcuMem::setMODE_search_en_zoom( Ccu_reg value )
{
	reg_assign_field(MODE,search_en_zoom,value);
}
void CcuMem::setMODE_search_en_ch( Ccu_reg value )
{
	reg_assign_field(MODE,search_en_ch,value);
}
void CcuMem::setMODE_search_en_la( Ccu_reg value )
{
	reg_assign_field(MODE,search_en_la,value);
}
void CcuMem::setMODE_mode_import_type( Ccu_reg value )
{
	reg_assign_field(MODE,mode_import_type,value);
}
void CcuMem::setMODE_mode_export( Ccu_reg value )
{
	reg_assign_field(MODE,mode_export,value);
}
void CcuMem::setMODE_mode_import_play( Ccu_reg value )
{
	reg_assign_field(MODE,mode_import_play,value);
}
void CcuMem::setMODE_mode_import_rec( Ccu_reg value )
{
	reg_assign_field(MODE,mode_import_rec,value);
}
void CcuMem::setMODE_measure_en_zoom( Ccu_reg value )
{
	reg_assign_field(MODE,measure_en_zoom,value);
}
void CcuMem::setMODE_measure_en_ch( Ccu_reg value )
{
	reg_assign_field(MODE,measure_en_ch,value);
}
void CcuMem::setMODE_measure_en_la( Ccu_reg value )
{
	reg_assign_field(MODE,measure_en_la,value);
}
void CcuMem::setMODE_wave_ch_en( Ccu_reg value )
{
	reg_assign_field(MODE,wave_ch_en,value);
}
void CcuMem::setMODE_wave_la_en( Ccu_reg value )
{
	reg_assign_field(MODE,wave_la_en,value);
}
void CcuMem::setMODE_zoom_en( Ccu_reg value )
{
	reg_assign_field(MODE,zoom_en,value);
}
void CcuMem::setMODE_mask_err_stop_en( Ccu_reg value )
{
	reg_assign_field(MODE,mask_err_stop_en,value);
}
void CcuMem::setMODE_mask_save_fail( Ccu_reg value )
{
	reg_assign_field(MODE,mask_save_fail,value);
}
void CcuMem::setMODE_mask_save_pass( Ccu_reg value )
{
	reg_assign_field(MODE,mask_save_pass,value);
}
void CcuMem::setMODE_mask_pf_en( Ccu_reg value )
{
	reg_assign_field(MODE,mask_pf_en,value);
}
void CcuMem::setMODE_zone_trig_en( Ccu_reg value )
{
	reg_assign_field(MODE,zone_trig_en,value);
}
void CcuMem::setMODE_mode_roll_en( Ccu_reg value )
{
	reg_assign_field(MODE,mode_roll_en,value);
}
void CcuMem::setMODE_mode_fast_ref( Ccu_reg value )
{
	reg_assign_field(MODE,mode_fast_ref,value);
}
void CcuMem::setMODE_auto_trig( Ccu_reg value )
{
	reg_assign_field(MODE,auto_trig,value);
}
void CcuMem::setMODE_interleave_20G( Ccu_reg value )
{
	reg_assign_field(MODE,interleave_20G,value);
}
void CcuMem::setMODE_en_20G( Ccu_reg value )
{
	reg_assign_field(MODE,en_20G,value);
}

void CcuMem::setMODE( Ccu_reg value )
{
	reg_assign_field(MODE,payload,value);
}

void CcuMem::outMODE_cfg_mode_scan( Ccu_reg value )
{
	out_assign_field(MODE,cfg_mode_scan,value);
}
void CcuMem::pulseMODE_cfg_mode_scan( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,cfg_mode_scan,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,cfg_mode_scan,idleV);
}
void CcuMem::outMODE_sys_mode_scan_trace( Ccu_reg value )
{
	out_assign_field(MODE,sys_mode_scan_trace,value);
}
void CcuMem::pulseMODE_sys_mode_scan_trace( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,sys_mode_scan_trace,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,sys_mode_scan_trace,idleV);
}
void CcuMem::outMODE_sys_mode_scan_zoom( Ccu_reg value )
{
	out_assign_field(MODE,sys_mode_scan_zoom,value);
}
void CcuMem::pulseMODE_sys_mode_scan_zoom( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,sys_mode_scan_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,sys_mode_scan_zoom,idleV);
}
void CcuMem::outMODE_mode_scan_en( Ccu_reg value )
{
	out_assign_field(MODE,mode_scan_en,value);
}
void CcuMem::pulseMODE_mode_scan_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_scan_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_scan_en,idleV);
}
void CcuMem::outMODE_search_en_zoom( Ccu_reg value )
{
	out_assign_field(MODE,search_en_zoom,value);
}
void CcuMem::pulseMODE_search_en_zoom( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,search_en_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,search_en_zoom,idleV);
}
void CcuMem::outMODE_search_en_ch( Ccu_reg value )
{
	out_assign_field(MODE,search_en_ch,value);
}
void CcuMem::pulseMODE_search_en_ch( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,search_en_ch,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,search_en_ch,idleV);
}
void CcuMem::outMODE_search_en_la( Ccu_reg value )
{
	out_assign_field(MODE,search_en_la,value);
}
void CcuMem::pulseMODE_search_en_la( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,search_en_la,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,search_en_la,idleV);
}
void CcuMem::outMODE_mode_import_type( Ccu_reg value )
{
	out_assign_field(MODE,mode_import_type,value);
}
void CcuMem::pulseMODE_mode_import_type( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_import_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_import_type,idleV);
}
void CcuMem::outMODE_mode_export( Ccu_reg value )
{
	out_assign_field(MODE,mode_export,value);
}
void CcuMem::pulseMODE_mode_export( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_export,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_export,idleV);
}
void CcuMem::outMODE_mode_import_play( Ccu_reg value )
{
	out_assign_field(MODE,mode_import_play,value);
}
void CcuMem::pulseMODE_mode_import_play( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_import_play,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_import_play,idleV);
}
void CcuMem::outMODE_mode_import_rec( Ccu_reg value )
{
	out_assign_field(MODE,mode_import_rec,value);
}
void CcuMem::pulseMODE_mode_import_rec( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_import_rec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_import_rec,idleV);
}
void CcuMem::outMODE_measure_en_zoom( Ccu_reg value )
{
	out_assign_field(MODE,measure_en_zoom,value);
}
void CcuMem::pulseMODE_measure_en_zoom( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,measure_en_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,measure_en_zoom,idleV);
}
void CcuMem::outMODE_measure_en_ch( Ccu_reg value )
{
	out_assign_field(MODE,measure_en_ch,value);
}
void CcuMem::pulseMODE_measure_en_ch( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,measure_en_ch,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,measure_en_ch,idleV);
}
void CcuMem::outMODE_measure_en_la( Ccu_reg value )
{
	out_assign_field(MODE,measure_en_la,value);
}
void CcuMem::pulseMODE_measure_en_la( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,measure_en_la,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,measure_en_la,idleV);
}
void CcuMem::outMODE_wave_ch_en( Ccu_reg value )
{
	out_assign_field(MODE,wave_ch_en,value);
}
void CcuMem::pulseMODE_wave_ch_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,wave_ch_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,wave_ch_en,idleV);
}
void CcuMem::outMODE_wave_la_en( Ccu_reg value )
{
	out_assign_field(MODE,wave_la_en,value);
}
void CcuMem::pulseMODE_wave_la_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,wave_la_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,wave_la_en,idleV);
}
void CcuMem::outMODE_zoom_en( Ccu_reg value )
{
	out_assign_field(MODE,zoom_en,value);
}
void CcuMem::pulseMODE_zoom_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,zoom_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,zoom_en,idleV);
}
void CcuMem::outMODE_mask_err_stop_en( Ccu_reg value )
{
	out_assign_field(MODE,mask_err_stop_en,value);
}
void CcuMem::pulseMODE_mask_err_stop_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_err_stop_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_err_stop_en,idleV);
}
void CcuMem::outMODE_mask_save_fail( Ccu_reg value )
{
	out_assign_field(MODE,mask_save_fail,value);
}
void CcuMem::pulseMODE_mask_save_fail( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_save_fail,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_save_fail,idleV);
}
void CcuMem::outMODE_mask_save_pass( Ccu_reg value )
{
	out_assign_field(MODE,mask_save_pass,value);
}
void CcuMem::pulseMODE_mask_save_pass( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_save_pass,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_save_pass,idleV);
}
void CcuMem::outMODE_mask_pf_en( Ccu_reg value )
{
	out_assign_field(MODE,mask_pf_en,value);
}
void CcuMem::pulseMODE_mask_pf_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mask_pf_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mask_pf_en,idleV);
}
void CcuMem::outMODE_zone_trig_en( Ccu_reg value )
{
	out_assign_field(MODE,zone_trig_en,value);
}
void CcuMem::pulseMODE_zone_trig_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,zone_trig_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,zone_trig_en,idleV);
}
void CcuMem::outMODE_mode_roll_en( Ccu_reg value )
{
	out_assign_field(MODE,mode_roll_en,value);
}
void CcuMem::pulseMODE_mode_roll_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_roll_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_roll_en,idleV);
}
void CcuMem::outMODE_mode_fast_ref( Ccu_reg value )
{
	out_assign_field(MODE,mode_fast_ref,value);
}
void CcuMem::pulseMODE_mode_fast_ref( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,mode_fast_ref,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,mode_fast_ref,idleV);
}
void CcuMem::outMODE_auto_trig( Ccu_reg value )
{
	out_assign_field(MODE,auto_trig,value);
}
void CcuMem::pulseMODE_auto_trig( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,auto_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,auto_trig,idleV);
}
void CcuMem::outMODE_interleave_20G( Ccu_reg value )
{
	out_assign_field(MODE,interleave_20G,value);
}
void CcuMem::pulseMODE_interleave_20G( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,interleave_20G,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,interleave_20G,idleV);
}
void CcuMem::outMODE_en_20G( Ccu_reg value )
{
	out_assign_field(MODE,en_20G,value);
}
void CcuMem::pulseMODE_en_20G( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE,en_20G,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE,en_20G,idleV);
}

void CcuMem::outMODE( Ccu_reg value )
{
	out_assign_field(MODE,payload,value);
}

void CcuMem::outMODE()
{
	out_member(MODE);
}


Ccu_reg CcuMem::inMODE()
{
	reg_in(MODE);
	return MODE.payload;
}


void CcuMem::setMODE_10G_chn_10G( Ccu_reg value )
{
	reg_assign_field(MODE_10G,chn_10G,value);
}
void CcuMem::setMODE_10G_cfg_chn( Ccu_reg value )
{
	reg_assign_field(MODE_10G,cfg_chn,value);
}

void CcuMem::setMODE_10G( Ccu_reg value )
{
	reg_assign_field(MODE_10G,payload,value);
}

void CcuMem::outMODE_10G_chn_10G( Ccu_reg value )
{
	out_assign_field(MODE_10G,chn_10G,value);
}
void CcuMem::pulseMODE_10G_chn_10G( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE_10G,chn_10G,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE_10G,chn_10G,idleV);
}
void CcuMem::outMODE_10G_cfg_chn( Ccu_reg value )
{
	out_assign_field(MODE_10G,cfg_chn,value);
}
void CcuMem::pulseMODE_10G_cfg_chn( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MODE_10G,cfg_chn,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MODE_10G,cfg_chn,idleV);
}

void CcuMem::outMODE_10G( Ccu_reg value )
{
	out_assign_field(MODE_10G,payload,value);
}

void CcuMem::outMODE_10G()
{
	out_member(MODE_10G);
}


Ccu_reg CcuMem::inMODE_10G()
{
	reg_in(MODE_10G);
	return MODE_10G.payload;
}


void CcuMem::setPRE_SA( Ccu_reg value )
{
	reg_assign(PRE_SA,value);
}

void CcuMem::outPRE_SA( Ccu_reg value )
{
	out_assign(PRE_SA,value);
}

void CcuMem::outPRE_SA()
{
	out_member(PRE_SA);
}


Ccu_reg CcuMem::inPRE_SA()
{
	reg_in(PRE_SA);
	return PRE_SA;
}


void CcuMem::setPOS_SA( Ccu_reg value )
{
	reg_assign(POS_SA,value);
}

void CcuMem::outPOS_SA( Ccu_reg value )
{
	out_assign(POS_SA,value);
}

void CcuMem::outPOS_SA()
{
	out_member(POS_SA);
}


Ccu_reg CcuMem::inPOS_SA()
{
	reg_in(POS_SA);
	return POS_SA;
}


void CcuMem::setSCAN_TRIG_POSITION_position( Ccu_reg value )
{
	reg_assign_field(SCAN_TRIG_POSITION,position,value);
}
void CcuMem::setSCAN_TRIG_POSITION_trig_left_side( Ccu_reg value )
{
	reg_assign_field(SCAN_TRIG_POSITION,trig_left_side,value);
}

void CcuMem::setSCAN_TRIG_POSITION( Ccu_reg value )
{
	reg_assign_field(SCAN_TRIG_POSITION,payload,value);
}

void CcuMem::outSCAN_TRIG_POSITION_position( Ccu_reg value )
{
	out_assign_field(SCAN_TRIG_POSITION,position,value);
}
void CcuMem::pulseSCAN_TRIG_POSITION_position( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCAN_TRIG_POSITION,position,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCAN_TRIG_POSITION,position,idleV);
}
void CcuMem::outSCAN_TRIG_POSITION_trig_left_side( Ccu_reg value )
{
	out_assign_field(SCAN_TRIG_POSITION,trig_left_side,value);
}
void CcuMem::pulseSCAN_TRIG_POSITION_trig_left_side( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(SCAN_TRIG_POSITION,trig_left_side,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(SCAN_TRIG_POSITION,trig_left_side,idleV);
}

void CcuMem::outSCAN_TRIG_POSITION( Ccu_reg value )
{
	out_assign_field(SCAN_TRIG_POSITION,payload,value);
}

void CcuMem::outSCAN_TRIG_POSITION()
{
	out_member(SCAN_TRIG_POSITION);
}


Ccu_reg CcuMem::inSCAN_TRIG_POSITION()
{
	reg_in(SCAN_TRIG_POSITION);
	return SCAN_TRIG_POSITION.payload;
}


void CcuMem::setAUTO_TRIG_TIMEOUT( Ccu_reg value )
{
	reg_assign(AUTO_TRIG_TIMEOUT,value);
}

void CcuMem::outAUTO_TRIG_TIMEOUT( Ccu_reg value )
{
	out_assign(AUTO_TRIG_TIMEOUT,value);
}

void CcuMem::outAUTO_TRIG_TIMEOUT()
{
	out_member(AUTO_TRIG_TIMEOUT);
}


Ccu_reg CcuMem::inAUTO_TRIG_TIMEOUT()
{
	reg_in(AUTO_TRIG_TIMEOUT);
	return AUTO_TRIG_TIMEOUT;
}


void CcuMem::setAUTO_TRIG_HOLD_OFF( Ccu_reg value )
{
	reg_assign(AUTO_TRIG_HOLD_OFF,value);
}

void CcuMem::outAUTO_TRIG_HOLD_OFF( Ccu_reg value )
{
	out_assign(AUTO_TRIG_HOLD_OFF,value);
}

void CcuMem::outAUTO_TRIG_HOLD_OFF()
{
	out_member(AUTO_TRIG_HOLD_OFF);
}


Ccu_reg CcuMem::inAUTO_TRIG_HOLD_OFF()
{
	reg_in(AUTO_TRIG_HOLD_OFF);
	return AUTO_TRIG_HOLD_OFF;
}


void CcuMem::setFSM_DELAY( Ccu_reg value )
{
	reg_assign(FSM_DELAY,value);
}

void CcuMem::outFSM_DELAY( Ccu_reg value )
{
	out_assign(FSM_DELAY,value);
}

void CcuMem::outFSM_DELAY()
{
	out_member(FSM_DELAY);
}


Ccu_reg CcuMem::inFSM_DELAY()
{
	reg_in(FSM_DELAY);
	return FSM_DELAY;
}


void CcuMem::setPLOT_DELAY( Ccu_reg value )
{
	reg_assign(PLOT_DELAY,value);
}

void CcuMem::outPLOT_DELAY( Ccu_reg value )
{
	out_assign(PLOT_DELAY,value);
}

void CcuMem::outPLOT_DELAY()
{
	out_member(PLOT_DELAY);
}


Ccu_reg CcuMem::inPLOT_DELAY()
{
	reg_in(PLOT_DELAY);
	return PLOT_DELAY;
}


void CcuMem::setFRAME_PLOT_NUM( Ccu_reg value )
{
	reg_assign(FRAME_PLOT_NUM,value);
}

void CcuMem::outFRAME_PLOT_NUM( Ccu_reg value )
{
	out_assign(FRAME_PLOT_NUM,value);
}

void CcuMem::outFRAME_PLOT_NUM()
{
	out_member(FRAME_PLOT_NUM);
}


Ccu_reg CcuMem::inFRAME_PLOT_NUM()
{
	reg_in(FRAME_PLOT_NUM);
	return FRAME_PLOT_NUM;
}


void CcuMem::setREC_PLAY_index_full( Ccu_reg value )
{
	reg_assign_field(REC_PLAY,index_full,value);
}
void CcuMem::setREC_PLAY_loop_playback( Ccu_reg value )
{
	reg_assign_field(REC_PLAY,loop_playback,value);
}
void CcuMem::setREC_PLAY_index_load( Ccu_reg value )
{
	reg_assign_field(REC_PLAY,index_load,value);
}
void CcuMem::setREC_PLAY_index_inc( Ccu_reg value )
{
	reg_assign_field(REC_PLAY,index_inc,value);
}
void CcuMem::setREC_PLAY_mode_play( Ccu_reg value )
{
	reg_assign_field(REC_PLAY,mode_play,value);
}
void CcuMem::setREC_PLAY_mode_rec( Ccu_reg value )
{
	reg_assign_field(REC_PLAY,mode_rec,value);
}

void CcuMem::setREC_PLAY( Ccu_reg value )
{
	reg_assign_field(REC_PLAY,payload,value);
}

void CcuMem::outREC_PLAY_index_full( Ccu_reg value )
{
	out_assign_field(REC_PLAY,index_full,value);
}
void CcuMem::pulseREC_PLAY_index_full( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,index_full,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,index_full,idleV);
}
void CcuMem::outREC_PLAY_loop_playback( Ccu_reg value )
{
	out_assign_field(REC_PLAY,loop_playback,value);
}
void CcuMem::pulseREC_PLAY_loop_playback( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,loop_playback,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,loop_playback,idleV);
}
void CcuMem::outREC_PLAY_index_load( Ccu_reg value )
{
	out_assign_field(REC_PLAY,index_load,value);
}
void CcuMem::pulseREC_PLAY_index_load( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,index_load,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,index_load,idleV);
}
void CcuMem::outREC_PLAY_index_inc( Ccu_reg value )
{
	out_assign_field(REC_PLAY,index_inc,value);
}
void CcuMem::pulseREC_PLAY_index_inc( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,index_inc,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,index_inc,idleV);
}
void CcuMem::outREC_PLAY_mode_play( Ccu_reg value )
{
	out_assign_field(REC_PLAY,mode_play,value);
}
void CcuMem::pulseREC_PLAY_mode_play( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,mode_play,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,mode_play,idleV);
}
void CcuMem::outREC_PLAY_mode_rec( Ccu_reg value )
{
	out_assign_field(REC_PLAY,mode_rec,value);
}
void CcuMem::pulseREC_PLAY_mode_rec( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(REC_PLAY,mode_rec,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(REC_PLAY,mode_rec,idleV);
}

void CcuMem::outREC_PLAY( Ccu_reg value )
{
	out_assign_field(REC_PLAY,payload,value);
}

void CcuMem::outREC_PLAY()
{
	out_member(REC_PLAY);
}


Ccu_reg CcuMem::inREC_PLAY()
{
	reg_in(REC_PLAY);
	return REC_PLAY.payload;
}


void CcuMem::setWAVE_INFO_wave_plot_frame_cnt( Ccu_reg value )
{
	reg_assign_field(WAVE_INFO,wave_plot_frame_cnt,value);
}
void CcuMem::setWAVE_INFO_wave_play_vld( Ccu_reg value )
{
	reg_assign_field(WAVE_INFO,wave_play_vld,value);
}

void CcuMem::setWAVE_INFO( Ccu_reg value )
{
	reg_assign_field(WAVE_INFO,payload,value);
}

void CcuMem::outWAVE_INFO_wave_plot_frame_cnt( Ccu_reg value )
{
	out_assign_field(WAVE_INFO,wave_plot_frame_cnt,value);
}
void CcuMem::pulseWAVE_INFO_wave_plot_frame_cnt( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(WAVE_INFO,wave_plot_frame_cnt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(WAVE_INFO,wave_plot_frame_cnt,idleV);
}
void CcuMem::outWAVE_INFO_wave_play_vld( Ccu_reg value )
{
	out_assign_field(WAVE_INFO,wave_play_vld,value);
}
void CcuMem::pulseWAVE_INFO_wave_play_vld( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(WAVE_INFO,wave_play_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(WAVE_INFO,wave_play_vld,idleV);
}

void CcuMem::outWAVE_INFO( Ccu_reg value )
{
	out_assign_field(WAVE_INFO,payload,value);
}

void CcuMem::outWAVE_INFO()
{
	out_member(WAVE_INFO);
}


Ccu_reg CcuMem::inWAVE_INFO()
{
	reg_in(WAVE_INFO);
	return WAVE_INFO.payload;
}


void CcuMem::setINDEX_CUR_index( Ccu_reg value )
{
	reg_assign_field(INDEX_CUR,index,value);
}

void CcuMem::setINDEX_CUR( Ccu_reg value )
{
	reg_assign_field(INDEX_CUR,payload,value);
}

void CcuMem::outINDEX_CUR_index( Ccu_reg value )
{
	out_assign_field(INDEX_CUR,index,value);
}
void CcuMem::pulseINDEX_CUR_index( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(INDEX_CUR,index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INDEX_CUR,index,idleV);
}

void CcuMem::outINDEX_CUR( Ccu_reg value )
{
	out_assign_field(INDEX_CUR,payload,value);
}

void CcuMem::outINDEX_CUR()
{
	out_member(INDEX_CUR);
}


Ccu_reg CcuMem::inINDEX_CUR()
{
	reg_in(INDEX_CUR);
	return INDEX_CUR.payload;
}


void CcuMem::setINDEX_BASE_index( Ccu_reg value )
{
	reg_assign_field(INDEX_BASE,index,value);
}

void CcuMem::setINDEX_BASE( Ccu_reg value )
{
	reg_assign_field(INDEX_BASE,payload,value);
}

void CcuMem::outINDEX_BASE_index( Ccu_reg value )
{
	out_assign_field(INDEX_BASE,index,value);
}
void CcuMem::pulseINDEX_BASE_index( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(INDEX_BASE,index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INDEX_BASE,index,idleV);
}

void CcuMem::outINDEX_BASE( Ccu_reg value )
{
	out_assign_field(INDEX_BASE,payload,value);
}

void CcuMem::outINDEX_BASE()
{
	out_member(INDEX_BASE);
}


Ccu_reg CcuMem::inINDEX_BASE()
{
	reg_in(INDEX_BASE);
	return INDEX_BASE.payload;
}


void CcuMem::setINDEX_UPPER_index( Ccu_reg value )
{
	reg_assign_field(INDEX_UPPER,index,value);
}

void CcuMem::setINDEX_UPPER( Ccu_reg value )
{
	reg_assign_field(INDEX_UPPER,payload,value);
}

void CcuMem::outINDEX_UPPER_index( Ccu_reg value )
{
	out_assign_field(INDEX_UPPER,index,value);
}
void CcuMem::pulseINDEX_UPPER_index( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(INDEX_UPPER,index,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(INDEX_UPPER,index,idleV);
}

void CcuMem::outINDEX_UPPER( Ccu_reg value )
{
	out_assign_field(INDEX_UPPER,payload,value);
}

void CcuMem::outINDEX_UPPER()
{
	out_member(INDEX_UPPER);
}


Ccu_reg CcuMem::inINDEX_UPPER()
{
	reg_in(INDEX_UPPER);
	return INDEX_UPPER.payload;
}


void CcuMem::setTRACE_trace_once_req( Ccu_reg value )
{
	reg_assign_field(TRACE,trace_once_req,value);
}
void CcuMem::setTRACE_measure_once_req( Ccu_reg value )
{
	reg_assign_field(TRACE,measure_once_req,value);
}
void CcuMem::setTRACE_search_once_req( Ccu_reg value )
{
	reg_assign_field(TRACE,search_once_req,value);
}
void CcuMem::setTRACE_eye_once_req( Ccu_reg value )
{
	reg_assign_field(TRACE,eye_once_req,value);
}
void CcuMem::setTRACE_wave_bypass_wpu( Ccu_reg value )
{
	reg_assign_field(TRACE,wave_bypass_wpu,value);
}
void CcuMem::setTRACE_wave_bypass_trace( Ccu_reg value )
{
	reg_assign_field(TRACE,wave_bypass_trace,value);
}
void CcuMem::setTRACE_avg_clr( Ccu_reg value )
{
	reg_assign_field(TRACE,avg_clr,value);
}

void CcuMem::setTRACE( Ccu_reg value )
{
	reg_assign_field(TRACE,payload,value);
}

void CcuMem::outTRACE_trace_once_req( Ccu_reg value )
{
	out_assign_field(TRACE,trace_once_req,value);
}
void CcuMem::pulseTRACE_trace_once_req( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(TRACE,trace_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,trace_once_req,idleV);
}
void CcuMem::outTRACE_measure_once_req( Ccu_reg value )
{
	out_assign_field(TRACE,measure_once_req,value);
}
void CcuMem::pulseTRACE_measure_once_req( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(TRACE,measure_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,measure_once_req,idleV);
}
void CcuMem::outTRACE_search_once_req( Ccu_reg value )
{
	out_assign_field(TRACE,search_once_req,value);
}
void CcuMem::pulseTRACE_search_once_req( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(TRACE,search_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,search_once_req,idleV);
}
void CcuMem::outTRACE_eye_once_req( Ccu_reg value )
{
	out_assign_field(TRACE,eye_once_req,value);
}
void CcuMem::pulseTRACE_eye_once_req( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(TRACE,eye_once_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,eye_once_req,idleV);
}
void CcuMem::outTRACE_wave_bypass_wpu( Ccu_reg value )
{
	out_assign_field(TRACE,wave_bypass_wpu,value);
}
void CcuMem::pulseTRACE_wave_bypass_wpu( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(TRACE,wave_bypass_wpu,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,wave_bypass_wpu,idleV);
}
void CcuMem::outTRACE_wave_bypass_trace( Ccu_reg value )
{
	out_assign_field(TRACE,wave_bypass_trace,value);
}
void CcuMem::pulseTRACE_wave_bypass_trace( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(TRACE,wave_bypass_trace,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,wave_bypass_trace,idleV);
}
void CcuMem::outTRACE_avg_clr( Ccu_reg value )
{
	out_assign_field(TRACE,avg_clr,value);
}
void CcuMem::pulseTRACE_avg_clr( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(TRACE,avg_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(TRACE,avg_clr,idleV);
}

void CcuMem::outTRACE( Ccu_reg value )
{
	out_assign_field(TRACE,payload,value);
}

void CcuMem::outTRACE()
{
	out_member(TRACE);
}


Ccu_reg CcuMem::inTRACE()
{
	reg_in(TRACE);
	return TRACE.payload;
}


void CcuMem::setTRIG_DLY( Ccu_reg value )
{
	reg_assign(TRIG_DLY,value);
}

void CcuMem::outTRIG_DLY( Ccu_reg value )
{
	out_assign(TRIG_DLY,value);
}

void CcuMem::outTRIG_DLY()
{
	out_member(TRIG_DLY);
}


Ccu_reg CcuMem::inTRIG_DLY()
{
	reg_in(TRIG_DLY);
	return TRIG_DLY;
}


void CcuMem::setPOS_SA_LAST_VIEW( Ccu_reg value )
{
	reg_assign(POS_SA_LAST_VIEW,value);
}

void CcuMem::outPOS_SA_LAST_VIEW( Ccu_reg value )
{
	out_assign(POS_SA_LAST_VIEW,value);
}

void CcuMem::outPOS_SA_LAST_VIEW()
{
	out_member(POS_SA_LAST_VIEW);
}


Ccu_reg CcuMem::inPOS_SA_LAST_VIEW()
{
	reg_in(POS_SA_LAST_VIEW);
	return POS_SA_LAST_VIEW;
}


void CcuMem::setMASK_FRM_NUM_L( Ccu_reg value )
{
	reg_assign(MASK_FRM_NUM_L,value);
}

void CcuMem::outMASK_FRM_NUM_L( Ccu_reg value )
{
	out_assign(MASK_FRM_NUM_L,value);
}

void CcuMem::outMASK_FRM_NUM_L()
{
	out_member(MASK_FRM_NUM_L);
}


Ccu_reg CcuMem::inMASK_FRM_NUM_L()
{
	reg_in(MASK_FRM_NUM_L);
	return MASK_FRM_NUM_L;
}


void CcuMem::setMASK_FRM_NUM_H_frm_num_h( Ccu_reg value )
{
	reg_assign_field(MASK_FRM_NUM_H,frm_num_h,value);
}
void CcuMem::setMASK_FRM_NUM_H_frm_num_en( Ccu_reg value )
{
	reg_assign_field(MASK_FRM_NUM_H,frm_num_en,value);
}

void CcuMem::setMASK_FRM_NUM_H( Ccu_reg value )
{
	reg_assign_field(MASK_FRM_NUM_H,payload,value);
}

void CcuMem::outMASK_FRM_NUM_H_frm_num_h( Ccu_reg value )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_h,value);
}
void CcuMem::pulseMASK_FRM_NUM_H_frm_num_h( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_h,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_FRM_NUM_H,frm_num_h,idleV);
}
void CcuMem::outMASK_FRM_NUM_H_frm_num_en( Ccu_reg value )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_en,value);
}
void CcuMem::pulseMASK_FRM_NUM_H_frm_num_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MASK_FRM_NUM_H,frm_num_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_FRM_NUM_H,frm_num_en,idleV);
}

void CcuMem::outMASK_FRM_NUM_H( Ccu_reg value )
{
	out_assign_field(MASK_FRM_NUM_H,payload,value);
}

void CcuMem::outMASK_FRM_NUM_H()
{
	out_member(MASK_FRM_NUM_H);
}


Ccu_reg CcuMem::inMASK_FRM_NUM_H()
{
	reg_in(MASK_FRM_NUM_H);
	return MASK_FRM_NUM_H.payload;
}


void CcuMem::setMASK_TIMEOUT_timeout( Ccu_reg value )
{
	reg_assign_field(MASK_TIMEOUT,timeout,value);
}
void CcuMem::setMASK_TIMEOUT_timeout_en( Ccu_reg value )
{
	reg_assign_field(MASK_TIMEOUT,timeout_en,value);
}

void CcuMem::setMASK_TIMEOUT( Ccu_reg value )
{
	reg_assign_field(MASK_TIMEOUT,payload,value);
}

void CcuMem::outMASK_TIMEOUT_timeout( Ccu_reg value )
{
	out_assign_field(MASK_TIMEOUT,timeout,value);
}
void CcuMem::pulseMASK_TIMEOUT_timeout( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MASK_TIMEOUT,timeout,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TIMEOUT,timeout,idleV);
}
void CcuMem::outMASK_TIMEOUT_timeout_en( Ccu_reg value )
{
	out_assign_field(MASK_TIMEOUT,timeout_en,value);
}
void CcuMem::pulseMASK_TIMEOUT_timeout_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(MASK_TIMEOUT,timeout_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(MASK_TIMEOUT,timeout_en,idleV);
}

void CcuMem::outMASK_TIMEOUT( Ccu_reg value )
{
	out_assign_field(MASK_TIMEOUT,payload,value);
}

void CcuMem::outMASK_TIMEOUT()
{
	out_member(MASK_TIMEOUT);
}


Ccu_reg CcuMem::inMASK_TIMEOUT()
{
	reg_in(MASK_TIMEOUT);
	return MASK_TIMEOUT.payload;
}


void CcuMem::setSTATE_DISPLAY_scu_fsm( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,scu_fsm,value);
}
void CcuMem::setSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,sys_pre_sa_done,value);
}
void CcuMem::setSTATE_DISPLAY_sys_stop_state( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,sys_stop_state,value);
}
void CcuMem::setSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,value);
}
void CcuMem::setSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,disp_sys_trig_d,value);
}
void CcuMem::setSTATE_DISPLAY_rec_play_stop_state( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,rec_play_stop_state,value);
}
void CcuMem::setSTATE_DISPLAY_mask_stop_state( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,mask_stop_state,value);
}
void CcuMem::setSTATE_DISPLAY_stop_stata_clr( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,stop_stata_clr,value);
}
void CcuMem::setSTATE_DISPLAY_disp_trig_clr( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,disp_trig_clr,value);
}

void CcuMem::setSTATE_DISPLAY( Ccu_reg value )
{
	reg_assign_field(STATE_DISPLAY,payload,value);
}

void CcuMem::outSTATE_DISPLAY_scu_fsm( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,scu_fsm,value);
}
void CcuMem::pulseSTATE_DISPLAY_scu_fsm( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,scu_fsm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,scu_fsm,idleV);
}
void CcuMem::outSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,sys_pre_sa_done,value);
}
void CcuMem::pulseSTATE_DISPLAY_sys_pre_sa_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,sys_pre_sa_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,sys_pre_sa_done,idleV);
}
void CcuMem::outSTATE_DISPLAY_sys_stop_state( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,sys_stop_state,value);
}
void CcuMem::pulseSTATE_DISPLAY_sys_stop_state( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,sys_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,sys_stop_state,idleV);
}
void CcuMem::outSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,value);
}
void CcuMem::pulseSTATE_DISPLAY_disp_tpu_scu_trig( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,disp_tpu_scu_trig,idleV);
}
void CcuMem::outSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,disp_sys_trig_d,value);
}
void CcuMem::pulseSTATE_DISPLAY_disp_sys_trig_d( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,disp_sys_trig_d,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,disp_sys_trig_d,idleV);
}
void CcuMem::outSTATE_DISPLAY_rec_play_stop_state( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,rec_play_stop_state,value);
}
void CcuMem::pulseSTATE_DISPLAY_rec_play_stop_state( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,rec_play_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,rec_play_stop_state,idleV);
}
void CcuMem::outSTATE_DISPLAY_mask_stop_state( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,mask_stop_state,value);
}
void CcuMem::pulseSTATE_DISPLAY_mask_stop_state( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,mask_stop_state,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,mask_stop_state,idleV);
}
void CcuMem::outSTATE_DISPLAY_stop_stata_clr( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,stop_stata_clr,value);
}
void CcuMem::pulseSTATE_DISPLAY_stop_stata_clr( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,stop_stata_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,stop_stata_clr,idleV);
}
void CcuMem::outSTATE_DISPLAY_disp_trig_clr( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,disp_trig_clr,value);
}
void CcuMem::pulseSTATE_DISPLAY_disp_trig_clr( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(STATE_DISPLAY,disp_trig_clr,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(STATE_DISPLAY,disp_trig_clr,idleV);
}

void CcuMem::outSTATE_DISPLAY( Ccu_reg value )
{
	out_assign_field(STATE_DISPLAY,payload,value);
}

void CcuMem::outSTATE_DISPLAY()
{
	out_member(STATE_DISPLAY);
}


Ccu_reg CcuMem::inSTATE_DISPLAY()
{
	reg_in(STATE_DISPLAY);
	return STATE_DISPLAY.payload;
}


Ccu_reg CcuMem::inMASK_TIMER()
{
	reg_in(MASK_TIMER);
	return MASK_TIMER;
}


void CcuMem::setCONFIG_ID( Ccu_reg value )
{
	reg_assign(CONFIG_ID,value);
}

void CcuMem::outCONFIG_ID( Ccu_reg value )
{
	out_assign(CONFIG_ID,value);
}

void CcuMem::outCONFIG_ID()
{
	out_member(CONFIG_ID);
}


Ccu_reg CcuMem::inCONFIG_ID()
{
	reg_in(CONFIG_ID);
	return CONFIG_ID;
}


void CcuMem::setDEUBG_wave_sa_ctrl( Ccu_reg value )
{
	reg_assign_field(DEUBG,wave_sa_ctrl,value);
}
void CcuMem::setDEUBG_sys_auto_trig_act( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_auto_trig_act,value);
}
void CcuMem::setDEUBG_sys_auto_trig_en( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_auto_trig_en,value);
}
void CcuMem::setDEUBG_sys_fine_trig_req( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_fine_trig_req,value);
}
void CcuMem::setDEUBG_sys_meas_only( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_meas_only,value);
}
void CcuMem::setDEUBG_sys_meas_en( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_meas_en,value);
}
void CcuMem::setDEUBG_sys_meas_type( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_meas_type,value);
}
void CcuMem::setDEUBG_spu_tx_roll_null( Ccu_reg value )
{
	reg_assign_field(DEUBG,spu_tx_roll_null,value);
}
void CcuMem::setDEUBG_measure_once_done_hold( Ccu_reg value )
{
	reg_assign_field(DEUBG,measure_once_done_hold,value);
}
void CcuMem::setDEUBG_measure_finish_done_hold( Ccu_reg value )
{
	reg_assign_field(DEUBG,measure_finish_done_hold,value);
}
void CcuMem::setDEUBG_sys_mask_en( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_mask_en,value);
}
void CcuMem::setDEUBG_mask_pf_result_vld_hold( Ccu_reg value )
{
	reg_assign_field(DEUBG,mask_pf_result_vld_hold,value);
}
void CcuMem::setDEUBG_mask_pf_result_cross_hold( Ccu_reg value )
{
	reg_assign_field(DEUBG,mask_pf_result_cross_hold,value);
}
void CcuMem::setDEUBG_mask_pf_timeout( Ccu_reg value )
{
	reg_assign_field(DEUBG,mask_pf_timeout,value);
}
void CcuMem::setDEUBG_mask_frm_overflow( Ccu_reg value )
{
	reg_assign_field(DEUBG,mask_frm_overflow,value);
}
void CcuMem::setDEUBG_measure_once_buf( Ccu_reg value )
{
	reg_assign_field(DEUBG,measure_once_buf,value);
}
void CcuMem::setDEUBG_zone_result_vld_hold( Ccu_reg value )
{
	reg_assign_field(DEUBG,zone_result_vld_hold,value);
}
void CcuMem::setDEUBG_zone_result_trig_hold( Ccu_reg value )
{
	reg_assign_field(DEUBG,zone_result_trig_hold,value);
}
void CcuMem::setDEUBG_wave_proc_la( Ccu_reg value )
{
	reg_assign_field(DEUBG,wave_proc_la,value);
}
void CcuMem::setDEUBG_sys_zoom( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_zoom,value);
}
void CcuMem::setDEUBG_sys_fast_ref( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_fast_ref,value);
}
void CcuMem::setDEUBG_spu_tx_done( Ccu_reg value )
{
	reg_assign_field(DEUBG,spu_tx_done,value);
}
void CcuMem::setDEUBG_sys_avg_en( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_avg_en,value);
}
void CcuMem::setDEUBG_sys_trace( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_trace,value);
}
void CcuMem::setDEUBG_sys_pos_sa_last_frm( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_pos_sa_last_frm,value);
}
void CcuMem::setDEUBG_sys_pos_sa_view( Ccu_reg value )
{
	reg_assign_field(DEUBG,sys_pos_sa_view,value);
}
void CcuMem::setDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg value )
{
	reg_assign_field(DEUBG,spu_wav_sa_rd_tx_done,value);
}
void CcuMem::setDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg value )
{
	reg_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,value);
}
void CcuMem::setDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg value )
{
	reg_assign_field(DEUBG,scan_roll_pos_last_frm_buf,value);
}

void CcuMem::setDEUBG( Ccu_reg value )
{
	reg_assign_field(DEUBG,payload,value);
}

void CcuMem::outDEUBG_wave_sa_ctrl( Ccu_reg value )
{
	out_assign_field(DEUBG,wave_sa_ctrl,value);
}
void CcuMem::pulseDEUBG_wave_sa_ctrl( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,wave_sa_ctrl,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,wave_sa_ctrl,idleV);
}
void CcuMem::outDEUBG_sys_auto_trig_act( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_auto_trig_act,value);
}
void CcuMem::pulseDEUBG_sys_auto_trig_act( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_auto_trig_act,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_auto_trig_act,idleV);
}
void CcuMem::outDEUBG_sys_auto_trig_en( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_auto_trig_en,value);
}
void CcuMem::pulseDEUBG_sys_auto_trig_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_auto_trig_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_auto_trig_en,idleV);
}
void CcuMem::outDEUBG_sys_fine_trig_req( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_fine_trig_req,value);
}
void CcuMem::pulseDEUBG_sys_fine_trig_req( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_fine_trig_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_fine_trig_req,idleV);
}
void CcuMem::outDEUBG_sys_meas_only( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_meas_only,value);
}
void CcuMem::pulseDEUBG_sys_meas_only( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_meas_only,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_meas_only,idleV);
}
void CcuMem::outDEUBG_sys_meas_en( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_meas_en,value);
}
void CcuMem::pulseDEUBG_sys_meas_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_meas_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_meas_en,idleV);
}
void CcuMem::outDEUBG_sys_meas_type( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_meas_type,value);
}
void CcuMem::pulseDEUBG_sys_meas_type( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_meas_type,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_meas_type,idleV);
}
void CcuMem::outDEUBG_spu_tx_roll_null( Ccu_reg value )
{
	out_assign_field(DEUBG,spu_tx_roll_null,value);
}
void CcuMem::pulseDEUBG_spu_tx_roll_null( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,spu_tx_roll_null,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,spu_tx_roll_null,idleV);
}
void CcuMem::outDEUBG_measure_once_done_hold( Ccu_reg value )
{
	out_assign_field(DEUBG,measure_once_done_hold,value);
}
void CcuMem::pulseDEUBG_measure_once_done_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,measure_once_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,measure_once_done_hold,idleV);
}
void CcuMem::outDEUBG_measure_finish_done_hold( Ccu_reg value )
{
	out_assign_field(DEUBG,measure_finish_done_hold,value);
}
void CcuMem::pulseDEUBG_measure_finish_done_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,measure_finish_done_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,measure_finish_done_hold,idleV);
}
void CcuMem::outDEUBG_sys_mask_en( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_mask_en,value);
}
void CcuMem::pulseDEUBG_sys_mask_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_mask_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_mask_en,idleV);
}
void CcuMem::outDEUBG_mask_pf_result_vld_hold( Ccu_reg value )
{
	out_assign_field(DEUBG,mask_pf_result_vld_hold,value);
}
void CcuMem::pulseDEUBG_mask_pf_result_vld_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_pf_result_vld_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_pf_result_vld_hold,idleV);
}
void CcuMem::outDEUBG_mask_pf_result_cross_hold( Ccu_reg value )
{
	out_assign_field(DEUBG,mask_pf_result_cross_hold,value);
}
void CcuMem::pulseDEUBG_mask_pf_result_cross_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_pf_result_cross_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_pf_result_cross_hold,idleV);
}
void CcuMem::outDEUBG_mask_pf_timeout( Ccu_reg value )
{
	out_assign_field(DEUBG,mask_pf_timeout,value);
}
void CcuMem::pulseDEUBG_mask_pf_timeout( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_pf_timeout,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_pf_timeout,idleV);
}
void CcuMem::outDEUBG_mask_frm_overflow( Ccu_reg value )
{
	out_assign_field(DEUBG,mask_frm_overflow,value);
}
void CcuMem::pulseDEUBG_mask_frm_overflow( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,mask_frm_overflow,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,mask_frm_overflow,idleV);
}
void CcuMem::outDEUBG_measure_once_buf( Ccu_reg value )
{
	out_assign_field(DEUBG,measure_once_buf,value);
}
void CcuMem::pulseDEUBG_measure_once_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,measure_once_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,measure_once_buf,idleV);
}
void CcuMem::outDEUBG_zone_result_vld_hold( Ccu_reg value )
{
	out_assign_field(DEUBG,zone_result_vld_hold,value);
}
void CcuMem::pulseDEUBG_zone_result_vld_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,zone_result_vld_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,zone_result_vld_hold,idleV);
}
void CcuMem::outDEUBG_zone_result_trig_hold( Ccu_reg value )
{
	out_assign_field(DEUBG,zone_result_trig_hold,value);
}
void CcuMem::pulseDEUBG_zone_result_trig_hold( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,zone_result_trig_hold,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,zone_result_trig_hold,idleV);
}
void CcuMem::outDEUBG_wave_proc_la( Ccu_reg value )
{
	out_assign_field(DEUBG,wave_proc_la,value);
}
void CcuMem::pulseDEUBG_wave_proc_la( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,wave_proc_la,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,wave_proc_la,idleV);
}
void CcuMem::outDEUBG_sys_zoom( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_zoom,value);
}
void CcuMem::pulseDEUBG_sys_zoom( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_zoom,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_zoom,idleV);
}
void CcuMem::outDEUBG_sys_fast_ref( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_fast_ref,value);
}
void CcuMem::pulseDEUBG_sys_fast_ref( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_fast_ref,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_fast_ref,idleV);
}
void CcuMem::outDEUBG_spu_tx_done( Ccu_reg value )
{
	out_assign_field(DEUBG,spu_tx_done,value);
}
void CcuMem::pulseDEUBG_spu_tx_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,spu_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,spu_tx_done,idleV);
}
void CcuMem::outDEUBG_sys_avg_en( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_avg_en,value);
}
void CcuMem::pulseDEUBG_sys_avg_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_avg_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_avg_en,idleV);
}
void CcuMem::outDEUBG_sys_trace( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_trace,value);
}
void CcuMem::pulseDEUBG_sys_trace( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_trace,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_trace,idleV);
}
void CcuMem::outDEUBG_sys_pos_sa_last_frm( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_pos_sa_last_frm,value);
}
void CcuMem::pulseDEUBG_sys_pos_sa_last_frm( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_pos_sa_last_frm,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_pos_sa_last_frm,idleV);
}
void CcuMem::outDEUBG_sys_pos_sa_view( Ccu_reg value )
{
	out_assign_field(DEUBG,sys_pos_sa_view,value);
}
void CcuMem::pulseDEUBG_sys_pos_sa_view( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,sys_pos_sa_view,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,sys_pos_sa_view,idleV);
}
void CcuMem::outDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg value )
{
	out_assign_field(DEUBG,spu_wav_sa_rd_tx_done,value);
}
void CcuMem::pulseDEUBG_spu_wav_sa_rd_tx_done( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,spu_wav_sa_rd_tx_done,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,spu_wav_sa_rd_tx_done,idleV);
}
void CcuMem::outDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg value )
{
	out_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,value);
}
void CcuMem::pulseDEUBG_scan_roll_pos_trig_frm_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,scan_roll_pos_trig_frm_buf,idleV);
}
void CcuMem::outDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg value )
{
	out_assign_field(DEUBG,scan_roll_pos_last_frm_buf,value);
}
void CcuMem::pulseDEUBG_scan_roll_pos_last_frm_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEUBG,scan_roll_pos_last_frm_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEUBG,scan_roll_pos_last_frm_buf,idleV);
}

void CcuMem::outDEUBG( Ccu_reg value )
{
	out_assign_field(DEUBG,payload,value);
}

void CcuMem::outDEUBG()
{
	out_member(DEUBG);
}


Ccu_reg CcuMem::inDEUBG()
{
	reg_in(DEUBG);
	return DEUBG.payload;
}


Ccu_reg CcuMem::inDEBUG_GT()
{
	reg_in(DEBUG_GT);
	return DEBUG_GT.payload;
}


void CcuMem::setDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg value )
{
	reg_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,value);
}

void CcuMem::setDEBUG_PLOT_DONE( Ccu_reg value )
{
	reg_assign_field(DEBUG_PLOT_DONE,payload,value);
}

void CcuMem::outDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg value )
{
	out_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,value);
}
void CcuMem::pulseDEBUG_PLOT_DONE_wpu_plot_done_dly( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_PLOT_DONE,wpu_plot_done_dly,idleV);
}

void CcuMem::outDEBUG_PLOT_DONE( Ccu_reg value )
{
	out_assign_field(DEBUG_PLOT_DONE,payload,value);
}

void CcuMem::outDEBUG_PLOT_DONE()
{
	out_member(DEBUG_PLOT_DONE);
}


Ccu_reg CcuMem::inDEBUG_PLOT_DONE()
{
	reg_in(DEBUG_PLOT_DONE);
	return DEBUG_PLOT_DONE.payload;
}


void CcuMem::setFPGA_SYNC_sync_cnt( Ccu_reg value )
{
	reg_assign_field(FPGA_SYNC,sync_cnt,value);
}
void CcuMem::setFPGA_SYNC_debug_scu_reset( Ccu_reg value )
{
	reg_assign_field(FPGA_SYNC,debug_scu_reset,value);
}
void CcuMem::setFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg value )
{
	reg_assign_field(FPGA_SYNC,debug_sys_rst_cnt,value);
}

void CcuMem::setFPGA_SYNC( Ccu_reg value )
{
	reg_assign_field(FPGA_SYNC,payload,value);
}

void CcuMem::outFPGA_SYNC_sync_cnt( Ccu_reg value )
{
	out_assign_field(FPGA_SYNC,sync_cnt,value);
}
void CcuMem::pulseFPGA_SYNC_sync_cnt( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(FPGA_SYNC,sync_cnt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FPGA_SYNC,sync_cnt,idleV);
}
void CcuMem::outFPGA_SYNC_debug_scu_reset( Ccu_reg value )
{
	out_assign_field(FPGA_SYNC,debug_scu_reset,value);
}
void CcuMem::pulseFPGA_SYNC_debug_scu_reset( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(FPGA_SYNC,debug_scu_reset,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FPGA_SYNC,debug_scu_reset,idleV);
}
void CcuMem::outFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg value )
{
	out_assign_field(FPGA_SYNC,debug_sys_rst_cnt,value);
}
void CcuMem::pulseFPGA_SYNC_debug_sys_rst_cnt( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(FPGA_SYNC,debug_sys_rst_cnt,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(FPGA_SYNC,debug_sys_rst_cnt,idleV);
}

void CcuMem::outFPGA_SYNC( Ccu_reg value )
{
	out_assign_field(FPGA_SYNC,payload,value);
}

void CcuMem::outFPGA_SYNC()
{
	out_member(FPGA_SYNC);
}


Ccu_reg CcuMem::inFPGA_SYNC()
{
	reg_in(FPGA_SYNC);
	return FPGA_SYNC.payload;
}


Ccu_reg CcuMem::inWPU_ID()
{
	reg_in(WPU_ID);
	return WPU_ID.payload;
}


void CcuMem::setDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,value);
}
void CcuMem::setDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,value);
}
void CcuMem::setDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,value);
}
void CcuMem::setDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,value);
}
void CcuMem::setDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,value);
}
void CcuMem::setDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,value);
}
void CcuMem::setDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,value);
}
void CcuMem::setDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,value);
}
void CcuMem::setDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,value);
}
void CcuMem::setDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,value);
}
void CcuMem::setDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,value);
}
void CcuMem::setDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,value);
}
void CcuMem::setDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,value);
}
void CcuMem::setDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,value);
}
void CcuMem::setDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,value);
}
void CcuMem::setDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,value);
}

void CcuMem::setDEBUG_WPU_PLOT( Ccu_reg value )
{
	reg_assign_field(DEBUG_WPU_PLOT,payload,value);
}

void CcuMem::outDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_wpu_plot_display_finish( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_finish,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_wpu_fast_done_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_fast_done_buf,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_wpu_display_done_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_display_done_buf,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_wpu_plot_display_id_equ( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_display_id_equ,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_scu_wpu_plot_last( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_last,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_scu_wave_plot_req( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wave_plot_req,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_scu_wpu_start( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_start,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_sys_wpu_plot( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,sys_wpu_plot,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_wpu_plot_done_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_done_buf,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_scu_wpu_plot_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_plot_en,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_wpu_plot_scan_finish_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_finish_buf,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_wpu_plot_scan_zoom_finish_buf( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,wpu_plot_scan_zoom_finish_buf,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_en( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_en,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_scu_wpu_stop_plot_vld( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,scu_wpu_stop_plot_vld,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_cfg_wpu_flush( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,cfg_wpu_flush,idleV);
}
void CcuMem::outDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,value);
}
void CcuMem::pulseDEBUG_WPU_PLOT_cfg_stop_plot( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_WPU_PLOT,cfg_stop_plot,idleV);
}

void CcuMem::outDEBUG_WPU_PLOT( Ccu_reg value )
{
	out_assign_field(DEBUG_WPU_PLOT,payload,value);
}

void CcuMem::outDEBUG_WPU_PLOT()
{
	out_member(DEBUG_WPU_PLOT);
}


Ccu_reg CcuMem::inDEBUG_WPU_PLOT()
{
	reg_in(DEBUG_WPU_PLOT);
	return DEBUG_WPU_PLOT.payload;
}


void CcuMem::setDEBUG_GT_CRC_fail_sum( Ccu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,fail_sum,value);
}
void CcuMem::setDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,value);
}
void CcuMem::setDEBUG_GT_CRC_crc_valid_i( Ccu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,crc_valid_i,value);
}

void CcuMem::setDEBUG_GT_CRC( Ccu_reg value )
{
	reg_assign_field(DEBUG_GT_CRC,payload,value);
}

void CcuMem::outDEBUG_GT_CRC_fail_sum( Ccu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,fail_sum,value);
}
void CcuMem::pulseDEBUG_GT_CRC_fail_sum( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_GT_CRC,fail_sum,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_GT_CRC,fail_sum,idleV);
}
void CcuMem::outDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,value);
}
void CcuMem::pulseDEBUG_GT_CRC_crc_pass_fail_n_i( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_GT_CRC,crc_pass_fail_n_i,idleV);
}
void CcuMem::outDEBUG_GT_CRC_crc_valid_i( Ccu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,crc_valid_i,value);
}
void CcuMem::pulseDEBUG_GT_CRC_crc_valid_i( Ccu_reg activeV, Ccu_reg idleV, int timeus )
{
	out_assign_field(DEBUG_GT_CRC,crc_valid_i,activeV);
	if ( timeus > 0 ) {{  IPhy::usleep( timeus);}}
	mem_assign_field(DEBUG_GT_CRC,crc_valid_i,idleV);
}

void CcuMem::outDEBUG_GT_CRC( Ccu_reg value )
{
	out_assign_field(DEBUG_GT_CRC,payload,value);
}

void CcuMem::outDEBUG_GT_CRC()
{
	out_member(DEBUG_GT_CRC);
}


Ccu_reg CcuMem::inDEBUG_GT_CRC()
{
	reg_in(DEBUG_GT_CRC);
	return DEBUG_GT_CRC.payload;
}


cache_addr CcuMem::addr_VERSION(){ return 0x1C00+mAddrBase; }
cache_addr CcuMem::addr_SCU_STATE(){ return 0x1C04+mAddrBase; }
cache_addr CcuMem::addr_CTRL(){ return 0x1C08+mAddrBase; }
cache_addr CcuMem::addr_MODE(){ return 0x1C0C+mAddrBase; }

cache_addr CcuMem::addr_MODE_10G(){ return 0x1C10+mAddrBase; }
cache_addr CcuMem::addr_PRE_SA(){ return 0x1C14+mAddrBase; }
cache_addr CcuMem::addr_POS_SA(){ return 0x1C18+mAddrBase; }
cache_addr CcuMem::addr_SCAN_TRIG_POSITION(){ return 0x1C1C+mAddrBase; }

cache_addr CcuMem::addr_AUTO_TRIG_TIMEOUT(){ return 0x1C20+mAddrBase; }
cache_addr CcuMem::addr_AUTO_TRIG_HOLD_OFF(){ return 0x1C24+mAddrBase; }
cache_addr CcuMem::addr_FSM_DELAY(){ return 0x1C28+mAddrBase; }
cache_addr CcuMem::addr_PLOT_DELAY(){ return 0x1C34+mAddrBase; }

cache_addr CcuMem::addr_FRAME_PLOT_NUM(){ return 0x1C38+mAddrBase; }
cache_addr CcuMem::addr_REC_PLAY(){ return 0x1C3C+mAddrBase; }
cache_addr CcuMem::addr_WAVE_INFO(){ return 0x1C40+mAddrBase; }
cache_addr CcuMem::addr_INDEX_CUR(){ return 0x1C44+mAddrBase; }

cache_addr CcuMem::addr_INDEX_BASE(){ return 0x1C48+mAddrBase; }
cache_addr CcuMem::addr_INDEX_UPPER(){ return 0x1C4C+mAddrBase; }
cache_addr CcuMem::addr_TRACE(){ return 0x1C50+mAddrBase; }
cache_addr CcuMem::addr_TRIG_DLY(){ return 0x1C54+mAddrBase; }

cache_addr CcuMem::addr_POS_SA_LAST_VIEW(){ return 0x1C5C+mAddrBase; }
cache_addr CcuMem::addr_MASK_FRM_NUM_L(){ return 0x1C60+mAddrBase; }
cache_addr CcuMem::addr_MASK_FRM_NUM_H(){ return 0x1C64+mAddrBase; }
cache_addr CcuMem::addr_MASK_TIMEOUT(){ return 0x1C68+mAddrBase; }

cache_addr CcuMem::addr_STATE_DISPLAY(){ return 0x1C6C+mAddrBase; }
cache_addr CcuMem::addr_MASK_TIMER(){ return 0x1C70+mAddrBase; }
cache_addr CcuMem::addr_CONFIG_ID(){ return 0x1C74+mAddrBase; }
cache_addr CcuMem::addr_DEUBG(){ return 0x1C80+mAddrBase; }

cache_addr CcuMem::addr_DEBUG_GT(){ return 0x1C84+mAddrBase; }
cache_addr CcuMem::addr_DEBUG_PLOT_DONE(){ return 0x1C88+mAddrBase; }
cache_addr CcuMem::addr_FPGA_SYNC(){ return 0x1C8C+mAddrBase; }
cache_addr CcuMem::addr_WPU_ID(){ return 0x1C90+mAddrBase; }

cache_addr CcuMem::addr_DEBUG_WPU_PLOT(){ return 0x1C94+mAddrBase; }
cache_addr CcuMem::addr_DEBUG_GT_CRC(){ return 0x1C98+mAddrBase; }


