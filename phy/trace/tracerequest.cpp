#include "tracerequest.h"
#include "../../include/dsocfg.h"   //! adc_max
#include "../../include/dsodbg.h"
namespace dso_phy {

traceRequest::traceRequest()
{
    for( int i = 0; i < array_count(mDigitalLevels); i+=2 )
    {
        mDigitalLevels[i + 0] = 128 + 6;
        mDigitalLevels[i + 1] = 128 - 6;

        mDigitalLevelsL[i + 0] = 128 + 6;
        mDigitalLevelsL[i + 1] = 128 - 6;
    }

    mBinz = false;
    mChCnt = 4;
    meSource = trace_ch_main;
    for ( int i = 0; i < array_count(mChLengthes); i++ )
    {
        mChLengthes[i] = 1000;
        mLaLengthes[i] = 1000;
    }

    for( int i=0; i<array_count(mChOnOff); i++ )
    {
        mChOnOff[i] = true;
    }
}

void traceRequest::setChCnt( int cnt )
{
    mChCnt = cnt;
}

void traceRequest::setSource( traceRequest::traceSource eSrc )
{
    meSource = eSrc;
}

void traceRequest::setChLength( int len, int viewId )
{
    Q_ASSERT( viewId >= 0 && viewId < array_count(mChLengthes) );
    mChLengthes[viewId] = len;
}
void traceRequest::setLaLength( int len, int viewId )
{
    Q_ASSERT( viewId >= 0 && viewId < array_count(mLaLengthes) );
    mLaLengthes[viewId] = len;
}
void traceRequest::setEyeLength( int len )
{ mEyeLength = len; }

int traceRequest::getChLength( int viewId )
{
    Q_ASSERT( viewId >= 0 && viewId < array_count(mChLengthes) );
    return mChLengthes[ viewId ];
}
int traceRequest::getLaLength( int viewId )
{
    Q_ASSERT( viewId >= 0 && viewId < array_count(mLaLengthes) );
    return mLaLengthes[viewId];
}
int traceRequest::getEyeLength()
{ return mEyeLength; }


//! la or ch
void traceRequest::attachChan( Chan ch, DsoWfm *pWfm, int viewId )
{
    Q_ASSERT( NULL != pWfm );

    ChanWfmMap *pMap;

    pMap = getChanWfmMap( ch, viewId );
    Q_ASSERT( NULL != pMap );

    Q_ASSERT( !pMap->contains(ch) );
    pMap->insert( ch, pWfm );
}

void traceRequest::setLevel( int levels[8] )
{
    memcpy( mDigitalLevels, levels, sizeof(mDigitalLevels) );
}

void traceRequest::setLevelL( int levels[8] )
{
    memcpy( mDigitalLevelsL, levels, sizeof(mDigitalLevels) );
}

void traceRequest::setBinaryzation( bool b )
{
    mBinz = b;
}

void traceRequest::setAcquireTimeMode(AcquireTimemode acqTimeMode)
{
    mAcqTimeMode = acqTimeMode;
}

ChanWfmMap *traceRequest::getChanWfmMap( Chan ch, int viewId )
{
    //! main + zoom
    Q_ASSERT( viewId >= 0 && viewId < 2 );

    if ( ch >= chan1 && ch <= chan4 )
    { return mChWfms + viewId; }
    else if ( ch >= d0 && ch <= d15 )
    { return mDxWfms + viewId; }
    else if ( ch >= digi_ch1 && ch <= digi_ch4_l )
    { return &mDigiWfms; }
    else if ( ch >= eye_ch1 && ch <= eye_ch4 )
    { return &mEyeWfms; }
    else if ( ch == la )
    { return mLaWfms + viewId; }
    else
    {
        Q_ASSERT(false);
        return NULL;
    }
}

}

