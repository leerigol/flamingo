#ifndef IPHYTRACE_H
#define IPHYTRACE_H

#include <QtCore>       //! sin

#include "../iphyfpga.h"
#include "../spu/iphyspu.h"
#include "../scu/iphyscu.h"
#include "../ccu/iphyccu.h"
#include "../wpu/iphywpu.h"
#include "../zynqfcu/iphyzynqfcurd.h"
#include "../zynqfcu/iphyzynqfcuwr.h"

#include "../../baseclass/dsowfm.h"
#include "tracerequest.h"

namespace dso_phy {

//#include "datagen.h"

class IPhyTrace : public IPhyFpga
{
public:
    enum tracePipe
    {
        trace_ch,
        trace_la,
        trace_digi,
        trace_eye,
        trace_pipe_sum,
    };

public:
    IPhyTrace();

public:
    void attachZynqFcu( IPhyZynqFcuWr *pFcuWr,
                        IPhyZynqFcuRd *pFcuRd );
    void attachSpu( IPhySpuGp *pSpus );
    void attachScu( IPhyScuGp *pScus );
    void attachCcu( IPhyCcu *pCcu);
    void attachWpu( IPhyWpu *pWpu);
public:
    void setPipeOpen( tracePipe pipe, bool b );
    bool getPipeOpen( tracePipe pipe );

    bool isInDataDone( traceRequest::traceSource src );
    bool isDeinteDone( traceRequest::traceSource src = traceRequest::trace_ch_main);

    void requestTraceEyeScu();

    int requestData( traceRequest &req, int tmous );
    int requestDataTrace( traceRequest &req, int tmous );
    int requestDataEye( traceRequest &req, int tmous );

    typedef int (IPhyTrace::*p_request)( traceRequest &req, int tmous );
    int requestCmbMain( traceRequest &req, int tmous );
    int requestAnaMain( traceRequest &req, int tmous );
    int requestLaMain( traceRequest &req, int tmous );

    int requestCmbZoom( traceRequest &req, int tmous );
    int requestAnaZoom( traceRequest &req, int tmous );
    int requestLaZoom( traceRequest &req, int tmous );

    int requestAnaMainZoom( traceRequest &req, int tmous );
    int requestLaMainZoom( traceRequest &req, int tmous );

    int requestCmbMainZoom( traceRequest &req, int tmous );

    int requestCmbView( traceRequest &req,
                        int viewId,
                        traceRequest::traceSource reqSrc,
                        traceRequest::traceSource a,
                        traceRequest::traceSource l,
                        bool bBinzEn, int tmous );
    int requestAnaView( traceRequest &req,
                        int viewId,
                        traceRequest::traceSource a,
                        bool bBinzEn, int tmous );
    int requestLaView( traceRequest &req,
                       int viewId,
                       traceRequest::traceSource l,
                       int tmous );

    int outAnaDeinterReadAddr(traceRequest &req,
                              int viewId,
                              int tmous);
    int outLaDeinterReadAddr(traceRequest &req,
                              int viewId,
                              int tmous);

    int deInterCH( traceRequest &req,
                   int viewId,
                   traceRequest::traceSource a,
                   int tmous);
    int deInterLa( traceRequest &req,
                   int viewId,
                   traceRequest::traceSource a,
                   int tmous );

    int requestEye( traceRequest &req, int tmous );

    typedef void (IPhyTrace::*trace_call_back)();
    int request(traceRequest &req,
                 int length,
                 int tmous=200000,
                 trace_call_back callback = NULL
                 );

    int deInter( traceRequest req,
                 traceRequest::traceSource src,
                int length,
                int tmous );

    //! cfg
    int requestCfg(traceRequest &req);

    int dispatch( ChanWfmMap *chanWfmMap,
                  int size,
                  int len );

    //!
    int dispatchBinary( ChanWfmMap *chanWfmMap,
                 int size,
                 int len,
                 int offset);

public: //! [dba: trace Import reqs]
    int importRequest( int length,
                       int tmous=2000);
    bool isImportDone();

public:
    //! xxx = addr
    int assign( DsoWfm *pWfm,
                int size,
                int len,
                quint32 addr );

    int assignLa(DsoWfm *pWfm,
            int size,
            int len,
            int offset,
            quint32 addr);

    int assign( void *pOut,
                int size,
                int len,
                quint32 addr );

    int assign( void *pOut,
                int size,
                int len,
                chanMeta &meta );

    //! addr = xxx
    int assign( quint32 addr,
                int size,
                int len,
                DsoWfm *pWfm );

    int assign( quint32 addr,
                int size,
                int len,
                void *pOut );

    int assign( chanMeta &meta,
                int size,
                int len,
                void *pOut );

    //! write back
    int writeMission( quint32 type,
                      quint32 addr,
                      int size,
                      int tmoms = 2000
                      );

    //! mapAddr
    void* mapAddr( quint32 addr, quint32 size, void **pDst );
    void  unmapAddr( void *pMap, quint32 size );
public:
#if 1
    int debug_trace_id;
#endif

public:
    IPhyZynqFcuWr *m_pZynqFcuWr;
    IPhyZynqFcuRd *m_pZynqFcuRd;

    IPhyCcu   *m_pCcu;
    IPhyWpu   *m_pWpu;
    IPhySpuGp *m_pSpuGp;
    IPhyScuGp *m_pScuGp;

    bool mPipeOpens[trace_pipe_sum];
};


}

#endif // IPHYTRACE_H
