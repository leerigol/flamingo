#include "iphytrace.h"
#include "../../service/servdso/sysdso.h"
#include <errno.h>
#include "../../phy/zynqfcu/iphyfcu_addr.h"

#define pack_8_bit( len )    ((len+7)/8)

namespace dso_phy {

IPhyTrace::IPhyTrace()
{
    m_pZynqFcuWr = NULL;
    m_pZynqFcuRd = NULL;

    m_pSpuGp = NULL;
    m_pScuGp = NULL;

    debug_trace_id = 0;
    //! pipe
    for ( int i = trace_ch; i < trace_pipe_sum; i++ )
    {
        mPipeOpens[ i - trace_ch] = true;
    }
}

void IPhyTrace::attachZynqFcu( IPhyZynqFcuWr *pFcuWr,
                               IPhyZynqFcuRd *pFcuRd )
{
    Q_ASSERT( NULL != pFcuWr );
    Q_ASSERT( NULL != pFcuRd );

    m_pZynqFcuWr = pFcuWr;
    m_pZynqFcuRd = pFcuRd;
}

void IPhyTrace::attachSpu( IPhySpuGp *pSpus )
{
    Q_ASSERT( pSpus != NULL );

    m_pSpuGp = pSpus;
}

void IPhyTrace::attachScu( IPhyScuGp *pScus )
{
    Q_ASSERT( NULL != pScus );
    m_pScuGp = pScus;
}

void IPhyTrace::attachCcu(IPhyCcu *pCcu)
{
    Q_ASSERT( NULL != pCcu);
    m_pCcu = pCcu;
}

void IPhyTrace::attachWpu(IPhyWpu *pWpu)
{
    m_pWpu = pWpu;
}

void IPhyTrace::setPipeOpen( tracePipe pipe, bool b )
{
    Q_ASSERT( pipe >= trace_ch && pipe < trace_pipe_sum );

    mPipeOpens[ pipe - trace_ch ] = b;
}
bool IPhyTrace::getPipeOpen( tracePipe pipe )
{
    Q_ASSERT( pipe >= trace_ch && pipe < trace_pipe_sum );

    return mPipeOpens[ pipe - trace_ch ];
}

bool IPhyTrace::isInDataDone(traceRequest::traceSource src)
{
    Q_ASSERT( NULL != m_pZynqFcuRd );

    int ch_main = 1;
    int la_main = 2;
    int ch_zoom = 4;
    int la_zoom = 8;
    m_pZynqFcuRd->intrace_wddr3_done();
    int done = m_pZynqFcuRd->gettrace_wddr3_done_trace_done();

    switch (src) {
    case traceRequest::trace_ch_main:
        return (done & ch_main) == ch_main;
    case traceRequest::trace_la_main:
        return (done & la_main) == la_main;
    case traceRequest::trace_ch_zoom:
        return (done & ch_zoom)== ch_zoom;
    case traceRequest::trace_la_zoom:
        return (done & la_zoom) == la_zoom;
    case traceRequest::trace_cmb_main:
        return (done & (ch_main + la_main)) == (ch_main + la_main);
    case traceRequest::trace_cmb_zoom:
        return (done & (ch_zoom + la_zoom)) == (ch_zoom + la_zoom);
    case traceRequest::trace_ch_main_zoom:
        return (done & (ch_main + ch_zoom)) == (ch_main + ch_zoom);
    case traceRequest::trace_la_main_zoom:
        return (done & (la_main + la_zoom)) == (la_main + la_zoom);
    case traceRequest::trace_cmb_main_zoom:
        return (done & (ch_main + la_main + ch_zoom + la_zoom))
                == (ch_main + la_main + ch_zoom + la_zoom);
    default:
        Q_ASSERT(false);
        break;
    }

    return  0;
}

bool IPhyTrace::isDeinteDone(traceRequest::traceSource src)
{
    LOG_ON_FILE();
    Q_UNUSED(src);
    Q_ASSERT( NULL != m_pZynqFcuRd );
    m_pZynqFcuRd->indeinterweave_done();

    return  m_pZynqFcuRd->getdeinterweave_done_deinter_done() == 1;
}

void IPhyTrace::requestTraceEyeScu()
{
    //! -- scu request:eye trace
    m_pCcu->outTRACE_eye_once_req( 1 );
}

int IPhyTrace::requestData( traceRequest &req,
                            int tmous )
{
    int ret;

    //! normal trace
    {
        ret = requestDataTrace( req, tmous );
        if ( ret != 0 ) return ret;
    }

    //! pipe not opened
    if ( mPipeOpens[trace_eye] )
    {
        ret = requestDataEye( req, tmous );
        if ( ret != 0 ) return ret;
    }

    return 0;
}

//! matrix
//! main_ch    main_la      zoom_ch     zoom_la
//!    0          0           0            0        -- no ch
//!    0          0           0            1        -- zoom_la
//!    0          0           1            0        -- zoom_ch
//!    0          0           1            1        -- zoom_cmb

//!    0          1           0            0        -- main_la
//!    0          1           0            1        -- main_la zoom_la
//!    0          1           1            0        -- main_zoom
//!    0          1           1            1        -- main_zoom

//!    1          0           0            0        -- main_ch
//!    1          0           0            1        -- main_zoom
//!    1          0           1            0        -- main_ch zoom_ch
//!    1          0           1            1        -- main_zoom

//!    1          1           0            0        -- main_cmb
//!    1          1           0            1        -- main_zoom
//!    1          1           1            0        -- main_zoom
//!    1          1           1            1        -- main_zoom

struct traceRequestStub
{
    quint32 enBm;
    IPhyTrace::p_request pRequest;
};
static traceRequestStub _trace_stubs[] =
{
    0x0,    0x0,
    0x1,    &IPhyTrace::requestLaZoom,
    0x2,    &IPhyTrace::requestAnaZoom,
    0x3,    &IPhyTrace::requestCmbZoom,

    0x4,    &IPhyTrace::requestLaMain,
    0x5,    &IPhyTrace::requestLaMainZoom,
    0x6,    &IPhyTrace::requestCmbMainZoom,
    0x7,    &IPhyTrace::requestCmbMainZoom,

    0x8,    &IPhyTrace::requestAnaMain,
    0x9,    &IPhyTrace::requestCmbMainZoom,
    0xa,    &IPhyTrace::requestAnaMainZoom,
    0xb,    &IPhyTrace::requestCmbMainZoom,

    0xc,    &IPhyTrace::requestCmbMain,
    0xd,    &IPhyTrace::requestCmbMainZoom,
    0xe,    &IPhyTrace::requestCmbMainZoom,
    0xf,    &IPhyTrace::requestCmbMainZoom,
};

int IPhyTrace::requestDataTrace( traceRequest &req, int tmous )
{
    int ret;

    bool bCH[2], bLa[2];

    //! main + zoom
    for ( int i = 0; i < 2; i++ )
    {
        bCH[i] = req.mChWfms[i].size() > 0 ;
        bLa[i] = req.mDxWfms[i].size() > 0 || req.mLaWfms[i].size() > 0;
    }

    bCH[0] |= req.mDigiWfms.size() > 0 ;

    //! build the mask
    quint32 enBm = 0;

    enBm = bLa[1]<<0;
    enBm |= bLa[0]<<2;
    enBm |= bCH[1]<<1;
    enBm |= bCH[0]<<3;

    if ( enBm == 0 )
    { return ERR_NO_TRACE; }

    //! get the api and do
    Q_ASSERT( enBm < 16 );
    Q_ASSERT( _trace_stubs[enBm].pRequest != NULL );
    ret = (this->*_trace_stubs[enBm].pRequest)( req, tmous );
    if ( ret != 0 )
    {
        return ret; }

    return 0;
}

int IPhyTrace::requestDataEye( traceRequest &req, int tmous )
{
    int ret;

    if ( req.mEyeWfms.size() > 0 )
    {
        ret = requestEye( req, tmous );

        if ( ret != 0 ) { return ret; }

        return 0;
    }

    return 0;
}

//! -- main
int IPhyTrace::requestCmbMain( traceRequest &req, int tmous )
{
    int ret;
    //! -- req stack
    req.setSource( traceRequest::trace_cmb_main );
    req.setBinaryzation( req.mDigiWfms.size() > 0 );

    ret = requestCmbView( req,
                          0,
                          traceRequest::trace_cmb_main,
                          traceRequest::trace_ch_main,
                          traceRequest::trace_la_main,
                          true,
                          tmous
                          );

    return ret;
}

int IPhyTrace::requestAnaMain( traceRequest &req, int tmous )
{
    return requestAnaView( req, 0, traceRequest::trace_ch_main, true, tmous );
}

int IPhyTrace::requestLaMain( traceRequest &req, int tmous )
{
    return requestLaView( req, 0, traceRequest::trace_la_main, tmous );
}

//! -- zoom
int IPhyTrace::requestCmbZoom( traceRequest &req, int tmous )
{
    int ret;
    //! -- req stack
    req.setSource( traceRequest::trace_cmb_zoom );
    req.setBinaryzation( false );

    ret = requestCmbView( req,
                          1,
                          traceRequest::trace_cmb_zoom,
                          traceRequest::trace_ch_zoom,
                          traceRequest::trace_la_zoom,
                          false,
                          tmous
                          );

    return ret;
}

int IPhyTrace::requestAnaZoom( traceRequest &req, int tmous )
{
    return requestAnaView( req, 1, traceRequest::trace_ch_zoom, false, tmous );
}

int IPhyTrace::requestLaZoom( traceRequest &req, int tmous )
{
    return requestLaView( req, 1, traceRequest::trace_la_zoom, tmous );
}

int IPhyTrace::requestAnaMainZoom( traceRequest &req, int tmous )
{
    int ret;

    //! -- req stack
    req.setSource( traceRequest::trace_ch_main_zoom );
    req.setBinaryzation( req.mDigiWfms.size() > 0 );

    //! -- request
    int chLength = req.getChLength( 0 );
    ret = request( req, chLength, tmous );
    LOG_ON_FILE()<<chLength;
    if ( ret != 0 ) { LOG_DBG();return ret; }

    //! -- addr
    //! ch main
    m_pZynqFcuWr->outdeinterweave_chx_raddr(
                trace_ana_main_wbase_addr
                );

    ret = deInterCH( req, 0, traceRequest::trace_ch_main, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    //! ch zoom
    m_pZynqFcuWr->outdeinterweave_chx_raddr(
                trace_ana_zoom_wbase_addr
                );

    ret = deInterCH( req, 1, traceRequest::trace_ch_zoom, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    return 0;
}

int IPhyTrace::requestLaMainZoom( traceRequest &req, int tmous )
{
    int ret;

    //! -- stack
    int laLength;
    laLength = req.getLaLength( 0 );
    req.setSource( traceRequest::trace_la_main_zoom );
    req.setBinaryzation( 0 );

    //! -- request
    ret = request( req, laLength, tmous );
    if ( ret != 0 ) { LOG_DBG()<<laLength;return ret; }

    //! -- addr
    //! la main
    m_pZynqFcuWr->outdeinterweave_lax_raddr( trace_la_main_wbase_addr );
    ret = deInterLa( req, 0, traceRequest::trace_la_main, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    //! la zoom
    m_pZynqFcuWr->outdeinterweave_lax_raddr( trace_la_zoom_wbase_addr);
    ret = deInterLa( req, 1, traceRequest::trace_la_zoom, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    return 0;
}

int IPhyTrace::requestCmbMainZoom( traceRequest &req, int tmous )
{
    int ret;

    //! request + main deinter
    ret = requestCmbView( req, 0,
                          traceRequest::trace_cmb_main_zoom,
                          traceRequest::trace_ch_main,
                          traceRequest::trace_la_main,
                          true,
                          tmous );
    if ( ret != 0 )
    { LOG_DBG(); return ret; }

    //! zoom:  m_la + m_ch + z_la + z_ch
    //! -- addr
//    quint32 zoomBase;
//    zoomBase = m_pZynqFcuWr->trace_wbaseaddr
//               + req.getChLength( 0 ) * req.mChCnt
//               + req.getLaLength( 0 ) * 2;
    //! la zoom
    m_pZynqFcuWr->outdeinterweave_lax_raddr(
                trace_la_zoom_wbase_addr
                );

    //! ch zoom
    m_pZynqFcuWr->outdeinterweave_chx_raddr(
                trace_ana_zoom_wbase_addr
                );

    ret = deInterCH( req,
                     1,
                     traceRequest::trace_ch_zoom,
                     tmous );
    if ( ret != 0 )
    { LOG_DBG(); return ret; }

    ret = deInterLa( req,
                     1,
                     traceRequest::trace_la_zoom,
                     tmous );
    if ( ret != 0 )
    { LOG_DBG(); return ret; }

    return 0;
}

int IPhyTrace::requestCmbView( traceRequest &req,
                               int viewId,
                               traceRequest::traceSource reqSrc,
                               traceRequest::traceSource a,
                               traceRequest::traceSource l,
                               bool bBinzEn, int tmous )
{
    int ret;

    //! -- req stack
    req.setSource( reqSrc );
    if ( bBinzEn )
    { req.setBinaryzation( req.mDigiWfms.size() > 0 ); }
    else
    { req.setBinaryzation( false); }

    //! -- request
    if(req.mAcqTimeMode == Acquire_ROLL)
    {
        //! -- addr
        ret = outAnaDeinterReadAddr(req, viewId, tmous);
        ret = outLaDeinterReadAddr(req, viewId, tmous);
    }
    else
    {
        int chLength = req.getChLength( viewId );
        ret = request( req, chLength, tmous );
        if ( ret != 0 ) { LOG_DBG();return ret; }

        m_pZynqFcuWr->outdeinterweave_lax_raddr(
                        trace_la_main_wbase_addr
                        );
        m_pZynqFcuWr->outdeinterweave_chx_raddr(
                        trace_ana_main_wbase_addr
                        );

        req.rollChOffset = 0;
        req.rollLaOffset = 0;
    }

    //! -- ch
    ret = deInterCH( req, viewId, a, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    //! -- la
    ret = deInterLa( req, viewId, l, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

#ifdef SAVE_DEBUG_TRACE
    if(sysHasArg("-log_la_base_trace"))
    {
        LOG_ON_FILE();
        QString file("/rigol/trace/la_before_deinter");
        DsoWfm temp;
        assignLa(&temp, roll_trace_la_slice * 16 * 2, roll_trace_la_slice * 8 * 16 *2, 0,
               roll_trace_la_begin);
        temp.save(file);
    }
#endif

    return 0;
}

int IPhyTrace::requestAnaView( traceRequest &req,
                               int viewId,
                               traceRequest::traceSource a,
                               bool bBinzEn, int tmous )
{
    //! -- req stack
    req.setSource( a );
    if ( bBinzEn )
    {
        req.setBinaryzation( req.mDigiWfms.size() > 0 );
    }
    else
    {
        req.setBinaryzation( false);
    }

    DsoErr err = outAnaDeinterReadAddr(req, viewId, tmous);

    if(err != 0)
    {
        LOG_ON_FILE()<<err;
        return ERR_NO_TRACE;
    }

    err = deInterCH( req, viewId, a, tmous );
#ifdef SAVE_DEBUG_TRACE
    if(sysHasArg("-log_ana_base_trace"))
    {
        LOG_ON_FILE();
        QString file("/rigol/trace/anala_before_deinter");
        DsoWfm temp;
        assign(&temp, roll_trace_ana_slice,  roll_trace_ana_slice, roll_trace_ana_begin);
        temp.save(file);
    }
#endif
    return err;
}

int IPhyTrace::requestLaView( traceRequest &req,
                              int viewId,
                              traceRequest::traceSource l,
                              int tmous )
{
    int ret;

    //! -- stack
    req.setSource( l );
    req.setBinaryzation( 0 );

    ret = outLaDeinterReadAddr(req, viewId, tmous);
    if(ret != 0)
    {
        return ERR_NO_TRACE;
    }
    Q_UNUSED(ret);

    return deInterLa( req, viewId, l, tmous );
}

int IPhyTrace::outAnaDeinterReadAddr(traceRequest &req , int viewId, int tmous)
{
    //! -- addr
    //! ch
    uint writeAddr = 0;
    uint readAddr = 0;
    int ret = ERR_NONE;
    int chLength = req.getChLength( viewId );

    //! -- request
    if( req.mAcqTimeMode == Acquire_ROLL && !req.mRequestOnce)
    {
        m_pZynqFcuRd->inroll_ana_trace_waddr_latch();
        writeAddr = m_pZynqFcuRd->getroll_ana_trace_waddr_latch();

        //! 逻辑地址两边都是闭区间
        m_pZynqFcuRd->inroll_full_screen_wdone_flag();
        if(m_pZynqFcuRd->getroll_full_screen_wdone_flag_roll_full_screen_wdone_flag())
        {//! 满屏
            LOG_ON_FILE()<<"full";
            if((int)(writeAddr - roll_trace_ana_begin) < (chLength * req.mChCnt - 1))
            {
                //! 左端放不下
                LOG_ON_FILE()<<"left and right";
                uint left = writeAddr - roll_trace_ana_begin + 1;
                uint right = chLength * req.mChCnt - left;
                readAddr = roll_trace_ana_begin + req.mChCnt * chLength * 2 - right;
                LOG_ON_FILE()<<left<<right<<"trace chcnt:"<<req.mChCnt;
            }
            else
            {
                //! 左端放得下
                LOG_ON_FILE()<<"only left";
                readAddr = writeAddr - roll_trace_ana_slice * req.mChCnt + 1;
            }
            req.rollChOffset = 0;
        }
        else
        {//! 非满屏
            LOG_DBG()<<"not full";
            readAddr = roll_trace_ana_begin;
            int realLenth = (writeAddr - readAddr) / 4;
            req.rollChOffset = chLength - realLenth;
        }
        //! 128B对齐
        readAddr = readAddr - readAddr % 128;
        m_pZynqFcuWr->outdeinterweave_chx_raddr( readAddr );

        m_pZynqFcuRd->inframe_id();
        int frameId = m_pZynqFcuRd->getframe_id_frame_trace_id_expand();
        req.mRequestAttrId = frameId;

        ret = ERR_NONE;
    }
    else
    {
        if( chLength > 0 )
        {
            ret = request( req, chLength, tmous );
            if ( ret != 0 )
            {
                LOG_DBG();
                return ret;
            }

            m_pZynqFcuWr->outdeinterweave_chx_raddr(
                        m_pZynqFcuWr->trace_wbaseaddr
                        );
        }
        else
        {
            qDebug()<<"Warnning.........chlength "<<chLength;
        }
    }
    return ret;
}

int IPhyTrace::outLaDeinterReadAddr(traceRequest &req, int viewId, int tmous)
{
    //! -- addr
    //! ch
    uint writeAddr = 0;
    uint readAddr = 0;
    int ret = 0;
    int laLength;
    laLength = req.getLaLength( viewId );
    if(req.mAcqTimeMode == Acquire_ROLL && !req.mRequestOnce)
    {
        m_pZynqFcuRd->inroll_la_trace_waddr_latch();
        writeAddr = m_pZynqFcuRd->getroll_la_trace_waddr_latch();

        //! 逻辑地址两边都是闭区间
        m_pZynqFcuRd->inroll_full_screen_wdone_flag();
        if(m_pZynqFcuRd->getroll_full_screen_wdone_flag_roll_full_screen_wdone_flag())
        {//! 满屏
            LOG_DBG()<<"full";
            if((writeAddr - roll_trace_la_begin) < (roll_trace_la_slice * 16 - 1))
            {
                //! 左端放不下
                LOG_DBG()<<"left and right";
                uint left = writeAddr - roll_trace_la_begin + 1;
                uint right = roll_trace_la_slice * 16 - left;
                readAddr = roll_trace_la_begin + 16 * roll_trace_la_slice * 2 - right;
                LOG_ON_FILE()<<left<<right<<"la chcnt:"<<16;
            }
            else
            {
                //! 左端放得下
                LOG_DBG()<<"only left";
                readAddr = writeAddr - roll_trace_la_slice * 16 + 1;
            }
            req.rollLaOffset = 0;
        }
        else
        {//! 非满屏
            LOG_DBG()<<"not full";
            readAddr = roll_trace_la_begin;
            int realLength = (writeAddr - readAddr) / 16 * 8;
            req.rollLaOffset = laLength - realLength;
        }
        //! 128B对齐
        readAddr = readAddr - readAddr % 128;
        m_pZynqFcuWr->outdeinterweave_lax_raddr( readAddr );

        m_pZynqFcuRd->inframe_id();
        int frameId = m_pZynqFcuRd->getframe_id_frame_trace_id_expand();
        req.mRequestAttrId = frameId;
    }
    else
    {
        LOG_ON_FILE()<<"lalength"<<laLength;
        ret = request( req, laLength, tmous );
        if ( ret != 0 ) { LOG_DBG()<<laLength;return ret; }

        //! -- addr
        //! la
        m_pZynqFcuWr->outdeinterweave_lax_raddr(
                    trace_la_main_wbase_addr
                    );
        req.rollLaOffset = 0;
    }
    return ret;
}

///
/// \brief IPhyTrace::deInterCH
/// \param req
/// \param viewId 0:main 1:zoom
/// \param a
/// \param tmous
/// \return
///
int IPhyTrace::deInterCH( traceRequest &req,
                          int viewId,
                           traceRequest::traceSource a,
                           int tmous )
{
    int chLength;
    int ret;
    chLength = req.getChLength( viewId );

    LOG_ON_FILE()<<chLength;

    //! -- deinter
    ret = deInter( req, a, chLength, tmous );
    if ( ret != 0 )
    {
        LOG_DBG()<<a<<chLength;
        return ret;
    }

    int size = chLength;
    //! dispatch ch
    if(req.mAcqTimeMode == Acquire_ROLL && !req.mRequestOnce)
    {
        ret = dispatch( &req.mChWfms[viewId], size, size - req.rollChOffset );
    }
    else
    {
        ret = dispatch( &req.mChWfms[viewId], size, size );
    }

    if ( ret != 0 ) { LOG_DBG(); return ret; }

    //! digi dispatch:only for main
    if ( req.mBinz )//need to fetch decoder data
    {
        if( viewId != 0)
        {
            return 0;
        }
        ret = dispatchBinary( &req.mDigiWfms,
                              pack_8_bit(chLength),
                              chLength,
                              (size - chLength)/8);
        if ( ret != 0 )
        {
            LOG_DBG();
            return ret;
        }
    }
    return 0;
}

int IPhyTrace::deInterLa(  traceRequest &req,
                           int viewId,
                           traceRequest::traceSource l,
                           int tmous )
{
    int laLength;
    int ret;

    laLength = req.getLaLength( viewId );

    //! -- packed la
    ret = dispatchBinary( &req.mLaWfms[viewId], laLength*2, laLength, req.rollLaOffset);
    if ( ret != 0 ) { return ret; }

    //! -- deinter
    ret = deInter( req, l, laLength, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    //! -- dispatch
    //! byte packed
    ret = dispatchBinary( &req.mDxWfms[viewId], pack_8_bit(laLength), laLength, req.rollLaOffset);
    if ( ret != 0 ) { LOG_DBG();return ret; }

    return 0;
}

int IPhyTrace::requestEye( traceRequest &req, int tmous )
{
    int ret;

    //! -- stack
    req.setSource( traceRequest::trace_eye );
    req.setBinaryzation( 0 );

    //! -- request
    ret = request( req, req.mEyeLength, tmous, &IPhyTrace::requestTraceEyeScu );
    if ( ret != 0 ) { LOG_DBG()<<req.mEyeLength;return ret; }

    //! -- deinter
    ret = deInter( req, traceRequest::trace_eye, req.mEyeLength, tmous );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    //! -- dispatch
    ret = dispatch( &req.mEyeWfms, (req.mEyeLength), req.mEyeLength );
    if ( ret != 0 ) { LOG_DBG();return ret; }

    return 0;
}

int IPhyTrace::request( traceRequest& req,
                        int length,
                        int tmous,
                        trace_call_back callback )
{
    int ret;

    //! request cfg
    ret = requestCfg( req );
    if ( ret != 0 ) return ret;

    //! request
    LOG_ON_FILE()<<"trace length"<<length;

    if(req.mRequestOnce)
    {
        //!将单次请求移到主线程的波形操作当中
        if(isInDataDone(req.meSource))
        {
            m_pZynqFcuRd->inframe_id();
            int frameId = m_pZynqFcuRd->getframe_id_frame_trace_id_expand();
            m_pScuGp->inCONFIG_ID();
            int configId = m_pScuGp->getCONFIG_ID();
            req.mRequestAttrId = frameId;
            if(frameId != configId
                    && configId != (frameId + 1) % 4096 )
            {
                m_pZynqFcuWr->endTrace();
                if(sysHasArg( "-log_id" ))
                {
                    qDebug()<<__LINE__<<configId<<frameId<<"different trace id";
                }
                return ERR_NO_TRACE;
            }
            return ERR_NONE;
        }
        else
        {
            m_pCcu->outTRACE_trace_once_req( 1 );
        }
    }
    else
    {
//        m_pZynqFcuWr->endTrace();
//        m_pCcu->outTRACE_trace_once_req( 1 );
    }

    if ( NULL != callback )
    {
        (this->*callback)();
    }

    Q_ASSERT( tmous > 0 );
    //! wait on condition
    int tickms = 10;
    do
    {
        //! wait a few time
        if ( tmous > 0 )
        {
            m_pZynqFcuRd->inframe_id();
            int frameId = m_pZynqFcuRd->getframe_id_frame_trace_id_expand();
            m_pScuGp->inCONFIG_ID();
            int configId = m_pScuGp->getCONFIG_ID();
            req.mRequestAttrId = frameId;
            if(configId != frameId
                    && configId != (frameId + 1) % 4096 )
            {
                if(sysHasArg( "-log_id" ))
                {
                    qDebug()<<__LINE__<<"different trace id";
                }
                return ERR_NO_TRACE;
            }

            if ( isInDataDone( req.meSource ) )
            {
                ret = 0;
                QThread::msleep(1);
                break;
            }

            tmous -= tickms;
            QThread::msleep( tickms );
        }

        //! check end
        if ( tmous <= 0 )
        {
            //! wait on condition
            if ( isInDataDone( req.meSource ) )
            {
                QThread::msleep(10);
                break;
            }
            else
            {
                //qWarning()<<"trace indata wait time out";
                ret = -1;
                break;
            }
        }
    }while( 1 );

    LOG_ON_FILE()<<tmous;
    //! req end
    return ret;
}

int IPhyTrace::deInter(  traceRequest req,
                         traceRequest::traceSource src,
                        int length,
                        int tmous  )
{
    int ret;

    //! main
    if ( src == traceRequest::trace_ch_main )
    {
        m_pZynqFcuWr->outdeinterweave_property_cfg1_deinterweave_source_type(0);
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en( req.mBinz );
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en_sec( req.mBinz );
    }
    else if ( src == traceRequest::trace_la_main )
    {
        m_pZynqFcuWr->outdeinterweave_property_cfg1_deinterweave_source_type(1);
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en( 0 );
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en_sec( 0 );
    }
    //! zoom
    else if ( src == traceRequest::trace_ch_zoom )
    {
        m_pZynqFcuWr->outdeinterweave_property_cfg1_deinterweave_source_type(0);
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en( 0 );
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en_sec( 0 );
    }
    else if ( src == traceRequest::trace_la_zoom )
    {
        m_pZynqFcuWr->outdeinterweave_property_cfg1_deinterweave_source_type(1);
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en( 0 );
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en_sec( 0 );
    }
    //! eye
    else if ( src == traceRequest::trace_eye )
    {
        m_pZynqFcuWr->outdeinterweave_property_cfg1_deinterweave_source_type(0);
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en( 0 );
        m_pZynqFcuWr->outdeinterweave_property_cfg1_decode_en_sec( 0 );
    }
    else
    { LOG_DBG()<<src;return -1; }

    //! length
    m_pZynqFcuWr->outdeinterweave_chx_lax_rdat_num( length );

    //! -- deinit
    m_pZynqFcuWr->requestDeinte();

    //! wait recv done
    ret = wait( (IPhy::p_completed)(&IPhyTrace::isDeinteDone), tmous);
    QThread::msleep(1);
    m_pZynqFcuWr->endDeinte();

    return ret;
}

int IPhyTrace::requestCfg( traceRequest &req )
{
    Q_ASSERT( m_pZynqFcuWr != NULL );
    Q_ASSERT( m_pZynqFcuRd != NULL );

    //single
    if( req.mChCnt == 1 )
    {
        for(int i=0; i<4; i++)
        {
            if( req.mChOnOff[i] )
            {
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_a_level_h( req.mDigitalLevels[2*i] );
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_a_level_l( req.mDigitalLevels[2*i+1] );

                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( req.mDigitalLevelsL[2*i] );
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( req.mDigitalLevelsL[2*i+1] );
                break;
            }
        }
    }
    //2 channel mode
    else if( req.mChCnt == 2 )
    {
        bool a = false;
        bool b = false;
        for(int i=0; i<4; i++)
        {
            if( req.mChOnOff[i] && !a)
            {
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_a_level_h( req.mDigitalLevels[2*i] );
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_a_level_l( req.mDigitalLevels[2*i+1] );

                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( req.mDigitalLevelsL[2*i] );
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( req.mDigitalLevelsL[2*i+1] );
                a = true;
                continue;
            }

            if( req.mChOnOff[i] && !b)
            {
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_b_level_h( req.mDigitalLevels[2*i] );
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_b_level_l( req.mDigitalLevels[2*i+1] );

                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_b_level_h( req.mDigitalLevels[2*i] );
                m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_b_level_l( req.mDigitalLevels[2*i+1] );
                b = true;
            }

        }
    }//4 channel mode
    else
    {
        //! cfg
        //! -- level H
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_a_level_h( req.mDigitalLevels[0] );
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_a_level_l( req.mDigitalLevels[1] );
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_b_level_h( req.mDigitalLevels[2] );
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_cmp_b_level_l( req.mDigitalLevels[3] );

        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_cmp_c_level_h( req.mDigitalLevels[4] );
        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_cmp_c_level_l( req.mDigitalLevels[5] );
        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_cmp_d_level_h( req.mDigitalLevels[6] );
        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_cmp_d_level_l( req.mDigitalLevels[7] );

        //! -- level L
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_a_level_h_sec( req.mDigitalLevelsL[0] );
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_a_level_l_sec( req.mDigitalLevelsL[1] );
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_b_level_h_sec( req.mDigitalLevelsL[2] );
        m_pZynqFcuWr->setdecode_a_or_b_channel_cmp_sec_cmp_b_level_l_sec( req.mDigitalLevelsL[3] );

        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_sec_cmp_c_level_h_sec( req.mDigitalLevelsL[4] );
        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_sec_cmp_c_level_l_sec( req.mDigitalLevelsL[5] );
        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_sec_cmp_d_level_h_sec( req.mDigitalLevelsL[6] );
        m_pZynqFcuWr->setdecode_c_or_d_channel_cmp_sec_cmp_d_level_l_sec( req.mDigitalLevelsL[7] );
    }

    //! source
    m_pZynqFcuWr->settrace_req_trace_datype( req.meSource );

    //! chan mode
    //! configured by spu->zynq->wpu compress
    m_pZynqFcuWr->setdeinterweave_property_cfg4_channel_mode( req.mChCnt );
    m_pZynqFcuWr->flushWCache();

    return 0;
}

int IPhyTrace::dispatch(
                         ChanWfmMap *chanWfmMap,
                         int size,
                         int len )
{
    Q_ASSERT( NULL != chanWfmMap );
    int ret;
    //! read addr
    quint32 addr;

    //! split -- ch
    QMapIterator<Chan, DsoWfm *> iter( *chanWfmMap );

    m_pZynqFcuRd->inframe_id();
    int frameId = m_pZynqFcuRd->getframe_id_frame_trace_id_expand();

    while (iter.hasNext())
    {
        iter.next();

        iter.value()->setAttrId( frameId );

        //! ch->addr
        addr = iter.value()->getAddr();
        Q_ASSERT(  addr != 0 );

        //! mount data from addr
        iter.value()->m_wfmMutex.lock();
        ret = assign( iter.value(), size, len, addr );
        iter.value()->m_wfmMutex.unlock();
#ifdef SAVE_DEBUG_TRACE
        if(sysHasArg("-log_deInter_ch"))
        {
            LOG_DBG()<<"save after_deInter data";
            if(iter.key() == chan1)
            {
                debug_trace_id ++;
                iter.value()->save(QString("/rigol/trace/traceDeinter_%1.dat").arg(debug_trace_id));
            }
        }
#endif
        if ( ret != len )
        {
            //LOG_ERR()<<"err";
            return ret;
        }
    }

    return 0;
}

int IPhyTrace::dispatchBinary(ChanWfmMap *chanWfmMap, int size, int len, int offset)
{
    Q_ASSERT( NULL != chanWfmMap );
    int ret;
    //! read addr
    quint32 addr;

    //! split -- ch
    QMapIterator<Chan, DsoWfm *> iter( *chanWfmMap );
    m_pZynqFcuRd->inframe_id();
    int frameId = m_pZynqFcuRd->getframe_id_frame_trace_id_expand();
    while (iter.hasNext())
    {
        iter.next();
        //! ch->addr
        addr = iter.value()->getAddr();
        Q_ASSERT(  addr != 0 );

        iter.value()->setAttrId(frameId);

        int sizeOffset = offset / 8;
        //! mount data from addr
        ret = assignLa( iter.value(), size, len, sizeOffset, addr);
        iter.value()->setRange(sizeOffset, size);


#ifdef SAVE_DEBUG_TRACE
        //if(sysHasArg("-log_deInter_la"))
        {
            LOG_ON_FILE()<<"save after_deInter la";
            if(iter.key() == digi_ch1)
            {
                //debug_trace_id ++;
                iter.value()->expandLa();
                iter.value()->save(QString("/tmp/traceDeinter_%1.dat").arg(debug_trace_id));
            }
        }
#endif

        if ( ret != len ) { LOG_ERR()<<"err"<<len<<size<<ret; return ret; }
    }

    return 0;
}

int IPhyTrace::importRequest(int length, int tmous)
{
    int ret = -1;
    //! cfg
    m_pZynqFcuWr->setfpga_rdat_cfg1_fpga_rdat_num(length);
    m_pZynqFcuWr->setfpga_rdat_cfg1_fpga_rdat_type(1);
    m_pZynqFcuWr->flushWCache();

    //! start
    m_pZynqFcuWr->outfpga_rdat_cfg2_fpga_rdat_start(1);
    m_pZynqFcuWr->outfpga_rdat_cfg2_fpga_rdat_start(0);

    //! done
    do
    {
        if(isImportDone())
        {
            ret = 0;
            break;
        }


        if( tmous <= 0 )
        {
            ret = -1;
            qWarning()<<__FILE__<<__LINE__<<"wait import done failure.";
            break;
        }

        tmous -= 10;
        QThread::msleep(10);
    }while(1);

    //! clear done
    m_pZynqFcuWr->outfpga_rdat_cfg3_fpga_rdat_done_clr(1);
    m_pZynqFcuWr->outfpga_rdat_cfg3_fpga_rdat_done_clr(0);

    return ret;
}

bool IPhyTrace::isImportDone()
{
    bool done = false;
    m_pZynqFcuRd->infpga_rdat_done();
    done = m_pZynqFcuRd->getfpga_rdat_done();
    return done;
}

int IPhyTrace::assign(
                     DsoWfm *pWfm,
                     int size,
                     int len,
                     quint32 addr
                      )
{
    Q_ASSERT( NULL != pWfm );
    void *pAddr, *pMem;

    pAddr = mapAddr( addr, size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! read data
    pWfm->init(size);

    //!temp change:断言错误
    if(len < 0 || size < len)
    {
        return -1;
    }

    pWfm->setPoint( (DsoPoint*)pMem, len, len, size - len);

    //! unmap
    unmapAddr( pAddr, size );
    return len;
}

int IPhyTrace::assignLa(
        DsoWfm *pWfm,
        int size,
        int len,
        int offset,
        quint32 addr )
{
    Q_ASSERT( NULL != pWfm );
    void *pAddr, *pMem;

    pAddr = mapAddr( addr, size, &pMem );
    if ( NULL == pAddr )
    {
        LOG_DBG()<<size<<QString::number(addr,16)<<pMem<<pAddr;
        return -1;
    }

    //! read data
    pWfm->setPoint( (DsoPoint*) pMem, size, len, offset);

    //! unmap
    unmapAddr( pAddr, size );
    return len;
}

//! *pOut = addr
int IPhyTrace::assign( void *pOut,
            int size,
            int len,
            quint32 addr )
{
    Q_ASSERT( NULL != pOut );

    void *pAddr, *pMem;

    pAddr = mapAddr( addr, size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! read data
    memcpy( pOut, pMem, len );

    //! unmap
    unmapAddr( pAddr, size );

    return len;
}

int IPhyTrace::assign( void *pOut,
            int size,
            int len,
            chanMeta &meta )
{
    Q_ASSERT( NULL != pOut );
    void *pAddr, *pMem;

    pAddr = mapAddr( meta.getAddr(), size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! assign data
    quint8 *pSrc = (quint8*)pMem;
    quint8 *pDst = (quint8*)pOut;

    Q_ASSERT(len <= size);
    memcpy( pDst, pSrc, len );

    //! unmap
    unmapAddr( pAddr, size );

    return len;
}

//! addr = xxx
int IPhyTrace::assign( quint32 addr,
            int size,
            int len,
            DsoWfm *pWfm )
{
    Q_ASSERT( NULL != pWfm );
    void *pAddr, *pMem;

    pAddr = mapAddr( addr, size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! assign data
    memcpy( pMem, pWfm->getPoint(), len );

    //! unmap
    unmapAddr( pAddr, size );

    return len;
}

int IPhyTrace::assign( quint32 addr,
            int size,
            int len,
            void *pOut )
{
    Q_ASSERT( NULL != pOut );
    void *pAddr, *pMem;

    pAddr = mapAddr( addr, size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! assign data
    memcpy( pMem, pOut, len );

    //! unmap
    unmapAddr( pAddr, size );

    return len;
}

int IPhyTrace::assign( chanMeta &meta,
            int size,
            int len,
            void *pOut )
{
    Q_ASSERT( NULL != pOut );
    void *pAddr = NULL;
    void *pMem  = NULL;

    pAddr = mapAddr( meta.getAddr(), size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! assign data
    quint8 *pSrc = (quint8*)pOut;
    quint8 *pDst = (quint8*)pAddr;

    Q_ASSERT(len <= size);
    memcpy( pDst, pSrc, len);

    //! unmap
    unmapAddr( pAddr, size );
    return len;
}

//! ret != size timeout occured
int IPhyTrace::writeMission(
                  quint32 type,
                  quint32 addr,
                  int size,
                  int tmo
                  )
{
    //qDebug()<<"\n------------------------"<<__FUNCTION__<<"-------------------------------";

    Q_ASSERT( size > 0 && size <= 0x400000 );   //! <4M
    Q_ASSERT( NULL != m_pZynqFcuWr );

    int ret = 0;

    const quint32 l_iLaAddr  = trace_la_main_wbase_addr;
    const quint32 l_iAnaAddr = trace_ana_main_wbase_addr;

    //! read spu history write ddr size.
    quint32 zynqAddr = addr;
    int historySize = 0;
    if( zynqAddr == l_iLaAddr)
    {
        historySize = m_pSpuGp->inIMPORT_CNT_LA();
    }
    else if(zynqAddr == l_iAnaAddr)
    {
        historySize = m_pSpuGp->inIMPORT_CNT_CH();
    }
    else
    {Q_ASSERT(false);}

    //qDebug("----historySize:%X", historySize);

    //! set addr
    m_pZynqFcuWr->outfpga_rdat_cfg4_exdat_rbaseaddr( addr );
    //qDebug("----out fpga_rdat_cfg4_exdat_rbaseaddr:%X", addr);

    //! type -- size
    m_pZynqFcuWr->setfpga_rdat_cfg1_fpga_rdat_num( size );
    m_pZynqFcuWr->setfpga_rdat_cfg1_fpga_rdat_type( type );
    m_pZynqFcuWr->flushWCache();
    //qDebug("----out fpga_rdat_cfg1_fpga_rdat num:%X , type:%d", size, type);

    //! start
    m_pZynqFcuWr->outfpga_rdat_cfg2_fpga_rdat_start(1);
    m_pZynqFcuWr->outfpga_rdat_cfg2_fpga_rdat_start(0);

    //! wait

    bool zynqDone = false;
    do
    {
         m_pZynqFcuRd->infpga_rdat_done();

         if(m_pZynqFcuRd->getfpga_rdat_done() == 1)
         {
             zynqDone = true;
         }

         if( zynqDone)
         {
             ret = size;
             break;
         }

         if(tmo <= 0)
         {
             qDebug()<<__FILE__<<__LINE__<<"zynq done failure.";
             ret = 0;
             break;
         }

         waitIdle( 10 );     //! tick 10 ms
         tmo -= 10;

    }while(1);

    //! end
    m_pZynqFcuWr->outfpga_rdat_cfg3_fpga_rdat_done_clr(1);
    m_pZynqFcuWr->outfpga_rdat_cfg3_fpga_rdat_done_clr(0);
    //qDebug("----in dbg_rd_reg_3:%X", m_pZynqFcuRd->indbg_rd_reg_3() );

#if 1
    bool         spuDone  = false;
    volatile int  wDDRSize =  0;
    do
    {
         if( zynqAddr == l_iLaAddr)
         {
             wDDRSize = m_pSpuGp->inIMPORT_CNT_LA();
         }
         else if(zynqAddr == l_iAnaAddr)
         {
             wDDRSize = m_pSpuGp->inIMPORT_CNT_CH();
         }
         else
         {Q_ASSERT(false);}

         if( ((wDDRSize - historySize) * 8) >=  size)
         {
             spuDone = true;
         }

         if(spuDone)
         {
             ret = size;
             break;
         }

         if(tmo <= 0)
         {
             qDebug()<<__FILE__<<__LINE__<<"fpga write ddr failure.";
             ret = 0;
             break;
         }

         waitIdle( 10 );     //! tick 10 ms
         tmo -= 10;

    }while(1);

//    qDebug()<<__FILE__<<__LINE__
//           <<"zynqDone:"<<zynqDone<<"spuDone:"<<spuDone
//           <<"historySize:"<<historySize<<"wDDRSize:"<<wDDRSize
//           <<"write ddr:"<<(wDDRSize - historySize)*8 << size;

//    qDebug("----in  IMPORT_INFO  :%X", m_pSpuGp->inIMPORT_INFO());

    //QThread::msleep(1000);
#endif
//    qDebug()<<"-------------------------------------------------------------------\n";

    return ret;
}

#define page_mask   0xfff
void * IPhyTrace::mapAddr( quint32 addr,
                           quint32 size,
                           void **pDst )
{
    Q_ASSERT( mBuses.size() > 1 );
    Q_ASSERT( mBuses[1] != NULL );
    Q_ASSERT( NULL != pDst );

    size_t mapSize;
    void *pAddr;
    quint32 alignAddr;
    int addrShift;

    //! shift
    addrShift = addr & page_mask;
    size += addrShift;

    //! check addr
    if ( addrShift !=0 )
    {
        alignAddr = addr & (~page_mask);
//        qWarning()<<"!!!addr not aligned"<<QString::number( addr, 16 );
    }
    else
    {
        alignAddr = addr;
    }
    //! map memory
    mapSize = (size_t)size;
    pAddr = mBuses[1]->mmap( (void*)alignAddr,
                             mapSize );

    if ( pAddr == MAP_FAILED )
    {
        LOG_PHY()<<QString::number( addr, 16 )<<mapSize<<errno;
        return NULL;
    }

    *pDst = (void*)((quint32)pAddr + addrShift );
//    LOG_DBG()<<QString::number((quint32)pAddr, 16 )<<QString::number((quint32)pDst, 16 )<<addrShift;
    return pAddr;
}

void IPhyTrace::unmapAddr( void *pAddr, quint32 size )
{
    Q_ASSERT( NULL != pAddr );

    //! unmap
    mBuses[1]->munmap( pAddr, size );
}

}
