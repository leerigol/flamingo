#ifndef TRACEREQUEST_H
#define TRACEREQUEST_H

#include "../../baseclass/dsowfm.h"

namespace dso_phy {

typedef QMap< Chan, DsoWfm *> ChanWfmMap;

class traceRequest
{
public:
    enum traceSource
    {
        trace_ch_main = 0,
        trace_la_main,
        trace_ch_zoom,
        trace_la_zoom,

        trace_cmb_main,         //! la + ch main
        trace_cmb_zoom,         //! la + ch zoom
        trace_ch_main_zoom,
        trace_la_main_zoom,

        trace_cmb_main_zoom,    //! la_m + ch_m + la_z + ch_z

        trace_eye,

        trace_export,
    };

public:
    //! by user
    ChanWfmMap mChWfms[2];      //! main + zoom
    ChanWfmMap mDxWfms[2];
    ChanWfmMap mDigiWfms;
    ChanWfmMap mEyeWfms;
    ChanWfmMap mLaWfms[2];      //! main + zoom

    //! by stack
    int mDigitalLevels[ 8 ];    //! 2 * 4 h,l,h,l,h,l,h,l
    int mDigitalLevelsL[ 8 ];   //! 2 * 4 h,l,h,l,h,l,h,l
    int mChCnt;                 //! 1,2,4
    bool mChOnOff[8];           //  more than 8 channels for DS9000

                                //! main + zoom
    int mChLengthes[2], mLaLengthes[2], mEyeLength;

    //! by combine
    bool mBinz;
    traceSource meSource;
    bool mRequestOnce;

    AcquireTimemode mAcqTimeMode;
    int  rollChOffset;
    int  rollLaOffset;
    bool mbRollTraceFull;
    int  mRequestAttrId;
public:
    traceRequest();

public:
    void setChCnt( int cnt );
    void setSource( traceSource eSrc );

    void setChLength( int len, int viewId = 0 );
    void setLaLength( int len, int viewId = 0 );
    void setEyeLength( int len );

    int getChLength( int viewId = 0 );
    int getLaLength( int viewId = 0 );
    int getEyeLength();

    int getAnaSum();

    //! la or ch
    //! viewId = 0/main, 1/zoom
    void attachChan( Chan ch, DsoWfm *pWfm, int viewId = 0 );

    //! h,l,h,l
    void setLevel( int levels[8] );

    //! level 2
    void setLevelL( int levels[8] );

    void setBinaryzation( bool b );

    void setAcquireTimeMode( AcquireTimemode acqTimeMode);
protected:
    ChanWfmMap *getChanWfmMap( Chan ch, int viewId );
};

}

#endif // TRACEREQUEST_H
