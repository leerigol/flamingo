#ifndef DATAGEN
#define DATAGEN

#define DATA_CAP  1000
#define ADC_DIV   30
class dataGen
{
private:
    int mPeri;

public:
    int scales[4];
    int offsets[4];

public:
    dataGen()
    {
        scales[0] = 1000000;
        scales[1] = 1000000;
        scales[2] = 1000000;
        scales[3] = 1000000;

        offsets[0] = 0;
        offsets[1] = 0;
        offsets[2] = 0;
        offsets[3] = 0;

        mPeri = 50;
    }

public:
    int getCap()
    {
        return DATA_CAP;
    }

    void setChScale( Chan chId, int scale )
    {
        Q_ASSERT( chId >= chan1 && chId <= chan4 );
        scales[ chId-chan1 ] = scale;
    }

    void setChOffset( Chan chId, int offset )
    {
        Q_ASSERT( chId >= chan1 && chId <= chan4 );
        offsets[ chId-chan1 ] = offset;
    }

public:

    int genData( int i, int peri, double amp, double offs )
    {
        double val;

        val = (sin( (double)(i)/peri ) * (amp) + (offs) );

        if ( val < 0 ) return 0;
        if ( val >255 ) return 255;

        return (int)val;
    }

    DsoErr getData( Chan chanId, DsoWfm &wfm )
    {
        int noiseRand;
        int noise;
        int peri;

        int yGnd;
//        float yInc;
        int point;

        yGnd = 128 + ADC_DIV*offsets[chanId-chan1]/scales[chanId-chan1];

        wfm.setRange( 0, 1000 );

        for ( int i= 0; i < DATA_CAP/2; i++ )
        {
            noiseRand = rand();
            if ( noiseRand < RAND_MAX / 5 )
            {
                noise = -2;
            }
            else if ( noiseRand <  2 * (RAND_MAX / 5) )
            {
                noise = -1;
            }
            else if ( noiseRand <  3 * (RAND_MAX / 5) )
            {
                noise = 0;
            }
            else if ( noiseRand <  4 * (RAND_MAX / 5) )
            {
                noise = 1;
            }
            else
            {
                noise = 0;
            }

            peri = mPeri*(chanId+1)/4;

            //! 300mv
            point = genData((i), peri,
                              0.3*ADC_DIV*1000000/scales[chanId-chan1],
                              yGnd )
                              + noise;
            point = qMax( 0, point );
            point = qMin( point, 255 );
            wfm[i + DATA_CAP/2] = point;

            point = genData( (i), peri,
                               -0.3*ADC_DIV*1000000/scales[chanId-chan1],
                               yGnd )
                               + noise;
            point = qMax( 0, point );
            point = qMin( point, 255 );
            wfm[ - i + DATA_CAP/2 ] = point;
        }
        wfm[0] = genData( (DATA_CAP/2), peri,
                          -0.3*ADC_DIV*1000000/scales[chanId-chan1],
                          yGnd )
                          + noise;

        //mPeri++;
        if ( mPeri > 200 )
        {
            mPeri = 5;
        }

        return ERR_NONE;
    }
};


#endif // DATAGEN

