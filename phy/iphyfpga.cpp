#include "iphyfpga.h"
#include <QtCore>
namespace dso_phy {

IPhyFpga::IPhyFpga()
{

}

void IPhyFpga::write( void *pMem, cache_addr addr )
{
    Q_ASSERT( NULL != pMem );
    Q_ASSERT( NULL != m_pBus );

    m_pBus->write( *(unsigned int*)pMem,
                   addr );

    //! wait a little time?
//    QThread::usleep(100 );
}

void IPhyFpga::read( void *pMem, cache_addr addr )
{
    Q_ASSERT( NULL != pMem );
    Q_ASSERT( NULL != m_pBus );

    m_pBus->read( *(unsigned int*) pMem,
                  addr );
}

}

