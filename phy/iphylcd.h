#ifndef IPHYLCD_H
#define IPHYLCD_H

#include "iphy.h"

namespace dso_phy {

class IPhyLcd : public IPhy
{
public:
    IPhyLcd();

public:
    void setBklight( int light );

};

}

#endif // IPHYLCD_H
