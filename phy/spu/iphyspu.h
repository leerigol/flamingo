#ifndef IPHYSPU_H
#define IPHYSPU_H

#include "../iphyfpga.h"

namespace dso_phy
{

#include "../ic/spu.h"

class IPhySpu : public SpuMem
{
public:
    enum spuFilter
    {
        filter_iir,
        filter_fir,
    };

public:
    IPhySpu( Spu_reg base = 0 );

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();

    //! override
public:
    void setWAVE_LEN_MAIN( Spu_reg value );
    void outWAVE_LEN_MAIN( Spu_reg value );
    void outWAVE_LEN_MAIN(  );

    void setWAVE_LEN_ZOOM( Spu_reg value );
    void outWAVE_LEN_ZOOM( Spu_reg value );
    void outWAVE_LEN_ZOOM(  );
public:
    //! api
    void cfgFilter( int chId,   //! 0,1,2,3
                    bool bEn,
                    spuFilter fltType,
                    const QList<qint16> &wnd );

    //! 对齐存储深度
    //! 实际的内存深度和理论的存储深度以及当前显示的点数量有关
    qlonglong alignRecordLen( qlonglong depth,
                              qlonglong viewLen );

    void setRecordLen( qlonglong len );
    void setAverageCount( quint32 avg );

    void setMain( qlonglong offs, qlonglong len );
    void setZoom( qlonglong offs, qlonglong len );

    void setCHExtractor( qint32 eH, qint64 eL );
    void setLaExtractor( qint32 eH, qint64 eL );

    void setMainCHCompress( int compressRate );
    void setMainLaCompress( int comp );

    void setZoomCHCompress( int compressRate );
    void setZoomLaCompress( int comp );

    //! ctrl
    void setSampClr();
    void setCtrl_adcsync();

    //! rpi
    void setFilterEn( bool bEn, quint32 mask );

    void setMainDelay( int chid, qint32 coarseDelay, qint32 fineDelay );
    void setZoomDelay( int chid, qint32 coarseDelay, qint32 fineDelay );

    //! get
    Spu_reg getADC_DATA_CHECK();

    //! adc average
    void startAdcAvg( quint32 avgCnt = 0x18 );
    void stopAdcAvg();

    bool getAdcAvgDone();
    void getAvg( quint32 *avgAB, quint32 *avgCD );

    //! adc pp
    //! avgCnt => time
    void startAdcPeak( quint32 avgCnt = 0x18 );
    void stopAdcPeak();

    bool getAdcPeakDone();
    void getAdcPeaks( qint32 peakAry[2*4] );

    //! la pattern
    quint32 getLaPattern( );

//    void setTX_LEN(unsigned int value);
public:
    qlonglong requestBank( qlonglong size );

public:
    void snapState();
};

class IPhySpuGp : public SpuGp
{
public:
    //! ctrl
    void setSampClr( int id = -1 );
    void setCtrl_adcsync( int id = -1 );

    void setRecordLen( qlonglong len, int id = -1 );

    void setMainCHCompress( int compressRate, int id = -1 );
    void setMainLaCompress( int comp, int id = -1 );

    void setZoomCHCompress( int compressRate, int id = -1 );
    void setZoomLaCompress( int comp, int id = -1 );

    void setCHExtractor( qint32 eH, qint64 eL, int id = -1 );
    void setLaExtractor( qint32 eH, qint64 eL, int id = -1 );

    void setTX_LEN( int type, int len, int id = -1 );
    void setTX_LEN_2( int type, int len, int id = -1 );

    //added by hxh for 750ns freeze
    void setWAVE_LEN_MAIN(Spu_reg value,int id=-1);
    void setWAVE_LEN_ZOOM(Spu_reg value,int id=-1);

public:
    qint32 getTxLen( int type );
    qint32 getTxLen2( int type );

private:
    qint32 mTxLen[4];
    qint32 mTxLen2[4];
};

}

#endif // IPHYSPU_H
