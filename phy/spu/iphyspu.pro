TEMPLATE = lib

TARGET = ../../lib$$(PLATFORM)/phy/iphyspu
CONFIG += static

# dbg show
dbg = $$(_DEBUG)
equals(dbg, _DEBUG){
DEFINES += _DEBUG
}

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

HEADERS += \
    iphyspu.h

SOURCES += \
    iphyspu.cpp

CONFIG += c++11
