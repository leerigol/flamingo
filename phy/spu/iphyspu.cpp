#include "iphyspu.h"
namespace dso_phy
{
#include "../ic/spu.cpp"

//! spu do not need align
//#undef align_8
//#define align_8( a )    (a)

IPhySpu::IPhySpu( Spu_reg base) : SpuMem( base )
{
}

void IPhySpu::init()
{
//    setSampClr();

    setCTRL_adc_sync( 0 );
    setCTRL_spu_clr( 0 );

    _outOtp();
}

void IPhySpu::loadOtp()
{
    _loadOtp();
}
void IPhySpu::reMap()
{
    _remap_Spu_();
}

void IPhySpu::setWAVE_LEN_MAIN( Spu_reg value )
{
    SpuMem::setWAVE_LEN_MAIN( align_8(value) );
}
void IPhySpu::outWAVE_LEN_MAIN( Spu_reg value )
{
    SpuMem::outWAVE_LEN_MAIN( align_8(value) );
}
void IPhySpu::outWAVE_LEN_MAIN(  )
{
    SpuMem::outWAVE_LEN_MAIN( );
}
void IPhySpu::setWAVE_LEN_ZOOM( Spu_reg value )
{
    SpuMem::setWAVE_LEN_ZOOM( align_8(value) );
}
void IPhySpu::outWAVE_LEN_ZOOM( Spu_reg value )
{
    SpuMem::outWAVE_LEN_ZOOM( align_8(value) );
}
void IPhySpu::outWAVE_LEN_ZOOM(  )
{
    SpuMem::outWAVE_LEN_ZOOM( );
}
void IPhySpu::cfgFilter(
                int chId, //! 0,1,2,3
                bool bEn,
                spuFilter fltType,
                const QList<qint16> &wnd )
{
    Q_UNUSED(fltType);
    Spu_PRE_PROC preProc;
    Spu_reg reg;

    //! save the pre
    preProc = PRE_PROC;

    if ( bEn )
    {
        //! disable
        reg = preProc.proc_en;
        unset_bit( reg, chId );

        setPRE_PROC_proc_en( reg );
        setPRE_PROC_cfg_filter_en( 1 );
        flushWCache();

        //! must be an integer
        Q_ASSERT( wnd.size() > 0 );
        LOG_DBG()<<chId<<wnd.size();

        setFILTER_coeff_ch( (1<<chId) );
        //! now for the coeff
        int i;
        //! 64 + 1024 :分两部分向下进行配置
        for ( i = 0; i < 64; i++ )
        {
            setFILTER_coeff_sel(0);
            setFILTER_coeff( (short)wnd[i] );
            setFILTER_coeff_id( i );
            flushWCache();
        }
        for ( i = 64; i < wnd.size(); i++ )
        {
            setFILTER_coeff_sel(1);
            setFILTER_coeff( (short)wnd[i] );
            setFILTER_coeff_id( i - 64);
            flushWCache();
        }

        //! turn off the cfg channel
        setPRE_PROC_cfg_filter_en( 0 );
        flushWCache();

        //! en the proc now
        preProc.proc_en = preProc.proc_en | (1<<chId);
        setPRE_PROC_proc_en( preProc.proc_en );
    }
    else
    {
        //! 将fir使能关闭的同时还需要将系数全部配置为0
        //! disable
//        reg = preProc.proc_en;
//        unset_bit( reg, chId );

//        flushWCache();

//        setFILTER_coeff_ch( (1<<chId) );
//        //! now for the coeff
//        int i;
//        //! 64 + 1024 :分两部分向下进行配置
//        for ( i = 0; i < 64; i++ )
//        {
//            setFILTER_coeff_sel(0);
//            setFILTER_coeff( 0 );
//            setFILTER_coeff_id( i );
//            flushWCache();
//        }
//        for ( i = 0; i < 1024; i++ )
//        {
//            setFILTER_coeff_sel(1);
//            setFILTER_coeff( 0 );
//            setFILTER_coeff_id( i - 64);
//            flushWCache();
//        }

//        //! turn off the cfg channel
//        setPRE_PROC_cfg_filter_en( 0 );
//        flushWCache();

        preProc.proc_en = preProc.proc_en & (~(1<<chId));
        setPRE_PROC_proc_en( preProc.proc_en );
    }
}

qlonglong IPhySpu::alignRecordLen( qlonglong depth,
                          qlonglong viewLen )
{
    qlonglong alignViewLen;
    qlonglong rawMemLen;

    alignViewLen = ((viewLen + 127)/128)*128;

    alignViewLen += 1024;

    if ( alignViewLen < 2048 )
    {
        rawMemLen = 2048;
    }
    else if ( depth < alignViewLen )
    {
        rawMemLen = alignViewLen;
    }
    else
    {
        rawMemLen = depth;
    }

    //! align the record len
    qlonglong alignRecordLen;

    alignRecordLen = ((rawMemLen + 127)/128)*128;

    return alignRecordLen;
}

void IPhySpu::setRecordLen( qlonglong len )
{
    SpuMem::setRECORD_LEN( len );
}

//! Yn = Xn/N + (1-1/N)Yn-1
//!
void IPhySpu::setAverageCount( quint32 avg )
{
    Q_ASSERT( avg >= 2 && avg <= 65536 );

    quint32 c1, c2;

    c1 = 0x10000*1/avg;
    c2 = 0x10000-c1;

    setTRACE_AVG_RATE_X( c1 );
    setTRACE_AVG_RATE_Y( c2 );
}

void IPhySpu::setMain( qlonglong offs, qlonglong len )
{
//    reg_assign_field( WAVE_OFFSET_MAIN, offset,offs );
    reg_assign( WAVE_OFFSET_MAIN, offs );
    reg_assign( WAVE_LEN_MAIN, len );
}
void IPhySpu::setZoom( qlonglong offs, qlonglong len )
{
//    reg_assign_field( WAVE_OFFSET_ZOOM, offset, offs );
    reg_assign( WAVE_OFFSET_ZOOM, offs );
    reg_assign( WAVE_LEN_ZOOM, len );
}

//! eh, el
void IPhySpu::setCHExtractor( qint32 eh, qint64 el )
{
    reg_assign_field( DOWN_SAMP_FAST, down_samp_fast, eh );
    reg_assign_field( DOWN_SAMP_SLOW_H, down_samp_slow_h, (el >> 32)&0xff );
    reg_assign( DOWN_SAMP_SLOW_L, el & 0xffffffff );
}
void IPhySpu::setLaExtractor( qint32 /*el*/, qint64 /*eh*/ )
{
//    spu_DOWN_SAMP downSamp;
//    downSamp.down_samp_h = el;
//    downSamp.down_samp_l = eh;

//    reg_assign( DOWN_SAMP_LA, downSamp.payload );
}

void IPhySpu::setMainCHCompress( int compressRate )
{
    Q_ASSERT( compressRate > 0 );
    if( compressRate >= 0x10000000 )
    {
        compressRate = 0x10000000;
    }

    Q_ASSERT( compressRate < 0x10000000 );   //! 28 bits only

    if ( compressRate == 1 )
    {
        reg_assign_field( COMPRESS_MAIN, en, 0 );
    }
    else
    {
        reg_assign_field( COMPRESS_MAIN, en, 1 );
        reg_assign_field( COMPRESS_MAIN, rate, compressRate );
    }
}
void IPhySpu::setMainLaCompress( int comp )
{
    Q_ASSERT( comp > 0 );
    Q_ASSERT( comp < 0x10000000 );   //! 26 bits only

    if ( comp == 1 )
    {
        reg_assign_field( LA_COMP_MAIN, en, 0 );
    }
    else
    {
        reg_assign_field( LA_COMP_MAIN, en, 1 );
        reg_assign_field( LA_COMP_MAIN, rate, comp );
    }
}

void IPhySpu::setZoomCHCompress( int compressRate )
{
    Q_ASSERT( compressRate > 0 );
    Q_ASSERT( compressRate < 0x10000000 );   //! 28 bits only

    if ( compressRate == 1 )
    {
        reg_assign_field( COMPRESS_ZOOM, en, 0 );
    }
    else
    {
        reg_assign_field( COMPRESS_ZOOM, en, 1 );
        reg_assign_field( COMPRESS_ZOOM, rate, compressRate );
    }
}
void IPhySpu::setZoomLaCompress( int comp )
{
    Q_ASSERT( comp > 0 );
    Q_ASSERT( comp < 0x10000000 );   //! 26 bits only

    if ( comp == 1 )
    {
        reg_assign_field( LA_COMP_ZOOM, en, 0 );
    }
    else
    {
        reg_assign_field( LA_COMP_ZOOM, en, 1 );
        reg_assign_field( LA_COMP_ZOOM, rate, comp );
    }
}

void IPhySpu::setSampClr()
{
    reg_assign_field( CTRL, adc_sync, 0 );
    pulseCTRL_spu_clr( 1  );
}

void IPhySpu::setCtrl_adcsync()
{
    //! first time
    outCTRL_adc_sync( 1 );
    outCTRL_adc_sync( 0 );

    //! second time
    outCTRL_adc_sync( 1 );
    outCTRL_adc_sync( 0 );
    QThread::usleep(1);
}

void IPhySpu::setFilterEn( bool bEn, quint32 mask )
{
    Spu_reg reg = PRE_PROC.proc_en;

    if ( bEn )
    {
        reg |= mask;
    }
    else
    {
        reg &= ~mask;
    }

    setPRE_PROC_proc_en( reg );
}

void IPhySpu::setMainDelay( int chid, qint32 coarseDelay, qint32 fineDelay )
{
    //! coarse delay
//    if ( coarseDelay > 0 )
    {
        setCOARSE_DELAY_MAIN_chn_ce( 1<<chid );
        setCOARSE_DELAY_MAIN_chn_delay( coarseDelay );
    }

    //! fine delay
//    if ( fineDelay > 0 )
    {
        setFINE_DELAY_MAIN_chn_ce( 1<<chid );
        setFINE_DELAY_MAIN_chn_delay( fineDelay );
    }

}
void IPhySpu::setZoomDelay( int chid, qint32 coarseDelay, qint32 fineDelay )
{
    //! coarse delay
//    if ( coarseDelay > 0 )
    {
        setCOARSE_DELAY_ZOOM_chn_ce( 1<<chid );
        setCOARSE_DELAY_ZOOM_chn_delay( coarseDelay );
    }

    //! fine delay
//    if ( fineDelay > 0 )
    {
        setFINE_DELAY_ZOOM_chn_ce( 1<<chid );
        setFINE_DELAY_ZOOM_chn_delay( fineDelay );
    }
}

//! get
Spu_reg IPhySpu::getADC_DATA_CHECK()
{
    in( &ADC_DATA_CHECK );

    return ADC_DATA_CHECK.payload;
}

void IPhySpu::startAdcAvg( quint32 avgCnt )
{
    setADC_AVG_en( 0 );
    out( &ADC_AVG );

    setADC_AVG_en( 1 );
    setADC_AVG_rate( avgCnt );
}
void IPhySpu::stopAdcAvg()
{
    setADC_AVG_en( 0 );
}

bool IPhySpu::getAdcAvgDone()
{
    inADC_AVG();

    return ADC_AVG.done;
}
void IPhySpu::getAvg( quint32 *avgAB, quint32 *avgCD )
{
    Q_ASSERT( NULL != avgAB );
    Q_ASSERT( NULL != avgCD );

    inADC_AVG_RES_AB();
    inADC_AVG_RES_CD();

    *avgAB = ADC_AVG_RES_AB.payload;
    *avgCD = ADC_AVG_RES_CD.payload;
}

void IPhySpu::startAdcPeak( quint32 avgCnt )
{
    //outCTRL_spu_clr(1);
    outADC_AVG_en( 0 );

    setADC_AVG_en( 1 );
    setADC_AVG_rate( avgCnt );
}
void IPhySpu::stopAdcPeak()
{
    outADC_AVG_en( 0 );
}

bool IPhySpu::getAdcPeakDone()
{
    inADC_AVG();

    return ADC_AVG.done;
}
void IPhySpu::getAdcPeaks( qint32 peakAry[2*4] )
{
    inADC_AVG_MAX();
    inADC_AVG_MIN();

    //! a
    peakAry[0] = ADC_AVG_MAX.max_A;
    peakAry[1] = ADC_AVG_MIN.min_A;

    //! b
    peakAry[2] = ADC_AVG_MAX.max_B;
    peakAry[3] = ADC_AVG_MIN.min_B;

    //! c
    peakAry[4] = ADC_AVG_MAX.max_C;
    peakAry[5] = ADC_AVG_MIN.min_C;

    //! d
    peakAry[6] = ADC_AVG_MAX.max_D;
    peakAry[7] = ADC_AVG_MIN.min_D;
}

quint32 IPhySpu::getLaPattern(  )
{

    inLA_SA_RES();

    return getLA_SA_RES_la();
}

//void IPhySpu::setTX_LEN(unsigned int value)
//{
//    //! workaround align to 8 bytes
//    Spu_reg len = ((value + 7)/8)*8;

//    SpuMem::setTX_LEN( len );
//}

qlonglong IPhySpu::requestBank( qlonglong size )
{
    qlonglong alignSize;

    alignSize = ( (size + 1023) / 1024 ) * 1024;

    Q_ASSERT( alignSize < 512 * 1024 * 1024 );

    return alignSize;
}

//! state spy
struct sysStat
{
    const char *name;
    quint32 addr;
};
sysStat _states[]
{
    {"scu_state", 0x1c04 },
    {"spu_tx_len", 0x149c },
    {"spu_debug1", 0x14c0 },
    {"spu_debug2", 0x14c4 },

    {"spu_debug3", 0x14c8 },
    {"spu_debug4", 0x14cc },
    {"spu_debug5", 0x14d0 },
    {"spu_debug6", 0x14d4 },

    {"spu_debug7", 0x14d8 },
    {"spu_debug8", 0x14dc },

    {0,0},      //! end
};

void IPhySpu::snapState()
{
#if 1
    quint32 stat;
    int i = 0;
    LOG_PHY()<<"-----";
    while( _states[i].name != 0 )
    {
        m_pBus->read( stat, _states[i].addr );
        LOG_PHY()<<_states[i].name<<stat<<QString::number(stat, 16);
        i++;
    }
    LOG_PHY()<<"-----";
#endif
}

void IPhySpuGp::setSampClr( int id )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setSampClr();
        }
    }
    else
    {
        mSubItems[id]->setSampClr();
    }
}
void IPhySpuGp::setCtrl_adcsync( int id )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setCtrl_adcsync();
        }
    }
    else
    {
        mSubItems[id]->setCtrl_adcsync();
    }
}

void IPhySpuGp::setRecordLen( qlonglong len, int id )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setRecordLen( len );
        }
    }
    else
    {
        mSubItems[id]->setRecordLen( len );
    }
}

void IPhySpuGp::setMainCHCompress( int compressRate, int id  )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setMainCHCompress( compressRate );
        }
    }
    else
    {
        mSubItems[id]->setMainCHCompress( compressRate );
    }
}

void IPhySpuGp::setMainLaCompress( int comp, int id  )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setMainLaCompress( comp );
        }
    }
    else
    {
        mSubItems[id]->setMainLaCompress( comp );
    }
}

void IPhySpuGp::setZoomCHCompress( int compressRate, int id  )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setZoomCHCompress( compressRate );
        }
    }
    else
    {
        mSubItems[id]->setZoomCHCompress( compressRate );
    }
}

void IPhySpuGp::setZoomLaCompress( int comp, int id  )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setZoomLaCompress( comp );
        }
    }
    else
    {
        mSubItems[id]->setZoomLaCompress( comp );
    }
}

void IPhySpuGp::setCHExtractor( qint32 eH, qint64 eL, int id )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setCHExtractor( eH, eL );
        }
    }
    else
    {
        mSubItems[id]->setCHExtractor( eH, eL );
    }
}
void IPhySpuGp::setLaExtractor( qint32 eH, qint64 eL, int id )
{
    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            pItem->setLaExtractor( eH, eL );
        }
    }
    else
    {
        mSubItems[id]->setLaExtractor( eH, eL );
    }
}

//! 1116 TX_LEN
//void IPhySpuGp::setTX_LEN( int type, int len, int id  )
//{
//    //! save tx len
//    Q_ASSERT( type >= 0 && type < array_count(mTxLen) );
//    mTxLen[ type ] = len;

//    //! \errant len != 0
//    if ( len < 1)
//    { len = 1; }

//    if ( id == -1 )
//    {
//        foreach( IPhySpu *pItem, mSubItems )
//        {
//            Q_ASSERT( NULL != pItem );
//            pItem->setTX_LEN_type( type );
//            pItem->setTX_LEN_len( align_8(len) );
//            pItem->outTX_LEN();
//        }
//    }
//    else
//    {
//        mSubItems[id]->setTX_LEN_type( type );
//        mSubItems[id]->setTX_LEN_len( align_8(len) );
//        mSubItems[id]->outTX_LEN();
//    }
//}

///
/// \brief for bug 750ns
/// \param value
/// \param id
///
void IPhySpuGp::setWAVE_LEN_MAIN(unsigned int value, int id)
{
    value += 64;
    SpuGp::setWAVE_LEN_MAIN(value,id);
}

void IPhySpuGp::setWAVE_LEN_ZOOM(unsigned int value, int id)
{
    value += 64;
    SpuGp::setWAVE_LEN_ZOOM(value,id);
}

void IPhySpuGp::setTX_LEN( int type, int len, int id  )
{
    //! save tx len
    Q_ASSERT( type >= 0 && type < array_count(mTxLen) );
    mTxLen[ type ] = len;

    //! \errant len != 0
    if ( len < 1)
    { len = 1; }

    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            switch (type) {
            case 0:
                pItem->outTX_LEN_CH( align_8(len) );
                break;
            case 1:
                pItem->outTX_LEN_LA( align_8(len) );
                break;
            case 2:
                pItem->outTX_LEN_ZOOM_CH( align_8(len) );
                break;
            case 3:
                pItem->outTX_LEN_ZOOM_LA( align_8(len) );
                break;
            default:
                Q_ASSERT(false);
            }
        }
    }
    else
    {
        switch (type) {
        case 0:
            mSubItems[id]->outTX_LEN_CH( align_8(len) );
            break;
        case 1:
            mSubItems[id]->outTX_LEN_LA( align_8(len) );
            break;
        case 2:
            mSubItems[id]->outTX_LEN_ZOOM_CH( align_8(len) );
            break;
        case 3:
            mSubItems[id]->outTX_LEN_ZOOM_LA( align_8(len) );
            break;
        default:
            Q_ASSERT(false);
        }
    }
}

void IPhySpuGp::setTX_LEN_2( int type, int len, int id  )
{
    //! save tx2
    Q_ASSERT( type >= 0 && type < array_count(mTxLen2) );
    mTxLen2[ type ] = len;

    //! \errant len != 0
    if ( len < 1)
    { len = 1; }

    if ( id == -1 )
    {
        foreach( IPhySpu *pItem, mSubItems )
        {
            Q_ASSERT( NULL != pItem );
            switch ( type ) {
            case 0:
                pItem->outTX_LEN_EYE( align_8(len) );
                break;
            case 1:
                pItem->outTX_LEN_CH( align_8(len) );//! 导出模拟
                pItem->outTX_LEN_LA( align_8(len) );//! 导出La
                break;
            default:
                Q_ASSERT(false);
                break;
            }
        }
    }
    else
    {
        switch (type) {
        case 0:
            mSubItems[id]->outTX_LEN_EYE( align_8(len) );
            break;
        case 1:
            mSubItems[id]->outTX_LEN_TRACE_CH( align_8(len) );//! 导出模拟
            mSubItems[id]->outTX_LEN_TRACE_LA( align_8(len) );//! 导出La
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }
}

qint32 IPhySpuGp::getTxLen( int type )
{
    Q_ASSERT( type >= 0 && type < array_count(mTxLen) );
    return mTxLen[ type ];
}

qint32 IPhySpuGp::getTxLen2( int type )
{
    Q_ASSERT( type >= 0 && type < array_count(mTxLen2) );
    return mTxLen2[ type ];
}

}

