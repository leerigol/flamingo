#ifndef IPHYMEASCFG_H
#define IPHYMEASCFG_H

#include "../iphyfpga.h"

namespace dso_phy {

#include "../ic/meascfgu.h"

class IPhyMeasCfgu : public MeasCfguMem
{
public:
    IPhyMeasCfgu();

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();
};

}

#endif // IPHYMEASCFG_H
