#include "iphymeasret.h"

namespace dso_phy {

#include "../ic/measretu.cpp"

horiMeta::horiMeta()
{
    mOrigin = 0;
    mLength = 0;

    mTOrigin.set( 0 );
    mTInc.set( 0 );
}

void horiMeta::setMeta( qlonglong ori,
              qlonglong len,
              const dsoFract &tOri,
              const dsoFract &tInc
              )
{
    Q_ASSERT( ori >= 0 && len > 0 && tInc > 0 );

    mOrigin = ori;
    mLength = len;

    mTOrigin = tOri;
    mTInc = tInc;
}

vertMeta::vertMeta()
{
    myGnd = 0;
    myInc.set( 0 );
}

void vertMeta::setMeta( qint32 gnd,
              qint32 scale, qint32 div )
{
    myGnd = gnd;
    myInc.set( scale, div );
}

measMeta::measMeta()
{}

void measMeta::setHMeta( const horiMeta &hMeta )
{
    mHMeta = hMeta;
}

void measMeta::setHMeta( Chan ch, const horiMeta &hMeta )
{
    if ( ana == ch )
    { mHMetas[0] = hMeta; }
    else if ( la == ch )
    { mHMetas[1] = hMeta; }
    else
    { Q_ASSERT( false ); }
}
void measMeta::setVMeta( int index, const vertMeta &vMeta )
{
    Q_ASSERT( index >= 0 && index < array_count( mVMetas ) );

    mVMetas[ index ] = vMeta;
}

IPhyMeasRetu::IPhyMeasRetu()
{

}
void IPhyMeasRetu::init()
{
}

void IPhyMeasRetu::loadOtp()
{
    _loadOtp();
}
void IPhyMeasRetu::reMap()
{
    _remap_MeasRetu_();
}


}

