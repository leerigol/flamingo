#ifndef IPHYMEASRET_H
#define IPHYMEASRET_H

#include "../../baseclass/dsofract.h"
#include "../iphyfpga.h"

namespace dso_phy {

#include "../ic/measretu.h"

class horiMeta
{
public:
    horiMeta();
public:
    void setMeta( qlonglong ori,
                  qlonglong len,
                  const dsoFract &tOri,
                  const dsoFract &tInc
                  );
public:
    qlonglong mOrigin, mLength;
    dsoFract mTOrigin, mTInc;
};

class vertMeta
{
public:
    vertMeta();

public:
    void setMeta( qint32 gnd,
                  qint32 scale, qint32 div = 25 );
public:
    qint32 myGnd;
    dsoFract myInc;
};

class measMeta
{
public:
    measMeta();

public:
    void setHMeta( const horiMeta &hMeta );
    //! only ana, la
    void setHMeta( Chan ch, const horiMeta &hMeta );
    void setVMeta( int index, const vertMeta &vMeta );

public:
    horiMeta mHMeta;         //! ch meta

    horiMeta mHMetas[2];     //! 0 -- ch, 1 -- la


    vertMeta mVMetas[4 ];    //! ch1/2/3/4
};

class IPhyMeasRetu : public MeasRetuMem
{
public:
    IPhyMeasRetu();
public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();
public:
    measMeta mMeta;
};

}

#endif // IPHYMEASRET_H
