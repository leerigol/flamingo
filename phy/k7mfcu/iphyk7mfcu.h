#ifndef IPHYK7MFCU_H
#define IPHYK7MFCU_H

#include "../iphyfpga.h"

namespace dso_phy
{

#include "../ic/k7mfcu.h"

class IPhyK7mFcu : public K7mFcuMem
{
public:
    IPhyK7mFcu();

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();
};

}

#endif // IPHYK7MFCU_H
