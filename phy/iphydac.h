#ifndef IPHYDAC_H
#define IPHYDAC_H

#include "iphyfpga.h"
namespace dso_phy {

typedef unsigned short dac_reg;
struct dacReg
{
    dac_reg reg;
};

#define dac_chn_time_base   100 //! ns

class IPhyDac : public IPhyFpga
{
public:
    IPhyDac();

    //! 0~15
    void set( dac_reg dac,
              int chan );
    dac_reg get( int chan );
    dac_reg inReg( int chan );

    //! time in ns
    void setDacChnTime( int time );


private:
    unsigned int mBaseAddr;

    dacReg mReg[15];
    int mChnTime;
};

}

#endif // IPHYDAC_H
