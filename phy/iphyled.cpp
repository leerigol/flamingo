#include "iphyled.h"

#include "../com/keyevent/ckeyscan.h"

namespace dso_phy {

IPhyLed::IPhyLed()
{
    mLed = 0;
}

void IPhyLed::seLedOp( int led, bool b )
{
    if ( b )
    { set_bit(mLed, led); }
    else
    { unset_bit(mLed, led); }

    app_key_board::CKeyScan::ledOp( led, b );
}
int IPhyLed::getLed()
{
    return mLed;
}

}

