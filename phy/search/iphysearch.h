#ifndef IPHYSEARCH_H
#define IPHYSEARCH_H

#include "../iphyfpga.h"

namespace dso_phy {

#include "../trig/iphytpu_core.h"

class IPhySearch : public IPhyTpu
{
public:
    IPhySearch();

public:
    virtual quint64   get_c_tick();
    void              set_c_tick(quint64 tick);

    virtual quint64   get_td_tick();
    void              set_td_tick(quint64 tick);

public:
    int assign( void *pOut,
                int size,
                int len,
                quint32 addr );

    int assign( quint32 addr,
                int size,
                int len,
                void *pOut );
public:
    //! mapAddr
    void* mapAddr( quint32 addr, quint32 size, void **pDst );
    void  unmapAddr(void *pAddr, quint32 size );

private:
    quint64 m_tick;  //! ps
    quint64 m_td_tick;  //! ps
};


}

#endif // IPHYDTU_H
