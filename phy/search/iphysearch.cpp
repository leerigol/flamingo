#include "iphysearch.h"
#include <errno.h>
namespace dso_phy {

IPhySearch::IPhySearch()
{
    m_tick = 1;
}

quint64 IPhySearch::get_c_tick()
{
//    qDebug()<<__FILE__<<__LINE__<<"TICK:"<<m_tick;
    return m_tick ;  //! ps
}

void IPhySearch::set_c_tick(quint64 tick)
{
    Q_ASSERT(tick > 0);
    m_tick = tick;
}

quint64 IPhySearch::get_td_tick()
{
    return m_td_tick;
}

void IPhySearch::set_td_tick(quint64 tick)
{
    m_td_tick = tick;
}

int IPhySearch::assign(void *pOut, int size, int len, quint32 addr)
{
    Q_ASSERT( NULL != pOut );

    void *pAddr, *pMem;

    pAddr = mapAddr( addr, size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! read data
    memcpy( pOut, pMem, len );

    //! unmap
    unmapAddr( pAddr, size );

    return len;
}

int IPhySearch::assign(quint32 addr, int size, int len, void *pOut)
{
    Q_ASSERT( NULL != pOut );
    void *pAddr, *pMem;

    pAddr = mapAddr( addr, size, &pMem );
    if ( NULL == pAddr )
    { LOG_DBG(); return -1; }

    //! assign data
    memcpy( pMem, pOut, len );

    //! unmap
    unmapAddr( pAddr, size );

    return len;
}

#define page_mask   0xfff
void *IPhySearch::mapAddr(quint32 addr, quint32 size, void **pDst)
{
    Q_ASSERT( mBuses.size() > 1 );
    Q_ASSERT( mBuses[1] != NULL );
    Q_ASSERT( NULL != pDst );

    size_t mapSize;
    void *pAddr;
    quint32 alignAddr;
    int addrShift;

    //! shift
    addrShift = addr & page_mask;
    size += addrShift;

    //! check addr
    if ( addrShift !=0 )
    {
        alignAddr = addr & (~page_mask);
        qWarning()<<"!!!addr not aligned"<<QString::number( addr, 16 );
    }
    else
    {
        alignAddr = addr;
    }

    //! map memory
    mapSize = (size_t)size;
    pAddr = mBuses[1]->mmap( (void*)alignAddr, mapSize);

    if ( pAddr == MAP_FAILED )
    {
        LOG_PHY()<<QString::number( addr, 16 )<<mapSize<<errno;
        return NULL;
    }

    *pDst = (void*)((quint32)pAddr + addrShift );
    //LOG_DBG()<<QString::number((quint32)pAddr, 16 )<<QString::number((quint32)pDst, 16 )<<addrShift;
    return pAddr;
}

void IPhySearch::unmapAddr(void *pAddr, quint32 size)
{
    Q_ASSERT( NULL != pAddr );

    //! unmap
    mBuses[1]->munmap( pAddr, size );
}

}

