#include "iphypll.h"

namespace dso_phy {

#define PLL_CHAN        1

/*! 5G时钟的配置序列
*/
static pllCfg _5GSequence[]=
{
    {0x40,0xff},
    {0x30,0x3fd},
    {0x2f,0x8cf},
    {0x2e,0xf20},

    {0x2d,0x0},
    {0x2c,0x0},
    {0x2b,0x0},
    {0x2a,0x0},

    {0x29,0x3e8},
    {0x28,0x0},
    {0x27,0x8104},
    {0x26,0x1f4},

    {0x25,0x4000},
    {0x24,0x0},
    {0x23,0x1d},
    {0x22,0xc3d0},

    {0x21,0x4210},
    {0x20,0x4210},
    {0x1f,0x81},
    {0x1e,0x34},

    {0x1d,0x84},
    {0x1c,0x2924},
    {0x18,0x509},
    {0x17,0x8842},

    {0x13,0x965},
    {0x0e,0xffc},
    {0x0d,0x4000},
    {0x0c,0x7001},

    {0x0b,0x18},
    {0x0a,0x10d8},
    {0x9,0x302},
    {0x8,0x1084},

    {0x7,0x28b2},
    {0x1,0x808},
    {0x0,0x223c},
};

static pllCfg _1_1GSequence[]=
{
    {0x40,0x03FF},
    {0x30,0x03FC},
    {0x2F,0x00C1},
    {0x2E,0x3FA3},
    //{0x2E,0x0FA3},
    //{0x2E,0x08A3},
    {0x2D,0x0000},
    {0x2C,0x0000},
    {0x2B,0x0000},
    {0x2A,0x0000},
    {0x29,0x03E8},
    {0x28,0x0000},
    {0x27,0x8204},
    {0x26,0x00DC},
    {0x25,0x4000},
    {0x24,0x0420},
    {0x23,0x029B},
    {0x22,0xC3F0},
    {0x21,0x4210},
    {0x20,0x4210},
    {0x1F,0x0401},
    {0x1E,0x0034},
    {0x1D,0x0084},
    {0x1C,0x2924},
    {0x18,0x0509},
    {0x17,0x8842},
    {0x13,0x0965},
    {0x0E,0x0420},
    {0x0D,0x4000},
    {0x0C,0x7001},
    {0x0B,0x0018},
    {0x0A,0x10D8},
    {0x09,0x0B02},
    {0x08,0x1084},
    {0x07,0x28B2},
    {0x01,0x0808},
    {0x00,0x221C},
};

IPhyPLL::IPhyPLL()
{
    m_pDac = NULL;
    mDacChan = 0;
}

void IPhyPLL::init()
{
//    output5G();
}

void IPhyPLL::output5G()
{
    unsigned short busVal;

    //! disable 4.4G
    busVal = 0<<5;
    toLE( busVal );
    m_pBus->write( busVal, 0x01, 0 );

    //! selsect 5G
    busVal = 1<<6;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    //! 5g sequence
    for ( int i = 0; i < array_count(_5GSequence); i++ )
    {
        busVal = _5GSequence[i].regData;
        toLE( busVal );
        m_pBus->write(
                       busVal,
                       _5GSequence[i].regAddr,
                       PLL_CHAN
                       );
    }

    //! de selsect 5G
    busVal = 0<<6;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
}

void IPhyPLL::output1_1G( bool bOnOff )
{
    unsigned short busVal;

    if ( bOnOff )
    {
        //! enable 1.1g
        busVal = (1<<2) | (1<<5) | (1<<7);
        toLE( busVal );
        m_pBus->write( busVal, 0x01, 0 );

        //! selsect 1.1g
        busVal = 1<<7;
        toLE( busVal );
        m_pBus->write( busVal, 0x04, 0 );

        //! 1.1g sequence
        for ( int i = 0; i < array_count(_1_1GSequence); i++ )
        {
            busVal = _1_1GSequence[i].regData;
            toLE( busVal );
            m_pBus->write(
                           busVal,
                           _1_1GSequence[i].regAddr,
                           PLL_CHAN
                           );
        }

        LOG_PHY();
    }
    else
    {
        //! disable 1.1g
        busVal = (1<<7);
        toLE( busVal );
        m_pBus->write( busVal, 0x01, 0 );

        //! selsect 1.1g
        busVal = 1<<7;
        toLE( busVal );
        m_pBus->write( busVal, 0x04, 0 );

        busVal = 0x01;  //! pwr down
        toLE( busVal );
        m_pBus->write(
                       busVal,
                       0,
                       PLL_CHAN
                       );

        LOG_PHY();
    }

    //! de selsect 1.1g
    busVal = 0<<7;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
}

void IPhyPLL::attachDac( IPhyDac *pDac, int dacChan )
{
    Q_ASSERT( NULL != pDac );

    m_pDac = pDac;
    mDacChan = dacChan;
}
void IPhyPLL::tuneClock( int clock )
{
    Q_ASSERT( NULL != m_pDac );

    m_pDac->set( clock, mDacChan );
}

void IPhyPLL::write5g( quint16 to, quint16 data )
{
    quint16 busVal;

    //! selsect 5G
    busVal = 1<<6;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    toLE( data );
    m_pBus->write( data, to, PLL_CHAN );

    //! de selsect 5G
    busVal = 0<<6;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
}
void IPhyPLL::write1_1g( quint16 to, quint16 data )
{
    quint16 busVal;

    //! selsect 1.1g
    busVal = 1<<7;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    toLE( data );
    m_pBus->write( data, to, PLL_CHAN );

    //! de selsect 1.1g
    busVal = 0<<7;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
}


}

