#include "iphyafe.h"

#ifdef DAMREY_VER_1_5_x

namespace dso_phy {

#define AFE_CHAN    1

IPhyAfe::IPhyAfe( int id )
{
    setId( id );
}

void IPhyAfe::init()
{
    r0 = 0x000C;    //! RMS_GAIN=12, 5V
    r1 = 0xf000;    //! R_SEL=7, C_SEL=0, full band
    r2 = 0x1023;
    //r2 = 0xA023;    //dsel=1
    r3 = 0x057f;    //! DC_TRIM 127

    r4 = 0x2f34;
    r5 = 0x790f;
    r6 = 0x7bde;
    r7 = 0x7502;

    r8 = 0xbfe0;    //! k[5] = 1
    r9 = 0x1f93;
    r10 = 0x0032;
    r11 = 0x2900;

    r12 = 0xff27;
    r13 = 0xffff;
    r14 = 0x00e1;

    /* ds8000: 3  ds7000: 1
     *             -----ADC1
     * signal-----|
     *             -----ADC2
     */
    setOen( 3 );
    //  setDacSel( 1 );
    //  setEnDclp( 1 );

    //! cctrim
    setCcTrim1( 5 );
    setCcTrim2( 0 );
    setCcTrim3( 0 );

    //! dc trim
    //! 70k
    setDcTrim( 161 );

    //! for all reg
    appendWCache( 0, &r0 );
    appendWCache( 1, &r1 );
    appendWCache( 2, &r2 );
    appendWCache( 3, &r3 );

    appendWCache( 4, &r4 );
    appendWCache( 5, &r5 );
    appendWCache( 6, &r6 );
    appendWCache( 7, &r7 );

    appendWCache( 8, &r8 );
    appendWCache( 9, &r9 );
    appendWCache( 10, &r10 );
    appendWCache( 11, &r11 );

    appendWCache( 12, &r12 );
    appendWCache( 13, &r13 );
    appendWCache( 14, &r14 );

    flushWCache();
}

void IPhyAfe::flushWCache()
{
    //! write all
    regCache *pItem;

    unsigned short busVal;
    unsigned int packVal;

    if ( mwCache.size() < 1 ) return;

    IPhy::lockWCache();

    busVal = 1 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
    foreach( pItem, mwCache )
    {
        Q_ASSERT( NULL != pItem );
        Q_ASSERT( NULL != pItem->m_pMem );

        busVal = *(unsigned short*)pItem->m_pMem;
        packVal = pack24AddrData( pItem->mAddr,
                                  busVal );

        m_pBus->send( &packVal, 3, AFE_CHAN );
    }

    busVal = 0 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    IPhy::unlockWCache();

    //! clear all
    IPhy::flushWCache();
}

void IPhyAfe::setId( int id )
{

    Q_ASSERT( id >= 0 && id <=3);

    mId = id;

    //! set cs
    mCs = mId;
}

//! reg0
void IPhyAfe::setRmsGain( damrey_reg gain )
{
    rs0.rms_gain = gain;
}

/**
 * @brief IPhyAfe::setAtten
 * @param att
 *
 * 高阻通路衰减器设置
 * 0：x1
   1: x0.8
   2: x0.63
   3: x0.5
   4: x0.4
   5: x0.316
   6: 输出悬空
   7：输出短路
 */
void IPhyAfe::setAtten( damrey_reg att )
{
    rs0.atten = att;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setProg( damrey_reg prog )
{
    rs0.prog = prog;
    appendWCache( 0, &rs0 );
}


//! read
damrey_reg IPhyAfe::getOvDet()
{
    readReg( (r_r0), 0, AFE_CHAN );
    return r_rs0.ov_det;
}
damrey_reg IPhyAfe::getZDet()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.z_det;
}
damrey_reg IPhyAfe::getTp2ZDet()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.tp2_zdet;
}
damrey_reg IPhyAfe::getRB()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.reg_rb;
}
damrey_reg IPhyAfe::getVersion()
{
    readReg( r_r0, 0, AFE_CHAN );

    if( mId == AFE_CHAN )
    {
        //showPara();
    }
    readReg( r_r0, 0, mId );

    //qDebug() << "------------AFE V5-------------" << mId << r_rs0.version;

    return r_rs0.version;
}

//reg0
void IPhyAfe::setThTrim(unsigned short trim)
{
    rs0.th_trim = trim;
    appendWCache( 0, &r0 );
}

damrey_reg IPhyAfe::getGsel1()
{
    //readReg( rs4, 0, AFE_CHAN );
    return rs4.gsel1;
}

//! reg1
void IPhyAfe::setHzFilter(
                damrey_reg r,
                damrey_reg c,
                damrey_reg c0, damrey_reg c1, damrey_reg c2, damrey_reg c3 )
{
    rs1.r_sel = r;
    rs1.c_sel = c;
    rs1.cturn0 = c0;
    rs1.cturn1 = c1;
    rs1.cturn2 = c2;
    rs1.cturn3 = c3;

    appendWCache( 1, &r1 );
}
void IPhyAfe::setHzFilter(
                damrey_reg r,
                damrey_reg c
                 )
{
    rs1.r_sel = r;
    rs1.c_sel = c;

    appendWCache( 1, &r1 );
}

/**
 * @brief IPhyAfe::setHzSga1Gs
 * @param sel
 * 高阻通路SGA1增益选择
 * 0: LG, Gain=20
 * 1: HG, Gain=3.16
 */
void IPhyAfe::setHzSga1Gs( damrey_reg sel )
{
    rs1.sga1_gs = sel;
    appendWCache( 1, &r1 );
}

//! reg2
void IPhyAfe::setGpo0( damrey_reg hl )
{
    rs2.gpo0 = hl;
    appendWCache( 2, &r2 );
}

void IPhyAfe::setVosTrim1( damrey_reg trim )
{
    rs2.vos_trim1 = trim;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setFbSel( damrey_reg sel )
{
    rs2.fb_sel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setFsSel( damrey_reg sel )
{
    rs2.fs_sel = sel;
    appendWCache( 2, &r2 );
}

void IPhyAfe::setFRw( damrey_reg rw )
{
    rs2.f_rw = rw;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setDsel( damrey_reg sel )
{
    rs2.dsel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setVfsCtrl( damrey_reg ctrl )
{
    rs2.vfs_ctrl = ctrl;
    appendWCache( 2, &r2 );
}


//! reg3
void IPhyAfe::setDcTrim( damrey_reg trim )
{
    rs3.dc_trim = trim;
    appendWCache( 3, &r3 );
}

//ISEL：低阻通路输入端口选择。
//0：IN_A
//1: IN_B
//2: IN_C
//3: 未定义
void IPhyAfe::setISel( damrey_reg sel )
{
    rs3.isel = sel;
    appendWCache( 3, &r3 );
}

void IPhyAfe::setTpSw( damrey_reg tpsw )
{
    rs3.tp_sw = tpsw;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setTp1Typ( damrey_reg tp1typ )
{
    rs3.tp1_typ = tp1typ;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setTpPol( damrey_reg tppol )
{
    rs3.tp_pol = tppol;
    appendWCache( 3, &r3 );
}

//! reg4
void IPhyAfe::setEnDclp( bool bEn )
{
    rs4.en_dclp = bEn;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setDacSel( bool bHl )
{
    rs4.dac_sel = bHl;
    appendWCache( 4, &r4 );
}

void IPhyAfe::setGtrim1( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel1 = sel;
    rs4.gtrim1 = trim;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setGtrim2( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel2 = sel;
    rs4.gtrim2 = trim;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setGtrim3( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel3 = sel;
    rs4.gtrim3 = trim;
    appendWCache( 4, &r4 );
}

void IPhyAfe::setVosTrim2( damrey_reg trim )
{
    rs4.vos_trim2 = trim;
    appendWCache( 4, &r4 );
}

//! reg5
void IPhyAfe::setVosTrim3( damrey_reg trim )
{
    rs5.vos_trim3 = trim;
    appendWCache( 5, &r5 );
}

void IPhyAfe::setOen( damrey_reg output )
{
    rs5.oen = output;
    appendWCache( 5, &r5 );
}

void IPhyAfe::setOSel( damrey_reg sel )
{
    rs5.osel = sel;
    appendWCache( 5, &r5 );
}

//sel
//0: x0.8
//1: x1.2
//
//gain
//0~15: 0.925~1，0.05%/Step
//0 0.926
//1 0.931
//2 0.936
//3 0.940
//4 0.945
//5 0.950
//6 0.954
//7 0.959
//8 0.964
//9 0.969
//10 0.974
//11 0.979
//12 0.984
//13 0.990
//14 0.995
//15 1.000


void IPhyAfe::setGtrim4( damrey_reg sel, damrey_reg gain )
{
    rs5.gsel4 = sel;
    rs5.gtrim4 = gain;
    appendWCache( 5, &r5 );
}

//! reg6
void IPhyAfe::setOlEn( damrey_reg olEn )
{
    rs6.ol_en = olEn;
    appendWCache( 6, &r6 );
}
void IPhyAfe::setVosTrim4( damrey_reg trim )
{
    rs6.vos_trim4 = trim;
    appendWCache( 6, &r6 );
}
void IPhyAfe::setVosTrim5( damrey_reg trim )
{
    rs6.vos_trim5 = trim;
    appendWCache( 6, &r6 );
}

void IPhyAfe::setGtrim5( damrey_reg sel, damrey_reg gain )
{
    rs6.gsel5 = sel;
    rs6.g_trim5 = gain;
    appendWCache( 6, &r6 );
}

//! reg7
void IPhyAfe::setVos( damrey_reg vos )
{
    rs7.vos = vos;
    appendWCache( 7, &r7 );
}

//! reg8
/**
 * @brief IPhyAfe::setK
 * @param k
 * 高阻输入衰减器衰减比设置
 * K<4:0> =31: 全关断
   K<4:0> =30: x0.00625
   K<4:0> =29: x0.009375
   K<4:0> =28: x0.0156
   K<4:0> =27: x0.0469
   K<4:0> =24: x0.0625
   K<4:0> =23: x0.1875
   K<4:0> =16: x0.25
   K<4:0> =15: x0.75
   K<4:0> =0: x1
   K<5> =0: VOS调节范围扩展
   K<5> =1: VOS正常调节范围
 */
void IPhyAfe::setK( damrey_reg k )
{
    rs8.k = k;
    appendWCache( 8, &r8 );
}
void IPhyAfe::setVosExp( damrey_reg vosExp )
{
    rs8.vos_exp = vosExp;
    appendWCache( 8, &r8 );
}
//! 50 ou attx
//! 1 -- 1/2
//! 0 -- 1/20
void IPhyAfe::setGpo1( damrey_reg gp )
{
    rs8.gpo1 = gp;
    appendWCache( 8, &r8 );
}
void IPhyAfe::setLfTrim( damrey_reg trim )
{
    rs8.lf_trim = trim;
    appendWCache( 8, &r8 );
}


//! reg9
void IPhyAfe::setHfTrim( damrey_reg trim )
{
    rs9.hf_trim = trim;
    appendWCache( 9, &r9 );
}
void IPhyAfe::setMfcTrim( damrey_reg trim )
{
    rs9.mf_c_trim = trim;
    appendWCache( 9, &r9 );
}

//! reg10
void IPhyAfe::setAzVos( damrey_reg vos )
{
    rs10.az_vos_p = (vos >> 10) & 0x01;
    rs10.az_vos_trim = (vos) & 0x3ff;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setAzIsel( damrey_reg sel )
{
    rs10.az_isel = sel;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setEnZd( damrey_reg ed )
{
    rs10.en_zd = ed;
    appendWCache( 10, &r10 );
}

void IPhyAfe::setHzNpd( damrey_reg npd )
{
    rs10.hz_npd = npd;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setLpp( damrey_reg lpp )
{
    rs10.lpp = lpp;
    appendWCache( 10, &r10 );
}

//! reg11
damrey_reg IPhyAfe::getFuseRb()
{
    readReg( r_r11, 11, AFE_CHAN );

    return r_rs11.fuse_rb;
}

//! write
void IPhyAfe::setNlTrim41( damrey_reg trim )
{
    rs11.nl_trim41 = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setNlTrim40( damrey_reg trim )
{
    rs11.nl_trim40 = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setCcTrim4B( damrey_reg trim )
{
    rs11.cc_trim4B = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setCcTrim4A( damrey_reg trim )
{
    rs11.cc_trim4A = trim;
    appendWCache( 11, &r11 );
}

void IPhyAfe::setCcTrim3( damrey_reg trim )
{
    rs11.cc_trim3 = trim;
    appendWCache( 11, &r11 );
}

//! reg12
void IPhyAfe::setLzNpd( damrey_reg lznpd )
{
    rs12.lz_npd = lznpd;
    appendWCache( 12, &r12 );
}
void IPhyAfe::setHzITrim( damrey_reg trim )
{
    rs12.hz_itrim = trim;
    appendWCache( 12, &r12 );
}
void IPhyAfe::setLzITrim( damrey_reg trim )
{
    rs12.lz_itrim = trim;
    appendWCache( 12, &r12 );

}
void IPhyAfe::setNlTrim31( damrey_reg trim )
{
    rs12.nl_trim31 = trim;
    appendWCache( 12, &r12 );
}

void IPhyAfe::setNlTrim30( damrey_reg trim )
{
    rs12.nl_trim30 = trim;
    appendWCache( 12, &r12 );
}

//! reg13
void IPhyAfe::setNlTrim21( damrey_reg trim )
{
    rs13.nl_trim21 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim20( damrey_reg trim )
{
    rs13.nl_trim20 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim11( damrey_reg trim )
{
    rs13.nl_trim11 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim10( damrey_reg trim )
{
    rs13.nl_trim10 = trim;
    appendWCache( 13, &r13 );
}

//! reg 14
void IPhyAfe::setCcTrim2( damrey_reg trim )
{
    rs14.cc_trim2 = trim;
    appendWCache( 14, &r14 );
}
void IPhyAfe::setVclp( damrey_reg lp )
{
    rs14.vclp = lp;
    appendWCache( 14, &r14 );
}
void IPhyAfe::setCcTrim1( damrey_reg trim )
{
    rs14.cc_trim1 = trim;
    appendWCache( 14, &r14 );
}

void IPhyAfe::setHzVosTrim2( damrey_reg trim )
{
    rs14.hz_vos_trim2 = trim;
    appendWCache( 14, &rs14 );
}
void IPhyAfe::setHzVosTrim3( damrey_reg trim )
{
    rs14.hz_vos_trim3 = trim;
    appendWCache( 14, &rs14 );
}

//r15
void IPhyAfe::setHzVosTrim4( damrey_reg trim )
{
    //qDebug() << "trim4:" << trim;
    rs15.hz_vos_trim4 = trim;
    appendWCache( 15, &r15 );
}

#define show_reg( id )  str = QString("%1").arg( (int)r##id,0,16 );qDebug()<<id<<str;
void IPhyAfe::showPara()
{
    QString str;

    show_reg( 0 );
    show_reg( 1 );
    show_reg( 2 );
    show_reg( 3 );

    show_reg( 4 );
    show_reg( 5 );
    show_reg( 6 );
    show_reg( 7 );

    show_reg( 8 );
    show_reg( 9 );
    show_reg( 10 );
    show_reg( 11 );

    show_reg( 12 );
    show_reg( 13 );
    show_reg( 14 );
    show_reg( 15 );
}

void IPhyAfe::readReg( damrey_reg &reg,
                       int addr,
                       int chan )
{
    unsigned int packVal;
    damrey_reg busVal;

    //! select afe
    busVal = 1 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    packVal = addr;
    packVal |= (0x80);

    m_pBus->read( reg, packVal, chan );
    toLE( reg );

    //! deselect afe
    busVal = 0 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

//    m_pBus->read( reg, 0x0c, 0 );
//    toLE( reg );
//    appendRCache( addr, &reg );

//    qDebug()<<reg;
}

void IPhyAfe::readReg( damrey_reg &reg,
                       int addr )
{
    readReg( reg, addr, AFE_CHAN );
}

}
#endif


#ifdef DAMREY_VER_1_5

namespace dso_phy {

#define AFE_CHAN    1

IPhyAfe::IPhyAfe( int id )
{
    setId( id );
}

void IPhyAfe::init()
{
    r0 = 0x000C;    //! RMS_GAIN=12, 5V
    r1 = 0xf000;    //! R_SEL=7, C_SEL=0, full band
    r2 = 0x1023;
    r3 = 0x057f;    //! DC_TRIM 127

    r4 = 0x2f34;
    r5 = 0x790f;
    r6 = 0x7bde;
    r7 = 0x7502;

    r8 = 0xbfe0;    //! k[5] = 1
    r9 = 0x1f93;
    r10 = 0x0032;
    r11 = 0x2900;

    r12 = 0xff27;
    r13 = 0xffff;
    r14 = 0x00e1;

    /* ds8000: 3  ds7000: 1
     *             -----ADC1
     * signal-----|
     *             -----ADC2
     */
    setOen( 3 );
    //  setDacSel( 1 );
    //  setEnDclp( 1 );

    //! cctrim
    setCcTrim1( 5 );
    setCcTrim2( 0 );
    setCcTrim3( 0 );

    //! dc trim
    //! 70k
    setDcTrim( 161 );

    //! for all reg
    appendWCache( 0, &r0 );
    appendWCache( 1, &r1 );
    appendWCache( 2, &r2 );
    appendWCache( 3, &r3 );

    appendWCache( 4, &r4 );
    appendWCache( 5, &r5 );
    appendWCache( 6, &r6 );
    appendWCache( 7, &r7 );

    appendWCache( 8, &r8 );
    appendWCache( 9, &r9 );
    appendWCache( 10, &r10 );
    appendWCache( 11, &r11 );

    appendWCache( 12, &r12 );
    appendWCache( 13, &r13 );
    appendWCache( 14, &r14 );

    flushWCache();
}

void IPhyAfe::flushWCache()
{
    //! write all
    regCache *pItem;

    unsigned short busVal;
    unsigned int packVal;

    if ( mwCache.size() < 1 ) return;

    IPhy::lockWCache();

    busVal = 1 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
    foreach( pItem, mwCache )
    {
        Q_ASSERT( NULL != pItem );
        Q_ASSERT( NULL != pItem->m_pMem );

        busVal = *(unsigned short*)pItem->m_pMem;
        packVal = pack24AddrData( pItem->mAddr,
                                  busVal );

        m_pBus->send( &packVal, 3, AFE_CHAN );
    }

    busVal = 0 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    IPhy::unlockWCache();

    //! clear all
    IPhy::flushWCache();
}

void IPhyAfe::setId( int id )
{

    Q_ASSERT( id >= 0 && id <=3);

    mId = id;

    //! set cs
    mCs = mId;
}

//! reg0
void IPhyAfe::setRmsGain( damrey_reg gain )
{
    rs0.rms_gain = gain;
}

void IPhyAfe::setAtten( damrey_reg att )
{
    rs0.atten = att;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setHzVosTrim2( damrey_reg trim )
{
    rs0.hz_vos_trim2 = trim;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setHzVosTrim3( damrey_reg trim )
{
    rs0.hz_vos_trim3 = trim;
    appendWCache( 0, &r0 );
}

//! read
damrey_reg IPhyAfe::getOvDet()
{
    readReg( (r_r0), 0, AFE_CHAN );
    return r_rs0.ov_det;
}
damrey_reg IPhyAfe::getZDet()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.z_det;
}
damrey_reg IPhyAfe::getTp2ZDet()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.tp2_zdet;
}
damrey_reg IPhyAfe::getRB()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.reg_rb;
}
damrey_reg IPhyAfe::getVersion()
{
    readReg( r_r0, 0, AFE_CHAN );
    qDebug() << "------------AFE V4------------- " << r_rs0.version;
    return r_rs0.version;
}

//! reg1
void IPhyAfe::setHzFilter(
                damrey_reg r,
                damrey_reg c,
                damrey_reg c0, damrey_reg c1, damrey_reg c2, damrey_reg c3 )
{
    rs1.r_sel = r;
    rs1.c_sel = c;
    rs1.cturn0 = c0;
    rs1.cturn1 = c1;
    rs1.cturn2 = c2;
    rs1.cturn3 = c3;

    appendWCache( 1, &r1 );
}
void IPhyAfe::setHzFilter(
                damrey_reg r,
                damrey_reg c
                 )
{
    rs1.r_sel = r;
    rs1.c_sel = c;

    appendWCache( 1, &r1 );
}
void IPhyAfe::setHzSga1Gs( damrey_reg sel )
{
    rs1.sga1_gs = sel;
    appendWCache( 1, &r1 );
}

//! reg2
void IPhyAfe::setGpo0( damrey_reg hl )
{
    rs2.gpo0 = hl;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setHzVosTrim4( damrey_reg trim )
{
    rs2.hz_vos_trim4 = trim;
    appendWCache( 2, &r2 );
}

void IPhyAfe::setFbCSel( damrey_reg sel )
{
    rs2.fb_csel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setFbRSel( damrey_reg sel )
{
    rs2.fb_rsel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setProg( damrey_reg prog )
{
    rs2.prog = prog;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setFRw( damrey_reg rw )
{
    rs2.f_rw = rw;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setDsel( damrey_reg sel )
{
    rs2.dsel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setVfsCtrl( damrey_reg ctrl )
{
    rs2.vfs_ctrl = ctrl;
    appendWCache( 2, &r2 );
}


//! reg3
void IPhyAfe::setDcTrim( damrey_reg trim )
{
    rs3.dc_trim = trim;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setISel( damrey_reg sel )
{
    rs3.isel = sel;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setVosTrim1( damrey_reg trim )
{
    rs3.vos_trim1_01 = trim & 0x03;
    rs9.vos_trim1_23 = (trim >>2)&0x03;
    rs11.vos_trim1_4 = (trim >>4)&0x01;

    appendWCache( 3, &r3 );
    appendWCache( 9, &r9 );
    appendWCache( 11, &r11 );
}
void IPhyAfe::setTpSw( damrey_reg tpsw )
{
    rs3.tp_sw = tpsw;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setTp1Typ( damrey_reg tp1typ )
{
    rs3.tp1_typ = tp1typ;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setTpPol( damrey_reg tppol )
{
    rs3.tp_pol = tppol;
    appendWCache( 3, &r3 );
}

//! reg4
void IPhyAfe::setEnDclp( bool bEn )
{
    rs4.en_dclp = bEn;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setDacSel( bool bHl )
{
    rs4.dac_sel = bHl;
    appendWCache( 4, &r4 );
}

void IPhyAfe::setGtrim1( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel1 = sel;
    rs4.gtrim1 = trim;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setGtrim2( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel2 = sel;
    rs4.gtrim2 = trim;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setGtrim3( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel3 = sel;
    rs4.gtrim3 = trim;
    appendWCache( 4, &r4 );
}

void IPhyAfe::setVosTrim2( damrey_reg trim )
{
    rs4.vos_trim2 = trim;
    appendWCache( 4, &r4 );
}

//! reg5
void IPhyAfe::setVosTrim3( damrey_reg trim )
{
    rs5.vos_trim3 = trim;
    appendWCache( 5, &r5 );
}

void IPhyAfe::setOen( damrey_reg output )
{
    rs5.oen = output;
    appendWCache( 5, &r5 );
}

void IPhyAfe::setOSel( damrey_reg sel )
{
    rs5.osel = sel;
    appendWCache( 5, &r5 );
}
void IPhyAfe::setGtrim4( damrey_reg sel, damrey_reg gain )
{
    rs5.gsel4 = sel;
    rs5.gtrim4 = gain;
    appendWCache( 5, &r5 );
}

//! reg6
void IPhyAfe::setOlEn( damrey_reg olEn )
{
    rs6.ol_en = olEn;
    appendWCache( 6, &r6 );
}
void IPhyAfe::setVosTrim4( damrey_reg trim )
{
    rs6.vos_trim4 = trim;
    appendWCache( 6, &r6 );
}
void IPhyAfe::setVosTrim5( damrey_reg trim )
{
    rs6.vos_trim5 = trim;
    appendWCache( 6, &r6 );
}

void IPhyAfe::setGtrim5( damrey_reg sel, damrey_reg gain )
{
    rs6.gsel5 = sel;
    rs6.g_trim5 = gain;
    appendWCache( 6, &r6 );
}

//! reg7
void IPhyAfe::setVos( damrey_reg vos )
{
    rs7.vos = vos;
    appendWCache( 7, &r7 );
}

//! reg8
void IPhyAfe::setK( damrey_reg k )
{
    rs8.k = k;
    appendWCache( 8, &r8 );
}
void IPhyAfe::setVosExp( damrey_reg vosExp )
{
    rs8.vos_exp = vosExp;
    appendWCache( 8, &r8 );
}
//! 50 ou attx
//! 1 -- 1/2
//! 0 -- 1/20
void IPhyAfe::setGpo1( damrey_reg gp )
{
    rs8.gpo1 = gp;
    appendWCache( 8, &r8 );
}
void IPhyAfe::setLfTrim( damrey_reg trim )
{
    rs8.lf_trim = trim;
    appendWCache( 8, &r8 );
}


//! reg9
void IPhyAfe::setHfTrim( damrey_reg trim )
{
    rs9.hf_trim = trim;
    appendWCache( 9, &r9 );
}
void IPhyAfe::setMfcTrim( damrey_reg trim )
{
    rs9.mf_c_trim = trim;
    appendWCache( 9, &r9 );
}

//! reg10
void IPhyAfe::setAzVos( damrey_reg vos )
{
    rs10.az_vos_p = (vos >> 10) & 0x01;
    rs10.az_vos_trim = (vos) & 0x3ff;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setAzIsel( damrey_reg sel )
{
    rs10.az_isel = sel;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setEnZd( damrey_reg ed )
{
    rs10.en_zd = ed;
    appendWCache( 10, &r10 );
}

void IPhyAfe::setHzNpd( damrey_reg npd )
{
    rs10.hz_npd = npd;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setLpp( damrey_reg lpp )
{
    rs10.lpp = lpp;
    appendWCache( 10, &r10 );
}

//! reg11
damrey_reg IPhyAfe::getFuseRb()
{
    readReg( r_r11, 11, AFE_CHAN );

    return r_rs11.fuse_rb;
}

//! write
void IPhyAfe::setNlTrim41( damrey_reg trim )
{
    rs11.nl_trim41 = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setNlTrim40( damrey_reg trim )
{
    rs11.nl_trim40 = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setCcTrim4B( damrey_reg trim )
{
    rs11.cc_trim4B = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setCcTrim4A( damrey_reg trim )
{
    rs11.cc_trim4A = trim;
    appendWCache( 11, &r11 );
}

void IPhyAfe::setCcTrim3( damrey_reg trim )
{
    rs11.cc_trim3 = trim;
    appendWCache( 11, &r11 );
}

//! reg12
void IPhyAfe::setLzNpd( damrey_reg lznpd )
{
    rs12.lz_npd = lznpd;
    appendWCache( 12, &r12 );
}
void IPhyAfe::setHzITrim( damrey_reg trim )
{
    rs12.hz_itrim = trim;
    appendWCache( 12, &r12 );
}
void IPhyAfe::setLzITrim( damrey_reg trim )
{
    rs12.lz_itrim = trim;
    appendWCache( 12, &r12 );

}
void IPhyAfe::setNlTrim31( damrey_reg trim )
{
    rs12.nl_trim31 = trim;
    appendWCache( 12, &r12 );
}

void IPhyAfe::setNlTrim30( damrey_reg trim )
{
    rs12.nl_trim30 = trim;
    appendWCache( 12, &r12 );
}

//! reg13
void IPhyAfe::setNlTrim21( damrey_reg trim )
{
    rs13.nl_trim21 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim20( damrey_reg trim )
{
    rs13.nl_trim20 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim11( damrey_reg trim )
{
    rs13.nl_trim11 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim10( damrey_reg trim )
{
    rs13.nl_trim10 = trim;
    appendWCache( 13, &r13 );
}

//! reg 14
void IPhyAfe::setCcTrim2( damrey_reg trim )
{
    rs14.cc_trim2 = trim;
    appendWCache( 14, &r14 );
}
void IPhyAfe::setVclp( damrey_reg lp )
{
    rs14.vclp = lp;
    appendWCache( 14, &r14 );
}
void IPhyAfe::setCcTrim1( damrey_reg trim )
{
    rs14.cc_trim1 = trim;
    appendWCache( 14, &r14 );
}

#define show_reg( id )  str = QString("%1").arg( (int)r##id,0,16 );qDebug()<<id<<str;
void IPhyAfe::showPara()
{
    QString str;

//    str.arg( rs0,0,16 );

    show_reg( 0 );
    show_reg( 1 );
    show_reg( 2 );
    show_reg( 3 );

    show_reg( 4 );
    show_reg( 5 );
    show_reg( 6 );
    show_reg( 7 );

    show_reg( 8 );
    show_reg( 9 );
    show_reg( 10 );
    show_reg( 11 );

    show_reg( 12 );
    show_reg( 13 );
    show_reg( 14 );
}

void IPhyAfe::readReg( damrey_reg &reg,
                       int addr,
                       int chan )
{
    unsigned int packVal;
    damrey_reg busVal;

    //! select afe
    busVal = 1 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    packVal = addr;
    packVal |= (0x80);

    m_pBus->read( reg, packVal, chan );
    toLE( reg );

    //! deselect afe
    busVal = 0 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

//    m_pBus->read( reg, 0x0c, 0 );
//    toLE( reg );
//    appendRCache( addr, &reg );

//    qDebug()<<reg;
}

void IPhyAfe::readReg( damrey_reg &reg,
                       int addr )
{
    readReg( reg, addr, AFE_CHAN );
}

}
#endif


#ifdef DAMREY_VER_1_4
namespace dso_phy {

#define AFE_CHAN    1

IPhyAfe::IPhyAfe( int id )
{
    setId( id );
}

void IPhyAfe::init()
{
    r0 = 0x00B8;
    r1 = 0xf000;
    r2 = 0x1023;
    r3 = 0x057f;

    r4 = 0x2f34;
    r5 = 0x790f;
    r6 = 0x7bde;
    r7 = 0x7502;

    r8 = 0xbfe0;
    r9 = 0x1f93;
    r10 = 0x0032;
    r11 = 0x2900;

    r12 = 0xff27;
    r13 = 0xffff;
    r14 = 0x00e1;


    //! a on, b on
    setOen( 3 );
//    setDacSel( 1 );
//    setEnDclp( 1 );

    //! cctrim
    setCcTrim1( 5 );
    setCcTrim2( 0 );
    setCcTrim3( 0 );

    //! dc trim
    //! 70k
    setDcTrim( 161 );

    //! for all reg
    appendWCache( 0, &r0 );
    appendWCache( 1, &r1 );
    appendWCache( 2, &r2 );
    appendWCache( 3, &r3 );

    appendWCache( 4, &r4 );
    appendWCache( 5, &r5 );
    appendWCache( 6, &r6 );
    appendWCache( 7, &r7 );

    appendWCache( 8, &r8 );
    appendWCache( 9, &r9 );
    appendWCache( 10, &r10 );
    appendWCache( 11, &r11 );

    appendWCache( 12, &r12 );
    appendWCache( 13, &r13 );
    appendWCache( 14, &r14 );

    flushWCache();
}

void IPhyAfe::flushWCache()
{
    //! write all
    regCache *pItem;

    unsigned short busVal;
    unsigned int packVal;

    if ( mwCache.size() < 1 ) return;

    IPhy::lockWCache();

    busVal = 1 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
    foreach( pItem, mwCache )
    {
        Q_ASSERT( NULL != pItem );
        Q_ASSERT( NULL != pItem->m_pMem );

        busVal = *(unsigned short*)pItem->m_pMem;
        packVal = pack24AddrData( pItem->mAddr,
                                  busVal );

        m_pBus->send( &packVal, 3, AFE_CHAN );
    }

    busVal = 0 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    IPhy::unlockWCache();

    //! clear all
    IPhy::flushWCache();
}

void IPhyAfe::setId( int id )
{

    Q_ASSERT( id >= 0 && id <=3);

    mId = id;

    //! set cs
    mCs = mId;
}

quint32 IPhyAfe::getVersion()
{
    return 0;
}

//void IPhyAfe::setLozScale( int scale )
//{
//    lozScaleCfg *pCfg;

//    pCfg = getLozScaleCfg( scale );
//    if ( NULL == pCfg )
//    {
//        qWarning()<<"invalid scale"<<scale;
//        return;
//    }

//    //! config
//    setGtrim1( pCfg->gsel1, pCfg->gtrim1 );
//    setGtrim2( pCfg->gsel2, pCfg->gtrim2 );
//    setVosTrim2( pCfg->vos_trim2 );
//    setGtrim3( pCfg->gsel3, pCfg->gtrim3 );

//    setVosTrim3( pCfg->vos_trim3 );
//    setGtrim4( pCfg->gsel4, pCfg->gtrim4 );
//    setVosTrim4( pCfg->vos_trim4 );
//    setGtrim5( pCfg->gsel5, pCfg->gtrim5 );

//    setVosTrim5( pCfg->vos_trim5 );
//    setDcTrim( pCfg->dc_trim );

////    flushWCache();
////    qDebug()<<__FUNCTION__<<__LINE__;
//}

//void IPhyAfe::setHizScale( int scale )
//{
//    hizScaleCfg *pCfg;
//    pCfg = getHizScaleCfg( scale );
//    if ( NULL == pCfg )
//    {
//        qWarning()<<"invalid scale"<<scale;
//        return;
//    }

//    setK( pCfg->k );
//    setHzSga1Gs( pCfg->sga1_gs );
//    setAtten( pCfg->atten );
//    setDcTrim( pCfg->dc_trim );

//    setLfTrim( pCfg->lf_trim );
//    setMfcTrim( pCfg->mf_c_trim );
//    setHfTrim( pCfg->hf_trim );

////    flushWCache();
////    qDebug()<<__FUNCTION__<<__LINE__;
//}

//float IPhyAfe::getLozAtten( int scale )
//{
//    if ( scale <= 100000 )
//    { return 1.0f; }
//    else
//    { return 10.0f; }
//}

//float IPhyAfe::getHizAtten( int scale )
//{
//    hizScaleCfg *pCfg;
//    pCfg = getHizScaleCfg( scale );
//    if ( NULL == pCfg )
//    { return 0.0f; }

//    return getKAtten( pCfg->k );
//}


//! reg0
void IPhyAfe::setOvDet( damrey_reg det )
{
    rs0.ov_det = det;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setOlEn( damrey_reg olen )
{
    setOvDet( olen );
}

void IPhyAfe::setOvLvlFine( damrey_reg lvl )
{
    rs0.ov_lvl = lvl;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setTpSw( damrey_reg sw )
{
    rs0.tp_sw = sw;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setAtten( damrey_reg att )
{
    rs0.atten = att;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setHzVosTrim2( damrey_reg trim )
{
    rs0.hz_vos_trim2 = trim;
    appendWCache( 0, &r0 );
}
void IPhyAfe::setHzVosTrim3( damrey_reg trim )
{
    rs0.hz_vos_trim3 = trim;
    appendWCache( 0, &r0 );

}

//! read
damrey_reg IPhyAfe::getOvDet()
{
    readReg( (r_r0), 0, AFE_CHAN );
    return r_rs0.ov_det;
}
damrey_reg IPhyAfe::getZDet()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.z_det;
}

damrey_reg IPhyAfe::getRB()
{
    readReg( r_r0, 0, AFE_CHAN );
    return r_rs0.reg_rb;
}

//! reg1
void IPhyAfe::setHzFilter(
                damrey_reg r,
                damrey_reg c,
                damrey_reg c0,
                damrey_reg c1,
                damrey_reg c2,
                damrey_reg c3 )
{
    rs1.r_sel = r;
    rs1.c_sel = c;
    rs1.cturn0 = c0;
    rs1.cturn1 = c1;
    rs1.cturn2 = c2;
    rs1.cturn3 = c3;

    appendWCache( 1, &r1 );
}

void IPhyAfe::setHzFilter(
                damrey_reg r,
                damrey_reg c
                 )
{
    rs1.r_sel = r;
    rs1.c_sel = c;

    appendWCache( 1, &r1 );
}

void IPhyAfe::setHzSga1Gs( damrey_reg sel )
{
    rs1.sga1_gs = sel;
    appendWCache( 1, &r1 );
}

//! reg2
void IPhyAfe::setGpo0( damrey_reg hl )
{
    rs2.gpo0 = hl;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setHzVosTrim4( damrey_reg trim )
{
    rs2.hz_vos_trim4 = trim;
    appendWCache( 2, &r2 );

}

void IPhyAfe::setFbCSel( damrey_reg sel )
{
    rs2.fb_csel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setFbRSel( damrey_reg sel )
{
    rs2.fb_rsel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setProg( damrey_reg prog )
{
    rs2.prog = prog;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setFRw( damrey_reg rw )
{
    rs2.f_rw = rw;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setDsel( damrey_reg sel )
{
    rs2.dsel = sel;
    appendWCache( 2, &r2 );
}
void IPhyAfe::setVfsCtrl( damrey_reg ctrl )
{
    rs2.vfs_ctrl = ctrl;
    appendWCache( 2, &r2 );
}


//! reg3
void IPhyAfe::setDcTrim( damrey_reg trim )
{
    rs3.dc_trim = trim;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setISel( damrey_reg sel )
{
    rs3.isel = sel;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setVClamp( damrey_reg clamp )
{
    rs3.vclamp = clamp;
    appendWCache( 3, &r3 );
}
void IPhyAfe::setOvLvl( damrey_reg level )
{
    rs3.ov_lvl = level;
    appendWCache( 3, &r3 );
}

//! reg4
void IPhyAfe::setEnDclp( bool bEn )
{
    rs4.en_dclp = bEn;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setDacSel( bool bHl )
{
    rs4.dac_sel = bHl;
    appendWCache( 4, &r4 );
}

void IPhyAfe::setGtrim1( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel1 = sel;
    rs4.gtrim1 = trim;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setGtrim2( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel2 = sel;
    rs4.gtrim2 = trim;
    appendWCache( 4, &r4 );
}
void IPhyAfe::setGtrim3( damrey_reg sel,
                damrey_reg trim )
{
    rs4.gsel3 = sel;
    rs4.gtrim3 = trim;
    appendWCache( 4, &r4 );
}

void IPhyAfe::setVosTrim2( damrey_reg trim )
{
    rs4.vos_trim2 = trim;
    appendWCache( 4, &r4 );

}

//! reg5
void IPhyAfe::setVosTrim3( damrey_reg trim )
{
    rs5.vos_trim3 = trim;
    appendWCache( 5, &r5 );
}
void IPhyAfe::setTp1Typ( damrey_reg tpe )
{
    rs5.tp1_typ = tpe;
    appendWCache( 5, &r5 );
}
void IPhyAfe::setTpPol( damrey_reg pol )
{
    rs5.tp_pol = pol;
    appendWCache( 5, &r5 );
}
void IPhyAfe::setOen( damrey_reg output )
{
    rs5.oen = output;
    appendWCache( 5, &r5 );
}

void IPhyAfe::setOSel( damrey_reg sel )
{
    rs5.osel = sel;
    appendWCache( 5, &r5 );
}
void IPhyAfe::setGtrim4( damrey_reg sel, damrey_reg gain )
{
    rs5.gsel4 = sel;
    rs5.gtrim4 = gain;
    appendWCache( 5, &r5 );
}

//! reg6
void IPhyAfe::setOvRng( damrey_reg over )
{
    rs6.ov_rng = over;
    appendWCache( 6, &r6 );
}
void IPhyAfe::setVosTrim4( damrey_reg trim )
{
    rs6.vos_trim4 = trim;
    appendWCache( 6, &r6 );
}
void IPhyAfe::setVosTrim5( damrey_reg trim )
{
    rs6.vos_trim5 = trim;
    appendWCache( 6, &r6 );
}

void IPhyAfe::setGtrim5( damrey_reg sel, damrey_reg gain )
{
    rs6.gsel5 = sel;
    rs6.g_trim5 = gain;
    appendWCache( 6, &r6 );
}

//! reg7
void IPhyAfe::setVos( damrey_reg vos )
{
    rs7.vos = vos;
    appendWCache( 7, &r7 );
}

//! reg8
void IPhyAfe::setK( damrey_reg k )
{
    rs8.k = k;
    appendWCache( 8, &r8 );
}

void IPhyAfe::setVosExp( damrey_reg vosExp )
{
    rs8.vos_exp = vosExp;
    appendWCache( 8, &r8 );
}

void IPhyAfe::setGpo1( damrey_reg gp )
{
    rs8.gpo1 = gp;
    appendWCache( 8, &r8 );
}
void IPhyAfe::setLfTrim( damrey_reg trim )
{
    rs8.lf_trim = trim;
    appendWCache( 8, &r8 );
}
void IPhyAfe::setIloEn( damrey_reg en )
{
    rs8.ilo_en = en;
    appendWCache( 8, &r8 );
}

//! reg9
void IPhyAfe::setHfTrim( damrey_reg trim )
{
    rs9.hf_trim = trim;
    appendWCache( 9, &r9 );
}
void IPhyAfe::setMfcTrim( damrey_reg trim )
{
    rs9.mf_c_trim = trim;
    appendWCache( 9, &r9 );
}


void IPhyAfe::setAzVos( damrey_reg vos )
{
    rs10.az_vos_p = (vos >> 10) & 0x01;
    rs10.az_vos_trim = (vos) & 0x3ff;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setAzIsel( damrey_reg sel )
{
    rs10.az_isel = sel;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setEnZd( damrey_reg ed )
{
    rs10.en_zd = ed;
    appendWCache( 10, &r10 );
}

void IPhyAfe::setHzNpd( damrey_reg npd )
{
    rs10.hz_npd = npd;
    appendWCache( 10, &r10 );
}
void IPhyAfe::setLpp( damrey_reg lpp )
{
    rs10.lpp = lpp;
    appendWCache( 10, &r10 );
}

//! reg11
damrey_reg IPhyAfe::getFuseRb()
{
    readReg( r_r11, 11, AFE_CHAN );

    return r_rs11.fuse_rb;
}

//! write
void IPhyAfe::setNlTrim41( damrey_reg trim )
{
    rs11.nl_trim41 = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setNlTrim40( damrey_reg trim )
{
    rs11.nl_trim40 = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setCcTrim4B( damrey_reg trim )
{
    rs11.cc_trim4B = trim;
    appendWCache( 11, &r11 );
}
void IPhyAfe::setCcTrim4A( damrey_reg trim )
{
    rs11.cc_trim4A = trim;
    appendWCache( 11, &r11 );
}

void IPhyAfe::setCcTrim3( damrey_reg trim )
{
    rs11.cc_trim3 = trim;
    appendWCache( 11, &r11 );
}

//! reg12
void IPhyAfe::setLzNpd( damrey_reg lznpd )
{
    rs12.lz_npd = lznpd;
    appendWCache( 12, &r12 );
}
void IPhyAfe::setHzITrim( damrey_reg trim )
{
    rs12.hz_itrim = trim;
    appendWCache( 12, &r12 );
}
void IPhyAfe::setLzITrim( damrey_reg trim )
{
    rs12.lz_itrim = trim;
    appendWCache( 12, &r12 );
}
void IPhyAfe::setNlTrim31( damrey_reg trim )
{
    rs12.nl_trim31 = trim;
    appendWCache( 12, &r12 );

}

void IPhyAfe::setNlTrim30( damrey_reg trim )
{
    rs12.nl_trim30 = trim;
    appendWCache( 12, &r12 );
}

//! reg13
void IPhyAfe::setNlTrim21( damrey_reg trim )
{
    rs13.nl_trim21 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim20( damrey_reg trim )
{
    rs13.nl_trim20 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim11( damrey_reg trim )
{
    rs13.nl_trim11 = trim;
    appendWCache( 13, &r13 );
}
void IPhyAfe::setNlTrim10( damrey_reg trim )
{
    rs13.nl_trim10 = trim;
    appendWCache( 13, &r13 );
}

//! reg 14
void IPhyAfe::setCcTrim2( damrey_reg trim )
{
    rs14.cc_trim2 = trim;
    appendWCache( 14, &r14 );
}
void IPhyAfe::setVclp( damrey_reg lp )
{
    rs14.vclp = lp;
    appendWCache( 14, &r14 );
}
void IPhyAfe::setCcTrim1( damrey_reg trim )
{
    rs14.cc_trim1 = trim;
    appendWCache( 14, &r14 );
}

#define show_reg( id )  str = QString("%1").arg( (int)r##id,0,16 );qDebug()<<id<<str;
void IPhyAfe::showPara()
{
    QString str;

    show_reg( 0 );
    show_reg( 1 );
    show_reg( 2 );
    show_reg( 3 );

    show_reg( 4 );
    show_reg( 5 );
    show_reg( 6 );
    show_reg( 7 );

    show_reg( 8 );
    show_reg( 9 );
    show_reg( 10 );
    show_reg( 11 );

    show_reg( 12 );
    show_reg( 13 );
    show_reg( 14 );
}

void IPhyAfe::readReg( damrey_reg &reg,
                       int addr,
                       int chan )
{
    //! exist now
    if ( findCache( addr, mrCache ) != NULL )
    { return; }

    unsigned int packVal;
    damrey_reg busVal;

    //! select afe
    busVal = 1 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    packVal = addr;
    packVal |= (0x80);

    m_pBus->read( reg, packVal, chan );
    toLE( reg );

    //! deselect afe
    busVal = 0 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

//    m_pBus->read( reg, 0x0c, 0 );
//    toLE( reg );
//    appendRCache( addr, &reg );

//    qDebug()<<reg;
}

void IPhyAfe::readReg( damrey_reg &reg,
                       int addr )
{
    readReg( reg, addr, AFE_CHAN );
}


}
#endif


