
#include "chcal.h"
#include "../../include/dsodbg.h"
#include "../../service/servdso/sysdso.h"
namespace dso_phy {

#define scale_fract_base (1.0e-6f)

afeCfgPair::afeCfgPair()
{
    clear();
}
void afeCfgPair::clear()
{
    mCfg.mKey = 0;
    mCfg.lzBw = BW_20M;

    mVal = 0;
    mfVal = 0;
}

void afeCfgPair::setHzCfg(
               quint8 k,
               quint8 vos,
               quint8 gs,
               quint8 att,
               quint8 sga4, quint8 sga4Trim,
               quint8 sga5, quint8 sga5Trim )
{
    mCfg.hzK = k;
    mCfg.hzVos = vos;

    mCfg.hzGs = gs;
    mCfg.hzAtten = att;

    mCfg.hzSga4 = sga4;
    mCfg.hzSga4Trim = sga4Trim;

    mCfg.hzSga5 = sga5;
    mCfg.hzSga5Trim  = sga5Trim;
}

void afeCfgPair::setLzCfg( quint8 att,
               quint8 sga1, quint8 sga1Trim,
               quint8 sga2, quint8 sga2Trim,
               quint8 sga3, quint8 sga3Trim,
               quint8 flt,
               quint8 sga4, quint8 sga4Trim,
               quint8 sga5, quint8 sga5Trim,
               quint8 bw)
{
    mCfg.lzAtt = att;

    mCfg.lzSga1 = sga1;
    mCfg.lzSga1Trim = sga1Trim;

    mCfg.lzSga2 = sga2;
    mCfg.lzSga2Trim = sga2Trim;

    mCfg.lzSga3 = sga3;
    mCfg.lzSga3Trim = sga3Trim;

    mCfg.lzFlt = flt;

    mCfg.lzSga4 = sga4;
    mCfg.lzSga4Trim = sga4Trim;

    mCfg.lzSga5 = sga5;
    mCfg.lzSga5Trim = sga5Trim;

    mCfg.lzBw = bw;
}

#define hz_gnd_match_bypass()   if ( matchCnt <=0 ) { return true; }\
                         matchCnt--;
bool afeCfgPair::hzGndEqual( afeCfgPair *pPair, int matchCnt )
{
    Q_ASSERT( NULL != pPair );

    hz_gnd_match_bypass();
    if ( pPair->mCfg.hzK != mCfg.hzK )
    { return false; }

    hz_gnd_match_bypass();
    if ( pPair->mCfg.hzVos != mCfg.hzVos )
    { return false; }

    hz_gnd_match_bypass();
    if ( pPair->mCfg.hzGs != mCfg.hzGs )
    { return false; }

    hz_gnd_match_bypass();
    if ( pPair->mCfg.hzSga4 != mCfg.hzSga4 )
    { return false; }

    hz_gnd_match_bypass();
    if ( pPair->mCfg.hzAtten != mCfg.hzAtten )
    { return false; }

    hz_gnd_match_bypass();
    if ( pPair->mCfg.hzSga4Trim != mCfg.hzSga4Trim )
    { return false; }

    //! \todo sga5

    return true;
}

#define lz_gnd_match_bypass()   if ( matchCnt <=0 ) \
                                     { return true; } \
                                matchCnt--;

bool afeCfgPair::lzGndEqual( afeCfgPair *pPair,
                             int matchCnt )
{
    Q_ASSERT( NULL != pPair );

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzAtt != mCfg.lzAtt )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga1 != mCfg.lzSga1 )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga2 != mCfg.lzSga2 )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga3 != mCfg.lzSga3 )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzFlt != mCfg.lzFlt )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga4 != mCfg.lzSga4 )
    { return false; }

    //! for trim
    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga3Trim != mCfg.lzSga3Trim )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga1Trim != mCfg.lzSga1Trim )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga2Trim != mCfg.lzSga2Trim )
    { return false; }

    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzSga4Trim != mCfg.lzSga4Trim )
    { return false; }

    //! \todo sga5


    lz_gnd_match_bypass();
    if ( pPair->mCfg.lzBw != mCfg.lzBw )
    { return false; }

    return true;
}

void afeCfgPair::setValue( int val )
{
    mVal = val;
}

void afeCfgPair::setValue( float val )
{
    mfVal = val;
}

void afeCfgPair::exportHz( QTextStream &stream )
{
    QString str;

    str += QString("%1,").arg(mCfg.hzK);
    str += QString("%1,").arg(mCfg.hzVos);
    str += QString("%1,").arg(mCfg.hzGs);
    str += QString("%1,").arg(mCfg.hzAtten);

    str += QString("%1,").arg(mCfg.hzSga4);
    str += QString("%1,").arg(mCfg.hzSga4Trim);
    str += QString("%1,").arg(mVal);
    str += QString("%1,").arg(mfVal);

    stream<<str<<endl;
}
void afeCfgPair::exportLz( QTextStream &stream )
{
    QString str;

    str += QString("%1,").arg(mCfg.lzAtt);
    str += QString("%1,").arg(mCfg.lzSga1);
    str += QString("%1,").arg(mCfg.lzSga1Trim);
    str += QString("%1,").arg(mCfg.lzSga2);
    str += QString("%1,").arg(mCfg.lzSga2Trim);
    str += QString("%1,").arg(mCfg.lzSga3);
    str += QString("%1,").arg(mCfg.lzSga3Trim);

    str += QString("%1,").arg(mCfg.lzFlt);
    str += QString("%1,").arg(mCfg.lzSga4);
    str += QString("%1,").arg(mCfg.lzSga4Trim);
    str += QString("%1,").arg(mVal);
    str += QString("%1,").arg(mfVal);

    str += QString("%1,").arg(mCfg.lzBw);

    stream<<str<<endl;
}

void afeCfgPair::showHz()
{
    if ( sysHasArg("-log_afe") )
    { LOG_DBG()<<"k/vos/gs/atten/sga4(sel/trim)/scale/scale"<<mCfg.hzK<<mCfg.hzVos<<mCfg.hzGs<<mCfg.hzAtten<<mCfg.hzSga4<<mCfg.hzSga4Trim<<mVal<<mfVal; }
}
void afeCfgPair::showLz()
{
    if ( sysHasArg("-log_afe") )
    { LOG_DBG()<<"att/sga1(sel/trim)/sga2/sga3/flt/sga4"<<mCfg.lzAtt<<mCfg.lzSga1<<mCfg.lzSga1Trim<<mCfg.lzSga2<<mCfg.lzSga2Trim<<mCfg.lzSga3<<mCfg.lzSga3Trim<<mCfg.lzFlt<<mCfg.lzSga4<<mCfg.lzSga4Trim<<mVal<<mfVal; }
}

chCalProxy::chCalProxy( chCal *pCal )
{
    Q_ASSERT( NULL != pCal );
    m_pCal = pCal;

    //! hz
    hzDefScale.mCfg.hzK = 0;
    hzDefScale.mCfg.hzVos = 1;
    hzDefScale.mCfg.hzGs = 0;
    hzDefScale.mCfg.hzAtten = 0;
    hzDefScale.mCfg.hzSga4 = 0;
    hzDefScale.mCfg.hzSga4Trim = 15;

    hzDefScale.mCfg.hzSga5 = 0;
    hzDefScale.mCfg.hzSga5Trim = 15;

    hzDefScale.mVal = 100000;
    hzDefScale.mfVal = 0.1;

    //! lz
    lzDefScale.mCfg.lzAtt = 0;

    lzDefScale.mCfg.lzSga1 = 0;
    lzDefScale.mCfg.lzSga1Trim = 1;

    lzDefScale.mCfg.lzSga2 = 0;
    lzDefScale.mCfg.lzSga2Trim = 3;

    lzDefScale.mCfg.lzSga3 = 0;
    lzDefScale.mCfg.lzSga3Trim = 3;

    lzDefScale.mCfg.lzFlt = 0;

    lzDefScale.mCfg.lzSga4 = 0;
    lzDefScale.mCfg.lzSga4Trim = 15;

    lzDefScale.mCfg.lzSga5 = 0;
    lzDefScale.mCfg.lzSga5Trim = 15;

    lzDefScale.mVal = 100000;
    lzDefScale.mfVal = 0.1;
}

afeCfgPair *chCalProxy::findHzScale( int scale )
{
    afeCfgPair *pair;

    //! K range
    int jumpPoints[]={  50000,
                        100000,
                        260000,
                        1000000,
                        3500000,
                        6000000
                        };

    pair = findNearScale( scale,
                         m_pCal->_hzScales,
                         m_pCal->mHzScales,
                         jumpPoints,
                         array_count(jumpPoints) );

    if ( NULL == pair )
    { return &hzDefScale; }
    else
    { return pair; }
}
int chCalProxy::findHzGnd( int scale, int vos)
{
    afeCfgPair *scaleCfg;

    scaleCfg = findHzScale( scale );
    if ( NULL == scaleCfg )
    { return 32768; }

    //! backup
    afeCfgPair hzCfg;
    hzCfg = *scaleCfg;

    hzCfg.mCfg.hzVos = vos;

    return findNearHzGnd( &hzCfg,
                        m_pCal->_hzGnds,
                        m_pCal->mHzGnds );
}

float chCalProxy::findHzKx( int scale )
{
    afeCfgPair *scaleCfg;

    scaleCfg = findHzScale( scale );
    if ( NULL == scaleCfg )
    {
        return 1.0f;
    }

    for ( int i = 0; i < m_pCal->mhzKs; i++ )
    {
        if ( scaleCfg->mCfg.hzK == m_pCal->_hzKs[i].mKey )
        {
            return m_pCal->_hzKs[i].mVal;
        }
    }

    qWarning()<<"!!!no kx"<<m_pCal->mhzKs;

    return 1.0f;
}

afeCfgPair *chCalProxy::findLzScale( int scale,
                                     int flt,
                                     int bw)
{
    afeCfgPair *pair;

    //! attx
    //! 20x
    if ( scale > 100000 )
    {
        //! flt off
        if ( flt == 0 )
        {
            pair = findNearScale( scale,
                                 m_pCal->_lzScales_20x_normal,
                                 m_pCal->mLzScales_20x_normal,
                                  0,
                                  0
                                  );
        }
        //! flt on
        else
        {
            pair = findNearScale( scale,
                                 m_pCal->_lzScales_20x_flt,
                                 m_pCal->mLzScales_20x_flt,
                                  0,0 );
        }
    }
    //! 2x
    else
    {
        if ( flt == 0 )
        {
            if( scale <= 2000 )
            {
                pair = findNearLzScale( scale,
                                     m_pCal->_lzScales_2x_normal,
                                     m_pCal->mLzScales_2x_normal,
                                      0,0,bw);
            }
            else
            {
                pair = findNearScale( scale,
                                     m_pCal->_lzScales_2x_normal,
                                     m_pCal->mLzScales_2x_normal,
                                      0,0);
            }
        }
        else
        {
            pair = findNearScale( scale,
                                 m_pCal->_lzScales_2x_flt,
                                 m_pCal->mLzScales_2x_flt,
                                  0,0 );
        }
    }

    if ( NULL == pair )
    {
        return &lzDefScale;
    }
    else
    {
        return pair;
    }
}

int chCalProxy::findLzGnd( int scale,
                           int flt,
                           int bw)
{

    afeCfgPair *scaleCfg  = findLzScale( scale, flt, bw);

    if ( NULL == scaleCfg )
    {
        return 32768;
    }

    return findNearLzGnd( scaleCfg,
                        m_pCal->_lzGnds,
                        m_pCal->mLzGnds);
}

/*
 *  Only for 1mV and 2mV
*/
afeCfgPair *chCalProxy::findNearLzScale( int scale,
                                       afeCfgPair *pPairs,
                                       int cnt,
                                       int jumpPoints[],
                                       int jumpCnt,
                                       int bw )
{
    Q_ASSERT( NULL != pPairs );

    //! no item
    if ( cnt < 1 )
    {
        return NULL;
    }

    //! only one
    if ( cnt == 1 )
    {
        return pPairs;
    }


    //! in middle
    for ( int i = 0; i < cnt-1; i++ )
    {
        if ( scale <= pPairs[i].mVal )
        {
            if( BW_20M == bw)
            {
                return pPairs + i + 1;
            }
            else
            {
                return pPairs + i;
            }
        }
    }

    //useless, just for removing warnning
    if( jumpCnt > 0 )
    {
        jumpPoints[0] = jumpPoints[0];
    }
    qWarning()<<"!!!no scale";

    return NULL;
}

afeCfgPair *chCalProxy::findNearScale( int scale,
                                       afeCfgPair *pPairs,
                                       int cnt,
                                       int jumpPoints[],
                                       int jumpCnt)
{
    Q_ASSERT( NULL != pPairs );

    //! no item
    if ( cnt < 1 )
    { return NULL; }

    //! only one
    if ( cnt == 1 )
    { return pPairs; }

    //! >=2
    if ( scale <= pPairs[0].mVal )
    { return pPairs; }

    if ( scale >= pPairs[cnt-1].mVal )
    { return pPairs + cnt - 1; }

    //! in middle
    for ( int i = 0; i < cnt-1; i++ )
    {
        if ( scale == pPairs[i].mVal )
        { return pPairs + i; }

        //! i: [0, cnt-2]
        if ( scale == pPairs[i+1].mVal )
        { return pPairs + i + 1; }

        //! in middle
        if ( scale > pPairs[i].mVal
             && scale < pPairs[i+1].mVal )
        {
            //! jump point
            for ( int j = 0; j < jumpCnt; j++ )
            {
                //! over jump point use next
                if ( jumpPoints[j] >  pPairs[i].mVal
                     && jumpPoints[j] < pPairs[i+1].mVal )
                {
                    if ( scale <= jumpPoints[j] )
                    {
                        return pPairs + i;
                    }
                    else
                    {
                        return pPairs + i + 1;
                    }
                }
            }

            if ( scale >
                 (pPairs[i].mVal + pPairs[i+1].mVal)/2 )
            {
                return pPairs + i + 1;
            }
            else
            {
                return pPairs + i;
            }
        }
    }

    qWarning()<<"!!!no scale";

    return NULL;
}

int chCalProxy::findNearHzGnd( afeCfgPair *pPair,
                               afeCfgPair *pPairs,
                               int cnt )
{
    if ( cnt == 0 ) return 32768;

    int matchCnts[]={ 6, 5, 4 };

    for ( int j = 0; j < array_count(matchCnts); j++ )
    {
        for ( int i = 0; i < cnt; i++ )
        {
            if ( pPairs[i].hzGndEqual( pPair, matchCnts[j]) )
            {
                return pPairs[i].mVal;
            }
        }
    }

    qWarning()<< __FUNCTION__ << "!!!no gnd";

    return 32768;
}

int chCalProxy::findNearLzGnd( afeCfgPair *pPair,
                               afeCfgPair *pPairs,
                               int cnt)
{
    if ( cnt == 0 ) return 32768;

    int matchCnts[]={ 11, 10, 9, 8, 7 };

    for ( int j = 0; j < array_count(matchCnts); j++ )
    {
        for ( int i = 0; i < cnt; i++ )
        {
            if ( pPairs[i].lzGndEqual( pPair, matchCnts[j] ) )
            {
                return pPairs[i].mVal;
            }
        }
    }
    qWarning()<< __FUNCTION__ << "!!!no gnd";

    return 32768;
}

int HizLfTrimProxy::selectHizLfTrim( chHizLfTrim &lfTrim,
                                     int k,
                                     hizLfTrim *pTrim )
{
    Q_ASSERT( NULL != pTrim );

    int i;
    for ( i = 0; i < lfTrim.mTrimCount; i++ )
    {
        if ( lfTrim.mLfTrims[i].mK == k )
        {
            //! export trim
            *pTrim = lfTrim.mLfTrims[i];
            return 0;
        }
    }

    return -1;
}

}
