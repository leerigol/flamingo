#ifndef IPHYCH_H
#define IPHYCH_H

#include "../iphy.h"

#include "./iphyafe.h"
#include "../wpu/iphywpu.h"

#include "../iphydac.h"
#include "../relay/iphyrelay.h"

#include "chcal.h"
#include "../adc/iphyadc.h"

namespace dso_phy {

/*!
 * \brief The IPhyCH class
 * - 通道有高阻和50欧两个通路
 * - 配置依赖于当前的通路选择
 */
class IPhyCH : public IPhy
{
public:
    IPhyCH( Chan id = chan1 );

public:
    virtual void flushWCache();

    void setId( Chan id );
    Chan getId();

    void attachAfe( IPhyAfe *pAfe );
    void attachDac( IPhyDac *pDac, int dacChan );
    void attachWpu( IPhyWpu *pWpu );
    void attachRelay( IPhyRelay *pRelay );

public:
    DsoErr setOnOff( bool bOnOff );

    DsoErr setScale( int scale );
    DsoErr setOffset( int offset );
    DsoErr applyOffset( int offset );
    DsoErr setCoupling( Coupling coup );

    DsoErr setThTrim(int value);

    DsoErr setImpedance( Impedance imp );

    DsoErr setBw( Bandwidth bw, Impedance imp );
    DsoErr setCalBw( Bandwidth bw, Impedance imp );

    void setDac( int dac );
    dac_reg getDac();
    dac_reg getDacReg();

    //! attach cal data
    void setCal( void *pCal );
    void setHizTrim( void *pTrim );

    DsoErr setHzScaleOnly( int scale );
    DsoErr setLzScaleOnly( int scale, int flt = 0 );

    DsoErr setHzScaleOnly( int scale, afeCfgPair *pCfgPair );
    DsoErr setLzScaleOnly( int scale, int flt, afeCfgPair *pCfgPair );

    void setAfeScale( float realScale );
    int getAfeScale();

    void setAfeGnd( int afeGnd );
    int getAfeGnd();

    quint16 getHzVos( int scale );

    void syncData();

public:


protected:
    Impedance mImp;
    int mScale;
    int mOffset;
    bool mOnOff;

    Chan mChan;         //! chid is used to identify the chan1/2/3/4
    int mDacChan;

    int mBw100Bit, mBw20Bit;
    Bandwidth mBw;
public:
                        //! link phy
    IPhyAfe *m_pAfe;
    IPhyDac *m_pDac;
    IPhyWpu *m_pWpu;
    IPhyRelay *m_pRelay;

    chCal *m_pCal;
    chHizLfTrim *m_pHizLfTrim;

    adcCalGroup *m_pAdcCoreGo;

    int mAfeScale;      //! afe scale
    int mAfeGnd;
};

}

#endif // IPHYCH_H
