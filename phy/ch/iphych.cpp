
#include "iphych.h"
#include "../../include/dsocfg.h"


namespace dso_phy {

/*!
 * \brief IPhyCH::IPhyCH
 * \param id
 * constructor
 */
IPhyCH::IPhyCH( Chan id )
{
    m_pAfe = NULL;
    m_pDac = NULL;
    m_pWpu = NULL;
    m_pRelay = NULL;

    m_pCal = NULL;
    m_pHizLfTrim = NULL;

    m_pAdcCoreGo = NULL;

    mImp = IMP_1M;
    mScale = 100000;
    mOffset= 0;

    mAfeScale = 100000;
    mAfeGnd = 128;

    setId( id );

    mAfeScale = 100000;
    mAfeGnd = 128;

    mBw = BW_OFF;
}

void IPhyCH::flushWCache()
{
    Q_ASSERT( NULL != m_pAfe );
    Q_ASSERT( NULL != m_pDac );

    m_pAfe->flushWCache();
    m_pDac->flushWCache();
}

/* 解决校准小档位50欧零点偏移问题，在切换阻抗时，零点需要重新配置. by hxh*/
void IPhyCH::syncData()
{
    if( mScale <= 2000 && mImp == IMP_50 )
    {
        applyOffset( mOffset );
    }
}
/*!
 * \brief IPhyCH::setId
 * \param id
 * ch id
 * id = 1/2/3/4
 *
 */
void IPhyCH::setId( Chan id )
{
    mChan = id;
                                //! see K7M_FCU.xlsm
    mDacChan = mChan - chan1 + 10;
                                //! set relay cfg
    mBw20Bit = mChan - chan1 + 12;
    mBw100Bit = mChan - chan1 + 8;
}
/*!
 * \brief IPhyCH::getId
 * \return
 * get id
 */
Chan IPhyCH::getId()
{
    return mChan;
}

void IPhyCH::attachAfe( IPhyAfe *pAfe )
{
    Q_ASSERT( NULL != pAfe );
    m_pAfe = pAfe;
}
void IPhyCH::attachDac( IPhyDac *pDac, int dacChan )
{
    Q_ASSERT( NULL != pDac );
    m_pDac = pDac;
    mDacChan = dacChan;
}
void IPhyCH::attachWpu( IPhyWpu *pWpu )
{
    Q_ASSERT( NULL != pWpu );

    m_pWpu = pWpu;
}

void IPhyCH::attachRelay( IPhyRelay *pRelay )
{
    Q_ASSERT( NULL != pRelay );

    m_pRelay = pRelay;
}

/*!
 * \brief IPhyCH::setOnOff
 * \param bOnOff
 * \return
 * on off
 */
DsoErr IPhyCH::setOnOff( bool bOnOff )
{
    mOnOff = bOnOff;

    if ( mImp == IMP_1M )
    {
        if ( bOnOff )
        {
            m_pAfe->setLzNpd( 0 );
            m_pAfe->setHzNpd( 1 );
        }
        else
        {
            m_pAfe->setHzNpd( 0 );
        }
    }
    else if ( mImp == IMP_50 )
    {
        if ( bOnOff )
        {
            m_pAfe->setHzNpd( 0 );
            m_pAfe->setLzNpd( 1 );
        }
        else
        {
            m_pAfe->setLzNpd( 0 );
        }
    }
    else
    {
        Q_ASSERT( false );
    }

    return ERR_NONE;
}

/*!
 * \brief IPhyCH::setScale
 * \param scale
 * \return
 * scale is in uv unit
 */
DsoErr IPhyCH::setScale( int scale )
{
    DsoErr err;

    //! save
    mScale = scale;

    if ( mImp == IMP_50 )
    {
        err = setLzScaleOnly( scale );
        if ( err != ERR_NONE ) return err;
    }
    else if ( mImp == IMP_1M )
    {
        //! config
        err = setHzScaleOnly( scale );
        if ( err != ERR_NONE ) return err;
    }
    else
    {}

    return ERR_NONE;
}
/*!
 * \brief IPhyCH::setOffset
 * \param offset
 * \return
 * offset is in uv unit
 */
DsoErr IPhyCH::setOffset( int offset )
{
    applyOffset( offset );

    //! afe gnd
    Q_ASSERT( mAfeScale != 0 );

    //! 偏移超过86V后，乘以8会溢出，运算过程中需要转化为qlonglong
    mAfeGnd = adc_center + (qlonglong) offset*adc_vdiv_dots/mAfeScale;

    return ERR_NONE;
}

DsoErr IPhyCH::setThTrim(int value)
{
    if( mImp == IMP_50 )
    {
        float TH_TRIM = 0.0;
        float Offset  = 0.25 * (-2.5 + 5*value/65535);
        // 计算TH_TRIM寄存器值
        if (m_pAfe->getGsel1() == 1) //GSEL1寄存器值为1，增益x3.12
        {
            // Target_Gain是临时变量，无物理意义
            // Offset是前端50欧管脚处的偏移电压
            // 需要根据偏移的DAC值以及硬件参数换算。
           float Target_Gain = 1.0154;

           TH_TRIM = (Target_Gain - (0.002106*Offset + 1.019185) ) / -2.41e-4;
        }
        else
        {
           float Target_Gain = 1.00936;
           TH_TRIM = (Target_Gain - (0.000877*Offset + 1.011977) ) / -1.54e-4;
        }

        // 结果取整，钳位
        int th_trim = (int)TH_TRIM;

        if ( th_trim < 0 )
        {
            th_trim = 0;
        }
        else if ( th_trim > 31 )
        {
            th_trim = 31;
        }

        // 将TH_TRIM寄存器值写入芯片
        //X1605_REG( TestSite ).TH_TRIM = TH_TRIM;
        //WriteX1605Reg( fo, 'TH_TRIM', TestSite, 0 );
        m_pAfe->setThTrim( th_trim );
    }

    return ERR_NONE;
}

//! only apply offset
DsoErr IPhyCH::applyOffset( int offset )
{
    int offs=0;
    int gnd=0;

    mOffset = offset;
    if ( mImp == IMP_1M )
    {
        Q_ASSERT( m_pCal != NULL );
        Q_ASSERT( m_pCal->mLsb1M != 0 );

        chCalProxy proxy( m_pCal );

        float lsb;
        //! expand offset
        if ( mScale > 50000 )
        {
            gnd = proxy.findHzGnd( mScale, 0 );
            lsb = m_pCal->mLsb1M_X;
        }
        else
        {
            gnd = proxy.findHzGnd( mScale, 1 );
            lsb = m_pCal->mLsb1M;
        }

        //! find K
        float kx;
        kx = proxy.findHzKx( mScale);

        Q_ASSERT( lsb != 0 );
        offs = gnd + offset*kx / lsb;
    }
    else if ( mImp == IMP_50 )
    {
        float lsb50;

        Q_ASSERT( m_pCal != NULL );
        if ( mScale > 100000 )
        {
            lsb50 = m_pCal->mLsb50_att;
        }
        else
        {
            lsb50 = m_pCal->mLsb50;
        }

        Q_ASSERT( lsb50 != 0 );
        Q_ASSERT( m_pCal != NULL );

        chCalProxy proxy( m_pCal );

        //! find scale
        //! \todo flt
        gnd = proxy.findLzGnd( mScale, 0, mBw);
        Q_ASSERT( lsb50 != 0 );
        offs = gnd + ( offset/lsb50 );
    }
    else
    {}

    if ( offs < 0 )
    {
        offs = 0;
    }

    if ( offs > 0xffff )
    {
        offs = 0xffff;
    }

    Q_ASSERT( m_pDac != NULL );
    m_pDac->set( offs, mDacChan );

    //setThTrim(offs);

    return ERR_NONE;
}

/*!
 * \brief IPhyCH::setCoupling
 * \param coup
 * \return
 * DC/AC/GND
 */
DsoErr IPhyCH::setCoupling( Coupling coup )
{
    if ( coup == DC )
    {
        m_pAfe->setGpo0( 1 );
    }
    else if ( coup == AC )
    {
        m_pAfe->setGpo0( 0 );
    }
    else
    { qWarning()<<__FUNCTION__<<__LINE__; }

    return ERR_NONE;
}

/*!
 * \brief IPhyCH::setImpedance
 * \param imp
 * \return
 * impedance
 */
DsoErr IPhyCH::setImpedance( Impedance imp )
{
    if ( imp == IMP_1M )
    {
        mImp = imp;
        m_pAfe->setOlEn( 0 );
        m_pAfe->setOSel( 2 );
    }
    else if ( imp == IMP_50 )
    {
        mImp = imp;
        m_pAfe->setOlEn( 1 );
        m_pAfe->setOSel( 0 );

        syncData();
    }
    else
    {
    }

    //! post on/off
    setOnOff( mOnOff );

    return ERR_NONE;
}

DsoErr IPhyCH::setCalBw( Bandwidth bw,
                      Impedance /*imp*/ )
{

    if ( bw == BW_20M )
    {
        m_pRelay->write( 1, mBw20Bit );
        m_pRelay->write( 1, mBw100Bit );

        //qDebug() << mChan << "setCalBw=20M";
    }
    else //if ( bw == BW_OFF )
    {
        m_pRelay->write( 0, mBw20Bit );
        m_pRelay->write( 0, mBw100Bit );

        //qDebug() << mChan << "setCalBw=off";
    }
    return ERR_NONE;
}

DsoErr IPhyCH::setBw( Bandwidth bw,
                      Impedance /*imp*/ )
{
    Q_ASSERT( NULL != m_pRelay );

    if ( bw == BW_20M )
    {
        m_pRelay->write( 1, mBw20Bit );
        m_pRelay->write( 1, mBw100Bit );
    }
    else if ( bw == BW_100M || bw == BW_250M )
    {
        m_pRelay->write( 0, mBw20Bit );
        m_pRelay->write( 1, mBw100Bit );
    }
    else //if ( bw == BW_OFF )
    {
        m_pRelay->write( 0, mBw20Bit );
        m_pRelay->write( 0, mBw100Bit );
    }

    mBw = bw;

    syncData();

    return ERR_NONE;
}

void IPhyCH::setDac( int dac )
{
    Q_ASSERT( m_pDac != NULL );
    m_pDac->set( dac, mDacChan );
}
dac_reg IPhyCH::getDac()
{
    Q_ASSERT( m_pDac != NULL );
    return m_pDac->get( mDacChan );
}
dac_reg IPhyCH::getDacReg()
{
    Q_ASSERT( m_pDac != NULL );
    return m_pDac->inReg( mDacChan );
}

void IPhyCH::setCal( void *pCal)
{
    Q_ASSERT( pCal != NULL );

    m_pCal = ((chCal*)(pCal));

    //! apply otp
    Q_ASSERT( m_pCal != NULL );
    m_pAfe->setAzVos( m_pCal->mAzVosTrim );
    m_pAfe->setHzVosTrim2( m_pCal->mVosTrim2 );
    m_pAfe->setHzVosTrim3( m_pCal->mVosTrim3 );

    m_pAfe->flushWCache();

    //LOG_PHY()<<m_pCal->mAzVosTrim<<m_pCal->mVosTrim2<<m_pCal->mVosTrim3;
}

void IPhyCH::setHizTrim( void *pTrim )
{
    Q_ASSERT( NULL != pTrim );

    m_pHizLfTrim = (chHizLfTrim*)pTrim;
}

DsoErr IPhyCH::setHzScaleOnly( int scale )
{
    //! find
    afeCfgPair *pCfgPair;


    chCalProxy proxy( m_pCal );

    pCfgPair = proxy.findHzScale( scale );
    if ( NULL == pCfgPair )
    {
        return ERR_CH_HZ_GAIN_DISPATCH;
    }

    setHzScaleOnly( scale, pCfgPair );

    //! wpu gain
    Q_ASSERT( scale != 0 );
    float wpuGain = pCfgPair->mfVal/(scale*1.0e-6f);
    m_pWpu->setCHGain( mChan - chan1, wpuGain );

    //! afe scale
    setAfeScale( pCfgPair->mfVal );

#ifdef _DEBUG
    pCfgPair->showHz();
#endif

    //! lf trim
    HizLfTrimProxy hizLfProx;
    hizLfTrim lfTrim;
    int ret;
    ret = hizLfProx.selectHizLfTrim( *m_pHizLfTrim,
                               pCfgPair->mCfg.hzK,
                               &lfTrim );
    if ( ret != 0 )
    {
        return ERR_CH_HZ_TRIM_DISPATCH;
    }
    else
    {
        //! dc trim otp
        m_pAfe->setLfTrim( lfTrim.mTrims[1] );
        m_pAfe->setMfcTrim( lfTrim.mTrims[2] );
        m_pAfe->setHfTrim( lfTrim.mTrims[3] );

        m_pAfe->setHzFilter( lfTrim.mRSel, lfTrim.mCSel );
    }

    return ERR_NONE;
}

DsoErr IPhyCH::setLzScaleOnly( int scale, int flt )
{
    //! find
    afeCfgPair *pCfgPair;

    chCalProxy proxy( m_pCal );

    pCfgPair = proxy.findLzScale( scale, flt, mBw );
    if ( NULL == pCfgPair )
    {
        return ERR_CH_HZ_GAIN_DISPATCH;
    }

    setLzScaleOnly( scale, flt, pCfgPair );

    //! wpu gain
    if( scale != 0 )
    {
        float wpuGain = pCfgPair->mfVal/(scale*1.0e-6f);
        m_pWpu->setCHGain( mChan - chan1, wpuGain );

        //qDebug() << "chan:" << getId() << "scale:" << scale << "wpu gain:" << wpuGain;
    }
    else
    {
        qDebug() << "Divide by 0" << __FUNCTION__ << __LINE__;
        //Q_ASSERT( scale != 0 );
    }

    setAfeScale( pCfgPair->mfVal );

#ifdef _DEBUG
    pCfgPair->showLz();
#endif
    return ERR_NONE;
}

DsoErr IPhyCH::setHzScaleOnly( int scale,
                               afeCfgPair *pCfgPair )
{
    //! config
    afeCfg *pCfg;
    pCfg = &pCfgPair->mCfg;
    m_pAfe->setK( pCfg->hzK );
    m_pAfe->setHzSga1Gs( pCfg->hzGs );

    m_pAfe->setAtten( pCfg->hzAtten );

    m_pAfe->setGtrim4( pCfg->hzSga4,
                       pCfg->hzSga4Trim
                       );
    m_pAfe->setGtrim5( pCfg->hzSga5,
                       pCfg->hzSga5Trim );

    //! vos
    m_pAfe->setVosExp( getHzVos(scale) );

    return ERR_NONE;
}

DsoErr IPhyCH::setLzScaleOnly( int scale,
                               int flt,
                               afeCfgPair *pCfgPair )
{
    Q_ASSERT( NULL != pCfgPair );

    //! 开衰减
    if ( scale > 100000 )
    {
        m_pAfe->setGpo1( 0 );
        m_pAfe->setISel( 0 );   //! a

        m_pAfe->setOSel( flt );

        m_pAfe->setVosExp( 1 );
    }
    else
    {
        m_pAfe->setGpo1( 1 );
        m_pAfe->setISel( 2 );   //! c

        m_pAfe->setOSel( flt );

        m_pAfe->setVosExp( 1 );
    }

    //! config gain
    afeCfg *pCfg;
    pCfg = &pCfgPair->mCfg;
    m_pAfe->setGtrim1( pCfg->lzSga1,
                       pCfg->lzSga1Trim
                       );

    m_pAfe->setGtrim2( pCfg->lzSga2,
                       pCfg->lzSga2Trim
                       );

    m_pAfe->setGtrim3( pCfg->lzSga3,
                       pCfg->lzSga3Trim
                       );

    m_pAfe->setGtrim4( pCfg->lzSga4,
                       pCfg->lzSga4Trim
                       );

    m_pAfe->setGtrim5( pCfg->lzSga5,
                       pCfg->lzSga5Trim );

    return ERR_NONE;
}

void IPhyCH::setAfeScale( float realScale )
{
    mAfeScale = realScale * 1.0e6;
}
int IPhyCH::getAfeScale()
{
    return mAfeScale;
}

void IPhyCH::setAfeGnd( int afeGnd )
{
    mAfeGnd = afeGnd;
}

int IPhyCH::getAfeGnd()
{
    return mAfeGnd;
}

quint16 IPhyCH::getHzVos( int scale )
{
    return scale <= 50000 ? 1 : 0;
}

}
