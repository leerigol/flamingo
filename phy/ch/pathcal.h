#ifndef EXTCAL_H
#define EXTCAL_H

namespace dso_phy {

//!
//! \brief The pathCal struct
//! for ext,ext5, la
struct pathCal
{
   int mGnd;        //! gnd
   float mLsb;      //! lsb

   pathCal();
   void setCal( int gnd, float lsb );
};

}

#endif // EXTCAL_H
