#include "pathcal.h"

namespace dso_phy {

pathCal::pathCal()
{
    mGnd = 32768;
    mLsb = 0;
}

void pathCal::setCal( int gnd, float lsb )
{
    if ( lsb != 0 )
    {
        mGnd = gnd;
        mLsb = lsb;
    }
}

}


