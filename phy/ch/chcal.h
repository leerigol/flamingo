#ifndef CHCAL
#define CHCAL

#include "../../include/dsotype.h"
#include <QtCore>
#include <QTextStream>
using namespace DsoType;

namespace dso_phy {

//struct lineKM
//{
//    float mK, mM;
//    float mMul;
//};

//struct scaleGnd
//{
//    int mScale;
//    int mGnd;
//};

union afeCfg
{
    //! hz cfg
    struct
    {
        quint8 hzK : 6;
        quint8 hzVos : 2;
        quint8 hzGs : 3;
        quint8 hzAtten : 4;
        quint8 hzSga4 : 3;
        quint8 hzSga4Trim : 5;
        quint8 hzSga5 : 3;
        quint8 hzSga5Trim : 5;
    };
    //! lz cfg
    struct
    {
        quint8 lzAtt : 3;
        quint8 lzSga1 : 3;
        quint8 lzSga1Trim : 3;

        quint8 lzSga2 : 3;
        quint8 lzSga2Trim : 3;

        quint8 lzSga3 : 3;
        quint8 lzSga3Trim : 3;

        quint8 lzFlt : 2;

        quint8 lzSga4 : 3;
        quint8 lzSga4Trim : 5;

        quint8 lzSga5 : 3;
        quint8 lzSga5Trim : 5;

        quint8 lzBw:4;// by hxh
    };
    qulonglong mKey;
};

struct afeCfgPair
{
    afeCfg    mCfg; //! key
    int       mVal;

    float     mfVal;

    afeCfgPair();
    void clear();

    void setHzCfg( quint8 k,
                   quint8 vos,
                   quint8 gs,
                   quint8 att,
                   quint8 sga4, quint8 sga4Trim,
                   quint8 sga5, quint8 sga5Trim );

    void setLzCfg( quint8 att,
                   quint8 sga1, quint8 sga1Trim,
                   quint8 sga2, quint8 sga2Trim,
                   quint8 sga3, quint8 sga3Trim,
                   quint8 flt,
                   quint8 sga4, quint8 sga4Trim,
                   quint8 sga5, quint8 sga5Trim,
                   quint8 bw = BW_20M);

    bool hzGndEqual( afeCfgPair *pPair, int matchCnt=6 );
    bool lzGndEqual( afeCfgPair *pPair, int mathCnt=10 );

    void setValue( int val );
    void setValue( float val );

    void exportHz( QTextStream &stream );
    void exportLz( QTextStream &stream );

    void showHz();
    void showLz();
};

struct intkey_float
{
    int mKey;
    float mVal;
};

struct chCal
{
    //! ---- commmon ----
    int mVosTrim2, mVosTrim3;
    int mAzVosTrim;

    //! ---- 1M ----
    //! cfg 1M
    float mLsb1M, mLsb1M_X;     //! normal, expand lsb

    //! K Attr
    int mhzKs;                    //! 0,15,16,
    intkey_float _hzKs[ 16 ];     //! *1,*0.75,...

    //! --- cal ---
    //! --- lg/hg
    float mHzLgHg[2];    //! lg/hg

    //! Attens
    int   mhzAtts;          //! *1,*0.8...
                            //! 0,1,2,3,4,5,
    intkey_float _hzAtts[ 8 ];

    //! sga4
    float mHzSga4Sel[2];   //! lg/hg
    float mHzSga4Trim[2];

    //! sga5
    float mHzSga5Sel[2];   //! lg/hg
    float mHzSga5Trim[2];

    //! ref scale
    float mHzRefScale;

    //! scales
    int mHzScales;
    afeCfgPair _hzScales[1024];

    //! gnds 9 * 2 * 2 * 2
    //! key -- gnd
    int mHzGnds;
    afeCfgPair _hzGnds[1024*2];

    //! ---- 50 ----
    //! cfg 50
    float mLsb50, mLsb50_att;

    //! sga1
    float mLzSga1[2];      //! lg/hg
    float mLzSga1Trim[2];

    float mLzSga1_att[2];

    //! sga2
    float mLzSga2[3];
    float mLzSga2Trim[4];

    //! sga3
    float mLzSga3[2];
    float mLzSga3Trim[4];

    //! sga4
    float mLzSga4[2];
    //! sga5
    float mLzSga5[2];

    //! filter sga4
    float mLzSga4Flt[2];
    //! filter sga5
    float mLzSga5Flt[2];

    //! ref scale
    float mLzRefScale[4];   //! 0 -- 2x_normal 1 -- 2x_flt
                            //! 2 -- 20x_normal 3 -- 20x_flt

    //! scales
    int mLzScales_2x_normal;
    afeCfgPair _lzScales_2x_normal[1024];

    int mLzScales_2x_flt;
    afeCfgPair _lzScales_2x_flt[1024];

    int mLzScales_20x_normal;
    afeCfgPair _lzScales_20x_normal[1024];

    int mLzScales_20x_flt;
    afeCfgPair _lzScales_20x_flt[1024];

    //! gnds
    //! key -- gnd
    int mLzGnds;
    afeCfgPair  _lzGnds[1024*4];
};

class chCalProxy
{
public:
    chCalProxy( chCal *pCal );

public:
    //! hz
    afeCfgPair *findHzScale( int scale );
    //! vos: 1 -- normal
    //!      0 -- expand
    int findHzGnd( int scale, int vos=1 );

    float findHzKx( int scale );

    //! lz
    //! flt : 0 -- normal
    //!       1 -- flt on
    afeCfgPair *findLzScale( int scale,
                             int flt ,
                             int bw = BW_20M);

    int findLzGnd( int scale,
                   int flt,
                   int bw = BW_20M);

protected:
    afeCfgPair *findNearScale( int scale,
                               afeCfgPair *pPairs,
                               int cnt,
                               int jumpPoints[],
                               int jumpCnt);

    afeCfgPair *findNearLzScale( int scale,
                               afeCfgPair *pPairs,
                               int cnt,
                               int jumpPoints[],
                               int jumpCnt,
                               int bw =BW_20M );

    int findNearHzGnd( afeCfgPair *pPair,
                       afeCfgPair *pPairs,
                       int cnt );

    int findNearLzGnd( afeCfgPair *pPair,
                       afeCfgPair *pPairs,
                       int cnt);

public:
    chCal *m_pCal;

    afeCfgPair hzDefScale;
    afeCfgPair lzDefScale;
};

struct hzVgaCfg
{
    int mK, mGSel, mAtten;
    int mGSel4, mGTrim4;
    int mGSel5, mGTrim5;

    float mD4Gain, mD5Gain;

    //! internal used
    bool mbValid;
    int mSga;

public:
    hzVgaCfg();
public:
    void cacheRst();
    void cacheMinIn( int ke, int sel,
                     int atten,
                     int sgaSel, int sga );

    bool sgaNearest( int sga );
};

struct lzVgaCfg
{
    int mSga1Sel, mSga1Trim;
    int mSga2Sel, mSga2Trim;
    int mSga3Sel, mSga3Trim;

    int mSga4Sel, mSga4Trim;
    int mSga5Sel, mSga5Trim;

    //! digital gain
    float mD4Gain, mD5Gain;


    //! internal used
    bool mbValid;
    int mSga;

public:
    lzVgaCfg();
public:
    void cacheRst();
    void cacheMinIn( int sga1, int sga1_trim,
                     int sga2, int sga2_trim,
                     int sga3, int sga3_trim,
                     int sga4, int sga4_trim
                     );

    bool sgaNearest( int sga );

};

class gainDispatch
{
public:
    static float getKAtten( chCal *pCal, int k );

public:
    gainDispatch();

public:
    DsoErr dispatchHzGain( int scaleAim,
                       chCal &calPara,
                       hzVgaCfg &cfg );

    DsoErr dispatchHzGnd( int scaleAim,
                          chCal &calPara,
                          int &dacGnd );

    DsoErr dispatchLzGain( int scaleAim,
                          chCal &calPara,
                          lzVgaCfg &cfg );

    DsoErr dispatchLzGnd( int scaleAim,
                          chCal &calPara,
                          int &dacGnd );

private:

};

//! lf_trim
struct hizLfTrim
{
    int mK;
    int mTrims[4];      //! dc/lf/mfc/hf
    int mCSel, mRSel;
};

struct chHizLfTrim
{
    int mTrimCount;
    hizLfTrim mLfTrims[ 10 ];  //! k=0,15,16,23,24,27,28,29
};

class HizLfTrimProxy
{
public:
    int selectHizLfTrim( chHizLfTrim &lfTrim,
                         int k,
                         hizLfTrim *pTrim );
};

}

#endif // CHCAL

