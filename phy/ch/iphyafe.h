#ifndef IPHYAFE_H
#define IPHYAFE_H

#include "../iphy.h"
#include "../ic/damrey.h"


#ifdef DAMREY_VER_1_5_x

namespace dso_phy {

class IPhyAfe : public IPhy,
                public damrey
{
public:
    IPhyAfe( int id = 0 );

    virtual void init();
    virtual void flushWCache();

    void setId( int id );
public:

    void setLozScale( int scale );
    void setHizScale( int scale );

//    float getHizAtten( int scale );
//    float getLozAtten( int scale );

public:
    typedef void (*pf_set_reg)( damrey_reg );
    //! reg0
    void setRmsGain( damrey_reg gain );
    void setAtten( damrey_reg att );
    void setHzVosTrim2( damrey_reg trim );
    void setHzVosTrim3( damrey_reg trim );

    //! read
    damrey_reg getOvDet();
    damrey_reg getZDet();
    damrey_reg getTp2ZDet();

    damrey_reg getRB();
    damrey_reg getVersion();

    damrey_reg getGsel1();

    void setThTrim( damrey_reg value);

    //! reg1
    void setHzFilter(
                    damrey_reg r,
                    damrey_reg c,
                    damrey_reg c0,
                    damrey_reg c1,
                    damrey_reg c2,
                    damrey_reg c3 );
    void setHzFilter(
                    damrey_reg r,
                    damrey_reg c
                     );

    void setHzSga1Gs( damrey_reg sel );

    //! reg2
    void setGpo0( damrey_reg hl );
    void setHzVosTrim4( damrey_reg trim );

    void setFbSel( damrey_reg sel );
    void setFsSel( damrey_reg sel );
    void setProg( damrey_reg prog );
    void setFRw( damrey_reg rw );
    void setDsel( damrey_reg sel );
    void setVfsCtrl( damrey_reg ctrl );

    //! reg3
    void setDcTrim( damrey_reg trim );
    void setISel( damrey_reg sel );
    void setVosTrim1( damrey_reg trim );

    void setTpSw( damrey_reg tpsw );
    void setTp1Typ( damrey_reg tp1typ );
    void setTpPol( damrey_reg tppol );

    //! reg4
    void setEnDclp( bool bEn );
    void setDacSel( bool bHl );

    void setGsel(damrey_reg gsel);

    void setGtrim1( damrey_reg sel,
                    damrey_reg trim );
    void setGtrim2( damrey_reg sel,
                    damrey_reg trim );
    void setGtrim3( damrey_reg sel,
                    damrey_reg trim );

    void setVosTrim2( damrey_reg trim );

    //! reg5
    void setVosTrim3( damrey_reg trim );
    void setOen( damrey_reg output );

    void setOSel( damrey_reg sel );
    void setGtrim4( damrey_reg sel, damrey_reg gain );

    //! reg6
    void setOlEn( damrey_reg olen );
    void setVosTrim4( damrey_reg trim );
    void setVosTrim5( damrey_reg trim );

    void setGtrim5( damrey_reg sel, damrey_reg gain );

    //! reg7
    void setVos( damrey_reg vos );

    //! reg8
    void setK( damrey_reg k );
    void setVosExp( damrey_reg vosExp );
    void setGpo1( damrey_reg gp );
    void setLfTrim( damrey_reg trim );

    //! reg9
    void setHfTrim( damrey_reg trim );
    void setMfcTrim( damrey_reg trim );

    //! reg10

    //! 11 bit
    void setAzVos( damrey_reg vos );
    void setAzIsel( damrey_reg sel );
    void setEnZd( damrey_reg ed );

    void setHzNpd( damrey_reg npd );
    void setLpp( damrey_reg lpp );

    //! reg11
    damrey_reg getFuseRb();

    //! write
    void setNlTrim41( damrey_reg trim );
    void setNlTrim40( damrey_reg trim );
    void setCcTrim4B( damrey_reg trim );
    void setCcTrim4A( damrey_reg trim );

    void setCcTrim3( damrey_reg trim );

    //! reg12
    void setLzNpd( damrey_reg lznpd );
    void setHzITrim( damrey_reg trim );
    void setLzITrim( damrey_reg trim );
    void setNlTrim31( damrey_reg trim );

    void setNlTrim30( damrey_reg trim );

    //! reg13
    void setNlTrim21( damrey_reg trim );
    void setNlTrim20( damrey_reg trim );
    void setNlTrim11( damrey_reg trim );
    void setNlTrim10( damrey_reg trim );

    //! reg 14
    void setCcTrim2( damrey_reg trim );
    void setVclp( damrey_reg lp );
    void setCcTrim1( damrey_reg trim );

public:
    void showPara();

public:
    void readReg( damrey_reg &reg, int addr, int chan );
    void readReg( damrey_reg &reg, int addr );
private:
//    damrey mIc;

    int mId;
    int mCs;
};

}
#endif


#ifdef DAMREY_VER_1_5

namespace dso_phy {

class IPhyAfe : public IPhy,
                public damrey
{
public:
    IPhyAfe( int id = 0 );

    virtual void init();
    virtual void flushWCache();

    void setId( int id );
public:

    void setLozScale( int scale );
    void setHizScale( int scale );

//    float getHizAtten( int scale );
//    float getLozAtten( int scale );

public:
    typedef void (*pf_set_reg)( damrey_reg );
    //! reg0
    void setRmsGain( damrey_reg gain );
    void setAtten( damrey_reg att );
    void setHzVosTrim2( damrey_reg trim );
    void setHzVosTrim3( damrey_reg trim );

    //! read
    damrey_reg getOvDet();
    damrey_reg getZDet();
    damrey_reg getTp2ZDet();

    damrey_reg getRB();
    damrey_reg getVersion();

    //! reg1
    void setHzFilter(
                    damrey_reg r,
                    damrey_reg c,
                    damrey_reg c0,
                    damrey_reg c1,
                    damrey_reg c2,
                    damrey_reg c3 );
    void setHzFilter(
                    damrey_reg r,
                    damrey_reg c
                     );

    void setHzSga1Gs( damrey_reg sel );

    //! reg2
    void setGpo0( damrey_reg hl );
    void setHzVosTrim4( damrey_reg trim );

    void setFbCSel( damrey_reg sel );
    void setFbRSel( damrey_reg sel );
    void setProg( damrey_reg prog );
    void setFRw( damrey_reg rw );
    void setDsel( damrey_reg sel );
    void setVfsCtrl( damrey_reg ctrl );

    //! reg3
    void setDcTrim( damrey_reg trim );
    void setISel( damrey_reg sel );
    void setVosTrim1( damrey_reg trim );

    void setTpSw( damrey_reg tpsw );
    void setTp1Typ( damrey_reg tp1typ );
    void setTpPol( damrey_reg tppol );

    //! reg4
    void setEnDclp( bool bEn );
    void setDacSel( bool bHl );

    void setGtrim1( damrey_reg sel,
                    damrey_reg trim );
    void setGtrim2( damrey_reg sel,
                    damrey_reg trim );
    void setGtrim3( damrey_reg sel,
                    damrey_reg trim );

    void setVosTrim2( damrey_reg trim );

    //! reg5
    void setVosTrim3( damrey_reg trim );
    void setOen( damrey_reg output );

    void setOSel( damrey_reg sel );
    void setGtrim4( damrey_reg sel, damrey_reg gain );

    //! reg6
    void setOlEn( damrey_reg olen );
    void setVosTrim4( damrey_reg trim );
    void setVosTrim5( damrey_reg trim );

    void setGtrim5( damrey_reg sel, damrey_reg gain );

    //! reg7
    void setVos( damrey_reg vos );

    //! reg8
    void setK( damrey_reg k );
    void setVosExp( damrey_reg vosExp );
    void setGpo1( damrey_reg gp );
    void setLfTrim( damrey_reg trim );

    //! reg9
    void setHfTrim( damrey_reg trim );
    void setMfcTrim( damrey_reg trim );

    //! reg10

    //! 11 bit
    void setAzVos( damrey_reg vos );
    void setAzIsel( damrey_reg sel );
    void setEnZd( damrey_reg ed );

    void setHzNpd( damrey_reg npd );
    void setLpp( damrey_reg lpp );

    //! reg11
    damrey_reg getFuseRb();

    //! write
    void setNlTrim41( damrey_reg trim );
    void setNlTrim40( damrey_reg trim );
    void setCcTrim4B( damrey_reg trim );
    void setCcTrim4A( damrey_reg trim );

    void setCcTrim3( damrey_reg trim );

    //! reg12
    void setLzNpd( damrey_reg lznpd );
    void setHzITrim( damrey_reg trim );
    void setLzITrim( damrey_reg trim );
    void setNlTrim31( damrey_reg trim );

    void setNlTrim30( damrey_reg trim );

    //! reg13
    void setNlTrim21( damrey_reg trim );
    void setNlTrim20( damrey_reg trim );
    void setNlTrim11( damrey_reg trim );
    void setNlTrim10( damrey_reg trim );

    //! reg 14
    void setCcTrim2( damrey_reg trim );
    void setVclp( damrey_reg lp );
    void setCcTrim1( damrey_reg trim );

public:
    void showPara();

public:
    void readReg( damrey_reg &reg, int addr, int chan );
    void readReg( damrey_reg &reg, int addr );
private:
//    damrey mIc;

    int mId;
    int mCs;
};

}
#endif

#ifdef DAMREY_VER_1_4

namespace dso_phy {

class IPhyAfe : public IPhy,
                public damrey

{
public:
    IPhyAfe( int id = 0 );

    virtual void init();
    virtual void flushWCache();

    void setId( int id );
    quint32 getVersion();
public:


public:
    typedef void (*pf_set_reg)( damrey_reg );
    //! reg0
    void setOvDet( damrey_reg det );
    void setOlEn( damrey_reg olen );

    void setOvLvlFine( damrey_reg lvl );
    void setTpSw( damrey_reg sw );
    void setAtten( damrey_reg att );
    void setHzVosTrim2( damrey_reg trim );
    void setHzVosTrim3( damrey_reg trim );

    //! read
    damrey_reg getOvDet();
    damrey_reg getZDet();

    damrey_reg getRB();

    //! reg1
    void setHzFilter(
                    damrey_reg r,
                    damrey_reg c,

                    damrey_reg c0,
                    damrey_reg c1,
                    damrey_reg c2,
                    damrey_reg c3 );

    void setHzFilter(
                    damrey_reg r,
                    damrey_reg c
                     );


    void setHzSga1Gs( damrey_reg sel );

    //! reg2
    void setGpo0( damrey_reg hl );
    void setHzVosTrim4( damrey_reg trim );

    void setFbCSel( damrey_reg sel );
    void setFbRSel( damrey_reg sel );
    void setProg( damrey_reg prog );
    void setFRw( damrey_reg rw );
    void setDsel( damrey_reg sel );
    void setVfsCtrl( damrey_reg ctrl );

    //! reg3
    void setDcTrim( damrey_reg trim );
    void setISel( damrey_reg sel );
    void setVClamp( damrey_reg clamp );
    void setOvLvl( damrey_reg level );

    //! reg4
    void setEnDclp( bool bEn );
    void setDacSel( bool bHl );

    void setGtrim1( damrey_reg sel,
                    damrey_reg trim );
    void setGtrim2( damrey_reg sel,
                    damrey_reg trim );
    void setGtrim3( damrey_reg sel,
                    damrey_reg trim );

    void setVosTrim2( damrey_reg trim );

    //! reg5
    void setVosTrim3( damrey_reg trim );
    void setTp1Typ( damrey_reg tpe );
    void setTpPol( damrey_reg pol );
    void setOen( damrey_reg output );

    void setOSel( damrey_reg sel );
    void setGtrim4( damrey_reg sel, damrey_reg gain );

    //! reg6
    void setOvRng( damrey_reg over );
    void setVosTrim4( damrey_reg trim );
    void setVosTrim5( damrey_reg trim );

    void setGtrim5( damrey_reg sel, damrey_reg gain );

    //! reg7
    void setVos( damrey_reg vos );

    //! reg8
    void setK( damrey_reg k );

    void setVosExp( damrey_reg vosExp );

    void setGpo1( damrey_reg gp );
    void setLfTrim( damrey_reg trim );
    void setIloEn( damrey_reg en );

    //! reg9
    void setHfTrim( damrey_reg trim );
    void setMfcTrim( damrey_reg trim );

    //! reg10
//    void setAzVosTrim( damrey_reg trim );
//    void setAzVosPolarity( damrey_reg pos );
    //! 11 bit
    void setAzVos( damrey_reg vos );
    void setAzIsel( damrey_reg sel );
    void setEnZd( damrey_reg ed );

    void setHzNpd( damrey_reg npd );
    void setLpp( damrey_reg lpp );

    //! reg11
    damrey_reg getFuseRb();

    //! write
    void setNlTrim41( damrey_reg trim );
    void setNlTrim40( damrey_reg trim );
    void setCcTrim4B( damrey_reg trim );
    void setCcTrim4A( damrey_reg trim );

    void setCcTrim3( damrey_reg trim );

    //! reg12
    void setLzNpd( damrey_reg lznpd );
    void setHzITrim( damrey_reg trim );
    void setLzITrim( damrey_reg trim );
    void setNlTrim31( damrey_reg trim );

    void setNlTrim30( damrey_reg trim );

    //! reg13
    void setNlTrim21( damrey_reg trim );
    void setNlTrim20( damrey_reg trim );
    void setNlTrim11( damrey_reg trim );
    void setNlTrim10( damrey_reg trim );

    //! reg 14
    void setCcTrim2( damrey_reg trim );
    void setVclp( damrey_reg lp );
    void setCcTrim1( damrey_reg trim );

public:
    void showPara();

public:
    void readReg( damrey_reg &reg, int addr, int chan );
    void readReg( damrey_reg &reg, int addr );

private:
    int mId;
    int mCs;
};

}
#endif

#endif // IPHYVGA_H
