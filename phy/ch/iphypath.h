#ifndef IPHYEXT_H
#define IPHYEXT_H

#include "../iphy.h"

#include "../iphydac.h"
#include "./pathcal.h"

namespace dso_phy
{

class IPhyPath : public IPhy
{
public:
    IPhyPath();

public:
    virtual void flushWCache();

public:
    void setLevel( int lev, int sens = 0 );

    void setDac( quint32 dac, int waitidle = 0 );

    void attachDac( IPhyDac *pDac, int dacCh );
    void setCal( pathCal *pCal );

protected:
    IPhyDac *m_pDac;
    int mDacChan;
    pathCal *m_pCal;
};

}

#endif // IPHYEXT_H
