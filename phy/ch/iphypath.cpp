#include "iphypath.h"

namespace dso_phy {

IPhyPath::IPhyPath()
{
    m_pDac = NULL;
    mDacChan = -1;
    m_pCal = NULL;
}

void IPhyPath::flushWCache()
{
    if ( m_pDac != NULL )
    { m_pDac->flushWCache(); }
}

//! uv unit
void IPhyPath::setLevel( int lev, int /*sens*/ )
{
    if ( m_pDac != NULL && m_pCal != NULL )
    {
        int dac;
        if ( m_pCal->mLsb != 0 )
        {
            dac =  (lev /m_pCal->mLsb) + m_pCal->mGnd;

            //! range limit
            if ( dac < 0 ) dac = 0;
            else if ( dac > 65535 ) dac = 65535;
            else {}

            m_pDac->set( (dac_reg)dac, mDacChan );

            //LOG_PHY()<<dac<<m_pCal->mLsb<<m_pCal->mGnd<<lev;
        }
        else
        {
            //qWarning()<<"!!!divide by 0";
        }
    }
}

void IPhyPath::setDac( quint32 dac, int waitIdle )
{
    Q_ASSERT( m_pDac != NULL );
    Q_ASSERT( mDacChan >= 0 );

    //! check range
    if ( dac > 65535 )
    { dac = 65535; }

    m_pDac->set( (dac_reg)dac, mDacChan );
    m_pDac->flushWCache();
    if ( waitIdle > 0 )
    { m_pDac->waitIdle(waitIdle); }
}

void IPhyPath::attachDac( IPhyDac *pDac, int dacCh )
{
    Q_ASSERT( NULL != pDac );

    m_pDac = pDac;
    mDacChan = dacCh;
}
void IPhyPath::setCal( pathCal *pCal )
{
    Q_ASSERT( NULL != pCal );

    m_pCal = pCal;
}

}

