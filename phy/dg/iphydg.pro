TEMPLATE = lib

TARGET = ../../lib$$(PLATFORM)/phy/iphydg
CONFIG += static

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


HEADERS += \
    iphydg.h

SOURCES += \
    iphydg.cpp
