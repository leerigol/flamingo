#include "iphydg.h"

namespace dso_phy {

IPhyDg::IPhyDg()
{
    m_pRelay = NULL;
    m_pDac = NULL;
}

void IPhyDg::init()
{
    //! all output off
    m_pRelay->write( 0, 0x0, 6 );
}

void IPhyDg::flushWCache()
{
    Q_ASSERT( NULL != m_pDac );
    Q_ASSERT( NULL != m_pRelay );

    m_pDac->flushWCache();
    m_pRelay->flushWCache();

}

void IPhyDg::attatchRelay( IPhyRelay *pRelay )
{
    Q_ASSERT( NULL != pRelay );

    m_pRelay = pRelay;
}

void IPhyDg::attachDac( IPhyDac *pDac )
{
    Q_ASSERT( NULL != pDac );

    m_pDac = pDac;
}

void IPhyDg::write( dg_reg_addr addr, dg_reg_value reg )
{
    Q_ASSERT( NULL != m_pBus );
    //! direct write
    m_pBus->write( reg, addr );
}

//! len in unsigned int
void IPhyDg::write( dg_reg_addr addr,
            unsigned int *stream,
            int len )
{
    Q_ASSERT( NULL != stream );

    //! dgu 不支持burst方式写入
    //! 必须一个地址一个数据的方式
    for ( int i = 0; i < len; i++ )
    {
        write( addr, stream[i] );
    }
}
dg_reg_value IPhyDg::read( dg_reg_addr addr )
{
    Q_ASSERT( NULL != m_pBus );

    dg_reg_value val;
    m_pBus->read( val, addr );

    return val;
}

void IPhyDg::switchRelay(
                  relay_reg_value reg,
                  relay_reg_addr /*addr*/)
{
    Q_ASSERT( NULL != m_pRelay );

//    m_pRelay->write( addr, reg );
    m_pRelay->write( reg, 0, 6 );
    m_pRelay->flushWCache();
}

void IPhyDg::setDac( int dacChan, dac_reg dac )
{
    Q_ASSERT( NULL != m_pDac );

    m_pDac->set( dac, dacChan );
}

}
