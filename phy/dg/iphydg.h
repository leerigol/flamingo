#ifndef IPHYDG_H
#define IPHYDG_H

#include "../iphy.h"

#include "../relay/iphyrelay.h"
#include "../iphydac.h"
namespace dso_phy {

#define dg_reg_addr     unsigned int
#define dg_reg_value    unsigned int

class IPhyDg : public IPhy
{
public:
    IPhyDg();

public:
    virtual void init();
    virtual void flushWCache();
public:
    void attatchRelay( IPhyRelay *pRelay );
    void attachDac( IPhyDac *pDac );

public:
    void write( dg_reg_addr addr, dg_reg_value reg );
    //! len in bytes
    void write(dg_reg_addr addr,
                unsigned int *stream,
                int len );
    dg_reg_value read( dg_reg_addr addr );

    void switchRelay( relay_reg_value reg,
                      relay_reg_addr addr = 0);

    void setDac( int dacChan, dac_reg dac );

private:
    IPhyRelay *m_pRelay;
    IPhyDac *m_pDac;
};

}

#endif // IPHYDG_H
