
#include "iphyscu.h"

namespace dso_phy {

static scuCfg _scuInitSeq[]=
{
    {0x14e4, 0 },           //! frm_head_jmp for compatible wpu pre 0401
    {0x1c38, 1000     },    //! plot num
    //{0x1c34 ,7812500  },    //! plot interval:25ms/3.2ns
    {0x1c34, 12500000  },    //! plot interval:40ms/3.2ns
};

#include "../ic/scu.cpp"

IPhyScu::IPhyScu( Scu_reg base ) : ScuMem( base )
{
}

void IPhyScu::init()
{
    int i;

    for ( i = 0; i < array_count(_scuInitSeq); i++ )
    {
        m_pBus->write( _scuInitSeq[i].data,
                       _scuInitSeq[i].addr );
    }

}

void IPhyScu::loadOtp()
{
    _loadOtp();
}
void IPhyScu::reMap()
{
    _remap_Scu_();

    //! remove item
    mapOut( &CTRL );
    mapOut( &MODE );
    mapOut( &AUTO_TRIG_TIMEOUT );
    mapOut( &AUTO_TRIG_HOLD_OFF );

    mapOut( &FSM_DELAY );
    mapOut( &MASK_TIMEOUT );
    mapOut( &PLOT_DELAY );
    mapOut( &FRAME_PLOT_NUM );
    mapOut( &TRACE );
}

Scu_reg IPhyScu::getVersion()
{
    inVERSION();
    return VERSION;
}

scuFsm IPhyScu::getScuFsm()
{
    inSCU_STATE();

    return (scuFsm)SCU_STATE.scu_fsm;
}

void IPhyScu::setPrePostSa(
                   qlonglong preSa,
                   qlonglong postSa )
{
    Q_ASSERT( preSa >=0 );
    Q_ASSERT( postSa >= 0 );

    setPRE_SA( preSa );
    setPOS_SA( postSa );
}

void IPhyScu::setScuPlay( int /*n*/,
                 int /*step*/,
                 bool /*loop*/ )
{
//    outCTRL_fsm_run( 0 );

//    setCTRL_play_last( 1 );
//    setCTRL_fsm_run( 1 );
//    flushWCache();

//    assignCTRL_play_last( 0 );

//    waitStop();
}

void IPhyScu::waitStop()
{
    LOG_DBG();
//#ifndef _SIMULATE
    int tmo = 0;
    do
    {
        inCTRL();
        QThread::usleep( IPhyScu::_tickTime );
        tmo++;
    //! zx_roll
    }while ( CTRL.sys_stop_state != 1 && tmo < 200 );

    //! wait until pipe empty
    QThread::msleep( 1 );

    if ( tmo >= 200 )
    {
        qWarning()<<"!!!wait stop fail-IPhyScu";
    }
//#endif
    return;
}

void IPhyScuGp::setPrePostSa(
                   qlonglong preSa,
                   qlonglong postSa,
                   int id)
{
    if ( id == -1 )
    {
        foreach( IPhyScu *pScu, mSubItems )
        {
            Q_ASSERT( NULL != pScu );

            pScu->setPrePostSa( preSa, postSa );
        }
    }
    else
    {
        mSubItems[id]->setPrePostSa( preSa, postSa );
    }
}


}
