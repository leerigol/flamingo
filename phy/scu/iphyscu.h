#ifndef IPHYSCU
#define IPHYSCU

#include "../iphyfpga.h"

namespace dso_phy
{

#include "../ic/scu.h"

enum scuFsm
{
    SCU_FSM_IDLE = 1,
    SCU_FSM_START = 2,
    SCU_FSM_PRE_SA = 4,
    SCU_FSM_WAIT_TRIG = 8,

    SCU_FSM_POST_SA = 0x10,
    SCU_FSM_WAVE_RD = 0x20,
    SCU_FSM_WAVE_PROC = 0x40,
    SCU_FSM_RD_LOOP = 0x80,

    SCU_FSM_WAVE_PLOT = 0x100,
    SCU_FSM_BRANCH = 0x200,
    SCU_FSM_FINISH = 0x400,
    SCU_FSM_SYS_DLY = 0x800,
};

typedef struct _scuCfg
{
    Scu_reg addr;
    Scu_reg data;
}scuCfg;

class IPhyScu : public ScuMem
{
public:
    IPhyScu( Scu_reg base = 0 );
public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();
public:
    Scu_reg getVersion();

public:
    scuFsm getScuFsm();

    void setPrePostSa(
                       qlonglong preSa,
                       qlonglong postSa );


    //! start play
    void setScuPlay( int n = 1,
                     int step = 1,
                     bool loop = false );

private:
    void waitStop();

private:
    const static int _tickTime = 100;   //! us
};

class IPhyScuGp : public ScuGp
{

public:
    void setPrePostSa(
                       qlonglong preSa,
                       qlonglong postSa,
                       int id = -1 );
};

}

#endif // IPHYSCU

