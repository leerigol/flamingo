#include "cdsophy.h"

namespace dso_phy {

/*!
 * \brief CDsoPhy::CDsoPhy
 * composed of certain phys
 * -PhyCH
 * -PhyHori
 * -PhyAcquire
 * -PhyCon
 */
CDsoPhy::CDsoPhy()
{
    //! id
    m_ch[0].setId( chan1 );
    m_ch[1].setId( chan2 );
    m_ch[2].setId( chan3 );
    m_ch[3].setId( chan4 );

    mVGA[0].setId( 0 );
    mVGA[1].setId( 1 );
    mVGA[2].setId( 2 );
    mVGA[3].setId( 3 );

    //! \see span.doc
    mAdc[0].setLink( 4, 0 );
    mAdc[1].setLink( 5, 1 );

    mAdc[0].setId( 0 );
    mAdc[1].setId( 1 );

    //! relay
    mRelay.setCsChan( 8, 1 );

    //! spu scus
    mSpu.setAddrBase( 0 );
    mScu.setAddrBase( 0 );
    mSpu2.setAddrBase( 0x2000 );    //! base addr
    mScu2.setAddrBase( 0x2000 );

    //! spu group
    mSpuGp.attach( &mSpu );
    mScuGp.attach( &mScu );

    //! spu2/scu2
    if ( isDs8000() )
    {
        mSpuGp.attach( &mSpu2 );
        mScuGp.attach( &mScu2 );
    }

    //! ccu
    mCcu.attachSpu( &mSpuGp );
    mCcu.attachScu( &mScuGp );

    //! search base
    mSearch.setAddrBase( 0x2c00 - 0x1800 );

    //! probe
    m1WireProbe[0].setId( chan1 );
    m1WireProbe[1].setId( chan2 );
    m1WireProbe[2].setId( chan3 );
    m1WireProbe[3].setId( chan4 );

    mProbe[0].attachDac( &mDac, 6 );
    mProbe[1].attachDac( &mDac, 7 );
    mProbe[2].attachDac( &mDac, 8 );
    mProbe[3].attachDac( &mDac, 9 );

    //! phy list
    mPhyList.append( &mPll );
    mPhyList.append( &mAdc[0] );
    if ( getAdcCnt() > 1 )
    { mPhyList.append( &mAdc[1] ); }

    mPhyList.append( &m_ch[0] );
    mPhyList.append( &m_ch[1] );
    mPhyList.append( &m_ch[2] );
    mPhyList.append( &m_ch[3] );

    mPhyList.append( &m_hori );
    mPhyList.append( &m_trace );

    mPhyList.append( &mBeeper );
    mPhyList.append( &mLcd );
    mPhyList.append( &mFan );
    mPhyList.append( &mBoard );

    mPhyList.append( &mVGA[0] );
    mPhyList.append( &mVGA[1] );
    mPhyList.append( &mVGA[2] );
    mPhyList.append( &mVGA[3] );

    mPhyList.append( &mLed );
    mPhyList.append( &mTpu );
    mPhyList.append( &mSearch );

    mPhyList.append( &mDac );

    mPhyList.append( &mGtu );

    //! proc
    mPhyList.append( &mMeasCfg );
    mPhyList.append( &mMeasRet );

    mPhyList.append( &mCounter );

    //! mask
    mPhyList.append( &mMask );
    //! recPlay
    mPhyList.append( &mRecPlay );

//    //! 1wire
//    mPhyList.append( &m1Wire );
    //! 1wire
    mPhyList.append( &m1WireProbe[0] );
    mPhyList.append( &m1WireProbe[1] );
    mPhyList.append( &m1WireProbe[2] );
    mPhyList.append( &m1WireProbe[3] );


    mPhyList.append( &mProbe[0] );
    mPhyList.append( &mProbe[1] );
    mPhyList.append( &mProbe[2] );
    mPhyList.append( &mProbe[3] );


    //! zynq fcu
    mPhyList.append( &mZynqFcuWr );
    mPhyList.append( &mZynqFcuRd );

    //! misc
    mPhyList.append( &mMisc );

    //! dg && relay
    mPhyList.append( &mDgu );
    mPhyList.append( &mRelay );

    mPhyList.append( &mSpu );
    if ( getSpuCnt() > 1 )
    { mPhyList.append( &mSpu2 ); }

    //! scu
    mPhyList.append( &mScu );
    if ( getScuCnt() > 1 )
    { mPhyList.append( &mScu2 ); }

    //! wpu
    mPhyList.append( &mWpu );

    //! ccu at last
    mPhyList.append( &mCcu );

    //! --- configs
    mCcu.config( isDs8000() );
}

DsoErr CDsoPhy::open()
{
    return ERR_NONE;
}
void CDsoPhy::close()
{
}

/*!
 * \brief CDsoPhy::init
 * 在挂载到bus上后复位各个组成
 */
void CDsoPhy::init()
{
    //! phy load
    IPhy *pPhy;
    foreach( pPhy, mPhyList )
    {
        Q_ASSERT( NULL != pPhy );
        pPhy->reMap();
        pPhy->loadOtp();
    }


    /**********************************************************
     * Makesure FPGA stoped when systeming starting
     * ********************************************************/
    mCcu.setCcuRun( false );


    //! no influence
    for ( int i = 0; i < 4; i++ )
    {
        mVGA[i].init();
    }

    //! adcs
    mAdc[0].init();
    if ( getAdcCnt() > 1 )
    {
        mAdc[1].init();
    }

    //! tpu
    mTpu.init();

    //! fcu
    mZynqFcuWr.init();

    //! check the argument
    do
    {
        //! -do not cfg
        if ( sysHasArg("-no_cfg") )
        { break; }
        else
        { }

        //! phy inited
        foreach( pPhy, mPhyList )
        {
            Q_ASSERT( NULL != pPhy );
            pPhy->init();
        }


        LOG_PHY()<< "Phy is ready";
        return;
    }while( 0 );

    LOG_PHY()<<"scu wpu... not config.";
    return;
}

void CDsoPhy::appendBus( IBus *pBus )
{
    Q_ASSERT( NULL != pBus );

    mBuses.append( pBus );
}

void CDsoPhy::flushWCache()
{
   IPhy *pPhy;
   foreach( pPhy, mPhyList )
   {
       Q_ASSERT( NULL != pPhy );
       pPhy->flushWCache();
   }
}

int CDsoPhy::getAdcCnt()
{
    return isDs8000() ? 2 : 1;
}
int CDsoPhy::getSpuCnt()
{
    return isDs8000() ? 2 : 1;
}
int CDsoPhy::getScuCnt()
{
    return isDs8000() ? 2 : 1;
}
int CDsoPhy::getWpuCnt()
{ return 1;}

bool CDsoPhy::isDs8000()
{
    return sysHasArg( "-ds8000");
}

}
