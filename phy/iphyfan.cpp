#include "iphyfan.h"

namespace dso_phy {

#define FAN_ADDR    0x03
#define FAN_CHAN    0
IPhyFan::IPhyFan()
{

}
//! at max speed when startup
void IPhyFan::init()
{
    setSpeed( 100 );
}

void IPhyFan::setSpeed( int speed )
{
    if ( speed < 0 )
    {
        speed = 0;
    }
    else if ( speed > 100 )
    {
        speed = 100;
    }
    else
    {}

    Q_ASSERT( NULL != m_pBus );

    //! write
    unsigned short busVal;
    busVal = speed;
    toLE( busVal );
    m_pBus->write( busVal, FAN_ADDR, FAN_CHAN );
}



}

