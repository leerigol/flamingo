#include "iphybeeper.h"
#include <QThread>

namespace dso_phy {

#define CPLD_BEEPER_ROW 2
#define beeper_chan     0

typedef unsigned short beeper_reg;

IPhyBeeper::IPhyBeeper()
{
}

void IPhyBeeper::startBeep()
{
    Q_ASSERT( NULL != m_pBus );
    m_pBus->write( (beeper_reg)0x0100,
                   CPLD_BEEPER_ROW,
                   beeper_chan );
}
void IPhyBeeper::stopBeep()
{
    Q_ASSERT( NULL != m_pBus );
    m_pBus->write( (beeper_reg)0,
                   CPLD_BEEPER_ROW,
                   beeper_chan );
}

void IPhyBeeper::tick()
{
    Q_ASSERT( NULL != m_pBus );
    m_pBus->write( (beeper_reg)(0x3f00),
                   CPLD_BEEPER_ROW,
                   beeper_chan );
}
void IPhyBeeper::shortBeep()
{
    Q_ASSERT( NULL != m_pBus );
    m_pBus->write( (beeper_reg)0x0300,
                   CPLD_BEEPER_ROW,
                   beeper_chan );
}
void IPhyBeeper::intervalBeep()
{
    Q_ASSERT( NULL != m_pBus );
    m_pBus->write( (beeper_reg)0x0200,
                   CPLD_BEEPER_ROW,
                   beeper_chan );
}

}
