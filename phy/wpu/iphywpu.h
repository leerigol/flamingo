#ifndef IPHYWPU_H
#define IPHYWPU_H
#include "../iphyfpga.h"

#define ANA_CH_CNTs     8

#define WPU_CANVAS_ROW  8
#define WPU_CANVAS_V_DIV 25

#define WPU_CANVAS_MID  240

#define WPU_GAIN_SCALE  1024

#define ADC_MID         128

namespace dso_phy
{

#include "../ic/wpu.h"

struct wpuCfg
{
    Wpu_reg addr;
    Wpu_reg data;
};

struct laLine
{
    int mRow;
    quint8 mDx;
    quint8 mPatt;
    bool  mbZoom;
    bool mEn;
    quint32 mColor;

    laLine();
    void set( bool ben, bool bZoom, int row, int dx, int patt );
    void setColor( quint32 color );
};

class IPhyWpu;
typedef void (IPhyWpu::*p_wpu_set)( Wpu_reg val );

//! wave screen
class WavScr
{
public:
    WavScr();
    void cfgScreen( int height, int gnd = 0 );
    void attachReg( int id,
                    Wpu_reg *regGain,
                    Wpu_reg *regOffset );
    void attachpSets( p_wpu_set pHeightSet,
                      p_wpu_set pBotSet );

    void attachWpu( IPhyWpu *pWpu );

    void fitScreen( float gains[],
                    float divOffsets[],
                    int cnts );

    void setVisible( bool b );
    bool getVisible();
protected:
    void setCH( int id, float gain, float divDeltaOffset=0 );

public:
    bool mbVisible;
    qint32 mHeight, mGnd;

    qint32 mGains[ ANA_CH_CNTs ];
    qint32 mOffsets[ ANA_CH_CNTs ];

    //! sign gain/offset
    qint32 *mRegGains[ ANA_CH_CNTs ];
    qint32 *mRegOffsets[ ANA_CH_CNTs ];

    p_wpu_set m_pSetHeight;
    p_wpu_set m_pSetBottom;

    IPhyWpu *m_pWpu;
};

class IPhyWpu : public WpuMem
{
private:
    const static int _WPU_GAIN_SCALE = WPU_GAIN_SCALE;
    const static int _WPU_CANVAS_MID = WPU_CANVAS_MID;
    const static int _ADC_MID = 128;
    const static int _REFRESH_TICK = 25;        //! 25ms

public:
    enum viewWnd
    {
        view_main = 0,
        view_zoom,
        view_math,
        view_top = view_math,
    };
public:
    IPhyWpu();

private:
    Wpu_reg *mGainRegs[ ANA_CH_CNTs ];
    Wpu_reg *mOffsRegs[ ANA_CH_CNTs ];

public:
    virtual void init();
    virtual void flushWCache();

    virtual void loadOtp();
    virtual void reMap();

public:
    void setscale_length( Wpu_reg value );

    quint32 getMainLength();
    quint32 getZoomLength();

public:
    void syncClear();
    void clearMemory();
    void waitWpuStop();
    void setChannelEn( int ch, Wpu_reg en );

//    void setCHOffset( int chId, int offs );
    void setCHGain( int chId, float gain, float offs=0 );

    //! id
    void setInterleave( int master, int slave );
    void setInterleave( int master,
                        int slave1,
                        int slave2,
                        int slave3 );

    void setPersistTime( quint32 timems );

    void setMainView( int colFrom, int colWidth );
    void setZoomView( int colFrom, int colWidth );

    void setIntensitity( int intensity );
    void setColorSpec( int intensity );

    void setColor( int chId, int entry, quint32 color );

    void setNoise( int grade = 0 );
    void setMainNoise( int grade = 0 );
    void setZoomNoise( int grade = 0 );

    void setLaLine( laLine *pLine );
    void setLaLine( QList<laLine*> &lines );     

public:
    void setScreen( int scrId, bool bEn, int height, int gnd );

    void fitScreen();
    void flushScreen( int scrId );

public:
//    int toChGain( float gain );
//    int toChOffset( float gain );

    quint32 calcPersistDimmRate( quint32 timems );

protected:
    quint8 ds2000Color( int alignedIntens, int index );
    quint8 ds6000Color( int alignedIntens, int index );
    quint8 ds6000_2Color( int alignedIntens, int index );
    quint8 dsXColor( int alignedIntens, int index );

    quint8 dsLightColor(
                         int alignedIntens,
                         int index );

    quint8 dsCurveColor(
                         int alignedIntens,
                         int index );
    quint8 dsVColor( int alignedIntens,
                     int index );
    quint8 dsMColor( int alignedIntens,
                     int index );

private:
    float mScreenGain;

    float mAdcGains[ ANA_CH_CNTs ];     //! ch1~ch4,m1~m4
    float mAdcOffsets[ ANA_CH_CNTs ];   //! div
    WavScr mScreens[ view_top ];        //! main,zoom

    quint32 mMainLen, mZoomLen;
};

}

#endif // IPHYWPU_H
