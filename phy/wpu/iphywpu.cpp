
#include "../../include/dsocfg.h"
#include "iphywpu.h"

#define RGB( r, g, b )  (((r)<<16) | ((g)<<8) | (b))
#define dark_color      16
namespace dso_phy
{

#include "../ic/wpu.cpp"
#include "../ic/wpulacfg.txtd"
static wpuCfg _wpuInitSeq[]=
{
    {0x588,0x1       },
    {0x414,0x3       },
    {0x420,0xf4      },
    {0x42c,0x98000   },

    {0x43c,0x0   },
    {0x440,0x64  },
    {0x448,0x1   },
    {0x444,0x0   },
    {0x554,0xe4e424  },

    {0x550,0x5       },
    {0x44c,0xff    },

    {0x464,0x0   },
    {0x474,0x0    },
    {0x484,0x0    },
    {0x494,0x0    },

    {0x460,0x800     },
    {0x470,0x800     },
    {0x480,0x800     },
    {0x490,0x800     },
    {0x468,0x14       },
    {0x478,0x28       },
    {0x488,0x3c       },
    {0x498,0x50       },

    {0x558,0x1E12C000},
    {0x55c,0x1E301000},

    {0x410,0x3},

};

laLine::laLine()
{
    mEn = false;
    mbZoom = false;
    mRow = 0;
    mDx = 0;
    mPatt = 0;
    mColor = 0;
}
void laLine::set( bool ben, bool bZoom, int row, int dx, int patt )
{
    mEn = ben;
    mbZoom = bZoom;
    mRow =row;
    mDx = dx;
    mPatt = patt;
}

void laLine::setColor( quint32 color )
{
    mColor = color;
}

//! wav scr
WavScr::WavScr()
{
    mbVisible = false;
    mHeight = wave_height;
    mGnd = mHeight/2;

    for ( int i = 0; i < array_count(mGains); i++ )
    {
        mGains[i] = WPU_GAIN_SCALE;
        mOffsets[i] = 0;

        mRegGains[i] = NULL;
        mRegOffsets[i] = NULL;
    }

    m_pSetHeight = NULL;
    m_pSetBottom = NULL;

    m_pWpu = NULL;
}

void WavScr::cfgScreen( int height, int gnd )
{
    mHeight = height;
    mGnd = ( gnd <= 0 ? (height/2-1) : gnd );
}

void WavScr::attachReg( int id,
                Wpu_reg *regGain,
                Wpu_reg *regOffset )
{
    Q_ASSERT( id >= 0 && id < array_count(mGains) );

    Q_ASSERT( NULL != regGain && NULL != regOffset );

    mRegGains[ id ] = (qint32*)regGain;
    mRegOffsets[id] = (qint32*)regOffset;
}

void WavScr::attachpSets( p_wpu_set pHeightSet,
                  p_wpu_set pBotSet )
{
    Q_ASSERT( NULL != pHeightSet && NULL != pBotSet );

    m_pSetHeight = pHeightSet;
    m_pSetBottom = pBotSet;
}

void WavScr::attachWpu( IPhyWpu *pWpu )
{
    Q_ASSERT( NULL != pWpu );

    m_pWpu = pWpu;
}

void WavScr::fitScreen( float gains[],
                        float divOffsets[],
                        int cnts )
{
    //! gain/offset
    for ( int i = 0; i < cnts; i++ )
    {
        setCH( i, gains[i], divOffsets[i] );
    }

    Q_ASSERT( NULL != m_pWpu );

    //! height / bottom
    Q_ASSERT( NULL != m_pSetHeight );
    (m_pWpu->*m_pSetHeight)( mHeight-1);

    Q_ASSERT( NULL != m_pSetBottom );
    (m_pWpu->*m_pSetBottom)( mGnd - mHeight/2 );
}

void WavScr::setVisible( bool b )
{
    mbVisible = b;
}
bool WavScr::getVisible()
{ return mbVisible; }

//! tune screen gain/offset
//! pix = adc * gain / gain_scale + offset
void WavScr::setCH( int id, float gain, float divDeltaOffset )
{
    Q_ASSERT( id >= 0 && id < array_count(mGains) );

    //! tune gain
    int scrGain;
    scrGain = gain * WPU_GAIN_SCALE * mHeight / (WPU_CANVAS_ROW * WPU_CANVAS_V_DIV);

    mGains[ id ] = scrGain;

    //! tune offset
    int scrOffset;
    scrOffset = mHeight/2 - ADC_MID * scrGain / WPU_GAIN_SCALE;
    mOffsets[ id ] = scrOffset + (mHeight*divDeltaOffset/WPU_CANVAS_ROW);

    //! config reg
    if ( mRegGains[id] != NULL )
    {
        *mRegGains[id] = scrGain;
    }

    if ( mRegOffsets[id] != NULL )
    {
        *mRegOffsets[id] = mOffsets[ id ];
    }

//    if ( id == 0 )
//    {
//        LOG_DBG()<<gain<<scrGain<<scrOffset<<mGnd;
//    }

}

IPhyWpu::IPhyWpu()
{
    mScreenGain = 60.0f/25;

    //! gain && offset group
    mGainRegs[0] = &analog_ch0_gain;
    mGainRegs[1] = &analog_ch1_gain;
    mGainRegs[2] = &analog_ch2_gain;
    mGainRegs[3] = &analog_ch3_gain;

    mGainRegs[4] = &analog_ch0_zoom_gain.payload;
    mGainRegs[5] = &analog_ch1_zoom_gain.payload;
    mGainRegs[6] = &analog_ch2_zoom_gain.payload;
    mGainRegs[7] = &analog_ch3_zoom_gain.payload;

    //! offset
    mOffsRegs[0] = &analog_ch0_offset;
    mOffsRegs[1] = &analog_ch1_offset;
    mOffsRegs[2] = &analog_ch2_offset;
    mOffsRegs[3] = &analog_ch3_offset;

    mOffsRegs[4] = &analog_ch0_zoom_offset;
    mOffsRegs[5] = &analog_ch1_zoom_offset;
    mOffsRegs[6] = &analog_ch2_zoom_offset;
    mOffsRegs[7] = &analog_ch3_zoom_offset;

    //! init
    mMainLen = 0;
    mZoomLen = 0;
}

void IPhyWpu::init()
{
    //config color map table. hxh add
    setColorSpec(50);

    for ( int i = 0; i < array_count(_wpuInitSeq); i++ )
    {
        m_pBus->write( _wpuInitSeq[i].data,
                       _wpuInitSeq[i].addr );
    }

    for ( int i = 0; i < array_count(_wpuLaCfgSeq); i++ )
    {
        m_pBus->write( _wpuLaCfgSeq[i].data,
                       _wpuLaCfgSeq[i].addr );
    }

    //! main/zoom screen
    mScreens[0].cfgScreen( 480 );
    mScreens[1].cfgScreen( 144, 480 - 1 - 72 );

    //! regs
    mScreens[0].attachReg( 0, &analog_ch0_gain, &analog_ch0_offset );
    mScreens[0].attachReg( 1, &analog_ch1_gain, &analog_ch1_offset );
    mScreens[0].attachReg( 2, &analog_ch2_gain, &analog_ch2_offset );
    mScreens[0].attachReg( 3, &analog_ch3_gain, &analog_ch3_offset );

    mScreens[0].attachpSets( &WpuMem::setmax_out_main_max_out,
                             &WpuMem::setview_bottom_main_view_bottom );
    mScreens[0].attachWpu( this );

    //! zoom
    mScreens[1].attachReg( 0, &analog_ch0_zoom_gain.payload, &analog_ch0_zoom_offset );
    mScreens[1].attachReg( 1, &analog_ch1_zoom_gain.payload, &analog_ch1_zoom_offset );
    mScreens[1].attachReg( 2, &analog_ch2_zoom_gain.payload, &analog_ch2_zoom_offset );
    mScreens[1].attachReg( 3, &analog_ch3_zoom_gain.payload, &analog_ch3_zoom_offset );

    mScreens[1].attachpSets( &WpuMem::setmax_out_zoom_max_out,
                             &WpuMem::setview_bottom_zoom_view_bottom );
    mScreens[1].attachWpu( this );

    //! visible
    mScreens[0].setVisible( true );
    mScreens[1].setVisible( false );
}

void IPhyWpu::flushWCache()
{
    //! write id first
    setconfig_ID( 0 );
    out( &config_ID );

    WpuMem::flushWCache();

    //! write config id again
    setconfig_ID( 1 );
    out( &config_ID );
}

void IPhyWpu::loadOtp()
{
    _loadOtp();
}
void IPhyWpu::reMap()
{
    _remap_Wpu_();
}

void IPhyWpu::setscale_length( Wpu_reg value )
{
    mMainLen = value;

    //! \errant value != 0
    if ( value < 1 )
    { value = 1; }
    WpuMem::setscale_length( align_8( value ) );
}

quint32 IPhyWpu::getMainLength()
{ return mMainLen; }
quint32 IPhyWpu::getZoomLength()
{ return mZoomLen; }

void IPhyWpu::syncClear()
{
    LOG_FPGA_STATUS()<<"before clear";
    //! wave + persist
    outwpu_clear( 2 );
    waitWpuStop();
    LOG_FPGA_STATUS()<<"after clear";
}

void IPhyWpu::clearMemory()
{
    outwpu_clear( 1 );
    waitWpuStop();
}

void IPhyWpu::waitWpuStop()
{
    {
        int tmo = 0;

        int valid = 0;
        do
        {
            QThread::usleep( 1000 );
            tmo++;

            inwpu_status();
            if(getwpu_status() == 1 || getwpu_status() == 2)
            {
                valid ++;
            }
            else
            {
                valid = 0;
            }

            if(valid == 3 )
            {
                return;
            }
        }while ( tmo < 20 );

        //! wait until pipe empty
        if ( tmo >= 20 )
        {
            qWarning()<<"wpu invalid status";
        }

        QThread::msleep( 1 );
    }
}

void IPhyWpu::setChannelEn( int chId, Wpu_reg en )
{
    Q_ASSERT( chId >= 0 && chId < 4 );

    en = en & 0x01;

    analog_mode.analog_ch_en &= ~(1<<chId);
    analog_mode.analog_ch_en |= (en << chId);

    wcache( &analog_mode );
}

void IPhyWpu::setCHGain( int chId, float gain, float offs )
{
    Q_ASSERT( chId >= 0 && chId < 4 );

    mAdcGains[ chId ] = gain;
    mAdcOffsets[ chId ] = offs;
}

//! 2 - ch interleave
void IPhyWpu::setInterleave( int master,
                             int slave )
{
    Q_ASSERT( master >= 0 && master < 4 );
    Q_ASSERT( slave >= 0 && slave < 4 );

    //! main
    *mGainRegs[ slave ] = *mGainRegs[ master ];
    *mOffsRegs[ slave ] = *mOffsRegs[ master ];

    wcache( mGainRegs[slave] );
    wcache( mOffsRegs[slave] );

    //! zoom
    *mGainRegs[ slave + 4 ] = *mGainRegs[ master + 4 ];
    *mOffsRegs[ slave + 4 ] = *mOffsRegs[ master + 4 ];
    wcache( mGainRegs[slave+4] );
    wcache( mOffsRegs[slave+4] );
}

//! 4 - chs interleave
void IPhyWpu::setInterleave( int master,
                    int slave1,
                    int slave2,
                    int slave3 )
{
    Q_ASSERT( master >= 0 && master < 4 );
    Q_ASSERT( slave1 >= 0 && slave1 < 4 );
    Q_ASSERT( slave2 >= 0 && slave2 < 4 );
    Q_ASSERT( slave3 >= 0 && slave3 < 4 );

    //! main reg
    *mGainRegs[ slave1 ] = *mGainRegs[ master ];
    *mOffsRegs[ slave1 ] = *mOffsRegs[ master ];

    *mGainRegs[ slave2 ] = *mGainRegs[ master ];
    *mOffsRegs[ slave2 ] = *mOffsRegs[ master ];

    *mGainRegs[ slave3 ] = *mGainRegs[ master ];
    *mOffsRegs[ slave3 ] = *mOffsRegs[ master ];

    //! cache in
    wcache( mGainRegs[slave1] );
    wcache( mOffsRegs[slave1] );

    wcache( mGainRegs[slave2] );
    wcache( mOffsRegs[slave2] );

    wcache( mGainRegs[slave3] );
    wcache( mOffsRegs[slave3] );

    //! zoom regs
    *mGainRegs[ slave1 + 4] = *mGainRegs[ master + 4];
    *mOffsRegs[ slave1 + 4] = *mOffsRegs[ master + 4];

    *mGainRegs[ slave2 + 4] = *mGainRegs[ master + 4];
    *mOffsRegs[ slave2 + 4] = *mOffsRegs[ master + 4];

    *mGainRegs[ slave3 + 4] = *mGainRegs[ master + 4];
    *mOffsRegs[ slave3 + 4] = *mOffsRegs[ master + 4];

    wcache( mGainRegs[slave1+4] );
    wcache( mOffsRegs[slave1+4] );

    wcache( mGainRegs[slave2+4] );
    wcache( mOffsRegs[slave2+4] );

    wcache( mGainRegs[slave3+4] );
    wcache( mOffsRegs[slave3+4] );
}

void IPhyWpu::setPersistTime( quint32 timems )
{
    quint32 persistInit;

    persistInit = timems / IPhyWpu::_REFRESH_TICK;

    if ( persistInit < 5 )
    { persistInit = 5; }
    else if ( persistInit > 255 )
    { persistInit = 255; }
    else
    {}

    setpersist_dim_rate( 0 );
    setpersist_init( persistInit );
}

void IPhyWpu::setMainView( int colFrom, int colWidth )
{
    Q_ASSERT( colFrom >= 0 );
    Q_ASSERT( colWidth >= 0 );

    setwave_view_start( colFrom );

    //! \errant colWidth > 0
    if ( colWidth < 1 )
    { colWidth = 1; }
    setwave_view_length( colWidth );
}

void IPhyWpu::setZoomView( int colFrom, int colWidth )
{
    Q_ASSERT( colFrom >= 0 );
    Q_ASSERT( colWidth >= 0 );

    setzoom_view_start( colFrom );

    //! \errant colWidth > 0
    if ( colWidth < 1 )
    { colWidth = 1; }
    setzoom_view_length( colWidth );
}

//! 0~100
void IPhyWpu::setIntensitity( int intensity )
{
    int alignIntens;
    if ( intensity < 0 )
    {
        intensity = 0;
    }
    else if ( intensity > 100 )
    {
        intensity = 100;
    }
    else {}


    alignIntens = intensity;

    //! for each channel
    quint32 colors[8];
    int curColor;

    //qDebug() << "begin:" << QTime::currentTime();
    //! for each channel entry 0
    for( int i = 0; i < 8; i++ )
    {
        colors[i] = RGB( 0xcc, 0xcc, 0xcc );
        setColor( i, 0 , colors[i] );
    }

    //! for each entry
    for ( int i = 1; i < 256; i++ )
    {
//        curColor = ds6000Color( alignIntens, i );
//        curColor = ds6000_2Color( alignIntens, i );
//        curColor = ds2000Color( alignIntens, i );
//        curColor = dsXColor( alignIntens, i );

//        curColor = dsLightColor( alignIntens, i );
//        curColor = dsCurveColor( alignIntens, i );
//        curColor = dsVColor( alignIntens, i );

        curColor = dsMColor( alignIntens, i );
        //! for each channel
        colors[0] = RGB( curColor, curColor, 0 );
        colors[1] = RGB( 0, curColor, curColor );
        colors[2] = RGB( curColor, 0, (curColor*3/4) );
        colors[3] = RGB( 0, 0, curColor );

        colors[4] = RGB( curColor/2, 0, curColor );
        colors[5] = RGB( curColor/2, 0, curColor );
        colors[6] = RGB( curColor/2, 0, curColor );
        colors[7] = RGB( curColor/2, 0, curColor );

        //! for each channel
        for ( int j = 0; j < 8; j++ )
        {
            setColor( j, i, colors[j] );
        }
    }

    //! color line 1
    outinit_palette_table_palette_tbl_wen( 0 );
}

//! 0~100
void IPhyWpu::setColorSpec( int intensity )
{
    int alignIntens;
    if ( intensity < 0 ) intensity = 0;
    else if ( intensity > 100 ) intensity = 100;
    else {}

    //! align to 0~255
    alignIntens = intensity * 255 / 100;

    if ( alignIntens < 8 )
    { alignIntens = 8; }

    //! for each channel
    quint32 colors[8];

    //! for each channel entry 0
    for( int i = 0; i < 8; i++ )
    {
        colors[i] = RGB( 0xcc, 0xcc, 0xcc );
        setColor( i, 0 , colors[i] );
    }

    //! for each entry
    for ( int i = 1; i < 256; i++ )
    {
        int curveColor = dsMColor(15, i);
        //! all ch in one color
        colors[0] = QColor::fromHsl( 255 - curveColor, 255, alignIntens ).rgb();

        //! for each channel
        for ( int j = 0; j < 8; j++ )
        {
            setColor( j, i, colors[0] );
        }
    }

    //! color line 1
    outinit_palette_table_palette_tbl_wen( 0 );
}

void IPhyWpu::setColor( int chId, int entry, quint32 color )
{
    setinit_palette_table_palette_tbl_wen( 1 );
    setinit_palette_table_palette_tbl_waddr( entry );
    setinit_palette_table_palette_tbl_ch( chId );

    setpalette_tbl_wdata( color );
    outpalette_tbl_wdata();
    outinit_palette_table();
}

void IPhyWpu::setNoise( int grade )
{
    //! main
    setMainNoise( grade );

    //! zoom
    setZoomNoise( grade );
}

void IPhyWpu::setMainNoise( int grade )
{
    setanalog_ch0_noise_ch0_noise_grade( grade );
    setanalog_ch1_noise_ch1_noise_grade( grade );
    setanalog_ch2_noise_ch2_noise_grade( grade );
    setanalog_ch3_noise_ch3_noise_grade( grade );
}
void IPhyWpu::setZoomNoise( int grade )
{
    setanalog_ch0_noise_ch0_zoom_noise_grade( grade );
    setanalog_ch1_noise_ch1_zoom_noise_grade( grade );
    setanalog_ch2_noise_ch2_zoom_noise_grade( grade );
    setanalog_ch3_noise_ch3_zoom_noise_grade( grade );
}

void IPhyWpu::setLaLine( laLine *pLine )
{
    Q_ASSERT( NULL != pLine );

//    LOG_PHY()<<pLine->mEn<<pLine->mRow<<pLine->mDx<<QString::number(pLine->mColor,16);
//    return;

    //! 1. out addr
    setla_plot_tbl_data_CH_NO( pLine->mDx );
    setla_plot_tbl_data_POSITION( pLine->mPatt );
    setla_plot_tbl_data_zoom_chn_flag( pLine->mbZoom );
    setla_plot_tbl_data_COLOR( pLine->mColor );

    outla_plot_tbl_data();

    //! 2. out en
    setinit_la_plot_table_la_plot_tbl_wen( pLine->mEn );
    setinit_la_plot_table_la_plot_tbl_waddr( pLine->mRow );

    outinit_la_plot_table();
}

void IPhyWpu::setLaLine( QList<laLine*> &lines )
{
    foreach( laLine *pLine, lines )
    {
        Q_ASSERT( NULL != pLine );

        setLaLine( pLine );
    }
}

void IPhyWpu::setScreen( int scrId, bool bEn, int height, int gnd )
{
    //! only 2 screens
    Q_ASSERT( scrId >= 0 && scrId < 2 );

    Q_ASSERT( gnd >= 0 && gnd < wave_height );

    mScreens[ scrId - view_main ].cfgScreen( height, wave_height - 1 - gnd );
    mScreens[ scrId - view_main ].setVisible( bEn );
}

void IPhyWpu::fitScreen()
{
    //! foreach screen
    for( int i = 0; i < array_count(mScreens); i++ )
    {
        //! visible
        if ( mScreens[i].getVisible() )
        {
//            LOG_DBG()<<i;

            mScreens[i].fitScreen( mAdcGains,
                                   mAdcOffsets,
                                   array_count(mAdcGains) );

            flushScreen( i );

//            LOG_DBG()<<mScreens[i].mGains[0]<<mScreens[i].mOffsets[0];
        }
    }
}

void IPhyWpu::flushScreen( int scrId )
{
    //! main
    if ( scrId == 0 || scrId == -1 )
    {
        wcache( &analog_ch0_gain );
        wcache( &analog_ch1_gain );
        wcache( &analog_ch2_gain );
        wcache( &analog_ch3_gain );

        wcache( &analog_ch0_offset );
        wcache( &analog_ch1_offset );
        wcache( &analog_ch2_offset );
        wcache( &analog_ch3_offset );

        wcache( &max_out );
        wcache( &view_bottom );
    }

    //! zoom
    if ( scrId == 1 || scrId == -1 )
    {
        wcache( &analog_ch0_zoom_gain );
        wcache( &analog_ch1_zoom_gain );
        wcache( &analog_ch2_zoom_gain );
        wcache( &analog_ch3_zoom_gain );

        wcache( &analog_ch0_zoom_offset );
        wcache( &analog_ch1_zoom_offset );
        wcache( &analog_ch2_zoom_offset );
        wcache( &analog_ch3_zoom_offset );

        wcache( &max_out );
        wcache( &view_bottom );
    }
}

quint32 IPhyWpu::calcPersistDimmRate( quint32 timems )
{
    Wpu_reg persistInit;

    persistInit = inpersist_init();

    int n;
    if ( persistInit < 1 )
    {
        n = 1;
    }
    else
    {
        n = timems / ( IPhyWpu::_REFRESH_TICK*persistInit) - 1;
    }
    if ( n < 1 )
    { n = 1; }


    return (quint32)n;
}

//! alignedIntens: 0~255
//! index: 1~255
quint8 IPhyWpu::ds2000Color( int alignedIntens, int index )
{
    int curColor;

    curColor = index + alignedIntens;
    if ( curColor > 255 ) curColor = 255;
    if ( curColor < 8 ) curColor = 8;

    return (quint8)curColor;
}
quint8 IPhyWpu::ds6000Color( int alignedIntens, int index )
{
    int intenRadius;
    int colorIndex;
    int curColor;

    intenRadius	= (255 - alignedIntens);    // 调整半径

    if ( index > intenRadius)
    {
        colorIndex	= 0;
    }
    //! index <= intenRadius
    else
    {
        colorIndex 	= intenRadius - index;
    }
                                            // 高度
    colorIndex 	= sqrt(intenRadius*intenRadius - colorIndex*colorIndex);
                                            // 调整Y
    curColor  = colorIndex + alignedIntens ;

    if ( curColor > 255 ) curColor = 255;
    if ( curColor < 8 ) curColor = 8;

    return (quint8)curColor;
}

quint8 IPhyWpu::ds6000_2Color( int alignedIntens, int index )
{
    float step;

    if ( alignedIntens >= 255 )
    {
        step = 255;
    }
    else
    {
        step = 255.0/(255-alignedIntens);
    }

    int curColor;
    curColor = index * step;

    if ( curColor > 255 ) curColor = 255;
    if ( curColor < 8 ) curColor = 8;

    return (quint8)curColor;
}

quint8 IPhyWpu::dsXColor( int alignedIntens, int index )
{
    int curColor;

    curColor = index * alignedIntens / 255;

    return (quint8)curColor;
}

quint8 IPhyWpu::dsLightColor(
                             int alignedIntens,
                             int index )
{
    int curColor;

    int lightIndex;

    lightIndex = alignedIntens - 128 + index;

    curColor = lightIndex * alignedIntens / 128;

    if ( curColor > 255 ) curColor = 255;
    if ( curColor < 8 ) curColor = 8;

    return curColor;
}

quint8 IPhyWpu::dsCurveColor( int alignedIntens,
                              int index )
{
    float norm = index / 255.0f;
    float color;
    qint32 lightScale;

    //! 8
    lightScale = (alignedIntens - 128)/16;

    //! +light
    if ( lightScale > 0 )
    {
        color = pow( norm, 1.0/lightScale );
    }
    //! -light
    else if ( lightScale < 0 )
    {
        color = pow( norm, -lightScale );
    }
    else
    { color = norm; }

    int curColor;
    curColor = (color*255);
    if ( curColor < 8 ) curColor = 8;
    else if ( curColor > 255 ) curColor = 255;
    else{}

    return (quint8)curColor;
}

#define _head       (5.0f/8)
#define _body       (3.0f/8)

quint8 IPhyWpu::dsVColor( int alignedIntens,
                 int index )
{
    int deltaIntens;

    deltaIntens = alignedIntens - 128;

    //! 3 points
    QPoint ptS( 0 , 0 );
    QPoint ptAng( 256*7.0f/10, 255*_body );
    QPoint ptE( 255, 255+75 );

    //! tune the point
    if ( deltaIntens > 0 )
    {
        ptS.setY( deltaIntens );
        ptAng.setY( ptAng.y() + deltaIntens*255*_head/255 );
    }
    else if ( deltaIntens < 0 )
    {
        ptAng.setY( ptAng.y() + deltaIntens*255*_body/255 );
        ptE.setY( ptE.y() + deltaIntens*255/255 );
    }
    else
    {}

    //! now calc the value
    int curColor;
    if ( index < ptAng.x() )
    {
        curColor = ptS.y() + (index-ptS.x()) * ( ptAng.y()-ptS.y() ) / (ptAng.x() - ptS.x() );
    }
    else if ( index > ptAng.x() )
    {
        curColor = ptAng.y() + (index-ptAng.x()) * ( ptE.y()-ptAng.y() ) / (ptE.x() - ptAng.x() );
    }
    else
    { curColor = ptAng.y(); }

    //! trim color
    if ( curColor < dark_color ) curColor = dark_color;
    else if ( curColor > 255 ) curColor = 255;
    else{}

    return (quint8)curColor;
}

quint8 IPhyWpu::dsMColor(int alignedIntens, int index)
{
    int rawGrade = index * index * 0.004 + 0.057* index - 31;
    int temp = rawGrade + alignedIntens;
    if(temp > 255)
    {
        return 255;
    }
    else if( temp < 8 )
    {
        return 8;
    }
    else
    {
        return temp;
    }
}

}

