#ifndef IPHY_H
#define IPHY_H

#include <QtCore>

#include "../include/dsotype.h"
using namespace DsoType;

#include "./bus/ibus.h"

#include "../include/dsodbg.h"
#define LOG_PHY()   LOG_DBG()

namespace dso_phy {

#define cache_addr  unsigned int

class regCache
{
public:
    regCache( cache_addr addr, void *pMem );
    regCache();

    cache_addr mAddr;
    void *m_pMem;
};

class IPhy
{
private:
    static QMutex _phyMutex;

    static QMutex _mapMutex;

public:
    IPhy();

public:
    int attachBus( IBus *pBus );
    void detachBus();

    //! config ic directly
    virtual void init();

    //! load default value
    virtual void loadOtp();
    //! map the addr and mem
    virtual void reMap();

    virtual void flushWCache();
    virtual void flushRCache();

    virtual void write( void *pMem, cache_addr addr );
    virtual void read( void *pMem, cache_addr addr );

    virtual void waitIdle( int nms );
protected:
    bool checkMem( void *pMem );
    void lockWCache();
    void unlockWCache();
public:
    void appendWCache( cache_addr adr, void *pMem );
protected:
    void appendRCache( cache_addr adr, void *pMem );

    void removeWCache( cache_addr addr );

    void mapIn( void *pMem, cache_addr addr );
    void mapOut( void *pMem );
    void wcache( void *pMem );

    //! direct out
    void out( void *pMem );
    //! direct in
    void in( void *pMem );

    void setBits( unsigned int &dat,
                  unsigned int val,
                  int col,
                  int width );
    void setBits( unsigned int *pData,
                  unsigned int val,
                  int col, int width );

    unsigned int getBits( unsigned int dat, int col, int width );

    void toLE( short &dat );
    void toLE( unsigned short &dat );

    unsigned int pack24AddrData( int addr, unsigned short data );

    regCache *findCache( cache_addr addr,
                         QList <regCache*> & lst );
    void removeCache( cache_addr addr,
                      QList <regCache*> &lst );

    typedef bool (IPhy::*p_completed)();
    int wait( p_completed iscompleted,
              int tmous=200000, //! 200ms
              int tickus=5
              );
public:
    static void usleep( int tmous );

public:
    cache_addr memberAddr( void *pMember );

protected:
    IBus *m_pBus;       //! first bus
    QList <IBus*> mBuses;
    QList <regCache*> mwCache;
    QList <regCache*> mrCache;

    QHash< void *, cache_addr > mMap;
};

}

#endif // IPHY_H
