#include "iphydac.h"

namespace dso_phy {

IPhyDac::IPhyDac()
{
    mBaseAddr = 0x2000;

    unsigned int i;
    for( i =0; i < sizeof(mReg)/sizeof(mReg[0]); i++ )
    {
        mReg[i].reg = 0;
    }

    mChnTime = 500;
}

void IPhyDac::set( dac_reg dac,
                   int chan )
{
    Q_ASSERT( chan >= 0 && chan < (int)(sizeof(mReg)/sizeof(mReg[0])) );

    mReg[chan].reg = dac;
    appendWCache( mBaseAddr + chan * 4, &mReg[chan] );
}

dac_reg IPhyDac::get( int chan )
{
    Q_ASSERT( chan >= 0 && chan < array_count(mReg) );

    return mReg[chan].reg;
}
dac_reg IPhyDac::inReg( int chan )
{
    Q_ASSERT( chan >= 0 && chan < array_count(mReg) );

    cache_addr addr = mBaseAddr + chan * 4;

    quint32 regVal;

    read( &regVal, addr );

    return (quint16)regVal;
}

//! time in ns
void IPhyDac::setDacChnTime( int time )
{
    mChnTime = time / dac_chn_time_base;

    appendWCache( 0x2040, &mChnTime );
}


}

