#include "iphymask.h"

namespace dso_phy {

IPhyMask::IPhyMask()
{

}

void IPhyMask::attachSpuGp(IPhySpuGp *pSpuGp)
{
    Q_ASSERT( NULL != pSpuGp );

    m_pSpuGp = pSpuGp;
}

void IPhyMask::setMaskRule()
{}

void IPhyMask::clrErr()
{
    m_pSpuGp->pulseMASK_CTRL_cross_clr( 1 );
}
void IPhyMask::clrSum()
{
    m_pSpuGp->pulseMASK_CTRL_sum_clr( 1 );
}

#define low_invalid_point   -1
#define high_invalid_point   256
void IPhyMask::setLess( qint16 pt, bool bRuleValid )
{
    if ( pt == low_invalid_point || !bRuleValid )
    {
        m_pSpuGp->setMASK_TAB_threshold_l_less_en(0);
//        qDebug()<<"IPhyMask::setLess pt = "<<pt;
//        qDebug()<<"IPhyMask::setLess bRuleValid = "<<bRuleValid;
    }
    else
    {
        m_pSpuGp->setMASK_TAB_threshold_l_less_en(1);
        m_pSpuGp->setMASK_TAB_threshold_l(pt);
    }
}
void IPhyMask::setGreater( qint16 pt, bool bRuleValid )
{
    if ( pt == high_invalid_point || !bRuleValid )
    {
        m_pSpuGp->setMASK_TAB_threshold_h_greater_en(0);
//        qDebug()<<"IPhyMask::setGreater pt = "<<pt;
//        qDebug()<<"IPhyMask::setGreater bRuleValid = "<<bRuleValid;
    }
    else
    {
        m_pSpuGp->setMASK_TAB_threshold_h_greater_en(1);
        m_pSpuGp->setMASK_TAB_threshold_h( pt );
    }
}

void IPhyMask::setExLess( qint16 pt, bool bRuleValid )
{
    if ( pt == low_invalid_point || !bRuleValid )
    {
        m_pSpuGp->setMASK_TAB_EX_threshold_l_greater_en(0);
    }
    else
    {
        m_pSpuGp->setMASK_TAB_EX_threshold_l_greater_en(1);
        m_pSpuGp->setMASK_TAB_EX_threshold_l(pt);
    }
}
void IPhyMask::setExGreater( qint16 pt, bool bRuleValid )
{
    if ( pt == high_invalid_point || !bRuleValid )
    {
        m_pSpuGp->setMASK_TAB_EX_threshold_h_less_en(0);
    }
    else
    {
        m_pSpuGp->setMASK_TAB_EX_threshold_h_less_en(1);
        m_pSpuGp->setMASK_TAB_EX_threshold_h(1);
    }
}

}

