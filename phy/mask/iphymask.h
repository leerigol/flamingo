#ifndef IPHYMASK_H
#define IPHYMASK_H

#include "../iphyfpga.h"
#include "../spu/iphyspu.h"

namespace dso_phy
{

class IPhyMask  : public IPhy
{
public:
    IPhyMask();

public:
    void attachSpuGp( IPhySpuGp *pSpuGp );

public:
    void setMaskRule();

    void clrErr();
    void clrSum();

    void setLess( qint16 pt, bool bRuleValid );
    void setGreater( qint16 pt, bool bRuleValid );

    void setExLess( qint16 pt, bool bRuleValid );
    void setExGreater( qint16 pt, bool bRuleValid );

private:
    IPhySpuGp *m_pSpuGp;

};

}

#endif
