#ifndef IPHYRELAY_H
#define IPHYRELAY_H

#include "../iphy.h"

#define relay_reg_addr     unsigned char
#define relay_reg_value    unsigned short

namespace dso_phy {

class IPhyRelay : public IPhy
{
public:
    IPhyRelay();
    void setCsChan( int cs = 7, int chan=1 );

public:
    virtual void flushWCache();

protected:
    void write( relay_reg_addr addr,
                relay_reg_value value );
public:
    void write( relay_reg_value val,
                int offset,
                int size = 1 );
private:
    int mCs;
    int mSpiChan;

    relay_reg_value mRelay;
};

}

#endif // IPHYRELAY_H
