#include "iphyrelay.h"

namespace dso_phy {


IPhyRelay::IPhyRelay()
{
    mCs = 8;
    mSpiChan = 1;

    mRelay = 0;
}

void IPhyRelay::setCsChan( int cs, int chan  )
{
    mCs = cs;
    mSpiChan = chan;
}

void IPhyRelay::flushWCache()
{
    if ( mwCache.size() < 1 ) return;

    IPhy::lockWCache();

    regCache *pItem;
    foreach( pItem, mwCache )
    {
        write( pItem->mAddr, *((relay_reg_value*)pItem->m_pMem) );
    }

    IPhy::unlockWCache();

    IPhy::flushWCache();
}

void IPhyRelay::write( relay_reg_addr addr,
                       relay_reg_value value )
{
    Q_ASSERT( NULL != m_pBus );

    unsigned short busVal;
    unsigned int packVal;

    //! select
    busVal = 1 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );

    packVal = pack24AddrData( addr,
                              value );
    m_pBus->send( &packVal, 3, mSpiChan );

    //! deselect
    busVal = 0 << mCs;
    toLE( busVal );
    m_pBus->write( busVal, 0x04, 0 );
}

void IPhyRelay::write(
            relay_reg_value val,
            int offset,
            int size )
{
    //! unset
    for ( int i = 0; i < size; i++ )
    {
        unset_bit( mRelay, (offset+i) );
    }

    //! mask on
    mRelay |= val << offset;

    appendWCache( 0, &mRelay );
}

}

