
#ifndef IPHY_GTU_H
#define IPHY_GTU_H

#include "../iphyfpga.h"

namespace dso_phy
{

#include "../ic/gtu.h"

class IPhyGtu : public GtuMem
{
public:
    IPhyGtu();

public:
    virtual void init();

    virtual void loadOtp();
    virtual void reMap();

public:

};

}

#endif
