
#include "iphyhori.h"
#include "../../meta/crmeta.h"

#include "regarray.h"   //! about intxplate
#include "intxwriter.h"

namespace dso_phy {

//! max interplate factor
#define MAX_INTERP_X    200

//! rect
//! Blackman
//! Flat_Top

//! intxplate table
static int run_interp_X252[]={
#include "../../resource/intxtable/rect/interp_X252.csv"
};

static int run_interp_X200[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X200.csv"
#include "../../resource/intxtable/rect/interp_X200.csv"
};

static int run_interp_X160[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X160.csv"
#include "../../resource/intxtable/rect/interp_X160.csv"
};

static int run_interp_X100[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X100.csv"
#include "../../resource/intxtable/rect/interp_X100.csv"
};

static int run_interp_X80[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X80.csv"
#include "../../resource/intxtable/rect/interp_X80.csv"
};

static int run_interp_X50[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X50.csv"
#include "../../resource/intxtable/rect/interp_X50.csv"
};

static int run_interp_X40[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X40.csv"
#include "../../resource/intxtable/rect/interp_X40.csv"
};

static int run_interp_X32[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X32.csv"
#include "../../resource/intxtable/rect/interp_X32.csv"
};

static int run_interp_X25[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X25.csv"
#include "../../resource/intxtable/rect/interp_X25.csv"
};

static int run_interp_X20[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X20.csv"
#include "../../resource/intxtable/rect/interp_X20.csv"
};

static int run_interp_X16[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X16.csv"
#include "../../resource/intxtable/rect/interp_X16.csv"
};

static int run_interp_X10[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X10.csv"
#include "../../resource/intxtable/rect/interp_X10.csv"
};

static int run_interp_X8[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X8.csv"
#include "../../resource/intxtable/rect/interp_X8.csv"
};

static int run_interp_X5[]={
#include "../../resource/intxtable/rect/interp_X5.csv"
//#include "../../resource/intxtable/new_intx/interp_X5.csv"
};

static int run_interp_X4[]={
#include "../../resource/intxtable/rect/interp_X4.csv"
//#include "../../resource/intxtable/new_intx/interp_X4.csv"
};

static int run_interp_X2[]={
//#include "../../resource/intxtable/blackMan_seg/interp_X2.csv"
#include "../../resource/intxtable/rect/interp_X2.csv"
};

//!
IPhyHori::IPhyHori()
{
    r_meta::CBMeta meta;

    QString str;
    meta.getMetaVal( "phy/hori/sa_table/path", str );

    //! sa table
    setDataPath( sysGetResourcePath() + str );

    //! set intx table
    IntxCoeff::addMap( 2, run_interp_X2 );
    IntxCoeff::addMap( 4, run_interp_X4 );
    IntxCoeff::addMap( 5, run_interp_X5 );
    IntxCoeff::addMap( 8, run_interp_X8 );
    IntxCoeff::addMap( 10, run_interp_X10 );

    IntxCoeff::addMap( 16, run_interp_X16 );
    IntxCoeff::addMap( 20, run_interp_X20 );
    IntxCoeff::addMap( 25, run_interp_X25 );
    IntxCoeff::addMap( 32, run_interp_X32 );
    IntxCoeff::addMap( 40, run_interp_X40 );
    IntxCoeff::addMap( 50, run_interp_X50 );

    IntxCoeff::addMap( 80, run_interp_X80 );
    IntxCoeff::addMap( 100, run_interp_X100 );
    IntxCoeff::addMap( 160, run_interp_X160 );
    IntxCoeff::addMap( 200, run_interp_X200 );
    IntxCoeff::addMap( 252, run_interp_X252 );

    m_pSpuGp = NULL;
    m_pFcuW = NULL;
    m_pCcu = NULL;
}

void IPhyHori::init()
{
    //! hori unit
    mMainUnit.attachPhy( this );
    mZoomUnit.attachPhy( this );

    mMainPlay.attachPhy( this );
    mZoomPlay.attachPhy( this );
}

void IPhyHori::attachSpuGp( IPhySpuGp *pSpuGp )
{
    Q_ASSERT( NULL != pSpuGp );

    m_pSpuGp = pSpuGp;
}
void IPhyHori::attachFcuW( IPhyZynqFcuWr *pFcuW )
{
    Q_ASSERT( NULL != pFcuW );

    m_pFcuW = pFcuW;
}
void IPhyHori::attachCcu( IPhyCcu *pCcu )
{
    Q_ASSERT( NULL != pCcu );

    m_pCcu = pCcu;
}
void IPhyHori::attachWpu( IPhyWpu *pWpu )
{
    Q_ASSERT( NULL != pWpu );

    m_pWpu = pWpu;
}
//! dir must have /
void IPhyHori::setDataPath( const QString &dir )
{
    QString fileName;
    QString path;

    fileName = "hori_2_5g/";
    path = dir + fileName;

    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 0, path + "AUTO" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 1000, path + "1K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 10000, path + "10K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 100000, path + "100K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 1000000, path + "1M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 10000000, path + "10M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 25000000, path + "25M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 50000000, path + "50M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 100000000, path + "100M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_2_5G, 125000000, path + "125M" );

    fileName = "hori_5g/";
    path = dir + fileName;
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 0, path + "AUTO" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 1000, path + "1K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 10000, path + "10K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 100000, path + "100K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 1000000, path + "1M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 10000000, path + "10M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 25000000, path + "25M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 50000000, path + "50M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 100000000, path + "100M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 125000000, path + "125M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G, 250000000, path + "250M" );

    fileName = "hori_5g_100m/";
    path = dir + fileName;
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 0, path + "AUTO" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 1000, path + "1K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 10000, path + "10K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 100000, path + "100K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 1000000, path + "1M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 10000000, path + "10M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 25000000, path + "25M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 50000000, path + "50M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 100000000, path + "100M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 125000000, path + "125M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 250000000, path + "250M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_5G_100M, 500000000, path + "500M" );

    fileName = "hori_10g/";
    path = dir + fileName;
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 0, path + "AUTO" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 1000, path + "1K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 10000, path + "10K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 100000, path + "100K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 1000000, path + "1M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 10000000, path + "10M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 25000000, path + "25M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 50000000, path + "50M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 100000000, path + "100M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 125000000, path + "125M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 250000000, path + "250M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_10G, 500000000, path + "500M" );

    fileName = "hori_20g/";
    path = dir + fileName;
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 0, path + "AUTO" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 1000, path + "1K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 10000, path + "10K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 100000, path + "100K" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 1000000, path + "1M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 10000000, path + "10M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 50000000, path + "50M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 100000000, path + "100M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 125000000, path + "125M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 250000000, path + "250M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 500000000, path + "500M" );
    mHoriCfgMgr.regDir( CHoriPage::clk_20G, 1000000000, path + "1G" );
}

DsoErr IPhyHori::setInterxp(
                  int intX,
                  int intXCnt,
                  quint32 multAddr,
                  data_path path )
{
    Q_ASSERT( multAddr != 0 );
    IntxGen gen;

    //! gen intx table
    gen.gen( intX, intXCnt );

    //! fill coeff
    IntxCoeff::fillCoeff( &gen );

    //! write coeff
    IntxWriter writer;
    Q_ASSERT( NULL != m_pBus );
    writer.write( &gen,
                  m_pBus,
                  multAddr,
                  (quint32)path );

    return ERR_NONE;
}

DsoErr IPhyHori::setMainCfg( HoriPara &cfg )
{
    return mMainUnit.config( cfg );
}
DsoErr IPhyHori::setZoomCfg( HoriPara &cfg )
{
    return mZoomUnit.config( cfg );
}

DsoErr IPhyHori::setMainPlay(CFlowView &chView)
{
    //! convert para
    HoriPara horiPara;

    horiPara.setPath( data_path_main );

    horiPara.setAcquire( chView.mAcquire,
                         chView.mAcqChCnt,
                         chView.mIntxRema );

    horiPara.setCH( chView.mIntx,
                    chView.mCompL1,
                    chView.mCompL2,
                    chView.mTransferLen,

                    chView.mMStart,
                    chView.mMLen,

                    chView.mLinearIntxOn,
                    chView.mLinearIntx.mM,
                    chView.mLinearIntx.mN,

                    chView.mScrLeft.ceilInt(),
                    chView.mScrLen.toInt()
                    );

    if(chView.mIntx > 200)
    {
        return ERR_INVALID_CONFIG;
    }

    return mMainPlay.config( horiPara );
}

DsoErr IPhyHori::setZoomPlay( CFlowView &view )
{
    //! convert para
    HoriPara horiPara;

    horiPara.setPath( data_path_zoom );

    horiPara.setAcquire( view.mAcquire,
                         view.mAcqChCnt,
                         view.mIntxRema );

    horiPara.setCH( view.mIntx,
                    view.mCompL1,
                    view.mCompL2,

                    view.mTransferLen,
                    view.mMStart,
                    view.mMLen,

                    view.mLinearIntxOn,
                    view.mLinearIntx.mM,
                    view.mLinearIntx.mN,

                    view.mScrLeft.ceilInt(),
                    view.mScrLen.toInt()
                    );

    if(view.mIntx > 200)
    {
        return ERR_INVALID_CONFIG;
    }

    return mZoomPlay.config( horiPara );
}

//! to use
DsoErr IPhyHori::setRollPlay(CFlowView &view)
{
    LOG_DBG();
    //! convert para
    HoriPara horiPara;

    horiPara.setPath( data_path_main );

    horiPara.setAcquire( view.mAcquire,
                         view.mAcqChCnt,
                         view.mIntxRema );

    horiPara.setCH( view.mIntx,
                    view.mCompL1,
                    view.mFixCompL2,

                    view.mTransferLen,
                    view.mMStart,
                    view.mViewMemLenCeil, // mMLen

                    view.mLinearIntxOn,
                    view.mLinearIntx.mM,
                    view.mLinearIntx.mN,

                    view.mScrLeft.toInt(),
                    view.mScrLen.toInt()
                    );
    return mMainPlay.config( horiPara );
}

int IPhyHori::toXpCnt( int chCnt )
{
    if ( chCnt == 4 ) return 1;
    if ( chCnt == 2 ) return 2;
    if ( chCnt == 1 ) return 4;

    Q_ASSERT( false );
    return 1;
}

int IPhyHori::getMaxInterp()
{
    return MAX_INTERP_X;
}

}
