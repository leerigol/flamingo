#ifndef IPHY_HORI_H
#define IPHY_HORI_H

#include "../iphy.h"

#include "../spu/iphyspu.h"
#include "../ccu/iphyccu.h"
#include "../zynqfcu/iphyzynqfcuwr.h"
#include "../wpu/iphywpu.h"

#include "choricfg.h"
#include "horiunit.h"

namespace dso_phy {

/*!
 * \brief The IPhyHori class
 * horizontal interface
 */
class IPhyHori : public IPhy
{
public:
    IPhyHori();

public:
    virtual void init();

    void attachSpuGp( IPhySpuGp *pSpuGp );
    void attachFcuW( IPhyZynqFcuWr *pFcuW );
    void attachCcu( IPhyCcu *pCcu );
    void attachWpu( IPhyWpu *pWpu );
    void setDataPath( const QString &str );

public:
    DsoErr setInterxp( int intx,
                       int xCnt,
                       quint32 multAddr,
                       data_path path );
public:
    DsoErr setMainCfg( HoriPara &cfg );
    DsoErr setZoomCfg( HoriPara &cfg );

    DsoErr setMainPlay(CFlowView &chView);
    DsoErr setZoomPlay( CFlowView &view );
    DsoErr setRollPlay( CFlowView &view );

public:
    int toXpCnt( int chCnt );
    int getMaxInterp();

public:
    CHoriCfgMgr mHoriCfgMgr;

    IPhySpuGp *m_pSpuGp;
    IPhyCcu *m_pCcu;
    IPhyZynqFcuWr *m_pFcuW;
    IPhyWpu *m_pWpu;

protected:
    HoriMainUnit mMainUnit;
    HoriZoomUnit mZoomUnit;

    HoriMainPlayUnit mMainPlay;
    HoriZoomPlayUnit mZoomPlay;
};

}

#endif
