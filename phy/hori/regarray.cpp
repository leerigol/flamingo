#include "regarray.h"
#include "../../include/dsoarith.h"

namespace dso_phy {

LoopStack::LoopStack( int size )
{
    Q_ASSERT( size > 0 );

    m_pBase = new int[ size ];
    mSize = size;

    mIndex = 0;
}
LoopStack::~LoopStack()
{
    delete []m_pBase;
}

void LoopStack::push( int dat )
{
    Q_ASSERT( mIndex < mSize );

    m_pBase[ mIndex++ ] = dat;
}
int LoopStack::pop()
{
    Q_ASSERT( NULL != m_pBase );

    int val = m_pBase[ mIndex ];

    mIndex = ( mIndex + 1 ) % mSize;

    return val;
}

void LoopStack::shift( int n )
{
    Q_ASSERT( mSize > 0 );

    n = n % mSize;

    mIndex = (mSize - n);

    mIndex %= mSize;
}

RegArray::RegArray()
{
    mColSpan = 0;
    mRowMod = 0;
    mUsedRow = 0;

    m_pBase = NULL;

    mRow = 0;
    mColumn = 0;
}

RegArray::~RegArray()
{
    if ( NULL != m_pBase )
    { delete []m_pBase; }
}

void RegArray::init( int row, int col )
{
    Q_ASSERT( row > 0 && col > 0 );

    mRow = row;
    mColumn = col;

    m_pBase = new int [ mRow * mColumn ];
    Q_ASSERT( NULL != m_pBase );
}

void RegArray::setCell0( int cell0 )
{
    *cell(0,0) = cell0;

    //! now gen the first col
    for ( int i = 1; i < mUsedRow; i++ )
    {
        Q_ASSERT( mRowMod > 0 );
        *cell( i, 0 ) = (*cell(i-1, 0) + mRowSpan) % mRowMod;
    }

    //! now the the next cols
    for ( int i = 0; i < mUsedRow; i++ )
    {
        for ( int j = 1; j < mColumn; j++ )
        {
            Q_ASSERT( mRowMod > 0 );
            *cell( i, j ) = *cell(i, j - 1) + mColSpan ;
        }
    }
}

void RegArray::shift()
{
    for ( int i = 0; i < mColumn; i++ )
    {
        LoopStack stack( mUsedRow );

        //! stack in
        for ( int j = 0; j < mUsedRow; j++ )
        {
            stack.push( *cell(j,i) );
        }

        //! shift
        stack.shift( i );

        //! stackout
        for ( int j = 0; j < mUsedRow; j++ )
        {
            *cell( j, i ) = stack.pop();
        }
    }
}

void RegArray::setRange( int useRow )
{
    if( useRow > 0 && useRow <= mRow )
    {}
    else
    { qDebug()<<useRow<<mRow; Q_ASSERT(false);}

    mUsedRow = useRow;
}
void RegArray::setGener( int rowSpan,
               int colSpan,
               int rowMod )
{
    Q_ASSERT( rowSpan > 0 );
    Q_ASSERT( colSpan > 0 );
    Q_ASSERT( rowMod > 0 );

    mRowSpan = rowSpan;
    mColSpan = colSpan;
    mRowMod = rowMod;
}

void RegArray::save( QTextStream &stream )
{
//    stream<<mUsedRow<<","<<endl;
    for ( int i = 0; i < mUsedRow; i++ )
    {
        for ( int j = 0; j < mColumn; j++ )
        {
            stream<<*cell(i,j)<<",";
        }
        stream<<endl;
    }
    stream<<endl;
}

void RegArray::save( const QString &fileName )
{
    QFile file( fileName );

    if ( !file.open( QIODevice::WriteOnly ) )
    {
        Q_ASSERT( false );
    }

    QTextStream stream(&file);

    save( stream );

    file.close();
}

int *RegArray::cell(int row, int col )
{
    Q_ASSERT( row >= 0 && row < mRow );
    Q_ASSERT( col >=0 && col < mColumn );

    return m_pBase + row * mColumn + col;
}

IntxGen::IntxGen()
{
    mIntX = 0;
    mIntXCnt = 0;
    mLcm = 0;

    m_pArray = NULL;
}

IntxGen::~IntxGen()
{
    if ( NULL != m_pArray )
    { delete []m_pArray; }
}

void IntxGen::gen( int x, int cnt )
{
    mIntX = x;
    mIntXCnt = cnt;

    mLcm = lcm( mIntX, mIntXCnt );
    mRowCnt = mLcm / mIntXCnt;

    m_pArray = new RegArray[ mIntXCnt ];
    Q_ASSERT( NULL != m_pArray );

    for ( int i = 0; i < mIntXCnt; i++ )
    {
        m_pArray[i].init( 200, 8 );
        m_pArray[i].setRange( mRowCnt );

        m_pArray[i].setGener( mIntXCnt, mIntX, mIntX );

        m_pArray[i].setCell0( i );

        m_pArray[i].shift();
    }
}

void IntxGen::save( const QString &dir, const QString &filepre )
{
    QString fileName;

    fileName = dir + filepre + QString("_%1_%2").arg(mIntX).arg(mIntXCnt) + ".csv";

    QFile file( fileName );
    if ( !file.open(QIODevice::WriteOnly) )
    {
        Q_ASSERT( false );
    }

    QTextStream stream(&file);

    stream<<"intX,"<<mIntX<<",cnt,"<<mIntXCnt<<",lcm,"<<mLcm<<endl;
    for ( int i = 0; i < mIntXCnt; i++ )
    {
        m_pArray[i].save( stream );
    }

    file.close();
}

QMap<int, int *> IntxCoeff::_coeffMap;

void IntxCoeff::addMap( int x, int *pCoeff )
{
    if ( IntxCoeff::_coeffMap.contains(x) )
    {
        Q_ASSERT( false );
    }

    IntxCoeff::_coeffMap.insert( x, pCoeff );
}

void IntxCoeff::fillCoeff( IntxGen *pGen  )
{
    Q_ASSERT( NULL != pGen );

    //! find x
    QMap<int,int*>::const_iterator iter = IntxCoeff::_coeffMap.find( pGen->mIntX );

    if ( iter == IntxCoeff::_coeffMap.end() )
    { Q_ASSERT(false); }

    int *pCoeff = iter.value();
    Q_ASSERT( pCoeff );

    //! for each array
    RegArray *pAry;
    for ( int i = 0; i < pGen->mIntXCnt; i++ )
    {
        //! fill array
        pAry = &pGen->m_pArray[i];

        for ( int j = 0; j < pAry->mUsedRow*pAry->mColumn; j++ )
        {
            pAry->m_pBase[j] = pCoeff[ pAry->m_pBase[j] ];
        }
    }
}
}
