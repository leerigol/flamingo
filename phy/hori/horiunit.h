#ifndef HORIUNIT_H
#define HORIUNIT_H

#include "../iphy.h"

namespace dso_phy {

enum data_path
{
    data_path_main = 0,
    data_path_zoom = 1,
    data_path_fine = 2,
    data_path_eye = 3,
};

class HoriPara
{
public:
    HoriPara();
public:
    void setPath( data_path path );
    void setSa( int dep, int el, int eh );
    void setAcquire( AcquireMode acq,
                     int chCnt,
                     quint32 intxShift = 0 );

    void setCH(int intxp, int comp1, int comp2,
                int len, int MLeft, int MLen,
                bool LinearIntxOn,
                int LinearIntxM, int LinearIntxN,
                int scrLeft=0, int scrLen = 1000 );

    void setLa(int intxp, int comp1);

    void setEye( int intxp, int comp, int len=0 );
    void setMask( int intxp, int comp, int len=0 );

    void setTrigCol( int trigPos );
    void calFrame();

    void dbgOut();
public:
    data_path mPath;

    int mDepth;
    int mEl, mEh;

    AcquireMode mAcqMode;
    AcquireTimemode mAcqTimeMode;

    int mChCnt;

    qlonglong mMLeft, mMLen;
    int mScrLeft, mScrLen;

    qlonglong mTxLen;

    //! 是否打开线性内插
    bool mLinearIntxOn;
    qlonglong  mMLinearIntx;
    qlonglong  mNLinearIntx;

    qlonglong mIntxp;
    quint32   mIntxShift;
    qlonglong mCompL1;
    qlonglong mCompL2;

    qlonglong mLaIntxp;
    qlonglong mLaCompL1;

    qlonglong mEyeComp;
    qlonglong mEyeIntxp;
    qlonglong mEyeLength;

    qlonglong mMaskComp;
    qlonglong mMaskIntxp;
    qlonglong mMaskLength;

    //! scan and roll
    int  mFrameNum;
    int  mPointPerFrame;
    qlonglong mTrigCol;
};

class IPhyHori;
class HoriUnit
{
public:
    HoriUnit();

public:
    void attachPhy( IPhyHori *pHori );

    DsoErr config( HoriPara &para );

protected:
    virtual DsoErr configSa( HoriPara &para );
    virtual DsoErr configAcquire( HoriPara &para );
    virtual DsoErr configCH( HoriPara &para );
    virtual DsoErr configLA( HoriPara &para );
    virtual DsoErr configEye( HoriPara &para );
    virtual DsoErr configMask( HoriPara &para );

protected:
    DsoErr configMainInterp( HoriPara &para );
    DsoErr configZoomInterp( HoriPara &para );
    DsoErr configFineInterp( HoriPara &para );

protected:
    IPhyHori *m_pPhyHori;
};

//! main unit
class HoriMainUnit : public HoriUnit
{
public:
    HoriMainUnit();

protected:
    virtual DsoErr configSa( HoriPara &para );
    virtual DsoErr configAcquire( HoriPara &para );
    virtual DsoErr configCH( HoriPara &para );
    virtual DsoErr configLA( HoriPara &para );
    virtual DsoErr configEye( HoriPara &para );
    virtual DsoErr configMask( HoriPara &para );
};

//! zoom unit
class HoriZoomUnit : public HoriUnit
{
public:
    HoriZoomUnit();
protected:
    virtual DsoErr configAcquire( HoriPara &para );
    virtual DsoErr configCH( HoriPara &para );
    virtual DsoErr configLA( HoriPara &para );
};

class HoriMainPlayUnit : public HoriUnit
{
public:
    HoriMainPlayUnit();
protected:
    virtual DsoErr configAcquire( HoriPara &para );
    virtual DsoErr configCH( HoriPara &para );
    virtual DsoErr configLA( HoriPara &para );
};

class HoriZoomPlayUnit : public HoriUnit
{
public:
    HoriZoomPlayUnit();
protected:
    virtual DsoErr configAcquire( HoriPara &para );
    virtual DsoErr configCH( HoriPara &para );
    virtual DsoErr configLA( HoriPara &para );
};

}

#endif // HORIUNIT_H
