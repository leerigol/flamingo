#ifndef CHORIALIGN_H
#define CHORIALIGN_H

#include <QtCore>

namespace dso_phy {

class CHoriAlign
{
public:
    CHoriAlign();
public:
    //! two chan interleave
    void alignDouble( int adcs[2],
                      int aligns[2],
                      int adcRange = 256 );

    //! 4 chan interleave
    void alignQuad( int adcs[4],
                    int aligns[4],
                    int adcRange = 256 );

private:
    void normalize( int vals[],
                    int cnt,
                    int outNorm[],
                    int top );
};

}

#endif // CHORIALIGN_H
