#include "horiunit.h"

#include "./iphyhori.h"

namespace dso_phy {

HoriPara::HoriPara()
{
}

void HoriPara::setPath( data_path path )
{
    mPath = path;
}

void HoriPara::setSa( int dep, int el, int eh )
{
    mDepth = dep;
    mEl = el;
    mEh = eh;
}

void HoriPara::setAcquire( AcquireMode acq,
                           int chCnt,
                           quint32 intxShift )
{
    mAcqMode = acq;
    mChCnt = chCnt;

    mIntxShift = intxShift;
}

void HoriPara::setCH(int intxp, int comp1, int comp2, int len,
                      int MLeft, int MLen, bool LinearIntxOn, int LinearIntxM, int LinearIntxN,
                      int scrLeft, int scrLen )
{
    mIntxp = intxp;
    mCompL1 = comp1;
    mCompL2 = comp2;
    mTxLen = len;

    mMLeft = MLeft;
    mMLen = MLen;

    mLinearIntxOn = LinearIntxOn;
    mMLinearIntx = LinearIntxM;
    mNLinearIntx = LinearIntxN;

    mScrLeft = scrLeft;
    mScrLen = scrLen;
}

void HoriPara::setLa( int intxp, int comp1)
{
    mLaIntxp = intxp;
    mLaCompL1 = comp1;
}

void HoriPara::setEye( int intxp, int comp, int len )
{
    mEyeIntxp = intxp;
    mEyeComp = comp;
    mEyeLength = len;
}

void HoriPara::setMask( int intxp, int comp, int len )
{
    mMaskIntxp = intxp;
    mMaskComp = comp;
    mMaskLength = len;
}

void HoriPara::setTrigCol(int trigPos)
{
    mTrigCol = trigPos;
}

void HoriPara::dbgOut()
{
    LOG_ON_FILE()<<"memLeft,"<<mMLeft<<"memLen,"<<mMLen;
    LOG_ON_FILE()<<"scrLeft,"<<mScrLeft<<"scrLen,"<<mScrLen;
    LOG_ON_FILE()<<"mTxLen,"<<mTxLen;
    LOG_ON_FILE()<<"mCompL1"<<mCompL1<<"mCompL2"<<mCompL2;
    LOG_ON_FILE()<<"mIntxp"<<mIntxp;
    LOG_ON_FILE()<<"mLinearIntxOn"<<mLinearIntxOn;
    LOG_ON_FILE()<<"mLinearUp"<<mMLinearIntx<<"mLinearDown"<<mNLinearIntx;
    LOG_ON_FILE()<<"scan_trig_col"<<mTrigCol<<"scan roll frame"<<mFrameNum
                <<"point per frame"<<mPointPerFrame;
}

void HoriPara::calFrame()
{
    mPointPerFrame = align_8(mTxLen / 1000);

    if( mTrigCol >= 1000 )
    {
        mFrameNum = 0;
    }
    else
    {
        mFrameNum = 1000 - mTrigCol - 1;
    }
}

HoriUnit::HoriUnit()
{
    m_pPhyHori = NULL;
}

void HoriUnit::attachPhy( IPhyHori *pHori )
{
    Q_ASSERT( pHori != NULL );

    m_pPhyHori = pHori;
}

DsoErr HoriUnit::config( HoriPara &para )
{
    DsoErr err;
    if( m_pPhyHori == NULL )
    {
        LOG_DBG() << "!!Error";
        return ERR_NONE;
    }
    err = configSa( para );
    if ( err != ERR_NONE ) { return err; }

    err = configAcquire( para );
    if ( err != ERR_NONE ) { return err; }

    err = configCH( para );
    if ( err != ERR_NONE ) { return err; }

    err = configLA( para );
    if ( err != ERR_NONE ) { return err; }

    err = configEye( para );
    if ( err != ERR_NONE ) { return err; }

    err = configMask( para );
    if ( err != ERR_NONE ) { return err; }

    return ERR_NONE;
}

DsoErr HoriUnit::configSa( HoriPara &/*para*/ )
{ return ERR_NONE; }
DsoErr HoriUnit::configAcquire( HoriPara &/*para*/ )
{ return ERR_NONE; }
DsoErr HoriUnit::configCH( HoriPara &/*para*/ )
{ return ERR_NONE; }
DsoErr HoriUnit::configLA( HoriPara &/*para*/ )
{ return ERR_NONE; }
DsoErr HoriUnit::configEye( HoriPara &/*para*/ )
{ return ERR_NONE; }
DsoErr HoriUnit::configMask( HoriPara &/*para*/ )
{ return ERR_NONE; }

DsoErr HoriUnit::configMainInterp( HoriPara &para )
{
    //! max interp
    if ( para.mIntxp > m_pPhyHori->getMaxInterp() )
    {
        LOG_DBG()<<para.mIntxp;
        qWarning()<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! main  over range interp";
        return ERR_OVER_UPPER_RANGE;
    }

    //! cfg interp
    if ( para.mIntxp > 1 )
    {
        IPhySpu *pSpu;
        //! for each spu
        for ( int i = 0; i < m_pPhyHori->m_pSpuGp->size(); i++ )
        {
            pSpu = (*m_pPhyHori->m_pSpuGp)[i];

            //! coarse
            //! main window
            m_pPhyHori->setInterxp( para.mIntxp,
                        m_pPhyHori->toXpCnt( para.mChCnt ),
                        pSpu->addr_INTERP_MULT_MAIN(),
                        para.mPath );
        }

        LOG_ON_FILE()<<para.mIntxp;
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setINTERP_MULT_MAIN_en( 0 );
    }

    return ERR_NONE;
}

DsoErr HoriUnit::configZoomInterp( HoriPara &para )
{
    //! max interp
    if ( para.mIntxp > m_pPhyHori->getMaxInterp() )
    {
        qWarning()<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! zoom  over range interp";
        return ERR_OVER_UPPER_RANGE;
    }

    //! cfg interp
    if ( para.mIntxp > 1 )
    {
        IPhySpu *pSpu;
        //! for each spu
        for ( int i = 0; i < m_pPhyHori->m_pSpuGp->size(); i++ )
        {
            pSpu = (*m_pPhyHori->m_pSpuGp)[i];

            //! coarse
            m_pPhyHori->setInterxp( para.mIntxp,
                        m_pPhyHori->toXpCnt( para.mChCnt ),
                        pSpu->addr_INTERP_MULT_ZOOM(),
                        para.mPath );
        }
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setINTERP_MULT_ZOOM_en( 0 );
    }

    return ERR_NONE;
}

DsoErr HoriUnit::configFineInterp( HoriPara &para )
{
    //! cfg interp
    if ( para.mIntxp > 1 )
    {
        IPhySpu *pSpu;
        //! for each spu
        for ( int i = 0; i < m_pPhyHori->m_pSpuGp->size(); i++ )
        {
            pSpu = (*m_pPhyHori->m_pSpuGp)[i];

            //! fine: stable 4 intxs
            m_pPhyHori->setInterxp( 252,
                        4,
                        pSpu->addr_INTERP_MULT_MAIN_FINE(),
                        data_path_fine );
        }
    }
    else
    {
//        m_pPhyHori->m_pSpuGp->setINTERP_MULT_MAIN_FINE_en( 0 );
    }

    return ERR_NONE;
}

//! main unit
HoriMainUnit::HoriMainUnit()
{}

DsoErr HoriMainUnit::configSa( HoriPara &para )
{
    //! \errant fine trig delay +1
    m_pPhyHori->m_pSpuGp->setRecordLen( para.mDepth + 1 );

    //! cfg sample
    if ( para.mEl != 1 )
    {
        m_pPhyHori->m_pSpuGp->setCHExtractor( 0, para.mEl );
    }
    //! == 1
    else
    {
        m_pPhyHori->m_pSpuGp->setCHExtractor( para.mEh, 2 );
    }

    int temp = para.mEl;
    int antiRandom = 1;
    //! antiRand
    while(temp >= 1)
    {
        antiRandom *= 2;
        temp = temp / 2;
    }

    m_pPhyHori->m_pSpuGp->setRANDOM_RANGE(antiRandom - 1);
    //! meas
    m_pPhyHori->m_pSpuGp->setMEAS_LEN_CH( para.mDepth );
    //! \todo la

    return ERR_NONE;
}

DsoErr HoriMainUnit::configAcquire( HoriPara &para )
{
    //! chan mode
    Q_ASSERT( para.mChCnt == 1 || para.mChCnt == 2 || para.mChCnt == 4 );
    m_pPhyHori->m_pFcuW->setdeinterweave_property_cfg4_channel_mode( para.mChCnt );

    m_pPhyHori->m_pSpuGp->setCOMPRESS_MAIN_peak( 1 );
    //! acquire
    if ( para.mAcqMode == Acquire_Peak )
    {
        m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 1 );
    }
    else
    {
        //! normal
        //! 压缩率小于4的时候只能开启峰值
        if ( para.mCompL1 >= 4 )
        {
            if(sysHasArg("-peak_compress"))
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 1 );
            }
            else
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 0 );
            }
        }
        else
        {
            m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 1 );
        }
    }
    return ERR_NONE;
}

DsoErr HoriMainUnit::configCH( HoriPara &para )
{
    DsoErr err;
    LOG_ON_FILE()<<"Main Para Unit ";
    para.dbgOut();
    //! compress
    m_pPhyHori->m_pSpuGp->setMainCHCompress( para.mCompL1 );

    if(para.mMLinearIntx > para.mNLinearIntx)
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_STEP(
                    para.mNLinearIntx * 0xFFFFFFFF / para.mMLinearIntx);
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_mian_bypass( 0 );
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_STEP( 0xFFFFFFFF );
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_mian_bypass( 1 );
    }

    m_pPhyHori->m_pSpuGp->outZYNQ_COMPRESS_MAIN_rate( 1 );
    m_pPhyHori->m_pWpu->setscale_times_scale_times_en( para.mCompL2 > 1 );
    m_pPhyHori->m_pWpu->setscale_times_scale_times_m( para.mCompL2 );
    m_pPhyHori->m_pWpu->setscale_times_scale_times_n( 1 );
    m_pPhyHori->m_pWpu->setscale_length( para.mTxLen );

    //! cfg interp
    err = configMainInterp( para );
    if ( err != ERR_NONE )
    { return err; }

    configFineInterp( para );

    //! mem left--len

    //! ch length
    m_pPhyHori->m_pSpuGp->setTX_LEN_CH( para.mTxLen );

    //! scan
    if( para.mAcqTimeMode == Acquire_SCAN || para.mAcqTimeMode == Acquire_ROLL )
    {
        para.calFrame();
        m_pPhyHori->m_pSpuGp->setTX_LEN_COL_CH( para.mPointPerFrame );
        if( para.mAcqTimeMode == Acquire_SCAN )
        {
            m_pPhyHori->m_pSpuGp->setSCAN_ROLL_FRM_scan_roll_frm_num( para.mFrameNum );
        }
    }
    return ERR_NONE;
}

DsoErr HoriMainUnit::configLA( HoriPara &para )
{
    //! for la only spu0
    m_pPhyHori->m_pSpuGp->setMainLaCompress( para.mCompL1 );

    //! no wpu compress
    m_pPhyHori->m_pSpuGp->outZYNQ_COMPRESS_MAIN_rate( para.mCompL2 );

    //! la
    m_pPhyHori->m_pSpuGp->setTX_LEN( 1, para.mTxLen );

    int chToLa = 8 / para.mChCnt;

    Q_ASSERT( para.mChCnt > 0 );

    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_STEP(
                para.mNLinearIntx * 0xFFFFFFFF / para.mMLinearIntx/chToLa/para.mIntxp);
    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_CTRL_mian_bypass( 0 );

    //! scan
    if( para.mAcqTimeMode == Acquire_SCAN || para.mAcqTimeMode == Acquire_ROLL )
    {
        LOG_ON_FILE()<<"time_mode"<<para.mAcqTimeMode;
        para.calFrame();
        m_pPhyHori->m_pSpuGp->setTX_LEN_COL_LA( para.mPointPerFrame );
        if( para.mAcqTimeMode == Acquire_SCAN )
        {
            m_pPhyHori->m_pSpuGp->setSCAN_ROLL_FRM_scan_roll_frm_num( para.mFrameNum );
        }
    }
    return ERR_NONE;
}

DsoErr HoriMainUnit::configEye( HoriPara &para )
{
    //! eye limit
    if ( para.mEyeIntxp <= 200 )
    {}
    else
    { return ERR_NONE; }

    //! interp
    if ( para.mEyeIntxp > 1 )
    {
        IPhySpu *pSpu;
        //! for each spu
        for ( int i = 0; i < m_pPhyHori->m_pSpuGp->size(); i++ )
        {
            pSpu = (*m_pPhyHori->m_pSpuGp)[i];

            //! eye
            m_pPhyHori->setInterxp( para.mEyeIntxp,
                        m_pPhyHori->toXpCnt( para.mChCnt),
                        pSpu->addr_INTERP_MULT_EYE(),
                        data_path_eye );
        }
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setINTERP_MULT_EYE_en( 0 );
    }

    //! offset - len
    //! full memory
    m_pPhyHori->m_pSpuGp->setWAVE_LEN_EYE( para.mDepth );
    m_pPhyHori->m_pSpuGp->setWAVE_OFFSET_EYE( 0 );

    //! compress
    if ( para.mEyeComp > 1 )
    {
        m_pPhyHori->m_pSpuGp->setCOMPRESS_EYE_en( 1 );
        m_pPhyHori->m_pSpuGp->setCOMPRESS_EYE_rate( para.mEyeComp );
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setCOMPRESS_EYE_en( 0 );
    }

    m_pPhyHori->m_pSpuGp->setTX_LEN_2( 0, para.mEyeLength );   //! eye

    return ERR_NONE;
}

DsoErr HoriMainUnit::configMask( HoriPara &para )
{
    if ( para.mMaskComp > 1 )
    {
        { m_pPhyHori->m_pSpuGp->setMASK_COMPRESS_rate( para.mMaskComp ); }
//        if ( m_pPhyHori->m_pCcu->getMODE_mask_pf_en() )
        { m_pPhyHori->m_pSpuGp->setMASK_COMPRESS_en( 1 ); }
//        else
//        { m_pPhyHori->m_pSpuGp->setMASK_COMPRESS_en( 0 ); }
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setMASK_COMPRESS_en( 0 );
        m_pPhyHori->m_pSpuGp->setMASK_COMPRESS_rate( 1 );
    }

    return ERR_NONE;
}

//! zoom unit
HoriZoomUnit::HoriZoomUnit()
{}

DsoErr HoriZoomUnit::configAcquire( HoriPara &para )
{
    //! 波形压缩
    m_pPhyHori->m_pSpuGp->setCOMPRESS_ZOOM_peak( 1 );

    //! trace压缩
    if ( para.mAcqMode == Acquire_Peak )
    {
        m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 1 );
    }
    else
    {
        //! normal
        if ( para.mCompL1 >= 4 )
        {
            if(sysHasArg("-peak_compress"))
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 1 );
            }
            else
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 0 );
            }
        }
        else
        {
            m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 1 );
        }
    }

    return ERR_NONE;
}

DsoErr HoriZoomUnit::configCH( HoriPara &para )
{
    DsoErr err;

    //! compress
    m_pPhyHori->m_pSpuGp->setZoomCHCompress( para.mCompL1 );

    //! zynq compress
    if(para.mMLinearIntx > para.mNLinearIntx)
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_STEP_ZOOM( para.mNLinearIntx *
                                                        0xFFFFFFFF / para.mMLinearIntx);
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_zoom_bypass( 0 );
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_STEP_ZOOM( 0xFFFFFFFF);
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_zoom_bypass( 1 );
    }
    m_pPhyHori->m_pSpuGp->setZYNQ_COMPRESS_ZOOM_rate( 1 );

    m_pPhyHori->m_pWpu->setzoom_scale_times_zoom_scale_times_en( para.mCompL2 > 1 );
    m_pPhyHori->m_pWpu->setzoom_scale_times_zoom_scale_times_m( para.mCompL2 );
    m_pPhyHori->m_pWpu->setzoom_scale_times_zoom_scale_times_n( 1 );
    m_pPhyHori->m_pWpu->setzoom_scale_length( para.mTxLen );

    //! cfg interp
    err = configZoomInterp( para );
    if ( err != ERR_NONE )
    { return err; }

    //! Memory
    m_pPhyHori->m_pSpuGp->setWAVE_OFFSET_ZOOM( para.mMLeft );
    m_pPhyHori->m_pSpuGp->setWAVE_LEN_ZOOM( para.mMLen + 8 * para.mCompL1 );//! 防止由于对齐而产生的点数不足

    //! zoom ch
    m_pPhyHori->m_pSpuGp->outTX_LEN_ZOOM_CH(para.mTxLen, -1);
    LOG_ON_FILE()<<"Zoom Unit ";
    para.dbgOut();
    return ERR_NONE;
}

DsoErr HoriZoomUnit::configLA( HoriPara &para )
{
    //! for la only spu0
    m_pPhyHori->m_pSpuGp->setZoomLaCompress( para.mCompL1 );

    //! la zoom
    m_pPhyHori->m_pSpuGp->outZYNQ_COMPRESS_ZOOM_rate( para.mCompL2 );

    int chToLa = 8 / para.mChCnt;

    Q_ASSERT( para.mChCnt > 0 );

    //! la zoom
    m_pPhyHori->m_pSpuGp->setTX_LEN( 3, para.mTxLen );      //! zoom la


    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_STEP_ZOOM(
                para.mNLinearIntx * 0xFFFFFFFF / para.mMLinearIntx/chToLa/para.mIntxp);
    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_CTRL_zoom_bypass( 0 );


    return ERR_NONE;
}


HoriMainPlayUnit::HoriMainPlayUnit()
{
}

DsoErr HoriMainPlayUnit::configAcquire( HoriPara &para )
{
    //! 波形压缩
    m_pPhyHori->m_pSpuGp->setCOMPRESS_MAIN_peak( 1 );

    //! trace压缩
    if ( para.mAcqMode == Acquire_Peak )
    {
        m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 1 );
    }
    else
    {
        //! normal
        //! 压缩率小于4的时候只能开启峰值
        if ( para.mCompL1 >= 4 )
        {
            if(sysHasArg("-peak_compress"))
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 1 );
            }
            else
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 0 );
            }
        }
        else
        {
            m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_MAIN_peak( 1 );
        }
    }
    return ERR_NONE;
}

DsoErr HoriMainPlayUnit::configCH( HoriPara &para )
{
    DsoErr err;
    //! compress
    LOG_ON_FILE()<<"main play unit";
    para.dbgOut();
    m_pPhyHori->m_pSpuGp->setMainCHCompress( para.mCompL1 );

    //! main
    m_pPhyHori->m_pSpuGp->setTX_LEN( 0, para.mTxLen );
    m_pPhyHori->m_pSpuGp->setWAVE_OFFSET_MAIN( para.mMLeft );
    m_pPhyHori->m_pSpuGp->setWAVE_LEN_MAIN( para.mMLen + 8 * para.mCompL1);
    //! check length
    m_pPhyHori->m_pSpuGp->setFINE_DELAY_MAIN_chn_delay( para.mIntxShift );

    //! interp
    err = configMainInterp( para );
    if ( err != ERR_NONE )
    { return err; }

    //! wpu compress
    if(para.mMLinearIntx > para.mNLinearIntx)
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_STEP( para.mNLinearIntx *
                                                   0xFFFFFFFF / para.mMLinearIntx);
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_mian_bypass( 0 );
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_mian_bypass( 1 );
    }
    m_pPhyHori->m_pSpuGp->outZYNQ_COMPRESS_MAIN_rate( 1 );
    m_pPhyHori->m_pWpu->setscale_times_scale_times_en( para.mCompL2 > 1 );
    m_pPhyHori->m_pWpu->setscale_times_scale_times_m( para.mCompL2 );
    m_pPhyHori->m_pWpu->setscale_times_scale_times_n( 1 );
    m_pPhyHori->m_pWpu->setscale_length( para.mTxLen );

    m_pPhyHori->m_pWpu->setMainView( para.mScrLeft, para.mScrLen );
    return ERR_NONE;
}

//! \todo la only 1000 now
DsoErr HoriMainPlayUnit::configLA( HoriPara &para )
{   
    //! la no compress
    //! for la only spu0
    m_pPhyHori->m_pSpuGp->setMainLaCompress( para.mCompL1 );

    //! no wpu compress
    m_pPhyHori->m_pSpuGp->outZYNQ_COMPRESS_MAIN_rate( para.mCompL2 );

    int chToLa = 8 / para.mChCnt;
    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_STEP(
                para.mNLinearIntx * 0xFFFFFFFF / para.mMLinearIntx/chToLa/para.mIntxp);
    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_CTRL_mian_bypass( 0 );

    //! la
    m_pPhyHori->m_pSpuGp->setTX_LEN( 1, para.mTxLen );
    m_pPhyHori->m_pWpu->setMainView( para.mScrLeft, para.mScrLen );

    return ERR_NONE;
}

HoriZoomPlayUnit::HoriZoomPlayUnit()
{
}

DsoErr HoriZoomPlayUnit::configAcquire( HoriPara &para )
{
    //! 波形压缩
    m_pPhyHori->m_pSpuGp->setCOMPRESS_ZOOM_peak( 1 );

    //! trace压缩
    if ( para.mAcqMode == Acquire_Peak )
    {
        m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 1 );
    }
    else
    {
        //! normal
        if ( para.mCompL1 >= 4 )
        {
            if( sysHasArg("-peak_compress") )
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 1 );
            }
            else
            {
                m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 0 );
            }
        }
        else
        {
            m_pPhyHori->m_pSpuGp->setTRACE_COMPRESS_ZOOM_peak( 1 );
        }
    }
    return ERR_NONE;
}

DsoErr HoriZoomPlayUnit::configCH( HoriPara &para )
{
    DsoErr err;

    para.dbgOut();
    //! compress
    m_pPhyHori->m_pSpuGp->setZoomCHCompress( para.mCompL1 );

    //! zoom
    m_pPhyHori->m_pSpuGp->setTX_LEN( 2, para.mTxLen );
    m_pPhyHori->m_pSpuGp->setWAVE_OFFSET_ZOOM( para.mMLeft );
    m_pPhyHori->m_pSpuGp->setWAVE_LEN_ZOOM( para.mMLen  + 8 * para.mCompL1);

    m_pPhyHori->m_pSpuGp->setFINE_DELAY_ZOOM_chn_delay( para.mIntxShift );

    //! interp
    err = configZoomInterp( para );
    if ( err != ERR_NONE )
    {
        return err;
    }

    //! wpu compress
    if( para.mMLinearIntx > para.mNLinearIntx )
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_STEP_ZOOM( para.mNLinearIntx *
                                                        0x100000000 / para.mMLinearIntx);
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_zoom_bypass( 0 );
    }
    else
    {
        m_pPhyHori->m_pSpuGp->setLINEAR_INTX_CTRL_zoom_bypass( 1 );
    }
    m_pPhyHori->m_pSpuGp->outZYNQ_COMPRESS_ZOOM_rate( 1 );

    m_pPhyHori->m_pWpu->setzoom_scale_times_zoom_scale_times_en( para.mCompL2 > 1 );
    m_pPhyHori->m_pWpu->setzoom_scale_times_zoom_scale_times_m( para.mCompL2 );
    m_pPhyHori->m_pWpu->setzoom_scale_times_zoom_scale_times_n( 1 );

    m_pPhyHori->m_pWpu->setzoom_scale_length( para.mTxLen );

    m_pPhyHori->m_pWpu->setZoomView( para.mScrLeft, para.mScrLen );
    return ERR_NONE;
}

DsoErr HoriZoomPlayUnit::configLA( HoriPara &para )
{
    //! for la only spu0
    m_pPhyHori->m_pSpuGp->setZoomLaCompress( para.mCompL1 );

    //! la zoom
    m_pPhyHori->m_pSpuGp->outZYNQ_COMPRESS_ZOOM_rate( para.mCompL2 );

    int chToLa = 8 / para.mChCnt;
    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_STEP_ZOOM(
                para.mNLinearIntx * 0xFFFFFFFF / para.mMLinearIntx/chToLa/para.mIntxp);
    m_pPhyHori->m_pSpuGp->setLA_LINEAR_INTX_CTRL_zoom_bypass( 0 );
    //! la zoom
    m_pPhyHori->m_pSpuGp->setTX_LEN( 3, para.mTxLen );      //! zoom la
    m_pPhyHori->m_pWpu->setZoomView( para.mScrLeft, para.mScrLen );

    return ERR_NONE;
}
}

