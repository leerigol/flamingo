#ifndef FLOWVIEW_H
#define FLOWVIEW_H

#include <QtCore>

#include "../../include/dsostd.h"

#include "../../include/dsoarith.h"

#include "../../baseclass/saflow/saflow.h"


namespace dso_phy {

//! saFlow -> view -> saFlow
//!                -> view cfg
class CFlowView
{
public:
    CFlowView();

public:
    void set( qlonglong toffset,
              qlonglong tleft,
              qlonglong tinc,
              qlonglong width=trace_width );
    void setAcq( AcquireMode acq, int chCnt, quint32 finePos = 0 );

    void cfgTransfer( qlonglong transCap,
                       qlonglong transAlign = 1000,
                       qlonglong comp2Len = 1000 );
    void cfgAlign( qlonglong transAlign = 1000 );

public:
    CSaFlow& getFlow();

public:
    int viewOn( CSaFlow &saFlow, bool isRoll = false);
    int viewOn( CSaFlow &saFlow, int transLength );  //! full length
    int viewOn( CSaFlow &saFlow,
                qlonglong viewScale,
                qlonglong viewOffset );

    int viewOnFine( CFlowView* pCoarseView, CSaFlow &saFlow,
                   qlonglong normScale,
                   qlonglong viewScale,
                   qlonglong viewOffset );
    int viewOnPlayFine( CFlowView* pCoarseView, CSaFlow &saFlow,
                        qlonglong normScale,
                        qlonglong viewScale,
                        qlonglong viewOffset );
    int viewOnLa(CFlowView *pCHView);

    void setCorseComp(CFlowView* corseFlowView);

    void viewSlove( CSaFlow &saFlow, bool isRoll = false);
    void viewGen( CSaFlow &saFlow );

    void viewFix( CSaFlow &saFlow );
    void debugOut();

protected:
    void viewRange( CSaFlow &saFlow, bool isRoll = false);
    void viewMemLen(CSaFlow &saFlow);
    void viewIntxComp( CSaFlow &saFlow  );
    void viewRawLen( CSaFlow &saFlow );

    void viewMemRange(CSaFlow &saFlow , bool isRoll);

    void viewMemFine0( CSaFlow &saFlow );
    void viewMemFine( CSaFlow &saFlow );


    void transferIntxComp( CSaFlow &saFlow );

    void transferCompL1( CSaFlow &saFlow );
    void transferCompL2( CSaFlow &saFlow );

    //! \return comp
    qlonglong squash( qlonglong rawLen,
                    qlonglong cap = 1000000,
                    qlonglong align = 1000 );

public:
    qlonglong mTLeft, mTInc, mWidth, mTOffset;        //! view coordinate
    dsoFract mScrTLeft;
    quint32 mFinePos;

    AcquireMode mAcquire;                   //! chan count
    int mAcqChCnt;

    //! in memory
    dsoFract  mViewMemLen;                  //! mViewMemLen = M/N
    qlonglong mViewMemLenCeil;

    dsoFract  mViewMemSpan;

    qlonglong mViewRawLen;                  //! viewMem * intx/comp

    //! tranfer info
    qlonglong mTransferCap;                 //! config
    qlonglong mTransferAlign;
    qlonglong mCompL2Len;

    qlonglong mTransferLen;                 //! transfer len
    qlonglong mTransferDotTime;
    qlonglong mTransferT0;
public:
    dsoFract  mLinearIntx;
    bool      mLinearIntxOn;
    qlonglong mIntx, mComp;                 //! total
    qlonglong mCompL1;
    qlonglong mFixCompL2;
    int mFixScrLen;


    dsoFract mScrLeft, mScrRight;           //! view coord

    quint32  mIntxRema;
    quint32  mIntxRema0;

    qlonglong mMStart, mMLen;               //! memory

    qlonglong mCompL2;                      //! level2 comp
    dsoFract mScrLen;                       //! raw len

    dsoFract mTrigPos;
private:
    bool mbFailSlove;
    //! out flow
    CSaFlow mViewFlow;
};

}

#endif // FLOWVIEW_H
