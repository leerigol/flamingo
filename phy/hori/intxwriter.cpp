#include "intxwriter.h"

#include "../iphyfpga.h"

namespace dso_phy {

#include "../ic/spu.h"

//! addr config
#define addr_INTERP_COEF_TAP    0x14A0
#define addr_INTERP_COEF_WR     0x14A4

IntxWriter::IntxWriter()
{

}

void IntxWriter::write( IntxGen *pGen,
                        IBus *pBus,
                        unsigned int multAddr,
                        unsigned int coefGroup )
{
    Q_ASSERT( NULL != pGen );
    Q_ASSERT( NULL != pBus );

    //! mult
    Spu_INTERP_MULT_MAIN mult;
    mult.payload = 0;

    mult.coef_len = pGen->mRowCnt;
    mult.comm_mult = pGen->mLcm;
    mult.mult = pGen->mIntX;
    mult.en = 1;

    //! select
    Spu_INTERP_COEF_TAP tap;
    tap.payload = 0;

    //! config
    Spu_INTERP_COEF_WR wr;
    wr.payload = 0;
    wr.wr_en = 1;
    wr.coef_group = coefGroup;

    //! apply mult
    pBus->writeOp( &mult, 4, multAddr );

    //! 4 times
    if ( pGen->mIntXCnt == 4 )
    {
        tap.ce = 1;
        configArray( pGen->m_pArray[0], tap.payload, wr.payload, pBus );

        tap.ce = 2;
        configArray( pGen->m_pArray[1], tap.payload, wr.payload, pBus );

        tap.ce = 4;
        configArray( pGen->m_pArray[2], tap.payload, wr.payload, pBus );

        tap.ce = 8;
        configArray( pGen->m_pArray[3], tap.payload, wr.payload, pBus );
    }
    //! 2 times
    else if ( pGen->mIntXCnt == 2 )
    {
        tap.ce = 1+4;
        configArray( pGen->m_pArray[0], tap.payload, wr.payload, pBus );

        tap.ce = 2+8;
        configArray( pGen->m_pArray[1], tap.payload, wr.payload, pBus );
    }
    else if ( pGen->mIntXCnt == 1 )
    {
        //! all items
        tap.ce = 0x0f;

        configArray( pGen->m_pArray[0], tap.payload, wr.payload, pBus );
    }
    else
    {
        Q_ASSERT( false );
    }

    //! close
    tap.ce = 0x0;
    pBus->writeOp( &tap, 4, addr_INTERP_COEF_TAP );

    wr.wr_en = 0;
    pBus->writeOp( &wr, 4, addr_INTERP_COEF_WR );
}

void IntxWriter::configArray(
                  RegArray &pAry,
                  unsigned int utap,
                  unsigned int uwr,
                  IBus *pBus )
{
    Spu_INTERP_COEF_TAP tap;
    Spu_INTERP_COEF_WR wr;

    tap.payload = utap;
    wr.payload = uwr;

    //! for each item in the array
    for ( int i = 0; i < pAry.mUsedRow; i++ )
    {
        wr.waddr = i;

        for ( int j = 0; j < pAry.mColumn; j++ )
        {
            tap.tap = (1<<j);

            //! int to ->short
            wr.wdata = *pAry.cell( i, j );

            pBus->writeOp( &tap, 4, addr_INTERP_COEF_TAP );
            pBus->writeOp( &wr, 4, addr_INTERP_COEF_WR );

//            qDebug()<<QString::number( tap.ce, 16)<<QString::number(tap.tap,16);
//            qDebug()<<QString::number( wr.waddr, 16)<<QString::number(wr.wdata,16);

//            qDebug()<<QString::number( tap.payload, 16);
//            qDebug()<<QString::number( wr.payload, 16);
        }
    }
}
}
