#include "chorialign.h"

namespace dso_phy {

CHoriAlign::CHoriAlign()
{}

void CHoriAlign::alignDouble( int adcs[2],
                              int aligns[2],
                              int adcRange )
{
    normalize( adcs, 2, aligns, adcRange );
}
void CHoriAlign::alignQuad( int adcs[4],
                            int aligns[4],
                            int adcRange )
{
    normalize( adcs, 4, aligns, adcRange );
}

//! eg. top = 256
void CHoriAlign::normalize( int vals[],
                            int cnt,
                            int outNorm[],
                            int top )
{
    int i;
    int nom[ cnt ];
    int mid = 0;
    int mmax, mmin;

    //! find min max
    mmax = vals[0];
    mmin = mmax;
    for ( i = 1; i < cnt; i++ )
    {
        mmin = qMin( vals[i], mmin );
        mmax = qMax( vals[i], mmax );
    }

    //! calc the dist
    int dist = mmax - mmin;

    //! mid = 128
    mid = top / 2;
    //! in two sides
    if ( dist > mid )
    {
        for( i = 0; i < cnt; i++ )
        {
            //! < 128
            if ( vals[i] < mid )
            {
                nom[i] = vals[i];
            }
            //! = 256
            else
            {
                nom[i] = vals[i] - top;
            }
        }
    }
    //! only in one side
    else
    {
        for( i = 0; i < cnt; i++ )
        {
            nom[i] = vals[i];
        }
    }

//    //! now align the data
//    for( i = 0; i < cnt; i++ )
//    {
//        nom[i] += cnt - i;
//    }

    //! find max
    mmax = nom[0];
    for( i = 0; i < cnt; i++ )
    {
        mmax = qMax( nom[i], mmax );
    }

    //! compensate
    for ( i = 0; i < cnt; i++ )
    {
        outNorm[i] = mmax - nom[i];
    }
}

}

