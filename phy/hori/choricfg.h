#ifndef CHORICFG_H
#define CHORICFG_H

#include <QtCore>

#include "../../include/dsostd.h"

#include "../../include/dsoarith.h"

#include "../../baseclass/saflow/saflow.h"
#include "./flowview.h"
namespace dso_phy {

//! sa meta
//! timescale
//!         ch: sa, depth
//!         la: sa, depth
class CHoriCfg
{
public:
    qlonglong mTimeScale;    //! base: -12
    qlonglong mEh, mEl;

    CFlowAttr mCHFlow;       //! ch
    CFlowAttr mLaFlow;       //! la

public:
    //! left : neg
    void gettoffsetRange( qlonglong &tMin,
                          qlonglong &tMax );
};

//! horiconfig table
//! a table has many sa items
//! a table is a xml file
class CHoriGroup
{
public:
    CHoriGroup();
    ~CHoriGroup();
public:
    bool load( const QString &fileName );
    CHoriCfg *findCfg( qlonglong timeScale );

private:
    void readCfg( QXmlStreamReader &xml,
                  CHoriCfg *pCfg );
    void readScale( QXmlStreamReader &xml,
                    CHoriCfg *pCfg );

    void readCH( QXmlStreamReader &xml,
                 CHoriCfg *pCfg );

    void readLa( QXmlStreamReader &xml,
                 CHoriCfg *pCfg );

    void readExtract( QXmlStreamReader &xml,
                      CHoriCfg *pCfg );

    qlonglong nodell( QXmlStreamReader &xml );

protected:
    QList<CHoriCfg*> mCfgs;
};

class CHoriPage : public CHoriGroup
{
public:
    enum HoriMode
    {
        clk_2_5G,   // sample=2.5G
        clk_5G,     // sample=5G
        clk_5G_100M,// sample=5G ,bw = 100M
        clk_10G,    // sample=10G
        clk_20G,
    };

public:
    CHoriPage();
    CHoriPage( HoriMode mode, qlonglong depth );

public:
    void setPage( HoriMode mode, qlonglong depth );
    void setPath( const QString &path );
    QString &getPath();

    bool operator== ( CHoriPage &page );

private:
    HoriMode mMode;
    qlonglong mDepth;
    QString mPath;
};

//! manager
class CHoriCfgMgr
{
public:
    CHoriCfgMgr();
    ~CHoriCfgMgr();
public:
    //! register (mode,depth) :: sa table path
    void regDir( CHoriPage::HoriMode mode,
                 qlonglong depth,
                 const QString &path );

    //! find with (mode,depth,timescale ) ::
    CHoriCfg * findCfg( CHoriPage::HoriMode mode,
                        qlonglong depth,
                        qlonglong timeScale );

    //! view on
    void viewOn( qlonglong viewScale,
                 qlonglong viewOffse,
                 CFlowAttr &cfg,
                 CFlowView &view );

    void viewOn( CFlowAttr &cfg,
                 int dstLen,
                 CFlowView &view );

    //! compond api
    CHoriCfg * view( CHoriPage::HoriMode mode,
                 qlonglong dep,
                 qlonglong saScale,
                 qlonglong saOffset,
                 qlonglong viewScale,
                 qlonglong viewOffset,
                 CFlowView &viewCH,
                 CFlowView &viewLa );

    CHoriCfg * selectSa( CHoriPage::HoriMode mode,
                  qlonglong dep,
                  qlonglong saScale,
                  qlonglong saOffset,
                  CSaFlow &chFlow,
                  CSaFlow &laFlow );

private:
    bool isHoriPageExist( CHoriPage &key );
    CHoriPage * findPage( CHoriPage &page );

private:
    QList<CHoriPage*> mPages;
};

}

#endif // CHORICFG_H
