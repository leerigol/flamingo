#include "flowview.h"
#include "../../include/dsodbg.h"
namespace dso_phy
{

CFlowView::CFlowView()
{
    mbFailSlove = false;

    mTLeft = 0;
    mTInc = 0;
    mWidth = trace_width;
    mFinePos = 0;

    mTransferCap = 1000000;     //! 1M dots
    mTransferAlign = 1000;
    mCompL2Len = 1000;          //! max L2 length

    mComp = 1;

    mAcquire = Acquire_Normal;
    mAcqChCnt = 4;

    mLinearIntxOn = false;
    mLinearIntx = 1;
}

//! view
void CFlowView::set( qlonglong toffset,
                     qlonglong tleft,
                     qlonglong tinc,
                     qlonglong width )
{
    mTOffset = toffset;
    mTLeft = tleft;
    mTInc = tinc;
    mWidth = width;
}

void CFlowView::setAcq( AcquireMode acq,
                        int chCnt,
                        quint32 finePos )
{
    mAcquire = acq;
    mAcqChCnt = chCnt;
    mFinePos = finePos;
}

void CFlowView::cfgTransfer( qlonglong transCap,
               qlonglong transAlign,
               qlonglong comp2Len )
{
    mTransferCap = transCap;
    mTransferAlign = transAlign;
    mCompL2Len = comp2Len;
}

void CFlowView::cfgAlign( qlonglong transAlign )
{
    mTransferAlign = transAlign;
}

CSaFlow& CFlowView::getFlow()
{
    return mViewFlow;
}

//! 0 -- no error
int CFlowView::viewOn(CSaFlow &saFlow , bool isRoll)
{
    viewRange( saFlow, isRoll);

    viewMemLen( saFlow);

    viewSlove( saFlow, isRoll);

    viewGen( saFlow );

    viewFix( saFlow );

    return 0;
}

int CFlowView::viewOnFine(CFlowView *pCoarseView, CSaFlow &saFlow,
                          qlonglong normScale, qlonglong viewScale, qlonglong viewOffset)
{
    set( viewOffset,
         viewOffset - viewScale * hori_div/2,
         viewScale/adc_hdiv_dots );
    viewRange( saFlow );
    viewMemLen( saFlow );
    mIntx = pCoarseView->mIntx;
    mComp = pCoarseView->mComp;
    mCompL1 = pCoarseView->mCompL1;
    mCompL2 = pCoarseView->mCompL2;
    mCompL2Len = pCoarseView->mCompL2Len;
    mLinearIntxOn = true;
    mLinearIntx = dsoFract(normScale,viewScale);
    //! transfer len
    mTransferLen = pCoarseView->mTransferLen;
    //! 微调时基下时间间隔比上一个粗调时基的时间间隔小
    mTransferDotTime = pCoarseView->mTransferDotTime * mLinearIntx.N() / mLinearIntx.M();
    mTransferT0 = mScrTLeft.toLonglong();
    viewRawLen(saFlow);
    viewMemRange(saFlow, false);
    viewMemFine0(saFlow);
    viewMemFine(saFlow);
    viewGen(saFlow);
    viewFix(saFlow);

    return ERR_NONE;
}

int CFlowView::viewOnPlayFine(CFlowView *pCoarseView, CSaFlow &saFlow,
                              qlonglong normScale, qlonglong viewScale, qlonglong viewOffset)
{
    set( viewOffset,
         viewOffset - viewScale * hori_div/2,
         viewScale/adc_hdiv_dots );
    viewRange( saFlow );
    viewMemLen( saFlow );
    mIntx = pCoarseView->mIntx;
    mComp = pCoarseView->mComp;
    mCompL1 = pCoarseView->mCompL1;
    mCompL2 = pCoarseView->mCompL2;
    mCompL2Len = pCoarseView->mCompL2Len;
    mLinearIntxOn = true;
    mLinearIntx = dsoFract(normScale,viewScale);
//    transfer len
//    mTransferLen = pCoarseView->mTransferLen;
    //! 微调时基下时间间隔比上一个粗调时基的时间间隔小
    mTransferDotTime = pCoarseView->mTransferDotTime * mLinearIntx.N() / mLinearIntx.M();
    mTransferLen = mCompL2 * mScrLen.ceilInt();
    mTransferT0 = mScrTLeft.toLonglong();
    viewRawLen(saFlow);
    viewMemRange(saFlow, false);
    viewMemFine0(saFlow);
    viewMemFine(saFlow);
    viewGen(saFlow);

    mFixScrLen = mScrLen.toInt();
    mFixCompL2 = mCompL2;
    mMLen = mMLen + mFixCompL2 * mCompL1 / mIntx / mLinearIntx.toInt();
    return ERR_NONE;
}

int CFlowView::viewOnLa(CFlowView *pCHView)
{
    mIntx = pCHView->mIntx * pCHView->mLinearIntx.mM;
    mCompL1 = pCHView->mCompL1 * pCHView->mLinearIntx.mN;
    mLinearIntx = dsoFract(1,1);
    mLinearIntxOn = false;
    return 0;
}

//! by full length
int CFlowView::viewOn( CSaFlow &saFlow,
                       int transLength )
{
    //! view mem = full length
    mViewMemLenCeil = saFlow.mDepth.ceilLonglong();

    //! slove
    mTransferLen = transLength;

    //! mem * intx /comp = trans
    mIntx = mTransferLen * saFlow.mDepth.N();
    mComp = saFlow.mDepth.M();

    norm_m_n( mComp, mIntx );

    mTransferDotTime = (saFlow.mDotTime * mComp / mIntx).ceilLonglong();

    //! gen flow
    mViewFlow.setT0( saFlow.mDotTime.M(),
                     saFlow.mDotTime.N() );
    mViewFlow.setLength( mTransferLen );
    mViewFlow.setDotTime( mTransferDotTime );

    mViewFlow.setSaInfo( saFlow.mSaScale,
                         saFlow.mSaOffset );

    return 0;
}

int CFlowView::viewOn( CSaFlow &saFlow,
            qlonglong viewScale,
            qlonglong viewOffset )
{
    set( viewOffset,
         viewOffset - viewScale * hori_div/2,
         viewScale/adc_hdiv_dots );
    return viewOn( saFlow );
}

void CFlowView::setCorseComp(CFlowView *corseFlowView)
{
    mIntx = corseFlowView->mIntx;
    mComp = corseFlowView->mComp;
    mCompL1 = corseFlowView->mCompL1;
    mCompL2 = corseFlowView->mCompL2;
}

void CFlowView::viewSlove(CSaFlow &saFlow , bool isRoll)
{
    //! total
//    viewIntxComp( saFlow );

    //! trans
    //! intx comp1/2 =
    transferIntxComp( saFlow );

    viewRawLen( saFlow );

    viewMemRange( saFlow, isRoll);

    viewMemFine0( saFlow );

    viewMemFine( saFlow );
}

void CFlowView::viewGen( CSaFlow &saFlow )
{
    //! gen view flow
    mViewFlow.setT0( mTransferT0, 1 );
    mViewFlow.setLength( mTransferLen );

    mViewFlow.setDotTime( mTransferDotTime );

    mViewFlow.setSaInfo( saFlow.mSaScale,
                         saFlow.mSaOffset );
}

void CFlowView::viewFix( CSaFlow &/*saFlow*/ )
{
    mFixScrLen = mScrLen.toInt();
    if( mFixScrLen > 0)
    {
        mFixCompL2 = mTransferLen / mFixScrLen;
    }
    else
    {
        mFixScrLen = 1;
        mFixCompL2 = mTransferLen / mFixScrLen;
        Q_ASSERT( mFixScrLen >= 0);
    }
}

void CFlowView::debugOut()
{
    LOG_ON_FILE()<<"inc"<<mTInc<<"left"<<mTLeft;
    LOG_ON_FILE()<<",ViewMLen(m/n),"<<mViewMemLen.ceilInt()<<",CeilViewMLen,"<<mViewMemLenCeil<<",RawLen(m/n),"<<mViewRawLen;
    LOG_ON_FILE()<<",span len"<<mViewMemSpan.ceilInt();
    LOG_ON_FILE()<<",MSta,"<<mMStart<<",MLen,"<<mMLen<<",fine252"<<mFinePos;
    LOG_ON_FILE()<<",Intx,"<<mIntx<<",CompL1,"<<mCompL1<<",fineX"<<mIntxRema;
    LOG_ON_FILE()<<",transLen,"<<mTransferLen<<",CompL2,"<<mCompL2<<mFixCompL2;
    LOG_ON_FILE()<<",VSta,"<<mScrLeft.toInt();
}

/** @brief:screen left/right,Yt模式下表示屏幕上有波形的区域
  * @author:add_by_zx
  * @date:2017-12-23
  */
void CFlowView::viewRange(CSaFlow &saFlow , bool isRoll)
{
    dsoFract tEnd = saFlow.getTEnd();

    //! left
    dsoFract scrLeft;

    dsoFract realSaT0;
    if( isRoll )
    {
        //! 滚动模式下采样起点由前面往后面倒
        realSaT0 = saFlow.getTEnd() - saFlow.mRollSaSum * saFlow.getDotTime();
        LOG_DBG()<<"saFlow.mT0"<<saFlow.mT0.toInt();
    }
    else
    {
        realSaT0 = saFlow.mT0;
    }

    scrLeft = (realSaT0 - mTLeft) / mTInc;

    if( scrLeft.ceilInt() < 0)
    {
        LOG_DBG()<<scrLeft.ceilInt();
    }
    //! right
    dsoFract tRight;
    dsoFract scrCol;
    tRight = mTLeft + mTInc * mWidth;

    //! columns
    scrCol = ( tEnd - mTLeft) / mTInc;

    //!    [***********]
    //! (
    if ( dsoFract(mTLeft) < realSaT0 )      //! over left
    {
        mScrLeft = scrLeft;
        mScrTLeft = realSaT0;
    }
    //!    [***********]
    //!                (
    else if ( dsoFract(mTLeft) >= tEnd )      //! over right
    {
        mScrLeft = -1;
        mScrTLeft = realSaT0;               //! rst
    }
    //!    [***********]
    //!            (
    else
    {
        mScrLeft = 0;
        mScrTLeft = mTLeft;
    }

    //!    [***********]
    //!    )
    if ( tRight <= realSaT0 )      //! over left
    {
        mScrRight = -1;
        mScrTLeft = realSaT0;     //! rstT0
    }
    //!    [***********]
    //!                  )
    else if ( tRight >= tEnd )      //! over right
    {
        mScrRight = scrCol;
    }
    //!    [***********]
    //!       )
    else
    { mScrRight = mWidth; }

    //! scr length
    if ( mScrLeft == -1 || mScrRight == -1 )
    {
        mScrLeft = 0;
        mScrRight = 0;
        mScrLen = 0;
    }
    else
    {
        //! add by zx
        //! 2499999999987/5000000000 - 2500000000013/5000000000 =
        //! 6399663990518016/126953125
//        if( mScrRight.ceilInt()  -  mScrLen.toInt() == 1)
//        {
//            mScrRight = mScrRight.ceilInt();
//            mScrLen = mScrLen.toInt();
//        }

        mScrLen = (mScrRight - mScrLeft );

        if ( mScrLen <= 0 )
        {
            debugOut();
            Q_ASSERT(false);
        }
    }

    //! trigpos 为正表示触发点在屏幕右侧
    mTrigPos = - mTLeft / mTInc;
}

//! view mem len by screen
//! may be fract
void CFlowView::viewMemLen(CSaFlow &saFlow)
{
    //! view len
    //! width * tInc / saDotTime
    mViewMemLen = (mScrLen * mTInc / saFlow.mDotTime);

    //! ceil len
    mViewMemLenCeil = mViewMemLen.ceilLonglong();

    //! spanLen
    mViewMemSpan = mWidth * mTInc / saFlow.mDotTime;
}

void CFlowView::viewIntxComp( CSaFlow &saFlow  )
{
    //! comp
    //! dotTime * intx / comp = tinc
    //! m/n*intx/comp = tinc
    //! intx/comp = tinc*n/m
    mComp = saFlow.mDotTime.M();
    mIntx = mTInc*saFlow.mDotTime.N();

    norm_m_n( mComp, mIntx );

    Q_ASSERT( mComp > 0  && mIntx > 0 );
}

//! view raw len : pixel len
//! must be integer
void CFlowView::viewRawLen( CSaFlow &/*saFlow*/ )
{
    if( mLinearIntxOn )
    {
        mViewRawLen = (mViewMemLen * mLinearIntx * mIntx/mCompL1/mCompL2).ceilLonglong();
    }
    else
    {
        mViewRawLen = (mViewMemLen * mIntx/mCompL1/mCompL2).ceilLonglong();
    }
}

//! mem start / length
void CFlowView::viewMemRange( CSaFlow &saFlow, bool isRoll)
{
    //! zx_roll
    dsoFract realSaT0;
    if(isRoll)
    {
        realSaT0 = saFlow.getTEnd() - saFlow.mRollSaSum * saFlow.getDotTime();
        LOG_DBG()<<"saFlow.mT0"<<saFlow.mT0.toInt();
    }
    else
    {
        realSaT0 = saFlow.mT0;
    }

    if( saFlow.mDepth < mViewMemLenCeil )
    {
        int depth = saFlow.mDepth.ceilLonglong();
        qDebug()<<__FILE__<<__LINE__<<"depth"<<depth;
        qDebug()<<"viewLen"<<mViewMemLenCeil;
    }

    //! must be integer
    Q_ASSERT( saFlow.mDepth >= mViewMemLenCeil );

    Q_ASSERT( saFlow.mDotTime > 0 );
    if ( ( mScrTLeft >= realSaT0 ) )
    {}
    else
    { Q_ASSERT(false); }

    dsoFract fStart = (mScrTLeft - realSaT0) / saFlow.mDotTime;
    mMStart = fStart.toLonglong();
    mMLen = mViewMemLenCeil;

//    LOG_DBG()<<mMStart<<fStart.ceilLonglong()<<saFlow.mT0.M()<<saFlow.mT0.N();
}

void CFlowView::viewMemFine0( CSaFlow &saFlow )
{
    //! finePos / 252 = fineTime/dotTime
    //! findTime = finePos * dotTime / 252
    dsoFract fineTime;
    fineTime = saFlow.mDotTime * mFinePos / 252;        //! ps

    dsoFract totalRema;

    //! time / (dotTime/intx)
    totalRema = fineTime * mIntx / saFlow.mDotTime;

    mIntxRema0 = (quint32)totalRema.toInt();
}

void CFlowView::viewMemFine( CSaFlow &saFlow )
{
    //! rema
    qlonglong remaTime;
    //! screen shift
    if ( mScrLeft > 0  )
    {
        remaTime = 0;
    }
    //! saFlow.getTEnd() / saFlow.mDotTime
    //!  t % tInc
    else
    {
        Q_ASSERT( saFlow.mDotTime.mN == 1);
        remaTime = mTOffset % saFlow.mDotTime.mM;         //! ps

        //! to positive
        //! as % near to 0
        if ( remaTime < 0 )
        { remaTime = remaTime + saFlow.mDotTime.mM; }
        else
        { }
    }

    //! finePos / 252 = fineTime/dotTime
    //! fineTime = finePos * dotTime / 252

    Q_ASSERT( mIntx > 0 );

    dsoFract fineTime;
    qint32 shiftOffset;
    shiftOffset = adc_hdiv_dots*hori_div/2%mIntx;
    fineTime = shiftOffset * saFlow.mDotTime / mIntx;

    dsoFract totalRema = fineTime + remaTime;           //! ps

    totalRema = totalRema * mIntx / saFlow.mDotTime;

    Q_ASSERT( totalRema >= 0 );

    //! now for mem shift
    qlonglong mShift;
    mShift = totalRema.toLonglong();
    if ( mShift >= mIntx )
    {
        totalRema = totalRema - mIntx;
    }
    else
    {
    }

    mIntxRema = totalRema.toInt();
}

//! -- trans intx comp
void CFlowView::transferIntxComp( CSaFlow &saFlow )
{
    transferCompL1( saFlow );
    transferCompL2( saFlow );

    mLinearIntxOn = false;
    mLinearIntx = dsoFract(100, 100);
//    //! 内插倍数在1-2.5倍之间的时候,需要改为线性内插
//    mComp = mCompL1 * mCompL2;
////    qDebug()<<"/********linearIntx test**********/";
////    qDebug()<<"mIntx"<<mIntx<<"mComp"<<mComp<<
////              "mComp1"<<mCompL1<<"mComp2"<<mCompL2;
}

//! intx =
//! compL1 =
void CFlowView::transferCompL1( CSaFlow &/*saFlow*/ )
{
    qlonglong lcmLen;

    //! aligned length
    lcmLen = lcm_ll( mViewMemSpan.M(), mViewMemSpan.N() * mTransferAlign );

    if( mViewMemSpan.M() > 0 && mViewMemSpan.N() > 0 )
    {}
    else
    { Q_ASSERT(false); }

    //! norm lcmlen
    //! lcm( m/n, align ) may != lcm( m, n*align )
    //! lcm( m, n*align ) = a * m = b * n * align
    //! a/n * m = b * align
    //! a * m / n = b * align

    qlonglong a = lcmLen / mViewMemSpan.M();
    if ( a % mViewMemSpan.N() == 0 )
    { lcmLen = lcmLen / mViewMemSpan.N(); }

    mIntx = lcmLen * mViewMemSpan.N() / mViewMemSpan.M();

    //! comp
    mCompL1 = squash( lcmLen, mTransferCap, mTransferAlign );
    LOG_ON_FILE()<<mIntx<<lcmLen<<mCompL1<<mTransferCap<<mTransferAlign;
    //! workaround for 2
    if ( mIntx == 2 )
    { mIntx *= 2; mCompL1 *= 2; }
}

//! trans_len =
//! compL2 =
void CFlowView::transferCompL2( CSaFlow &saFlow )
{
    Q_ASSERT( mCompL1 > 0 );

    //! span
    qlonglong rawTransferLen = mViewMemSpan.M() * mIntx / (mViewMemSpan.N() * mCompL1);

    Q_UNUSED(saFlow);
    mTransferLen = mViewMemLen.M() * mIntx / (mViewMemLen.N() * mCompL1);
    mTransferDotTime = ( saFlow.mDotTime * mCompL1 / mIntx ).ceilLonglong();
    mTransferT0 = mScrTLeft.toLonglong();

    //! comp2
    Q_ASSERT( mCompL2Len > 0 );
    if( rawTransferLen % mCompL2Len == 0 )
    {}
    else
    {

        Q_ASSERT(false);
    }
    mCompL2 = rawTransferLen / mCompL2Len;
}

qlonglong CFlowView::squash( qlonglong rawLen,
                           qlonglong cap,
                           qlonglong align )
{
    if ( rawLen <= cap ) return 1;

    Q_ASSERT( cap > 0  );
    Q_ASSERT( align > 0 );

    //! iter range
    qlonglong from = (rawLen + cap-1)/ cap;
    qlonglong end = rawLen / align;

    qlonglong iter;

    for ( qlonglong i = from; i <= end; i++ )
    {
        iter = rawLen / i;

        if ( (iter % align) == 0 )
        { return i; }
    }

    qWarning()<<"!!!Fail squash";
    return (from + 1);
}

}
