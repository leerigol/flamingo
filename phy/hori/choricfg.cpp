
#include "hori_base.h"
#include "choricfg.h"
#include "../../include/dsodbg.h"

namespace dso_phy {

//! [1000s, ]
//! \todo 1s~10*scale
void CHoriCfg::gettoffsetRange( qlonglong &tMin,
                                qlonglong &tMax )
{
    //! \todo depth - 1?
    tMin = (-mCHFlow.mDepth * mCHFlow.mDotTime / 2).ceilLonglong();

    tMax = 1000*_hori_s;
}

//! -- hori group
CHoriGroup::CHoriGroup()
{
    mCfgs.clear();
}

CHoriGroup::~CHoriGroup()
{
    CHoriCfg* pCfg;

    foreach( pCfg, mCfgs )
    {
        Q_ASSERT( NULL != pCfg );
        delete pCfg;
    }
}

bool CHoriGroup::load( const QString &fileName )
{
    //! has loaded
    if ( mCfgs.size() > 0 ) return true;

    bool b = QFile::exists(fileName);
    Q_ASSERT( b );

    QFile file( fileName );

    b = file.open( QIODevice::ReadOnly ) ;
    Q_ASSERT( b );

    QXmlStreamReader xml( &file );
    CHoriCfg *pCfg;

    if (xml.readNextStartElement())
    { }
    else
    { Q_ASSERT(false); }

    b = xml.isStartElement() && xml.name()=="hori";
    Q_ASSERT( b );

    while (xml.readNextStartElement())
    {
        if (xml.name() == "cfg")
        {
            pCfg = new CHoriCfg();
            Q_ASSERT( NULL != pCfg );

            readCfg( xml, pCfg );

            mCfgs.append( pCfg );
        }
        else
        {
            Q_ASSERT( false );
        }
    }

    file.close();

    return true;
}

CHoriCfg *CHoriGroup::findCfg( qlonglong timeScale )
{
    CHoriCfg* pCfg;

    foreach( pCfg, mCfgs )
    {
        Q_ASSERT( NULL != pCfg );
        if ( pCfg->mTimeScale == timeScale )
        {
            return pCfg;
        }
    }

    return NULL;
}

void CHoriGroup::readCfg( QXmlStreamReader &xml,
                          CHoriCfg *pCfg )
{
    Q_ASSERT( xml.name() == "cfg" );

    while( xml.readNextStartElement() )
    {
        if ( xml.name() == "scale" )
        {
            readScale( xml, pCfg );
        }
        else if ( xml.name() == "ch" )
        {
            readCH( xml, pCfg );
        }
        else if ( xml.name() == "la" )
        {
            readLa( xml, pCfg );
        }
        else if ( xml.name() == "extract" )
        {
            readExtract( xml, pCfg );
        }
        else
        {
            Q_ASSERT( false );
        }
    }
}
void CHoriGroup::readScale( QXmlStreamReader &xml,
                            CHoriCfg *pCfg )
{
    Q_ASSERT( xml.name() == "scale" && NULL != pCfg);

    pCfg->mTimeScale = nodell( xml );
}

void CHoriGroup::readCH( QXmlStreamReader &xml,
                         CHoriCfg *pCfg )
{
    Q_ASSERT( xml.name() == "ch" && NULL != pCfg );

    while( xml.readNextStartElement() )
    {
        if ( xml.name() == "sa" )
        {
//            pCfg->mCHFlow.mSa = nodell( xml );
            xml.readElementText();
        }
        else if ( xml.name() == "dottime" )
        {
            pCfg->mCHFlow.mDotTime = nodell( xml );
        }
        else if ( xml.name() == "depth" )
        {
            pCfg->mCHFlow.mDepth = nodell( xml );
        }
        else
        {
            Q_ASSERT( false );
        }
    }
}

void CHoriGroup::readLa( QXmlStreamReader &xml,
                         CHoriCfg *pCfg )
{
    Q_ASSERT( xml.name() == "la" && NULL != pCfg );

    while( xml.readNextStartElement() )
    {
        if ( xml.name() == "sa" )
        {
//            pCfg->mLaFlow.mSa = nodell( xml );
            xml.readElementText();
        }
        else if ( xml.name() == "dottime" )
        {
            pCfg->mLaFlow.mDotTime = nodell( xml );
        }
        else if ( xml.name() == "depth" )
        {
            pCfg->mLaFlow.mDepth = nodell( xml );
        }
        else
        {
            Q_ASSERT( false );
        }
    }
}

void CHoriGroup::readExtract( QXmlStreamReader &xml,
                  CHoriCfg *pCfg )
{
    Q_ASSERT( xml.name() == "extract" && NULL != pCfg );

    while( xml.readNextStartElement() )
    {
        if ( xml.name() == "eh" )
        {
            pCfg->mEh = nodell( xml );
        }
        else if ( xml.name() == "el" )
        {
            pCfg->mEl = nodell( xml );
        }
        else
        {
            Q_ASSERT( false );
        }
    }
}

qlonglong CHoriGroup::nodell( QXmlStreamReader &xml )
{
    QString str;

    str = xml.readElementText();

    bool bOK;

    double val;
    val = str.toDouble( &bOK );

    Q_ASSERT( bOK );

    return val;
}

CHoriPage::CHoriPage()
{
    mMode = clk_2_5G;
    mDepth = 0;
}
CHoriPage::CHoriPage( HoriMode mode,
                      qlonglong depth )
{
    mMode = mode;
    mDepth = depth;
}

void CHoriPage::setPage( HoriMode mode, qlonglong depth )
{
    mMode = mode;
    mDepth = depth;
}

void CHoriPage::setPath( const QString &path )
{
    mPath = path;
}

QString &CHoriPage::getPath()
{ return mPath; }

bool CHoriPage::operator==( CHoriPage &page )
{
    if ( mMode == page.mMode && mDepth == page.mDepth )
    { return true; }
    else
    { return false; }
}

CHoriCfgMgr::CHoriCfgMgr()
{}
CHoriCfgMgr::~CHoriCfgMgr()
{
    CHoriPage *pPage;

    foreach( pPage, mPages )
    {
        Q_ASSERT( NULL != pPage );

        delete pPage;
    }
}

void CHoriCfgMgr::regDir( CHoriPage::HoriMode mode,
                          qlonglong depth,
                          const QString &path )
{
    CHoriPage page(mode,depth);

    bool b = QFile::exists(path);
    if( !b )
    {
        Q_ASSERT( b );
    }

    if ( isHoriPageExist(page) )
    { return; }

    CHoriPage *pPage;
    pPage = new CHoriPage();
    Q_ASSERT( NULL != pPage );

    pPage->setPage( mode, depth );
    pPage->setPath( path );
    mPages.append( pPage );
}

CHoriCfg * CHoriCfgMgr::findCfg(
                    CHoriPage::HoriMode mode,
                    qlonglong depth,
                    qlonglong timeScale )
{
    //! find the page
    CHoriPage page( mode, depth );
    CHoriPage *pPage = findPage( page );
    if ( NULL == pPage )
    {
        page.setPage(mode,0);
        pPage = findPage( page );

        qDebug()<<"Warnning! Invalid param:mode="<<mode << "depth="<<depth;
        Q_ASSERT( NULL != pPage );
    }

    //! load page
    if ( pPage->load(pPage->getPath()) )
    {}
    else
    { Q_ASSERT(false);}

    //! find the cfg
    return pPage->findCfg( timeScale );
}

//! view config
void CHoriCfgMgr::viewOn( qlonglong viewScale,
                          qlonglong viewOffset,
                          CFlowAttr &cfg,
                          CFlowView &view )
{

    CSaFlow saFlow( cfg, viewScale, viewOffset );

    view.viewOn( saFlow );
}

void CHoriCfgMgr::viewOn( CFlowAttr &cfg,
                         int dstLen,
                         CFlowView &view )
{
    Q_ASSERT( dstLen > 0 );

    CSaFlow saFlow( cfg );

    view.viewOn( saFlow, dstLen );
}

//void CHoriCfgMgr::viewOn(
//             CFlowView &src,
//             int dstLen,
//             CFlowView &view )
//{
//    //! view mem len
//    //! comp
//    if ( src.mViewMemLenM > src.mViewMemLenN * dstLen )
//    {
//        qlonglong m, n;
//        m = src.mViewMemLenM;
//        n = src.mViewMemLenN * dstLen;
//        norm_m_n( m, n );

//        view.mComp = m;

//        //! \todo not intx
//    }
//    //! intx
//    else if ( src.mViewMemLenM < src.mViewMemLenN * dstLen )
//    {
//        view.mComp = 1;
//    }
//    //! equal
//    else
//    {
//        view.mComp = 1;
//    }
//}

CHoriCfg * CHoriCfgMgr::view(
                          CHoriPage::HoriMode mode,
                          qlonglong dep,
                          qlonglong saScale,
                          qlonglong saOffset,

                          qlonglong viewScale,
                          qlonglong viewOffset,

                          CFlowView &viewCH,
                          CFlowView &viewLa )
{
    CHoriCfg *pCfg;

    //! find cfg
    pCfg = findCfg( mode, dep, saScale );
    if ( NULL == pCfg )
    {
        pCfg = findCfg( mode,
                            dep,
                            ll_roof125( saScale) );

        if( pCfg == NULL )
        {
            Q_ASSERT( NULL != pCfg );
        }
    }

    CSaFlow chSa( pCfg->mCHFlow, saScale, saOffset );

    viewCH.set( viewOffset, viewOffset - viewScale * hori_div/2, viewScale/100 );
    viewCH.viewOn( chSa );

    CSaFlow laSa( pCfg->mLaFlow, saScale, saOffset );
    viewLa.set( viewOffset, viewOffset - viewScale * hori_div/2, viewScale/100 );
    viewLa.viewOn( laSa );

    return pCfg;
}

CHoriCfg * CHoriCfgMgr::selectSa( CHoriPage::HoriMode mode,
                          qlonglong dep,
                          qlonglong saScale,
                          qlonglong saOffset,
                          CSaFlow &chFlow,
                          CSaFlow &laFlow )
{
    CHoriCfg *pCfg;

    //! find cfg
    pCfg = findCfg( mode, dep, saScale );
    if ( NULL == pCfg )
    {
        pCfg = findCfg( mode,
                            dep,
                            ll_roof125( saScale) );

        Q_ASSERT( NULL != pCfg );
        return NULL;
    }

    //! gen sa
    //! depth/sa rate
    CSaFlow chSa( pCfg->mCHFlow, saScale, saOffset );
    CSaFlow laSa( pCfg->mLaFlow, saScale, saOffset );

    //! sa time info
    chSa.setSaInfo( saScale, saOffset );
    laSa.setSaInfo( saScale, saOffset );

    chFlow = chSa;
    laFlow = laSa;

    return pCfg;
}

bool CHoriCfgMgr::isHoriPageExist( CHoriPage &key )
{
    CHoriPage *pPage;

    foreach( pPage, mPages )
    {
        Q_ASSERT( NULL != pPage );

        if ( *pPage == key )
        { return true; }
    }

    return false;
}

CHoriPage * CHoriCfgMgr::findPage( CHoriPage &page )
{
    CHoriPage *pPage;

    foreach( pPage, mPages )
    {
        Q_ASSERT( NULL != pPage );

        if ( *pPage == page )
        { return pPage; }
    }

    return NULL;
}
}
