#ifndef REGARRAY_H
#define REGARRAY_H

#include <QtCore>

namespace dso_phy {

class IntxWriter;
class IntxCoeff;

class LoopStack
{
public:
    LoopStack( int size );
    ~LoopStack();
public:
    void push( int dat );
    int pop();

    void shift( int n );

private:
    int *m_pBase;
    int mSize;
    int mIndex;
};


class RegArray
{
public:
    RegArray();
    ~RegArray();
public:
    void init( int row, int col );
    void setCell0( int cell0 );

    void shift();

    void setRange( int useRow );
    void setGener( int rowSpan,
                   int colSpan,
                   int rowMod );

public:
    void save( QTextStream &stream );
    void save( const QString &fileName );

private:
    int *cell(int row, int col );

private:
    int mColSpan;
    int mRowSpan;
    int mRowMod;

    int mUsedRow;

private:
    int mRow, mColumn;
    int *m_pBase;

friend class IntxCoeff;
friend class IntxWriter;
};

class IntxGen
{
public:
    IntxGen();
    ~IntxGen();

public:
    void gen( int x, int cnt );

    void save( const QString &dir, const QString &file );

private:
    int mIntX, mIntXCnt;

    //! calced
    int mLcm;
    int mRowCnt;

    RegArray *m_pArray;

friend class IntxWriter;
friend class IntxCoeff;
};

class IntxCoeff
{
private:
    static QMap<int, int *> _coeffMap;
public:
    static void addMap( int x, int *pCoeff );
    static void fillCoeff( IntxGen *pGen  );
};

}

#endif // REGARRAY_H
