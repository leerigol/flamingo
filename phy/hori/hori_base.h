#ifndef HORI_BASE
#define HORI_BASE

//! base time
#define _hori_ps    (1ll)
#define _hori_ns    (1000*_hori_ps)
#define _hori_us    (1000*_hori_ns)
#define _hori_ms    (1000*_hori_us)

#define _hori_s     (1000*_hori_ms)
#define _hori_ks     (1000*_hori_s)


#endif // HORI_BASE

