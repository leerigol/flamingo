#ifndef INTXWRITER_H
#define INTXWRITER_H

#include "../bus/ibus.h"
#include "regarray.h"

namespace  dso_phy {

class IntxWriter
{
public:
    IntxWriter();

public:
    void write( IntxGen *pGen,
                IBus *pBus,
                unsigned int multAddr,
                unsigned int coefGroup );
private:
    void configArray( RegArray &pAry,
                      unsigned int tap,
                      unsigned int wr,
                      IBus *pBus );
};
}
#endif // INTXWRITER_H
