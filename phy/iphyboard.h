#ifndef IPHYBOARD_H
#define IPHYBOARD_H

#include "iphy.h"

namespace dso_phy {

#define board_reg   unsigned short

class IPhyBoard : public IPhy
{
public:
    IPhyBoard();

public:
    virtual void loadOtp();

public:
    quint16 getCpldVer();
    quint16 getMBVer();

    //! 开关状态控制 - 1: 常开
    void setSwitch( bool b );
    bool getSwitch();

    //! 重新上电启动
    void setRePower();
    //! 断电
    void setShutDown();

    void rstCtp();

    void clrInt();
    void maskInt(int mask);
    board_reg readIntStat();
    board_reg readIntEn();
    board_reg readIntMask();

    void testWrite( board_reg tst );
    board_reg testRead();

private:
    void refresh();

    void push( quint8 addr, board_reg val );
    board_reg pull( quint8 addr );

private:
    quint16 mCpldVer;
    quint16 mMBVer;

    bool mbDirty;
};

}

#endif // IPHYBOARD_H
