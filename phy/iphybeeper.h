#ifndef IPHYBEEPER_H
#define IPHYBEEPER_H

#include "iphy.h"

namespace dso_phy {

class IPhyBeeper : public IPhy
{
public:
    IPhyBeeper();

public:
    void stopBeep();
    void startBeep();

public:
    void tick();
    void shortBeep();
    void intervalBeep();
};

}

#endif // IPHYBEEPER_H
