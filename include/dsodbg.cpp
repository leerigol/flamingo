#include "dsodbg.h"
#include <QTime>
#include <QThread>
#include <QFile>

#include "../service/servdso/sysdso.h"

QString getDebugTime()
{
    static QTime debug_timer;
    static int i = 0;
    static int last = 0;

    if(i == 0)
    {
        debug_timer.start();
        i++;
    }

    int now = debug_timer.elapsed();
    QString result = QString::number(now,10) + "ms " + QString::number(now-last,10) + "ms";
    last = now;

    return result;
}

void errMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    Q_UNUSED(context);
    QByteArray localMsg = msg.toLocal8Bit();
    switch(type)
    {
    case QtFatalMsg:
        fprintf( stderr, "%s",localMsg.constData());
        abort();
        break;
    case QtDebugMsg:
        fprintf( stderr, "Debug:%s (%s:%u, %s) \n", localMsg.constData(), context.file, context.line, context.function);
        break;
    default:
        break;
    }
}

void handler(int sig)
{
    if(sysHasArg("-wait_assert"))
    {
        void *array[10];
        size_t size;

        static bool inited = false;
        if(!inited)
        {
            inited = true;
            QFile file("core_log.txt");
            if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                qDebug()<<__FUNCTION__<<__FILE__<<"no such file";
            }
            else
            {
                file.close();
            }
        }

        // get void*'s for all entries on the stack
        size = backtrace(array, 15);
        qDebug()<<QThread::currentThread();
        // print out all the frames to stderr
        QFile file("core_log.txt");
        if(!file.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            qDebug()<<"open file fail";
            return;
        }
        fprintf(stderr, "Error: signal %d:\n",sig);
        backtrace_symbols_fd(array, size, STDERR_FILENO);
    #if 1
        backtrace_symbols_fd(array, size, file.handle());
    #endif
        exit(0);
    }
}
