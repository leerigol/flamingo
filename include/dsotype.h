#ifndef DSOTYPE
#define DSOTYPE

#include <QtCore>
#include <QColor>
#include <QRect>

//! int max
#ifndef INT_MAX
#define INT_MAX (0X7FFFFFFF)
#endif
#ifndef INT_MIN
#define INT_MIN (-INT_MAX-1)
#endif

//#define INT32_MIN 	 (-2147483647L-1)
//#define INT32_MAX 	 2147483647L
//#define UINT32_MAX       4294967295UL

//#define INT64_MIN 	(-9223372036854775807L-1L)
//#define INT64_MAX 	 9223372036854775807L
//#define UINT64_MAX 	18446744073709551615U

//! float max
#ifndef FLOAT_MAX
#define FLOAT_MAX (9.9e37f)
#endif
#ifndef FLOAT_MIN
#define FLOAT_MIN -FLOAT_MAX
#endif

#include "dsoerr.h"

namespace DsoType{
#define SCREENWID 1000
#define SCREENHEIGHT 480

    /*! \ingroup common
     * @{
     */
    /*! \enum DsoE */
    typedef enum
    {
        E_MIN = -19,
        E_N18,E_N17,E_N16,
        E_N15,E_N14,E_N13,
        E_N12,E_N11,E_N10,
        E_N9,E_N8,E_N7,
        E_N6,E_N5,E_N4,
        E_N3,E_N2,E_N1,
        E_0,
        E_P1,E_P2,E_P3,
        E_P4,E_P5,E_P6,
        E_P7,E_P8,E_P9,
        E_P10,E_P11,E_P12,
        E_P13,E_P14,E_P15,
        E_P16,E_P17,E_P18,

        E_P36 = 36,
        E_MAX,
    }DsoE;

    //! \typedef pointer
    typedef void * pointer;

    class ArbBin
    {
    public:
        int     mBlockLen;  /*!< block length */
        void    *mPtr;     /*!< block pointer */
    private:
        bool deleteTag;    /*! 标记此对象 mPtr指向的内存空间可以通过free释放*/
    public:
        ArbBin(int len, void *p)
        {
            Q_ASSERT( NULL != p );
            Q_ASSERT( len > 0 );

            mPtr = new quint8[len];
            memcpy(mPtr, p, len);
            deleteTag = true;
            mBlockLen = len;
        }

        ArbBin()
        {
            deleteTag = false;
            mPtr      = NULL;
            mBlockLen = 0;
        }

        //! 0--no error
        int attach( int len, void *p )
        {
            Q_ASSERT( len > 0 && p != NULL );

            //! valid resource
            if ( NULL != mPtr  )
            {
                //! free
                if ( mBlockLen != 0)
                {
                    delete [](quint8*)mPtr;
                    mBlockLen = 0;

                    //! allocate new
                    mPtr = new quint8[ len ];
                    if ( NULL == mPtr )
                    { return -1; }

                    //! copy
                    memcpy( mPtr, p, len );
                    mBlockLen = len;

                    return 0;
                }
                else
                { Q_ASSERT(false); return -1; }
            }
            //! no resource
            else
            {
                Q_ASSERT( mBlockLen == 0 );

                mPtr = new quint8[ len ];
                if ( NULL == mPtr )
                { return -1; }

                memcpy( mPtr, p, len );
                mBlockLen = len;

                return 0;
            }
        }

        void free()
        {
            if(deleteTag)
            {
                if(mPtr != NULL)
                {
                    delete [](quint8*)mPtr;
                    mPtr = NULL;
                }
                mBlockLen = 0;
            }
        }
    };

    enum DsoView
    {
        view_yt,
        view_yt_main,
        view_yt_zoom,

        view_xy,
        view_xy_xgnd,
        view_xy_ygnd,

        view_fft_main,
        view_fft_zoom,

        view_ygnd,
        view_ygnd_main,
        view_ygnd_zoom,

        view_tgnd_main,
        view_tgnd_zoom,

        view_h_main,
        view_h_zoom,

        view_lv_gnd_main,
        view_lv_gnd_zoom,
    };

    enum DsoScreenMode
    {
        screen_unk,

        //! yt
        screen_yt_main,
        screen_yt_main_zoom,
        screen_yt_main_zfft,

        //! xy
        screen_xy_full,
        screen_xy_normal,
        screen_xy_fft,

        //! roll
        screen_roll_main,
        screen_roll_main_zoom,
    };

    enum DsoViewFmt
        {
            fmt_unk,
            fmt_bin,
            fmt_hex,
            fmt_int,
            fmt_float,  //! m,M,k...
            fmt_f,      //! .nf

            fmt_ip,     /*!< xxx.xxx.xxx.xxx */
            fmt_asc,    /*!< 0~127 */
            fmt_time,   /*!< xx:xx:xx */
            fmt_date,   /*!< xxxx/xx/xx */

            fmt_width_1=(1*256),    //! width
            fmt_width_2=(2*256),
            fmt_width_3=(3*256),
            fmt_width_4=(4*256),

            fmt_width_5=(5*256),
            fmt_width_6=(6*256),
            fmt_width_7=(7*256),
            fmt_width_8=(8*256),

            fmt_width_9=(9*256),

            fmt_1f = (1*256),       //! precision
            fmt_2f = (2*256),
            fmt_3f = (3*256),
            fmt_4f = (4*256),
            fmt_5f = (5*256),
            fmt_6f = (6*256),

                                    //! trim 0
            fmt_trim_0 = (0),
            fmt_no_trim_0 = (1*65536),
                                        //! keep 0
            fmt_trim_0_2 = (2*65536),   //! 0.0
            fmt_trim_0_3 = (3*65536),   //! 0.00
            fmt_trim_0_4 = (4*65536),   //! 0.000
            fmt_trim_0_5 = (5*65536),   //! 0.0000
            fmt_trim_0_6 = (6*65536),   //! 0.00000
        };
        #define fmt_type( fmt ) ( fmt & 0xff)
        #define fmt_width( fmt ) ( ((fmt)&0xff00)>>8 )
        #define fmt_trim( fmt )  ( ((fmt)&0xff0000)>>16 )

        #define fmt_def     (DsoViewFmt)(fmt_float | fmt_width_4)
        #define dso_view_format( fmt, wid ) (DsoViewFmt)( (fmt) | (wid))
        #define dso_fmt_trim( fmt, wid, trim )   (DsoViewFmt)( (fmt) | (wid) | (trim) )

        //! fmt_f
        #define f_nf( wid ) (DsoViewFmt)( (fmt_f) | (wid))
        #define f_precision( fmt )  ( ((fmt)&0xff00)>>8 )

    enum DsoWorkMode
    {
        work_normal,    /*!< 普通工作模式 */
        work_help,      /*!< 帮助打开 */
        work_demo,      /*!< 演示 */
    };

    enum DsoWorkAim
    {
        aim_normal,
        aim_calibrate,
    };

    /*! }@ */

    /*! \ingroup vertical 垂直设置量
     * @{
      */

    /*! \enum Chan */
    typedef enum
    {
        chan_none,

        //! analog
        //! -- 1
        chan1,
        chan2,
        chan3,
        chan4,

        //! la
        //! la_group
        la_g1 = 1, la_g2, la_g3, la_g4,
        //! -- 5
        d0,d1,d2,d3,d4,d5,d6,d7,
        //! -- 13
        d8,d9,d10,d11,d12,d13,d14,d15,
        all_pattern_src = d15,

        //! -- 21
        acline, ext, ext5,

        d0d7,d8d15,d0d15,
        all_trig_src = d0d7,

        //! ref
        //! -- 27
        r1 = 27,
        r2,r3,r4,r5,r6,r7,r8,r9,r10,
        //! math
        //! -- 37
        m1,m2,m3,m4,

        color_grade,
        //! 软通道
        soft_chan = 64,
        dg1_sync, dg2_sync,
        digi_ch1, digi_ch2, digi_ch3, digi_ch4,
        digi_ch1_l, digi_ch2_l, digi_ch3_l, digi_ch4_l,
        eye_ch1, eye_ch2, eye_ch3, eye_ch4,
        la, ref, ana, ana_la,

        chan_all,

    }Chan;

    enum VertZone
    {
        vert_analog,
        vert_la,
        vert_ref,
        vert_math,
    };

    /*! \enum Impedance
     */
    enum Impedance
    {
        IMP_1M,
        IMP_50,

        IMP_UNK,    //! unknown eg. math channel
    };

    /*! \enum ProbeHead
      */
    enum ProbeHead
    {
        Probe_Single,
        Probe_Diff,
        Probe_Comm,
    };

    enum ProbeType
    {
        Probe_V,
        probe_A,
    };

    enum ProbeModel
    {
        PROBE_BNC = 0,        //! bnc
        PROBE_RP1 = 1,        //! rigol probe

        PROBE_TP1 = 128,      //! tek probe

        PROBE_KP1 = 256,      //! keysight probe

        PROBE_LP1 = 512,      //! lecroy probe

    };

    /*! \enum Unit
      */
    typedef enum
    {
        Unit_none,      //! no unit for la

        Unit_W,
        Unit_A,
        Unit_V,
        Unit_U,

        Unit_s,
        Unit_hz,
        Unit_degree,
        Unit_percent,

        Unit_db,
        Unit_dbm,
        Unit_dbV,

        Unit_VmulS,     //! v*s
        Unit_VdivS,     //! v/s
        Unit_Vrms,      //! rms
        Unit_oum,

        Unit_Div,       //! div
        Unit_SaS,       //! Sa/s
        Unit_Pts,       //! pts
        Unit_Pts_,      //! pts*

        Unit_VA,        //! for UPA_Apparent_P
        Unit_VAR,       //! for UPA_Reactive_P
        Unit_wfm,       //! wfm

        Unit_V2,        //! for Meas Variance
        Unit_W2,        //! for Meas Variance
        Unit_A2,        //! for Meas Variance
        Unit_U2,        //! for Meas Variance

        Unit_WmulS,     //! W*s
        Unit_AmulS,     //! A*s
        Unit_UmulS,     //! U*s

        Unit_WdivS,     //! W/s
        Unit_AdivS,     //! A/s
        Unit_UdivS,     //! U/s

        Unit_End,
    }Unit;

    /*! \enum Coupling
     */
    enum Coupling
    {
        DC,
        AC,
        GND,
        LF, /*!< for trigger */
        HF, /*!< for trigger */
    };

    /*! \enum Bandwidth
     */
    enum Bandwidth
    {
        BW_FULL,        //! no filter on
        BW_OFF,         //! limit by sysbw
        BW_20M,
        BW_25M,
        BW_50M,
        BW_70M,
        BW_100M,

        BW_150M,
        BW_200M,
        BW_250M,
        BW_300M,
        BW_350M,

        BW_500M,
        BW_600M,
        BW_750M,
        BW_1G,
        BW_2G,

        BW_4G,
        BW_5G,
        BW_10G,
        BW_20G,

    };

    enum Filter
    {
        Filter_lp,
        Filter_hp,
        Filter_bp,
        Filter_bt,
    };

    enum ProbeX
    {
        Probe_X001,
        Probe_X002,
        Probe_X005,
        Probe_X01,

        Probe_X02,
        Probe_X05,
        Probe_X1,
        Probe_X2,

        Probe_X5,
        Probe_X10,
        Probe_X20,
        Probe_X50,

        Probe_X100,
        Probe_X200,
        Probe_X500,
        Probe_X1000,
    };

    /*! \enum LaScale
     */
    typedef enum
    {
        Small,
        Medium,
        Large,
    }LaScale;

    enum unGroupLa
    {
        unGroup1 = 129,
        unGroup2,
        unGroup3,
        unGroup4,
    };

    enum chZone
    {
        time_zone,
        freq_zone,
        logic_zone,
    };

    struct vertAttr
    {
        qlonglong yGnd;
        float yInc;
        Unit yUnit;

        //! data DsoWfm
        //! view = screen
        void getView( void *pData );
        //! >= view
        void getTrace( void *pData );
        //! >= view
        void getMemory( void *pData );
    };

    enum UicStatus
    {
        uic_enabled,
        uic_disabled,
        uic_actived,
        uic_downed,
    };

    /*!
     * }@
     */

    /*! \ingroup horizontal horizontal
     * @{
      */

    enum HorizontalZone
    {
        horizontal_time_zone,
        horizontal_freq_zone,
    };

    struct horiAttr
    {
        //! by view
        qlonglong gnd;            //! pt unit
        double inc;
        Unit unit;

        qlonglong tInc;

        HorizontalZone zone;
    };

    /*! \enum AcquireMode
     */
    typedef enum
    {
        Acquire_Normal,
        Acquire_Peak,
        Acquire_Average,
        Acquire_HighResolution,

        Acquire_Envelope,
        Acquire_RMS,
        Acquire_Equ,
    }AcquireMode;

    /*! \enum AcquireInterplate
     */
    typedef enum
    {
        Acquire_Auto,   /*!< ui use only */
        Acquire_Line,
        Acquire_Sinc,
        Acquire_SH,
    }AcquireInterplate;

    /*! \enum AcquireTimemode
     */
    enum AcquireTimemode
    {
        Acquire_YT,
        Acquire_XY,
        Acquire_ROLL,
        Acquire_SCAN,   /*!< inter use only */
    };

    enum AcquireSamplemode
    {
        Acquire_Real,
        Acquire_Equal,
    };

    enum AcquireDepth
    {
        Acquire_Depth_Auto,

        Acquire_Depth_1K,
        Acquire_Depth_10K,
        Acquire_Depth_100K,
        Acquire_Depth_1M,

        Acquire_Depth_10M,
        Acquire_Depth_25M,
        Acquire_Depth_50M,
        Acquire_Depth_100M,

        Acquire_Depth_125M,
        Acquire_Depth_250M,
        Acquire_Depth_500M,
        Acquire_Depth_1G,
    };

    /*! \enum HorizontalViewmode
     */
    typedef enum
    {
        Horizontal_Main,
        Horizontal_Zoom,
    }HorizontalViewmode;

    enum HorizontalView
    {
        horizontal_view_main,
        horizontal_view_zoom,
        horizontal_view_active,
    };

    /*! \enum HorizontalExpandMode
     */
    typedef enum
    {
        Horizontal_Expand_Center,
        Horizontal_Expand_LB,
        Horizontal_Expand_RB,
        Horizontal_Expand_Trig,
        Horizontal_Expand_User,
    }HorizontalExpandMode;

    enum HorizontalXYType
    {
        XY_CH12,
        XY_CH13,
        XY_CH14,

        XY_CH23,
        XY_CH24,
        XY_CH34,
    };

    enum LaNumType
    {
        LA_BIN_Val,
        LA_HEX_Val,
    };

    /*! }@ */

    /*! \ingroup  control conrol
     * @{
    */
    /*! \enum ControlAction
     */
    typedef enum
    {
        Control_Stop,
        Control_Run,
        Control_Single,
        Control_StopAtEnd,
    }ControlAction;

    /*! \enum ControlStatus */
    typedef enum
    {
        Control_Stoped,
        Control_Runing,

        Control_autoing,
        Control_waiting,
        Control_td,
        Control_Fpga_Force_Stopped,

        Control_no_upDate,
    }ControlStatus;
    /*! }@ */

    /*! \ingroup  trigger trigger
     * @{
    */

    /*! \enum TriggerMode.The order must be equal with trigger type */
    typedef enum
    {
        Trigger_Edge,
        Trigger_Pulse,
        Trigger_Slope,
        Trigger_Video,
        Trigger_Pattern,
        Trigger_Duration,//5

        Trigger_Timeout,
        Trigger_Runt,
        Trigger_Over,   //! 超幅
        Trigger_Window,
        Trigger_Delay,
        Trigger_Setup,
        Trigger_NEdge,//12

        Trigger_RS232,
        Trigger_I2C,
        Trigger_SPI,//15

        Trigger_CAN,
        Trigger_CANFD,
        Trigger_FlexRay,
        Trigger_LIN,
        Trigger_I2S,
        Trigger_SBUS,//21

        Trigger_1553,
        Trigger_429,
        Trigger_SENT,//24

        Trigger_Tmo,
        Trigger_Logic,//26

        Trigger_SH,
        Trigger_AB,
        Trigger_Area,//29

        Trigger_All
    }TriggerMode;

    typedef enum
    {
        SEARCH_TYPE_EDGE,
        SEARCH_TYPE_PULSE,
        SEARCH_TYPE_RUNT,
        SEARCH_TYPE_SLOPE,
        SEARCH_TYPE_RS232,
        SEARCH_TYPE_IIC,
        SEARCH_TYPE_SPI,
        SEARCH_PATTREN,

    }enumSearchType;

    /*! \enum TriggerSweep */
    typedef enum
    {
        Trigger_Sweep_Auto,
        Trigger_Sweep_Normal,
        Trigger_Sweep_Single,
        Trigger_Sweep_Free,
    }TriggerSweep;

    /*! \enum TriggerHoldMode */
    typedef enum
    {
        Trigger_Hold_Random,
        Trigger_Hold_Fixed,
        Trigger_Hold_Auto,
    }TriggerHoldMode;

    /*! \enum TriggerHoldObj */
    enum TriggerHoldObj
    {
        Trigger_Hold_Time,
        Trigger_Hold_Event,
    };

    enum TriggerLevelMode
    {
        Trigger_level_abs,
        Trigger_level_ref,
    };

    enum EdgeSlope
    {
        Trigger_Edge_Rising,
        Trigger_Edge_Falling,
        Trigger_Edge_Alternating,
        Trigger_Edge_Any,
    };

    enum EMoreThan
    {
        Trigger_When_None,
        Trigger_When_Morethan,
        Trigger_When_Lessthan,
        Trigger_When_MoreLess,
        Trigger_When_UnMoreLess,
    };

    enum TriggerPulsePolarity
    {
        Trigger_pulse_positive,
        Trigger_pulse_negative,
        Trigger_pulse_any,
    };


    enum WindowEvent
    {
        Trigger_window_enter,
        Trigger_window_exit,
        Trigger_window_in_width,
        Trigger_window_out_width,
    };

    enum SHEvent
    {
        Trigger_SH_setup,
        Trigger_SH_hold,
        Trigger_SH_setup_hold,
    };

    enum OverEvent
    {
        Trigger_over_enter,
        Trigger_over_exit,
        Trigger_over_time,
    };

    enum TriggerPattern
    {
        Trigger_pat_h,
        Trigger_pat_l,
        Trigger_pat_x,
        Trigger_pat_rise,
        Trigger_pat_fall,
    };

    enum TriggerLogicOperator
    {
        Trigger_logic_and,
        Trigger_logic_or,
        Trigger_logic_nand, //! not and
        Trigger_logic_nor,  //! not or
    };

    enum Trigger_Level_ID
    {
        Trigger_Level_High,
        Trigger_Level_Low,
        Trigger_Level_Double
    };

    //! video
    enum Trigger_Video_Sync
    {
        trig_video_any_line,
        trig_video_x_line,
        trig_video_odd,
        trig_video_even,
    };
    enum Trigger_Video_Format
    {
        Video_Stardard_NTSC,
        Video_Stardard_PAL,
        Video_Stardard_480P,
        Video_Stardard_576P,
    };

    enum Trigger_Video_level
    {
        trig_video_bi,
        trig_video_tri,
    };

    //! number compare
    enum Trigger_value_cmp
    {
        cmp_eq,
        cmp_neq,
        cmp_gt,
        cmp_lt,
        cmp_in,  //! in range
        cmp_out, //! out range
    };

    //! rs232
    enum Trigger_RS232_Event
    {
        RS232_When_Start,
        RS232_When_Data,
        RS232_When_Error,
    };

    enum Triger_RS232_Error
    {
        RS232_Err_Check,
        RS232_Err_Stop,
        RS232_Err_All,
    };

    enum
    {
        rs232_when_start,
        rs232_when_error,
        rs232_when_check_error,
        rs232_when_data,
    };

    enum Trigger_RS232_Stop
    {
        STOP_WIDTH_1,
        STOP_WIDTH_2,
        STOP_WIDTH_1_5
    };

    enum Trigger_RS232_Parity
    {
        RS232_Parity_None,
        RS232_Parity_Odd,
        RS232_Parity_Even,
    };

    enum Trigger_RS232_Width
    {
        RS232_Width_5 = 4,
        RS232_Width_6,
        RS232_Width_7,
        RS232_Width_8,
    };

    enum EBaudRate
    {
        BaudRate_50     = 50,
        BaudRate_75     = 75,
        BaudRate_110    = 110,
        BaudRate_134    = 134,
        BaudRate_150    = 150,
        BaudRate_300    = 300,
        BaudRate_600    = 600,
        BaudRate_1000   = 1000,
        BaudRate_1200   = 1200,
        BaudRate_1800   = 1800,

        BaudRate_2000   = 2000,
        BaudRate_2400   = 2400,
        BaudRate_4800   = 4800,
        BaudRate_9600   = 9600,

        BaudRate_10000  = 10000,
        BaudRate_20000  = 20000,

        BaudRate_19200  = 19200,
        BaudRate_33300  = 33200,
        BaudRate_38400  = 48400,
        BaudRate_50000  = 50000,
        BaudRate_57600  = 57600,

        BaudRate_62500  = 62500,
        BaudRate_83300  = 83300,
        BaudRate_100000 = 100000,
        BaudRate_115200 = 115200,
        BaudRate_125000 = 125000,

        BaudRate_230400 = 230400,
        BaudRate_250000 = 250000,
        BaudRate_460800 = 460800,
        BaudRate_500000 = 500000,
        BaudRate_800000 = 800000,

        BaudRate_921600 = 921600,
        BaudRate_1Mbps  = 1000000,
        BaudRate_2Mbps  = 2000000,
        BaudRate_3Mbps  = 3000000,
        BaudRate_4Mbps  = 4000000,

        BaudRate_5Mbps  = 5000000,
        BaudRate_6Mbps  = 6000000,
        BaudRate_7Mbps  = 7000000,
        BaudRate_8Mbps  = 8000000,
        BaudRate_9Mbps  = 9000000,

        BaudRate_10Mbps = 10000000,
        BaudRate_20Mbps = 20000000,
        BaudRate_Is_User   ,
        BaudRate_All
    };

    //! iis
    enum Trigger_IIS_data_cmp
    {
        trig_iis_data_eq = 0,
        trig_iis_data_neq,
        trig_iis_data_mt,
        trig_iis_data_lt,
        trig_iis_data_in,
        trig_iis_data_out,
        trig_iis_data_cross,
    };

    enum Trigger_IIS_Ch
    {        
        trig_iis_lch,
        trig_iis_rch,
        trig_iis_anych,
    };

    enum Trigger_IIS_Spec
    {
        trig_iis_standard,
        trig_iis_left_justified,
        trig_iis_right_justified,
    };

    //! lin
    enum Trigger_Lin_When
    {
        trig_lin_sync,
        trig_lin_id,
        trig_lin_data,
        trig_lin_id_data,

        trig_lin_sleep,
        trig_lin_wakeup,
        trig_lin_err,
    };

    enum Trigger_Lin_Err
    {
        trig_lin_sync_err,
        trig_lin_id_err,
        trig_lin_check_err,
        trig_lin_any_err,
    };

    enum Trigger_Lin_Ver
    {
        trig_lin_ver_1x,
        trig_lin_ver_2x,
        trig_lin_ver_any,
    };

    //! can
    enum Trigger_Can_When
    {
        trig_can_field,
        trig_can_frame,
        trig_can_err,
    };

    enum Trigger_Can_Field
    {
        trig_can_filed_start,
        trig_can_field_arb,
        trig_can_field_con,
        trig_can_field_data,

        trig_can_field_crc,
        trig_can_field_ack,
        trig_can_field_end,
    };

    enum Trigger_Can_Frame
    {
        trig_can_frame_data,
        trig_can_frame_rmt,
        trig_can_frame_err,
        trig_can_frame_overload,
    };

    enum Trigger_Can_Err
    {
        trig_can_err_bitfilled,
        trig_can_err_crc,
        trig_can_err_form,
        trig_can_err_ack,
        trig_can_err_any,
    };

    enum Trigger_Can_Phy
    {
        trig_can_h,
        trig_can_l,
        trig_can_tx_rx,
        trig_can_diff,
    };

    enum Trigger_Can_Spec
    {
        trig_can_norm,      //! 18
        trig_can_extend,    //! 29
    };

    //! flexray
    enum Trigger_Flex_When
    {
        trig_flex_pos,
        trig_flex_frame,
        trig_flex_symbol,
        trig_flex_err,
    };

    enum Trigger_Flex_Pos
    {
        trig_flex_pos_tss_end,
        trig_flex_pos_fss_bss_end,
        trig_flex_pos_fes_end,
        trig_flex_pos_dts_end,
    };

    enum Trigger_Flex_Frame
    {
        trig_flex_frame_invalid,
        trig_flex_frame_sync,
        trig_flex_frame_start,
        trig_flex_frame_all,
    };

    enum Trigger_Flex_Symbol
    {
        trig_flex_symbol_cas_mts,
        trig_flex_symbol_wus,
        trig_flex_symbol_wudop,

    };

    enum Trigger_Flex_Err
    {
        trig_flex_err_head_crc,
        trig_flex_err_tail_crc,
        trig_flex_err_decode,
        trig_flex_err_any,
    };

    enum Trigger_Flex_Baud
    {
        trig_flex_2_5M  = 2500000,
        trig_flex_5M    = 5000000,
        trig_flex_10M   = 10000000,
    };

    enum Trigger_Flex_Phy
    {
        trig_flex_cha,
        trig_flex_chb,
    };

    //! spi
    enum Trigger_Spi_CS
    {
        trig_spi_cs,
        trig_spi_idle,
    };

    //! i2c
    enum Trigger_I2C_When
    {
        trig_i2c_start,
        trig_i2c_end,
        trig_i2c_restart,
        trig_i2c_nack,
        trig_i2c_addr,
        trig_i2c_data,
        trig_i2c_addr_data,
    };

    enum Trigger_I2C_WR
    {
        trig_i2c_write,
        trig_i2c_read,
        trig_i2c_any,
    };

    enum Trigger_I2C_Spec
    {
        trig_i2c_addr7,
        trig_i2c_addr10,
        trig_i2c_addr8,  //!8bit只是 7bit+r/w的叫法
    };

    enum Trigger_I2C_Width
    {
        trig_i2c_Width_1,
        trig_i2c_Width_2,
        trig_i2c_Width_3,
        trig_i2c_Width_4,
        trig_i2c_Width_5,
    };

    //! 1553
    enum Trigger_1553_When
    {
        trig_1553_sync,
        trig_1553_data,
        trig_1553_cmd,
        trig_1553_status,
        trig_1553_err,
    };

    enum Trigger_1553_Sync
    {
        trig_1553_sync_data,
        trig_1553_sync_cmd_staus_sync,
        trig_1553_sync_all,
    };

    enum Trigger_1553_Err
    {
        trig_1553_err_sync,
        trig_1553_err_check,
        trig_1553_err_manchester,
        trig_1553_err_trans_data_interval,
    };

    /*! }@ */

    /*! \ingroup display
     * @{
      */
    enum WfmPalette
    {
        Wfm_color,
        Wfm_grade,
    };

    enum WfmLineType
    {
        Wfm_Line,
        Wfm_Dot,
    };

    /*! }@ */

    /*! \ingroup ref
     * @{
      */

    /*! \enum Refid */
    typedef enum
    {

         REF1,
         REF2,
         REF3,
         REF4,
         REF5,
         REF6,
         REF7,
         REF8,
         REF9,
         REF10,

    }enumRefID;
    typedef struct
    {
        unsigned long year;
        unsigned int month;
        unsigned int day;
        unsigned int hour;
        unsigned int min;
        unsigned int sec;
    }RefSavedTime;

    typedef enum
    {
        REF_DISP_TYPE_VECTOR,
        REF_DISP_TYPE_DOTS

    }enRefDsoType;




    typedef enum{
        Relative,
        Absolute,
    }ThresType;

    typedef enum{
        F_HEX,
        F_DEC,
        F_BIN,
        F_ASC,
        F_LINE,
    }DecodeFormat;
    /*! \enum BitOrder */
    typedef enum
    {
        lsb,
        msb,
    }BitOrder;

    /*! \enum StopWidth */
    typedef enum
    {
        stop1,
        stop1_5,
        stop2,
    }StopWidth;

    typedef enum
    {
        Lin_1X,
        Lin_2X,
        Lin_both,
    }LinProtocol;

    /*! \enum Parity */
    typedef enum
    {
        even,
        odd,
    }Parity;

    /*! }@ */

    /*! \ingroup math math
     * @{
      */
    enum MathOperator
    {
        //! arith
        operator_add,
        operator_sub,
        operator_mul,
        operator_div,

        //! logic
        operator_and,
        operator_or,
        operator_xor,
        operator_not,

        //! fft
        operator_fft,

        //! function
        operator_intg,
        operator_diff,
        operator_root,
        operator_lg,

        operator_ln,
        operator_exp,
        operator_abs,

        operator_lp,
        operator_hp,
        operator_bp,
        operator_bt,

        //! complex
        operator_ax_b,

        //! demod
        operator_trend,
        operator_max,
        operator_min,
        operator_envelope,
    };

    enum MathZone
    {
        math_analog_zone,
        math_logic_zone,
        math_freq_zone,
    };

    enum FftxType
    {
        fft_span_center,
        fft_start_end,
    };

    enum FftScreen
    {
        fft_screen_half,
        fft_screen_full,
    };

    /*! }@*/


    /*! \ingroup utility
     * @{
     */

    enum VertExpand
    {
        vert_expand_gnd,
        vert_expand_center,
    };

    enum SystemLanguage
    {
        language_english = 0,
        language_chinese,
        language_traditional_chinese,

        language_portugal = 3,
        language_german,
        language_borland,
        language_korean,

        language_japan = 7,
        language_french,
        language_russia,

        language_spanish = 10,
        language_thailand,
        language_indonesia,
    };

    /*! \enum Measure Source Type
      */
    enum MeasSrcType
    {
        Single_Src,        //! Single Source Measure
        Delay_Double_Src,  //! Delay Double Source Measure
        Phase_Double_Src,  //! Phase Double Source Measure
    };

    enum MeasType
    {
        Meas_HOR_TYPE = 0,  //! Meas Horizontal Type start
        Meas_Period = 0,
        Meas_Freq,
        Meas_RiseTime,
        Meas_FallTime,
        Meas_PWidth,
        Meas_NWidth,
        Meas_PDuty,
        Meas_NDuty,
        Meas_PPulses,
        Meas_NPulses,
        Meas_PEdges,
        Meas_NEdges,
        Meas_Tvmax,
        Meas_Tvmin,
        Meas_Pslew_rate,
        Meas_Nslew_rate,
        Meas_HOR_TYPE_END,   //! Meas Horizontal Type End

        Meas_DOUBLE_SRC_TYPE = 20,  //! Meas Double Source Type start
        Meas_DelayRR = 20,
        Meas_DelayRF,
        Meas_DelayFR,
        Meas_DelayFF,
        Meas_PhaseRR,
        Meas_PhaseRF,
        Meas_PhaseFR,
        Meas_PhaseFF,
        Meas_DOUBLE_SRC_TYPE_END,   //! Meas Double Source Type End

        Meas_VER_TYPE = 30,   //! Meas Vertical Type start
        Meas_Vmax = 30,
        Meas_Vmin,
        Meas_Vpp,
        Meas_Vtop,
        Meas_Vbase,
        Meas_Vamp,
        Meas_Vupper,
        Meas_Vmid,
        Meas_Vlower,
        Meas_Vavg,

        Meas_Vrms,
        Meas_Vrms_S,
        Meas_Overshoot,
        Meas_Preshoot,
        Meas_Area,
        Meas_Area_S,
        Meas_Variance,
        Meas_VER_TYPE_END,     //! Meas Vertical End

        UPA_Meas_Type = 60,    //! UPA Meas Type start
        UPA_Ref_Freq  = 60,
        UPA_MAX,
        UPA_RMS,
        UPA_AVG,
        UPA_Vrms,
        UPA_Irms,
        UPA_Real_P,
        UPA_Apparent_P,
        UPA_Reactive_P,
        UPA_P_Factor,
        UPA_Phase_Angle,
        UPA_Imp,
        UPA_V_CrestFactor,
        UPA_I_CrestFactor,
        UPA_Meas_END,         //! UPA Meas END
    };

    enum CounterMode
    {
        counter_freq,
        counter_event,
    };

    enum CounterGate
    {
        counter_gate_high,
        counter_gate_low,
    };

    enum CounterEvent
    {
        counter_event_edge,
        counter_event_trig,
    };

    /*! eye measure items
     */
    enum EyeMeasType
    {
        EYE_BEGIN = 0,
        EYE_ONE = 0,
        EYE_ZERO,
        EYE_WIDTH,
        EYE_HEIGHT,
        EYE_AMP,
        EYE_CROSSPER,
        EYE_QFACTOR,
        EYE_END,
    };
    /*! }@ */

    //! record&play
    enum RecAction
    {
        Rec_start,
        Rec_stop,
    };

    enum PlayAction
    {
        Play_start,
        Play_stop,
    };

    enum PlayDirection
    {
        Play_Inc,
        Play_Dec,
    };

    //! mask
    enum MaskActionEvent
    {
        mask_action_on_fail,
        mask_action_on_pass,
    };

    enum MaskActionAction
    {
        mask_action_none,
        mask_action_stop,
    };

    enum MaskRange
    {
        mask_range_screen,
        mask_range_user,
    };

    // supported HDMI resolution
    enum
    {
        HDMI_640_480_60,
        HDMI_640_480_75,
        HDMI_720_480_60,
        HDMI_720_576_50,
        HDMI_800_600_60,
        HDMI_1024_768_60,
        HDMI_1152_864_75,
        HDMI_1280_720_60,
        HDMI_1280_960_60,
        HDMI_1280_1024_60,
        HDMI_1360_768_60,
        HDMI_1920_1080_60
    };

    enum OptType
    {
        OPT_BW1T2,
        OPT_BW1T3,
        OPT_BW1T5,
        OPT_BW2T3,
        OPT_BW2T5,
        OPT_BW3T5,

        OPT_MSO,
        OPT_2RL,
        OPT_5RL,

        OPT_BND,

        OPT_COMP,
        OPT_EMBD,
        OPT_AUTO,
        OPT_FLEX,
        OPT_AUDIO,
        OPT_SENSOR,
        OPT_AERO,
        OPT_ARINC,
        OPT_DG,
        OPT_JITTER,
        OPT_MASK,
        OPT_PWR,
        OPT_DVM,
        OPT_CTR,

        OPT_EDK,
        NumOfOpt,
        OPT_UNKNOWN,
    };

}

//! assist struct
typedef QPair< QString, int > nameCmdPair;

#define array_count( ary )     (int)( sizeof(ary)/sizeof(ary[0]) )
#define foreach_array( index, array )   for( int index = 0; index < array_count(array); index++ )

#define is_attr( val, attr )   (( (val) & (attr) ) == (attr))
#define set_bit( val, bit )     \
                                ( val = val & (~(1<<(bit))) );\
                                ( val |= (1<<(bit)) );

#define unset_bit( val, bit )   ( val = val & (~(1<<(bit))) );

#define set_attr( type, val, attr )   ( val = (type)( (val)|(attr)) );
#define unset_attr( type, val, attr ) ( val = (type)( val&(~(attr))) );

#define set_x_bit( dst, bit, val ) if ( val ) { set_bit(dst,bit); }\
                                else { unset_bit(dst,bit); }

#define get_bit( val, bit )     (((( val >> (bit))) & 0x01 ))

#define pack_uint64( h, l )     (qulonglong)((((qulonglong)(h))<<32)|((qulonglong)(l)))
#define pack_int64( h, l )      (qlonglong)((((qlonglong)(h))<<32)|((qlonglong)(l)))

//! ch1 -- bit0
//! la -- bit4
#define trim_analog_bm( bm  )   ((bm)&0xf)

#endif // DSOTYPE

