#ifndef _DSO_DBG_H_
#define _DSO_DBG_H_

#include <QDebug>
#include <QString>
#include <stdio.h>

//#include "../dbg/debug_new.h"

#ifdef _DEBUG
#define LOG_DBG()    qDebug()<<__FILE__<<__FUNCTION__<<__LINE__
#else
#define LOG_DBG()    QT_NO_QDEBUG_MACRO()
#endif   

#define LOG_ERR()    qDebug()<<__FILE__<<__FUNCTION__<<__LINE__

#define LOG_ON_FILE()     if ( sysHasArg( __FILE__) )  qDebug()<<__FUNCTION__<<__LINE__

#ifdef   _LOG_FPGA
#define  LOG_FPGA_STATUS() qDebug()<<__FILE__<<__LINE__
#else
#define  LOG_FPGA_STATUS() QT_NO_QDEBUG_MACRO()
#endif
//#define _D_NEW

#ifdef _D_NEW
#define R_NEW( item )        (qDebug()<<__FUNCTION__<<__LINE__, new item)
#define R_DELETE     qDebug()<<__FUNCTION__<<__LINE__; delete
#else
#define R_NEW( item )        new item
#define R_DELETE     delete
#endif


#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>


void handler(int sig);

void errMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);

//! log data to /rigol/log/
//! fileName: no path
void dbgLog( const QString &fileName,
             void *pBuf,
             int size );

QString getDebugTime();

#endif
