#ifndef DSO_DATA_TYPE_H
#define DSO_DATA_TYPE_H

#ifdef Q_OS_LINUX
// linux
typedef unsigned char                   u8;
typedef char                            s8;
typedef unsigned short                  u16;
typedef short                           s16;
typedef unsigned long                   u32;
typedef long                            s32;
typedef long long   			s64;
typedef unsigned long long   		u64;
//typedef __int64                         s64;
//typedef unsigned __int64                u64;
typedef float                           f32;
#elif Q_OS_WIN32
#endif


#endif // DSO_DATA_TYPE_H

