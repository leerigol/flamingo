#ifndef DSOCFG
#define DSOCFG

//! basement
//! scale unit
#define uv_unit      1
#define mv_unit      1000*uv_unit
#define v_unit       1000*mv_unit

#define uv(val)   (int)((val)*uv_unit)
#define mv(val)   (int)((val)*mv_unit)
#define vv(val)   (int)((val)*v_unit)

//ps
#define ps_unit      ((long long)1)
#define ns_unit     (1000*ps_unit)
#define us_unit     (1000*ns_unit)
#define ms_unit     (1000*us_unit)
#define s_unit      (1000*ms_unit)

#define time_ps(t)   ((t)*ps_unit)
#define time_ns(t)   ((t)*ns_unit)
#define time_us(t)   ((t)*us_unit)
#define time_ms(t)   ((t)*ms_unit)
#define time_s(t)    ((t)*s_unit)

#define freq_uHz(h)   ((h)*(1ll))
#define freq_mHz(h)   ((h)*freq_uHz(1000))
#define freq_Hz(h)    ((h)*freq_mHz(1000))
#define freq_KHz(h)   ((h)*freq_Hz(1000))
#define freq_MHz(h)   ((h)*freq_KHz(1000))
#define freq_GHz(h)   ((h)*freq_MHz(1000))

#define _1E3_    1000ll
#define _1E6_    (_1E3_*_1E3_)
#define _1E9_    (_1E6_*_1E3_)
#define _1E12_    (_1E9_*_1E3_)
#define _1E15_    (_1E12_*_1E3_)
#define _1E18_    (_1E15_*_1E3_)
#define _1E21_    (_1E18_*_1E3_)

//! color
#define CH1_color   QColor::fromRgb(0xff,0xff,0x00)
#define CH2_color   QColor::fromRgb(0x00,0xff,0xff)
#define CH3_color   QColor::fromRgb(0xff,0x00,0xc0)
#define CH4_color   QColor::fromRgb(0x00,0x00,0xff)

#define Math_color  QColor::fromRgb(0x80,0x00,0xff)

#define Hori_Color  QColor::fromRgb(0xff,0x80,0x00)
#define Trig_Color  QColor::fromRgb(0xff,0x80,0x00)

//! label
#define max_label_length 22

//! screen
#define screen_width    1024
#define screen_height   600

//! zoom scr
#define zoom_mainscr_height 144
#define zoom_zoomscr_height 320

//! adc
#define adc_max         255
#define adc_center      128

//! \todo for high res
#define DsoPoint   unsigned char
#define ADC_DOTs    256
#define ADC_MAX     adc_max

//! trace is no adc range
#define trace_width     (1000)
#define trace_height    (480)

//! wave image
#define wave_width      (1000)
#define wave_height     (480)

#define vertical_center  wave_height/2

#define trace_1M_point  (1000000)

#define trace_vdiv_pixels ((trace_height)/(vert_div))
#define trace_hdiv_pixels ((trace_width)/(hori_div))

#define adcToTrace( adc )  ( ( adc_center - (adc) ) * trace_vdiv_pixels / adc_vdiv_dots + trace_height/2 )
#define adcIncToTrace( adcInc ) ((adcInc)*adc_vdiv_dots / trace_vdiv_pixels)

#define adcToPixel( adc )  ( ( adc_center - (adc) ) * trace_vdiv_pixels / adc_vdiv_dots + trace_height/2 )
#define adcIncToPixel( adcInc ) ((adcInc)*adc_vdiv_dots / trace_vdiv_pixels)

#define pixelToAdc( pix )  ( ( trace_height/2 - (pix) ) * adc_vdiv_dots / trace_vdiv_pixels + adc_center )

//! ch
#define adc_vdiv_dots   25
#define vert_div        8

#define pref_view_div   6

#define scr_vdiv_dots   60

#define ch_volt_base    (1.0e-6f)
#define ch_delay_base   (1e-12f)
#define ch_delay_step   (100)

#define probe_adc_vdiv_dots   61 //! 16bit dac 校准得到

//! hori
#define adc_hdiv_dots   100
#define hori_div        10

#define hori_time_base  (1e-12)
#define scan_time_scale (time_ms( 200 ))
#define sa_unit         (1e-6)
#define sa_base         1000000000000000000ll   //! 1e18

//! math
#define math_diff_window_max 10000
#define math_diff_window_min 5

#define math_threshold_sens_base (0.1f)
#define math_threshold_base  (1e-6f)


//! servfile ch mask
#define  CH1_MASK            0x01
#define  CH2_MASK            0x02
#define  CH3_MASK            0x04
#define  CH4_MASK            0x08
#define  D7_D0_MASK          0x10
#define  D15_D8_MASK         0x20


//! ref
#define ref_volt_base   (1.0e-6f)
#define ref_offset_base (1.0e-6f)

#define ref_src_num      24


#define math_fft_hz_base (1.0e-6f)   //! uhz

#define math_uhz        (1ll)
#define math_mhz        ( 1000*math_uhz )
#define math_hz         ( 1000*math_mhz )
#define math_khz        ( 1000*math_hz )
#define math_Mhz        ( 1000*math_khz )
#define math_Ghz        ( 1000*math_Mhz )

#define math_v_base     (1.0e-9f)

#define math_uv         (1000ll)
#define math_mv         (1000 * math_uv )
#define math_v          (1000 * math_mv )
#define math_kv         (1000 * math_v)
#define math_Mv         (1000 * math_kv)
#define math_Gv         (1000 * math_Mv)

#define math_view_offset_max    (8*25)
#define math_view_offset_min    (-8*25)

//! eye
#define eye_hz_base    (1ll)

#define eye_hz         (1ll)
#define eye_khz        (1000*eye_hz)
#define eye_Mhz        (1000*eye_khz)

//! cursor
#define cursor_v_handle_width     10
#define cursor_v_handle_height     5

#define cursor_h_handle_width     5
#define cursor_h_handle_height    10


/*SCPI*/
#define SCPI_USB_PID 0x0514
#define SCPI_USB_VID 0x1AB1

//not open functions for TR5. by hxh 2017.9.25
#define FLAMINGO_TR5  1
#endif // DSOCFG

