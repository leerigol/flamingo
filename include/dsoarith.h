
#ifndef _DSO_ARITH_H_
#define _DSO_ARITH_H_
#include <QtCore>

//! seq
#define __MAX_125_64_SEQ__ 56
#define __MIN_125_64_SEQ__ 0

#define __MAX_125_64_SCALE__    (5000000000000000000ll)
#define __MIN_125_64_SCALE__    (1ll)

//! api
#include "../arith/compare.h"
#include "../arith/gcd.h"
#include "../arith/lesssquare.h"
#include "../arith/linear.h"
#include "../arith/ln2.h"
#include "../arith/membar.h"
#include "../arith/sign.h"
#include "../arith/norm125.h"


#endif
