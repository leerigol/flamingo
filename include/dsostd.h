#ifndef _DSO_STD_H
#define _DSO_STD_H

#include "../include/dsotype.h"

#include "../menu/arch/rdsoui.h"
void sysAttachUIView( menu_res::RDsoUI *pUI, DsoView viewId );

#include "../service/servdso/sysdso.h"
//! system obj
#define chan_attr(ch)           getVertAttr(ch)
#define hori_attr(ch, area)     getHoriAttr(ch,area)

//! macro config
#include "dsocfg.h"

//! vertical
#include "../baseclass/dsovert.h"


//! debug
//#define dbg_log()   qDebug()<<__FUNCTION__<<__LINE__;
#define dbg_log()

#include <sys/time.h>
float operator-( timeval &tm1, timeval &tm2 );
#define sysStartTime()  timeval tm1, tm2;gettimeofday( &tm1, 0 );
#define sysDeltaTime()  gettimeofday( &tm2, 0 );qDebug()<<(tm2-tm1)/1000.0<<"ms";

#include "dsoassert.h"


#endif
