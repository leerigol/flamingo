#ifndef _DSOASSERT
#define _DSOASSERT


#define list_at( lst, index )   ( Q_ASSERT( (int)(index) >= 0 && \
                                            (int)(index)<(lst.size())),\
                                            lst[index] )

#endif // DSOASSERT

