#ifndef _DSO_SCPI_EVENT_H_
#define _DSO_SCPI_EVENT_H_

//! 定义SCPI寄存器
enum dsoScpiEvent
{
	//! over load
	scpi_event_ch1_overload,
	scpi_event_ch2_overload,
	scpi_event_ch3_overload,
	scpi_event_ch4_overload,

	scpi_event_ext_overload,

	//! fault
	scpi_event_ch1_fault,
	scpi_event_ch2_fault,
	scpi_event_ch3_fault,
	scpi_event_ch4_fault,

	scpi_event_ext_fault,

	//! mask
	scpi_event_mask_complete,
	scpi_event_mask_fail,
	scpi_event_mask_started,

	//! io
	scpi_event_io_complete,
	scpi_event_io_fail,

	//! oscr
	scpi_event_calibrating,
	scpi_event_setting,
	scpi_event_ranging,
	scpi_event_sweeping,
	
	scpi_event_measuring,
	scpi_event_waitingfortrigger,
	scpi_event_waitingforarm,
	scpi_event_correcting,

	scpi_event_inst_summary,
	scpi_event_command_warnning,

	//! esr
	scpi_event_operation_complete,
	scpi_event_request_control,
	scpi_event_query_error,
	scpi_event_device_depend_error,
	
	scpi_event_execution_error,
	scpi_event_command_error,
	scpi_event_user_request,
	scpi_event_power_on,

	//! qsr
	scpi_event_voltage,
	scpi_event_current,
	scpi_event_time,
	scpi_event_power,
	
	scpi_event_temperature,
	scpi_event_frequency,
	scpi_event_phase,
	scpi_event_modulation,
	
	scpi_event_calibration,

	//! stb
	scpi_event_error_queue,
	scpi_event_output_queue,
};



#endif

