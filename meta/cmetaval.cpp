#include "cmetaval.h"

namespace r_meta {

#define check_type(type) if ( mType != type ){ return false;}

CMetaVal::CMetaVal()
{
    mType = val_unk;
    int64Val = 0;
}

CMetaVal::CMetaVal( CMetaVal &meta )
{
    mType = meta.mType;

    intVal = meta.intVal;
    floatVal = meta.floatVal;
    doubleVal = meta.doubleVal;
    int64Val = meta.int64Val;

    strVal = meta.strVal;
}

CMetaVal CMetaVal::operator=(CMetaVal &meta )
{
    mType = meta.mType;

    intVal = meta.intVal;
    floatVal = meta.floatVal;
    doubleVal = meta.doubleVal;
    int64Val = meta.int64Val;
    return *this;
}

CMetaVal CMetaVal::operator=( int val )
{
    set( val );
    return *this;
}
CMetaVal CMetaVal::operator=( float val )
{
    set( val );
    return *this;
}
CMetaVal CMetaVal::operator=( double val )
{
    set( val );
    return *this;
}
CMetaVal CMetaVal::operator=( qint64 val )
{
    set( val );
    return *this;
}

CMetaVal CMetaVal::operator=( const QString &cvtVal )
{
    int base;
    QString val;
    bool bOk;

    val = cvtVal;

    //! string
    //! 去掉引号
    if ( val.contains('\"') )
    {
        val.remove('\"');
        mType = val_string;
        strVal = val;
        return *this;
    }

    do
    {
        //! fract
        if ( val.contains( QRegExp("[.eEf]")))
        {
            if ( val.contains( QString("f"), Qt::CaseInsensitive ) )
            {
                val.remove( QString("f"),Qt::CaseInsensitive );
                mType = val_float;
                floatVal = val.toFloat( &bOk );
            }
            else
            {
                mType = val_double;
                doubleVal = val.toDouble( &bOk );
            }

            if ( bOk )
            {
                break;
            }
        }

        //! int
        val = cvtVal;
        if ( val.contains(QString("0x"), Qt::CaseInsensitive ) )
        {
            val.remove( QString("0x"),Qt::CaseInsensitive);
            base = 16;
        }
        else
        {
            base = 10;
        }

        if ( val.contains( QString("ll"), Qt::CaseInsensitive ) )
        {
            val.remove(  QString("ll"),Qt::CaseInsensitive );
            mType = val_longlong;
            int64Val = val.toLongLong( &bOk, base );
        }
        else
        {
            mType = val_int;
            intVal = val.toInt( &bOk, base );
        }

        if ( bOk )
        {
            break;
        }

        //! string
        strVal = cvtVal;
        mType = val_string;
        bOk = true;

    }while(0);

    if ( bOk )
    {}
    else
    {
        mType = val_unk;
    }

    return *this;
}

CMetaVal::CMetaVal( int val )
{
    set(val);
}
CMetaVal::CMetaVal( float val )
{
    set(val);
}
CMetaVal::CMetaVal( double val )
{
    set(val);
}
CMetaVal::CMetaVal( qint64 val )
{
    set(val);
}
CMetaVal::CMetaVal( const QString &val )
{
    set(val);
}

void CMetaVal::setType( val_type type )
{
    mType = type;
}
val_type CMetaVal::getType( )
{
    return mType;
}

void CMetaVal::set( int val )
{
    intVal = val;
    mType = val_int;
}
void CMetaVal::set( float val )
{
    floatVal = val;
    mType = val_float;
}
void CMetaVal::set( double val )
{
    doubleVal = val;
    mType = val_double;
}
void CMetaVal::set( qint64 val )
{
    int64Val = val;
    mType = val_longlong;
}
void CMetaVal::set(const QString &val)
{
    mType = val_string;
    strVal = val;
}

bool CMetaVal::get( int &val )
{
    check_type( val_int );

    val = intVal;
    return true;
}

bool CMetaVal::get( float &val )
{
    check_type( val_float );
    val = floatVal;
    return true;
}
bool CMetaVal::get( double &val )
{
    check_type( val_double );
    val = doubleVal;
    return true;
}

bool CMetaVal::get( qlonglong &val )
{
    check_type( val_longlong );
    val = int64Val;

    return true;
}

bool CMetaVal::get(QString &val)
{
    check_type( val_string );
    val = strVal;
    return true;
}

#define verify_type() if ( mType != val.mType ) return false;
bool CMetaVal::operator<( CMetaVal &val )
{
    verify_type();

    return lt( val );
}
bool CMetaVal::operator<=( CMetaVal &val )
{
    verify_type();

    return lt( val );
}
bool CMetaVal::operator==( CMetaVal &val )
{
    verify_type();
    return eq(val);
}

bool CMetaVal::operator>( CMetaVal &val )
{
    verify_type();
    return !le(val);
}
bool CMetaVal::operator>=( CMetaVal &val )
{
    verify_type();
    return !lt(val);
}
bool CMetaVal::operator!=( CMetaVal &val )
{
    verify_type();
    return !eq(val);
}

#define cmp( op )   switch( mType ) \
{\
case val_int:\
    return intVal op val.intVal;\
    \
case val_float:\
    return floatVal op val.floatVal;\
    \
case val_double:\
    return doubleVal op val.doubleVal;\
    \
case val_longlong:\
    return int64Val op val.int64Val;\
    \
default:\
    return false;\
}

bool CMetaVal::lt( CMetaVal &val )
{
    cmp( < )
}
bool CMetaVal::le( CMetaVal &val )
{
    cmp( <= )
}
bool CMetaVal::eq( CMetaVal &val )
{
    cmp( == )
}

CMetaRange::CMetaRange()
{}

/*!
 * \brief CMetaVal::limit
 * \param meta
 * \return < 0 low range
 *         >0 upper range
 *         =0 in range
 */
int CMetaRange::limit( CMetaVal meta )
{
    switch( mType )
    {
    case range_1:
        if ( meta < mValA ) return -1;
        else if ( meta >mValB ) return 1;
        else return 0;

    case range_2:
        if ( meta <= mValA ) return -1;
        else if ( meta >mValB ) return 1;
        else return 0;

    case range_3:
        if ( meta < mValA ) return -1;
        else if ( meta >=mValB ) return 1;
        else return 0;

    case range_4:
        if ( meta <= mValA ) return -1;
        else if ( meta >=mValB ) return 1;
        else return 0;

    case range_5:
        if ( meta <= mValA ) return -1;
        else return 0;

    case range_6:
        if ( meta >=mValB ) return 1;
        else return 0;

    case range_7:
        if ( meta < mValA ) return -1;
        else return 0;

    case range_8:
        if ( meta >mValB ) return 1;
        else return 0;

    default:
        return 0;
    }
}

}
