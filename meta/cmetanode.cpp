#include "../include/dsodbg.h"
#include "cmetanode.h"

#include "crmeta.h"

namespace r_meta {

CMetaNode::CMetaNode( const QString &name,
                      const QString &content )
{
    set( name, content );

    m_pAttr = NULL;
}

CMetaNode::~CMetaNode()
{
}

void CMetaNode::gc()
{
    CMetaNode *pNode;
    foreach( pNode, mChilds )
    {
        Q_ASSERT( NULL != pNode );
        pNode->gc();

        delete pNode;
    }

    //! delete the attr
    if ( m_pAttr != NULL )
    {
        delete m_pAttr;
    }
}

void CMetaNode::set( const QString &name,
                         const QString &content )
{
    mName = name;
    mContent = content;
}

void CMetaNode::setName( const QString &name )
{
    mName = name;
}
QString &CMetaNode::getName()
{
    return mName;
}

void CMetaNode::setAttr( QXmlStreamAttributes attr )
{
    if ( NULL != m_pAttr )
    { delete m_pAttr; m_pAttr = NULL; }

    //! no size
    if ( attr.size() > 0 )
    {}
    else
    { return; }

    m_pAttr = new QXmlStreamAttributes();
    Q_ASSERT( NULL != m_pAttr );

    *m_pAttr = attr;
}
QXmlStreamAttributes *CMetaNode::getAttr()
{
    return m_pAttr;
}

void CMetaNode::setContent( const QString &content )
{
    mContent = content;
}
QString &CMetaNode::getContent()
{
    return mContent;
}

void CMetaNode::attachChild( CMetaNode *pNode )
{
    Q_ASSERT( pNode != NULL );

    mChilds.append( pNode );
}

CMetaNode *CMetaNode::find( const QString &fullName )
{
    QStringList strList;

    strList = fullName.split("/");

    return find( strList, 0 );
}

CMetaNode *CMetaNode::findChild( const QString &fullName )
{
    QStringList strList;

    strList = fullName.split("/");

    return findChild( strList, 0 );
}

CMetaNode *CMetaNode::decend( int level )
{
    Q_ASSERT( level >= 0 );

    if ( level == 0 ) return this;

    if ( mChilds.size() == 1 )
    {
        return mChilds[0]->decend( level - 1 );
    }
    else
    {
        Q_ASSERT( false );
        return NULL;
    }
}

CMetaNode *CMetaNode::find( const QStringList &nameList,
                            int from )
{
    //! at end
    if ( from == nameList.size()-1 )
    {
        if ( nameList[from] == mName )
        {
            return this;
        }
        else
        {
            return NULL;
        }
    }

    //! match self
    if ( nameList[from] == mName )
    {
        //! for the child
        CMetaNode *pFind;
        foreach( CMetaNode * pChild, mChilds )
        {
            Q_ASSERT( NULL != pChild );

            pFind = pChild->find( nameList, from + 1 );

            if ( pFind != NULL )
            {
                return pFind;
            }
        }

        return NULL;
    }
    else
    {
        return NULL;
    }
}

CMetaNode *CMetaNode::findChild( const QStringList &nameList,
                                 int from )
{
    //! for the child
    CMetaNode *pFind;
    foreach( CMetaNode * pChild, mChilds )
    {
        Q_ASSERT( NULL != pChild );

        pFind = pChild->find( nameList, from );

        if ( pFind != NULL )
        {
            return pFind;
        }
    }

    return NULL;
}

int CMetaNode::load( const QString &xmlName )
{
    //! open file
    QFile file(xmlName);
    if ( file.open(QIODevice::ReadOnly) )
    {}
    else
    { return -1; }

    CMetaNode *pParent, *pNow;
    pParent = this;

    //! the parent stack
    QStack< CMetaNode * > parentStack;
    parentStack.push( this );

    //! for parse
    QXmlStreamReader reader;
    reader.setDevice( &file );
    while( !reader.atEnd() )
    {
        reader.readNext();

        //! start
        if ( reader.isStartElement() )
        {
            //! new node
            pNow = new CMetaNode( reader.name().toString() );
            Q_ASSERT( NULL != pNow );

            //! attach to parent
            parentStack.push( pNow );
            pParent->attachChild( pNow );
            pParent = pNow;

            pNow->setAttr( reader.attributes() );

        }
        //! end
        else if ( reader.isEndElement() )
        {
            parentStack.pop();

            pParent = parentStack.top();
        }
        else
        {
            if ( !reader.isWhitespace() && reader.isCharacters() )
            {
                Q_ASSERT( NULL != pNow );
                pNow->setContent( reader.text().toString() );
            }
        }
    }

    file.close();

    return 0;
}

void CMetaNode::show( int preSpa )
{
    QString preStr;

    //! show content
    preStr.fill( '-', preSpa );
    if ( mContent.isEmpty() )
    {
        preStr.append(mName);

        LOG_DBG()<<preStr;
    }
    else
    {
        preStr.append(mName);
        preStr.append(":");
        preStr.append(mContent);

        LOG_DBG()<<preStr;
    }

    foreach( CMetaNode *pNode, mChilds )
    {
        Q_ASSERT( pNode != NULL );

        pNode->show( preSpa + 2 );
    }
}


//! test
void testMetaNode()
{
//    CMetaNode root(":");

//    CMetaNode a("a");
//    CMetaNode b("b");
//    CMetaNode c("c","c");

//    CMetaNode aa("a","aa");
//    CMetaNode ab("b","ab");
//    CMetaNode ac("c","ac");

//    CMetaNode aaa("a","aaa");
//    CMetaNode aab("b","aab");
//    CMetaNode aac("c","aac");

//    CMetaNode ba("a","ba");
//    CMetaNode bb("b","bb");
//    CMetaNode bc("c","bc");

//    root.attachChild( &a );
//    root.attachChild( &b );
//    root.attachChild( &c );

//    a.attachChild( &aa );
//    a.attachChild( &ab );
//    a.attachChild( &ac );

//    aa.attachChild( &aaa );
//    aa.attachChild( &aab );
//    aa.attachChild( &aac );

//    b.attachChild( &bb );
//    b.attachChild( &ba );
//    b.attachChild( &bc );

//    root.show();

//    CMetaNode *pNode = root.find( ":/a/a/c" );
//    if ( NULL != pNode )
//    {
//        qDebug()<<pNode->getContent();
//    }

//    CMetaNode meta;
////    meta.load( "/home/wzy/develope/flamingo/framework/meta/test/test.xml" );
//    meta.load( "/home/wzy/develope/flamingo/framework/meta/platform_a/boardmeta.xml" );
//    meta.decend( 1 );
//    meta.show();
//    meta.decend( 1 )->show();
//    meta.gc();

    r_meta::CBMeta::setMetaFile( "/home/wzy/develope/flamingo/framework/meta/platform_a/_boardmeta.xml" );

    r_meta::CBMeta meta;

    double val;
    meta.getMetaVal( "ch/volt_dot_range", val );
//    qDebug()<<val;

    int valIn = 10000;
    int valOut;
    int err;
    meta.limitRange( "ch/scale/onemegaou", valIn, valOut, err );
//    qDebug()<<valIn<<valOut<<err;

}


}

