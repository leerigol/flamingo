######################################################################
# Automatically generated by qmake (3.0) ?? 3? 3 09:45:35 2016
######################################################################

TEMPLATE = lib
TARGET = ../lib$$(PLATFORM)/meta
INCLUDEPATH += .

CONFIG += static

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


# Input

HEADERS += \
    crmeta.h \
    cmetaval.h \
    cmetanode.h


SOURCES += \
    crmeta.cpp \
    cmetaval.cpp \
    cmetanode.cpp

