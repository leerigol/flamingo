#ifndef CMETANODE_H
#define CMETANODE_H

#include <QtCore>

namespace r_meta {

class CMetaNode
{
public:
    CMetaNode( const QString &name="",
               const QString &content="" );
    virtual ~CMetaNode();

    void gc();

public:
    void set( const QString &name,
                  const QString &content="" );

    void setName( const QString &name );
    QString &getName();

    void setAttr( QXmlStreamAttributes attr );
    QXmlStreamAttributes *getAttr();

    void setContent( const QString &content );
    QString &getContent();

    void attachChild( CMetaNode *pNode );

    CMetaNode *find( const QString &fullName );
    CMetaNode *findChild( const QString &fullName );

    CMetaNode *decend( int level );
protected:
    CMetaNode *find( const QStringList &nameList, int from );
    CMetaNode *findChild( const QStringList &nameList, int from );

public:
    //! load from xml
    int load( const QString &xmlName );

public:
    void show( int prespa = 0 );

protected:
    QString mName;
    QString mContent;

    QXmlStreamAttributes *m_pAttr;
    QList< CMetaNode *> mChilds;

};

void testMetaNode();

}

#endif // CMETANODE_H
