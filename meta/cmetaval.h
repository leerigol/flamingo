#ifndef CMETAVAL_H
#define CMETAVAL_H

#include <QtCore>

namespace r_meta {

enum val_type
{
    val_unk,
    val_int,
    val_float,
    val_double,
    val_longlong,

    val_string,
};

enum range_type
{
    range_unk,
    range_1,    /*!< [] */
    range_2,    /*!< (] */
    range_3,    /*!< [) */
    range_4,    /*!< () */

    range_5,    /*!< ( */
    range_6,    /*!< ) */
    range_7,    /*!< [ */
    range_8,    /*!< ] */
};

class   CMetaVal
{
public:
    CMetaVal();

    CMetaVal( CMetaVal &meta );

    CMetaVal operator=(CMetaVal &meta );
    CMetaVal operator=( int val );
    CMetaVal operator=( float val );
    CMetaVal operator=( double val );
    CMetaVal operator=( qint64 val );

    CMetaVal operator=( const QString &val );

    CMetaVal( int val );
    CMetaVal( float val );
    CMetaVal( double val );
    CMetaVal( qint64 val );

    CMetaVal( const QString &val );

public:
    void setType( val_type type );
    val_type getType( );

    void set( int val );
    void set( float val );
    void set( double val );
    void set( qint64 val );

    void set( const QString &val );

    bool get( int &val );
    bool get( float &val );
    bool get( double &val );
    bool get( qint64 &val );

    bool get( QString &val );

public:
    bool operator<( CMetaVal &val );
    bool operator<=( CMetaVal &val );
    bool operator==( CMetaVal &val );

    bool operator>( CMetaVal &val );
    bool operator>=( CMetaVal &val );
    bool operator!=( CMetaVal &val );



protected:
    bool lt( CMetaVal &val );
    bool le( CMetaVal &val );
    bool eq( CMetaVal &val );

//private:
public:
    union
    {
        int intVal;
        float floatVal;
        double doubleVal;
        qint64 int64Val;
    };
    QString strVal;

    val_type mType;
};

class CMetaRange
{
public:
    CMetaRange();

public:
    int limit( CMetaVal meta );

public:
    CMetaVal mValA;
    CMetaVal mValB;

    range_type mType;
};

}

#endif // CMETAVAL_H
