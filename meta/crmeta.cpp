#include "crmeta.h"


namespace r_meta {

CMeta::CMeta()
{
    m_pMetaNode = NULL;
}

void CMeta::setMetaNode( CMetaNode *pNode )
{
    Q_ASSERT( NULL != pNode );

    m_pMetaNode = pNode;
}

bool CMeta::getMetaRange( const QString &path,
                          CMetaRange *pRange ) const
{
    Q_ASSERT( NULL != m_pMetaNode );
    Q_ASSERT( NULL != pRange );

    CMetaNode *pNode;

    //! base node
    pNode = m_pMetaNode->findChild( path );
    if ( NULL == pNode )
    {
        qDebug() << "XML file parse failed";
        qDebug()<<__FILE__<<__LINE__ << path;
        Q_ASSERT( false );
        return false;
    }

    //! node1/2
    CMetaNode *pNode1, *pNode2;
    pNode1 = pNode->findChild("value1");
    if ( NULL == pNode1 )
    {
        Q_ASSERT( false );
        return false;
    }

    //! parse attr
    QXmlStreamAttributes *pAttr;
    pAttr = pNode1->getAttr();
    if ( NULL == pAttr )
    {
        pRange->mType = range_1;
    }
    else
    {
        if ( pAttr->hasAttribute("range") )
        {
            pRange->mType = toRangeType( pAttr->value("range").toString() );
        }
        else
        {
            pRange->mType = range_1;
        }
    }

    //! decode value
    do
    {
        pRange->mValA = pNode1->getContent();

        //! only one side
        if ( pRange->mType == range_5 ||
             pRange->mType == range_6 ||
             pRange->mType == range_7 ||
             pRange->mType == range_8 )
        {
            pRange->mValB = pRange->mValA;
            break;
        }

        //! for the range2
        pNode2 = pNode->findChild("value2");
        if ( pNode2 == NULL )
        {
            Q_ASSERT( false );
            return false;
        }

        pRange->mValB = pNode2->getContent();
    }while( 0 );

    //! check the type
    if ( pRange->mType != range_unk
         && pRange->mValA.mType != val_unk
         && pRange->mValB.mType != val_unk )
    { return true; }
    else
    { return false; }
}

val_type CMeta::toValType( const QString &strType ) const
{
    if ( strType == "val_int")
    {
        return val_int;
    }
    else if ( strType == "val_float")
    {
        return val_float;
    }
    else if ( strType == "val_double")
    {
        return val_double;
    }
    else if ( strType == "val_longlong")
    {
        return val_longlong;
    }
    else
    {
        return val_unk;
    }
}

range_type CMeta::toRangeType( const QString &rangeType ) const
{
    if ( rangeType == "[]")
    {
        return range_1;
    }
    else if ( rangeType == "(]")
    {
        return range_2;
    }
    else if ( rangeType == "[)")
    {
        return range_3;
    }
    else if ( rangeType == "()")
    {
        return range_4;
    }
    else if ( rangeType == "(")
    {
        return range_5;
    }
    else if ( rangeType == ")")
    {
        return range_6;
    }
    else if ( rangeType == "[")
    {
        return range_7;
    }
    else if ( rangeType == "]")
    {
        return range_8;
    }
    else
    {
        return range_unk;
    }
}

QString CRMeta::_metaFile;
CMetaNode *CRMeta::_metaNode=NULL;
void CRMeta::setMetaFile( const QString &metaFile )
{
    CRMeta::_metaFile = metaFile;

    //! new nodes
    CRMeta::_metaNode = new CMetaNode();
    Q_ASSERT( NULL != CRMeta::_metaNode );

    int ret;
    ret = CRMeta::_metaNode->load( metaFile );
    Q_ASSERT( ret == 0 );
}

CRMeta::CRMeta() : CMeta()
{
    Q_ASSERT( CRMeta::_metaNode != NULL );
    setMetaNode( CRMeta::_metaNode->decend(1) );
}

QString CBMeta::_metaFile;
CMetaNode *CBMeta::_metaNode=NULL;
void CBMeta::setMetaFile( const QString &metaFile )
{
    //! new nodes
    CBMeta::_metaNode = new CMetaNode();
    Q_ASSERT( NULL != CBMeta::_metaNode );

    int ret;
    ret = CBMeta::_metaNode->load( metaFile );
    Q_ASSERT( ret == 0 );
}

CBMeta::CBMeta() : CMeta()
{
    Q_ASSERT( CBMeta::_metaNode != NULL );
    setMetaNode( CBMeta::_metaNode->decend(1) );
}

QString CServMeta::_metaFile;
CMetaNode *CServMeta::_metaNode=NULL;
void CServMeta::setMetaFile( const QString &metaFile )
{
    //! new nodes
    CServMeta::_metaNode = new CMetaNode();
    Q_ASSERT( NULL != CServMeta::_metaNode );

    int ret;
    ret = CServMeta::_metaNode->load( metaFile );
    Q_ASSERT( ret == 0 );

}

CServMeta::CServMeta() : CMeta( )
{
    Q_ASSERT( CServMeta::_metaNode != NULL );
    setMetaNode( CServMeta::_metaNode->decend(1) );
}

QString CAppMeta::_metaFile;
CMetaNode *CAppMeta::_metaNode;
void CAppMeta::setMetaFile( const QString &metaFile )
{
    //! new nodes
    CAppMeta::_metaNode = new CMetaNode();
    Q_ASSERT( NULL != CAppMeta::_metaNode );

    int ret;
    ret = CAppMeta::_metaNode->load( metaFile );
    Q_ASSERT( ret == 0 );

}
CAppMeta::CAppMeta() : CMeta( )
{
    Q_ASSERT( CAppMeta::_metaNode != NULL );
    setMetaNode( CAppMeta::_metaNode->decend(1) );
}



/////////////////////////////////////////////////////
CXmlMeta::CXmlMeta(const QString &metaFile)
{
    //! new nodes
    _metaNode = new CMetaNode();
    Q_ASSERT( NULL != _metaNode );

    int ret;
    ret = _metaNode->load( metaFile );
    Q_ASSERT( ret == 0 );

    setMetaNode( _metaNode->decend(1));

}

CXmlMeta::~CXmlMeta()
{
    _metaNode->gc();
    delete _metaNode;
}

//void CXmlMeta::setMetaFile(const QString &metaFile)
//{
//    if(_metaNode)
//    {
//        _metaNode->gc();
//        delete _metaNode;
//    }
//}

}
