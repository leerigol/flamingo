#ifndef CRMETA_H
#define CRMETA_H

#include <QtCore>
#include <QColor>

#include "../include/dsoerr.h"
#include "../include/dsodbg.h"

#include "./cmetaval.h"
#include "./cmetanode.h"

namespace r_meta {

class CMeta
{
private:
    CMetaNode *m_pMetaNode;

public:
    CMeta();

protected:
    void setMetaNode( CMetaNode *pNode );

public:
    bool getMetaRange( const QString &path,
                       CMetaRange *pRange ) const;

    val_type toValType( const QString &strType ) const;
    range_type toRangeType( const QString &rangeType ) const;

public:
    //! range
    template <typename type>
    bool limitRange( const QString & path,
                     type valIn,
                     type &valOut,
                     DsoErr &errOut
                     ) const
    {
        //! get range
        CMetaRange range;
        bool b = getMetaRange( path, &range );
        if ( !b )
        {
            Q_ASSERT( false );
            return false;
        }

        CMetaVal meta;
        meta = valIn;
        int err = range.limit( meta );
        if ( err < 0 )
        {
            range.mValA.get(valOut);
            errOut = ERR_OVER_LOW_RANGE;

        }
        else if ( err > 0 )
        {
            range.mValB.get(valOut);
            errOut = ERR_OVER_UPPER_RANGE;
        }
        else
        {
            valOut = valIn;
            errOut = ERR_NONE;
        }

        return true;
    }

    bool isNodeExist( const QString &path )
    {
        Q_ASSERT( NULL != m_pMetaNode );
        CMetaNode *pNode = m_pMetaNode->findChild( path );

        return pNode != NULL;
    }

    //! get value
    template <typename type>
    bool getMetaVal( const QString & path,
                     type &valOut
                     ) const
    {
        Q_ASSERT( NULL != m_pMetaNode );
        CMetaNode *pNode = m_pMetaNode->findChild( path );

        if ( NULL == pNode )
        {
            qDebug() << "XML file parse failed";
            qDebug()<<"xml failed:" << path;
            return false;
        }

        CMetaVal meta;
        meta = pNode->getContent();
        meta.get( valOut );

        return true;
    }

    //! x,y,w,h
    bool getMetaVal( const QString &dir,
                     QRect &rect
                     ) const
    {
        QString strSepRect;
        QString path;
        if ( dir.endsWith('/') )
        {
            path = dir.mid( 0, dir.length() - 1 );
        }
        else
        {
            path = dir;
        }

        if ( !getMetaVal( path, strSepRect ) ) return false;

        QStringList strList = strSepRect.split(',');
        Q_ASSERT( strList.size() == 4 );

        rect.setRect( strList[0].toInt(),
                      strList[1].toInt(),
                      strList[2].toInt(),
                      strList[3].toInt() );

        return true;
    }
    //! x,y
    bool getMetaVal( const QString &dir,
                     QPoint &pt ) const
    {
        QString strSepPoint;
        QString path;
        if ( dir.endsWith('/') )
        {
            path = dir.mid( 0, dir.length() - 1 );
        }
        else
        {
            path = dir;
        }

        if ( !getMetaVal( path, strSepPoint ) ) return false;

        QStringList strList = strSepPoint.split(',');
        if ( strList.size() != 2 )
        {
            qDebug()<<__FILE__<<__LINE__ << path;
            //Q_ASSERT(false);
            return false;
        }

        pt.setX( strList[0].toInt() );
        pt.setY( strList[1].toInt() );

        return true;
    }

    bool getMetaVal( const QString &path,
                     QColor &color ) const
    {
        int iColor;

        if ( !getMetaVal(path, iColor ) ) return false;

        color.setRed( (iColor >>16)&0xff );
        color.setGreen( (iColor >>8)&0xff );
        color.setBlue( (iColor >>0)&0xff );

        return true;
    }

    //! w,h
    bool getMetaVal( const QString &dir,
                      QSize &size ) const
    {
        QPoint pt;

        if ( !getMetaVal(dir,pt) ) return false;

        size.setWidth( pt.x() );
        size.setHeight( pt.y() );

        return true;
    }
};

/*!
 * \brief The CRMeta class
 * resource meta
 * 用于描述界面资源
 */
class CRMeta : public CMeta
{
private:
    static QString _metaFile;   /*!< 元数据文件名称 */
    static CMetaNode *_metaNode;
public:
    static void setMetaFile( const QString &metaFile );

public:
    CRMeta();
};

/*!
 * \brief The CBMeta class
 * board meta
 * - 在板元数据
 * - 描述电路板硬件特性
 */
class CBMeta : public CMeta
{
private:
    static QString _metaFile;
    static CMetaNode *_metaNode;
public:
    static void setMetaFile( const QString &metaFile );

public:
    CBMeta();
};

/*!
 * \brief The CServMeta class
 * serv 描述配置
 */
class CServMeta : public CMeta
{
private:
    static QString _metaFile;
    static CMetaNode *_metaNode;
public:
    static void setMetaFile( const QString &metaFile );

public:
    CServMeta();
};

/*!
 * \brief The CAppMeta class
 * app 描述配置
 * - ui元素描述
 */
class CAppMeta : public CMeta
{
private:
    static QString _metaFile;
    static CMetaNode *_metaNode;
public:
    static void setMetaFile( const QString &metaFile );

public:
    CAppMeta();
};

/**
 * @brief The CXmlMeta
 */
class CXmlMeta : public CMeta
{
private:
    CMetaNode *_metaNode;
public:
//    void setMetaFile( const QString &metaFile );

public:
    CXmlMeta(const QString &metaFile);
    ~CXmlMeta();
};

}

#endif // CRMETA_H
