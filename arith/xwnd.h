#ifndef XWND
#define XWND

#include <QtCore>

enum placePriority
{
    priority_none = 0,
    priority_top,
    priority_bottom,
    priority_left,
    priority_right,
};

QPoint sloveDstPt(
               int x, int y,
               const QSize &blockSize,
               const QSize &wndSize,
               const QRect &panelGeo,
               placePriority pri = priority_left,
               int dist = 0
               );

QPoint sloveDstPt(
               const QPoint &pt,
               const QSize &blockSize,
               const QSize &wndSize,
               const QRect &panelGeo,
               placePriority pri = priority_left,
               int dist = 0 );
#endif // XWND

