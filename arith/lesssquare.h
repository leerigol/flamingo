#ifndef LESSSQUARE
#define LESSSQUARE


//! 0 -- no error
//! y = a*x+b
template< typename type >
int lessSquare( type x[], type y[], int cnt,
                 float *a, float *b, float *r )
{
    int i;
    float sumX, sumY;
    float sumXY;
    float sumX2;

    sumX = 0;
    sumY = 0;
    sumXY = 0;
    sumX2 = 0;
    for ( i = 0; i < cnt; i++ )
    {
        sumX += x[i];
        sumY += y[i];
        sumXY += x[i]*y[i];
        sumX2 += x[i]*x[i];
    }

    float div = cnt*sumX2 - sumX*sumX;

    if ( div == 0 ) return -1;

    //! out
    *a = (cnt * sumXY - sumX * sumY) / div;
    *b = (sumX2*sumY - sumX*sumXY) / div;

    //! for r
    float xAvg, yAvg;
    xAvg = sumX / cnt;
    yAvg = sumY / cnt;

    float sumDeltaXY;
    float sumDeltaX2, sumDeltaY2;
    float deltaX, deltaY;

    sumDeltaXY = 0;
    sumDeltaX2 = 0;
    sumDeltaY2 = 0;
    for ( i = 0; i < cnt; i++ )
    {
        deltaX = x[i] - xAvg;
        deltaY = y[i] - yAvg;

        sumDeltaXY += deltaX*deltaY;

        sumDeltaX2 += deltaX*deltaX;
        sumDeltaY2 += deltaY*deltaY;
    }

    div = ( sqrt(sumDeltaX2) * sqrt(sumDeltaY2) );
    if ( div == 0 ) return -1;

    *r = sumDeltaXY  / div;

    return 0;
}


#endif // LESSSQUARE

