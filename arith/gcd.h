#ifndef GCD_H
#define GCD_H

#include <QtCore>

template <typename valType>
valType gcd( valType x, valType y );
//int gcd( int x, int y );
//qlonglong gcd_ll( qlonglong x, qlonglong y );

int lcm( int m, int n );
qlonglong lcm_ll( qlonglong m, qlonglong n );

template <typename valType>
void norm_m_n( valType &m, valType &n );

//void norm_m_n( int &m, int &n );
//void norm_m_n( qlonglong &m, qlonglong &n );

qlonglong norm125( qlonglong t );

#endif // GCD_H
