#ifndef COMPARE
#define COMPARE

//! with error
//! -1 : a < b
//! 1 : a > b
//! 0 : a == b
template< typename type >
int value_compare( type vala, type valb, type error )
{
    type delta;

    if ( error < 0 ) error = -error;

    delta = vala - valb;
    if ( delta < -error )
    { return -1; }
    else if ( delta > error )
    { return 1; }
    else
    { return 0; }
}


#endif // COMPARE

