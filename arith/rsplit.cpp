#include <QDebug>
#include "rsplit.h"

void rsplit( int length, int cnt,
             int sections[] )
{
    //! check zero
    if ( length < 0 )
    { length = -length; }
    else if ( length > 0 )
    {}
    else
    { return; }

    //! check size
    Q_ASSERT( length >= cnt );
    Q_ASSERT( cnt > 0 );

    int i;
    int s32ptr,s32temp;

    int s32mul = length / cnt;
    int s32rem = length % cnt;

    s32ptr = 0;
    s32temp = 0;

    for( i = 0; i < cnt; i++)
    {
        //! base size
        sections[ i ] = s32mul;
        length -= s32mul;

        s32ptr += s32mul;
        s32temp += s32rem;
        if( s32temp >= cnt )
        {
            sections[i]++;
            s32temp -= cnt;

            length--;
        }
    }

    //! check length
//    if ( length > 0 )
//    {
//        sections[i-1] += length;
//    }
}

void rsplit( int length, int cnt,
             int base,
             int from[],
             int span[],
             bool bInvert )
{
    int sections[ cnt ];
    int sign;

    sign = bInvert ? -1 : 1;

    //! split
    rsplit( length, cnt, sections );

    //! convert to range
    for ( int i = 0; i < cnt; i++ )
    {
        from[i] = base;
        span[i] = sections[i];

        base += sections[i] * sign;
    }
}
