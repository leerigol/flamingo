#include "savg.h"

sAvg::sAvg( int avgCnt )
{
    mAvgCnt = avgCnt;

    mCnt = 0;
    mAvg = 0;
    mSum = 0;
}

void sAvg::push( float val )
{
    mCnt++;

    //! max-min
    if ( mCnt == 1 )
    {
        mMax = val;
        mMin = val;
    }
    else
    {
        mMax = qMax( val, mMax );
        mMin = qMin( val, mMin );
    }

    //! calc the averge
    mAvg = ( (mCnt-1)*mAvg + val ) / mCnt;
}
float sAvg::getAverge( bool *pOk )
{
    if( pOk != NULL )
    {
        *pOk = (mCnt >= mAvgCnt);
    }

    return mAvg;
}
int sAvg::getCount( )
{
    return mCnt;
}

float sAvg::getMax()
{ return mMax; }
float sAvg::getMin()
{ return mMin; }

void sAvg::rst()
{
    mCnt = 0;
    mSum = 0;
}

void sAvg::setAvgCount( int cnt )
{
    mAvgCnt = cnt;
}

int sAvg::getAvgCount()
{
    return mAvgCnt;
}
