#ifndef MASKTOLERANCE
#define MASKTOLERANCE

#include <QtCore>
#include "../include/dsocfg.h"

qint32 maskTolerance( DsoPoint *pPoints,
                      qint32 offset,
                      qint32 len,

                      qint32 x,
                      qint32 y,

                      qint16 *pUpper,
                      qint16 *pLower,

                      qint16 overTop,
                      qint16 lowerBase,
                      qint32 cap
                      );

#endif // MASKTOLERANCE

