#ifndef SAVG_H
#define SAVG_H

#include <QtCore>

class sAvg
{
public:
    sAvg( int avgCnt = 16 );

public:
    void push( float val );

    float getAverge( bool *pOk=NULL );
    int getCount( );

    float getMax();
    float getMin();

    void rst();
    void setAvgCount( int cnt );
    int getAvgCount();

protected:
    int mAvgCnt;
    int mCnt;
    float mAvg;
    float mSum;
    float mMax, mMin;
};

#endif // SAVG_H
