#include "scalestep.h"
#include "norm125.h"



int scaleTuneCoarse(int scale, int keyCnt)
{
    int scaleIndex;

    int roofIndex, floorIndex;

    roofIndex = roof125Seq( scale );
    floorIndex = floor125Seq( scale );

    if ( keyCnt < 0 )
    {
        scaleIndex = roofIndex + keyCnt;
    }
    else if ( keyCnt > 0 )
    {
        scaleIndex = floorIndex + keyCnt;
    }
    else
    {
        scaleIndex = floorIndex;
    }

    qlonglong normScale;
    normScale = seqToNorm125( scaleIndex );

    if ( normScale >= __INT32_MAX__ )
    {  normScale = int_floor125( __INT32_MAX__); }
    else if ( normScale <= 0 )
    {  normScale = 1; }
    else
    {}

    return (int)normScale;
}

int scaleTuneFine(int scale, int keyCnt)
{
    int scaleCache;
    int dir;
    int i;

    //! direction
    if ( keyCnt > 0 )
    {
        dir = 1;
    }
    else if ( keyCnt < 0 )
    {
        dir = -1;
    }
    else
    {
        return scale;
    }

    scaleCache = scale;
    //! tune the scale by step
    //! the step is related to the scale
    for ( i = 0; i < keyCnt*dir; i++ )
    {
        scaleCache += dir * scaleToStep( scaleCache, dir );
    }

    return scaleCache;
}

int scaleToStep(int scale, int dir)
{
    int scaleRoof, scaleFloor;

    //! roof, floor
    scaleRoof = int_roof125( scale );
    scaleFloor = int_floor125( scale );

    //! 当前是整数档位
    if ( scaleRoof == scaleFloor )
    {
        //! 整数档位在减档位时使用的是小一档步进
        if ( dir < 0 )
        {
            //! roof not change
        }
        else if ( dir > 0 )
        {
            scaleRoof = int_roof125( scale + 1 );
        }
        else
        {
            Q_ASSERT( false );
        }
    }
    else
    {
        //! correct roof
    }

    return scaleRoof / 100;
}
