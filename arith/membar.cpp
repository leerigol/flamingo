#include "membar.h"

#define H_DIV_NUMS  10

//! type convert
#define BOOL bool
#define TRUE true
#define FALSE false

#define u8  quint8
#define u16 quint16
#define u32 quint32
#define u64 quint64

#define s8  qint8
#define s16 qint16
#define s32 qint32
#define s64 qint64

/** 外框 宽度 */
//#define APP_HOR_GUITOP_POOL_BORDER_WIDTH 2

#define _appHorGuiMemValidScrLen(len) \
    do{                               \
        if ((len) < 3) (len) = 3;     \
    }while(0)

#define _appHorGuiMemValidWaveLen(len) _appHorGuiMemValidScrLen(len)

memPara::memPara()
{
    bZoomOn = false;

    u64MainScale = 100000ll;   //! 100us
    s64MainOffset = 20*100000ll;

    u64ZoomScale = 50000ll;      //! 500ns
    s64ZoomOffset = 0;

    u32MemPts = 1000;
    u64SaPeriod = 2000;          //! 2us
}

barSession::barSession()
{
    bPlay = false;
}

/**
 *
 * @param pstMemRect[IN]
 * @param pstCalcPara[IN]
 * @param pstMemCells[OUT]
 *
 * 触发的零点为 内存中值
 * Stop时 偏移改变即 触发零点与屏幕零点间间距改变
 */
void memBarReMap( QRect* pstMemRect,
                              quint16 u16WavHeight,
                              barSession* pstCalcPara,
                              barCell* pstMemCells )
{
    u16 u16MemLen, u16WaveLen, u16PoolHeight, u16OffsetLen;
    u16 s16OffsetLen;

    s64 s64OffsetTime;
    u64 u64OffsetTime;
    u64 u64MemTime;      /**< 存储深度对应时间 */
    u64 u64ScrTime;      /**< 屏幕对应时间 */

    Q_ASSERT(pstMemRect != NULL
             && pstCalcPara != NULL
             && pstMemCells != NULL);

    //! init
    memset(pstMemCells, 0, sizeof(barCell));

    //! save pool
    pstMemCells->stPool = *pstMemRect;

    //! pool
    u16PoolHeight = pstMemCells->stPool.height();
    u16MemLen = pstMemCells->stPool.width() /*- 2*/;        /**< -2 减去左右边界 */
    u16WaveLen = u16MemLen;

    pstMemCells->stWave.setTop( pstMemCells->stPool.top()
                               + ((u16PoolHeight - u16WavHeight)>>1) );
    pstMemCells->stWave.setBottom( pstMemCells->stPool.bottom()
                               - ((u16PoolHeight - u16WavHeight)>>1) );

    pstMemCells->stScr.setTop( pstMemCells->stPool.top() /*+ 1*/ );
    pstMemCells->stScr.setBottom( pstMemCells->stPool.bottom() /*- 1*/ );

    if (pstCalcPara->bPlay)
    {
        /**
         * Play 模式 即 Stop 状态下
         *
         * 触发位置不变  偏移改变即触发零点与屏幕零点间距改变
         *
         * MemBar 组成为 HalfMem + HalfScr + Offset
         */
        u64 u64HalfScrTime;
        u64 u64HalfMemTime;
        u64 u64TotalTime;

        u16 u16ScrLen;

        BOOL bMemCentered = FALSE, bScrCentered = FALSE;

        u64ScrTime = pstCalcPara->stCurPara.u64MainScale * H_DIV_NUMS;
        u64MemTime = pstCalcPara->stPrePara.u64SaPeriod
                   * pstCalcPara->stPrePara.u32MemPts;

        s64OffsetTime = pstCalcPara->stCurPara.s64MainOffset
                      - pstCalcPara->stPrePara.s64MainOffset;
        u64OffsetTime = s64OffsetTime > 0 ? s64OffsetTime : (-s64OffsetTime);

        u64HalfScrTime = u64ScrTime>>1;
        u64HalfMemTime = u64MemTime>>1;

        if (u64ScrTime > u64MemTime && (u64HalfScrTime > u64HalfMemTime + u64OffsetTime))
        {
            u64TotalTime = u64ScrTime;

            bScrCentered = TRUE;
        }
        else if (u64ScrTime <= u64MemTime && (u64HalfMemTime > u64HalfScrTime + u64OffsetTime))
        {
            u64TotalTime = u64MemTime;

            bMemCentered = TRUE;
        }
        else
        {
            u64TotalTime = u64HalfScrTime + u64HalfMemTime + u64OffsetTime;
        }
        //qDebug() << __LINE__<< "is zero" << u64TotalTime;
        u16WaveLen = u64MemTime * u16MemLen / u64TotalTime;
        u16ScrLen = u64ScrTime * u16MemLen / u64TotalTime;
        u16OffsetLen = u64OffsetTime * u16MemLen / u64TotalTime;
        s16OffsetLen = s64OffsetTime > 0 ? u16OffsetLen : (-u16OffsetLen);

        _appHorGuiMemValidWaveLen(u16WaveLen);
        _appHorGuiMemValidScrLen(u16ScrLen);

        if (bScrCentered)              /**< 压缩 */
        {
            pstMemCells->stScr.setLeft( pstMemCells->stPool.left() /*+ 1*/ );
            pstMemCells->stScr.setRight( pstMemCells->stScr.left()+ u16ScrLen - 1 );

            pstMemCells->stWave.setLeft( (pstMemCells->stScr.left()
                                     + pstMemCells->stScr.width() / 2)
                                   - s16OffsetLen - (u16WaveLen>>1) );
            pstMemCells->stWave.setRight( pstMemCells->stWave.left() + u16WaveLen - 1 );
        }
        else if (bMemCentered)       /**< 扩展 */
        {
            pstMemCells->stWave.setLeft( pstMemCells->stPool.left() /*+ 1*/ );
            pstMemCells->stWave.setRight( pstMemCells->stWave.left() + u16WaveLen - 1 );

            pstMemCells->stScr.setLeft( (pstMemCells->stWave.left()
                                    + pstMemCells->stWave.width()/2)
                                  + s16OffsetLen - (u16ScrLen>>1) );
            pstMemCells->stScr.setRight( pstMemCells->stScr.left() + u16ScrLen - 1 );
        }
        /** 偏移  */
        else if (s64OffsetTime > 0)
        {
            pstMemCells->stWave.setLeft( pstMemCells->stPool.left() /*+ 1*/ );
            pstMemCells->stWave.setRight( pstMemCells->stWave.left() + u16WaveLen - 1 );

            pstMemCells->stScr.setRight( pstMemCells->stPool.right() - 1 );
            pstMemCells->stScr.setLeft( pstMemCells->stScr.right() - u16ScrLen /*+ 1*/ );
        }
        else
        {
            pstMemCells->stScr.setLeft( pstMemCells->stPool.left() /*+ 1*/ );
            pstMemCells->stScr.setRight( pstMemCells->stScr.left() + u16ScrLen - 1 );

            pstMemCells->stWave.setRight(  pstMemCells->stPool.right() - 1 );
            pstMemCells->stWave.setLeft( pstMemCells->stWave.right() - u16WaveLen /*+ 1*/ );
        }

        /**
         * 准备计算 TrigT
         */
        s64OffsetTime = pstCalcPara->stPrePara.s64MainOffset;
    }
    else
    {
        /**
         * 目前 非 Play 即 Run 状态下
         * 触发零点与屏幕零点重合 且保持位置不变
         */
        u64ScrTime = pstCalcPara->stCurPara.u64MainScale * H_DIV_NUMS;

        u64MemTime = pstCalcPara->stCurPara.u64SaPeriod
                   * pstCalcPara->stCurPara.u32MemPts;

        if(u64MemTime >= u64ScrTime)
        {
            /**
             * 计算坐标
             */
            u16 u16ScrLen;

            pstMemCells->stWave.setLeft( pstMemCells->stPool.left() /*+ 1*/ );
            pstMemCells->stWave.setRight( pstMemCells->stPool.right() /*- 1*/ );

            //qDebug() << __LINE__<< "is zero" << u64MemTime;

            u16ScrLen = (u64ScrTime * u16MemLen) / u64MemTime;

            _appHorGuiMemValidScrLen(u16ScrLen);

            pstMemCells->stScr.setLeft( pstMemCells->stPool.left()
                                  + ((u16MemLen- u16ScrLen)>>1) /*+ 1*/ );
            pstMemCells->stScr.setRight( pstMemCells->stScr.left() + u16ScrLen - 1 );

            /**
             * 准备计算 TrigT
             */
            s64OffsetTime = pstCalcPara->stCurPara.s64MainOffset;
        }
        else
        {
            //qDebug() << "Membar:" << u64MemTime << u64ScrTime;
            return;
        }
    }

    /**
     * 计算TrigT
     */
    u64OffsetTime = s64OffsetTime > 0 ? s64OffsetTime : (-s64OffsetTime);

    if (u64OffsetTime > u64MemTime)
    {
        u16OffsetLen = u16WaveLen;
    }
    else
    {
        if( u64MemTime != 0 )
        {
            u16OffsetLen = u64OffsetTime * u16WaveLen / u64MemTime;
        }
        else
        {
            //qDebug() << "Divide by 0 here" << __FILE__<<__FUNCTION__<<__LINE__;
        }
    }

    s16OffsetLen = s64OffsetTime > 0 ? u16OffsetLen : (-u16OffsetLen);

    pstMemCells->s16TrigTx = (pstMemCells->stWave.left() + pstMemCells->stWave.width() / 2)
                           - s16OffsetLen - 1;

    /**
     * 计算 Zoom
     */
    u16 u16ZoomScrLen;
    s16 s16ZoomOffsetLen;

    pstMemCells->stZoomScrL = pstMemCells->stScr;
    pstMemCells->stZoomScrR = pstMemCells->stScr;

    if (pstCalcPara->stCurPara.bZoomOn)
    {
        u16ZoomScrLen = pstCalcPara->stCurPara.u64ZoomScale * H_DIV_NUMS
                      * (pstMemCells->stScr.width())
                      / u64ScrTime;
        s16ZoomOffsetLen = (pstCalcPara->stCurPara.s64ZoomOffset
                           -pstCalcPara->stCurPara.s64MainOffset)
                         * (pstMemCells->stScr.width() )
                         / (s64)u64ScrTime;

        pstMemCells->stZoomScrL.setRight( (pstMemCells->stScr.left() + pstMemCells->stScr.width() / 2 )
                                          - (u16ZoomScrLen>>1)
                                          + s16ZoomOffsetLen );

        pstMemCells->stZoomScrR.setLeft( (pstMemCells->stScr.left() + pstMemCells->stScr.width() / 2 )
                                   + (u16ZoomScrLen>>1)
                                   + s16ZoomOffsetLen );
    }
    else
    {
        pstMemCells->stZoomScrL.setRight( pstMemCells->stScr.left() );
        pstMemCells->stZoomScrR.setLeft( pstMemCells->stScr.right() );
    }
}
