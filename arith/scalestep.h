#ifndef SCALESTEP_H
#define SCALESTEP_H

int scaleTuneCoarse( int scale, int keyCnt );
int scaleTuneFine( int scale, int keyCnt );

int scaleToStep( int scale, int dir );

#endif // SCALESTEP_H
