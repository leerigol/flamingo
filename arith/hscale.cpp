
#include "./hscale.h"

struct bw_minScale{
    Bandwidth bw;
    qlonglong minScale;
};

static bw_minScale _bw_minScale[]=
{
    { BW_20M, time_ns(10) },
    { BW_50M, time_ns(5) },
    { BW_70M, time_ns(5) },
    { BW_100M, time_ns(5) },

    { BW_150M, time_ns(2) },
    { BW_200M, time_ns(2) },
    { BW_250M, time_ns(2) },

    { BW_300M, time_ns(1) },
    { BW_350M, time_ns(1) },
    { BW_500M, time_ps(500) },

    { BW_600M, time_ps(500) },

    { BW_1G, time_ps(500) },
    { BW_2G, time_ps(200) },
    { BW_5G, time_ps(100) },
};

qlonglong minHScale( Bandwidth bw )
{
    for ( int i = 0; i < array_count(_bw_minScale); i++ )
    {
        if ( bw == _bw_minScale[i].bw )
        {
            return _bw_minScale[i].minScale;
        }
    }

    qWarning()<<"!!! mismatch bw scale";
    return _bw_minScale[0].minScale;
}
