#ifndef LINEAR
#define LINEAR

//! 0 -- no error
//! y = a*x  + b
template< typename type >
int linear( type x1, type y1,
            type x2, type y2,
                 float *a, float *b )
{
    //! y1 = a * x1 + b
    //! y2 = a * x2 + b

    if ( x2 == x1 ) return -1;

    *a = (y2-y1)/(float)(x2-x1);
    *b = y2 - (*a) * x2;

    return 0;
}

#endif // LINEAR

