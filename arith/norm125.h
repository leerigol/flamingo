#ifndef NORM125
#define NORM125

#include <QtCore>

int norm125ToSeq( qlonglong norm );
int floor125Seq( qlonglong val );
int roof125Seq( qlonglong val );

qlonglong seqToNorm125( int seq );

//! 125 norm
void roof125( int val, int &roof );
void roof125( qlonglong val, qlonglong &roof );

int int_roof125( int val );
qlonglong ll_roof125( qlonglong val );

void floor125( int val, int &floor );
void floor125( qlonglong val, qlonglong &floor );

int int_floor125( int val );
qlonglong ll_floor125( qlonglong val );

//! 10
int int_roof10( int val );
qlonglong ll_roof10( qlonglong val );

int int_roof10L( int val );
qlonglong ll_roof10L( qlonglong val );

int int_ceil2N( int val );
qlonglong ll_ceil2N( qlonglong val );

int int_floor2N( int val );
qlonglong ll_floor2N( qlonglong val );

#endif // NORM125

