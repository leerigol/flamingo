#include "gcd.h"


template <typename valType>
valType gcd( valType x, valType y )
{
    valType m;

    if( x < y )
    {
        return gcd( y, x );
    }

    if ( y == 0 )
    {
        return x;
    }

    m = x % y;
    if( m != 0 )
    {
        return gcd( y, m );
    }
    else
    {
        return y;
    }
}

////! x,y最大公约数
//int gcd( int x, int y )
//{
//    int m;

//    if( x < y )
//    {
//        return gcd( y, x );
//    }

//    if ( y == 0 )
//    {
//        return x;
//    }

//    m = x % y;
//    if( m != 0 )
//    {
//        return gcd( y, m );
//    }
//    else
//    {
//        return y;
//    }
//}

//qlonglong gcd_ll( qlonglong x, qlonglong y )
//{
//    qlonglong m;

//    if( x < y )
//    {
//        return gcd_ll( y, x );
//    }

//    if ( y == 0 )
//    {
//        return x;
//    }

//    m = x % y;
//    if( m != 0 )
//    {
//        return gcd_ll( y, m );
//    }
//    else
//    {
//        return y;
//    }
//}


//! m, n 最小公倍数
//! 4 , 5 = 20
//! m * n 除以 m , n 的最大公约数
int lcm( int m, int n )
{
    int nom;
    if ( m > n )
    {
        nom = gcd( m, n );
    }
    else
    {
        nom = gcd( n, m );
    }

    Q_ASSERT( nom != 0 );

    return m * n / nom;
}

qlonglong lcm_ll( qlonglong m, qlonglong n )
{
	qlonglong nom;
    if ( m > n )
    {
        nom = gcd( m, n );
    }
    else
    {
        nom = gcd( n, m );
    }

    Q_ASSERT( nom != 0 );

    return m * n / nom;
}

//! 用最大公约数公约 M/N
//! the sign is keeped
template< >
void norm_m_n( int &m, int &n )
{
    Q_ASSERT( n != 0 );
    if ( m == 0 )
    { return; }

    bool signM, signN;

    //! get sign
    signM = m > 0;
    signN = n > 0;

    //! to positive
    if ( !signM )
    { m = -m; }
    if ( !signN )
    { n = -n; }

    //! align
    int cM;
    cM = gcd( m, n );

    m /= cM;
    n /= cM;

    //! recover sign
    if ( !signM )
    { m = -m; }
    if ( !signN )
    { n = -n; }
}

//! 用最大公约数公约 M/N
//! the sign is keeped
template< >
void norm_m_n( qlonglong &m, qlonglong &n )
{
    Q_ASSERT( n != 0 );
    if ( m == 0 )
    { return; }

    bool signM, signN;

    //! get sign
    signM = m > 0;
    signN = n > 0;

    //! to positive
    if ( !signM )
    { m = -m; }
    if ( !signN )
    { n = -n; }

    //! align
    qlonglong cM;
    cM = gcd( m, n );

    m /= cM;
    n /= cM;

    //! recover sign
    if ( !signM )
    { m = -m; }
    if ( !signN )
    { n = -n; }
}

template <typename valType>
void norm_m_n( valType &m, valType &n )
{
    Q_ASSERT( n != 0 );
    if ( m == 0 )
    { return; }

    bool signM, signN;

    //! get sign
    signM = m > 0;
    signN = n > 0;

    //! to positive
    if ( !signM )
    { m = -m; }
    if ( !signN )
    { n = -n; }

    //! align
    valType cM;
    cM = gcd( m, n );

    m /= cM;
    n /= cM;

    //! recover sign
    if ( !signM )
    { m = -m; }
    if ( !signN )
    { n = -n; }
}
