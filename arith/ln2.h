#ifndef LN2_H
#define LN2_H

#include <QtCore>

template<typename type>
int ln2( type val )
{
    int po;

    po = 0;

    //! must be positive
    if ( val <= 0 )
    { return 0; }

    //! width
    for ( int i = 0; i < 31; i++ )
    {
        if ( (val & 0x01 ) == 0x01 )
        {
            po = i;
        }

        val >>= 1;
    }

    return po;
}


#endif // SQRT2_H

