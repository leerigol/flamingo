#ifndef MEMBAR
#define MEMBAR

#include <QtCore>

/**
 * 内存条各矩形坐标
 */
struct barCell
{
    QRect stPool;

    QRect stScr;          /**< 对应于 屏幕 */
    QRect stZoomScrL;     /**< 对应于屏幕左Zoom */
    QRect stZoomScrR;     /**< 对应于屏幕右Zoom */
    QRect stWave;         /**< 对应于存储深度 */

    qint16 s16TrigTx;
};

struct memPara
{
    bool bZoomOn;

    quint64  u64MainScale;
    qint64 s64MainOffset;

    quint64  u64ZoomScale;
    qint64  s64ZoomOffset;

    quint32  u32MemPts;
    quint64  u64SaPeriod;

    memPara();
};

struct barSession
{
    bool bPlay;

    memPara stCurPara;
    memPara stPrePara;

    barSession();
};

void memBarReMap( QRect* pstMemRect,
                  quint16 u16WavHeight,
                  barSession* pstCalcPara,
                  barCell* pstMemCells );

#endif // MEMBAR

