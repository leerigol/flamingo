#include "norm125.h"
#include "../include/dsocfg.h"


//! array size
#define array_count( ary )     (int)( sizeof(ary)/sizeof(ary[0]) )

#define seq125( scale ) 1*scale,2*scale,5*scale
#define seq125_k( base )    seq125( 1 * base ),\
                            seq125( 10 * base ),\
                            seq125( 100 * base )

//! pre-create the convert table
static int _norm125_int[]
{
    seq125_k( 1 ),
    seq125_k( 1000 ),
    seq125_k( 1000000 ),
    1000000000,2000000000,
};

static qlonglong _norm125_ll[]
{
    seq125_k( 1ll ),
    seq125_k( _1E3_ ),
    seq125_k( _1E6_ ),

    seq125_k( _1E9_ ),
    seq125_k( _1E12_ ),
    seq125_k( _1E15_ ),

    seq125( _1E18_ ),
};

//! seq -- value
//! 0 -- 1
//! 1 -- 2
//! 2 -- 5
//! 3 -- 10
//! 4 -- 20
//! 5 -- 50
//! 6 -- 100
int norm125ToSeq( qlonglong norm )
{
    Q_ASSERT( norm > 0 );

    for ( int i = 0; i < array_count(_norm125_ll); i++ )
    {
        if ( norm <= _norm125_ll[i] )
        { return i; }
    }

    qWarning()<<"!!!large sequence";
    //! max seq
    return array_count(_norm125_ll)-1;
}

int floor125Seq( qlonglong val )
{
    qlonglong floor;

    //! floor
    floor = ll_floor125( val );

    //! to seq
    return norm125ToSeq( floor );
}
int roof125Seq( qlonglong val )
{
    qlonglong roof;

    //! roof
    roof = ll_roof125( val );

    //! to seq
    return norm125ToSeq( roof );
}

//! 0,1,2,3,4,5
//! 1,2,5,10,20,50
qlonglong seqToNorm125( int seq )
{
    if ( seq <= 0 )
    { seq = 0; }

    if ( seq >= array_count( _norm125_ll) )
    {
        qWarning()<<"!!!large sequence";
        return _norm125_ll[ array_count( _norm125_ll) - 1 ];
    }

    return _norm125_ll[ seq ];
}

//! -- roof
void roof125( int val, int &roof )
{
    for ( int i = 0; i < array_count( _norm125_int ); i++ )
    {
        if ( val <= _norm125_int[i] )
        {
            roof = _norm125_int[i];
            return;
        }
    }

    //! max
    qWarning()<<"!!!large sequence";
    roof = _norm125_int[ array_count( _norm125_int) - 1 ];
}

void roof125( qlonglong val, qlonglong &roof )
{
    for ( int i = 0; i < array_count( _norm125_ll); i++ )
    {
        if ( val <= _norm125_ll[i] )
        {
            roof = _norm125_ll[i];
            return;
        }
    }

    //! max
    qWarning()<<"!!!large sequence";
    roof = _norm125_ll[ array_count( _norm125_ll) - 1 ];
}

int int_roof125( int val )
{
    int roof;

    roof125( val, roof );

    return roof;
}
qlonglong ll_roof125( qlonglong val )
{
    qlonglong roof;

    roof125( val, roof );

    return roof;
}

void floor125( int val, int &floor )
{
    for ( int i = array_count( _norm125_int ) - 1; i >= 0; i-- )
    {
        if ( val >= _norm125_int[i] )
        { floor = _norm125_int[i]; return; }
    }

    qWarning()<<"!!!small sequence";
    floor = _norm125_int[0];
}
void floor125( qlonglong val, qlonglong &floor )
{
    for ( int i = array_count( _norm125_ll ) - 1; i >= 0; i-- )
    {
        if ( val >= _norm125_ll[i] )
        {
            floor = _norm125_ll[i];
            return;
        }
    }

    qWarning()<<"!!!small sequence";
    floor = _norm125_ll[0];
}

int int_floor125( int val )
{
    int floor;

    floor125( val, floor );

    return floor;
}

qlonglong ll_floor125( qlonglong val )
{
    qlonglong floor;

    floor125( val, floor );

    return floor;
}

//! 0~9 -> 10
//! 10~99 -> 100
//! 100~999 -> 1000
int int_roof10( int val )
{
   Q_ASSERT( val != 0 );

   int scale;
   int roof;

   //! roof
   if ( val < 0 )
   { roof = -1; }
   else
   { roof = 1;}

   //! abs
   if ( val < 0 )
   { val = -val; }

   //! scale
   scale = 0;
   do
   {
       val /= 10;
       scale++;
   }while( val > 0 );

   //! cale the roof
   for ( int i = 0; i < scale && i < 9; i++ )
   {
        roof *= 10;
   }

   return roof;
}
qlonglong ll_roof10( qlonglong val )
{
    Q_ASSERT( val != 0 );

    int scale;
    qlonglong roof;

    //! roof
    if ( val < 0 )
    { roof = -1; }
    else
    { roof = 1;}

    //! abs
    if ( val < 0 )
    { val = -val; }

    //! scale
    scale = 0;
    do
    {
        val /= 10;
        scale++;
    }while( val > 0 );


    //! calc the roof
    for ( int i = 0; i < scale && i < 18; i++ )
    {
         roof *= 10;
    }

    return roof;
}

//! 0~10 -> 10
//! 11~100 -> 100
//! 101~1000 -> 1000
int int_roof10L( int val )
{
   int normRoof;

   normRoof = int_roof10( val );

   //! the jump limit
   if ( (normRoof / val) == 10 )
   { return normRoof / 10; }
   else
   { return normRoof;}
}
qlonglong ll_roof10L( qlonglong val )
{
    qlonglong normRoof;

    normRoof = ll_roof10( val );

    //! the jump limit
    if ( (normRoof / val) == 10 )
    { return normRoof / 10; }
    else
    { return normRoof;}
}

int int_ceil2N( int val )
{
    if ( val <= 1 )
    { Q_ASSERT( false ); }

    int msb, lsb, iMask;
    msb = -1;
    lsb = -1;

    iMask = 1;
    //! lsb->msb
    for( int i = 0; i < 31; i++ )
    {
        // bit i is not 0
        if ( (val & iMask) == iMask )
        {
            if ( lsb < 0 )
            { lsb = i; }

            msb = i;
        }

        iMask<<=1;
    }

    //! only 1 bit
    if ( lsb == msb )
    {
        return (1<<msb);
    }
    else if ( msb >= 30 )
    {
        qWarning()<<"!!!overflow";
        return 1<<30;
    }
    else
    {
        return 1<<(msb+1);
    }
}
qlonglong ll_ceil2N( qlonglong val )
{
    if ( val <= 1 )
    { Q_ASSERT( false ); }

    qlonglong msb, lsb, iMask;
    msb = -1;
    lsb = -1;

    iMask = 1ll;
    //! lsb->msb
    for( int i = 0; i < 63; i++ )
    {
        // bit i is not 0
        if ( (val & iMask) == iMask )
        {
            if ( lsb < 0 )
            { lsb = i; }

            msb = i;
        }

        iMask<<=1ll;
    }

    //! only 1 bit
    if ( lsb == msb )
    {
        return (1ll<<msb);
    }
    else if ( msb >= 62 )
    {
        qWarning()<<"!!!overflow";
        return ((1ll)<<62ll);
    }
    else
    {
        return ((1ll)<<(msb+1));
    }
}

int int_floor2N( int val )
{
    if ( val <= 1 )
    {
        Q_ASSERT( false );
    }

    int msb, lsb, iMask;
    msb = -1;
    lsb = -1;

    iMask = 1;
    //! lsb->msb
    for( int i = 0; i < 31; i++ )
    {
        // bit i is not 0
        if ( (val & iMask) == iMask )
        {
            if ( lsb < 0 )
            { lsb = i; }

            msb = i;
        }

        iMask<<=1;
    }

    //! only 1 bit
    if ( lsb == msb )
    {
        return (1<<msb);
    }
    else if ( msb >= 30 )
    {
        qWarning()<<"!!!overflow";
        return 1<<30;
    }
    else
    {
        return 1<<(msb);
    }
}
qlonglong ll_floor2N( qlonglong val )
{
    if ( val <= 1 )
    { Q_ASSERT( false ); }

    qlonglong msb, lsb, iMask;
    msb = -1;
    lsb = -1;

    iMask = 1ll;
    //! lsb->msb
    for( int i = 0; i < 63; i++ )
    {
        // bit i is not 0
        if ( (val & iMask) == iMask )
        {
            if ( lsb < 0 )
            { lsb = i; }

            msb = i;
        }

        iMask<<=1ll;
    }

    //! only 1 bit
    if ( lsb == msb )
    {
        return (1ll<<msb);
    }
    else if ( msb >= 62 )
    {
        qWarning()<<"!!!overflow";
        return ((1ll)<<62ll);
    }
    else
    {
        return ((1ll)<<(msb));
    }
}
