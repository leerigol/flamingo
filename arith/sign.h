#ifndef SIGN
#define SIGN

//! -1 < 0
//! 1 > 0
//! 0 -- 0
template< typename type >
int sign( type val )
{
    if ( val < 0 ) return -1;
    else if ( val > 0 ) return 1;
    else return 0;
}

#endif // SIGN

