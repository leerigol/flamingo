#include "masktolerance.h"

qint32 maskTolerance( DsoPoint *pRawPoints,
                      qint32 offset,
                      qint32 len,

                      qint32 x,
                      qint32 y,

                      qint16 *pRawUpper,
                      qint16 *pRawLower,

                      qint16 overTop,
                      qint16 lowerBase,
                      qint32 cap
                      )
{
    Q_ASSERT( NULL != pRawPoints );
    Q_ASSERT( NULL != pRawUpper && NULL != pRawLower );

    DsoPoint *pPoints;
    qint16 *pUpper, *pLower;
    qint32 top, base;

    //! real start
    pPoints = pRawPoints + offset;
    pUpper = pRawUpper + offset;
    pLower = pRawLower + offset;

    //! init
    for ( int i = 0; i < len; i++ )
    {
        pLower[i] = pUpper[i] = pPoints[i];
    }

    //! set the max && min
    for ( int i = 0; i < len; i++ )
    {
        base = pPoints[i] - y;
        top = pPoints[i] + y;

        //! center range
        for ( int j = i - x, to = i+x; j < len && j <= to; j++ )
        {
            //! over left
            if ( j < 0 )
            { continue; }

            if ( base < pLower[j] )
            { pLower[j] = base; }

            if ( top > pUpper[j] )
            { pUpper[j] = top; }
        }
    }

    //! set the head/tail
    for ( int i = 0; i < (offset + x) && i < (len + offset); i++ )
    {
        pRawLower[i] = lowerBase;
        pRawUpper[i] = overTop;
    }

    for ( int i = cap -1; i >= (offset) && i >= ( offset + len - x); i-- )
    {
        pRawLower[i] = lowerBase;
        pRawUpper[i] = overTop;
    }

    //! limit the range
    for ( int i = 0; i < len; i++ )
    {
        if ( pLower[i] < lowerBase )
        { pLower[i] = lowerBase; }

        if ( pUpper[i] > overTop )
        { pUpper[i] = overTop; }
    }

    return 0;
}
