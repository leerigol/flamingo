
#include <QtCore>
#include "./xwnd.h"

QPoint sloveDstPt( int x, int y,
               const QSize &blockSize,
               const QSize &wndSize,
               const QRect &panelGeo,
               placePriority pri,
               int dist )
{
    int x1, y1, x2, y2;

    //! theory x
    {
        if ( pri == priority_left )
        {   x1 = x - wndSize.width() - dist ; }
        else if ( pri == priority_right )
        {   x1 = x + blockSize.width() + dist; }
        else if ( pri == priority_top
                  || pri == priority_bottom )
        {   x1 = x; }
        else
        {   x1 = x - wndSize.width() - dist ; }

        if ( blockSize.width() > 0 )
        {
            //! over left
            if ( x1 < 0 )
            { x1 = x + blockSize.width(); }
            else
            {}
        }
        else
        {
            //! over left
            if ( x1 < 0 )
            { x1 = 0; }
        }

        x2 = x1 + wndSize.width();
        //! over right
        if ( x2 > panelGeo.right() )
        { x2 = panelGeo.right(); }

        x1 = x2 - wndSize.width();
        if ( x1 < panelGeo.left() ) x1 = panelGeo.left();
    }

    //! theory y
    {
        y1 = y;

        if ( pri == priority_left
             || pri == priority_right )
        {   y1 = y; }
        else if ( pri == priority_top )
        {   y1 = y - wndSize.height() - dist; }
        else if ( pri == priority_bottom )
        {   y1 = y + blockSize.height() + dist; }
        else
        {   y1 = y; }


        y2 = y1 + wndSize.height();
        if (  y2 > panelGeo.bottom() )
        { y2 = panelGeo.bottom(); }
        else
        {}

        y1 = y2 - wndSize.height();
        if ( y1 < panelGeo.top() ) y1 = panelGeo.top();
    }

    return QPoint( x1, y1 );
}

QPoint sloveDstPt(
               const QPoint &pt,
               const QSize &blockSize,
               const QSize &wndSize,
               const QRect &panelGeo,
               placePriority pri,
               int dist )
{
    return sloveDstPt( pt.x(),
                pt.y(),
                blockSize,
                wndSize,
                panelGeo,
                pri,
                dist
                );
}
