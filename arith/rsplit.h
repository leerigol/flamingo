#ifndef RSPLIT
#define RSPLIT

void rsplit( int length, int cnt,
             int sections[] );

void rsplit( int length, int cnt,
             int base,
             int from[],
             int span[],
             bool bInv = false );

#endif // RSPLIT

