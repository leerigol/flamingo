#-------------------------------------------------
#
# Project created by QtCreator 2016-10-18T10:23:05
#
#-------------------------------------------------

QT       += gui
QT       += widgets

TARGET = ../lib$$(PLATFORM)/dsofw
TEMPLATE = lib
CONFIG += staticlib

INCLUDEPATH += .

DEFINES += _DSO_KEYBOARD

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}

#DEFINES += _DEBUG

SOURCES += \
    service/service.cpp \
    service/timer.cpp \
    service/action.cpp \
    service/spy.cpp \
    service/object.cpp \
    service/executor.cpp \
    service/cintent.cpp \
    service/service_key.cpp \
    service/csignal.cpp

HEADERS += \
    service/service.h \
    service/service_msg.h \
    service/timer.h \
    service/action.h \
    service/spy.h \
    service/cintent.h \
    service/service_key.h \
    service/csignal.h

