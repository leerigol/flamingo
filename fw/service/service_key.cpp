
#include "service_key.h"

serviceKey::serviceKey()
{}

serviceKey::serviceKey( int key,
                        int msg,
                        int action )
{
    mKey = key;
    mMsg = msg;
    mAction = action;
}

serviceKeyMap::serviceKeyMap()
{}

void serviceKeyMap::append( int key,
                            int msg,
                            int action )
{
    servKeyList::append( serviceKey(key,msg,action) );
}

bool serviceKeyMap::find( int key,
                          serviceKey &servKey )
{
    QListIterator< serviceKey > iter(*this);

    while( iter.hasNext() )
    {
        servKey = iter.next();
        if ( servKey.mKey == key )
        { return true; }
    }

    return false;
}
