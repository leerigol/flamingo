#ifndef _FW_SERVICE_MSG_H_
#define _FW_SERVICE_MSG_H_

//! messages
#define  CMD_SERVICE_NONE       0       /*!< end of msg table */

//! user service msg
//! from 1

#define MENU_MSG_BASE   256
//! menu msg
//! from 256
#include "../../resource/menu/msg.h"

//! app msg
//! from 0xf000
#define CMD_APP_BASE    0xf000

//! system msg
//! from 0x10000
enum enumSysCommand
{
CMD_SERVICE_ACTIVE = 0x10000,       /*!< service active, para: nth menu */
CMD_SERVICE_DEACTIVE,               //! no para

CMD_SERVICE_ENTER_ACTIVE,           //! para: service id
CMD_SERVICE_EXIT_ACTIVE,            //! para: service id
CMD_SERVICE_RE_ENTER_ACTIVE,        //! para: service id

CMD_SERVICE_VERT_ACTIVE,            //! para: vert serv id

CMD_SERVICE_SUB_ENTER,              //! para: sub menu msg id
CMD_SERVICE_SUB_RETURN,             //! para: sub menu msg id

CMD_SERVICE_ITEM_FOCUS,             //! para: item msg
CMD_SERVICE_ITEM_FOCUS_AGAIN,       //! 重复获取焦点。


CMD_SERVICE_VIEW_CHANGE,             //! para: s32 -- no use

CMD_SERVICE_INTENT,                  //! argument: ctrl intent

CMD_SERVICE_PROGRESS_CANCEL,         //! progress cancel

CMD_SERVICE_INIT,                   //! do only once
CMD_SERVICE_RST,
CMD_SERVICE_STARTUP,    
CMD_SERVICE_SERIALIN,
CMD_SERVICE_SERIALOUT,

CMD_SERVICE_LOAD,                   //! load info from file: int (void)

CMD_SERVICE_MODIFIED,               //! setting change
CMD_SERVICE_SETTING_LOAD,

CMD_SERVICE_TIMEOUT,                //! para: timeid

                                    //! key msg
CMD_SERVICE_DO_ERR,                 //! para: DsoErr

CMD_SERVICE_HELP_REQUESST,          //! para: helpRequest

CMD_SERVICE_LED_REQUESET,           //! led, bool

CMD_SERVICE_PRE_DO,                 //! msg
CMD_SERVICE_POST_DO,                //! msg

CMD_SERVICE_ATTR_CHANGED,           //! attr changed

CMD_SERVICE_SOFT_INT,               //! quint32
CMD_SERVICE_PHY_KEY,                //! int: physical key
CMD_SERVICE_SERVICE_UP,             //! service up, manager the service status

CMD_SERVICE_CMD_EXIST,              //! qint32 msg, bool b
CMD_SERVICE_TICK,                   //! qint32 tick ms

CMD_SERVICE_PAGE_SHOW,              //! page show/hide 1:0
CMD_SERVICE_PAGE_HIDE,              //! only hide, no para

CMD_ENGINE_TICK,                   //! For Engine playback
CMD_SERVICE_LOAD_PRIVACY
};

#define is_system_msg( msg )    (msg >= CMD_SERVICE_ACTIVE)
#define is_ui_msg( msg )        ( msg >= MENU_MSG_BASE && msg < CMD_SERVICE_ACTIVE )
#endif
