#include "action.h"
#include "../include/dsodbg.h"
struActionContext::struActionContext( bool ui, bool user, bool init )
                    : isUi(ui),isUser(user), isInit(init)
{
}

/*! \brief 进入动作环境
*
* 在动作执行前被调用，通知动作环境，即将执行动作
*/
void struActionContext::enter()
{}

/*! \brief 退出动作环境
*
* 在动作执行完成后被调用，通知动作环境，已经完成动作执行
* \param err 动作执行的返回结果，动作环境可能需要将返回错误
* 进行格式化输出。例如，远程命令需要将错误消息值进行标准格式化。
*/
void struActionContext::exit( DsoErr /*err*/ )
{}

struActionContext *struActionContext::proxy()
{ return this; }

bool struActionContext::isUiAction()
{
    return isUi;
}
bool struActionContext::isUserAction()
{
    return isUser;
}

bool struActionContext::isInitAction()
{
    return isInit;
}

struActionContext_Console::struActionContext_Console( bool ui, bool user )
    :struActionContext( ui, user )
{
    isUi = false;
    isUser = true;
}

void struActionContext_Console::enter()
{}
void struActionContext_Console::exit( DsoErr /*err*/ )
{
//    qDebug()<<"console exit"<<err;
}

struActionContextRmt::struActionContextRmt( bool ui, bool user )
    :struActionContext( ui, user )
{
    isUi = false;
    isUser = true;

    m_pSema = NULL;

    mProxy = 0;
}

void struActionContextRmt::enter()
{
    mErr = ERR_NONE;
}
void struActionContextRmt::exit( DsoErr err )
{
    Q_ASSERT( NULL != m_pSema );
    //qDebug()<<__FILE__<<__LINE__<<mProxy<<m_pSema->available();
    if ( mProxy==0 )
    {
        m_pSema->release();
    }
    else
    {
        mProxy--;
    }

    mErr = err;
}

struActionContext* struActionContextRmt::proxy()
{
    mProxy++;
    return struActionContext::proxy();
}

/*! \brief struServAction::struServAction
 *
 * 创建服务动作
 */
struServAction::struServAction()
{}

/*! \brief struServAction
*
* 服务动作，即需要服务执行的消息
* \param id 服务目标
* \param msg 做什么
* \param para 做到什么程度
* \param pContext 识别动作环境，动作来源
*/
struServAction::struServAction( int id, int msg,
                                const struServPara &para,
                                struActionContext *pContext )
                : servId(id), servMsg(msg), servPara(para), pservContext(pContext)
{}

/*! \brief newAction
*
* 新建一个服务动作
* \sa struServAction
*/
struServAction * struServAction::newAction( int id, int msg,
                                            const struServPara &para,
                                            struActionContext *pContext)
{
    this->servId = id;
    this->servMsg = msg;
    this->servPara = para;

    this->pservContext = pContext;

    return this;
}

bool struServAction::isCompany( struServAction &act )
{
    if ( act.servId != servId ) return false;

    if ( act.servMsg != servMsg ) return false;

    return true;
}

bool struServAction::isCompany(int id, int msg) const
{
    if ( id != servId ) return false;

    if ( msg != servMsg ) return false;

    return true;
}

void struServAction::enter()
{
    if ( pservContext != NULL )
    { pservContext->enter();}
}
void struServAction::exit( DsoErr err )
{
    if ( pservContext != NULL )
    { pservContext->exit(err);}
}


QuickKey::QuickKey()
{
    mMsg = -1;
    mHead = -1;
}

void QuickKey::rst()
{
//    mHead = 0;
}

//! 键依次进入，如果最后一个键匹配，则发出事件返回 true
//!
bool QuickKey::sequenceIn( int key )
{
    Q_ASSERT( mKeys.size() > 0 );

    //! key match
    if ( mKeys[mHead] == key )
    {
        mHead++;
    }
    else
    {
        mHead = 0;
    }

    //! key match
    if ( mHead >= mKeys.size() )
    {
        mHead = 0;
        return true;
    }
    else
    {
        return false;
    }
}

void QuickKey::setKey( const QList<int> &keys,
                       int msg )
{
    Q_ASSERT( keys.size() > 1 );

    mKeys = keys;
    mMsg = msg;
    mHead = 0;
}

int QuickKey::getMsg() const
{
    return mMsg;
}
