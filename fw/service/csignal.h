#ifndef SIGNAL_H
#define SIGNAL_H

#include <QtCore>

class CSignal
{
public:
    enum sigType
    {
        unk_changed,

        value_changed,
        enable_changed,
        enable_changed_opt,
        visible_changed,
        visible_changed_opt,

        content_changed,
        context_changed,

        active_changed,
        active_in_changed,
        active_out_changed,
        focus_changed,
    };

public:
    CSignal( int servId, int msg, void *pServ, void *pContext );
    CSignal()
    {}
public:
    bool equ( CSignal &var );

public:
    bool operator==( const CSignal & var );
    bool operator!=( const CSignal & var )
    { return !( *this == var ); }

public:
    sigType mSigType;

    int mServId;
    int mMsg;

    void *m_pServ;
    void *m_pContext;
};

class Signal_value_changed : public CSignal
{
public:
    Signal_value_changed( int servId, int msg, void *pServ, void *pContext );
    Signal_value_changed()
    {}
};

class Signal_enable_changed : public CSignal
{
public:
    Signal_enable_changed( int servId, int msg, bool b,  void *pServ, void *pContext );
    Signal_enable_changed()
    {}
public:
    bool operator==( const Signal_enable_changed & var );
    bool operator!=( const Signal_enable_changed & var )
    { return !(*this == var); }

public:
    bool mbEnable;
};

class Signal_enable_changed_opt : public Signal_enable_changed
{
public:
    Signal_enable_changed_opt( int servId, int msg, int opt, bool b, void *pServ, void *pContext );
    Signal_enable_changed_opt()
    {}

public:
    bool operator==( const Signal_enable_changed_opt & var );
    bool operator!=( const Signal_enable_changed_opt & var )
    { return !(*this == var); }

public:
    int mOpt;
};

class Signal_visible_changed : public CSignal
{
public:
    Signal_visible_changed( int servId, int msg, bool b, void *pServ, void *pContext );
    Signal_visible_changed()
    {}
public:
    bool operator==( const Signal_visible_changed & var );
    bool operator!=( const Signal_visible_changed & var )
    { return !(*this == var); }

public:
    bool mbVisible;
};

class Signal_visible_changed_opt : public Signal_visible_changed
{
public:
    Signal_visible_changed_opt( int servId, int msg, int opt, bool b, void *pServ, void *pContext );
    Signal_visible_changed_opt()
    {}
public:
    bool operator==( const Signal_visible_changed_opt & var );
    bool operator!=( const Signal_visible_changed_opt & var )
    { return !(*this == var); }

public:
    int mOpt;
};

class Signal_content_changed : public CSignal
{
public:
    Signal_content_changed( int servId, int msg, void *pServ, void *pContext );
    Signal_content_changed()
    {}

};

class Signal_context_changed : public CSignal
{
public:
    Signal_context_changed( int servId, int msg, void *pServ, void *pContext );
    Signal_context_changed()
    {}
};

class Signal_active_changed : public CSignal
{
public:
    Signal_active_changed( int servId, int msg, void *pServ, void *pContext );
    Signal_active_changed()
    {}
};

class Signal_active_in : public CSignal
{
public:
    Signal_active_in( int servId, int msg, void *pServ, void *pContext );
    Signal_active_in()
    {}
};

class Signal_active_out : public CSignal
{
public:
    Signal_active_out( int servId, int msg, void *pServ, void *pContext );
    Signal_active_out()
    {}
};

class Signal_focus_changed : public CSignal
{
public:
    Signal_focus_changed( int servId, int msg, void *pServ, void *pContext );
    Signal_focus_changed()
    {}
};

class SignalQueue
{
public:
    SignalQueue();
    ~SignalQueue();

public:
    template< typename T >
    void attach( const T &sigVar )
    {
        T *pItem;

        pItem = (T*)find( sigVar );
        if ( NULL == pItem )
        {
            pItem = new T();
            if ( NULL == pItem )
            { return; }

            *pItem = sigVar;
            mSignalQueue.append( pItem );
        }
        else
        {
            mSignalQueue.removeAll(pItem);
            mSignalQueue.append( pItem );
        }
    }
//    void attach( const Signal_value_changed &sigVar );
//    void attach( const Signal_enable_changed &sigVar );
//    void attach( const Signal_enable_changed_opt &sigVar );
//    void attach( const Signal_visible_changed &sigVar );
//    void attach( const Signal_visible_changed_opt &sigVar );

//    void attach( const Signal_content_changed &sigVar );
//    void attach( const Signal_context_changed &sigVar );

//    void attach( const Signal_active_changed &sigVar );
//    void attach( const Signal_active_in &sigVar );
//    void attach( const Signal_active_out &sigVar );
//    void attach( const Signal_focus_changed &sigVar );

    void clear();

protected:
    CSignal *find( const CSignal &sigVar );

public:
    QList<CSignal*> mSignalQueue;
};

#endif // SIGNAL_H
