#include "../../engine/base/dsoengine.h"

#include "../../engine/cplatform.h"

#include "../baseclass/chelprequest.h"

#include "../../include/dsostd.h"

#include "service.h"
#include "service_msg.h"

IMPLEMENT_CMD( CExecutor, serviceExecutor )
start_of_entry()
on_set_int_int( CMD_SERVICE_ACTIVE, &serviceExecutor::setActive ),
on_get_bool   ( CMD_SERVICE_ACTIVE, &serviceExecutor::isActive ),

on_set_int_int( CMD_SERVICE_DEACTIVE, &serviceExecutor::setdeActive ),

on_set_int_int( CMD_SERVICE_ENTER_ACTIVE, &serviceExecutor::on_enterActive ),
on_set_int_int( CMD_SERVICE_EXIT_ACTIVE, &serviceExecutor::on_exitActive ),

on_set_int_void( CMD_SERVICE_ATTR_CHANGED, &serviceExecutor::on_attrChange ),

on_set_void_arg( CMD_SERVICE_PRE_DO, &serviceExecutor::on_pre_do ),
on_set_void_arg( CMD_SERVICE_POST_DO, &serviceExecutor::on_post_do ),

on_get_bool_int( CMD_SERVICE_CMD_EXIST, &serviceExecutor::queryEntryExist ),

end_of_entry()

IMPLEMENT_PRE_DO( CExecutor, serviceExecutor )
start_of_predo()
on_predo( CMD_SERVICE_ACTIVE, &serviceExecutor::on_pre_set_active ),
end_of_predo()

IMPLEMENT_POST_DO( CExecutor, serviceExecutor )
start_of_postdo()
end_of_postdo()

#define USE_EMIT    1

/*! \var _actionQueue
* 服务消息队列
*/
servActionQueue serviceExecutor::_actionQueue;
bool serviceExecutor::_actionQueueInited = false;
static int _maxQueueSize = 0;
/*! \var _actionQueueMutex
*
* 动作消息队列的互斥量
* - 消息队列在app或命令执行线程append
* - 消息队列在service线程remove
*/
QMutex serviceExecutor::_actionQueueMutex;
QMutex serviceExecutor::_signalQueueMutex;
//! sema for ui
QSemaphore serviceExecutor::_servExecSema;

/*! \var _servExecMutex
*
* - service的正常执行
* - send所引起的同步执行
*/
QMutex serviceExecutor::_servExecMutex( QMutex::Recursive );

servActionQueue serviceExecutor::_historyQueue;

/*! \var _actionExeInterval
*
* service线程基于消息队列，在队列为空时，让出CPU
*/
const int serviceExecutor::_actionExeInterval;

/*! \var _servSpyPreList
*
* 前侦听列表
*/
servSpyList serviceExecutor::_servSpyPreList;
/*! \var _servSpyPostList
*
* 后侦听列表
*/
servSpyList serviceExecutor::_servSpyPostList;

const QString serviceExecutor::_dumyServName="";

QList<struTimer*> serviceExecutor::_timerList;

QList<int> serviceExecutor::_broadcastList;
QList<int> serviceExecutor::_actionFilterList;

SignalQueue serviceExecutor::_signalQueue;

QThread *serviceExecutor::_execThread;

DsoWorkMode serviceExecutor::_workMode = work_normal;

/*
 * 执行完一条服务的消息后，是否需要立即更新APP,
 * 一般在水平档位偏移，垂直档位偏移等需要
 * 其它的尽量不需要设置
 *
 * hxh.2018.3.27
*/

bool  serviceExecutor::m_bAppUpdate = false;
/*! \var _contextScpiConsole
 *
 * 动作环境：
 * ui | user | comment
 * ---|------|--------
 * T  | T    | ui setting
 * T  | F    | invalid
 * F  | F    | internal config
 * F  | T    | remote config
 */
struActionContext serviceExecutor::_contextInit(false,false,true);
struActionContext serviceExecutor::_contextInternal(false,false,false);
struActionContext serviceExecutor::_contextUi( true, true,false );
struActionContextRmt serviceExecutor::_contextRmt;


serviceExecutor::eExeStatus serviceExecutor::_exeStat=serviceExecutor::e_unk ;

void serviceExecutor::setExeStatus( serviceExecutor::eExeStatus stat )
{
    serviceExecutor::_exeStat = stat;
}
serviceExecutor::eExeStatus serviceExecutor::getExeStatus()
{
    return serviceExecutor::_exeStat;
}

void serviceExecutor::lockExec()
{
    serviceExecutor::_servExecMutex.lock();
}
void serviceExecutor::unlockExec()
{
    serviceExecutor::_servExecMutex.unlock();
}

void serviceExecutor::pendUi()
{
//#ifndef _DEBUG
//    serviceExecutor::_servExecSema.acquire();
//#endif
}
void serviceExecutor::releaseUi()
{
//#ifndef _DEBUG
//    Q_ASSERT( serviceExecutor::_servExecSema.available() < 1 );
//    serviceExecutor::_servExecSema.release();
//#endif
}

void serviceExecutor::attachThread( QThread *pThread )
{
    Q_ASSERT( NULL != pThread );

    serviceExecutor::_execThread = pThread;
}

/*!
 * \brief serviceExecutor::post
 * \param servName
 * \param msg
 * \param para
 * \return
 * 向服务提交异步请求
 * -# 前侦听列表检查，一旦检查到登记项，则先投递消息 \todo 是否会形成循环？
 * -# 消息投递
 * -# 后侦听列表检查
 */
DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              const struServPara &para,
                              struActionContext *pContext)
{
    struServItem *pItem;
    DsoErr err, lastErr;

    pItem = service::findItem( servName );
    if ( NULL == pItem )
    {
        return ERR_SERV_NAME_INVALID;
    }

    lastErr = ERR_NONE;
    //! pre spy
    err = spyPost( pItem->servId, msg, para,
                   _servSpyPreList, pContext );
    if ( err != ERR_NONE )
    {
        lastErr = err;
    }

    //! action queue
    err = post( pItem->servId, msg, para, pContext );
    if ( err != ERR_NONE )
    {
        lastErr = err;
    }

    //! post spy
    err = spyPost( pItem->servId, msg, para,
                   _servSpyPostList, pContext );
    if ( err != ERR_NONE )
    {
        lastErr = err;
    }

    return lastErr;
}

/*!
 * \brief serviceExecutor::send
 * \param servName
 * \param msg
 * \param para
 * \return
 * 服务同步执行请求
 *
 * 和post一样也需要处理两个侦听队列 \todo loop?
 */
DsoErr serviceExecutor::send( const QString &servName,
                              int msg,
                              const struServPara &para,
                              struActionContext *pContext )
{
    struServItem *pItem;
    DsoErr err, lastErr;

//    if(servName == serv_name_hori)
//    {
//        qDebug()<<msg;
//        qDebug()<<__FUNCTION__<<__LINE__<<getDebugTime();
//    }

    pItem = service::findItem( servName );
    if ( NULL == pItem )
    {
        return ERR_SERV_NAME_INVALID;
    }

    lastErr = ERR_NONE;
    err = spySend( pItem->servId, msg, para,
                   _servSpyPreList, pContext );
    if ( err != ERR_NONE )
    {
        lastErr = err;
    }

    err = send( pItem->servId, msg, para, pContext);
    if ( err != ERR_NONE )
    {
        lastErr = err;
    }

    err = spySend( pItem->servId, msg, para,
                   _servSpyPostList, pContext );
    if ( err != ERR_NONE )
    {
        lastErr = err;
    }

    return lastErr;
}
/*! \brief query
* 查询指定服务状态
*/
DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               struServPara &para)
{
    struServItem *pItem;

    pItem = service::findItem( servName );
    if ( NULL == pItem )
    {
        return ERR_SERV_NAME_INVALID;
    }

    return query( pItem->servId, msg, para );
}

/*!
 * \brief serviceExecutor::broadcast
 * \param msg
 * \param para
 * \return
 * - 向所有服务进行广播，包括自己
 * - 广播不会检查侦听队列
 */
DsoErr serviceExecutor::broadcast( int msg,
                                   const struServPara &para,
                                   struActionContext *pContext )
{
    struServItem *pItem;
    int i;

    //! 记录广播消息
    //! 广播消息对于接收者可以不处理
    if ( !serviceExecutor::_broadcastList.contains( msg ) )
    {
        serviceExecutor::_broadcastList.append( msg );
    }

    for ( i = 0; i < service::_servList.size(); i++ )
    {
        pItem = service::_servList.at( i );
        if ( pItem != NULL && pItem->pService != NULL )
        {
            post( pItem->servId, msg, para, pContext );
        }
        else
        {
            return ERR_SERV_NULL;
        }
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::scanLau( const QString &servName,
                                 int msg,
                                 CArgument &arg )
{
    struServItem *pItem;

    pItem = service::findItem( servName );
    Q_ASSERT( NULL != pItem );

    return serviceExecutor::scanLau( msg, arg, pItem->servId );
}

bool serviceExecutor::queryActive( const QString &servName )
{
    struServItem *pItem;
    serviceExecutor *pExe;

    pItem = service::findItem( servName );
    Q_ASSERT( NULL != pItem );

    pExe = getExecutor( pItem->servId );
    Q_ASSERT( NULL != pItem );

    return pExe->isActive();
}
//! only physical key
bool serviceExecutor::keyEater(int key, int count, bool bRelease )
{
    serviceExecutor *pServ;

    //! physical key to servDso
    CArgument arg;
    arg.append( key );
    arg.append( count );
    arg.append( bRelease );
    serviceExecutor::post( E_SERVICE_ID_UTILITY,
                           CMD_SERVICE_PHY_KEY,
                           arg );

    for ( int i = 0; i < service::_activeList.size(); i++ )
    {
        pServ = dynamic_cast<serviceExecutor*>(service::_activeList[i]->pService);
        Q_ASSERT( NULL != pServ );

        if ( pServ->keyEat(key, count, bRelease ) )
        {
            return true;
        }
    }

    return false;
}

bool serviceExecutor::keyTranslate(int key,
                             QString &servName,
                             int &msg,
                             int &controlAction
                              )
{
    serviceExecutor *pServ;

    if( sysGetWorkMode() == work_help ) //bug 1.5.9 2566 qxl
    {
        if ( key ==  encode_inc(R_Pkey_WAVE_POS)
             ||  key ==  encode_dec(R_Pkey_WAVE_POS)
             ||  key ==  R_Pkey_WAVE_POS_Z )
        {
            CHelpRequest *pReq;
            pReq = R_NEW( CHelpRequest( serv_name_ref,
                                        MSG_WAVE_POS_HELP ) );
            serviceExecutor::post( (int)E_SERVICE_ID_HELP,
                                   CMD_SERVICE_HELP_REQUESST,
                                   pReq );
        }
        else if ( key ==  encode_inc(R_Pkey_WAVE_VOLT)
                  ||  key ==  encode_dec(R_Pkey_WAVE_VOLT)
                  ||  key ==  R_Pkey_WAVE_VOLT_Z )
        {
            CHelpRequest *pReq;
            pReq = R_NEW( CHelpRequest( serv_name_ref,
                                        MSG_WAVE_SCALE_HELP ) );
            serviceExecutor::post( (int)E_SERVICE_ID_HELP,
                                   CMD_SERVICE_HELP_REQUESST,
                                   pReq );
        }
    }

    for ( int i = 0; i < service::_activeList.size(); i++ )
    {
        pServ = dynamic_cast<serviceExecutor*>(service::_activeList[i]->pService);
        Q_ASSERT( NULL != pServ );

        if ( pServ->keyTranslate(key, msg, controlAction ) )
        {
            servName = pServ->getName();
            return true;
        }
    }

    return false;
}

bool serviceExecutor::quickKeyTranslate( int key, int intervalTime )
{
    serviceExecutor *pServ;

    //! timeout xx ms
    if ( intervalTime > 2500 )
    {
        for ( int i = 0; i < service::_servList.size(); i++ )
        {
            pServ = dynamic_cast<serviceExecutor*>(service::_servList[i]->pService);
            Q_ASSERT( NULL != pServ );

            pServ->rstQuickKey();
        }
    }

    //! compound quick key detect
    for ( int i = 0; i < service::_servList.size(); i++ )
    {
        pServ = dynamic_cast<serviceExecutor*>(service::_servList[i]->pService);
        Q_ASSERT( NULL != pServ );

        if ( pServ->quickKeyTranslate( key ) )
        {
            return true;
        }
    }

    return false;
}

bool serviceExecutor::checkRepeat(const int id,
                                  const int msg)
{

    serviceExecutor::_actionQueueMutex.lock();

    bool bCompany = false;
    QListIterator<struServAction> actionIter(serviceExecutor::_actionQueue);
    while(actionIter.hasNext())
    {
        if(actionIter.next().isCompany( id,msg ))
        {
            bCompany = true;
            break;
        }
    }

    serviceExecutor::_actionQueueMutex.unlock();

    return bCompany;
}

//确保消息队列中只有一个该消息
DsoErr serviceExecutor::postSingle( const int id,
                                    const int msg,
                                    const int val,
                                    struActionContext *pContext )
{
    if( !checkRepeat(id, msg) )
    {
        struServPara para;

        para.setVal( val );

        return post(  id, msg, para, pContext );
    }
    return ERR_NONE;
}





DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              const bool val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              const int val,
                              struActionContext *pContext)
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              const QString &val,
                              struActionContext *pContext)
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              pointer val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              const qlonglong val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              CObj *val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              DsoReal &val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::post( const QString &servName,
                              int msg,
                              dsoReal &val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return post(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::send( const QString &servName, int msg,
                              const bool val,
                              struActionContext *pContext)
{
    struServPara para;

    para.setVal( val );

    return send(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::send( const QString &servName, int msg,
                              const int val,
                              struActionContext *pContext)
{
    struServPara para;

    para.setVal( val );

    return send(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::send( const QString &servName, int msg,
                              const QString &val,
                              struActionContext *pContext)
{
    struServPara para;

    para.setVal( val );

    return send( servName, msg, para, pContext );
}

DsoErr serviceExecutor::send( const QString &servName,
                              int msg,
                              pointer val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return send(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::send( const QString &servName,
                              int msg,
                              const qlonglong val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return send(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::send( const QString &servName,
                              int msg,
                              DsoReal &val,
                              struActionContext *pContext )
{
    struServPara para;

    para.setVal( val );

    return send(  servName, msg, para, pContext );
}

DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               bool &val )
{
    struServPara para;
    DsoErr err;

    err = query( servName, msg, para );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( para.getVal( val ) != ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;

}
DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               int &val )
{
    struServPara para;
    DsoErr err;

    err = query( servName, msg, para );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( para.getVal( val ) !=  ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               float &val )
{
    struServPara para;
    DsoErr err;

    err = query( servName, msg, para );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( para.getVal( val ) != ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               QString &val )
{
    struServPara para;
    DsoErr err;

    err = query( servName, msg, para );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( para.getVal( val ) != ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               qlonglong &val )
{
    struServPara para;
    DsoErr err;

    err = query( servName, msg, para );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( para.getVal( val ) != ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               pointer &val )
{
    struServPara para;
    struServPara paraOut;
    DsoErr err;

    para.setVal( val );
    err = query( servName, msg, para, paraOut );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( paraOut.getVal( val ) != ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::query( const QString &servName,
                               int msg,
                               DsoReal &val )
{
    struServPara para;
    DsoErr err;

    err = query( servName, msg, para );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( para.getVal( val ) != ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::query( const QString &servName,
                     int msg,
                     CArgument &valIn,
                     CArgument &valOut )
{
    struServItem *pItem;

    pItem = service::findItem( servName );
    if ( NULL == pItem )
    {
        Q_ASSERT( false );
        return ERR_SERV_NAME_INVALID;
    }

    return query( pItem->servId, msg, valIn, valOut );
}

DsoErr serviceExecutor::post( const int eId,
                              int msg,
                              const int val,
                              struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, val, pContext );
}

DsoErr serviceExecutor::post( const int eId,
                              int msg,
                              const bool val,
                              struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, val, pContext );
}

DsoErr serviceExecutor::post( const int eId,
                              int msg,
                              const QString &val,
                              struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, val, pContext );
}

DsoErr serviceExecutor::post( const int eId,
                              int msg,
                              const qlonglong val,
                              struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, val, pContext );
}

DsoErr serviceExecutor::post( const int eId,
                              int msg,
                              DsoReal &val,
                              struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, val, pContext );
}
//DsoErr serviceExecutor::post( const int eId,
//                              int msg,
//                              const dsoReal val,
//                              struActionContext *pContext )
//{
//    QString str;

//    str = getServiceName( eId );

//    return post( str, msg, val, pContext );
//}
DsoErr serviceExecutor::post( const int eId,
                              int msg,
                              pointer val,
                              struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, val, pContext );
}

DsoErr  serviceExecutor::post( const int eId,
                               int msg,
                               CArgument &arg,
                               struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, arg, pContext );
}

DsoErr  serviceExecutor::post( const int eId,
                               int msg,
                               CObj *val,
                               struActionContext *pContext )
{
    QString str;

    str = getServiceName( eId );

    return post( str, msg, val, pContext );
}

DsoErr serviceExecutor::query( const int eId,
                               int msg,
                               QString &val )
{
    struServPara para;
    DsoErr err;

    err = query( eId, msg, para );
    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    if ( para.getVal( val ) != ERR_NONE )
    {
        return ERR_MSG_TYPE_MISMATCH;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::broadcast( int msg, int val )
{
    struServPara para;

    para.setVal( val );

    return broadcast( msg, para );
}

ui_attr::uiProperty * serviceExecutor::queryUiProperty(
                                              const QString &servName,
                                              int msg )
{
    struServItem *pItem;
    serviceExecutor *pExe;

    pItem = service::findItem( servName );
    if ( NULL == pItem )
    {
        return NULL;
    }

    pExe = getExecutor( pItem->servId );

    return pExe->getProperty(msg);
}

/*! \brief spyOn
*
* 注册侦听消息
* \param servName   被侦听服务
* \param msg        被侦听服务的消息
* \param spyName    侦听的服务
* \param aliasMag   侦听服务重命名的消息
* \param spyType    侦听类型
* \sa enumSpyType
* \todo 判定重复spy项
*/
int serviceExecutor::spyOn( const QString &servName, int msg,
                         const QString &spyName, int aliasMsg,
                         eSpyType spyType,
                         eSpyPara spyPara )
{

    struServItem *pItemServ, *pItemSpy;

    //! find item
    pItemServ = service::findItem( servName );
    pItemSpy = service::findItem( spyName );

    if ( pItemServ == NULL || pItemSpy == NULL )
    {
        return -1;
    }

    struSpyItem *spyItem = R_NEW( struSpyItem( pItemServ->servId, msg,
                           pItemSpy->servId, aliasMsg,
                           spyType, spyPara
                          ) );
    Q_ASSERT( NULL != spyItem );

    //! append
    if ( is_attr( spyType, e_spy_post ) )
    {
        _servSpyPostList.append( spyItem );
    }
    else if ( is_attr( spyType,e_spy_pre) )
    {
        _servSpyPreList.append( spyItem );
    }
    else
    {
        return -1;
    }

    return spyItem->spyId;
}

int serviceExecutor::spyOn( int id, int msg,
                     int selfId, int aliasMsg,
                     eSpyType spyType,
                     eSpyPara spyPara )
{
    QString name1, name2;

    name1 = service::getServiceName( id );
    name2 = service::getServiceName( selfId );

    return spyOn( name1, msg, name2, aliasMsg, spyType, spyPara );
}

DsoErr serviceExecutor::spyOff( const QString &servName, int msg,
                         const QString &spyName,
                         eSpyType spyType,
                         eSpyPara spyPara )
{
    int idSpy, idDst;

    idSpy = service::getServiceId( servName );
    idDst = service::getServiceId( spyName );
    return spyOff( idSpy, msg, idDst, spyType, spyPara );
}

DsoErr serviceExecutor::spyOff( int id, int msg,
                     int selfId,
                     eSpyType spyType,
                     eSpyPara spyPara)
{
    struSpyItem *pItem;

    //! remove
    if ( is_attr( spyType,e_spy_post ) )
    {
        pItem = findSpyItem( id, msg, selfId,
                             spyType, spyPara,
                             _servSpyPostList );
        if ( NULL == pItem )
        {   return ERR_INVALID_INPUT;   }

        _servSpyPostList.removeOne( pItem );
        delete pItem;

        return ERR_NONE;
    }
    else if ( is_attr( spyType,e_spy_pre ) )
    {
        pItem = findSpyItem( id, msg, selfId,
                             spyType, spyPara,
                             _servSpyPreList );
        if ( NULL == pItem )
        {   return ERR_INVALID_INPUT;   }

        _servSpyPreList.removeOne( pItem );
        delete pItem;

        return ERR_NONE;
    }
    else
    {
        return ERR_INVALID_INPUT;
    }
}

/*!
 * \brief serviceExecutor::exec
 *
 * 执行服务线程的消息队列
 * \todo exec err return
 */
DsoErr serviceExecutor::exec()
{
    serviceExecutor::setExeStatus( serviceExecutor::e_init );
    int uiRefTime = 0; //! ms

    //! build platform
    cplatform *platform;
    platform = cplatform::createInstance();
    Q_ASSERT( NULL != platform );
    platform->buildPlatform();
    platform->initPlatform();

    //! set rmt sema
    //! the sema is in stack while running
    QSemaphore rmtSema;
    serviceExecutor::_contextRmt.m_pSema = &rmtSema;

    serviceExecutor::setExeStatus( serviceExecutor::e_loop );

    while ( true )
    {
        //! a few msgs
        doQueue();

        //! ui refresh
        if ( uiRefTime <= 0 )
        {
            //! emit signal
            procSignalQueue();

            uiRefTime = 30;
        }
        else
        {
            uiRefTime -= _actionExeInterval;
        }

        //! wait a few time
        QThread::msleep( _actionExeInterval );
    }

    delete platform;

    serviceExecutor::setExeStatus( serviceExecutor::e_exit );

    return ERR_NONE;
}

void testOutput(QString str)
{
    QFile memInfo("/proc/meminfo");
    memInfo.open(QIODevice::ReadOnly);

    QTextStream txtStream(&memInfo);

    QString mem = txtStream.readLine();

    mem = mem.replace("         "," ");
    qDebug() << txtStream.readLine() << str ;

    memInfo.close();
}

void serviceExecutor::doQueue()
{
    serviceExecutor *pExecutor;
    DsoErr err = ERR_NONE;

    struServAction action;

    //! a few msgs
    while( !_actionQueue.isEmpty() )
    {
        //! get the action
        serviceExecutor::_actionQueueMutex.lock();
        action = _actionQueue.dequeue();
        serviceExecutor::_actionQueueMutex.unlock();

        //! action enter
        action.enter();

        //! filter
        if ( serviceExecutor::actionBypass( action ) )
        {
            //! error notify
            if(work_help == sysGetWorkMode())
            {
                return;
            }

            //except user define msg. by hxh on 2018.1.28
            if( action.servMsg > 255 )
            {
                serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                                       CMD_SERVICE_DO_ERR,
                                       (int)ERR_ACTION_DISABLED
                                       );

                action.exit( ERR_ACTION_DISABLED );
            }
            continue;
        }

        //! do action
        {
            pExecutor = getExecutor( action.servId );
            if ( NULL == pExecutor )
            {
                LOG_DBG()<<action.servId<<action.servMsg;
                Q_ASSERT( NULL != pExecutor );
            }

            /*****************************************************
             * Modified by hxh on 2017.12.22
             *
             * Just show the menu if it hidden and the corresponding
             * service is active when received CMD_SERIVCE_ACTIVE
             * ***************************************************/
            if(action.servMsg == CMD_SERVICE_ACTIVE &&
               pExecutor->isActive() &&
               sysGetMenuWindow()->isHidden())
            {
                    serviceExecutor::_signalQueueMutex.lock();
                    serviceExecutor::_signalQueue.attach(Signal_active_in(
                                                             action.servId,
                                                             action.servMsg,
                                                             pExecutor,
                                                             NULL ));
                    serviceExecutor::_signalQueueMutex.unlock();
            }
            else
            {
                pExecutor->enterContext( action );

//                if( action.servMsg == MSG_APP_UTILITY_CHECK_MORE_MENU  )
//                {
//                    qDebug() << "stop here"    ;
//                }

                /*if( action.servMsg == CMD_SERVICE_STARTUP ||
                    action.servMsg == CMD_SERVICE_RST )
                {
                    QString str = "before " + pExecutor->getName() ;

                    // testOutput( str );
                    qDebug() << str << ":" <<  QTime::currentTime();
                }*/

                err = pExecutor->doSet( action );

                /*if( action.servMsg == CMD_SERVICE_STARTUP ||
                    action.servMsg == CMD_SERVICE_RST )
                {
                    QString str = "after " + pExecutor->getName() ;

                    // testOutput( str );
                    qDebug() << str;
                }*/

                pExecutor->exitContext( action );
            }

            action.exit( err );

            //! 非系统消息or user define msg 而且改变了设置
            if ( is_ui_msg( action.servMsg ) &&
                 !pExecutor->isNotify( action.servMsg )  )
            {
                ISerial *pSerial;
                pSerial = dynamic_cast<ISerial*>(pExecutor);
                if ( NULL != pSerial )
                {
                    serviceExecutor::post( E_SERVICE_ID_DSO,
                                           CMD_SERVICE_MODIFIED,
                                           (int)pExecutor->mpItem->servId );

                    /*if(sysGetStarted())
                    {
                        qDebug() << "TrigSaveNvRam:serv=" << action.servId
                                 << "msg:" << action.servMsg;
                    }*/
                }
            }

            //! 执行错误
            if ( err != ERR_NONE )
            {
                //! 通知类消息执行错误，匹配错误，不进行提示
                //! 通知类消息不是必须要处理的
                if ( serviceExecutor::isNotifyMsg( action.servMsg ) )
                {
                    if ( err == ERR_TARGET_MIS_MATCH )
                    {
                        break;
                    }
                }

                LOG_DBG()<<pExecutor->getName()<<action.servMsg<<err;

                //! error notify
                serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
                                       CMD_SERVICE_DO_ERR,
                                       (int)err );
            }
        }
    }
}

//! called in service thread
void serviceExecutor::flushQueue()
{
    serviceExecutor::unlockExec();

    serviceExecutor::doQueue();

    serviceExecutor::lockExec();
}

bool serviceExecutor::isIdle()
{
    return _actionQueue.isEmpty();
}

//! < 0 wait inf
//! false -- timeout
bool serviceExecutor::waitIdle( int ms )
{
    while( !serviceExecutor::isIdle()  )
    {
        QThread::msleep(10);

        //! timeout
        if ( ms >= 0  )
        {
            ms -= 100;
            if ( ms <= 0 )
            { return false; }
            else
            {}
        }
        //! inf
        else
        {}
    }

    return true;
}

/*!
 * \brief serviceExecutor::qobject
 * \param servName
 * \return
 * 返回指定服务的QObject,用于signal/slot处理
 */
QObject *serviceExecutor::qobject( const QString &servName )
{
    struServItem *pItemServ;

    pItemServ = service::findItem( servName );

    return dynamic_cast<QObject*>(pItemServ->pService);
}

bool serviceExecutor::actionBypass( const struServAction &action )
{
    int servId;
    serviceExecutor *pExe;
    foreach( servId, serviceExecutor::_actionFilterList )
    {
        pExe = getExecutor( servId );
        Q_ASSERT( pExe != NULL );

        if ( pExe->actionFilter( action ) )
        {
            return true;
        }
    }

    return false;
}

void serviceExecutor::tickIt( int tickms )
{
    serviceExecutor::timerTick( tickms );
}

/*!
 * \brief serviceExecutor::timerTick
 * \param ticks
 * 时钟tick
 */
void serviceExecutor::timerTick( int tickms )
{
    struTimer *pTimer;
    QList<struTimer*> gcList;

    Q_ASSERT( tickms > 0 );

    foreach( pTimer, _timerList )
    {
        Q_ASSERT( pTimer != NULL );

        //! time stoped
        if ( pTimer->timeOut == 0 )
        {
        }
        else if ( pTimer->ticks > tickms )
        {
            pTimer->ticks -= tickms;
        }
        else
        {
            //! timeout
            pTimer->ticks = pTimer->timeOut;
            post( pTimer->servId, CMD_SERVICE_TIMEOUT, pTimer->timeId );

            if ( pTimer->mAttr == timer_single )
            {
                gcList.append( pTimer );
            }
        }
    }

    //! single timer
    foreach( pTimer, gcList )
    {
        _timerList.removeOne( pTimer );
        delete pTimer;
    }
}

/*!
 * \brief serviceExecutor::post
 * \param id
 * \param msg
 * \param para
 * \param pContext
 * \return
 * 服务消息请求
 * \todo spy list proc
 */
DsoErr serviceExecutor::post( int id, int msg,
                              const struServPara &para,
                              struActionContext *pContext)
{
    struServAction anAction( id, msg, para, pContext );

     serviceExecutor::_actionQueueMutex.lock();
     serviceExecutor::_actionQueue.enqueue( anAction );

     //! log max size
     if ( serviceExecutor::_actionQueue.size() > _maxQueueSize )
     {
         _maxQueueSize = serviceExecutor::_actionQueue.size();
         //qDebug()<<__FUNCTION__<<__LINE__<<_maxQueueSize;
     }

     serviceExecutor::logInHistory( anAction );

     serviceExecutor::_actionQueueMutex.unlock();


    return ERR_NONE;
}
/*!
 * \brief serviceExecutor::send
 * \param id
 * \param msg
 * \param para
 * \param pContext
 * \return
 * internal used by service id
 * 立即执行服务请求
 * \sa exec
 * \todo spy list proc
 */
DsoErr serviceExecutor::send( int id, int msg,
                              const struServPara &para,
                              struActionContext *pContext )
{
    struServAction action( id, msg, para, pContext );
    DsoErr err;

    err = exec( action );

    return err;
}

DsoErr serviceExecutor::query( int id, int msg, struServPara &para )
{
    serviceExecutor *pExecutor;
    DsoErr err;

    pExecutor = serviceExecutor::getExecutor( id );
    if ( NULL == pExecutor )
    {
        return ERR_NO_EXECUTOR;
    }

    err = pExecutor->query( msg, para );

    return err;
}

DsoErr serviceExecutor::query( int id, int msg,
                     struServPara &para,
                     CArgument &paraOut )
{
    serviceExecutor *pExecutor;
    DsoErr err;

    pExecutor = serviceExecutor::getExecutor( id );
    if ( NULL == pExecutor )
    {
        Q_ASSERT( false );
        return ERR_NO_EXECUTOR;
    }

    err = pExecutor->query( msg, para, paraOut );

    return err;
}

/*! \brief spyPost
*
* 侦听消息投递，当订阅的消息：
* - 目标服务
* - 消息值
* 匹配时，向侦听者推送侦听消息。
* - 预侦听
* - 后侦听
* - 侦听到的参数是spyid 或 目标参数，使用spyId是为了应对不关注目标参数的情况，但需要根据spy
*   进行区分动作
*/
DsoErr serviceExecutor::spyPost( int id, int msg,
                                 const struServPara &para,
                                 servSpyList &spyList,
                                 struActionContext *pContext )
{
    int i;
    struSpyItem *pSpyItem;
    DsoErr err;
    serviceExecutor *pExe;
    ui_attr::uiAttr *pAttr;
    ui_attr::uiProperty *pProp;

    for ( i = 0; i < spyList.size(); i++ )
    {
        pSpyItem = spyList.at(i);
        if ( pSpyItem != NULL
             && pSpyItem->servId == id
             && pSpyItem->msg == msg )
        {
            //! context match?
            if ( NULL != pContext )
            {
                //! only ui info
                if ( is_attr(pSpyItem->spyType, e_spy_user ) )
                {
                    if ( pContext->isUserAction() )
                    {  }
                    else
                    { continue; }
                }
                else
                {}
            }

            //! check enable
            pExe = getExecutor( pSpyItem->servId );
            if ( NULL == pExe )
            { continue; }
            pAttr = pExe->getAttr();
            if ( NULL == pAttr )
            { continue; }

            pProp = pAttr->getItem(msg);
            if ( NULL != pProp )
            {
                if ( pProp->getEnable() )
                {}
                else
                { continue; }
            }

            //! normal spy
            //! context no cascade
            if ( pSpyItem->spyPara==spy_para )
            {
                err = post( pSpyItem->dstId,
                            pSpyItem->aliasMsg,
                            para,
                            NULL );
            }
            else if ( pSpyItem->spyPara == spy_id )
            {
                err = post( pSpyItem->dstId,
                            pSpyItem->aliasMsg,
                            pSpyItem->spyId,
                            NULL );
            }
            else if ( pSpyItem->spyPara == spy_serv_id )
            {
                err = post( pSpyItem->dstId,
                            pSpyItem->aliasMsg,
                            pSpyItem->servId,
                            NULL );
            }
            else
            { err = ERR_NONE; }

            if ( err != ERR_NONE )
            {
                return err;
            }
        }
    }

    return ERR_NONE;
}

/*! \brief spySend
*
* 侦听消息的立即执行
* \note 侦听消息需要防止循环出现
*/
DsoErr serviceExecutor::spySend( int id, int msg,
                                 const struServPara &para,
                                 servSpyList &spyList,
                                 struActionContext * )
{
    int i;
    struSpyItem *pSpyItem;
    DsoErr err, lastErr;

    lastErr = ERR_NONE;
    for ( i = 0; i < spyList.size(); i++ )
    {
        pSpyItem = spyList.at(i);
        if ( pSpyItem != NULL
             && pSpyItem->servId == id
             && pSpyItem->msg == msg )
        {
            err = send( pSpyItem->dstId, pSpyItem->aliasMsg, para );
            if ( err != ERR_NONE )
            {
                lastErr = err;
            }
        }
    }

    return lastErr;
}

/*!
 * \brief serviceExecutor::exec
 * \param action
 * \return
 *
 * 执行动作，被以下调用：
 * - 消息队列循环
 * - send
 * 为了互斥，使用了信号量 _servExecMutex
 * \sa _servExecMutex
 */
DsoErr serviceExecutor::exec( const struServAction &action )
{
    serviceExecutor *pExecutor;
    DsoErr err;

    pExecutor = getExecutor( action.servId );
    if ( NULL == pExecutor )
    {
        qWarning()<<action.servId<<"no executor";
        return ERR_NO_EXECUTOR;
    }
    //! enter context
    if ( action.pservContext != NULL )
    {
        action.pservContext->enter();
    }

    //! do action
    err = pExecutor->doSet( action );

    //! exit context
    if ( action.pservContext != NULL )
    {
        action.pservContext->exit( err );
    }

    return err;
}

/*!
 * \brief serviceExecutor::getExecutor
 * \param id
 * \return
 * 通过服务ID得到服务执行体
 */
serviceExecutor * serviceExecutor::getExecutor( int id )
{
    struServItem *pItem = service::findItem(id);

    if ( NULL == pItem || pItem->pService == NULL )
    {
        LOG_DBG()<<id;
        return NULL;
    }

    return dynamic_cast<serviceExecutor *>(pItem->pService);
}

serviceExecutor * serviceExecutor::getExecutor( const QString &name )
{
    service *pServ;

    pServ = findService( name );

    if ( NULL == pServ )
    { return NULL; }

    return dynamic_cast<serviceExecutor*>(pServ);
}

struSpyItem *serviceExecutor::findSpyItem( int &id,
                                 int msg,
                                 int &dstId,
                                 eSpyType spyType,
                                 eSpyPara spyPara,
                                 servSpyList &list
                                 )
{
    struSpyItem *pItem;

    foreach (pItem, list)
    {
        Q_ASSERT( NULL != pItem );

        if ( pItem->msg == msg
             && pItem->servId == id
             && pItem->dstId == dstId
             && pItem->spyPara == spyPara
             && pItem->spyType == spyType )
        {
            return pItem;
        }
    }

    return NULL;
}

servActionQueue &serviceExecutor::history()
{
    return serviceExecutor::_historyQueue;
}

void serviceExecutor::clearHistory()
{
    serviceExecutor::_actionQueueMutex.lock();

    serviceExecutor::_historyQueue.clear();

    serviceExecutor::_actionQueueMutex.unlock();
}

void serviceExecutor::logInHistory( struServAction &anAction )
{
    //! into history
    if ( serviceExecutor::_historyQueue.size()
         >= serviceExecutor::_historyLength )
    { serviceExecutor::_historyQueue.dequeue(); }
    else
    { }

    //! history filter
    if ( !serviceExecutor::historyFilter( anAction) )
    {
        serviceExecutor::_historyQueue.enqueue( anAction );
    }
}

bool serviceExecutor::historyFilter( struServAction &anAction )
{
    //! notify msg -- filter
    if ( isNotifyMsg( anAction.servMsg) ) return true;

    //! app msg
    if ( anAction.servMsg < 256 ) return true;

    //! user msg
    if ( anAction.servMsg < MENU_MSG_BASE ) return true;

    //! empty -- not filter
    if ( serviceExecutor::_historyQueue.isEmpty() ) return false;

    struServAction act;

    act = serviceExecutor::_historyQueue.last();
    if ( act.isCompany( anAction) ) return true;

    return false;
}

//! system msg
//! broadcast msg
bool serviceExecutor::isNotifyMsg( int msg )
{
    if ( is_system_msg(msg) )
    { return true; }

    if ( serviceExecutor::_broadcastList.contains(msg) )
    { return true; }

    return false;
}

serviceExecutor::serviceExecutor( QString name, int eId ) :
                                  service( name, eId )
{
    if ( !_actionQueueInited )
    {
//        _actionQueueInited = true;
//        serviceExecutor::_actionQueue.reserve( 1024 );
    }

    mEnable = true;
    mDisableCause = ERR_NONE;
    mbUiCache = false;
}

serviceExecutor::~serviceExecutor()
{
    QuickKey *pQuickKey;

    foreach( pQuickKey, mQuickKeyList )
    {
        Q_ASSERT( NULL != pQuickKey );

        delete pQuickKey;
    }

    //! \todo the link change msg

    //! the stack ens
    QMapIterator<int, UiEnMap*> i(mUiEnStack);
    while (i.hasNext())
    {
        i.next();

        delete i.value();
    }
}

void serviceExecutor::registerSpy()
{}

void serviceExecutor::baseInit()
{
    collectUiMsg();
}

void serviceExecutor::enterContext( struServAction &action )
{
    if ( NULL == action.pservContext )
    { return; }

    if ( action.pservContext->isUiAction() )
    {}
    else
    {
        if ( action.pservContext->isUserAction() )
        {
#if USE_EMIT
//            emit sig_context_changed( action.servMsg,
//                                    this,
//                                    action.pservContext);
            serviceExecutor::_signalQueueMutex.lock();
            _signalQueue.attach( Signal_context_changed(
                                  getId(),
                                  action.servMsg,
                                  this,
                                  action.pservContext ) );
            serviceExecutor::_signalQueueMutex.unlock();
#endif
        }
    }
}

void serviceExecutor::exitContext( struServAction &/*action*/ )
{}

//! find the latest para
DsoErr serviceExecutor::scanLau( int msg, CArgument &arg, int servId )
{
    serviceExecutor::_actionQueueMutex.lock();

    QListIterator< struServAction > iter( serviceExecutor::_actionQueue );
    struServAction item;
    iter.toBack();
    while( iter.hasPrevious() )
    {
        item = iter.previous();
        if ( item.servId == servId && item.servMsg == msg )
        {
            serviceExecutor::_actionQueueMutex.unlock();

            arg = item.servPara;

            return ERR_NONE;
        }
    }

    serviceExecutor::_actionQueueMutex.unlock();

    return ERR_NO_EXECUTOR;
}

DsoErr serviceExecutor::scanLau( int msg, qlonglong &arg, int servId )
{
    CArgument argOut;

    //! do not find
    if ( scanLau( msg, argOut, servId ) != ERR_NONE )
    { return ERR_NO_EXECUTOR; }

    //! type match
    CArgument match;
    match.append( arg );
    if ( !argOut.isLike( match ) )
    { return ERR_NO_EXECUTOR; }

    //! extract
    if ( argOut.getVal( arg ) != ERR_NONE )
    { return ERR_NO_EXECUTOR; }

    return ERR_NONE;
}

void serviceExecutor::procSignalQueue()
{
    QListIterator<CSignal*> iter( serviceExecutor::_signalQueue.mSignalQueue );

    CSignal *pItem;
    serviceExecutor *pExe;

    serviceExecutor::_signalQueueMutex.lock();
    while( iter.hasNext() )
    {
        pItem = iter.next();
        Q_ASSERT( NULL != pItem );

        pExe = getExecutor( pItem->mServId );
        Q_ASSERT( NULL != pExe );


        pExe->doSignal( pItem );
    }    
    //! clear + gc

    serviceExecutor::_signalQueue.clear();
    serviceExecutor::_signalQueueMutex.unlock();
}

#define realSignal( type )  (static_cast<type*>(pSignal))
void serviceExecutor::doSignal( CSignal *pSignal )
{
    Q_ASSERT( NULL != pSignal );

    switch( pSignal->mSigType )
    {
        case CSignal::value_changed:
            emit sig_value_changed( pSignal->mMsg,
                                    pSignal->m_pServ,
                                    pSignal->m_pContext );
            break;

        case CSignal::enable_changed:
            emit sig_enable_changed( pSignal->mMsg,
                                     realSignal(Signal_enable_changed)->mbEnable,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::enable_changed_opt:
            emit sig_enable_changed( pSignal->mMsg,
                                     realSignal(Signal_enable_changed_opt)->mOpt,
                                     realSignal(Signal_enable_changed_opt)->mbEnable,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::visible_changed:
            emit sig_visible_changed( pSignal->mMsg,
                                     realSignal(Signal_visible_changed)->mbVisible,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::visible_changed_opt:
            emit sig_visible_changed( pSignal->mMsg,
                                     realSignal(Signal_visible_changed_opt)->mOpt,
                                     realSignal(Signal_visible_changed_opt)->mbVisible,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::content_changed:
            emit sig_content_changed( pSignal->mMsg,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::context_changed:
            emit sig_context_changed( pSignal->mMsg,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::active_changed:
            emit sig_active_changed( pSignal->mMsg,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::active_in_changed:
            emit sig_active_in( pSignal->mMsg,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::active_out_changed:
            emit sig_active_out( pSignal->mMsg,
                                     pSignal->m_pServ,
                                     pSignal->m_pContext
                                     );
            break;

        case CSignal::focus_changed:
            emit sig_focus_changed( pSignal->mMsg );
            break;

        default:
            Q_ASSERT( false );
    }
}

void serviceExecutor::linkChange( int keyMsg, int l1,
                                  int l2, int l3, int l4,
                                  int l5, int l6, int l7, int l8 )
{
    struCmdEntry *pEntry;

    pEntry = (struCmdEntry *)getEntry( keyMsg, enum_w );
    if ( NULL == pEntry )
    { return; }

    //! build the list
    if ( pEntry->pChangeList != NULL )
    {}
    else
    {
        pEntry->pChangeList = R_NEW( QList<int>() );
        Q_ASSERT( NULL != pEntry->pChangeList );
    }

    //! msgs
    int links[8]={ l1, l2, l3, l4,
                   l5, l6, l7, l8 };

    for ( int i = 0; i < array_count(links) && links[i]!=0; i++ )
    {
        //! attach the link msg
        if ( pEntry->pChangeList->contains(links[i]) )
        {}
        else
        {
            pEntry->pChangeList->append( links[i] );
        }
    }
}

void serviceExecutor::emitChanges( const struCmdEntry *pEntry,
                                   struActionContext *pActionContext )
{
    Q_ASSERT( pEntry != NULL );

    #if USE_EMIT
    #else
        return;
    #endif

    //! key msg changed
//    emit sig_value_changed( pEntry->cmd, this, pActionContext );
    serviceExecutor::_signalQueueMutex.lock();
        serviceExecutor::_signalQueue.attach(
                Signal_value_changed( getId(),
                                      pEntry->cmd,
                                      this,
                                      pActionContext )
                );
    serviceExecutor::_signalQueueMutex.unlock();
    //! for the link msgs
    if ( NULL != pEntry->pChangeList )
    {
        foreach( int linkMsg, *pEntry->pChangeList )
        {
//            emit sig_value_changed( linkMsg, this, pActionContext );
            serviceExecutor::_signalQueueMutex.lock();
            serviceExecutor::_signalQueue.attach(
                        Signal_value_changed( getId(),
                                              linkMsg,
                                              this,
                                              pActionContext )
                        );
            serviceExecutor::_signalQueueMutex.unlock();
        }
    }
}

/*!
 * \brief serviceExecutor::filter
 * \param msg
 * \param curPara
 * \param dstPara
 * \return
 * 消息过滤用于避免重复设置，如果当前状态已经是目标状态，则不再进行设置
 * - 用于消除反向更新UI所引起的配置过程
 * - 优化程序的执行过程
 * \todo no para?
 */
bool serviceExecutor::filter(  struServAction &action,
                      struServPara &curPara )
{
    if ( action.pservContext != NULL && !action.pservContext->isUserAction() )
    {
        return false;
    }

    if ( curPara == action.servPara )
    {
        return true;
    }
    else
    {
        return false;
    }
}

void serviceExecutor::installActionFilter()
{
    int index;

    //! find index
    index = serviceExecutor::_actionFilterList.indexOf( mpItem->servId );

    //! top now
    if ( index == 0 )
    {
        qWarning()<<"!!!filter at top now";
    }
    //! move to first
    else if ( index > 0 )
    {
        serviceExecutor::_actionFilterList.move( index, 0 );
    }
    //! insert to first
    else
    {
        serviceExecutor::_actionFilterList.prepend( mpItem->servId );
    }
}
void serviceExecutor::uninstallActionFilter()
{
    int index;

    //! find index
    index = serviceExecutor::_actionFilterList.indexOf( mpItem->servId );
    if ( index >= 0 )
    {
        serviceExecutor::_actionFilterList.removeAll( mpItem->servId );
    }
    else
    {}
}

/*!
 * \brief serviceExecutor::updateUi
 * \return
 * 在UI不一致是更新UI
 * - 通过远程命令设置时，需要更新UI
 * - 通过UI设置，但范围被限定后，需要更新UI
 * \todo optimize the ui
 */
DsoErr serviceExecutor::updateUi( const struServAction &action,
                                  struServPara &/*prePara*/ )
{   
    const struCmdEntry *pEntry;

    //! no ui
    if ( isConsole(action.servMsg) )
    {
        return ERR_NONE;
    }

    //BY HXH. TICK willnot generate signal
    if (  action.servMsg == CMD_SERVICE_TICK ||
          action.servMsg == CMD_ENGINE_TICK)
    {
        return ERR_NONE;
    }

    //! get entry write
    //! the stub is attached to the 'set' item
    pEntry = getEntry( action.servMsg, enum_w );
    if ( NULL == pEntry )
    {
        serviceExecutor::_signalQueueMutex.lock();
        serviceExecutor::_signalQueue.attach(
                    Signal_value_changed( getId(),
                                          action.servMsg,
                                          this,
                                          action.pservContext )
                    );
        serviceExecutor::_signalQueueMutex.unlock();
        return ERR_NONE;
    }
    else
    {
        emitChanges( pEntry, action.pservContext );
        return ERR_NONE;
    }
}

DsoErr serviceExecutor::updateAllUi()
{
#if 1
    //! get all map
    QList< struCmdEntry *> entryMap;
    getEntries( entryMap );

    //! for each map
    const struCmdEntry *pCmdEntry;
    foreach( struCmdEntry *pMapEntry, entryMap )
    {
        pCmdEntry = pMapEntry;

        while( pCmdEntry != NULL && pCmdEntry->cmd != 0 )
        {
            updateEntry( pCmdEntry );

            pCmdEntry++;
        }
    }
#endif

    mbUiCache = false;
    return ERR_NONE;
}

DsoErr serviceExecutor::updateAllUi( bool /*bFlush*/ )
{
    updateAllUi();
    return ERR_NONE;
}

DsoErr serviceExecutor::updateEntry( const struCmdEntry *pEntry )
{
    Q_ASSERT( pEntry != NULL );

    //! update value
    ui_attr::uiProperty *prop;
    do
    {
        if ( pEntry->wr != enum_w )
        {
            break;
        }
        if ( isConsole( pEntry->cmd) )
        {
            break;
        }

        if ( pEntry->cmd == CMD_SERVICE_TICK ||
             pEntry->cmd == CMD_ENGINE_TICK)
        {
            //Q_ASSERT(false);
            qDebug() << "timer tick won't make menu redraw";
            continue;
        }

        prop = mUiAttr.findProperty( pEntry->cmd, ui_attr::e_item );
        //! no prop
        if ( NULL == prop )
        {
            //! user items
            serviceExecutor::_signalQueueMutex.lock();
            serviceExecutor::_signalQueue.attach(
                        Signal_value_changed( getId(),
                                              pEntry->cmd,
                                              this,
                                              NULL )
                        );
            serviceExecutor::_signalQueueMutex.unlock();
            break;
        }
        else
        {
            if ( prop->isXControl(Button_One) )
            {
                break;
            }

            if ( prop->isXControl(Space) )
            {
                break;
            }

            serviceExecutor::_signalQueueMutex.lock();

            serviceExecutor::_signalQueue.attach(
                        Signal_value_changed( getId(),
                                              pEntry->cmd,
                                              this,
                                              NULL )
                        );
            serviceExecutor::_signalQueueMutex.unlock();
        }
    }while( 0 );

    return ERR_NONE;
}

/*!
 * \brief serviceExecutor::updateUiAttr
 * update the ui attributes
 */
void serviceExecutor::updateUiAttr()
{
    ui_attr::uiProperty* pProperty;

#if USE_EMIT
#else
    return;
#endif

    foreach( pProperty, mUiAttr.getListProperty() )
    {
        Q_ASSERT( pProperty != NULL );

        //! enable changed
        if ( pProperty->isEnableModified() )
        {
            serviceExecutor::_signalQueueMutex.lock();
            if ( pProperty->isOption() )
            {
                serviceExecutor::_signalQueue.attach(
                            Signal_enable_changed_opt( getId(),
                                                   pProperty->mMsg,
                                                   pProperty->mValue,
                                                   pProperty->mEnable,
                                                   this,
                                                   NULL ));
            }

            if ( pProperty->isItem())
            {
                serviceExecutor::_signalQueue.attach(
                            Signal_enable_changed( getId(),
                                                   pProperty->mMsg,
                                                   pProperty->mEnable,
                                                   this,
                                                   NULL ) );
            }
            serviceExecutor::_signalQueueMutex.unlock();
        }

        //! visible changed
        if ( pProperty->isVisibleModified() )
        {
            serviceExecutor::_signalQueueMutex.lock();
            if ( pProperty->isOption() )
            {
                serviceExecutor::_signalQueue.attach(
                            Signal_visible_changed_opt( getId(),
                                                   pProperty->mMsg,
                                                   pProperty->mValue,
                                                   pProperty->mVisible,
                                                   this,
                                                   NULL ));
            }

            if ( pProperty->isItem())
            {
                serviceExecutor::_signalQueue.attach(
                            Signal_visible_changed( getId(),
                                                   pProperty->mMsg,
                                                   pProperty->mVisible,
                                                   this,
                                                   NULL ));
            }
            serviceExecutor::_signalQueueMutex.unlock();
        }

        pProperty->rstModified();
    }
}

/*!
 * \brief serviceExecutor::startTimer
 * \param timeoutms
 * \param id
 * \param attr
 * 启动时钟
 * -时钟不精确
 * -时钟在消息循环空闲时才会累计
 */
void serviceExecutor::startTimer( int timeoutms,
                                  int id, enumTimerAttr attr )
{
//    qDebug()<<"serviceExecutor::startTimer S"<<getDebugTime();
    struTimer *pTimer;

    Q_ASSERT( timeoutms > 0 );

    foreach( pTimer, _timerList )
    {
        Q_ASSERT( pTimer != NULL );

        //! find exist
        if ( pTimer->servId == mpItem->servId
             && pTimer->timeId == id )
        {
            pTimer->ticks = timeoutms;
            pTimer->timeOut = timeoutms;
            return;
        }
    }

    //! add new
    pTimer = R_NEW( struTimer( mpItem->servId, timeoutms, id, attr ) );
    Q_ASSERT( pTimer != NULL );
    _timerList.append( pTimer );
//    qDebug()<<"serviceExecutor::startTimer E"<<getDebugTime();
}
void serviceExecutor::stopTimer( int id )
{
    struTimer *pTimer;

    foreach( pTimer, _timerList )
    {
        Q_ASSERT( pTimer != NULL );

        //! match
        if ( pTimer->servId == mpItem->servId
             && pTimer->timeId == id )
        {
            _timerList.removeOne( pTimer );
            R_DELETE( pTimer );
            return;
        }
    }
}

int serviceExecutor::spyOn( const QString &servName, int msg,
                     int aliasMsg,
                     eSpyType spyType,
                     eSpyPara spyPara )
{
    return spyOn( servName, msg, mpItem->servName, aliasMsg,
                  spyType, spyPara );
}

int serviceExecutor::spyOn( int id, int msg,
                     int aliasMsg,
                     eSpyType spyType,
                     eSpyPara spyPara )
{
    return spyOn( id, msg, mpItem->servId, aliasMsg, spyType, spyPara );
}

int serviceExecutor::spyOn( const QString &servName, int msg,
                     eSpyType spyType,
                     eSpyPara spyPara )
{
    return spyOn( servName, msg, msg, spyType, spyPara );
}

int serviceExecutor::spyOn( int id, int msg,
                     eSpyType spyType,
                     eSpyPara spyPara )
{
    return spyOn( id, msg, msg, spyType, spyPara );
}

/*!
 * \brief serviceExecutor::startIntent
 * \param servName
 * \param intent
 * \return
 * 启动指定的服务，执行预定的功能
 */
DsoErr serviceExecutor::startIntent( const QString &servName, CIntent &intent )
{
    CArgument arg;

    CIntent *pIntent;

    pIntent = R_NEW( CIntent() );
    Q_ASSERT( NULL != pIntent );

    //! attach
    *pIntent = intent;

    //! client id
    pIntent->mClientId = this->mpItem->servId;

    arg.setVal( pIntent );

    //! 切换到指定的窗口页面上
    if ( intent.mMsg != -1 )
    {
        serviceExecutor::post( servName, CMD_SERVICE_ACTIVE, intent.mMsg );
    }

    return serviceExecutor::post( servName, CMD_SERVICE_INTENT, arg );
}

DsoErr serviceExecutor::returnIntent( CIntent &intent, int ret )
{
    CArgument arg;

    if ( intent.mClientId == -1 || intent.mClientMsg == -1 )
    {
        return ERR_INVALID_CONFIG;
    }

    arg.setVal( intent.mClientPara );
    arg.setVal( ret, 1 );

    return serviceExecutor::post( intent.mClientId, intent.mClientMsg,  arg );
}

DsoErr serviceExecutor::deActive( )
{
    return async( CMD_SERVICE_DEACTIVE, (int)0 );
}

/*!
 * \brief serviceExecutor::setActive
 * \param act
 * \return
 * 响应被active请求
 */
DsoErr serviceExecutor::setActive( int msg )
{
#if USE_EMIT
#else
    return ERR_NONE;
#endif

    DsoErr err;
    service *pServ;
    int id;

    //! 设置为active
    err = service::active( mpItem->servId );
    if ( err == ERR_SERICE_ACTIVE_NOW )
    {
        active_in( msg );

        id = mpItem->servId;
        post( id, CMD_SERVICE_RE_ENTER_ACTIVE, id );

        return ERR_NONE;
    }
    else if ( err != ERR_NONE )
    {
        return err;
    }
    else
    {}
//    emit sig_active_changed( msg, this );
    serviceExecutor::_signalQueueMutex.lock();
    serviceExecutor::_signalQueue.attach(
                Signal_active_changed( getId(),
                                       msg,
                                       this,
                                       NULL ));
    serviceExecutor::_signalQueueMutex.unlock();
    //! 前一个退出了活动状态
    if ( service::_activeList.size() > 1 )
    {
        id = service::_activeList[1]->servId;
        pServ = findService( id );
        (dynamic_cast<serviceExecutor*>(pServ))->active_out( msg );

        post( id, CMD_SERVICE_EXIT_ACTIVE, id );
    }

    //! 后一个进入了活动状态
    id = service::_activeList[0]->servId;
    pServ = findService( id );
    (dynamic_cast<serviceExecutor*>(pServ))->active_in( msg );
    post( id, CMD_SERVICE_ENTER_ACTIVE, id );

    return ERR_NONE;
}
DsoErr serviceExecutor::setdeActive( int /*para*/ )
{
    int preId;
    service *pServ;

    //! self not active
    if ( !isActive() )
    { return ERR_NONE; }

    Q_ASSERT( service::_activeList.size() > 0 );
    //! no active on
    if ( service::_activeList.size() > 1 )
    {}
    else
    { return ERR_NONE; }


    //! 将前一个设置为active
    {
        preId = _activeList[0]->servId;
        if ( preId != getId() )
        { return ERR_NONE; }

        pServ = findService( preId );
        (dynamic_cast<serviceExecutor*>(pServ))->active_out( 0 );

        //! remove the first item
        service::deActive();

        //! 需要对应的executor发出signal通知其menu
        pServ = findService( service::_activeList[0]->servId );
        (dynamic_cast<serviceExecutor*>(pServ))->active_in( CMD_SERVICE_DEACTIVE );

        post( preId, CMD_SERVICE_EXIT_ACTIVE, preId );
        post( (int)service::_activeList[0]->servId,
              CMD_SERVICE_ENTER_ACTIVE,
              (int)service::_activeList[0]->servId );
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::deactiveActive( int actId )
{
    int preId;
    service *pServ;

    Q_ASSERT( service::_activeList.size() > 0 );
    //! no active on
    if ( service::_activeList.size() > 1 )
    {}
    else
    { return ERR_NONE; }

    //! 将前一个设置为active
    {
        preId = _activeList[0]->servId;

        //! active now
        if ( actId == preId ) return ERR_NONE;

        pServ = findService( preId );
        Q_ASSERT( NULL != pServ );
        (dynamic_cast<serviceExecutor*>(pServ))->active_out( 0 );

        //! remove the first item
        service::deActive();

        //! 需要对应的executor发出signal通知其menu
        pServ = findService( actId );
        Q_ASSERT( NULL != pServ );
        (dynamic_cast<serviceExecutor*>(pServ))->active_in( CMD_SERVICE_DEACTIVE );

        //! active item to head
        service::active( actId );

        post( preId, CMD_SERVICE_EXIT_ACTIVE,  preId );
        post( actId, CMD_SERVICE_ENTER_ACTIVE, actId );
    }

    return ERR_NONE;
}

bool serviceExecutor::isActive()
{
    if ( service::_activeList.size() < 1 )
        return false;

    if ( mpItem->servId == service::_activeList[0]->servId )
    { return true; }

    return false;
}

DsoErr serviceExecutor::onIntent( CIntent *pIntent )
{
    Q_ASSERT( NULL != pIntent );

    return ERR_NONE;
}

void serviceExecutor::active_in( int msg )
{
#if USE_EMIT
#else
    return;
#endif
//    emit sig_active_in( msg, this );
    serviceExecutor::_signalQueueMutex.lock();
    serviceExecutor::_signalQueue.attach(
                Signal_active_in( getId(),
                                       msg,
                                       this,
                                       NULL ));
    serviceExecutor::_signalQueueMutex.unlock();
}
void serviceExecutor::active_out( int msg )
{
#if USE_EMIT
#else
    return;
#endif
//    emit sig_active_out( msg, this );
    serviceExecutor::_signalQueueMutex.lock();
    serviceExecutor::_signalQueue.attach(
                Signal_active_out( getId(),
                                       msg,
                                       this,
                                       NULL ));
    serviceExecutor::_signalQueueMutex.unlock();
}

DsoErr serviceExecutor::onViewChange( int /*val*/ )
{
    return ERR_NONE;
}

DsoErr serviceExecutor::on_enterActive( int /*id*/ )
{
    return ERR_NONE;
}
DsoErr serviceExecutor::on_exitActive( int /*id*/ )
{
    return ERR_NONE;
}

DsoErr serviceExecutor::on_attrChange()
{
    return ERR_NONE;
}

void serviceExecutor::on_pre_do( CArgument &arg )
{
    //! check
    if ( arg.size() < 2 )
    { return; }

    //! deload
    int msg;
    arg.getVal( msg );

    CArgument localArg;
    localArg.append( arg[1] );

    //! do
    struStubDo *pDoEntry;
    pDoEntry = getPreDoEntry();
    void_do_arg pDo;
    while( NULL != pDoEntry && pDoEntry->cmd != 0 && pDoEntry->stubDo != NULL )
    {
        //! do the cmd
        if ( pDoEntry->cmd == msg )
        {
            pDo = pDoEntry->stubDo;
            (this->*pDo)( localArg );
        }

        pDoEntry++;
    }
}

void serviceExecutor::on_post_do( CArgument &arg )
{
    if ( arg.size() < 2 )
    { return; }

    int msg;
    arg.getVal( msg );

    CArgument localArg;
    localArg.append( arg[1] );

    struStubDo *pDoEntry;
    pDoEntry = getPostDoEntry();
    void_do_arg pDo;
    while( NULL != pDoEntry && pDoEntry->cmd != 0 && pDoEntry->stubDo != NULL )
    {
        //! do the cmd
        if ( pDoEntry->cmd == msg )
        {
            pDo = pDoEntry->stubDo;
            (this->*pDo)( localArg );
        }

        pDoEntry++;
    }
}

void serviceExecutor::on_pre_set_active()
{
    if ( mbUiCache )
    {
        updateAllUi();
        LOG_DBG()<<getName();
    }
}

//! async by name

///
/// \brief serviceExecutor::async
/// \param msg
/// \param bVal
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::async( int msg,
                               const bool bVal,
                               const QString &servName,
                               struActionContext *pContext )
{
    return serviceExecutor::post( selectName(servName),
                                  msg,
                                  bVal,
                                  selectContext( pContext ) );
}

///
/// \brief serviceExecutor::async
/// \param msg
/// \param iVal
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::async( int msg,
                               const int iVal,
                               const QString &servName,
                               struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servName),
                                  msg,
                                  iVal,
                                  selectContext( pContext ) );
}
///
/// \brief serviceExecutor::async
/// \param msg
/// \param val
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::async( int msg,
                               const QString &val,
                               const QString &servName,
                               struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servName),
                                  msg,
                                  val,
                                  selectContext( pContext ) );
}
///
/// \brief serviceExecutor::async
/// \param msg
/// \param val
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::async( int msg,
                               const qlonglong val,
                               const QString &servName,
                               struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servName),
                                  msg,
                                  val,
                                  selectContext( pContext ) );
}
///
/// \brief serviceExecutor::async
/// \param msg
/// \param val
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::async( int msg,
                               CObj *val,
                               const QString &servName,
                               struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servName),
                                  msg,
                                  val,
                                  selectContext( pContext ) );
}
///
/// \brief serviceExecutor::async
/// \param msg
/// \param val
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::async( int msg,
                               const struServPara &val,
                               const QString &servName,
                               struActionContext *pContext )
{
    return serviceExecutor::post( selectName(servName),
                                  msg,
                                  val,
                                  selectContext( pContext ) );
}
///
/// \brief serviceExecutor::async
/// \param msg
/// \param val
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::async( int msg,
                               DsoReal &val,
                               const QString &servName,
                               struActionContext *pContext )
{
    return serviceExecutor::post( selectName(servName),
                                  msg,
                                  val,
                                  selectContext( pContext ) );
}
///
/// \brief serviceExecutor::defer
/// \param msg
/// \param servName
/// \param pContext
/// \return
///
DsoErr serviceExecutor::defer( int msg,
                               const QString &servName,
                               struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servName), msg, (int)0, pContext );
}

/////
/// \brief serviceExecutor::id_async
/// \param servId
/// \param msg
/// \param bVal
/// \param pContext
/// \return
///
DsoErr serviceExecutor::id_async( int servId,
                                  int msg,
                                  const bool bVal,
                                  struActionContext *pContext )
{
    return serviceExecutor::post( selectName(servId), msg, bVal, pContext );
}

///
/// \brief serviceExecutor::id_async
/// \param servId
/// \param msg
/// \param iVal
/// \param pContext
/// \return
///
DsoErr serviceExecutor::id_async( int servId,
                                  int msg,
                                  const int iVal,
                                  struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servId), msg, iVal, pContext );
}
///
/// \brief serviceExecutor::id_async
/// \param servId
/// \param msg
/// \param val
/// \param pContext
/// \return
///
DsoErr serviceExecutor::id_async( int servId,
                                  int msg,
                                  const QString &val,
                                  struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servId), msg, val, pContext );
}
///
/// \brief serviceExecutor::id_async
/// \param servId
/// \param msg
/// \param val
/// \param pContext
/// \return
///
DsoErr serviceExecutor::id_async( int servId,
                                  int msg,
                                  const qlonglong val,
                                  struActionContext *pContext )
{
    return serviceExecutor::post( selectName(servId), msg, val, pContext );
}
///
/// \brief serviceExecutor::id_async
/// \param servId
/// \param msg
/// \param val
/// \param pContext
/// \return
///
DsoErr serviceExecutor::id_async( int servId,
                                  int msg,
                                  CObj *val,
                                  struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servId), msg, val, pContext );
}
///
/// \brief serviceExecutor::id_async
/// \param servId
/// \param msg
/// \param val
/// \param pContext
/// \return
///
DsoErr serviceExecutor::id_async( int servId,
                                  int msg,
                                  DsoReal &val,
                                  struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servId), msg, val, pContext );
}
///
/// \brief serviceExecutor::id_async
/// \param servId
/// \param msg
/// \param pContext
/// \return
///
DsoErr serviceExecutor::id_async( int servId, int msg,

                               struActionContext *pContext)
{
    return serviceExecutor::post( selectName(servId), msg, (int)0, pContext );
}

//! ssync
DsoErr serviceExecutor::ssync( int msg, const bool bVal,
                               const QString &servName,
                               struActionContext *pContext)
{
    return serviceExecutor::send( selectName(servName), msg, bVal, pContext );
}

DsoErr serviceExecutor::ssync( int msg, const int iVal,
                               const QString &servName,
                              struActionContext *pContext)
{
    return serviceExecutor::send( selectName(servName), msg, iVal, pContext );
}
DsoErr serviceExecutor::ssync( int msg, const QString &val,
                              const QString &servName,
                              struActionContext *pContext)
{
    return serviceExecutor::send( selectName(servName), msg, val, pContext );
}

DsoErr serviceExecutor::ssync( int msg, const qlonglong val,
                              const QString &servName,
                              struActionContext *pContext)
{
    return serviceExecutor::send( selectName(servName), msg, val, pContext );
}

DsoErr serviceExecutor::incur( int msg,
             const QString &servName,
             struActionContext *pContext
              )
{
    return serviceExecutor::send( selectName(servName), msg, (int)0, pContext );
}

const QString &serviceExecutor::selectName( const QString &str )
{
    if ( str == _dumyServName )
    {
        Q_ASSERT( mpItem != NULL );
        return mpItem->servName;
    }
    else
    {
        return str;
    }
}

const QString &serviceExecutor::selectName( int servId )
{
    Q_ASSERT( mpItem != NULL );
    //! self
    if ( E_SERVICE_ID_NONE == servId
         || mpItem->servId == servId )
    { return mpItem->servName; }
    else
    {
        struServItem *pItem;
        pItem = findItem( servId );

        Q_ASSERT( NULL != pItem );
        return pItem->servName;
    }
}

struActionContext * serviceExecutor::selectContext( struActionContext *pContext )
{
    //! internal
    if ( NULL == pContext )
    {
        struActionContext * proxy;
        if ( NULL != m_pActiveContext )
        {
            proxy = m_pActiveContext->proxy();

            Q_ASSERT( NULL != proxy );

            return proxy;
        }
        else
        { return &_contextInternal; }
    }
    else
    { return pContext; }
}

ui_attr::uiProperty *serviceExecutor::getProperty( int msg )
{
    return mUiAttr.getItem( msg );
}

ui_attr::uiAttr *serviceExecutor::getAttr()
{
    return &mUiAttr;
}

/*!
 * \brief serviceExecutor::post
 * \param msg
 * \param para
 * \return
 */
DsoErr serviceExecutor::post( int msg, const struServPara &para )
{
    return post( mpItem->servId, msg, para );
}

DsoErr serviceExecutor::send( int msg, const struServPara &para )
{
    return send( mpItem->servId, msg, para );
}

DsoErr serviceExecutor::query( int msg, struServPara &para )
{
    return doGet( msg, para );
}

DsoErr serviceExecutor::query( int msg,
                               CArgument &para,
                               CArgument &outPara )
{
    return doGet( msg, para, outPara );
}

bool serviceExecutor::keyEat( int /*key*/, int /*count*/, bool )
{
    return false;
}

//! key -> action translate
bool serviceExecutor::keyTranslate( int key,
                        int &msg,
                        int &controlAction )
{
    if ( mServKeyMap.size() < 1 ) return false;

    //! find
    serviceKey servKey;
    if ( mServKeyMap.find( key, servKey) )
    {
        msg = servKey.mMsg;
        controlAction = servKey.mAction;

        return true;
    }

    return false;
}

bool serviceExecutor::actionFilter( const struServAction &action )
{
    //! bypass action from ui
    if ( action.pservContext != NULL &&
         /*action.pservContext->isUserAction() &&*/
         action.pservContext->isUiAction())
    {
        //! to self
        if ( action.servId == mpItem->servId )
        {
            return false;
        }
        //! system msg
        else if ( is_system_msg( action.servMsg) )
        {
            //! disable active
            if ( action.servMsg == CMD_SERVICE_ACTIVE )
            {
                return true;
            }

            return false;
        }
        //! system service
        else if ( action.servId == E_SERVICE_ID_DSO )
        {
            return false;
        }
        else
        {
//            qDebug() << "serv:" << action.servId << "msg:"<<action.servMsg;
            return true;
        }
    }
    //! do not filter
    else
    {
        return false;
    }
}

bool serviceExecutor::quickKeyTranslate( int key )
{
    QuickKey *pQuickKey;

    if( key == R_Skey_Active )
    {
        return false;
    }
    foreach( pQuickKey, mQuickKeyList )
    {
        Q_ASSERT( NULL != pQuickKey );
        if ( pQuickKey->sequenceIn(key) )
        {
            //! async msg
            defer( pQuickKey->getMsg() );
            return true;
        }
    }

    return false;
}

bool serviceExecutor::msgFilter( const QString &servName,
                                 int msg )
{
    if ( serviceExecutor::_workMode == work_help )
    {
        //! post to help service
        CHelpRequest *pReq;
        pReq = R_NEW( CHelpRequest( servName, msg ) );
        Q_ASSERT( NULL != pReq );

        //! help msg request
        serviceExecutor::post( (int)E_SERVICE_ID_HELP,
                               CMD_SERVICE_HELP_REQUESST,
                               pReq );

        return true;
    }

    return false;
}

/*!
 * \brief serviceExecutor::doSet
 * \param action
 * \return
 * 执行设置动作
 */
DsoErr serviceExecutor::doSet( const struServAction &action )
{
    DsoErr err = 0;
    DsoErr uiErr;
    struServPara localPara = action.servPara;

    //! ----check en
    if ( uiActionFilter( action, &err ) )
    {
        return err;
    }


    //! ----filter
    struServPara qPara;
    uiErr = query( action.servMsg, qPara );
    //! ----check range
    if ( uiErr == ERR_NONE )
    {
        err = checkRange( action.servMsg,
                          localPara,
                          qPara );
        if ( err != ERR_NONE )
        {
//            serviceExecutor::post( E_SERVICE_ID_ERR_LOGER,
//                                   CMD_SERVICE_DO_ERR,
//                                   (int)err );

            LOG_DBG()<<action.servMsg<<localPara[0].vType;
            LOG_DBG()<<action.servMsg<<qPara[0].vType;
        }
    }

    //! ----call CExecutor
    serviceExecutor::_servExecMutex.lock();
    mCurrentMsg = action.servMsg;

    //! ----do proc
    if ( isEntryExist( action.servMsg, enum_w) )
    {
        CArgument argMsg;
        argMsg.append( action.servMsg );
        argMsg.append( localPara );

        //! pre do
        onDoCmd( CMD_SERVICE_PRE_DO, argMsg, enum_w );

        //! set active context
        m_pActiveContext = action.pservContext;

        err = onDoCmd( action.servMsg, localPara, enum_w );

        //! post do
        onDoCmd( CMD_SERVICE_POST_DO, argMsg, enum_w );
    }
    else
    {
        err = 0;
    }

    mCurrentMsg = 0;
    serviceExecutor::_servExecMutex.unlock();

    //! ----check real value
    updateUi( action, qPara );

    //! ---ui attrs
    updateUiAttr();

    //! \note 这里将内部错误码转换为ui错误码
    if( err < 0 )
    {
        //! no cmd entry
        if ( err == ERR_TARGET_MIS_MATCH )
        {
            return ERR_NONE;
        }
        else
        {
            LOG_DBG()<<getName()<<action.servMsg<<err;
        }
        return ERR_MSG_DO_FAIL;
    }
    else
    {
        return (DsoErr)err;
    }
}

/*! \brief doGet
*
* 执行查询动作
* \param msg 消息
* \param [out] para 查询得到的参数
*/
DsoErr serviceExecutor::doGet( int msg,
                               struServPara &para )
{
    int err;

    CArgument listVal;

    //! save current msg
    serviceExecutor::_servExecMutex.lock();
    mCurrentMsg = msg;
    err = onDoCmd( msg, listVal, enum_r, para );

    mCurrentMsg = 0;
    serviceExecutor::_servExecMutex.unlock();

    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    return ERR_NONE;
}

DsoErr serviceExecutor::doGet( int msg,
                               CArgument &para,
                               CArgument &outPara )
{
    int err;

    //! save current msg
    serviceExecutor::_servExecMutex.lock();
    mCurrentMsg = msg;

    err = onDoCmd( msg, para, enum_r, outPara );

    mCurrentMsg = 0;
    serviceExecutor::_servExecMutex.unlock();

    if ( err != ERR_NONE )
    {
        return ERR_MSG_GET_FAIL;
    }

    return ERR_NONE;
}

bool serviceExecutor::uiActionFilter( const struServAction &action,
                                      DsoErr *pCause )
{
    Q_ASSERT( NULL != pCause );
    *pCause = ERR_ACTION_DISABLED;

    //! bypass action from ui
    if ( action.pservContext != NULL
         && action.pservContext->isUserAction() )
    {
    }
    //! do not filter
    else
    {
        return false;
    }

    //! not system msg
    if ( !mEnable && !is_system_msg( action.servMsg) )
    {
        *pCause = mDisableCause;
        return true;
    }

    //! find the attr
    ui_attr::uiProperty *prop;
    prop = mUiAttr.findProperty( action.servMsg, ui_attr::e_item );
    if ( NULL == prop )
    {
        return false;
    }

    //! control item
    if ( prop->filter() )
    {
        //BUG 1033
        if( !prop->getVisible() )
        {
            *pCause = ERR_NONE;
        }
        LOG_DBG()<< "MSG=" <<prop->mMsg<< "SERV="<<getName();
        return true;
    }

    //! option item?
    if ( action.servPara.size() == 1 )
    {}
    else
    {
        return false;
    }

    CVal arg0;
    DsoErr err = action.servPara.getVal( arg0 );
    if ( err != ERR_NONE  )
    {
        return false;
    }

    if( arg0.vType == val_int )
    {
        //! checkbox use mask
        if ( prop->isXControl(Check_Box) )
        {
            //! scan bit 1 pos
            int checks[32];
            for ( int i = 0; i < 32; i++ )
            { checks[i] = get_bit( arg0.uVal, i ); }

            //! check the bit enable attr
            for ( int i = 0; i < 32; i++ )
            {
                //! 1
                if ( checks[i] )
                {
                    prop = mUiAttr.findProperty( action.servMsg,
                                                 ui_attr::e_option,
                                                 i );

                    if ( NULL == prop )
                    { return false;}

                    if ( prop->filter() )
                    {
                        LOG_DBG()<<i<<action.servMsg;
                        return true;
                    }
                }
            }
        }
        else
        {
            prop = mUiAttr.findProperty( action.servMsg,
                                         ui_attr::e_option,
                                         arg0.iVal );
            if ( NULL == prop )
            {
                return false;
            }

            if ( prop->filter() )
            {
                return true;
            }
        }
    }
    else if ( arg0.vType == val_string )
    {
        //! valid
        if ( prop->isXxxValid( ui_attr::e_max_text_length ) )
        {
            if ( prop->getMaxLength() < arg0.strVal.length() )
            {
                return true;
            }
        }

        //! invalid length
        if ( arg0.strVal.length() < 1 )
        {
            return true;
        }
    }
    else
    {}

    return false;
}

bool serviceExecutor::isInUiContext()
{
    QThread *pCurThread = QThread::currentThread();

    if ( pCurThread == NULL )
    { return false; }

    //! in service thread
    if ( pCurThread == serviceExecutor::_execThread )
    { return true; }

    return false;
}

void serviceExecutor::setuiEnable( bool b )
{
    mUiAttr.setEnable( mCurrentMsg, b );
}
void serviceExecutor::setuiVisible( bool b )
{
    mUiAttr.setVisible( mCurrentMsg, b );
}
void serviceExecutor::setuiEnable( int opt, bool b )
{
    mUiAttr.setEnable( mCurrentMsg, opt, b );
}
void serviceExecutor::setuiVisible( int opt, bool b )
{
    mUiAttr.setVisible( mCurrentMsg, opt, b );
}
void serviceExecutor::setuiZMsg( int msg )
{
    mUiAttr.setZMsg( mCurrentMsg, msg);
}
void serviceExecutor::setuiStep( qlonglong step )
{
    mUiAttr.setStep( mCurrentMsg, step );
}
void serviceExecutor::setuiIStep( qlonglong step )
{
    mUiAttr.setIStep( mCurrentMsg, step );
}
void serviceExecutor::setuiDStep( qlonglong step )
{
    mUiAttr.setDStep( mCurrentMsg, step );
}
void serviceExecutor::setuiZVal( qlonglong z )
{
    mUiAttr.setZVal( mCurrentMsg, z );
}
void serviceExecutor::setuiZVal( int z )
{
    mUiAttr.setZVal( mCurrentMsg, z );
}
void serviceExecutor::setuiZVal( dsoReal &real )
{
    mUiAttr.setZVal( mCurrentMsg, real );
}
void serviceExecutor::setuiZVal( DsoRealType_A a, DsoE e)
{
    mUiAttr.setZVal( mCurrentMsg, a, e );
}

void serviceExecutor::setuiMinVal( qlonglong v )
{
    mUiAttr.setMinVal( mCurrentMsg, v );
}
void serviceExecutor::setuiMinVal( int v )
{
    mUiAttr.setMinVal( mCurrentMsg, v );
}
void serviceExecutor::setuiMinVal( dsoReal &real )
{
    mUiAttr.setMinVal( mCurrentMsg, real );
}
void serviceExecutor::setuiMinVal( DsoRealType_A a, DsoE e)
{
    mUiAttr.setMinVal( mCurrentMsg, a, e );
}
void serviceExecutor::setuiMaxVal( qlonglong v )
{
    mUiAttr.setMaxVal( mCurrentMsg, v );
}
void serviceExecutor::setuiMaxVal( int v )
{
    mUiAttr.setMaxVal( mCurrentMsg, v );
}
void serviceExecutor::setuiMaxVal( dsoReal &real )
{
    mUiAttr.setMaxVal( mCurrentMsg, real );
}
void serviceExecutor::setuiMaxVal( DsoRealType_A a, DsoE e)
{
    mUiAttr.setMaxVal( mCurrentMsg, a, e );
}
void serviceExecutor::setuiRange( int minVal, int maxVal, int zVal )
{
    mUiAttr.setRange( mCurrentMsg, minVal, maxVal, zVal );
}
void serviceExecutor::setuiRange( qlonglong minVal, qlonglong maxVal, qlonglong zVal )
{
    mUiAttr.setRange( mCurrentMsg, minVal, maxVal, zVal );
}
void serviceExecutor::setuiRange( dsoReal &minVal, dsoReal &maxVal, dsoReal &zVal )
{
    mUiAttr.setRange( mCurrentMsg, minVal, maxVal, zVal);
}
void serviceExecutor::setuiRange(
                 DsoRealType_A mina, DsoE mine,
                 DsoRealType_A maxa, DsoE maxe,
                 DsoRealType_A za, DsoE ze
                 )
{
    mUiAttr.setRange( mCurrentMsg,
                      mina, mine,
                      maxa, maxe,
                      za, ze );
}
void serviceExecutor::setuiUnit( DsoType::Unit unit )
{
    mUiAttr.setUnit( mCurrentMsg, unit );
}
void serviceExecutor::setuiFmt( DsoType::DsoViewFmt fmt )
{
    mUiAttr.setFormat( mCurrentMsg, fmt );
}
void serviceExecutor::setuiAcc(  key_acc::KeyAcc acc )
{
    mUiAttr.setAcc( mCurrentMsg, acc );
}
/*!
 * \brief serviceExecutor::setUiStick
 * \param val
 * 只设置一个值
 */
void serviceExecutor::setUiStick( int val )
{
    mUiAttr.setStick( mCurrentMsg, val );
}
void serviceExecutor::setUiStick( qlonglong val )
{
    mUiAttr.setStick( mCurrentMsg, val );
}
/*!
 * \brief serviceExecutor::setUiStick
 * \param listVal
 * -设置多个粘滞值
 * -粘滞值的顺序是从小到大
 */
void serviceExecutor::setUiStick( QList<int> &listVal )
{
    mUiAttr.setStick( mCurrentMsg, listVal );
}
void serviceExecutor::setUiStick( QList<qlonglong> &listVal)
{
    mUiAttr.setStick( mCurrentMsg, listVal );
}
void serviceExecutor::setuiBase( float base )
{
    mUiAttr.setBase( mCurrentMsg, base );
}
void serviceExecutor::setuiBase( DsoReal real )
{
    mUiAttr.setBase( mCurrentMsg, real );
}
void serviceExecutor::setuiBase( DsoRealType_A a, DsoE e )
{
    mUiAttr.setBase( mCurrentMsg, a, e );
}
void serviceExecutor::setuiPreStr(  const QString &str )
{
    mUiAttr.setPreStr( mCurrentMsg, str );
}
void serviceExecutor::setuiPostStr(  const QString &str )
{
    mUiAttr.setPostStr( mCurrentMsg, str );
}
void serviceExecutor::setuiOutStr( const QString &str )
{
    mUiAttr.setOutStr( mCurrentMsg, str );
}
void serviceExecutor::setuiNodeCompress( bool b )
{
    mUiAttr.setNodeCompress( mCurrentMsg, b );
}
void serviceExecutor::setuiMaxLength( int len )
{
    mUiAttr.setMaxLength( mCurrentMsg, len);
}

void serviceExecutor::setuiKeyRequest( CKeyRequest &keyReq )
{
    mUiAttr.setKeyRequest( mCurrentMsg, keyReq );
}

void serviceExecutor::setuiChange( int msg )
{

    if( msg != CMD_SERVICE_TICK &&
        msg != CMD_ENGINE_TICK)
    {
        serviceExecutor::_signalQueueMutex.lock();
        serviceExecutor::_signalQueue.attach(
                    Signal_value_changed( getId(),
                                           msg,
                                           this,
                                           NULL ));
        serviceExecutor::_signalQueueMutex.unlock();
    }
}

void serviceExecutor::setuiFocus( int msg )
{
    serviceExecutor::_signalQueueMutex.lock();
    serviceExecutor::_signalQueue.attach(
                Signal_focus_changed( getId(),
                                       msg,
                                       this,
                                       NULL ));
    serviceExecutor::_signalQueueMutex.unlock();
}

DsoErr serviceExecutor::checkRange( int msg,
                                    struServPara &paraI,
                                    struServPara &paraQ )
{
    DsoErr err;
    ui_attr::uiProperty *pProp;

    pProp = mUiAttr.getItem( msg );
    if ( pProp == NULL ) return ERR_NONE;

    do
    {
        //! has min/max attr
        if ( pProp->isXxxValid( ui_attr::e_max_mask ) &&
             pProp->isXxxValid( ui_attr::e_min_mask ) )
        { }
        else
        { break; }

        //! type mismatch
        if ( !paraI.isLike( paraQ )  )
        { break; }

        //! only one argument
        if ( paraI.size() != 1 )
        { break; }

        //! only value
        if ( paraQ[0].vType == val_int ||
             paraQ[0].vType == val_longlong )
        {
        }
        else
        { break; }

        ui_attr::PackInt packMin, packMax;

        packMin = pProp->getMinVal();
        packMax = pProp->getMaxVal();

        //! check range
        if ( paraI[0].vType == val_int )
        {
            if ( paraI[0].iVal < packMin.val32 )
            {
                paraI[0].iVal = packMin.val32;
                err = ERR_OVER_LOW_RANGE;
            }
            else if ( paraI[0].iVal > packMax.val32 )
            {
                paraI[0].iVal = packMax.val32;
                err = ERR_OVER_UPPER_RANGE;
            }
            else
            {
                err = ERR_NONE;
            }

//            qDebug()<<__FUNCTION__<<paraI[0].iVal;
        }
        else if ( paraI[0].vType == val_longlong )
        {
            if ( paraI[0].llVal < packMin.val64 )
            {
                paraI[0].llVal = packMin.val64;
                err = ERR_OVER_LOW_RANGE;
            }
            else if ( paraI[0].llVal > packMax.val64 )
            {
                paraI[0].llVal = packMax.val64;
                err = ERR_OVER_UPPER_RANGE;
            }
            else
            {
                err = ERR_NONE;
            }

//            qDebug()<<__FUNCTION__<<paraI[0].llVal;
        }
        else
        {
            err = ERR_NONE;
        }

        return err;

    }while( 0 );

    return ERR_NONE;
}

DsoErr serviceExecutor::checkRange( qint32 &val,
                                    qint32 mi, qint32 ma )
{
    if ( val < mi )
    {
        val = mi;
        return ERR_OVER_LOW_RANGE;
    }
    else if ( val > ma )
    {
        val = ma;
        return ERR_OVER_UPPER_RANGE;
    }
    else
    {
        return ERR_NONE;
    }
}
DsoErr serviceExecutor::checkRange( qint64 &val,
                                    qint64 mi, qint64 ma )
{
    if ( val < mi )
    {
        val = mi;
        return ERR_OVER_LOW_RANGE;
    }
    else if ( val > ma )
    {
        val = ma;
        return ERR_OVER_UPPER_RANGE;
    }
    else
    {
        return ERR_NONE;
    }
}

void serviceExecutor::reset()
{
    //! 复位快捷键状态
    rstQuickKey();
}

void serviceExecutor::setMsgAttr( int msg, enumMsgAttr attr )
{
    if ( mNotifyMsgList.contains(msg) )
    {
        mNotifyMsgList[ msg ] = attr;
    }
    else
    {
        mNotifyMsgList.insert( msg, attr );
    }
}

bool serviceExecutor::isNotify( int msg )
{
    if ( mNotifyMsgList.contains(msg) &&
         is_attr( mNotifyMsgList[msg], msg_notify) )
    { return true; }

    return false;
}
bool serviceExecutor::isConsole( int msg )
{
    if ( mNotifyMsgList.contains(msg) &&
         is_attr( mNotifyMsgList[msg], msg_console) )
    { return true; }

    return false;
}

DsoErr serviceExecutor::appendQuickKey( const QList<int> &key,
                     int msg )
{
    //! find msg in list
    QuickKey *pQuickKey;
    foreach( pQuickKey, mQuickKeyList )
    {
        Q_ASSERT( NULL != pQuickKey );
        if ( pQuickKey->getMsg() == msg )
        { return ERR_QUICK_KEY_EXIST; }
    }

    //! a new item
    pQuickKey = R_NEW( QuickKey() );
    Q_ASSERT( NULL != pQuickKey );
    pQuickKey->setKey( key, msg );

    //! append to list
    mQuickKeyList.append( pQuickKey );

    return ERR_NONE;
}

void serviceExecutor::rstQuickKey()
{
    QuickKey *pQuickKey;
    foreach( pQuickKey, mQuickKeyList )
    {
        Q_ASSERT( NULL != pQuickKey );
        pQuickKey->rst();
    }
}

void serviceExecutor::collectUiMsg()
{
    //! has collected
    if ( mUiMsgs.size() > 0 )
    { return; }

    //! get all map
    QList< struCmdEntry *> entryMap;
    getEntries( entryMap );

    //! search the map list
    const struCmdEntry *pCmdEntry;
    foreach( struCmdEntry *pMapEntry, entryMap )
    {
        //! for each entry in map
        pCmdEntry = pMapEntry;
        while( pCmdEntry != NULL && pCmdEntry->cmd != 0 )
        {
            //! write
            if ( pCmdEntry->wr == enum_w )
            {
                //! ui msg
                if ( is_ui_msg(pCmdEntry->cmd) &&
                     !mUiMsgs.contains(pCmdEntry->cmd) )
                {
                    mUiMsgs.append( pCmdEntry->cmd );
                }
            }

            pCmdEntry++;
        }
    }
}

void serviceExecutor::saveUiEnMap( UiEnMap *pMap )
{
    Q_ASSERT( NULL != pMap );
    ui_attr::uiProperty *pProp;

    bool bEn;
    foreach( int msg, mUiMsgs )
    {
        //! find the property
        pProp = mUiAttr.findProperty( msg, ui_attr::e_item );
        if ( NULL != pProp )
        { bEn = pProp->getEnable(); }
        else
        { bEn = true; }

        //! insert map
        if ( !pMap->contains(msg) )
        {
            pMap->insert( msg, bEn );
        }
    }
}
void serviceExecutor::loadUiEnMap( UiEnMap *pMap )
{
    Q_ASSERT( NULL != pMap );
    ui_attr::uiProperty *pProp;

    foreach( int msg, mUiMsgs )
    {
        //! find the property
        pProp = mUiAttr.findProperty( msg, ui_attr::e_item );
        if ( NULL != pProp )
        {
            if ( pMap->contains(msg) )
            {
                pProp->setEnable( (*pMap)[msg] );
            }
        }
    }
}

void serviceExecutor::saveUiEns( int id )
{
    //! contains
    if ( mUiEnStack.contains(id) )
    {
        Q_ASSERT( mUiEnStack[id] != NULL );
        saveUiEnMap( mUiEnStack[id] );
    }
    //! new one
    else
    {
        UiEnMap *pMap = R_NEW( UiEnMap() );
        if ( NULL != pMap )
        {
            mUiEnStack.insert( id, pMap );
            saveUiEnMap( pMap );
        }
        else
        {
        }
    }
}
void serviceExecutor::restoreUiEns( int id )
{
    if ( mUiEnStack.contains(id) )
    {
        Q_ASSERT( NULL != mUiEnStack[id] );

        loadUiEnMap( mUiEnStack[id] );

        defer( CMD_SERVICE_ATTR_CHANGED );
    }
}

void serviceExecutor::deleteUiEns( int id )
{
    if ( mUiEnStack.contains(id) )
    {
        Q_ASSERT( NULL != mUiEnStack[id] );

        delete mUiEnStack[id];

        mUiEnStack.remove( id );
    }
}

void serviceExecutor::saveUiEns( int id, QStringList &servList )
{
    serviceExecutor *pExe;
    foreach( QString str, servList )
    {
        pExe = serviceExecutor::getExecutor( str );
        if ( NULL != pExe )
        { pExe->saveUiEns( id );}
    }
}
void serviceExecutor::restoreUiEns( int id, QStringList &servList )
{
    serviceExecutor *pExe;
    foreach( QString str, servList )
    {
        pExe = serviceExecutor::getExecutor( str );
        if ( NULL != pExe )
        { pExe->restoreUiEns( id );}
    }
}
void serviceExecutor::deleteUiEns( int id, QStringList &servList )
{
    serviceExecutor *pExe;
    foreach( QString str, servList )
    {
        pExe = serviceExecutor::getExecutor( str );
        if ( NULL != pExe )
        { pExe->deleteUiEns( id );}
    }
}

void serviceExecutor::setUiEnables( const QStringList &strList,
                   bool bUiEnable,
                   int msg1,
                   int msg2,
                   int msg3,
                   int msg4,
                   int msg5,
                   int msg6,
                   int msg7,
                   int msg8 )
{

    foreach( QString str, strList )
    {
        setUiEnables( str, bUiEnable,
                            msg1, msg2, msg3, msg4,
                            msg5, msg6, msg7, msg8 );
    }
}

void serviceExecutor::setUiEnables( const QString &strName,
                   bool bUiEnable,
                   int msg1,
                   int msg2,
                   int msg3,
                   int msg4,
                   int msg5,
                   int msg6,
                   int msg7,
                   int msg8 )
{
    serviceExecutor *pExe;
    pExe = getExecutor( strName );
    if ( pExe != NULL )
    {
        pExe->setUiEnables( bUiEnable,
                            msg1, msg2, msg3, msg4,
                            msg5, msg6, msg7, msg8 );
    }
}
//! all Ui En = bUiEnable except the msg1~8
void serviceExecutor::setUiEnables( bool bUiEnable,
                                    int msg1,int msg2, int msg3,int msg4,
                                    int msg5,int msg6, int msg7,int msg8 )
{
    //! build the not ary
    int notMsgsAry[8] = { msg1, msg2, msg3, msg4, msg5, msg6, msg7, msg8 };

    QList<int> notMsgs;
    for( int i = 0; i < array_count(notMsgsAry); i++ )
    {
        notMsgs.append( notMsgsAry[i] );
    }

    bool bOpEn;
    foreach( int msg, mUiMsgs )
    {
        //! in not msgs
        if ( notMsgs.contains(msg) )
        {
            bOpEn = !bUiEnable;
        }
        else
        {
            bOpEn = bUiEnable;
        }
        //! change enable
        mUiAttr.setEnable( msg, bOpEn );
    }

    defer( CMD_SERVICE_ATTR_CHANGED );
}

void serviceExecutor::setEnable( bool b, int cause )
{
    mEnable = b;
    mDisableCause = cause;
}
bool serviceExecutor::getEnable()
{
    return mEnable;
}
