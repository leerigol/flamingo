#ifndef CINENT_H
#define CINENT_H

#include "../../baseclass/cargument.h"

class CIntent : public CObj
{
public:
    CIntent();
    ~CIntent();

public:
    void setIntent( int viewMsg, int intent,
                              int cMsg, int cId );
    void setArgment( CArgument &arg );

public:
    int mMsg;                   //! 界面消息
    int mIntent;                //! 执行的动作
    CArgument mArgs ;   //! 参数:和执行的动作有关

    int mClientMsg;         //! 客户反馈消息
    int mClientPara;        //! 客户反馈标记，通过标记确定是什么任务。反馈的消息参数是int型

public:
    int mClientId;             //! 客户id
};

#endif // CINENT_H
