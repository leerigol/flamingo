#include "cintent.h"

CIntent::CIntent()
{
    mMsg = -1;
    mIntent  = -1;

    mClientMsg = -1;
    mClientPara = -1;

    mClientId = -1;
}

CIntent::~CIntent()
{}

void CIntent::setIntent( int viewMsg, int intent,
                          int cMsg, int cId )
{
    mMsg = viewMsg;
    mIntent = intent;

    mClientMsg = cMsg;
    mClientPara = cId;
}

void  CIntent::setArgment( CArgument &arg )
{
    mArgs = arg;
}
