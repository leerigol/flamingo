#ifndef SERVICE_KEY
#define SERVICE_KEY

#include <QtCore>

//! 服务自定义键的翻译过程
class serviceKey
{
public:
    serviceKey();
    serviceKey( int key, int msg, int action );
public:
    int mKey;
    int mMsg;
    int mAction;
};

typedef QList<serviceKey> servKeyList;

class serviceKeyMap : public servKeyList
{
public:
    serviceKeyMap();

public:
    void append( int key, int msg, int action );
    bool find( int key, serviceKey &servKey );
};

#endif // SERVICE_KEY

