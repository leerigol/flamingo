#ifndef ACTION_H
#define ACTION_H

#include <QtCore>

#include "../../baseclass/cargument.h"

struct struActionContext
{
    bool isUi;    /*!< if from ui no ui update needed*/
    bool isUser;  /*!< if from user, the user setting will be called*/
    bool isInit;  /*!< init the ui will be updated */

    struActionContext( bool ui = true, bool user = true, bool init=false );
    virtual void enter();
    virtual void exit( DsoErr err );
    virtual struActionContext *proxy();

    bool isUiAction();
    bool isUserAction();
    bool isInitAction();
};

struct struActionContext_Console : public struActionContext
{
    struActionContext_Console( bool ui = true, bool user = true );
    virtual void enter();
    virtual void exit( DsoErr err );
};

//! for remote used
struct struActionContextRmt : public struActionContext
{
    struActionContextRmt( bool ui = false, bool user = true );
    virtual void enter();
    virtual void exit( DsoErr err );

    virtual struActionContext *proxy();

    DsoErr mErr;
    QSemaphore *m_pSema;
    int mProxy;
};

struct struServAction
{
    int servId;             /*!< dst id */
    int servMsg;            /*!< dst msg */
                            /*!< 复合的输入参数*/
    struServPara servPara;

    struActionContext *pservContext;    /*!< action context */

    struServAction();
    struServAction( int id, int msg, const struServPara &para,  struActionContext *pContext = NULL );
    struServAction *newAction( int id, int msg, const struServPara &para, struActionContext *pContext = NULL );

    //! id,msg equal
    bool isCompany( struServAction &act );
    bool isCompany( int id, int msg ) const;

    void enter();
    void exit( DsoErr err );
};

//! 定义快捷键，顺序按下一些按键会触发一个操作
//! A,B,C --> msg
class QuickKey
{
public:
    QuickKey();

public:
    void rst();
    bool sequenceIn( int key );
    void setKey( const QList<int> &keys, int msg );

    int getMsg() const;
private:
    QList<int> mKeys;
    int mMsg;

    int mHead;
};

typedef QQueue< struServAction > servActionQueue;

#endif // ACTION_H
