#ifndef TIMER_H
#define TIMER_H

enum enumTimerAttr
{
    timer_single,
    timer_repeat,
};

struct struTimer
{
    struTimer( int sId,
               int timeOutms,
               int tId=0,
               enumTimerAttr attr=timer_single );

    int servId;
    int timeId;
    enumTimerAttr mAttr;

    int timeOut;        //! timeout period
    int ticks;          //! ticks now
};

#endif // TIMER_H
