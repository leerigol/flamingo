#ifndef SPY_H
#define SPY_H

#include <QtCore>

enum eSpyType
{
    e_spy_post = 1, /*!< 在侦听消息执行完成后执行 */
    e_spy_pre = 2,  /*!< 在侦听消息执行前执行 */

                    //! only after started
    e_spy_after = 4,

                    //! context
    e_spy_user = 0x100,

};
enum eSpyPara
{
    spy_para = 0,   //! 目标参数
    spy_id = 1,     //! 参数为id
    spy_serv_id,    //! service id
};

struct struSpyItem
{
    static int _spyId;

    int servId;   /*!< spy id */
    int msg;      /*!< spy msg */

    int dstId;    /*!< the dest id */
    int aliasMsg; /*!< the aliased msg */

    int spyId;    /*!< id */
    eSpyPara spyPara;  /*!< 是否监听参数: 0 -- 目标参数，1--spyId*/

    eSpyType spyType;

    struSpyItem( int idSpy, int spyMsg,
                 int idDst, int aliasMsg,
                 eSpyType spyType,
                 eSpyPara spyPara = spy_para
                  );

    bool operator==( struSpyItem &item );
};
typedef QList<struSpyItem* > servSpyList;


#endif // SPY_H
