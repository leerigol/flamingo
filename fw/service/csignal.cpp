#include <QDebug>
#include "csignal.h"
#include "../../include/dsodbg.h"
CSignal::CSignal( int servId, int msg, void *pServ, void *pContext ):
                  mServId(servId),mMsg(msg),m_pServ(pServ),m_pContext(pContext)
{
    mSigType = unk_changed;
}

#define signal_comp( type )  ( (*(static_cast<type*>(this))) == (*(static_cast<type*>(&var))) )
bool CSignal::equ( CSignal &var )
{
    if ( mSigType != var.mSigType )
    { return false; }

    switch( mSigType )
    {
        case value_changed:
            return signal_comp( Signal_value_changed );

        case enable_changed:
            return signal_comp( Signal_enable_changed );

        case enable_changed_opt:
            return signal_comp( Signal_enable_changed_opt );

        case visible_changed:
            return signal_comp( Signal_visible_changed );

        case visible_changed_opt:
            return signal_comp( Signal_visible_changed_opt );

        case content_changed:
            return signal_comp( Signal_content_changed );

        case context_changed:
            return signal_comp( Signal_context_changed );

        case active_changed:
            return signal_comp( Signal_active_changed );

        case active_in_changed:
            return signal_comp( Signal_active_in );

        case active_out_changed:
            return signal_comp( Signal_active_out );

        case focus_changed:
            return signal_comp( Signal_focus_changed );

        default:
            qDebug()<<var.mServId<<var.mMsg;
            Q_ASSERT( false );
        }

    return false;
}

bool CSignal::operator==( const CSignal & var )
{
    do
    {
        if ( mServId != var.mServId )
        { return false; }

        if ( mMsg != var.mMsg )
        { return false; }

        if ( m_pServ != var.m_pServ )
        { return false; }

        if ( m_pContext != var.m_pContext )
        { return false; }

    }while(0);

    return true;
}

Signal_value_changed::Signal_value_changed( int servId, int msg, void *pServ, void *pContext ) :
                     CSignal( servId, msg, pServ, pContext )
{
    mSigType = value_changed;
}

Signal_enable_changed::Signal_enable_changed(
                                              int servId,
                                              int msg,
                                              bool b,
                                              void *pServ,
                                              void *pContext ):
                        CSignal( servId, msg, pServ, pContext ),mbEnable(b)
{
    mSigType = enable_changed;
}

bool Signal_enable_changed::operator==( const Signal_enable_changed & var )
{
    do
    {
        if ( *((CSignal*)this) != *((CSignal*)&var) )
        { return false; }

        if ( mbEnable != var.mbEnable )
        { return false; }

    }while(0);

    return true;
}

Signal_enable_changed_opt::Signal_enable_changed_opt(
                           int servId, int msg, int opt, bool b, void *pServ, void *pContext ) :
                           Signal_enable_changed( servId, msg, b, pServ, pContext ),
                           mOpt(opt)
{
    mSigType = enable_changed_opt;
}

bool Signal_enable_changed_opt::operator==( const Signal_enable_changed_opt & var )
{
    do
    {
        if ( *((Signal_enable_changed*)this) != *((Signal_enable_changed*)&var) )
        { return false; }

        if ( mOpt != var.mOpt )
        { return false; }

    }while( 0 );

    return true;
}

Signal_visible_changed::Signal_visible_changed(
                        int servId, int msg, bool b, void *pServ, void *pContext ) :
                        CSignal( servId, msg, pServ, pContext ),mbVisible(b)
{ mSigType = visible_changed; }
bool Signal_visible_changed::operator==( const Signal_visible_changed & var )
{
    do
    {
        if ( *((CSignal*)this) != *((CSignal*)&var) )
        { return false; }

        if ( mbVisible != var.mbVisible )
        { return false; }

    }while(0);
    return true;
}

Signal_visible_changed_opt::Signal_visible_changed_opt(
                            int servId, int msg, int opt, bool b,  void *pServ, void *pContext ) :
                            Signal_visible_changed( servId, msg, b, pServ, pContext ), mOpt(opt)
{ mSigType = visible_changed_opt; }
bool Signal_visible_changed_opt::operator==( const Signal_visible_changed_opt & var )
{
    do
    {
        if ( *((Signal_visible_changed*)this) != *((Signal_visible_changed*)&var) )
        { return false; }

        if ( mOpt != var.mOpt )
        { return false; }

    }while(0);
    return true;
}

Signal_content_changed::Signal_content_changed( int servId, int msg, void *pServ, void *pContext ) :
                     CSignal( servId, msg, pServ, pContext )
{
    mSigType = content_changed;
}

Signal_context_changed::Signal_context_changed( int servId, int msg, void *pServ, void *pContext ) :
                     CSignal( servId, msg, pServ, pContext )
{
    mSigType = context_changed;
}

Signal_active_changed::Signal_active_changed( int servId, int msg, void *pServ, void *pContext ) :
                     CSignal( servId, msg, pServ, pContext )
{
    mSigType = active_changed;
}

Signal_active_in::Signal_active_in( int servId, int msg, void *pServ, void *pContext ) :
                     CSignal( servId, msg, pServ, pContext )
{
    mSigType = active_in_changed;
}

Signal_active_out::Signal_active_out( int servId, int msg, void *pServ, void *pContext ) :
                     CSignal( servId, msg, pServ, pContext )
{
    mSigType = active_out_changed;
}

Signal_focus_changed::Signal_focus_changed( int servId, int msg, void *pServ, void *pContext ) :
                     CSignal( servId, msg, pServ, pContext )
{
    mSigType = focus_changed;
}

SignalQueue::SignalQueue()
{}

SignalQueue::~SignalQueue()
{}

#if 0

#define _ATTACH_( T )    \
        T *pItem;\
\
        pItem = (T*)find( sigVar );\
        if ( NULL == pItem )\
        {\
            pItem = new T();\
            if ( NULL == pItem )\
            { return; }\
\
            *pItem = sigVar;\
            mSignalQueue.append( pItem );\
        }\
        else\
        {\
            mSignalQueue.removeAll(pItem);\
            mSignalQueue.append( pItem );\
        }
void SignalQueue::attach( const Signal_value_changed &sigVar )
{ _ATTACH_(Signal_value_changed); }

void SignalQueue::attach( const Signal_enable_changed &sigVar )
{ _ATTACH_(Signal_enable_changed); }
void SignalQueue::attach( const Signal_enable_changed_opt &sigVar )
//{ _ATTACH_(Signal_enable_changed_opt); }
{
    Signal_enable_changed_opt *pItem;

    pItem = (Signal_enable_changed_opt*)find( sigVar );
    if ( NULL == pItem )
    {
        pItem = new Signal_enable_changed_opt();
        if ( NULL == pItem )
        { return; }

        *pItem = sigVar;
        mSignalQueue.append( pItem );
    }
    else
    {
        mSignalQueue.removeAll(pItem);
        mSignalQueue.append( pItem );
    }
}

void SignalQueue::attach( const Signal_visible_changed &sigVar )
{ _ATTACH_(Signal_visible_changed); }
void SignalQueue::attach( const Signal_visible_changed_opt &sigVar )
{ _ATTACH_(Signal_visible_changed_opt); }

void SignalQueue::attach( const Signal_content_changed &sigVar )
{ _ATTACH_(Signal_content_changed); }
void SignalQueue::attach( const Signal_context_changed &sigVar )
{ _ATTACH_(Signal_context_changed); }

void SignalQueue::attach( const Signal_active_changed &sigVar )
{ _ATTACH_(Signal_active_changed); }
void SignalQueue::attach( const Signal_active_in &sigVar )
{ _ATTACH_(Signal_active_in); }
void SignalQueue::attach( const Signal_active_out &sigVar )
{ _ATTACH_(Signal_active_out); }

void SignalQueue::attach( const Signal_focus_changed &sigVar )
{ _ATTACH_(Signal_focus_changed); }
#endif

void SignalQueue::clear()
{
    QListIterator<CSignal*> iter( mSignalQueue );

    CSignal* pItem;
    while( iter.hasNext() )
    {
        pItem = iter.next();

        Q_ASSERT( NULL != pItem );
        delete pItem;
    }

    mSignalQueue.clear();
}

CSignal *SignalQueue::find( const CSignal &sigVar )
{
    QListIterator<CSignal*> iter( mSignalQueue );
    CSignal* pVar;
    CSignal* pItem;

    pVar = (CSignal*)( &sigVar );
    while( iter.hasNext() )
    {
        pItem = iter.next();

        if( pItem == NULL)
        {
            qDebug()<<pItem;
        }

        Q_ASSERT( NULL != pItem );
        if ( pItem->equ( *pVar ) )
        { return pItem; }
    }

    return NULL;
}
