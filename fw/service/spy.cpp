#include "spy.h"

int struSpyItem::_spyId = 1;

struSpyItem::struSpyItem( int idSpy, int spyMsg,
             int dst, int dstMsg,
             eSpyType spyT,
             eSpyPara bSpyPara
             )
{
    servId = idSpy;
    msg = spyMsg;

    dstId = dst;
    aliasMsg = dstMsg;

    spyType = spyT;
    spyPara = bSpyPara;

    spyId = struSpyItem::_spyId++;
}

bool struSpyItem::operator==( struSpyItem &item )
{
    if ( servId != item.servId )
    { return false; }

    if ( msg != item.msg )
    { return false; }

    if ( dstId != item.dstId )
    { return false; }

    if ( aliasMsg != item.aliasMsg )
    { return false; }

    if ( spyPara != item.spyPara )
    { return false; }

    if ( spyType == item.spyType )
    { return false; }

    return true;
}
