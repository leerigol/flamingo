
#include "../../engine/base/dsoengine.h"

#include "../../engine/cplatform.h"

#include "../../include/dsostd.h"

#include "service.h"
#include "service_msg.h"

/*! \var _serviceList
* - 静态变量，记录系统当前的所有服务
* - 列表在 service 的构造函数中自动添加到列表中
*/
servItemList service::_servList;
servItemList service::_activeList;
servItemList service::_cacheList;

/*! \var mautoServId
* - 当前自动累计的服务号
* - 每个服务实例都有一个独立的ID，可以指定也可以由系统自动指定
* - 系统指定的ID在 E_SERVICE_ID_AUTO 之后
*/	
int service::mautoServId = (int)E_SERVICE_ID_AUTO;

/*!
 * \brief struServItem::struServItem
 * \param name 
 * \param id
 * \param p 
 * - 服务信息项创建
 * - 服务信息项登记在列表中便于进行查询和管理
 */
struServItem::struServItem( QString name, int id, service *p )
{
    servId = id;
    servName = name;
    pService = p;
}

/*!
 * \brief service::serviceList
 * \return struItemList&
 * 
 * 返回服务的登记表
 * \note 返回的是引用
 */
servItemList & service::serviceList()
{
    return _servList;
}

void service::appendCacheList( service* s)
{
    _cacheList.append(s->mpItem);
}

servItemList& service::cacheList()
{
    return _cacheList;
}

/*!
 * \brief service::findItem
 * \param strName
 * \return struServItem *
 * \retval NULL 没有找到服务
 * \retval other 服务登记信息
 * - 查找指定名称的服务
 * - 必须在新创建服务前调用，用于判定是否存在名字冲突
 * - 服务名字区分大小写
 * \todo use iterator
 */
struServItem *service::findItem( QString strName )
{
    struServItem *pItem;

    for (int i = 0; i < _servList.size(); i++ )
    {
        pItem = _servList.at( i );
        if ( pItem->servName == strName )
        {
            return pItem;
        }
    }

    return NULL;
}
/*! \brief findItem
*
* 以服务ID查询服务
* \param id
* 
*/
struServItem *service::findItem( int id )
{
    struServItem *pItem;
    int i;

    for ( i = 0; i < _servList.size(); i++ )
    {
        pItem = _servList.at( i );
        if ( pItem->servId == id )
        {
            return pItem;
        }
    }

    return NULL;
}

service* service::findService( const QString &name )
{
    struServItem *pItem;

    pItem = findItem(name);

    if ( NULL == pItem )
    {
        qWarning()<<name;
    }
    Q_ASSERT( pItem != NULL );

    return pItem->pService;
}

service* service::findService( int id )
{
    struServItem *pItem;

    pItem = findItem( id );

    if( NULL == pItem )
    {
        qWarning() << "no service" <<id;
        //Q_ASSERT(false);
        return NULL;
    }

    return pItem->pService;
}

/*!
 * \brief service::addItem
 * \param pItem
 * 
 * 将服务登记信息添加到服务列表中
 */
void service::addItem( struServItem *pItem )
{
    _servList.append(pItem);
}

/*!
 * \brief service::active
 * \param id
 * \return
 * 将指定的服务设置为活动服务
 */
DsoErr service::active( int id )
{
    struServItem *pItem;

    pItem = findItem( id );
    Q_ASSERT( pItem!= NULL );

    //! add to first
    if ( _activeList.contains( pItem) )
    {
        //! in head now
        if ( _activeList[0] == pItem )
        {
            return ERR_SERICE_ACTIVE_NOW;
        }

        _activeList.removeOne( pItem );
    }
    _activeList.prepend( pItem );

    return ERR_NONE;
}
/*!
 * \brief service::deActive
 * \return
 * 退出活动服务状态
 * -退出之后前一个服务变成活动服务
 * -历史中只有一个服务时不能再进行回退
 */
DsoErr service::deActive()
{
    if ( _activeList.count() < 2 )
    {
        return ERR_SERICE_ACTIVE_FAIL;
    }

    _activeList.removeFirst();

    return ERR_NONE;
}

int service::getActiveService()
{
    if ( _activeList.size() > 0 )
    {
        return _activeList[0]->servId;
    }

    return E_SERVICE_ID_NONE;
}

QString service::getServiceName( int id )
{
    struServItem *pItem;

    pItem = findItem(id);
    if( NULL == pItem )
    {
        qWarning()<<id;
        Q_ASSERT(false);
    }

    return pItem->servName;
}

int service::getServiceId( const QString &name )
{
    struServItem *pItem;

    pItem = findItem( name );
    Q_ASSERT( NULL != pItem );

    return pItem->servId;
}

/*!
 * \brief service::service
 * if eId == E_SERVICE_ID_NONE, the internal id is auto increamented
 * 新建服务
 * \param servName 服务名称
 * \param eId 服务ID，如果ID指定为：E_SERVICE_ID_NONE，则自动分配
 * \sa findItem
 * \note 需要在调用前进行重名查询
 */
service::service( QString servName, int eId  )
{
    int localId;
    int servId;

    if ( eId == E_SERVICE_ID_NONE )
    {
        localId = service::mautoServId;
        servId = service::mautoServId;
        localId++;
        service::mautoServId = (int)(localId);
    }
    else
    {
        servId = eId;
    }
    mpItem = R_NEW( struServItem( servName, servId, this ) );

    addItem( mpItem );
}

service::~service()
{
    if ( NULL != mpItem )
    {
        delete mpItem;
        mpItem = NULL;
    }
}

int service::getId() const
{
    Q_ASSERT( NULL!=mpItem);
    return mpItem->servId;
}

/*! \brief 获取服务项名称
* \note 返回的是引用
*/
const QString & service::getName() const
{
    Q_ASSERT( mpItem != NULL );
    return mpItem->servName;
}

/*!
 * \brief service::start
 *
 * 启动服务
 * 服务创建只实例化了对象，通过start可以进行具体的启动工作。
 * 例如启动线程
 */
DsoErr service::start()
{
    return ERR_NONE;
}


DsoErr service::syncEngine( EngineMsg msg )
{
    EngineMsg sMsg;
    EnginePara para( 0 );

    sMsg = packMsg( msg, E_CFG_PARA_NONE );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::syncEngine( EngineMsg msg, bool val )
{
    EngineMsg sMsg;
    EnginePara para( val );

    sMsg = packMsg( msg, E_CFG_PARA_BOOL );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::syncEngine( EngineMsg msg, int val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_CFG_PARA_S32 );

    return engine::receiveMsg( sMsg, para );
}


DsoErr service::syncEngine( EngineMsg msg, quint16 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_CFG_PARA_U16 );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::syncEngine( EngineMsg msg, quint32 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_CFG_PARA_U32 );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::syncEngine( EngineMsg msg, quint64 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_CFG_PARA_U64 );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::syncEngine( EngineMsg msg, qint64 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_CFG_PARA_S64 );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::syncEngine( EngineMsg msg, int id, bool val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_CFG_PARA_INT_BOOL );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::syncEngine( EngineMsg msg, int id, int val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_CFG_PARA_INT_INT );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::syncEngine( EngineMsg msg, int id, qint64 val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_CFG_PARA_INT_S64 );

    return engine::receiveMsg( sMsg, para );
}

/*!
 * \brief service::postEngine
 * \param msg
 * \return
 * - 向引擎投递消息
 * - 消息中附带有参数
 */
DsoErr service::postEngine( EngineMsg msg )
{
    EngineMsg sMsg;
    EnginePara para( E_PARA_NONE );

    sMsg = packMsg( msg, E_PARA_NONE );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, bool val )
{
    EngineMsg sMsg;
    EnginePara para( val );

    sMsg = packMsg( msg, E_PARA_BOOL );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, int val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_S32 );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, qint64 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_S64 );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, quint8 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_U8 );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::postEngine( EngineMsg msg, quint16 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_U16 );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::postEngine( EngineMsg msg, quint32 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_U32 );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::postEngine( EngineMsg msg, quint64 val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_U64 );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::postEngine( EngineMsg msg, DsoReal val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_REAL );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, void* val )
{
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_PTR );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, int id, bool val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_INT_BOOL );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::postEngine( EngineMsg msg, int id, int val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_INT_INT );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::postEngine( EngineMsg msg, int id, qint64 val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_INT_S64 );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, int id, void *ptr )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(ptr);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_INT_PTR );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, int id,
                   int p1, int p2 )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para1(p1);
    EnginePara para2(p2);

    para.pushPara(&para1);
    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_STACK );

    return engine::receiveMsg( sMsg, para );
}
DsoErr service::postEngine( EngineMsg msg, int id,
                   qlonglong p1, qlonglong p2 )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para1(p1);
    EnginePara para2(p2);

    para.pushPara(&para1);
    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_STACK );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg,
                   qlonglong p1, qlonglong p2 )
{
    EngineMsg sMsg;

    EnginePara para(p1);
    EnginePara para1(p2);

    para.pushPara(&para1);

    sMsg = packMsg( msg, E_PARA_STACK );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::postEngine( EngineMsg msg, quint32 a,
                   quint32 b, quint32 c )
{
    EngineMsg sMsg;

    EnginePara para(a);
    EnginePara para1(b);
    EnginePara para2(c);

    para.pushPara(&para1);
    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_STACK );\

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::queryEngine( EngineMsg msg, bool &val )
{
    EngineMsg sMsg;
    EnginePara para( val );
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qBOOL );

    err = engine::receiveMsg( sMsg, para );

    val = para.bVal;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, qint32 &val )
{
    EngineMsg sMsg;
    EnginePara para(val);
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qS32 );

    err = engine::receiveMsg( sMsg, para );

    val = para.s32Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, qint64 &val )
{
    EngineMsg sMsg;
    EnginePara para(val);
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qS64 );

    err = engine::receiveMsg( sMsg, para );

    val = para.s64Val;

    return err;
}

DsoErr service::queryEngine( EngineMsg msg, quint8 &val )
{
    EngineMsg sMsg;
    EnginePara para(val);
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qU8 );

    err = engine::receiveMsg( sMsg, para );

    val = para.u8Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, quint16 &val )
{
    EngineMsg sMsg;
    EnginePara para(val);
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qU16 );

    err = engine::receiveMsg( sMsg, para );

    val = para.u16Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, quint32 &val )
{
    EngineMsg sMsg;
    EnginePara para(val);
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qU32 );

    err = engine::receiveMsg( sMsg, para );

    val = para.u32Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, quint64 &val )
{
    EngineMsg sMsg;
    EnginePara para(val);
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qU64 );

    err = engine::receiveMsg( sMsg, para );

    val = para.u64Val;

    return err;
}

DsoErr service::queryEngine( EngineMsg msg, DsoReal &val )
{
    EngineMsg sMsg;
    EnginePara para(val);
    DsoErr err;

    sMsg = packMsg( msg, E_PARA_qREAL );

    err = engine::receiveMsg( sMsg, para );

    val = para.realVal;

    return err;
}

DsoErr service::queryEngine( EngineMsg msg, void *val )
{   
    EngineMsg sMsg;
    EnginePara para(val);

    sMsg = packMsg( msg, E_PARA_qPTR );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::queryEngine( EngineMsg msg, int id, bool &val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);
    DsoErr err;

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qBOOL_INT );

    err = engine::receiveMsg( sMsg, para );

    val = para.bVal;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, int id, int &val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);
    DsoErr err;

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qS32_INT );

    err = engine::receiveMsg( sMsg, para );

    val = para.s32Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, int id, qlonglong &val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);
    DsoErr err;

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qS64_INT );

    err = engine::receiveMsg( sMsg, para );

    val = para.s64Val;

    return err;
}

DsoErr service::queryEngine( EngineMsg msg, int id, void *ptr )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(ptr);

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qINT_PTR );

    return engine::receiveMsg( sMsg, para );
}

DsoErr service::queryEngine( EngineMsg msg, int id, quint8 &val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);
    DsoErr err;

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qU8_INT );

    err = engine::receiveMsg( sMsg, para );

    val = para.u8Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, int id, quint16 &val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);
    DsoErr err;

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qU16_INT );

    err = engine::receiveMsg( sMsg, para );

    val = para.u16Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, int id, quint32 &val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);
    DsoErr err;

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qU32_INT );

    err = engine::receiveMsg( sMsg, para );

    val = para.u32Val;

    return err;
}
DsoErr service::queryEngine( EngineMsg msg, int id, quint64 &val )
{
    EngineMsg sMsg;

    EnginePara para(id);
    EnginePara para2(val);
    DsoErr err;

    para.pushPara(&para2);

    sMsg = packMsg( msg, E_PARA_qU64_INT );

    err = engine::receiveMsg( sMsg, para );

    val = para.u64Val;

    return err;
}

DsoErr service::queryEngine( EngineMsg msg, quint32 a, quint32 b, quint32 &val )
{
    EngineMsg sMsg;

    EnginePara para(a);
    EnginePara para1(b);

    DsoErr err;

    para.pushPara(&para1);

    sMsg = packMsg( msg, E_PARA_qU32_STACK );

    err = engine::receiveMsg( sMsg, para );

    val = para.u32Val;

    return err;
}

/*!
 * \brief rpcPost
 * \param pExe
 * \param cmd
 * \param para
 * \todo convert err to rpc
 * 异步的远端调用，供SCPI命令执行线程调用
 */
int rpcCall( CExecutor *pExe,
             int cmd,
             CArgument &para )
{
    service *pServ;

    //! \todo
    pServ = dynamic_cast<service*>(pExe);

    serviceExecutor::post( pServ->getName(), cmd, para );

    //! convert to scpi error
    return 0;
}
/*! \var pRpcCall
*
* 命令执行线程的调用接口
*/
serviceMgr::serviceMgr( QObject *parent ) : QThread(parent)
{
    CExecutor::setRpcCall( rpcCall );
}

void serviceMgr::run()
{
    serviceExecutor::attachThread( this );
    serviceExecutor::exec();
}
