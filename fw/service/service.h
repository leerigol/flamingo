#ifndef FW_SERVICE_H
#define FW_SERVICE_H

/*! \file
* \page service 服务实现
*/

#include <QObject>
#include <QtCore>
#include "../../include/dsotype.h"
#include "../../engine/base/engine.h"
#include "../../engine/enginemsg.h"

//! ui attributes
#include "../../com/uiattr/uiattr.h"

#include "../../baseclass/ccmdtarget.h"
#include "../../baseclass/cargument.h"

//! sub components
#include "action.h"
#include "timer.h"
#include "spy.h"
#include "cintent.h"
#include "csignal.h"

#include "service_key.h"

using namespace DsoType;
using namespace DsoEngine;

enum enumMsgAttr
{
    msg_notify = 1,     /*!< notify msg, do not modify setting */
    msg_console = 2,    //! no ui element
};

class service;

/*! \struct struServItem
* \brief struServItem
* 服务项
* - 在列表中记录服务项信息
* - 通过列表可以找到服务
* - APP不直接和具体的服务对象交互而是通过中间的服务管理器
*/
struct struServItem
{
    int servId;   /*!< service key id */
    QString servName;   /*!< service key name */

    service *pService;  /*!< service */

    struServItem( QString name, int id, service *p );
};
typedef QList<struServItem *> servItemList;

/*!
 * \brief The service class
 *
 * 服务基类
 *
 * \section serviceInfo 服务信息
 * - name: 字符名称
 * - id： 数字id,
 */
class service
{

public:
    service( QString servName, int eId = E_SERVICE_ID_NONE );
    virtual ~service();

    int getId() const;
    const QString & getName() const;
    virtual DsoErr start();

public:
    static struServItem *findItem( QString strName );
    static struServItem *findItem( int id );

    static service* findService( const QString &name );
    static service* findService( int id );

    static void addItem( struServItem *pItem );

protected:
    static servItemList _servList;      /*!< 服务列表 */
    static servItemList _activeList;    /*!< active service list */
    static servItemList & serviceList();
    static int mautoServId;             /*!< 当前自动服务号ID */

    /* Service list of needing serial out or in when Timebase mode changed*/
    static servItemList _cacheList;
    static void appendCacheList(service*);
    static servItemList& cacheList();

protected:
                                        //! service
    static DsoErr active( int id );
    static DsoErr deActive();
    static int    getActiveService();

public:
    static QString getServiceName( int id );
    static int     getServiceId( const QString &name );



protected:
    struServItem *mpItem;           /*!< 服务信息 */

public:

    //Config non FPGA
    DsoErr syncEngine( EngineMsg msg );
    DsoErr syncEngine( EngineMsg msg, bool val );
    DsoErr syncEngine( EngineMsg msg, int val );
    DsoErr syncEngine( EngineMsg msg, qint64 val );
    DsoErr syncEngine( EngineMsg msg, quint16 val );
    DsoErr syncEngine( EngineMsg msg, quint32 val );
    DsoErr syncEngine( EngineMsg msg, quint64 val );
    DsoErr syncEngine( EngineMsg msg, int id, bool val );
    DsoErr syncEngine( EngineMsg msg, int id, int val );
    DsoErr syncEngine( EngineMsg msg, int id, qint64 val );

    //! post
    DsoErr postEngine( EngineMsg msg );
    DsoErr postEngine( EngineMsg msg, bool val );
    DsoErr postEngine( EngineMsg msg, int val );
    DsoErr postEngine( EngineMsg msg, qint64 val );

    DsoErr postEngine( EngineMsg msg, quint8 val );
    DsoErr postEngine( EngineMsg msg, quint16 val );
    DsoErr postEngine( EngineMsg msg, quint32 val );
    DsoErr postEngine( EngineMsg msg, quint64 val );
    DsoErr postEngine( EngineMsg msg, DsoReal val );

    DsoErr postEngine( EngineMsg msg, void * val );

    DsoErr postEngine( EngineMsg msg, int id, bool val );
    DsoErr postEngine( EngineMsg msg, int id, int val );
    DsoErr postEngine( EngineMsg msg, int id, qint64 val );
    DsoErr postEngine( EngineMsg msg, int id, void *ptr );

    DsoErr postEngine( EngineMsg msg, int id,
                       int para1, int para2 );
    DsoErr postEngine( EngineMsg msg, int id,
                       qlonglong para1, qlonglong para2 );

    DsoErr postEngine( EngineMsg msg,
                       qlonglong para1, qlonglong para2 );

    DsoErr postEngine( EngineMsg msg, quint32 a,
                       quint32 b, quint32 c );

    //! query
    DsoErr queryEngine( EngineMsg msg, bool &val );
    DsoErr queryEngine( EngineMsg msg, qint32 &val );
    DsoErr queryEngine( EngineMsg msg, qint64 &val );

    DsoErr queryEngine( EngineMsg msg, quint8 &val );
    DsoErr queryEngine( EngineMsg msg, quint16 &val );
    DsoErr queryEngine( EngineMsg msg, quint32 &val );
    DsoErr queryEngine( EngineMsg msg, quint64 &val );

    DsoErr queryEngine( EngineMsg msg, DsoReal &val );

    DsoErr queryEngine( EngineMsg msg, void *ptr );

    DsoErr queryEngine( EngineMsg msg, int id, bool &val );
    DsoErr queryEngine( EngineMsg msg, int id, int &val );
    DsoErr queryEngine( EngineMsg msg, int id, qlonglong &val );
    DsoErr queryEngine( EngineMsg msg, int id, void *ptr );

    DsoErr queryEngine( EngineMsg msg, int id, quint8 &val );
    DsoErr queryEngine( EngineMsg msg, int id, quint16 &val );
    DsoErr queryEngine( EngineMsg msg, int id, quint32 &val );
    DsoErr queryEngine( EngineMsg msg, int id, quint64 &val );

    DsoErr queryEngine( EngineMsg msg, quint32 a, quint32 b, quint32 &val );
};

#define enter_service_context() DsoErr _err; bool _bOK = false;
#define exit_service_context()
#define query_enum( serv_name, msg, type, val ) type val; \
                                             do\
                                             {\
                                             int _iVal;\
                                             _err = query( serv_name, msg, _iVal );\
                                             _bOK = (_err == ERR_NONE); \
                                             if ( _err != ERR_NONE ) break; \
                                             val = (type)_iVal;\
                                             }while(0);

/*!
 * \brief The serviceExecutor class
 *
 * 服务执行器，扩展了消息映射表，实际服务的直接派生类
 */
class serviceExecutor : public CExecutor, public service
{
    Q_OBJECT

    DECLARE_CMD()

    DECLARE_PRE_DO()
    DECLARE_POST_DO()

protected:
    static servActionQueue _actionQueue;        /*!< 消息队列 */
    static bool _actionQueueInited;
    static QMutex _actionQueueMutex;            /*!< 消息队列mutex，用于消息队列的add,delete */
    static QMutex _servExecMutex;               /*!< 执行中的mutex,用于消息队列的依次执行和send*/

    static QSemaphore _servExecSema;

    static servActionQueue _historyQueue;       /*!< 历史记录 */
    const static int _historyLength = 32;

    const static int _actionExeInterval = 2;    /*!< 消息循环空时的等待间隔 */

    static servSpyList _servSpyPreList;         /*!< 预执行监视队列 \sa servSpyList */
    static servSpyList _servSpyPostList;        /*!< 后执行监视队列 \sa servSpyList */

    const static QString _dumyServName;         /*!< 哑服务名*/

    static QList<struTimer*> _timerList;        /*!< 时钟列表*/

    static QList<int> _broadcastList;           /*!< 广播消息列表,广播消息不是必须处理的*/
    static QList<int> _actionFilterList;        /*!< action filter */

    static SignalQueue _signalQueue;
    static QMutex _signalQueueMutex;

    static QThread *_execThread;
    static DsoWorkMode _workMode;

    static bool     m_bAppUpdate;

protected:
    static struActionContext _contextInit;
    static struActionContext _contextInternal;
    static struActionContext _contextUi;

public:
    static struActionContextRmt _contextRmt;    //! for remote used

public:
    enum eExeStatus
    {
        e_unk,
        e_init,
        e_loop,
        e_exit,
    };

    static eExeStatus _exeStat;
    static void setExeStatus( eExeStatus stat );
    static eExeStatus getExeStatus();

    static void lockExec();
    static void unlockExec();

    static void pendUi();
    static void releaseUi();

    static void attachThread( QThread *pThread );

public:
    static bool queryActive( const QString &servName );

    static bool keyEater( int key, int count, bool bRelease = true );
    static bool keyTranslate(  int key,
                               QString &servName,
                               int &msg,
                               int &controlAction
                            );
    static bool quickKeyTranslate( int key, int intervalTime );

public:
    // Only single message after post
    static bool   checkRepeat(const int id,
                              const int msg);

    static DsoErr postSingle( const int id,
                              const int msg,
                              const int iVal,
                              struActionContext *pContext = &_contextUi );

    //! user post
    static DsoErr post( const QString &servName,
                        int msg,
                        const bool bVal,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        const int iVal,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        const QString &val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        pointer val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        const qlonglong val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        CObj *val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        DsoReal &val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        dsoReal &val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr post( const QString &servName,
                        int msg,
                        const struServPara &para,
                        struActionContext *pContext = &_contextUi );

    //! user send
    static DsoErr send( const QString &servName,
                        int msg,
                        const bool bVal,
                        struActionContext *pContext = &_contextUi );

    static DsoErr send( const QString &servName,
                        int msg,
                        const int iVal,
                        struActionContext *pContext = &_contextUi );

    static DsoErr send( const QString &servName,
                        int msg,
                        const QString &val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr send( const QString &servName,
                        int msg,
                        pointer val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr send( const QString &servName,
                        int msg,
                        const qlonglong val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr send( const QString &servName,
                        int msg,
                        DsoReal &val,
                        struActionContext *pContext = &_contextUi );

    static DsoErr send( const QString &servName,
                        int msg,
                        const struServPara &para,
                        struActionContext *pContext = &_contextUi );

    //! query
    static DsoErr query( const QString &servName, int msg, bool &val );
    static DsoErr query( const QString &servName, int msg, int &iVal );
    static DsoErr query( const QString &servName, int msg, float &val );
    static DsoErr query( const QString &servName, int msg, QString &val );

    static DsoErr query( const QString &servName, int msg, qlonglong &val );
    static DsoErr query( const QString &servName, int msg, pointer &val );
    static DsoErr query( const QString &servName, int msg, DsoReal &val );
    static DsoErr query( const QString &servName,
                         int msg,
                         CArgument &valIn,
                         CArgument &valOut );

    static DsoErr query( const QString &servName, int msg, struServPara &para );

    //! broadcast
    static DsoErr broadcast( int msg,
                             const struServPara &para,
                             struActionContext *pContext = NULL );

    //! scan
    static DsoErr scanLau( const QString &servName, int msg, CArgument &arg );

public:
    //! by id -- internal using
    static DsoErr post( const int eId,
                        int msg,
                        const int iVal,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr post( const int eId,
                        int msg,
                        const bool bVal,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr post( const int eId,
                        int msg,
                        const QString &val,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr post( const int eId,
                        int msg,
                        const qlonglong val,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr post( const int eId,
                        int msg,
                        pointer val,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr post( const int eId,
                        int msg,
                        CArgument &arg,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr post( const int eId,
                        int msg,
                        DsoReal &val,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr post( const int eId,
                        int msg,
                        CObj *val,
                        struActionContext *pContext = &_contextInternal );

    static DsoErr query( const int eId,
                         int msg,
                         QString &val);

protected:
    //! async
    DsoErr async( int msg,
                  const bool bVal,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=NULL);

    DsoErr async( int msg,
                  const int iVal,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=NULL );

    DsoErr async( int msg,
                  const QString &val,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=NULL );

    DsoErr async( int msg,
                  const qlonglong val,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=NULL );

    DsoErr async( int msg,
                  CObj *pObj,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=NULL );

    DsoErr async( int msg,
                  const struServPara &para,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext =NULL );

    DsoErr async( int msg,
                  DsoReal &val,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext =NULL );

    DsoErr defer( int msg,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=&_contextInternal);

    ///
    /// \brief id_async
    /// \param servId
    /// \param msg
    /// \param bVal
    /// \param pContext
    /// \return
    ///
    DsoErr id_async( int servId,
                     int msg,
                     const bool bVal,
                     struActionContext *pContext=&_contextInternal);

    DsoErr id_async( int servId,
                     int msg,
                     const int iVal,
                     struActionContext *pContext=&_contextInternal );

    DsoErr id_async( int servId,
                     int msg,
                     const QString &val,
                     struActionContext *pContext=&_contextInternal );

    DsoErr id_async( int servId,
                     int msg,
                     const qlonglong val,
                     struActionContext *pContext=&_contextInternal );

    DsoErr id_async( int servId,
                     int msg,
                     CObj *pObj,
                     struActionContext *pContext=&_contextInternal );

    DsoErr id_async( int servId,
                     int msg,
                     DsoReal &val,
                     struActionContext *pContext=&_contextInternal );

    DsoErr id_async( int servId,
                     int msg,
                     struActionContext *pContext=&_contextInternal);

    //! ssync
    DsoErr ssync( int msg,
                  const bool bVal,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=&_contextInternal);

    DsoErr ssync( int msg,
                  const int iVal,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=&_contextInternal);

    DsoErr ssync( int msg,
                  const QString &val,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=&_contextInternal);

    DsoErr ssync( int msg,
                  const qlonglong val,
                  const QString &servName=_dumyServName,
                  struActionContext *pContext=&_contextInternal);

    DsoErr incur( int msg,
                  const QString &servName=_dumyServName,
                 struActionContext *pContext=&_contextInternal);

    const QString &selectName( const QString &str );
    const QString &selectName( int servId );
    struActionContext * selectContext( struActionContext *pContext );
public:
    static DsoErr broadcast( int msg, int val=0 );

    static ui_attr::uiProperty * queryUiProperty( const QString &servName,
                                                  int msg );

    /*!
     * \brief spyOn
     * \param servName
     * \param msg
     * \param spyName
     * \param aliasMsg
     * \param eSpyType
     * \return 返回spy handle, -1 -- fail
     */
    static int spyOn( const QString &servName, int msg,
                         const QString &spyName, int aliasMsg,
                         eSpyType spyType = e_spy_post,
                         eSpyPara spyPara = spy_para );

    static int spyOn( int id, int msg,
                         int selfId, int aliasMsg,
                         eSpyType spyType = e_spy_post,
                         eSpyPara spyPara = spy_para );

    static DsoErr spyOff( const QString &servName, int msg,
                       const QString &spyName,
                       eSpyType spyType = e_spy_post,
                       eSpyPara spyPara = spy_para );

    static DsoErr spyOff( int id, int msg,
                         int selfId,
                         eSpyType spyType = e_spy_post,
                         eSpyPara spyPara = spy_para );

    static DsoErr exec();
    static void doQueue();
    static void flushQueue();

    static bool isIdle();
    static bool waitIdle( int ms = 2000000 );

    static QObject *qobject( const QString &servName );

protected:
    static bool actionBypass( const struServAction &action );
    static void tickIt( int ticks );
    static void timerTick( int ticks );

protected:
    static DsoErr post( int id,
                        int msg,
                        const struServPara &para,
                        struActionContext *pContext = NULL );

    static DsoErr send( int id,
                        int msg,
                        const struServPara &para,
                        struActionContext *pContext = NULL );

    static DsoErr query( int id,
                         int msg,
                         struServPara &para );

    static DsoErr query( int id,
                         int msg,
                         struServPara &para,
                         CArgument &paraOut );

    static DsoErr spyPost( int id, int msg,
                           const struServPara &para,
                           servSpyList &spyList,
                           struActionContext *pContext );
    static DsoErr spySend( int id, int msg,
                           const struServPara &para,
                           servSpyList &spyList,
                           struActionContext *pContext );

    static DsoErr exec( const struServAction &action );

    static serviceExecutor * getExecutor( int id );
    static serviceExecutor * getExecutor( const QString &name );

    static struSpyItem *findSpyItem( int &servId,
                                     int msg,
                                     int &dstId,
                                     eSpyType spyType,
                                     eSpyPara spyPara,
                                     servSpyList &list
                                     );
    static servActionQueue &history();
public:
    static void clearHistory();
protected:
    static void logInHistory( struServAction &action );
    static bool historyFilter( struServAction &anAction );

    static bool isNotifyMsg( int msg );

public:
    serviceExecutor( QString name, int eId = E_SERVICE_ID_NONE );
    virtual ~serviceExecutor();
    virtual void registerSpy();

protected:
    void baseInit();
    void enterContext( struServAction &action );
    void exitContext( struServAction &action );
public:
    static DsoErr scanLau( int msg, CArgument &arg, int servId );
    static DsoErr scanLau( int msg, qlonglong &arg, int servId );

Q_SIGNALS:
    void sig_value_changed( int msg,
                            void *pServ=NULL,
                            void *pActionContext=NULL );

    void sig_enable_changed( int msg,
                             bool b,
                             void *pServ=NULL,
                             void *pActionContext=NULL );
    void sig_visible_changed( int msg,
                              bool b,
                              void *pServ=NULL,
                              void *pActionContext=NULL );

    void sig_enable_changed( int msg, int opt, bool b,
                             void *pServ=NULL,
                             void *pActionContext=NULL );
    void sig_visible_changed( int msg, int opt, bool b,
                              void *pServ=NULL,
                              void *pActionContext=NULL );

    void sig_content_changed( int msg,
                              void *pServ=NULL,
                              void *pActionContext=NULL );
    void sig_context_changed( int msg,
                              void *pServ=NULL,
                              void *pActionContext=NULL );

    void sig_active_changed( int active,
                             void *pServ=NULL,
                             void *pActionContext=NULL );

    void sig_active_in( int msg,
                        void *pServ = NULL,
                        void *pActionContext=NULL );
    void sig_active_out( int msg,
                         void *pServ = NULL,
                         void *pActionContext=NULL );

    void sig_focus_changed( int msg );

protected:
    static void procSignalQueue();
    void doSignal( CSignal *pSignal );

protected:
    void linkChange( int keyMsg,
                     int l1,
                     int l2 = 0,
                     int l3 = 0,
                     int l4 = 0,
                     int l5 = 0,
                     int l6 = 0,
                     int l7 = 0,
                     int l8 = 0
                     );

    void emitChanges( const struCmdEntry *pEntry,
                      struActionContext *pActionContext );

    virtual bool filter(  struServAction &action,
                          struServPara &curPara );

    //! make a filter list
    void installActionFilter();
    void uninstallActionFilter();

    DsoErr updateUi( const struServAction &action, struServPara &prePara );
    DsoErr updateAllUi();
    DsoErr updateAllUi( bool bFlush );
    DsoErr updateEntry( const struCmdEntry *pEntry );

    void updateUiAttr();

    void startTimer( int timeoutms, int id = 0, enumTimerAttr attr=timer_single );
    void stopTimer( int id = 0 );

    int spyOn( const QString &servName, int msg,
                         int aliasMsg,
                         eSpyType eSpyType = e_spy_post,
                         eSpyPara spyPara = spy_para );

    int spyOn( int id, int msg,
                         int aliasMsg,
                         eSpyType eSpyType = e_spy_post,
                         eSpyPara spyPara = spy_para );

    int spyOn( const QString &servName, int msg,
                         eSpyType eSpyType = e_spy_post,
                         eSpyPara spyPara = spy_para );

    int spyOn( int id, int msg,
                         eSpyType eSpyType = e_spy_post,
                         eSpyPara spyPara = spy_para );

    DsoErr startIntent( const QString &servName, CIntent &intent );
    DsoErr returnIntent( CIntent &intent, int ret );

protected:
    DsoErr deActive();

protected:
    DsoErr setActive( int active=1 );
    DsoErr setdeActive( int para=0 );
    DsoErr deactiveActive( int actId );
    bool isActive();

    DsoErr onIntent( CIntent *pIntent );

    void active_in( int msg );
    void active_out( int msg );

    DsoErr onViewChange( int val );

    DsoErr on_enterActive( int id );
    DsoErr on_exitActive( int id );

    DsoErr on_attrChange();

    void on_pre_do( CArgument &arg );
    void on_post_do( CArgument &arg );

    void on_pre_set_active();

    ui_attr::uiProperty *getProperty( int msg );
public:
    ui_attr::uiAttr *getAttr();
public:
    virtual DsoErr post( int msg, const struServPara &para );
    virtual DsoErr send( int msg, const struServPara &para );
    virtual DsoErr query( int msg, struServPara &outPara );
    virtual DsoErr query( int msg, CArgument &para, CArgument &outPara );

protected:

    //! by pass all key events
    virtual bool keyEat( int key, int count, bool bRelease );

    //! physical key translator
    virtual bool keyTranslate( int key,
                            int &msg,
                            int &controlAction );

    //! action filter ui context
    virtual bool actionFilter( const struServAction &action );

    //! key sequence
    bool quickKeyTranslate( int key );

    //! msg filter
    bool msgFilter( const QString &servName, int msg );

    DsoErr doSet( const struServAction &action );
    DsoErr doGet( int msg, struServPara &para );
    DsoErr doGet( int msg, CArgument &para, CArgument &outPara );

protected:
    bool uiActionFilter( const struServAction &action,
                         DsoErr *pCause );

    bool isInUiContext();

    void setuiEnable( bool b );
    void setuiVisible( bool b );

    void setuiEnable( int opt, bool b );
    void setuiVisible( int opt, bool b );

    void setuiStep( qlonglong step );
    void setuiIStep( qlonglong step );
    void setuiDStep( qlonglong step );

    void setuiZMsg( int msg );
    void setuiZVal( qlonglong z );
    void setuiZVal( int z );
    void setuiZVal( dsoReal &real );
    void setuiZVal( DsoRealType_A a, DsoE e);

    void setuiMinVal( qlonglong v );
    void setuiMinVal( int v );
    void setuiMinVal( dsoReal &real );
    void setuiMinVal( DsoRealType_A a, DsoE e);

    void setuiMaxVal( qlonglong v );
    void setuiMaxVal( int V );
    void setuiMaxVal( dsoReal &real );
    void setuiMaxVal( DsoRealType_A a, DsoE e);

    void setuiRange( int minVal, int maxVal, int zVal );
    void setuiRange( qlonglong minVal, qlonglong maxVal, qlonglong zVal );

    void setuiRange( dsoReal &minVal, dsoReal &maxVal, dsoReal &zVal );
    void setuiRange(
                     DsoRealType_A mina, DsoE mine,
                     DsoRealType_A maxa, DsoE maxe,
                     DsoRealType_A za, DsoE ze
                     );


    void setuiUnit( DsoType::Unit unit );
    void setuiFmt( DsoType::DsoViewFmt fmt );

    void setuiAcc(  key_acc::KeyAcc acc );

    void setUiStick( int val );
    void setUiStick( qlonglong val );

    void setUiStick( QList<int> &listVal );
    void setUiStick( QList<qlonglong> &listVal);

    void setuiBase( float base );
    void setuiBase( DsoReal real );
    void setuiBase( DsoRealType_A a, DsoE e );
    void setuiPreStr(  const QString &str );
    void setuiPostStr( const QString &str );

    void setuiOutStr( const QString &str );
    void setuiNodeCompress( bool b );

    void setuiMaxLength( int len );
    void setuiKeyRequest( CKeyRequest &keyReq );

    void setuiChange( int msg );
    void setuiFocus( int msg );

protected:
    DsoErr checkRange( int msg,
                       struServPara &iPara,
                       struServPara &qPara );

    DsoErr checkRange( qint32 &val, qint32 mi, qint32 ma );
    DsoErr checkRange( qint64 &val, qint64 mi, qint64 ma );

protected:
    void reset();

    void setMsgAttr( int msg, enumMsgAttr attr = msg_notify );
    bool isNotify( int msg );
    bool isConsole( int msg );

    DsoErr appendQuickKey( const QList<int> &key, int msg );
    void rstQuickKey();

private:
    void collectUiMsg();

private:
    typedef QMap<int,bool> UiEnMap;
    void saveUiEnMap( UiEnMap *pMap );
    void loadUiEnMap( UiEnMap *pMap );

public:
    void saveUiEns( int id );
    void restoreUiEns( int id );
    void deleteUiEns( int id );

    void saveUiEns( int id, QStringList &servList );
    void restoreUiEns( int id, QStringList &servList );
    void deleteUiEns( int id, QStringList &servList );

    void setUiEnables( const QStringList &strList,
                       bool bUiEnable,
                       int msg1 = 0,
                       int msg2 = 0,
                       int msg3 = 0,
                       int msg4 = 0,
                       int msg5 = 0,
                       int msg6 = 0,
                       int msg7 = 0,
                       int msg8 = 0 );

    void setUiEnables( const QString &strName,
                       bool bUiEnable,
                       int msg1 = 0,
                       int msg2 = 0,
                       int msg3 = 0,
                       int msg4 = 0,
                       int msg5 = 0,
                       int msg6 = 0,
                       int msg7 = 0,
                       int msg8 = 0 );
    void setUiEnables( bool bUiEnable,
                       int msg1 = 0,
                       int msg2 = 0,
                       int msg3 = 0,
                       int msg4 = 0,
                       int msg5 = 0,
                       int msg6 = 0,
                       int msg7 = 0,
                       int msg8 = 0 );

protected:
    void setEnable( bool b, int disCause = ERR_ACTION_DISABLED );
    bool getEnable();

protected:
    ui_attr::uiAttr mUiAttr;
    bool mEnable;
    int mDisableCause;
    bool mbUiCache;

    int mCurrentMsg;
    struActionContext *m_pActiveContext;

    QMap< int, int > mNotifyMsgList;
    QList<QuickKey*> mQuickKeyList;

    //! key transeMap
    serviceKeyMap mServKeyMap;

    //! ui msgs
    QList<int> mUiMsgs;

    QMap< int, UiEnMap*> mUiEnStack;
};

#define Executor( exe )     serviceExecutor::getExecutor(exe)

#define enter_service_context() DsoErr _err; bool _bOK = false;
#define exit_service_context()
#define query_service_enum( serv_name, msg, type, val )  \
                                             type val; \
                                             do\
                                             {\
                                             int _iVal;\
                                             _err = serviceExecutor::query( serv_name, msg, _iVal );\
                                             _bOK = (_err == ERR_NONE);\
                                             if ( _err != ERR_NONE ) break; \
                                             val = (type)_iVal;\
                                             }while(0);

int rpcCall( void *pExe, int cmd,  CArgument &para );

/*!
 * \brief The serviceMgr class
 *
 * - 服务管理器，依次提取服务消息队列中的消息并执行。
 * - 在一个单独的线程中运行
 */
class serviceMgr : public QThread
{
    Q_OBJECT
public:
    serviceMgr( QObject *parent = 0 );
public:
    void run();
};





#endif // SERVICE_H
