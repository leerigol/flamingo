#include <QApplication>
#include <QDebug>
#include <QMetaEnum>
#include <QTime>
#include "../include/dsodbg.h"
#include "./rdsoapplication.h"
#include "../meta/crmeta.h"

#include "../app/appmain/cappmain.h"

#include "../service/service.h"
#include "../service/servicefactory.h"

//! keyproc
#include "../com/keyevent/ckeyproc.h"

#include "../service/servscpi/servscpidso.h"
#include "../service/servlicense/servlicense.h"
#include "../service/servcal/servcal.h"     //! export cal info

#ifdef _SIMULATE


#define license_path   "/home/disk1/code/flamingo/framework/license/"
#define pubkey_path   "/home/disk1/code/flamingo/framework/license/"
#define meta_path   "/home/disk1/code/flamingo/framework/meta/platform_a/"
#define file_path   "/home/disk1/code/flamingo/framework/resource/"


#else
#define meta_path     "/rigol/resource/"
#define file_path     "/rigol/resource/"
#define license_path  "/rigol/data/"
#define pubkey_path   "/rigol/data/"
#endif

//#define def_time()          QTime timeTick;
//#define start_time()        timeTick.start();
//#define end_time()          qDebug()<<__FUNCTION__<<__LINE__<<timeTick.elapsed()<<"ms";
//#define dbgout()            qWarning()<<__FUNCTION__ <<__LINE__<<__DATE__<<__TIME__;

#define def_time()
#define start_time()
#define end_time()
#define dbgout()


//! 参数顺序无关
//! see
//! main -cfg -default -notrace -noical -ds8000  -nointer
int main( int argc, char * argv[] )
{
    //! check input
    if ( argc < 2 )
    {
        qWarning()<<"para needed";
        return 0;
    }
    def_time();

    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF8"));

    RDsoApplication a( argc, argv );

//    qDebug()<<__FUNCTION__<<"cursor"<<QThread::currentThread();
//    for(int i = 1; i < 14;i++)
//    {
//        signal(i, handler);
//    };

    //! sys argument
    sysSetArg( argc, argv );

    //! config arg
    if ( sysCmpArg( 6, "-nointer") == 0 )
    {
        CVertical::setInterleaveEn( false );
    }

    #ifdef _SIMULATE
        a.setPalette(Qt::black);
    #else
        a.setPalette(QColor(0xC8,0x98,0x60));
    #endif

    //! meta
    sysSetResourcePath( file_path );

    r_meta::CRMeta::setMetaFile( meta_path"dsometa.xml" );
    r_meta::CBMeta::setMetaFile( meta_path"boardmeta.xml");
    r_meta::CServMeta::setMetaFile( meta_path"servmeta.xml");
    r_meta::CAppMeta::setMetaFile( meta_path"appmeta.xml" );

    menu_res::RMenuBuilder::attachResource();

    //! license
    servLicense::setLicensePath( license_path );
    servLicense::setKeyPath( pubkey_path );

    //! start key scan
    app_key_board::CKeyProc keyProc;
    keyProc.start();dbgout();

    //! -- services
    serviceMgr *mgr = new serviceMgr();

    start_time();
    serviceFactory::createServices();
    serviceFactory::registerSpy();

    //! move to service thread
    serviceFactory::attachServices( mgr );
    end_time();

    start_time();
    //! window
    CAppMain mW;

    //! apps    
    mW.createApps();
    end_time();

    start_time();
    mW.setupUi();//cost more time. need to costdown
    end_time();

    start_time();
    mW.buildConnection();
    end_time();

    //！将区域绘制到顶层
    serviceExecutor::post( serv_name_utility,
                           CMD_ZONE_TOUCH_ENABLE,
                           (int)1 );

    //! active utility first
    serviceExecutor::post( serv_name_utility,
                           CMD_SERVICE_ACTIVE,
                           (int)1 );

    start_time();
    //! start working
    mgr->start();

    //! wait until mgr
    while( serviceExecutor::getExeStatus()
           != serviceExecutor::e_loop )
    {
        QThread::msleep(1);
    }
    end_time();

    //! start service
    start_time();
    serviceFactory::startServices();
    serviceFactory::startup();
    end_time();


    start_time();
    //! wait start completed
    serviceExecutor::waitIdle( -1 );
    end_time();

    serviceFactory::serviceup();

    QResizeEvent *pEvt = new QResizeEvent( QSize(1024,600), QSize(0,0) );
    qApp->postEvent( &mW,pEvt);

    start_time();
    //! show
    mW.show();
    end_time();


    #ifdef _SIMULATE
        mW.move(200,100);
    #else
        mW.resize(1024,600);
    #endif


    start_time();
    a.processEvents();

    sysSetStarted();
    end_time();

    {
        serviceExecutor::clearHistory();

        //! init the event filter
        REventFilterMgr::rst();


        //! active utility on start
        serviceExecutor::post( serv_name_help,
                               CMD_SERVICE_ACTIVE,
                               (int)1 );
    #ifdef USE_CBB_LXI
        // start to listen the network
        serviceExecutor::post( serv_name_utility_IOset,
                               servInterface::cmd_net_init,
                               (int)0 );
    #endif
        //! power on event
        sysSetScpiEvent(scpi_event_power_on,1);
    }


    CDelayStart ds;
    ds.start();

    int ret = a.exec();
    return ret;
}
