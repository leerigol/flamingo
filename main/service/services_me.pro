TEMPLATE = subdirs

platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
}

#SUBDIRS += ../../service/servmeasure
#SUBDIRS += ../../service/servcounter
#SUBDIRS += ../../service/servdvm
#SUBDIRS += ../../service/servDG
#SUBDIRS += ../../service/servHotplug

#SUBDIRS += ../../service/servdisplay
#SUBDIRS += ../../service/servlicense
#SUBDIRS += ../../service/servwfmdata
#SUBDIRS += ../../service/servstorage

#SUBDIRS += ../../service/servplot

#SUBDIRS += ../../service/servquick
#SUBDIRS += ../../service/servutility
#SUBDIRS += ../../service/servcursor
#SUBDIRS += ../../service/servref


#SUBDIRS += ../../service/servdecode
#SUBDIRS += ../../service/servmask
#SUBDIRS += ../../service/servscpi


#SUBDIRS += ../../service/servmath
#SUBDIRS += ../../service/servSearch

#SUBDIRS += ../../service/servhelp
#SUBDIRS += ../../service/serveyejit

#SUBDIRS += ../../service/servUPA

SUBDIRS += ../../service/servtrig

#SUBDIRS += ../../service/servgui
SUBDIRS += ../../service/servdso
SUBDIRS += ../../service/servtrace

SUBDIRS += ../../service/servvertmgr

SUBDIRS += ../../service/servcal
SUBDIRS += ../../service/servauto
SUBDIRS += ../../service/servRecord
SUBDIRS += ../../service/servch
SUBDIRS += ../../service/servla
SUBDIRS += ../../service/servhori

#SUBDIRS += ../../service/servmenutest
