TEMPLATE = subdirs

platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
}

SUBDIRS += ../../service/servmeasure
SUBDIRS += ../../service/servmemory
SUBDIRS += ../../service/servcounter
SUBDIRS += ../../service/servdvm

SUBDIRS += ../../service/servdisplay
SUBDIRS += ../../service/servlicense
SUBDIRS += ../../service/servwfmdata

SUBDIRS += ../../service/servstorage
SUBDIRS += ../../service/servdso

SUBDIRS += ../../service/servmenutest
SUBDIRS += ../../service/servgui
SUBDIRS += ../../service/servplot

SUBDIRS += ../../service/servquick

SUBDIRS += ../../service/servutility
SUBDIRS += ../../service/servcursor
SUBDIRS += ../../service/servla
SUBDIRS += ../../service/servref

SUBDIRS += ../../service/servvertmgr
SUBDIRS += ../../service/servdecode
SUBDIRS += ../../service/servmask
SUBDIRS += ../../service/servscpi


SUBDIRS += ../../service/servDG
SUBDIRS += ../../service/servmath
SUBDIRS += ../../service/servSearch

SUBDIRS += ../../service/servHotplug

SUBDIRS += ../../service/servhelp
SUBDIRS += ../../service/serveyejit

SUBDIRS += ../../service/servUPA

SUBDIRS += ../../service/servtrig

SUBDIRS += ../../service/servtrace

SUBDIRS += ../../service/servauto

SUBDIRS += ../../service/servRecord
SUBDIRS += ../../service/servcal
SUBDIRS += ../../service/servch
SUBDIRS += ../../service/servvert
SUBDIRS += ../../service/servhori

SUBDIRS += ../../service/servHisto

