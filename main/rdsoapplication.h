#ifndef RDSOAPPLICATION_H
#define RDSOAPPLICATION_H

#include <QApplication>
#include <QThread>

class REventObject : public QObject
{
    Q_OBJECT
public:
    REventObject();
    ~REventObject();

    bool eventFilter(QObject *, QEvent *);
};

class RDsoApplication : public QApplication
{
    Q_OBJECT
public:
    RDsoApplication( int & argc, char ** argv );
protected:
//    virtual bool event( QEvent * e );

protected Q_SLOTS:
    void onTimeout();

protected:
    void keyOwnerProc( QWidget *pre, QWidget *pnext );
    void ledShow( int led, bool show );


protected:
    QTimer *m_pTimer;
    QWidget *m_pPreWidget;

private:
    REventObject m_EventFilter;
};

class CDelayStart : public QThread
{
public:
    CDelayStart();

protected:
    void run();
};

#endif // RDSOAPPLICATION_H
