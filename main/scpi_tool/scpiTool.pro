#-------------------------------------------------
#
# Project created by QtCreator 2016-11-14T16:10:22
#
#-------------------------------------------------

QT       -= gui
QT       += core
QT       += gui
QT       += widgets
QT       += network
QT       += xml

#***************** static lib************************
#TARGET = ../../framework/lib$$(PLATFORM)/scpi/scpiTool
#TEMPLATE = lib     #定义生成为库

#DEFINES += SCPITOOL_LIBRARY
#CONFIG += staticlib

#***************** dll lib************************
#TARGET = ../../framework/lib$$(PLATFORM)/scpi/scpiTool
#TEMPLATE = lib     #定义生成为库
#DEFINES += SCPITOOL_LIBRARY

#******************app**************************
TARGET   =  Tool
TEMPLATE =  app

#设置中间文件存放路劲
OBJECTS_DIR = build


SOURCES +=  \
    ../../../tool/scpiTool/main.cpp \
    ../../../tool/scpiTool/scpitool.cpp

HEADERS += \
    ../../../tool/scpiTool/include.h \
    ../../../tool/scpiTool/scpitool.h \
    ../../com/scpiparse/scpiStruct.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

