#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#include "rdsoapplication.h"

#include "../menu/eventfiltermgr/reventfiltermgr.h"
#include "../menu/rmenus.h"

#include "../com/keyevent/rdsokey.h"

#include "../service/service.h"
#include "../service/service_msg.h"
#include "../service/servutility/servutility.h"


RDsoApplication::RDsoApplication( int & argc, char ** argv)
                 :QApplication( argc, argv )
{
    m_pTimer = new QTimer( this );
    Q_ASSERT( NULL != m_pTimer );

    connect( m_pTimer, SIGNAL(timeout()),
             this, SLOT(onTimeout()) );

    m_pPreWidget = NULL;

    m_pTimer->start( 1000 );
    m_pTimer->setSingleShot( false );

    this->installEventFilter(&m_EventFilter);
}

void RDsoApplication::onTimeout()
{
    QWidget *pOwnerNext;

    pOwnerNext = REventFilterMgr::findOwner( R_Pkey_FZ );

    keyOwnerProc( m_pPreWidget, pOwnerNext );

    m_pPreWidget = pOwnerNext;
}

void RDsoApplication::keyOwnerProc( QWidget *pre, QWidget *pnext )
{
    if ( sysGetStarted() )
    {}
    else
    { return; }

    //! changed
    if ( pre != pnext )
    {
        if ( pnext != NULL )
        {
            menu_res::RControl *pControl;
            pControl = dynamic_cast< menu_res::RControl *>(pnext);
            if ( pControl != NULL )
            {
                //qDebug()<<pControl->getTitleString()<<__FUNCTION__<<__LINE__;
            }
        }

        //! led
        ledShow( DsoEngine::led_func, pnext != NULL && pnext->isVisible() );
    }
}

void RDsoApplication::ledShow( int led, bool b )
{
    //! led control
    CArgument arg;

    arg.append( led );
    arg.append( b );
    serviceExecutor::post( E_SERVICE_ID_DSO,
                           CMD_SERVICE_LED_REQUESET,
                           arg );
}


REventObject::REventObject()
{
}

REventObject::~REventObject()
{
}

bool REventObject::eventFilter(QObject *obj, QEvent *event)
{
    Q_UNUSED(obj);
    if( !sysGetTouchEnable() )
    {
        QEvent::Type t = event->type();
        if(t == QEvent::TouchBegin ||
         t == QEvent::TouchUpdate ||
         t == QEvent::TouchEnd ||
         t == QEvent::MouseMove ||
         t == QEvent::MouseButtonPress ||
         t == QEvent::MouseButtonRelease ||
         t == QEvent::MouseButtonDblClick)
        {
            //qDebug() << event;
            event->accept();
            return true;
        }
    }
    return false;
}


#define DPU_BT_BASEADDR   0x1FDA8000//for logo

#include "../service/servscpi/servscpidso.h"
#include "../service/servplot/servplot.h"
CDelayStart::CDelayStart()
{}

void CDelayStart::run()
{
    sleep(6);

    /* Open display layer */
    {
        serviceExecutor::send( serv_name_plot,
                               servPlot::cmd_open_layer,
                               DPU_Layer_Fore );

        //servUtility::forceHDMI();
    }

    sleep(1);

    /* Load driver after service is idle */
    //!初始化USB TMC连接
    serviceExecutor::post( E_SERVICE_ID_SCPI,
                           dsoServScpi::cmd_usbTmc_newconnected, 0);
    //!初始化USB GPIB连接
    serviceExecutor::post( E_SERVICE_ID_SCPI,
                           dsoServScpi::cmd_usbgpib_newconnected, 0);
}
