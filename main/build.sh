QMAKE=/home/rigolee/workspace/3rd/Qt5.5/bin/qmake

$QMAKE flamingo/flamingo.pro -r -spec linux-arm-gnueabi-g++ CONFIG+=debug

cd flamingo
make clean
make -j 16
cd ..

$QMAKE flamingo_console/flamingo_console.pro -r -spec linux-arm-gnueabi-g++ CONFIG+=debug

cd flamingo_console

touch main.cpp
rm flamingo_console
make -j 16

arm-xilinx-linux-gnueabi-strip  flamingo_console
cp flamingo_console ~/workspace/flamingo/images/rigol
