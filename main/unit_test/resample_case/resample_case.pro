#-------------------------------------------------
#
# Project created by QtCreator 2016-10-14T08:59:58
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = resample_case
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += ../../../baseclass/resample

SOURCES += main.cpp
