#include <QCoreApplication>
#include <QDebug>
#include "dataCompress.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
//    return a.exec();

    int dataA[]={1,3,3,2,1,6};
    int dataB[4];

    dataCompress( dataA, dataB, 5, 3 );
    qDebug()<<dataB[0]<<dataB[1]<<dataB[2];

    dataCompress_Peak( dataA, dataB, 5, 3 );
    qDebug()<<dataB[0]<<dataB[1]<<dataB[2];

    dataCompress_Peak( dataA, dataB, 5, 4 );
    qDebug()<<dataB[0]<<dataB[1]<<dataB[2]<<dataB[3];

    dataCompress_Peak( dataA, dataB, 6, 4 );
    qDebug()<<dataB[0]<<dataB[1]<<dataB[2]<<dataB[3];

    qDebug()<<QString::asprintf("%.3f",1.1234567e-6);
//    QString str;
//    str = QString("%1").arg((double)1e-12,0,'E');
//    qDebug()<<str;

//    str = QString("%1").arg(1.1234567e-12f,0,'E');
//    qDebug()<<str;
}
