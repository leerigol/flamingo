
#include "cplatform.h"

void adc_case()
{
    cplatform platform;
    platform.buildPlatform();
    platform.initPlatform();

    int adcId = 0;

#define test_tf( func, tf ) \
    platform.m_pPhy->mAdc[adcId].set##func( tf );\
    qWarning( #func":%d(%d)", platform.m_pPhy->mAdc[adcId].get##func(), tf );

#define test_uint( func, val ) \
    platform.m_pPhy->mAdc[adcId].set##func( val ); \
    qWarning( #func":%d(%d)", platform.m_pPhy->mAdc[adcId].get##func(), val );

#define test_uint2( func, val, val2 ) \
    platform.m_pPhy->mAdc[adcId].set##func( val, val2 ); \
    qWarning( #func":%d(%d)", platform.m_pPhy->mAdc[adcId].get##func(), val );


#define test_get( func )    \
    qWarning( #func":%d", platform.m_pPhy->mAdc[adcId].get##func() );

    //! read otp
    unsigned char bankData[128];
    memset( bankData, 0, 128 );
    int retLen;
    dso_phy::ROM_V1_HEAD head;
    dso_phy::ROM_V1_INL inls;

//    platform.m_pPhy->mAdc[0].setStdby( 0 );
//    platform.m_pPhy->mAdc[0].setMode( 0 );
//    qDebug()<<platform.m_pPhy->mAdc[0].getSyncEn()<<__FUNCTION__<<__LINE__;
//    platform.m_pPhy->mAdc[0].setSyncEn( 1 );
//    qDebug()<<platform.m_pPhy->mAdc[0].getSyncEn()<<__FUNCTION__<<__LINE__;
//    platform.m_pPhy->mAdc[0].setSyncEn( 0 );
//    return;

    for ( adcId = 0; adcId < 2; adcId ++ )
    {
        qDebug()<<"------";
        retLen = platform.m_pPhy->mAdc[adcId].readBank( 0,
                                                    0,
                                                    sizeof(head),
                                                    32,
                                                    (unsigned char*)&head
                                                     );
        qWarning( "rom head:%d(%d)", retLen, sizeof(head) );
        qDebug()<<"ids:"<<head.ID0<<head.ID1<<head.ID2<<head.ID3;
        retLen = platform.m_pPhy->mAdc[adcId].readBank( 0,
                                                    32,
                                                    sizeof(inls),
                                                    112,
                                                    (unsigned char*)&inls
                                                     );
        qWarning( "rom inl:%d(%d)", retLen, sizeof(inls) );


        //! phy test
        qWarning( "chipid:%x", platform.m_pPhy->mAdc[adcId].getChipId() );

        test_tf( SyncEn, true );
        test_tf( SyncEn, false );

        test_uint( ThDca, 1 );
        test_uint( ThDca, 2 );
        test_uint( ThDca, 3 );
        test_uint( ThDca, 4 );
        test_uint( ThDca, 0 );

        test_tf( TcEn, true );
        test_tf( TcEn, false );

        test_uint( ItrimC, 1 );
        test_uint( ItrimC, 2 );
        test_uint( ItrimC, 3 );
        test_uint( ItrimC, 0 );

        test_uint( Ilvds, 1 );
        test_uint( Ilvds, 2 );
        test_uint( Ilvds, 3 );
        test_uint( Ilvds, 0 );

        test_tf( Test, true );
        test_tf( Test, false );

        test_uint( Stdby, 1 );
        test_uint( Stdby, 2 );
        test_uint( Stdby, 3 );
        test_uint( Stdby, 0 );

        test_uint2( Mode, 1, aim_calibrate );

        test_uint2( Mode, 4, aim_calibrate );
        test_uint2( Mode, 5, aim_calibrate );
        test_uint2( Mode, 6, aim_calibrate );
        test_uint2( Mode, 7, aim_calibrate );

        test_uint2( Mode, 8, aim_calibrate );
        test_uint2( Mode, 9, aim_calibrate );
        test_uint2( Mode, 10, aim_calibrate );
        test_uint2( Mode, 11, aim_calibrate );

        test_uint2( Mode, 12, aim_calibrate );
        test_uint2( Mode, 13, aim_calibrate );
        test_uint2( Mode, 14, aim_calibrate );
        test_uint2( Mode, 15, aim_calibrate );

        test_get( Adcxup );

        test_uint( ItrimA, 1 );
        test_uint( ItrimA, 2 );
        test_uint( ItrimA, 3 );
        test_uint( ItrimA, 0 );

        test_tf( Dec, true );
        test_tf( Dec, false );

        test_tf( Track, true );
        test_tf( Track, false );

        test_get( TrimStatus );

        test_uint( DlyFine, 1 );
        test_uint( DlyFine, 2 );
        test_uint( DlyFine, 1023 );
        test_uint( DlyFine, 0 );

        test_tf( Flip, true );
        test_tf( Flip, false );

        test_uint( DlyCoarse, 1 );
        test_uint( DlyCoarse, 2 );
        test_uint( DlyCoarse, 3 );
        test_uint( DlyCoarse, 4 );

        test_uint( DlyCoarse, 5 );
        test_uint( DlyCoarse, 6 );
        test_uint( DlyCoarse, 0 );

        test_uint( DlyInter, 1 );
        test_uint( DlyInter, 2 );
        test_uint( DlyInter, 0 );
        test_uint( DlyInter, 15 );

        test_uint( RwLoop, 1 );
        test_uint( RwLoop, 2 );
    }
}
