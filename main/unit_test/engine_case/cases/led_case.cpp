
#include "cplatform.h"
#include "unit_test.h"

void led_case()
{
    cplatform platform;dbg( 1 );
    platform.buildPlatform();dbg( 1 );
    platform.initPlatform();dbg( 1 );

    int i;

    //! off all
    for ( i = DsoEngine::led_ch1; i <= DsoEngine::led_func; i++ )
    {
        platform.m_pPhy->mLed.seLedOp( i, false );
    }

    for ( i = DsoEngine::led_ch1; i <= DsoEngine::led_func; i++ )
    {
        platform.m_pPhy->mLed.seLedOp( i, true );

        QThread::sleep(1);
    }

    dbg( 1 );

    for ( i = DsoEngine::led_ch1; i <= DsoEngine::led_func; i++ )
    {
        platform.m_pPhy->mLed.seLedOp( i, false );

        QThread::sleep(1);
    }
}
