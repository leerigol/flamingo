#ifndef TEST_CASE
#define TEST_CASE

#include "adc_case.h"
#include "misc_case.h"
#include "led_case.h"
#include "saw_wave.h"

#include "afe.h"
#include "dac.h"

#include "dg_case.h"
#include "fram_case/cases/fram_case.h"

#endif // TEST_CASE

