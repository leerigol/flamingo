#include "dac.h"


#include "cplatform.h"

void dac_case()
{
    cplatform platform;
    platform.buildPlatform();
    platform.initPlatform();

    uint dac = 32768;
    int dacId = 0;
    while( 1 )
    {
        switch( getchar() )
        {
            case 'q': dac += 1; break;
            case 'z': dac -= 1; break;
            case 'w': dac += 20; break;
            case 'x': dac -= 20; break;
            case 'e': dac += 50; break;
            case 'c': dac -= 50; break;
            case 'r': dac += 100; break;
            case 'v': dac -= 100; break;
            case 't': dac += 200; break;
            case 'b': dac -= 200; break;
            case 'y': dac += 500; break;
            case 'n': dac -= 500; break;
            case 'u': dac += 1000; break;
            case 'm': dac -= 1000; break;
            case 'a': dac = 32768; break;
            default: break;
        }
        qDebug()<<dac;
        platform.m_pPhy->mDac.set( dac, dacId );
        platform.m_pPhy->flushWCache();
    }

    for( int i = 10; i < 15; i++ )
    {
        qDebug()<<i;
        platform.m_pPhy->mDac.set( 0, i );
        platform.m_pPhy->flushWCache();
        qDebug()<<0;
        getchar();
        platform.m_pPhy->mDac.set( 32767, i );
        platform.m_pPhy->flushWCache();
        qDebug()<<32767;
        getchar();
        platform.m_pPhy->mDac.set( 65535, i );
        platform.m_pPhy->flushWCache();
        qDebug()<<65535;
        getchar();
        platform.m_pPhy->mDac.set( 0, i );
        platform.m_pPhy->flushWCache();
    }

//    platform.m_pPhy->mDac.set( 0, 13 );
//    platform.m_pPhy->flushWCache();
//    getchar();
//    platform.m_pPhy->mDac.set( 32767, 13 );
//    platform.m_pPhy->flushWCache();
//    getchar();
//    platform.m_pPhy->mDac.set( 65535, 13 );
//    platform.m_pPhy->flushWCache();
//    getchar();

//    quint16 dacs[]={ 38754, 27218, 39834, 26196 };

//    platform.m_pPhy->mDac.set( 0, 13 );
//    getchar();
//    for ( int i = 0; i < 4; i++ )
//    {
//        platform.m_pPhy->mDac.set( dacs[i], 13 );
//        platform.m_pPhy->flushWCache();
//        getchar();
//    }

}
