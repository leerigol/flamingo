#include "cplatform.h"

void dg_case()
{
    cplatform platform;
    platform.buildPlatform();
    platform.initPlatform();

    qDebug()<<QString::number( platform.m_pPhy->mSpuGp.inVERSION(), 16 );

    qDebug()<<QString::number( platform.m_pPhy->mZynqFcuRd.inVERSION(), 16 );

    for( int i = 0; i < 10; i++ )
    {
//        platform.m_pPhy->mDgu.switchRelay(0x24);
//        QThread::msleep( 100 );
//        platform.m_pPhy->mDgu.switchRelay(0x00);
//        QThread::msleep( 100 );

//        platform.m_pPhy->mRelay.write( 0xff, 8, 8 );
//        platform.m_pPhy->flushWCache();
//        getchar();
//        platform.m_pPhy->mRelay.write( 0, 8, 8 );
//        platform.m_pPhy->flushWCache();
//        getchar();
//        platform.m_pPhy->mRelay.write( 0xff, 0, 8 );
//        platform.m_pPhy->flushWCache();
//        getchar();
//        platform.m_pPhy->mRelay.write( 0, 0, 8 );
//        platform.m_pPhy->flushWCache();
//        getchar();

        for ( int j = 0; j < 16; j++ )
        {
            platform.m_pPhy->mRelay.write( 1, j );
            platform.m_pPhy->flushWCache();
            getchar();

            platform.m_pPhy->mRelay.write( 0, j );
            platform.m_pPhy->flushWCache();
            getchar();
        }
    }
}
