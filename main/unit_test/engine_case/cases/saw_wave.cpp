

#include "cplatform.h"

void sawWave_case()
{
    cplatform platform;
    platform.buildPlatform();
    platform.initPlatform();

    qDebug()<<QString("%1").arg( platform.m_pPhy->mWpu.inversion(),
                           0,
                           16 );
    qDebug()<<QString("%1").arg( platform.m_pPhy->mScu.getVersion(),
                                 0,
                                 16);

    qDebug()<<QString("%1").arg( platform.m_pPhy->mCcu.getCcuStop(),
                                 0,
                                 16);

//    platform.m_pPhy->mPll.output5G();
//    platform.m_pPhy->mAdc[0].init();
//    platform.m_pPhy->mAdc[1].init();

    qDebug()<<__FUNCTION__<<__LINE__;

//    QThread::sleep(10);

//    platform.m_pPhy->mWpu.init();
//    platform.m_pPhy->mScu.init();

    qDebug()<<__FUNCTION__<<__LINE__;

//    for ( ;; )
    {
//        platform.m_pPhy->mScu.setSysRun( true );
//        platform.m_pPhy->mScu.setScuStop( 0 );
//        QThread::msleep( 100 );
//        platform.m_pPhy->mScu.setSysRun( false );
//        QThread::msleep( 100 );

//        qDebug()<<platform.m_pPhy->mScu.getVersion();
//        QThread::msleep( 100 );

//        QThread::sleep(1);
//        qDebug()<<platform.m_pPhy->mWpu.getVersion();
    }
}
