#include "cplatform.h"

void afe_case()
{
    cplatform *platform=cplatform::createInstance();
    platform->buildPlatform();
    platform->initPlatform();

    int chId = 3;
    int dacId = 10 + chId;

    //! close ch3
    platform->m_pPhy->mVGA[2].setHzNpd( 0 );
    platform->m_pPhy->mVGA[2].setLzNpd( 0 );

    platform->m_pPhy->mVGA[2].flushWCache();
    return;


    //! tick attenurator
    for ( int i = 0; i < 2; i++ )
    {
        for( chId  = 0; chId < 4; chId++ )
        {
            platform->m_pPhy->mVGA[ chId ].setGpo1( 0 );
            platform->m_pPhy->flushWCache();
            QThread::msleep( 10 );
            platform->m_pPhy->mVGA[ chId ].setGpo1( 1 );
            platform->m_pPhy->flushWCache();
            QThread::msleep( 10 );
        }

        for( chId  = 0; chId < 4; chId++ )
        {
            platform->m_pPhy->mDac.set( 32768, 10 + chId );

            //! 50 OHM
            platform->m_pPhy->mVGA[ chId ].setGpo1( 1 );
            platform->m_pPhy->mVGA[ chId ].setISel( 2 );
        }

        platform->m_pPhy->flushWCache();
    }
    return;

    for (  chId = 0; chId < 4; chId++ )
    {
        //! lpd
        platform->m_pPhy->mVGA[ chId ].setLzNpd( 1 );
        platform->m_pPhy->mVGA[ chId ].setHzNpd( 1 );
        platform->m_pPhy->mVGA[ chId ].setLpp( 0 );

        //! set to 1MOHm
        platform->m_pPhy->mVGA[ chId ].setOlEn( 0 );
        platform->m_pPhy->mVGA[ chId ].setOSel( 2 );
        platform->m_pPhy->mVGA[ chId ].setOen( 1 );

        platform->m_pPhy->mVGA[ chId ].setK( 0 );
        platform->m_pPhy->mVGA[ chId ].setHzSga1Gs( 0 );
        platform->m_pPhy->mVGA[ chId ].setAtten( 0 );

//        //! set to 50
//        platform->m_pPhy->mVGA[ chId ].setOlEn( 1 );
//        platform->m_pPhy->mVGA[ chId ].setOSel( 0 );
//        platform->m_pPhy->mVGA[ chId ].setOen( 1 );
//        //! 1/20
//        platform->m_pPhy->mVGA[ chId ].setGpo1( 0 );
//        platform->m_pPhy->mVGA[ chId ].setISel( 0 );

//        //! 1/2
//        platform->m_pPhy->mVGA[ chId ].setGpo1( 1 );
//        platform->m_pPhy->mVGA[ chId ].setISel( 2 );

        platform->m_pPhy->mVGA[ chId ].setGtrim4( 1, 15 );
        platform->m_pPhy->mVGA[ chId ].setGtrim5( 1, 15 );

        //! set offset
        platform->m_pPhy->mDac.set( 32768, 10 + chId );

    }

    platform->m_pPhy->flushWCache();
    return;
//    getchar();

//    platform->m_pPhy->mVGA[ chId ].setOen( 3 );
//    platform->m_pPhy->mVGA[ chId ].setOSel( 0 );

    platform->m_pPhy->mVGA[ chId ].setLzNpd( 1 );
    platform->m_pPhy->mVGA[ chId ].setHzNpd( 1 );
    platform->m_pPhy->mVGA[ chId ].setLpp( 0 );

    platform->m_pPhy->mVGA[ chId ].setOlEn( 1 );

    platform->m_pPhy->mVGA[ chId ].setGtrim1( 0, 0 );
    platform->m_pPhy->mVGA[ chId ].setGtrim2( 0, 3 );
    platform->m_pPhy->mVGA[ chId ].setGtrim3( 0, 3 );

    platform->m_pPhy->mVGA[ chId ].setGtrim4( 0, 1 );
    platform->m_pPhy->mVGA[ chId ].setGtrim5( 0, 1 );


    //! 1/2
    platform->m_pPhy->mVGA[ chId ].setGpo1( 1 );
    platform->m_pPhy->mVGA[ chId ].setISel( 2 );
    platform->m_pPhy->flushWCache();
    qDebug()<<"1/2";

//    platform->m_pPhy->mDac.set( 0, dacId );
//    platform->m_pPhy->flushWCache();
//    getchar();
//    platform->m_pPhy->mDac.set( 32768, dacId );
//    platform->m_pPhy->flushWCache();
//    getchar();
//    platform->m_pPhy->mDac.set( 65536, dacId );
//    platform->m_pPhy->flushWCache();
//    getchar();

    platform->m_pPhy->mVGA[ chId ].showPara();
//    getchar();

    //! 1/20
//    platform->m_pPhy->mVGA[ chId ].setGpo1( 0 );
//    platform->m_pPhy->mVGA[ chId ].setISel( 0 );
//    platform->m_pPhy->flushWCache();
//    qDebug()<<"1/20";
//    getchar();

//    //! inte offset
//    platform->m_pPhy->flushWCache();

//    qDebug()<<"1/2-inte";
//    platform->m_pPhy->mVGA[ chId ].setGpo1( 1 );
//    platform->m_pPhy->mVGA[ chId ].setISel( 1 );

//    platform->m_pPhy->mVGA[ chId ].setDacSel( 1 );
//    platform->m_pPhy->mVGA[ chId ].setEnDclp( 1 );
//    platform->m_pPhy->flushWCache();
//    getchar();

//    platform->m_pPhy->mVGA[ chId ].setOen( 3 );
//    platform->m_pPhy->mVGA[ chId ].setOSel( 0 );

    //! hiz
    qDebug()<<"hiz";
    platform->m_pPhy->mVGA[ chId ].setOlEn( 0 );
    platform->m_pPhy->mVGA[ chId ].setOSel( 2 );
    platform->m_pPhy->mVGA[ chId ].setOen( 3 );
    platform->m_pPhy->mVGA[ chId ].setK( 0 );
    platform->m_pPhy->mVGA[ chId ].setAtten( 0 );

//    getchar();qDebug()<<0;
//    platform->m_pPhy->mVGA[ chId ].setGtrim1( 0 , 0 );
//    platform->m_pPhy->flushWCache();
    getchar();qDebug()<<1;
    platform->m_pPhy->mVGA[ chId ].setGtrim1( 0 , 1 );
    platform->m_pPhy->flushWCache();
//    getchar();qDebug()<<2;
//    platform->m_pPhy->mVGA[ chId ].setGtrim1( 1 , 0 );
//    platform->m_pPhy->flushWCache();
//    getchar();qDebug()<<3;
//    platform->m_pPhy->mVGA[ chId ].setGtrim1( 1 , 1 );
//    platform->m_pPhy->flushWCache();
//    getchar();
    qDebug()<<"end";

    uint dac = 32768;
    while( 1 )
    {
        switch( getchar() )
        {
            case 'q': dac += 1; break;
            case 'z': dac -= 1; break;
            case 'w': dac += 20; break;
            case 'x': dac -= 20; break;
            case 'e': dac += 50; break;
            case 'c': dac -= 50; break;
            case 'r': dac += 100; break;
            case 'v': dac -= 100; break;
            case 't': dac += 200; break;
            case 'b': dac -= 200; break;
            case 'y': dac += 500; break;
            case 'n': dac -= 500; break;
            case 'u': dac += 1000; break;
            case 'm': dac -= 1000; break;
            case 'a': dac = 32768; break;
            default: break;
        }
        qDebug()<<dac;
        platform->m_pPhy->mDac.set( dac, dacId );
        platform->m_pPhy->flushWCache();
    }

    int scales[]={1000,2000,5000,
                  10000,20000,50000,
                  100000,200000,500000,
                  1000000,0};

    for(;;)
    for ( int i = 0; i < 4; i++ )
    {
        qDebug()<<i;
        platform->m_pPhy->mVGA[ i ].setGpo0( 0 );
        platform->m_pPhy->mVGA[ i ].setGpo1( 0 );
        platform->m_pPhy->mVGA[ i ].setOlEn( 0 );
        platform->m_pPhy->flushWCache();
        getchar();

        platform->m_pPhy->mVGA[ i ].setGpo0( 1 );
        platform->m_pPhy->mVGA[ i ].setGpo1( 1 );
        platform->m_pPhy->mVGA[ i ].setOlEn( 1 );
        platform->m_pPhy->flushWCache();
        getchar();

        platform->m_pPhy->mVGA[ i ].setGpo0( 0 );
        platform->m_pPhy->mVGA[ i ].setGpo1( 0 );
        platform->m_pPhy->mVGA[ i ].setOlEn( 0 );
        platform->m_pPhy->flushWCache();
        getchar();

        platform->m_pPhy->mVGA[ i ].setGpo0( 1 );
        platform->m_pPhy->mVGA[ i ].setGpo1( 1 );
        platform->m_pPhy->mVGA[ i ].setOlEn( 1 );
        platform->m_pPhy->flushWCache();
        getchar();

    }

    platform->m_pPhy->mDac.set( 36000, 13 );


//    platform.m_pPhy->mVGA[3].setGpo0(1);
//    platform->m_pPhy->mVGA[3].setGpo1(0);
//    platform->m_pPhy->mVGA[3].setOSel( 2 );
//    platform->m_pPhy->mVGA[3].setOen( 3 );

//    platform->m_pPhy->mVGA[3].setAzIsel( 0 );
//    platform->m_pPhy->mVGA[3].setEnZd( 1 );

    platform->m_pPhy->mVGA[ 3 ].setVos( 1149 );

    platform->m_pPhy->mVGA[ 3 ].setDcTrim( 161 );

    platform->m_pPhy->mVGA[ 3 ].setK( 31+32 );
    platform->m_pPhy->mVGA[ 3 ].setAzIsel( 1 );
    platform->m_pPhy->mVGA[ 3 ].setEnZd( 1 );
    platform->m_pPhy->mVGA[ 3 ].setTp1Typ( 1 );
//    platform->m_pPhy->mVGA[ 3 ].setTpSw( 1 );
    platform->m_pPhy->mVGA[ 3 ].setTpSw( 0 );

    quint16 val = 0;
    for(;;)
    {
//        qDebug()<<"scale:"<<scales[i]/1000<<"mv";
//        platform.m_pPhy->mVGA[3].setHizScale( scales[i] );
//        platform.m_pPhy->mVGA[3].setLozScale( scales[i] );

//        platform->m_pPhy->mVGA[3].setOlEn( 1 );
//        platform.m_pPhy->mVGA[3].setGpo0(1);
//        platform.m_pPhy->mVGA[3].setGpo1(1);
//        platform->m_pPhy->flushWCache();
//        QThread::msleep( 100 );
//        platform->m_pPhy->mVGA[3].setOlEn( 0 );
//        platform.m_pPhy->mVGA[3].setGpo0(0);
//        platform.m_pPhy->mVGA[3].setGpo1(0);

//        platform->m_pPhy->mVGA[3].setHzVosTrim2( (val & 0xf) );
//        platform->m_pPhy->mVGA[3].setHzVosTrim3( (val+1) & 0xf );

//        platform->m_pPhy->mVGA[3].setAzVos( val );
        platform->m_pPhy->mVGA[3].setHzVosTrim2( val );
        platform->m_pPhy->mVGA[3].setHzVosTrim3( val );

        platform->m_pPhy->flushWCache();
        QThread::msleep( 100 );

//        qDebug()<<QString("%1").arg( (val & 0xf),0,16 );val+=2;
//        qDebug()<<QString("%1").arg( platform->m_pPhy->mVGA[3].getRB(),
//                                     0,
//                                     16 );
        qDebug()<<platform->m_pPhy->mVGA[3].getZDet()<<val;
        val += 1;

        getchar();
    }

//    platform.m_pPhy->mVGA[3].setOvLvl(1);
//    platform.m_pPhy->mVGA[3].setAtten(1);
//    platform.m_pPhy->mVGA[3].setHzVosTrim2(1);
//    platform.m_pPhy->mVGA[3].setHzVosTrim3(1);
//    platform.m_pPhy->mVGA[3].flushWCache();

//    for( ; ; )
//    {

//////        qDebug()<<platform.m_pPhy->mVGA[3].getRB();
//        platform.m_pPhy->mVGA[3].getRB();
//        platform.m_pPhy->mVGA[3].flushRCache();

//        QThread::msleep( 1000 );
//    }
//    for( ; ; )
//    {
//        platform.m_pPhy->mVGA[3].setGpo0( 1 );
//        platform.m_pPhy->mVGA[3].setGpo1( 1 );
//        platform.m_pPhy->mVGA[3].flushWCache();
//        QThread::msleep( 100 );
//        platform.m_pPhy->mVGA[3].setGpo0( 0 );
//        platform.m_pPhy->mVGA[3].setGpo1( 0 );
//        platform.m_pPhy->mVGA[3].flushWCache();
//        QThread::msleep( 100 );
//    }
}
