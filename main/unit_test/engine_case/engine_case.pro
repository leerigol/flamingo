#-------------------------------------------------
#
# Project created by QtCreator 2016-10-13T11:14:43
#
#-------------------------------------------------

QT       += core
QT       += gui

TARGET = engine_case
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QT += widgets

LIBS += -L../../../../lib -L../../../../lib/3rdlib
LIBS += -L../../../../lib/phy

LIBS += -llua -ldsoengine -lbus -lcom -lbaseclass


LIBS += -liphych -liphyhori -liphy
LIBS += -liphytrig -liphysearch
LIBS += -liphymeas -liphycounter
LIBS += -liphymask -liphy1wire -liphyzynqfcu -liphytrace
LIBS += -liphydg -liphyrelay
LIBS += -liphyk7mfcu

LIBS += -liphyscu -liphyspu -liphywpu -liphygtu -liphyccu
LIBS += -liphyrecplay
LIBS += -liphyadc


LIBS += -lkeyboard
LIBS += -lmeta

LIBS += -lbaseclass

LIBS += -larith

INCLUDEPATH += ../../../engine
INCLUDEPATH += ./cases
INCLUDEPATH += ../

SOURCES += main.cpp \
    cases/misc_case.cpp \
    cases/led_case.cpp \
    cases/adc_case.cpp \
    cases/saw_wave.cpp \
    cases/afe.cpp \
    cases/dac.cpp \
    cases/dg_case.cpp

HEADERS += \
    cases/test_case.h \
    cases/misc_case.h \
    cases/led_case.h \
    cases/adc_case.h \
    cases/saw_wave.h \
    cases/afe.h \
    cases/dac.h \
    cases/dg_case.h
