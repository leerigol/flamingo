#include <QCoreApplication>

#include "test_case.h"

//! keyproc
#include "../../../com/keyevent/ckeyproc.h"

#include "../../../meta/crmeta.h"

#define dbg( lev )   {qDebug()<<__FUNCTION__<<__LINE__;}

//! assist
static QString _sysResourcePath;
void sysSetResourcePath( const QString &path )
{
    _sysResourcePath = path;
}
QString &sysGetResourcePath()
{
    return _sysResourcePath;
}

static int _argc;
static char** _argv;
void sysSetArg( int argc, char **argv )
{
    _argc = argc;
    _argv = argv;
}

int sysGetArgc()
{
    return _argc;
}
char** sysGetArgv()
{
    return _argv;
}

bool sysHasArg( const QString &str, bool caseSen )
{
    Qt::CaseSensitivity cs = caseSen ? Qt::CaseSensitive : Qt::CaseInsensitive;

   for ( int i = 1; i < _argc; i++ )
   {
       if ( QString::compare( str, _argv[i], cs) == 0 )
       { return true; }
   }

   return false;
}

#define meta_path   "/rigol/resource/"
#define file_path   "/rigol/resource/"



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //! arg
    sysSetArg( argc, argv );

    //! meta
    sysSetResourcePath( file_path );

    r_meta::CRMeta::setMetaFile( meta_path"dsometa.xml" );
    r_meta::CBMeta::setMetaFile( meta_path"boardmeta.xml");
    r_meta::CAppMeta::setMetaFile( meta_path"appmeta.xml" );
    r_meta::CServMeta::setMetaFile( meta_path"servmeta.xml" );


    //! start key scan
    app_key_board::CKeyProc keyProc;
    dbg(1);

    keyProc.start();
    dbg(1);

//    led_case();

//    adc_case();

//    misc_case();

//    sawWave_case();
    afe_case();
//    dac_case();

//    dg_case();

//    return a.exec();
}


