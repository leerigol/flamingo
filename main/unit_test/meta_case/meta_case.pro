#-------------------------------------------------
#
# Project created by QtCreator 2016-10-17T09:16:43
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += testlib

TARGET = meta_case
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L../../../../lib_gcc
LIBS += -lmeta

INCLUDEPATH += ../../../meta

SOURCES += main.cpp
