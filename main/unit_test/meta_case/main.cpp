#include <QCoreApplication>
#include <QtTest>

#include "crmeta.h"
using namespace r_meta;

#define meta_file   "/home/wzy/develope/flamingo/code/main/unit_test/meta_case/test.xml"

class MetaTest : public QObject
{
    Q_OBJECT

public:
    MetaTest(){}

private Q_SLOTS:
    void initTestCase(){}
    void cleanupTestCase(){}

    void init(){}
    void cleanup(){}

    void cmeta_test()
    {
        CMeta meta( meta_file );
        QString str;
        int iVal;
        QRect rect;
        QColor color;

        QBENCHMARK{
        meta.getMetaVal( "val/val_string", str );
        }
        QCOMPARE( str, QLatin1String("abc") );

        meta.getMetaVal( "val/val_int", iVal );
        QCOMPARE( iVal, 123 );

        meta.getMetaVal( "color", color );
        QCOMPARE( color, QColor(0x12,0x34,0x56));

        meta.getMetaVal( "rect/", rect );
        QCOMPARE( rect, QRect(0,1,2,3) );

        rect.setRect(0,0,0,0);
        meta.getMetaVal( "rect", rect );
        QCOMPARE( rect, QRect(0,1,2,3) );
    }

    void crmeta_test()
    {
        CRMeta::setMetaFile( meta_file );
        CRMeta meta;

        QString str;
        int iVal;
        QRect rect;
        QColor color;

        QBENCHMARK{
        meta.getMetaVal( "val/val_string", str );
        }
        QCOMPARE( str, QLatin1String("abc") );

        meta.getMetaVal( "val/val_int", iVal );
        QCOMPARE( iVal, 123 );

        meta.getMetaVal( "color", color );
        QCOMPARE( color, QColor(0x12,0x34,0x56));

        meta.getMetaVal( "rect/", rect );
        QCOMPARE( rect, QRect(0,1,2,3) );

        rect.setRect(0,0,0,0);
        meta.getMetaVal( "rect", rect );
        QCOMPARE( rect, QRect(0,1,2,3) );
    }
};

QTEST_MAIN(MetaTest)

#include "main.moc"


