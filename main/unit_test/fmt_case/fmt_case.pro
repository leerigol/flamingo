#-------------------------------------------------
#
# Project created by QtCreator 2016-10-14T11:40:32
#
#-------------------------------------------------

QT       += core

QT       += gui

TARGET = fmt_case
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

LIBS += -L../../../../lib_gcc
LIBS += -lgui

INCLUDEPATH += ../../../gui

SOURCES += main.cpp
