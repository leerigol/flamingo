#include <QCoreApplication>
#include <QDebug>
#include "cguiformatter.h"

using namespace gui_fmt;
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString str;
    CRealString realStr( -1.23 );

    gui_fmt::CGuiFormatter::format(str, 0.1f );
    qDebug()<<str;

    realStr.normalize( str, 4 );
    qDebug()<<str;

    realStr = 1.01256e4;
    realStr.normalize( str, 4 );
    qDebug()<<str;

    realStr = -1.56789e-4;
    realStr.normalize( str );
    qDebug()<<str;

    realStr = -1000;
    realStr.normalize( str );
    qDebug()<<str;

    realStr = -10;
    realStr.normalize( str );
    qDebug()<<str;

    CGuiFormatter::format( str, -1234.01f, "%6f");
    qDebug()<<str;

    CGuiFormatter::format( str, -1234.01f, "%7f");
    qDebug()<<str;

    CGuiFormatter::format( str, -12345 );
    qDebug()<<str;

    CGuiFormatter::format( str, -123 );
    qDebug()<<str;

    CGuiFormatter::format( str, -123, "", DsoType::Unit_W );
    qDebug()<<str;

    CGuiFormatter::format( str, -123, "", DsoType::Unit_V );
    qDebug()<<str;

    CGuiFormatter::format( str, -123, "", DsoType::Unit_degree );
    qDebug()<<str;

    CGuiFormatter::format( str, -123, "", DsoType::Unit_Div );
    qDebug()<<str;
//    return a.exec();
}
