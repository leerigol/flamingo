#-------------------------------------------------
#
# Project created by QtCreator 2015-12-18T15:27:13
#
#-------------------------------------------------

QT       += core
QT       += widgets
QT       += gui
QT       += network
QT       += xml
CONFIG+=debug_and_release
keybd = $$(DSO_KEYBOARD)
equals(keybd, yes){
QT += serialport
}

DEFINES += _DO_TEST

TARGET = math_case
CONFIG   -= app_bundle

TEMPLATE = app

# 库路径
LIBS += -L../../../lib$$(PLATFORM)
LIBS += -L../../../lib$$(PLATFORM)/3rdlib
LIBS += -L../../../lib$$(PLATFORM)/services
LIBS += -L../../../lib$$(PLATFORM)/apps
LIBS += -L../../../lib$$(PLATFORM)/scpi
LIBS += -L../../../lib$$(PLATFORM)/phy

# app
LIBS += -lappmain

LIBS += -lservtrig
LIBS += -lapptrigger

LIBS += -lappdisplay
LIBS += -lappla
LIBS += -lappmenu
LIBS += -lappref
LIBS += -lappmeasure
LIBS += -lappcursor

LIBS += -lappmenutest
LIBS += -lappsearch
LIBS += -lappstorage
LIBS += -lappdecode
LIBS += -lappgui
LIBS += -lappmask
LIBS += -lappplot
LIBS += -lapputility
#LIBS += -lapptrigger
LIBS += -lappdg
LIBS += -lapphori
LIBS += -lappmath
LIBS += -lappwrec

LIBS += -lappch
LIBS += -lappdso
LIBS += -lapphelp
LIBS += -lappUPA

LIBS += -lappcal

# 服务
#LIBS += -lservdisplay

LIBS += -lservstorage
LIBS += -lservdso

LIBS += -lservmenutest
LIBS += -lservtrace
LIBS += -lservgui
LIBS += -lservplot

#LIBS += -lservtrig
LIBS += -lservch
LIBS += -lservhori
LIBS += -lservquick

LIBS += -lservutility
LIBS += -lservcursor
LIBS += -lservla
LIBS += -lservref

LIBS += -lservvertmgr
LIBS += -lservdecode
LIBS += -lservmask
LIBS += -lservscpi

LIBS += -lservWRec
LIBS += -lservDG
LIBS += -lservmath
LIBS += -lservSearch

LIBS += -lservmeasure

LIBS += -lservHotplug
LIBS += -lservhelp
LIBS += -lservUPA
LIBS += -lservcal

LIBS += -lservdisplay

# 基础库

LIBS += -ldsofw -lgui -ldsoengine -lbus
# phys
LIBS += -liphych -liphyhori -liphy


LIBS += -lmenu
LIBS += -linputkeypad
LIBS += -lvirtualkb
LIBS += -lrpinyin

LIBS += -lrdsowidget
LIBS += -lrwnd
LIBS += -lrwidget
LIBS += -lrevent

LIBS += -lrstyle

LIBS += -ltiff

LIBS += -lmeta
LIBS += -leventtable

LIBS += -lcom
LIBS += -lbaseclass
LIBS += -lkeyboard
LIBS += -llua
LIBS += -lNE10
LIBS += -larith

LIBS += -lscpiparse

LIBS += -lusbtmc
LIBS += -lvxi11

#for ARM
OBJECTS_DIR = ./build/arm
MOC_DIR     = ./build/arm/moc

#for PC
platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
LIBS -= -lkeyboard
LIBS -= -lNE10
OBJECTS_DIR = ./build/x86
MOC_DIR     = ./build/x86/moc
}


SOURCES += \
    ./math_case/main.cpp \
    ../../service/servicefactory.cpp \
    ../../app/capp.cpp \

HEADERS += \
    ../../include/dsostd.h \
    ../../include/dsotype.h \
    ../../service/service.h \
    ../../service/servicefactory.h \
    ../../service/service_msg.h \
    ../../service/service_name.h \
    ../../app/capp.h \
    ../../include/dsocfg.h \
    ../../service/service_id.h \
    ../../include/dsoassert.h

RESOURCES += \
    ../../resource/res.qrc
