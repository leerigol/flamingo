#include <QApplication>

#include "../../../meta/crmeta.h"
#include "../../../menu/rmenus.h"


#include "../../../com/virtualkb/virtualkb.h"
#include "../../../menu/wnd/rmessagebox.h"

#include "../../../com/eventtable/reventtable.h"

//#define meta_path   "/home/wzy/develope/flamingo/meta/platform_a/"
//#define file_path   "/home/wzy/develope/flamingo/resource"

#define meta_path   "/home/wzy/develope/flamingo/code/meta/platform_a/"
#define file_path   "/home/wzy/develope/flamingo/code/resource"

int main( int argc, char * argv[] )
{
    QApplication a( argc, argv );

    sysSetResourcePath( file_path );

    //! meta
    r_meta::CRMeta::setMetaFile( meta_path"dsometa.xml" );
    r_meta::CBMeta::setMetaFile( meta_path"boardmeta.xml");
    r_meta::CAppMeta::setMetaFile( meta_path"appmeta.xml");

//    InputKeypad::getInstance()->show();
//    menu_res::RTimeEdit edit;
//    QPushButton edit;
//    edit.setTime( 13,15 );

//    menu_res::RDateEdit edit;
//    menu_res::RIpEdit edit;
//    menu_res::RSeekBar edit;

//    edit.setVal( 100 );

//    menu_res::RProgressBar edit;

//    menu_res::RButton edit;
//    menu_res::RAnimation edit;

//    QString str;
//    int i;
//    for ( i = 0; i < 12; i++ )
//    {
//        str = QString(":/pictures/wnd/progbar_rotate%1.bmp").arg(i);
//        edit.addAnimation( str );
//    }

//    edit.start();
//    edit.resize( 25, 25 );

//    menu_res::RProgressDialog edit;
//    edit.setInfo( "saving now" );
//    edit.setRange( 0, 100 );
//    edit.setValue( 100 );

//    menu_res::RDsoCHLabel edit;
//    edit.setColor( Qt::yellow );
//    edit.setText("hello");

//    menu_res::RDsoHLine edit;
//    menu_res::RDsoVLine edit;
//    edit.setColor( Qt::red );
//    edit.setGeometry(10,10,1,100 );
//    edit.setFrameShape( QFrame::VLine );
//    edit.setFrameShadow(QFrame::Sunken);
//    edit.setStyleSheet("background-color:red");
//    edit.setFrameRect( QRect(0,0,100,1) );

//    menu_res::RFuncKnob edit;

//    //edit.resize(200,28);
//    edit.show();

//    menu_res::RScrollBar edit;
//    edit.resize( 13, 200 );
//    edit.setScroll( 0, 100, 50, 10 );

//    edit.setStyleSheet( "border: 2px solid #800000;" );
//    edit.setStyleSheet( "border-radius: 4px;"
//                        "padding: 2px;" );

//    qDebug()<<QColor(Qt::red).name( QColor::QColor::HexRgb );

//    edit.show();

//    menu_res::RTextBox edit;
//    edit.show();

//    menu_res::RButton button;
//    button.show();
//    ImeKeyboard::IME()->show();

//    VirtualKB kb;
//    kb.show();

//    RMessageBox box;
//    box.show();

//    RMessageBox::Show("nihaomammmmmmmmmmmmmmmmmmmmmmmmmm");

    int heap[1024];

    CTable myTable;
    myTable.test( heap, sizeof(heap) );

    REventTable myTableWig;
    myTableWig.attachTable( &myTable );

    myTableWig.show();

    return a.exec();
}

