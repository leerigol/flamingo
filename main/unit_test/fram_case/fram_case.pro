QT       += core

QT       -= gui

TARGET = fram_test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += fram_main.cpp \ 
    ./cases/fram_case.cpp \
    ../../../com/fram/cfram.cpp

HEADERS += \
    ./cases/fram_case.h \
    ../../../com/fram/cfram.h

