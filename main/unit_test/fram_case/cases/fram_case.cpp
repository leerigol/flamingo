#include <QDebug>
#include "../../../com/fram/cfram.h"

using namespace fram_disk;

#if 1
void fram_case()
{
    int size = 2048;
    int offset = 0;
    CFram fram( size );
    quint8 datW, datR;

    quint8 dat[size];
    quint8 datRd[size];
    int i;

    qDebug()<<__LINE__<<fram.open();

////    for ( int page = 0; page < 8192/256; page++ )
//    {
////        offset = page * 256+128;

//        for( i = 0; i < size; i++ )
//        { dat[i] = rand(); }
//        Q_ASSERT( size == fram._write( offset, dat, size ) );
//        Q_ASSERT( size == fram._read( offset, datRd, size ) );
//        for ( i = 0; i < size; i++ )
//        {
//            Q_ASSERT( dat[i] == datRd[i] );
//        }
//    }

//    fram.close();
//    return;


    QTime timeTick;
    timeTick.start();
    //! cache test
    Q_ASSERT( 0 == fram.loadCache() );
    qDebug()<<timeTick.elapsed();



    //! write 0
    memset( dat, 0, sizeof(dat));
    Q_ASSERT ( fram.write( dat, 0, sizeof(dat) ) == sizeof(dat) );

    Q_ASSERT ( fram.read( datRd, 0, sizeof(datRd) ) == sizeof(datRd) );

    Q_ASSERT( fram.flushCache() == 0 );

    //! read cache
    memset( datRd, 0xff, sizeof(datRd) );
    Q_ASSERT ( fram.read( datRd, 0, sizeof(datRd) ) == sizeof(datRd) );
    for ( i = 0; i < sizeof(datRd); i++ )
    {
        Q_ASSERT( datRd[i] == 0x00 );
    }

    //! read 0
    Q_ASSERT( fram.loadCache() == 0 );
    Q_ASSERT ( fram.read( datRd, 0, sizeof(datRd) )== sizeof(datRd) );
    for ( i = 0; i < sizeof(datRd); i++ )
    {
        Q_ASSERT( datRd[i] == 0 );
    }

    //! write ff
    memset( dat, 0xff, sizeof(dat));
    Q_ASSERT ( fram.write( dat, 0, sizeof(dat) )== sizeof(dat) );

    Q_ASSERT( fram.flushCache() == 0 );

    //! read ff
    Q_ASSERT( fram.loadCache() == 0 );
    Q_ASSERT ( fram.read( datRd, 0, sizeof(datRd) )== sizeof(datRd) );
    for ( i = 0; i < sizeof(datRd); i++ )
    {
        Q_ASSERT( datRd[i] == 0xff );
    }

    fram.close();
}

#endif
