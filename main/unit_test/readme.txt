1. 存储结构：
   - build/: 构建目录，里面存放各个用例工程构建文件（夹）
   - xxx_case/: 测试用例工程文件，main.cpp
        - x.pro:
        - main.cpp
        - cases/ 测试用例文件
             - xxx_case.cpp/.h
             - test_case.h 包含所有测试用例的头文件

2. gui_case.pro 特殊，工程中使用到外部的源码文件，而且源码文件中使用了相对包含路径所
   以工程文件没有在gui_case/路径下
3. board_unit_test.pro 是子工程文件，用于在板测试功能模块
4. simulate_unit_test.pro 是子工程文件，用于 在pc上模拟 测试功能模块
