
#include <QApplication>
#include <QDebug>
#include <QMetaEnum>

#include "../../meta/crmeta.h"

#include "../../app/appmain/cappmain.h"

#include "../../service/service.h"
#include "../../service/servicefactory.h"

//! keyproc
#include "../../com/keyevent/ckeyproc.h"

#ifdef _DO_TEST
#include "../../app/appdisplay/cgrid.h"

#include "../../service/service_name.h"
#include "../../service/servmath/servmath.h"
#endif


#ifdef _SIMULATE

#define meta_path   "/home/wzy/develope/flamingo/code/meta/platform_a/"
#define file_path   "/home/wzy/develope/flamingo/code/resource/"


#else
#define meta_path   "/rigol/resource/"
#define file_path   "/rigol/resource/"

#endif

//#define def_time()   timeval tmA, tmB;
//#define start_time() gettimeofday( &tmA, 0 ); printf("%d\n", __LINE__ );
//#define end_time()   gettimeofday( &tmB, 0 ); printf("%d %fms\n", __LINE__, (tmB-tmA)/1000.0 );
//#define dbgout()     qWarning()<<__LINE__<<__DATE__<<__TIME__;


#define def_time()
#define start_time()
#define end_time()
#define dbgout()
//! main -cfg
int main( int argc, char * argv[] )
{
    def_time();

    start_time();
    end_time();

    start_time();
    QApplication a( argc, argv );
    end_time();

//    double val = -1e-9 * 100000000ll;
    float val = 0;

    QString str;

    str = QString("%1").arg( val, 0, 'E' );

    gui_fmt::CGuiFormatter::format( str, val );
    gui_fmt::CGuiFormatter::format( str, val, f_nf(fmt_1f) );

    gui_fmt::CGuiFormatter::format( str, val, f_nf(fmt_2f), Unit_Div );



    val -= 4e-9f;
    str = QString("%1").arg( val, 0, 'E' );

    //! sys argument
    sysSetArg( argc, argv );

    #ifdef _SIMULATE
        a.setPalette(Qt::black);
    #else
        a.setPalette(QColor(0xC8,0x98,0x60));
//        a.setPalette(Qt::black);
    #endif

    //! meta
    sysSetResourcePath( file_path );

    r_meta::CRMeta::setMetaFile( meta_path"dsometa.xml" );
    r_meta::CBMeta::setMetaFile( meta_path"boardmeta.xml");
    r_meta::CServMeta::setMetaFile( meta_path"servmeta.xml");
    r_meta::CAppMeta::setMetaFile( meta_path"appmeta.xml" );
#ifndef _DO_TEST
    menu_res::RMenuBuilder::attachResource();

    //! start key scan
    app_key_board::CKeyProc keyProc;
    keyProc.start();


    //! services
    serviceMgr *mgr = new serviceMgr();
    //! start service thread
    mgr->start();

    start_time();
    serviceFactory::createServices();dbgout();
    serviceFactory::registerSpy();dbgout();

    //! move to service thread
    serviceFactory::attachServices( mgr );dbgout();
    end_time();
//#endif

//#ifndef _DO_TEST
    //! window
    CAppMain mW;
    //! apps
    start_time();
    mW.createApps();
    end_time();
    mW.setupUi();
    start_time();
    mW.linkEvent();
    end_time();
    mW.retranslateUi();
    start_time();
    mW.buildConnection();
    end_time();

    //! active utility first
    serviceExecutor::post( serv_name_utility,
                           CMD_SERVICE_ACTIVE,
                           (int)1 );

    //! start service
    start_time();
    serviceFactory::startServices();
    serviceFactory::startup();
    end_time();

    //! wait start completed

    sysStartTime();
    while( !serviceExecutor::isIdle() )
    {
        QThread::msleep(100);
    }
    sysDeltaTime();

#endif

#ifdef _DO_TEST

    CAppMain appMain;
    menu_res::RLabel_i_f_f_knob knob;

//    CGrid::getVDivPixels();

    servMath math( serv_name_math1, m1, E_SERVICE_ID_MATH1 );

    while( 1 )
    {
        math.doTest();
    }

#endif

#ifndef _DO_TEST
    start_time();
    QResizeEvent *pEvt = new QResizeEvent( QSize(1024,600), QSize(0,0) );
    qApp->postEvent( &mW,pEvt);

    //! show
    mW.show();

    #ifdef _SIMULATE
        mW.move(200,100);
    #else
        mW.resize(1024,600);
    #endif
    end_time();
    a.processEvents();

    //flamingo is started.
    sysSetStarted();

    //default wave.x=23, wave.y=56
    //serviceExecutor::post( serv_name_plot, servPlot::cmd_wave_xy, (23 << 16)|56 );
    serviceExecutor::post( serv_name_plot, servPlot::cmd_open_layer, DPU_Layer_Fore );

    //!启动USB连接
    //serviceExecutor::post( serv_name_scpi, servScpi::cmd_usbTmcNewConnectint, 0);

    serviceExecutor::clearHistory();

    //checking while new firmware is ready. 1--auto checking
    //serviceExecutor::post( serv_name_storage, MSG_STORAGE_CHECK, 1);
#endif
    return a.exec();
}


