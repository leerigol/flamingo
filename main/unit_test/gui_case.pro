QT       += core
QT       += widgets
QT       += gui
QT       += network


TARGET = gui_case
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += "../../com/lua/lua-5.3.2/src"

platform = $$(PLATFORM)
contains(platform,_gcc ){
DEFINES += _SIMULATE
QT       += serialport
}

# gui测试
DEFINES += _GUI_CASE

HEADERS += /home/wzy/develope/flamingo/framework/app/capp.h
LIBS += -L/home/wzy/develope/flamingo/framework/lib$$(PLATFORM)
LIBS += -L/home/wzy/develope/flamingo/framework/lib$$(PLATFORM)/3rdlib
LIBS += -L/home/wzy/develope/flamingo/framework/lib$$(PLATFORM)/services
LIBS += -L/home/wzy/develope/flamingo/framework/lib$$(PLATFORM)/phy
LIBS += -L/home/wzy/develope/flamingo/framework/lib$$(PLATFORM)/scpi
LIBS += -L/home/wzy/develope/flamingo/framework/lib$$(PLATFORM)/module

#LIBS += -L/home/wzy/develope/flamingo/lib_gcc/3rdlib
#LIBS += -L/home/wzy/develope/flamingo/lib_gcc/services
#LIBS += -L/home/wzy/develope/flamingo/lib_gcc

LIBS += -lservgui -ldsofw -lgui -ldsoengine -lbus

LIBS += -liphych -liphyhori -liphy
LIBS += -liphydg -liphyrelay


LIBS += -lmenu

LIBS += -lservdso

LIBS += -lvirtualkb
LIBS += -linputkeypad

LIBS += -lrwidget
LIBS += -lrwnd
LIBS += -lrstyle

LIBS += -lrevent
LIBS += -lrdsowidget


LIBS += -lcom
LIBS += -lbaseclass
LIBS += -leventtable

LIBS += -lmeta
LIBS += -llua
LIBS += -lrpinyin

LIBS += -lkeyboard
LIBS += -llua
LIBS += -lNE10
LIBS += -larith

platform = $$(PLATFORM)
contains( platform, _gcc ){
DEFINES += _SIMULATE
LIBS -= -lkeyboard
LIBS -= -lNE10
}

SOURCES += \
    ./gui_case/main.cpp \
#    ./widget_case/main.cpp\
    ../../service/servicefactory.cpp \
    ../../service/servdso/servdso.cpp \
    ../../service/servmenutest/servmenutest.cpp \
    ../../app/capp.cpp \
    ../../app/appmenu/cappmenu.cpp \
    ../../app/appmain/cappstartwnd.cpp \
    ../../app/appmain/cappmain.cpp \
    ../../app/appmenutest/cappmenutest.cpp \
    ../../service/servgui/cctrlprogress.cpp \
    ../../service/servgui/ctrlparagraph.cpp

HEADERS += \
    ../../service/service.h \
    ../../service/servicefactory.h \
    ../../service/servdso/servdso.h \
    ../../service/servmenutest/servmenutest.h \
    ../../app/capp.h \
    ../../app/appmenu/cappmenu.h \
    ../../app/appmain/cappstartwnd.h \
    ../../app/appmain/cappmain.h \
    ../../app/appmenutest/cappmenutest.h \
    ../../service/servgui/cctrlprogress.h \
    ../../service/servgui/ctrlparagraph.h

RESOURCES += \
    ../../resource/res.qrc
