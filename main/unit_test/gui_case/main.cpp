#include <QApplication>
#include <QDebug>

#include "../../../meta/crmeta.h"
#include "../../../app/appmain/cappmain.h"
#include "../../../service/service.h"
#include "../../../service/servicefactory.h"

//! keyproc
#include "../../../com/keyevent/ckeyproc.h"

//#define dbgout()    qWarning()<<__LINE__<<__DATE__<<__TIME__;
#define dbgout()


#ifdef _SIMULATE

#define meta_path   "/home/wzy/develope/flamingo/framework/meta/platform_a/"
#define file_path   "/home/wzy/develope/flamingo/framework/resource/"

#else
#define meta_path   "/rigol/w_resource/"
#define file_path   "/rigol/w_resource/"
#endif

//#define def_time()          timeval tmA, tmB;
//#define start_time()        gettimeofday( &tmA, 0 );
//#define end_time()          gettimeofday( &tmB, 0 ); printf("%d %fms\n", __LINE__, (tmB-tmA)/1000.0 );

#define def_time()
#define start_time()
#define end_time()

int main( int argc, char * argv[] )
{
//    QString str;
//    qlonglong llVal = 100000000000;
//    float base = 1.0e-12;
//    float val = llVal * base;
//    gui_fmt::CGuiFormatter::format( str, val );
//    qDebug()<<str;
//    return 0;

    def_time();

    start_time();
    end_time();

    start_time();
    QApplication a( argc, argv );
    end_time();

    a.setPalette(Qt::black);

    sysSetResourcePath( file_path );

    //! meta
    r_meta::CRMeta::setMetaFile( meta_path"dsometa.xml" );
    r_meta::CBMeta::setMetaFile( meta_path"boardmeta.xml");
    r_meta::CAppMeta::setMetaFile( meta_path"appmeta.xml" );
    r_meta::CServMeta::setMetaFile( meta_path"servmeta.xml" );

    menu_res::RMenuBuilder::attachResource();

//#ifndef _SIMULATE
    //! start key scan
    app_key_board::CKeyProc keyProc;
    keyProc.start();
//#endif

    //! services
    serviceMgr *mgr = new serviceMgr();
    //! start service thread
    mgr->start();

    start_time();
    serviceFactory::createServices();
    serviceFactory::registerSpy();

    //! move to service thread
    serviceFactory::attachServices( mgr );
    end_time();

    //! window
    CAppMain mW;

    //! apps
    start_time();
    mW.createApps();
    end_time();
    mW.setupUi();
    start_time();
    mW.linkEvent();
    end_time();
    mW.retranslateUi();
    start_time();
    mW.buildConnection();
    end_time();

    serviceExecutor::post( serv_name_menutester, CMD_SERVICE_ACTIVE, (int)1 );

    //! start service
    start_time();
    serviceFactory::startServices();
    serviceFactory::startup();
    end_time();

    //! wait start completed

    sysStartTime();
    while( !serviceExecutor::isIdle() )
    {
        QThread::msleep(100);
    }
//    sysDeltaTime();

    start_time();
    QResizeEvent *pEvt = new QResizeEvent( QSize(1024,600), QSize(0,0) );
    qApp->postEvent( &mW,pEvt);

    //! show
    mW.show();

    #ifdef _SIMULATE
        mW.move(200,100);
    #else
        mW.resize(1024,600);
    #endif
    end_time();

    int end;
    end = a.exec();

    return end;
}
